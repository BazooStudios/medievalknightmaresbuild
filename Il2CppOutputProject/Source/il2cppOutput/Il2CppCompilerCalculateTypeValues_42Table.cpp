﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ASCLBasicController
struct ASCLBasicController_t2851843984;
// BSPTree/Node
struct Node_t1584791709;
// CollisionSphere
struct CollisionSphere_t3916568399;
// CollisionSphere[]
struct CollisionSphereU5BU5D_t1317006998;
// Enemy[]
struct EnemyU5BU5D_t2006687320;
// PauseMenu
struct PauseMenu_t3916167947;
// SimpleStateMachine/State
struct State_t3920417347;
// SuperCharacterController
struct SuperCharacterController_t3568978942;
// SuperCharacterController/SuperGround
struct SuperGround_t3112734080;
// SuperCharacterController/SuperGround/GroundHit
struct GroundHit_t3864137795;
// SuperCharacterController/UpdateDelegate
struct UpdateDelegate_t1636609007;
// SuperCollisionType
struct SuperCollisionType_t119653226;
// SuperStateMachine/State
struct State_t3931175374;
// System.Action
struct Action_t1264377477;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Enum,System.Collections.Generic.Dictionary`2<System.String,System.Delegate>>
struct Dictionary_2_t74011037;
// System.Collections.Generic.List`1<Ability>
struct List_1_t1638366214;
// System.Collections.Generic.List`1<AbilityCollision>
struct List_1_t3734734497;
// System.Collections.Generic.List`1<SuperCharacterController/IgnoredCollider>
struct List_1_t2120843455;
// System.Collections.Generic.List`1<SuperCollision>
struct List_1_t3251629602;
// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_t3245421752;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Enum
struct Enum_t4135868527;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t2285235057;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Text.DecoderFallback
struct DecoderFallback_t3123823036;
// System.Text.EncoderFallback
struct EncoderFallback_t1188251036;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Void
struct Void_t1185182177;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.ParticleEmitter
struct ParticleEmitter_t3771232109;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t245602842;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;




#ifndef U3CMODULEU3E_T692745564_H
#define U3CMODULEU3E_T692745564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745564 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745564_H
#ifndef U3CMODULEU3E_T692745565_H
#define U3CMODULEU3E_T692745565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745565 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745565_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ABILITY_T166291472_H
#define ABILITY_T166291472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ability
struct  Ability_t166291472  : public RuntimeObject
{
public:
	// System.Int32 Ability::weaponstate
	int32_t ___weaponstate_0;
	// System.Collections.Generic.List`1<AbilityCollision> Ability::collChecks
	List_1_t3734734497 * ___collChecks_1;

public:
	inline static int32_t get_offset_of_weaponstate_0() { return static_cast<int32_t>(offsetof(Ability_t166291472, ___weaponstate_0)); }
	inline int32_t get_weaponstate_0() const { return ___weaponstate_0; }
	inline int32_t* get_address_of_weaponstate_0() { return &___weaponstate_0; }
	inline void set_weaponstate_0(int32_t value)
	{
		___weaponstate_0 = value;
	}

	inline static int32_t get_offset_of_collChecks_1() { return static_cast<int32_t>(offsetof(Ability_t166291472, ___collChecks_1)); }
	inline List_1_t3734734497 * get_collChecks_1() const { return ___collChecks_1; }
	inline List_1_t3734734497 ** get_address_of_collChecks_1() { return &___collChecks_1; }
	inline void set_collChecks_1(List_1_t3734734497 * value)
	{
		___collChecks_1 = value;
		Il2CppCodeGenWriteBarrier((&___collChecks_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABILITY_T166291472_H
#ifndef COLLISIONSPHERE_T3916568399_H
#define COLLISIONSPHERE_T3916568399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollisionSphere
struct  CollisionSphere_t3916568399  : public RuntimeObject
{
public:
	// System.Single CollisionSphere::offset
	float ___offset_0;
	// System.Boolean CollisionSphere::isFeet
	bool ___isFeet_1;
	// System.Boolean CollisionSphere::isHead
	bool ___isHead_2;

public:
	inline static int32_t get_offset_of_offset_0() { return static_cast<int32_t>(offsetof(CollisionSphere_t3916568399, ___offset_0)); }
	inline float get_offset_0() const { return ___offset_0; }
	inline float* get_address_of_offset_0() { return &___offset_0; }
	inline void set_offset_0(float value)
	{
		___offset_0 = value;
	}

	inline static int32_t get_offset_of_isFeet_1() { return static_cast<int32_t>(offsetof(CollisionSphere_t3916568399, ___isFeet_1)); }
	inline bool get_isFeet_1() const { return ___isFeet_1; }
	inline bool* get_address_of_isFeet_1() { return &___isFeet_1; }
	inline void set_isFeet_1(bool value)
	{
		___isFeet_1 = value;
	}

	inline static int32_t get_offset_of_isHead_2() { return static_cast<int32_t>(offsetof(CollisionSphere_t3916568399, ___isHead_2)); }
	inline bool get_isHead_2() const { return ___isHead_2; }
	inline bool* get_address_of_isHead_2() { return &___isHead_2; }
	inline void set_isHead_2(bool value)
	{
		___isHead_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONSPHERE_T3916568399_H
#ifndef CONSTS_T1749595338_H
#define CONSTS_T1749595338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Consts
struct  Consts_t1749595338  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTS_T1749595338_H
#ifndef DEBUGDRAW_T3647725267_H
#define DEBUGDRAW_T3647725267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugDraw
struct  DebugDraw_t3647725267  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGDRAW_T3647725267_H
#ifndef UNIFORMS_T1233092826_H
#define UNIFORMS_T1233092826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleWheelController/Uniforms
struct  Uniforms_t1233092826  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t1233092826_StaticFields
{
public:
	// System.Int32 ExampleWheelController/Uniforms::_MotionAmount
	int32_t ____MotionAmount_0;

public:
	inline static int32_t get_offset_of__MotionAmount_0() { return static_cast<int32_t>(offsetof(Uniforms_t1233092826_StaticFields, ____MotionAmount_0)); }
	inline int32_t get__MotionAmount_0() const { return ____MotionAmount_0; }
	inline int32_t* get_address_of__MotionAmount_0() { return &____MotionAmount_0; }
	inline void set__MotionAmount_0(int32_t value)
	{
		____MotionAmount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T1233092826_H
#ifndef STATE_T3920417347_H
#define STATE_T3920417347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleStateMachine/State
struct  State_t3920417347  : public RuntimeObject
{
public:
	// System.Action SimpleStateMachine/State::DoUpdate
	Action_t1264377477 * ___DoUpdate_0;
	// System.Action SimpleStateMachine/State::DoFixedUpdate
	Action_t1264377477 * ___DoFixedUpdate_1;
	// System.Action SimpleStateMachine/State::DoLateUpdate
	Action_t1264377477 * ___DoLateUpdate_2;
	// System.Action SimpleStateMachine/State::DoManualUpdate
	Action_t1264377477 * ___DoManualUpdate_3;
	// System.Action SimpleStateMachine/State::enterState
	Action_t1264377477 * ___enterState_4;
	// System.Action SimpleStateMachine/State::exitState
	Action_t1264377477 * ___exitState_5;
	// System.Enum SimpleStateMachine/State::currentState
	Enum_t4135868527 * ___currentState_6;

public:
	inline static int32_t get_offset_of_DoUpdate_0() { return static_cast<int32_t>(offsetof(State_t3920417347, ___DoUpdate_0)); }
	inline Action_t1264377477 * get_DoUpdate_0() const { return ___DoUpdate_0; }
	inline Action_t1264377477 ** get_address_of_DoUpdate_0() { return &___DoUpdate_0; }
	inline void set_DoUpdate_0(Action_t1264377477 * value)
	{
		___DoUpdate_0 = value;
		Il2CppCodeGenWriteBarrier((&___DoUpdate_0), value);
	}

	inline static int32_t get_offset_of_DoFixedUpdate_1() { return static_cast<int32_t>(offsetof(State_t3920417347, ___DoFixedUpdate_1)); }
	inline Action_t1264377477 * get_DoFixedUpdate_1() const { return ___DoFixedUpdate_1; }
	inline Action_t1264377477 ** get_address_of_DoFixedUpdate_1() { return &___DoFixedUpdate_1; }
	inline void set_DoFixedUpdate_1(Action_t1264377477 * value)
	{
		___DoFixedUpdate_1 = value;
		Il2CppCodeGenWriteBarrier((&___DoFixedUpdate_1), value);
	}

	inline static int32_t get_offset_of_DoLateUpdate_2() { return static_cast<int32_t>(offsetof(State_t3920417347, ___DoLateUpdate_2)); }
	inline Action_t1264377477 * get_DoLateUpdate_2() const { return ___DoLateUpdate_2; }
	inline Action_t1264377477 ** get_address_of_DoLateUpdate_2() { return &___DoLateUpdate_2; }
	inline void set_DoLateUpdate_2(Action_t1264377477 * value)
	{
		___DoLateUpdate_2 = value;
		Il2CppCodeGenWriteBarrier((&___DoLateUpdate_2), value);
	}

	inline static int32_t get_offset_of_DoManualUpdate_3() { return static_cast<int32_t>(offsetof(State_t3920417347, ___DoManualUpdate_3)); }
	inline Action_t1264377477 * get_DoManualUpdate_3() const { return ___DoManualUpdate_3; }
	inline Action_t1264377477 ** get_address_of_DoManualUpdate_3() { return &___DoManualUpdate_3; }
	inline void set_DoManualUpdate_3(Action_t1264377477 * value)
	{
		___DoManualUpdate_3 = value;
		Il2CppCodeGenWriteBarrier((&___DoManualUpdate_3), value);
	}

	inline static int32_t get_offset_of_enterState_4() { return static_cast<int32_t>(offsetof(State_t3920417347, ___enterState_4)); }
	inline Action_t1264377477 * get_enterState_4() const { return ___enterState_4; }
	inline Action_t1264377477 ** get_address_of_enterState_4() { return &___enterState_4; }
	inline void set_enterState_4(Action_t1264377477 * value)
	{
		___enterState_4 = value;
		Il2CppCodeGenWriteBarrier((&___enterState_4), value);
	}

	inline static int32_t get_offset_of_exitState_5() { return static_cast<int32_t>(offsetof(State_t3920417347, ___exitState_5)); }
	inline Action_t1264377477 * get_exitState_5() const { return ___exitState_5; }
	inline Action_t1264377477 ** get_address_of_exitState_5() { return &___exitState_5; }
	inline void set_exitState_5(Action_t1264377477 * value)
	{
		___exitState_5 = value;
		Il2CppCodeGenWriteBarrier((&___exitState_5), value);
	}

	inline static int32_t get_offset_of_currentState_6() { return static_cast<int32_t>(offsetof(State_t3920417347, ___currentState_6)); }
	inline Enum_t4135868527 * get_currentState_6() const { return ___currentState_6; }
	inline Enum_t4135868527 ** get_address_of_currentState_6() { return &___currentState_6; }
	inline void set_currentState_6(Enum_t4135868527 * value)
	{
		___currentState_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentState_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T3920417347_H
#ifndef SUPERCOLLIDER_T154048620_H
#define SUPERCOLLIDER_T154048620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperCollider
struct  SuperCollider_t154048620  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPERCOLLIDER_T154048620_H
#ifndef SUPERMATH_T2556666644_H
#define SUPERMATH_T2556666644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperMath
struct  SuperMath_t2556666644  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPERMATH_T2556666644_H
#ifndef STATE_T3931175374_H
#define STATE_T3931175374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperStateMachine/State
struct  State_t3931175374  : public RuntimeObject
{
public:
	// System.Action SuperStateMachine/State::DoSuperUpdate
	Action_t1264377477 * ___DoSuperUpdate_0;
	// System.Action SuperStateMachine/State::enterState
	Action_t1264377477 * ___enterState_1;
	// System.Action SuperStateMachine/State::exitState
	Action_t1264377477 * ___exitState_2;
	// System.Enum SuperStateMachine/State::currentState
	Enum_t4135868527 * ___currentState_3;

public:
	inline static int32_t get_offset_of_DoSuperUpdate_0() { return static_cast<int32_t>(offsetof(State_t3931175374, ___DoSuperUpdate_0)); }
	inline Action_t1264377477 * get_DoSuperUpdate_0() const { return ___DoSuperUpdate_0; }
	inline Action_t1264377477 ** get_address_of_DoSuperUpdate_0() { return &___DoSuperUpdate_0; }
	inline void set_DoSuperUpdate_0(Action_t1264377477 * value)
	{
		___DoSuperUpdate_0 = value;
		Il2CppCodeGenWriteBarrier((&___DoSuperUpdate_0), value);
	}

	inline static int32_t get_offset_of_enterState_1() { return static_cast<int32_t>(offsetof(State_t3931175374, ___enterState_1)); }
	inline Action_t1264377477 * get_enterState_1() const { return ___enterState_1; }
	inline Action_t1264377477 ** get_address_of_enterState_1() { return &___enterState_1; }
	inline void set_enterState_1(Action_t1264377477 * value)
	{
		___enterState_1 = value;
		Il2CppCodeGenWriteBarrier((&___enterState_1), value);
	}

	inline static int32_t get_offset_of_exitState_2() { return static_cast<int32_t>(offsetof(State_t3931175374, ___exitState_2)); }
	inline Action_t1264377477 * get_exitState_2() const { return ___exitState_2; }
	inline Action_t1264377477 ** get_address_of_exitState_2() { return &___exitState_2; }
	inline void set_exitState_2(Action_t1264377477 * value)
	{
		___exitState_2 = value;
		Il2CppCodeGenWriteBarrier((&___exitState_2), value);
	}

	inline static int32_t get_offset_of_currentState_3() { return static_cast<int32_t>(offsetof(State_t3931175374, ___currentState_3)); }
	inline Enum_t4135868527 * get_currentState_3() const { return ___currentState_3; }
	inline Enum_t4135868527 ** get_address_of_currentState_3() { return &___currentState_3; }
	inline void set_currentState_3(Enum_t4135868527 * value)
	{
		___currentState_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentState_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T3931175374_H
#ifndef ENCODING_T1523322056_H
#define ENCODING_T1523322056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t1523322056  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t2285235057 * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t1188251036 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t3123823036 * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___dataItem_10)); }
	inline CodePageDataItem_t2285235057 * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t2285235057 ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t2285235057 * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((&___dataItem_10), value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___encoderFallback_13)); }
	inline EncoderFallback_t1188251036 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_t1188251036 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_t1188251036 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___encoderFallback_13), value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___decoderFallback_14)); }
	inline DecoderFallback_t3123823036 * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_t3123823036 ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_t3123823036 * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___decoderFallback_14), value);
	}
};

struct Encoding_t1523322056_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t1523322056 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t1523322056 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t1523322056 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t1523322056 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t1523322056 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t1523322056 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t1523322056 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t1523322056 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t1853889766 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t1523322056 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t1523322056 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t1523322056 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_0), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t1523322056 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t1523322056 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t1523322056 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_1), value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t1523322056 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t1523322056 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t1523322056 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUnicode_2), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t1523322056 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t1523322056 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t1523322056 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_3), value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t1523322056 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t1523322056 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t1523322056 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((&___utf8Encoding_4), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t1523322056 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t1523322056 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t1523322056 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_5), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t1523322056 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t1523322056 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t1523322056 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_6), value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t1523322056 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t1523322056 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t1523322056 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___latin1Encoding_7), value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___encodings_8)); }
	inline Hashtable_t1853889766 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t1853889766 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t1853889766 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_8), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T1523322056_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef __STATICARRAYINITTYPESIZEU3D512_T3317833663_H
#define __STATICARRAYINITTYPESIZEU3D512_T3317833663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512
struct  __StaticArrayInitTypeSizeU3D512_t3317833663 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D512_t3317833663__padding[512];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D512_T3317833663_H
#ifndef __STATICARRAYINITTYPESIZEU3D512_T3317833662_H
#define __STATICARRAYINITTYPESIZEU3D512_T3317833662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512
struct  __StaticArrayInitTypeSizeU3D512_t3317833662 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D512_t3317833662__padding[512];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D512_T3317833662_H
#ifndef MONOENCODING_T666837952_H
#define MONOENCODING_T666837952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.MonoEncoding
struct  MonoEncoding_t666837952  : public Encoding_t1523322056
{
public:
	// System.Int32 I18N.Common.MonoEncoding::win_code_page
	int32_t ___win_code_page_16;

public:
	inline static int32_t get_offset_of_win_code_page_16() { return static_cast<int32_t>(offsetof(MonoEncoding_t666837952, ___win_code_page_16)); }
	inline int32_t get_win_code_page_16() const { return ___win_code_page_16; }
	inline int32_t* get_address_of_win_code_page_16() { return &___win_code_page_16; }
	inline void set_win_code_page_16(int32_t value)
	{
		___win_code_page_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOENCODING_T666837952_H
#ifndef IGNOREDCOLLIDER_T648768713_H
#define IGNOREDCOLLIDER_T648768713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperCharacterController/IgnoredCollider
struct  IgnoredCollider_t648768713 
{
public:
	// UnityEngine.Collider SuperCharacterController/IgnoredCollider::collider
	Collider_t1773347010 * ___collider_0;
	// System.Int32 SuperCharacterController/IgnoredCollider::layer
	int32_t ___layer_1;

public:
	inline static int32_t get_offset_of_collider_0() { return static_cast<int32_t>(offsetof(IgnoredCollider_t648768713, ___collider_0)); }
	inline Collider_t1773347010 * get_collider_0() const { return ___collider_0; }
	inline Collider_t1773347010 ** get_address_of_collider_0() { return &___collider_0; }
	inline void set_collider_0(Collider_t1773347010 * value)
	{
		___collider_0 = value;
		Il2CppCodeGenWriteBarrier((&___collider_0), value);
	}

	inline static int32_t get_offset_of_layer_1() { return static_cast<int32_t>(offsetof(IgnoredCollider_t648768713, ___layer_1)); }
	inline int32_t get_layer_1() const { return ___layer_1; }
	inline int32_t* get_address_of_layer_1() { return &___layer_1; }
	inline void set_layer_1(int32_t value)
	{
		___layer_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SuperCharacterController/IgnoredCollider
struct IgnoredCollider_t648768713_marshaled_pinvoke
{
	Collider_t1773347010 * ___collider_0;
	int32_t ___layer_1;
};
// Native definition for COM marshalling of SuperCharacterController/IgnoredCollider
struct IgnoredCollider_t648768713_marshaled_com
{
	Collider_t1773347010 * ___collider_0;
	int32_t ___layer_1;
};
#endif // IGNOREDCOLLIDER_T648768713_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255372_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255372  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::05C64B887A4D766EEB5842345D6B609A0E3A91C9
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::148F346330E294B4157ED412259A7E7F373E26A8
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___148F346330E294B4157ED412259A7E7F373E26A8_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::1F867F0E96DB3A56943B8CB2662D1DA624023FCD
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::3220DE2BE9769737429E0DE2C6D837CB7C5A02AD
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::356CE585046545CD2D0B109FF8A85DB88EE29074
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___356CE585046545CD2D0B109FF8A85DB88EE29074_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::4FEA2A6324C0192B29C90830F5D578051A4B1B16
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::598D9433A53523A59D462896B803E416AB0B1901
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___598D9433A53523A59D462896B803E416AB0B1901_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::6E6F169B075CACDE575F1FEA42B1376D31D5A7E5
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::7089F9820A6F9CC830909BCD1FAF2EF0A540F348
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::D5DB24A984219B0001B4B86CDE1E0DF54572D83A
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::E11338F644FF140C0E23D8B7526DB4D2D73FC3F7
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4
	__StaticArrayInitTypeSizeU3D512_t3317833663  ___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14;

public:
	inline static int32_t get_offset_of_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0() const { return ___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0() { return &___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0; }
	inline void set_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___05C64B887A4D766EEB5842345D6B609A0E3A91C9_0 = value;
	}

	inline static int32_t get_offset_of_U3148F346330E294B4157ED412259A7E7F373E26A8_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___148F346330E294B4157ED412259A7E7F373E26A8_1)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U3148F346330E294B4157ED412259A7E7F373E26A8_1() const { return ___148F346330E294B4157ED412259A7E7F373E26A8_1; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U3148F346330E294B4157ED412259A7E7F373E26A8_1() { return &___148F346330E294B4157ED412259A7E7F373E26A8_1; }
	inline void set_U3148F346330E294B4157ED412259A7E7F373E26A8_1(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___148F346330E294B4157ED412259A7E7F373E26A8_1 = value;
	}

	inline static int32_t get_offset_of_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2() const { return ___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2() { return &___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2; }
	inline void set_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___1F867F0E96DB3A56943B8CB2662D1DA624023FCD_2 = value;
	}

	inline static int32_t get_offset_of_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3() const { return ___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3() { return &___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3; }
	inline void set_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___3220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3 = value;
	}

	inline static int32_t get_offset_of_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___356CE585046545CD2D0B109FF8A85DB88EE29074_4)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4() const { return ___356CE585046545CD2D0B109FF8A85DB88EE29074_4; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4() { return &___356CE585046545CD2D0B109FF8A85DB88EE29074_4; }
	inline void set_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___356CE585046545CD2D0B109FF8A85DB88EE29074_4 = value;
	}

	inline static int32_t get_offset_of_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5() const { return ___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5() { return &___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5; }
	inline void set_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___4FEA2A6324C0192B29C90830F5D578051A4B1B16_5 = value;
	}

	inline static int32_t get_offset_of_U3598D9433A53523A59D462896B803E416AB0B1901_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___598D9433A53523A59D462896B803E416AB0B1901_6)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U3598D9433A53523A59D462896B803E416AB0B1901_6() const { return ___598D9433A53523A59D462896B803E416AB0B1901_6; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U3598D9433A53523A59D462896B803E416AB0B1901_6() { return &___598D9433A53523A59D462896B803E416AB0B1901_6; }
	inline void set_U3598D9433A53523A59D462896B803E416AB0B1901_6(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___598D9433A53523A59D462896B803E416AB0B1901_6 = value;
	}

	inline static int32_t get_offset_of_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7() const { return ___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7() { return &___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7; }
	inline void set_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___6E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7 = value;
	}

	inline static int32_t get_offset_of_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8() const { return ___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8() { return &___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8; }
	inline void set_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___7089F9820A6F9CC830909BCD1FAF2EF0A540F348_8 = value;
	}

	inline static int32_t get_offset_of_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9() const { return ___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9() { return &___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9; }
	inline void set_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___9ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9 = value;
	}

	inline static int32_t get_offset_of_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10() const { return ___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10() { return &___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10; }
	inline void set_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10 = value;
	}

	inline static int32_t get_offset_of_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11() const { return ___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11() { return &___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11; }
	inline void set_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11 = value;
	}

	inline static int32_t get_offset_of_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12() const { return ___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12() { return &___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12; }
	inline void set_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12 = value;
	}

	inline static int32_t get_offset_of_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13() const { return ___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13() { return &___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13; }
	inline void set_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13 = value;
	}

	inline static int32_t get_offset_of_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields, ___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663  get_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14() const { return ___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833663 * get_address_of_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14() { return &___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14; }
	inline void set_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14(__StaticArrayInitTypeSizeU3D512_t3317833663  value)
	{
		___FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255372_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255371_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255371  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::05B1E1067273E188A50BA4342B4BC09ABEE669CD
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::09885C8B0840E863B3A14261999895D896677D5A
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___09885C8B0840E863B3A14261999895D896677D5A_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::271EF3D1C1F8136E08DBCAB443CE02538A4156F0
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::2E1C78D5F4666324672DFACDC9307935CF14E98F
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___2E1C78D5F4666324672DFACDC9307935CF14E98F_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::3551F16F8A11003B8289B76B9E3D97969B68E7D9
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::36A0A1CE2E522AB1D22668B38F6126D2F6E17D44
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::37C03949C69BDDA042205E1A573B349E75F693B8
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___37C03949C69BDDA042205E1A573B349E75F693B8_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::47DCC01E928630EBCC018C5E0285145985F92532
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___47DCC01E928630EBCC018C5E0285145985F92532_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::52623ED7F33E8C1C541F1C3101FA981B173D795E
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___52623ED7F33E8C1C541F1C3101FA981B173D795E_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::659C24C855D0E50CBD366B2774AF841E29202E70
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___659C24C855D0E50CBD366B2774AF841E29202E70_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::82273BF74F02F3FDFABEED76BCA4B82DDB2C2761
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::8E958C1847104F390F9C9B64F0F39C98ED4AC63F
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::94F89F72CC0508D891A0C71DD7B57B703869A3FB
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::9EF46C677634CB82C0BD475E372C71C5F68C981A
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___9EF46C677634CB82C0BD475E372C71C5F68C981A_21;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::A5A5BE77BD3D095389FABC21D18BB648787E6618
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___A5A5BE77BD3D095389FABC21D18BB648787E6618_22;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B1D1CEE08985760776A35065014EAF3D4D3CDE8D
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B2574B82126B684C0CB3CF93BF7473F0FBB34400
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::B6BD9A82204938ABFE97CF3FAB5A47824080EAA0
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::BC1CEF8131E7F0D8206029373157806126E21026
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___BC1CEF8131E7F0D8206029373157806126E21026_27;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C14817C33562352073848F1A8EA633C19CF1A4F5
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___C14817C33562352073848F1A8EA633C19CF1A4F5_28;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C392E993B9E622A36C30E0BB997A9F41293CD9EF
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::CFB85A64A0D1FAB523C3DE56096F8803CDFEA674
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::D257291FBF3DA6F5C38B3275534902BC9EDE92EA
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::D4795500A3D0F00D8EE85626648C3FBAFA4F70C3
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::DF4C38A5E59685BBEC109623762B6914BE00E866
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___DF4C38A5E59685BBEC109623762B6914BE00E866_35;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=512 <PrivateImplementationDetails>::F632BE2E03B27F265F779A5D3D217E5C14AB6CB5
	__StaticArrayInitTypeSizeU3D512_t3317833662  ___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36;

public:
	inline static int32_t get_offset_of_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0() const { return ___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0() { return &___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0; }
	inline void set_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___05B1E1067273E188A50BA4342B4BC09ABEE669CD_0 = value;
	}

	inline static int32_t get_offset_of_U309885C8B0840E863B3A14261999895D896677D5A_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___09885C8B0840E863B3A14261999895D896677D5A_1)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U309885C8B0840E863B3A14261999895D896677D5A_1() const { return ___09885C8B0840E863B3A14261999895D896677D5A_1; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U309885C8B0840E863B3A14261999895D896677D5A_1() { return &___09885C8B0840E863B3A14261999895D896677D5A_1; }
	inline void set_U309885C8B0840E863B3A14261999895D896677D5A_1(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___09885C8B0840E863B3A14261999895D896677D5A_1 = value;
	}

	inline static int32_t get_offset_of_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2() const { return ___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2() { return &___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2; }
	inline void set_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___0BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2 = value;
	}

	inline static int32_t get_offset_of_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3() const { return ___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3() { return &___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3; }
	inline void set_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3 = value;
	}

	inline static int32_t get_offset_of_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___2E1C78D5F4666324672DFACDC9307935CF14E98F_4)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4() const { return ___2E1C78D5F4666324672DFACDC9307935CF14E98F_4; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4() { return &___2E1C78D5F4666324672DFACDC9307935CF14E98F_4; }
	inline void set_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___2E1C78D5F4666324672DFACDC9307935CF14E98F_4 = value;
	}

	inline static int32_t get_offset_of_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5() const { return ___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5() { return &___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5; }
	inline void set_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___3551F16F8A11003B8289B76B9E3D97969B68E7D9_5 = value;
	}

	inline static int32_t get_offset_of_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6() const { return ___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6() { return &___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6; }
	inline void set_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___36A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6 = value;
	}

	inline static int32_t get_offset_of_U337C03949C69BDDA042205E1A573B349E75F693B8_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___37C03949C69BDDA042205E1A573B349E75F693B8_7)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U337C03949C69BDDA042205E1A573B349E75F693B8_7() const { return ___37C03949C69BDDA042205E1A573B349E75F693B8_7; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U337C03949C69BDDA042205E1A573B349E75F693B8_7() { return &___37C03949C69BDDA042205E1A573B349E75F693B8_7; }
	inline void set_U337C03949C69BDDA042205E1A573B349E75F693B8_7(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___37C03949C69BDDA042205E1A573B349E75F693B8_7 = value;
	}

	inline static int32_t get_offset_of_U347DCC01E928630EBCC018C5E0285145985F92532_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___47DCC01E928630EBCC018C5E0285145985F92532_8)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U347DCC01E928630EBCC018C5E0285145985F92532_8() const { return ___47DCC01E928630EBCC018C5E0285145985F92532_8; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U347DCC01E928630EBCC018C5E0285145985F92532_8() { return &___47DCC01E928630EBCC018C5E0285145985F92532_8; }
	inline void set_U347DCC01E928630EBCC018C5E0285145985F92532_8(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___47DCC01E928630EBCC018C5E0285145985F92532_8 = value;
	}

	inline static int32_t get_offset_of_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___52623ED7F33E8C1C541F1C3101FA981B173D795E_9)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9() const { return ___52623ED7F33E8C1C541F1C3101FA981B173D795E_9; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9() { return &___52623ED7F33E8C1C541F1C3101FA981B173D795E_9; }
	inline void set_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___52623ED7F33E8C1C541F1C3101FA981B173D795E_9 = value;
	}

	inline static int32_t get_offset_of_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10() const { return ___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10() { return &___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10; }
	inline void set_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___5EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10 = value;
	}

	inline static int32_t get_offset_of_U3659C24C855D0E50CBD366B2774AF841E29202E70_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___659C24C855D0E50CBD366B2774AF841E29202E70_11)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U3659C24C855D0E50CBD366B2774AF841E29202E70_11() const { return ___659C24C855D0E50CBD366B2774AF841E29202E70_11; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U3659C24C855D0E50CBD366B2774AF841E29202E70_11() { return &___659C24C855D0E50CBD366B2774AF841E29202E70_11; }
	inline void set_U3659C24C855D0E50CBD366B2774AF841E29202E70_11(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___659C24C855D0E50CBD366B2774AF841E29202E70_11 = value;
	}

	inline static int32_t get_offset_of_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12() const { return ___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12() { return &___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12; }
	inline void set_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___6B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12 = value;
	}

	inline static int32_t get_offset_of_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13() const { return ___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13() { return &___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13; }
	inline void set_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___71D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13 = value;
	}

	inline static int32_t get_offset_of_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14() const { return ___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14() { return &___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14; }
	inline void set_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___7A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14 = value;
	}

	inline static int32_t get_offset_of_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15() const { return ___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15() { return &___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15; }
	inline void set_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___7AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15 = value;
	}

	inline static int32_t get_offset_of_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16() const { return ___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16() { return &___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16; }
	inline void set_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___82273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16 = value;
	}

	inline static int32_t get_offset_of_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17() const { return ___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17() { return &___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17; }
	inline void set_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___8C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17 = value;
	}

	inline static int32_t get_offset_of_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18() const { return ___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18() { return &___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18; }
	inline void set_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___8E958C1847104F390F9C9B64F0F39C98ED4AC63F_18 = value;
	}

	inline static int32_t get_offset_of_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19() const { return ___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19() { return &___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19; }
	inline void set_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___94F89F72CC0508D891A0C71DD7B57B703869A3FB_19 = value;
	}

	inline static int32_t get_offset_of_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20() const { return ___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20() { return &___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20; }
	inline void set_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___9E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20 = value;
	}

	inline static int32_t get_offset_of_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___9EF46C677634CB82C0BD475E372C71C5F68C981A_21)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21() const { return ___9EF46C677634CB82C0BD475E372C71C5F68C981A_21; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21() { return &___9EF46C677634CB82C0BD475E372C71C5F68C981A_21; }
	inline void set_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___9EF46C677634CB82C0BD475E372C71C5F68C981A_21 = value;
	}

	inline static int32_t get_offset_of_A5A5BE77BD3D095389FABC21D18BB648787E6618_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___A5A5BE77BD3D095389FABC21D18BB648787E6618_22)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_A5A5BE77BD3D095389FABC21D18BB648787E6618_22() const { return ___A5A5BE77BD3D095389FABC21D18BB648787E6618_22; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_A5A5BE77BD3D095389FABC21D18BB648787E6618_22() { return &___A5A5BE77BD3D095389FABC21D18BB648787E6618_22; }
	inline void set_A5A5BE77BD3D095389FABC21D18BB648787E6618_22(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___A5A5BE77BD3D095389FABC21D18BB648787E6618_22 = value;
	}

	inline static int32_t get_offset_of_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23() const { return ___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23() { return &___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23; }
	inline void set_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23 = value;
	}

	inline static int32_t get_offset_of_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24() const { return ___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24() { return &___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24; }
	inline void set_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24 = value;
	}

	inline static int32_t get_offset_of_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25() const { return ___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25() { return &___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25; }
	inline void set_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___B2574B82126B684C0CB3CF93BF7473F0FBB34400_25 = value;
	}

	inline static int32_t get_offset_of_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26() const { return ___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26() { return &___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26; }
	inline void set_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26 = value;
	}

	inline static int32_t get_offset_of_BC1CEF8131E7F0D8206029373157806126E21026_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___BC1CEF8131E7F0D8206029373157806126E21026_27)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_BC1CEF8131E7F0D8206029373157806126E21026_27() const { return ___BC1CEF8131E7F0D8206029373157806126E21026_27; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_BC1CEF8131E7F0D8206029373157806126E21026_27() { return &___BC1CEF8131E7F0D8206029373157806126E21026_27; }
	inline void set_BC1CEF8131E7F0D8206029373157806126E21026_27(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___BC1CEF8131E7F0D8206029373157806126E21026_27 = value;
	}

	inline static int32_t get_offset_of_C14817C33562352073848F1A8EA633C19CF1A4F5_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___C14817C33562352073848F1A8EA633C19CF1A4F5_28)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_C14817C33562352073848F1A8EA633C19CF1A4F5_28() const { return ___C14817C33562352073848F1A8EA633C19CF1A4F5_28; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_C14817C33562352073848F1A8EA633C19CF1A4F5_28() { return &___C14817C33562352073848F1A8EA633C19CF1A4F5_28; }
	inline void set_C14817C33562352073848F1A8EA633C19CF1A4F5_28(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___C14817C33562352073848F1A8EA633C19CF1A4F5_28 = value;
	}

	inline static int32_t get_offset_of_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29() const { return ___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29() { return &___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29; }
	inline void set_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___C392E993B9E622A36C30E0BB997A9F41293CD9EF_29 = value;
	}

	inline static int32_t get_offset_of_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30() const { return ___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30() { return &___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30; }
	inline void set_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30 = value;
	}

	inline static int32_t get_offset_of_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31() const { return ___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31() { return &___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31; }
	inline void set_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31 = value;
	}

	inline static int32_t get_offset_of_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32() const { return ___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32() { return &___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32; }
	inline void set_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32 = value;
	}

	inline static int32_t get_offset_of_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33() const { return ___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33() { return &___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33; }
	inline void set_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33 = value;
	}

	inline static int32_t get_offset_of_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34() const { return ___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34() { return &___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34; }
	inline void set_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34 = value;
	}

	inline static int32_t get_offset_of_DF4C38A5E59685BBEC109623762B6914BE00E866_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___DF4C38A5E59685BBEC109623762B6914BE00E866_35)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_DF4C38A5E59685BBEC109623762B6914BE00E866_35() const { return ___DF4C38A5E59685BBEC109623762B6914BE00E866_35; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_DF4C38A5E59685BBEC109623762B6914BE00E866_35() { return &___DF4C38A5E59685BBEC109623762B6914BE00E866_35; }
	inline void set_DF4C38A5E59685BBEC109623762B6914BE00E866_35(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___DF4C38A5E59685BBEC109623762B6914BE00E866_35 = value;
	}

	inline static int32_t get_offset_of_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields, ___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36)); }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662  get_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36() const { return ___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36; }
	inline __StaticArrayInitTypeSizeU3D512_t3317833662 * get_address_of_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36() { return &___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36; }
	inline void set_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36(__StaticArrayInitTypeSizeU3D512_t3317833662  value)
	{
		___F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255371_H
#ifndef ABILITYCOLLISION_T2262659755_H
#define ABILITYCOLLISION_T2262659755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AbilityCollision
struct  AbilityCollision_t2262659755  : public RuntimeObject
{
public:
	// System.Int32 AbilityCollision::type
	int32_t ___type_0;
	// UnityEngine.Vector3 AbilityCollision::position
	Vector3_t3722313464  ___position_1;
	// UnityEngine.Vector3 AbilityCollision::rotation
	Vector3_t3722313464  ___rotation_2;
	// System.Single AbilityCollision::range
	float ___range_3;
	// System.Single AbilityCollision::angle
	float ___angle_4;
	// System.Single AbilityCollision::speed
	float ___speed_5;
	// System.Single AbilityCollision::damage
	float ___damage_6;
	// UnityEngine.Transform AbilityCollision::missile
	Transform_t3600365921 * ___missile_7;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AbilityCollision_t2262659755, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(AbilityCollision_t2262659755, ___position_1)); }
	inline Vector3_t3722313464  get_position_1() const { return ___position_1; }
	inline Vector3_t3722313464 * get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(Vector3_t3722313464  value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(AbilityCollision_t2262659755, ___rotation_2)); }
	inline Vector3_t3722313464  get_rotation_2() const { return ___rotation_2; }
	inline Vector3_t3722313464 * get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(Vector3_t3722313464  value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_range_3() { return static_cast<int32_t>(offsetof(AbilityCollision_t2262659755, ___range_3)); }
	inline float get_range_3() const { return ___range_3; }
	inline float* get_address_of_range_3() { return &___range_3; }
	inline void set_range_3(float value)
	{
		___range_3 = value;
	}

	inline static int32_t get_offset_of_angle_4() { return static_cast<int32_t>(offsetof(AbilityCollision_t2262659755, ___angle_4)); }
	inline float get_angle_4() const { return ___angle_4; }
	inline float* get_address_of_angle_4() { return &___angle_4; }
	inline void set_angle_4(float value)
	{
		___angle_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(AbilityCollision_t2262659755, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_damage_6() { return static_cast<int32_t>(offsetof(AbilityCollision_t2262659755, ___damage_6)); }
	inline float get_damage_6() const { return ___damage_6; }
	inline float* get_address_of_damage_6() { return &___damage_6; }
	inline void set_damage_6(float value)
	{
		___damage_6 = value;
	}

	inline static int32_t get_offset_of_missile_7() { return static_cast<int32_t>(offsetof(AbilityCollision_t2262659755, ___missile_7)); }
	inline Transform_t3600365921 * get_missile_7() const { return ___missile_7; }
	inline Transform_t3600365921 ** get_address_of_missile_7() { return &___missile_7; }
	inline void set_missile_7(Transform_t3600365921 * value)
	{
		___missile_7 = value;
		Il2CppCodeGenWriteBarrier((&___missile_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABILITYCOLLISION_T2262659755_H
#ifndef NODE_T1584791709_H
#define NODE_T1584791709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BSPTree/Node
struct  Node_t1584791709  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 BSPTree/Node::partitionPoint
	Vector3_t3722313464  ___partitionPoint_0;
	// UnityEngine.Vector3 BSPTree/Node::partitionNormal
	Vector3_t3722313464  ___partitionNormal_1;
	// BSPTree/Node BSPTree/Node::positiveChild
	Node_t1584791709 * ___positiveChild_2;
	// BSPTree/Node BSPTree/Node::negativeChild
	Node_t1584791709 * ___negativeChild_3;
	// System.Int32[] BSPTree/Node::triangles
	Int32U5BU5D_t385246372* ___triangles_4;

public:
	inline static int32_t get_offset_of_partitionPoint_0() { return static_cast<int32_t>(offsetof(Node_t1584791709, ___partitionPoint_0)); }
	inline Vector3_t3722313464  get_partitionPoint_0() const { return ___partitionPoint_0; }
	inline Vector3_t3722313464 * get_address_of_partitionPoint_0() { return &___partitionPoint_0; }
	inline void set_partitionPoint_0(Vector3_t3722313464  value)
	{
		___partitionPoint_0 = value;
	}

	inline static int32_t get_offset_of_partitionNormal_1() { return static_cast<int32_t>(offsetof(Node_t1584791709, ___partitionNormal_1)); }
	inline Vector3_t3722313464  get_partitionNormal_1() const { return ___partitionNormal_1; }
	inline Vector3_t3722313464 * get_address_of_partitionNormal_1() { return &___partitionNormal_1; }
	inline void set_partitionNormal_1(Vector3_t3722313464  value)
	{
		___partitionNormal_1 = value;
	}

	inline static int32_t get_offset_of_positiveChild_2() { return static_cast<int32_t>(offsetof(Node_t1584791709, ___positiveChild_2)); }
	inline Node_t1584791709 * get_positiveChild_2() const { return ___positiveChild_2; }
	inline Node_t1584791709 ** get_address_of_positiveChild_2() { return &___positiveChild_2; }
	inline void set_positiveChild_2(Node_t1584791709 * value)
	{
		___positiveChild_2 = value;
		Il2CppCodeGenWriteBarrier((&___positiveChild_2), value);
	}

	inline static int32_t get_offset_of_negativeChild_3() { return static_cast<int32_t>(offsetof(Node_t1584791709, ___negativeChild_3)); }
	inline Node_t1584791709 * get_negativeChild_3() const { return ___negativeChild_3; }
	inline Node_t1584791709 ** get_address_of_negativeChild_3() { return &___negativeChild_3; }
	inline void set_negativeChild_3(Node_t1584791709 * value)
	{
		___negativeChild_3 = value;
		Il2CppCodeGenWriteBarrier((&___negativeChild_3), value);
	}

	inline static int32_t get_offset_of_triangles_4() { return static_cast<int32_t>(offsetof(Node_t1584791709, ___triangles_4)); }
	inline Int32U5BU5D_t385246372* get_triangles_4() const { return ___triangles_4; }
	inline Int32U5BU5D_t385246372** get_address_of_triangles_4() { return &___triangles_4; }
	inline void set_triangles_4(Int32U5BU5D_t385246372* value)
	{
		___triangles_4 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T1584791709_H
#ifndef BYTEENCODING_T1371924561_H
#define BYTEENCODING_T1371924561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.ByteEncoding
struct  ByteEncoding_t1371924561  : public MonoEncoding_t666837952
{
public:
	// System.Char[] I18N.Common.ByteEncoding::toChars
	CharU5BU5D_t3528271667* ___toChars_17;
	// System.String I18N.Common.ByteEncoding::encodingName
	String_t* ___encodingName_18;
	// System.String I18N.Common.ByteEncoding::bodyName
	String_t* ___bodyName_19;
	// System.String I18N.Common.ByteEncoding::headerName
	String_t* ___headerName_20;
	// System.String I18N.Common.ByteEncoding::webName
	String_t* ___webName_21;
	// System.Boolean I18N.Common.ByteEncoding::isBrowserDisplay
	bool ___isBrowserDisplay_22;
	// System.Boolean I18N.Common.ByteEncoding::isBrowserSave
	bool ___isBrowserSave_23;
	// System.Boolean I18N.Common.ByteEncoding::isMailNewsDisplay
	bool ___isMailNewsDisplay_24;
	// System.Boolean I18N.Common.ByteEncoding::isMailNewsSave
	bool ___isMailNewsSave_25;
	// System.Int32 I18N.Common.ByteEncoding::windowsCodePage
	int32_t ___windowsCodePage_26;

public:
	inline static int32_t get_offset_of_toChars_17() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___toChars_17)); }
	inline CharU5BU5D_t3528271667* get_toChars_17() const { return ___toChars_17; }
	inline CharU5BU5D_t3528271667** get_address_of_toChars_17() { return &___toChars_17; }
	inline void set_toChars_17(CharU5BU5D_t3528271667* value)
	{
		___toChars_17 = value;
		Il2CppCodeGenWriteBarrier((&___toChars_17), value);
	}

	inline static int32_t get_offset_of_encodingName_18() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___encodingName_18)); }
	inline String_t* get_encodingName_18() const { return ___encodingName_18; }
	inline String_t** get_address_of_encodingName_18() { return &___encodingName_18; }
	inline void set_encodingName_18(String_t* value)
	{
		___encodingName_18 = value;
		Il2CppCodeGenWriteBarrier((&___encodingName_18), value);
	}

	inline static int32_t get_offset_of_bodyName_19() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___bodyName_19)); }
	inline String_t* get_bodyName_19() const { return ___bodyName_19; }
	inline String_t** get_address_of_bodyName_19() { return &___bodyName_19; }
	inline void set_bodyName_19(String_t* value)
	{
		___bodyName_19 = value;
		Il2CppCodeGenWriteBarrier((&___bodyName_19), value);
	}

	inline static int32_t get_offset_of_headerName_20() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___headerName_20)); }
	inline String_t* get_headerName_20() const { return ___headerName_20; }
	inline String_t** get_address_of_headerName_20() { return &___headerName_20; }
	inline void set_headerName_20(String_t* value)
	{
		___headerName_20 = value;
		Il2CppCodeGenWriteBarrier((&___headerName_20), value);
	}

	inline static int32_t get_offset_of_webName_21() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___webName_21)); }
	inline String_t* get_webName_21() const { return ___webName_21; }
	inline String_t** get_address_of_webName_21() { return &___webName_21; }
	inline void set_webName_21(String_t* value)
	{
		___webName_21 = value;
		Il2CppCodeGenWriteBarrier((&___webName_21), value);
	}

	inline static int32_t get_offset_of_isBrowserDisplay_22() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___isBrowserDisplay_22)); }
	inline bool get_isBrowserDisplay_22() const { return ___isBrowserDisplay_22; }
	inline bool* get_address_of_isBrowserDisplay_22() { return &___isBrowserDisplay_22; }
	inline void set_isBrowserDisplay_22(bool value)
	{
		___isBrowserDisplay_22 = value;
	}

	inline static int32_t get_offset_of_isBrowserSave_23() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___isBrowserSave_23)); }
	inline bool get_isBrowserSave_23() const { return ___isBrowserSave_23; }
	inline bool* get_address_of_isBrowserSave_23() { return &___isBrowserSave_23; }
	inline void set_isBrowserSave_23(bool value)
	{
		___isBrowserSave_23 = value;
	}

	inline static int32_t get_offset_of_isMailNewsDisplay_24() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___isMailNewsDisplay_24)); }
	inline bool get_isMailNewsDisplay_24() const { return ___isMailNewsDisplay_24; }
	inline bool* get_address_of_isMailNewsDisplay_24() { return &___isMailNewsDisplay_24; }
	inline void set_isMailNewsDisplay_24(bool value)
	{
		___isMailNewsDisplay_24 = value;
	}

	inline static int32_t get_offset_of_isMailNewsSave_25() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___isMailNewsSave_25)); }
	inline bool get_isMailNewsSave_25() const { return ___isMailNewsSave_25; }
	inline bool* get_address_of_isMailNewsSave_25() { return &___isMailNewsSave_25; }
	inline void set_isMailNewsSave_25(bool value)
	{
		___isMailNewsSave_25 = value;
	}

	inline static int32_t get_offset_of_windowsCodePage_26() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561, ___windowsCodePage_26)); }
	inline int32_t get_windowsCodePage_26() const { return ___windowsCodePage_26; }
	inline int32_t* get_address_of_windowsCodePage_26() { return &___windowsCodePage_26; }
	inline void set_windowsCodePage_26(int32_t value)
	{
		___windowsCodePage_26 = value;
	}
};

struct ByteEncoding_t1371924561_StaticFields
{
public:
	// System.Byte[] I18N.Common.ByteEncoding::isNormalized
	ByteU5BU5D_t4116647657* ___isNormalized_27;
	// System.Byte[] I18N.Common.ByteEncoding::isNormalizedComputed
	ByteU5BU5D_t4116647657* ___isNormalizedComputed_28;
	// System.Byte[] I18N.Common.ByteEncoding::normalization_bytes
	ByteU5BU5D_t4116647657* ___normalization_bytes_29;

public:
	inline static int32_t get_offset_of_isNormalized_27() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561_StaticFields, ___isNormalized_27)); }
	inline ByteU5BU5D_t4116647657* get_isNormalized_27() const { return ___isNormalized_27; }
	inline ByteU5BU5D_t4116647657** get_address_of_isNormalized_27() { return &___isNormalized_27; }
	inline void set_isNormalized_27(ByteU5BU5D_t4116647657* value)
	{
		___isNormalized_27 = value;
		Il2CppCodeGenWriteBarrier((&___isNormalized_27), value);
	}

	inline static int32_t get_offset_of_isNormalizedComputed_28() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561_StaticFields, ___isNormalizedComputed_28)); }
	inline ByteU5BU5D_t4116647657* get_isNormalizedComputed_28() const { return ___isNormalizedComputed_28; }
	inline ByteU5BU5D_t4116647657** get_address_of_isNormalizedComputed_28() { return &___isNormalizedComputed_28; }
	inline void set_isNormalizedComputed_28(ByteU5BU5D_t4116647657* value)
	{
		___isNormalizedComputed_28 = value;
		Il2CppCodeGenWriteBarrier((&___isNormalizedComputed_28), value);
	}

	inline static int32_t get_offset_of_normalization_bytes_29() { return static_cast<int32_t>(offsetof(ByteEncoding_t1371924561_StaticFields, ___normalization_bytes_29)); }
	inline ByteU5BU5D_t4116647657* get_normalization_bytes_29() const { return ___normalization_bytes_29; }
	inline ByteU5BU5D_t4116647657** get_address_of_normalization_bytes_29() { return &___normalization_bytes_29; }
	inline void set_normalization_bytes_29(ByteU5BU5D_t4116647657* value)
	{
		___normalization_bytes_29 = value;
		Il2CppCodeGenWriteBarrier((&___normalization_bytes_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEENCODING_T1371924561_H
#ifndef GROUNDHIT_T3864137795_H
#define GROUNDHIT_T3864137795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperCharacterController/SuperGround/GroundHit
struct  GroundHit_t3864137795  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 SuperCharacterController/SuperGround/GroundHit::<point>k__BackingField
	Vector3_t3722313464  ___U3CpointU3Ek__BackingField_0;
	// UnityEngine.Vector3 SuperCharacterController/SuperGround/GroundHit::<normal>k__BackingField
	Vector3_t3722313464  ___U3CnormalU3Ek__BackingField_1;
	// System.Single SuperCharacterController/SuperGround/GroundHit::<distance>k__BackingField
	float ___U3CdistanceU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CpointU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GroundHit_t3864137795, ___U3CpointU3Ek__BackingField_0)); }
	inline Vector3_t3722313464  get_U3CpointU3Ek__BackingField_0() const { return ___U3CpointU3Ek__BackingField_0; }
	inline Vector3_t3722313464 * get_address_of_U3CpointU3Ek__BackingField_0() { return &___U3CpointU3Ek__BackingField_0; }
	inline void set_U3CpointU3Ek__BackingField_0(Vector3_t3722313464  value)
	{
		___U3CpointU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CnormalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GroundHit_t3864137795, ___U3CnormalU3Ek__BackingField_1)); }
	inline Vector3_t3722313464  get_U3CnormalU3Ek__BackingField_1() const { return ___U3CnormalU3Ek__BackingField_1; }
	inline Vector3_t3722313464 * get_address_of_U3CnormalU3Ek__BackingField_1() { return &___U3CnormalU3Ek__BackingField_1; }
	inline void set_U3CnormalU3Ek__BackingField_1(Vector3_t3722313464  value)
	{
		___U3CnormalU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CdistanceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GroundHit_t3864137795, ___U3CdistanceU3Ek__BackingField_2)); }
	inline float get_U3CdistanceU3Ek__BackingField_2() const { return ___U3CdistanceU3Ek__BackingField_2; }
	inline float* get_address_of_U3CdistanceU3Ek__BackingField_2() { return &___U3CdistanceU3Ek__BackingField_2; }
	inline void set_U3CdistanceU3Ek__BackingField_2(float value)
	{
		___U3CdistanceU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUNDHIT_T3864137795_H
#ifndef SUPERCOLLISION_T1779554860_H
#define SUPERCOLLISION_T1779554860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperCollision
struct  SuperCollision_t1779554860 
{
public:
	// CollisionSphere SuperCollision::collisionSphere
	CollisionSphere_t3916568399 * ___collisionSphere_0;
	// SuperCollisionType SuperCollision::superCollisionType
	SuperCollisionType_t119653226 * ___superCollisionType_1;
	// UnityEngine.GameObject SuperCollision::gameObject
	GameObject_t1113636619 * ___gameObject_2;
	// UnityEngine.Vector3 SuperCollision::point
	Vector3_t3722313464  ___point_3;
	// UnityEngine.Vector3 SuperCollision::normal
	Vector3_t3722313464  ___normal_4;

public:
	inline static int32_t get_offset_of_collisionSphere_0() { return static_cast<int32_t>(offsetof(SuperCollision_t1779554860, ___collisionSphere_0)); }
	inline CollisionSphere_t3916568399 * get_collisionSphere_0() const { return ___collisionSphere_0; }
	inline CollisionSphere_t3916568399 ** get_address_of_collisionSphere_0() { return &___collisionSphere_0; }
	inline void set_collisionSphere_0(CollisionSphere_t3916568399 * value)
	{
		___collisionSphere_0 = value;
		Il2CppCodeGenWriteBarrier((&___collisionSphere_0), value);
	}

	inline static int32_t get_offset_of_superCollisionType_1() { return static_cast<int32_t>(offsetof(SuperCollision_t1779554860, ___superCollisionType_1)); }
	inline SuperCollisionType_t119653226 * get_superCollisionType_1() const { return ___superCollisionType_1; }
	inline SuperCollisionType_t119653226 ** get_address_of_superCollisionType_1() { return &___superCollisionType_1; }
	inline void set_superCollisionType_1(SuperCollisionType_t119653226 * value)
	{
		___superCollisionType_1 = value;
		Il2CppCodeGenWriteBarrier((&___superCollisionType_1), value);
	}

	inline static int32_t get_offset_of_gameObject_2() { return static_cast<int32_t>(offsetof(SuperCollision_t1779554860, ___gameObject_2)); }
	inline GameObject_t1113636619 * get_gameObject_2() const { return ___gameObject_2; }
	inline GameObject_t1113636619 ** get_address_of_gameObject_2() { return &___gameObject_2; }
	inline void set_gameObject_2(GameObject_t1113636619 * value)
	{
		___gameObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_2), value);
	}

	inline static int32_t get_offset_of_point_3() { return static_cast<int32_t>(offsetof(SuperCollision_t1779554860, ___point_3)); }
	inline Vector3_t3722313464  get_point_3() const { return ___point_3; }
	inline Vector3_t3722313464 * get_address_of_point_3() { return &___point_3; }
	inline void set_point_3(Vector3_t3722313464  value)
	{
		___point_3 = value;
	}

	inline static int32_t get_offset_of_normal_4() { return static_cast<int32_t>(offsetof(SuperCollision_t1779554860, ___normal_4)); }
	inline Vector3_t3722313464  get_normal_4() const { return ___normal_4; }
	inline Vector3_t3722313464 * get_address_of_normal_4() { return &___normal_4; }
	inline void set_normal_4(Vector3_t3722313464  value)
	{
		___normal_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SuperCollision
struct SuperCollision_t1779554860_marshaled_pinvoke
{
	CollisionSphere_t3916568399 * ___collisionSphere_0;
	SuperCollisionType_t119653226 * ___superCollisionType_1;
	GameObject_t1113636619 * ___gameObject_2;
	Vector3_t3722313464  ___point_3;
	Vector3_t3722313464  ___normal_4;
};
// Native definition for COM marshalling of SuperCollision
struct SuperCollision_t1779554860_marshaled_com
{
	CollisionSphere_t3916568399 * ___collisionSphere_0;
	SuperCollisionType_t119653226 * ___superCollisionType_1;
	GameObject_t1113636619 * ___gameObject_2;
	Vector3_t3722313464  ___point_3;
	Vector3_t3722313464  ___normal_4;
};
#endif // SUPERCOLLISION_T1779554860_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef CURSORMODE_T4010326064_H
#define CURSORMODE_T4010326064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CursorMode
struct  CursorMode_t4010326064 
{
public:
	// System.Int32 UnityEngine.CursorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CursorMode_t4010326064, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORMODE_T4010326064_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef QUERYTRIGGERINTERACTION_T962663221_H
#define QUERYTRIGGERINTERACTION_T962663221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.QueryTriggerInteraction
struct  QueryTriggerInteraction_t962663221 
{
public:
	// System.Int32 UnityEngine.QueryTriggerInteraction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QueryTriggerInteraction_t962663221, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYTRIGGERINTERACTION_T962663221_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef CP858_T2383198942_H
#define CP858_T2383198942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP858
struct  CP858_t2383198942  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP858_t2383198942_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP858::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP858_t2383198942_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP858_T2383198942_H
#ifndef CP862_T864234704_H
#define CP862_T864234704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP862
struct  CP862_t864234704  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP862_t864234704_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP862::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP862_t864234704_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP862_T864234704_H
#ifndef CP864_T57665650_H
#define CP864_T57665650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP864
struct  CP864_t57665650  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP864_t57665650_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP864::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP864_t57665650_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP864_T57665650_H
#ifndef CP866_T3189833532_H
#define CP866_T3189833532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP866
struct  CP866_t3189833532  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP866_t3189833532_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP866::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP866_t3189833532_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP866_T3189833532_H
#ifndef CP869_T3949348419_H
#define CP869_T3949348419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP869
struct  CP869_t3949348419  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP869_t3949348419_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP869::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP869_t3949348419_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP869_T3949348419_H
#ifndef CP870_T2027099654_H
#define CP870_T2027099654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP870
struct  CP870_t2027099654  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP870_t2027099654_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP870::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP870_t2027099654_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP870_T2027099654_H
#ifndef CP875_T1623815127_H
#define CP875_T1623815127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.CP875
struct  CP875_t1623815127  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP875_t1623815127_StaticFields
{
public:
	// System.Char[] I18N.Rare.CP875::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP875_t1623815127_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP875_T1623815127_H
#ifndef CP10000_T776152390_H
#define CP10000_T776152390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP10000
struct  CP10000_t776152390  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP10000_t776152390_StaticFields
{
public:
	// System.Char[] I18N.West.CP10000::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP10000_t776152390_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP10000_T776152390_H
#ifndef CP10079_T3879478589_H
#define CP10079_T3879478589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP10079
struct  CP10079_t3879478589  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP10079_t3879478589_StaticFields
{
public:
	// System.Char[] I18N.West.CP10079::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP10079_t3879478589_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP10079_T3879478589_H
#ifndef CP1250_T1787607523_H
#define CP1250_T1787607523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP1250
struct  CP1250_t1787607523  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP1250_t1787607523_StaticFields
{
public:
	// System.Char[] I18N.West.CP1250::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP1250_t1787607523_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP1250_T1787607523_H
#ifndef CP1252_T2169944547_H
#define CP1252_T2169944547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP1252
struct  CP1252_t2169944547  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP1252_t2169944547_StaticFields
{
public:
	// System.Char[] I18N.West.CP1252::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP1252_t2169944547_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP1252_T2169944547_H
#ifndef CP1253_T4126259683_H
#define CP1253_T4126259683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP1253
struct  CP1253_t4126259683  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP1253_t4126259683_StaticFields
{
public:
	// System.Char[] I18N.West.CP1253::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP1253_t4126259683_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP1253_T4126259683_H
#ifndef CP28592_T3519602206_H
#define CP28592_T3519602206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28592
struct  CP28592_t3519602206  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP28592_t3519602206_StaticFields
{
public:
	// System.Char[] I18N.West.CP28592::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28592_t3519602206_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28592_T3519602206_H
#ifndef CP28593_T3519602207_H
#define CP28593_T3519602207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28593
struct  CP28593_t3519602207  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP28593_t3519602207_StaticFields
{
public:
	// System.Char[] I18N.West.CP28593::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28593_t3519602207_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28593_T3519602207_H
#ifndef CP28597_T3519602203_H
#define CP28597_T3519602203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28597
struct  CP28597_t3519602203  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP28597_t3519602203_StaticFields
{
public:
	// System.Char[] I18N.West.CP28597::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28597_t3519602203_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28597_T3519602203_H
#ifndef CP28605_T1600022914_H
#define CP28605_T1600022914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP28605
struct  CP28605_t1600022914  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP28605_t1600022914_StaticFields
{
public:
	// System.Char[] I18N.West.CP28605::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP28605_t1600022914_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP28605_T1600022914_H
#ifndef CP437_T3683438768_H
#define CP437_T3683438768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP437
struct  CP437_t3683438768  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP437_t3683438768_StaticFields
{
public:
	// System.Char[] I18N.West.CP437::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP437_t3683438768_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP437_T3683438768_H
#ifndef CP850_T1311178993_H
#define CP850_T1311178993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP850
struct  CP850_t1311178993  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP850_t1311178993_StaticFields
{
public:
	// System.Char[] I18N.West.CP850::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP850_t1311178993_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP850_T1311178993_H
#ifndef CP860_T1311113457_H
#define CP860_T1311113457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP860
struct  CP860_t1311113457  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP860_t1311113457_StaticFields
{
public:
	// System.Char[] I18N.West.CP860::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP860_t1311113457_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP860_T1311113457_H
#ifndef CP861_T2877197398_H
#define CP861_T2877197398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP861
struct  CP861_t2877197398  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP861_t2877197398_StaticFields
{
public:
	// System.Char[] I18N.West.CP861::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP861_t2877197398_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP861_T2877197398_H
#ifndef CP863_T1714397984_H
#define CP863_T1714397984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP863
struct  CP863_t1714397984  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP863_t1714397984_StaticFields
{
public:
	// System.Char[] I18N.West.CP863::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP863_t1714397984_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP863_T1714397984_H
#ifndef CP865_T551598570_H
#define CP865_T551598570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.CP865
struct  CP865_t551598570  : public ByteEncoding_t1371924561
{
public:

public:
};

struct CP865_t551598570_StaticFields
{
public:
	// System.Char[] I18N.West.CP865::ToChars
	CharU5BU5D_t3528271667* ___ToChars_30;

public:
	inline static int32_t get_offset_of_ToChars_30() { return static_cast<int32_t>(offsetof(CP865_t551598570_StaticFields, ___ToChars_30)); }
	inline CharU5BU5D_t3528271667* get_ToChars_30() const { return ___ToChars_30; }
	inline CharU5BU5D_t3528271667** get_address_of_ToChars_30() { return &___ToChars_30; }
	inline void set_ToChars_30(CharU5BU5D_t3528271667* value)
	{
		___ToChars_30 = value;
		Il2CppCodeGenWriteBarrier((&___ToChars_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CP865_T551598570_H
#ifndef GROUND_T4164855697_H
#define GROUND_T4164855697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperCharacterController/Ground
struct  Ground_t4164855697 
{
public:
	// UnityEngine.RaycastHit SuperCharacterController/Ground::<hit>k__BackingField
	RaycastHit_t1056001966  ___U3ChitU3Ek__BackingField_0;
	// UnityEngine.RaycastHit SuperCharacterController/Ground::<nearHit>k__BackingField
	RaycastHit_t1056001966  ___U3CnearHitU3Ek__BackingField_1;
	// UnityEngine.RaycastHit SuperCharacterController/Ground::<farHit>k__BackingField
	RaycastHit_t1056001966  ___U3CfarHitU3Ek__BackingField_2;
	// UnityEngine.RaycastHit SuperCharacterController/Ground::<secondaryHit>k__BackingField
	RaycastHit_t1056001966  ___U3CsecondaryHitU3Ek__BackingField_3;
	// SuperCollisionType SuperCharacterController/Ground::<collisionType>k__BackingField
	SuperCollisionType_t119653226 * ___U3CcollisionTypeU3Ek__BackingField_4;
	// UnityEngine.Transform SuperCharacterController/Ground::<transform>k__BackingField
	Transform_t3600365921 * ___U3CtransformU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3ChitU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Ground_t4164855697, ___U3ChitU3Ek__BackingField_0)); }
	inline RaycastHit_t1056001966  get_U3ChitU3Ek__BackingField_0() const { return ___U3ChitU3Ek__BackingField_0; }
	inline RaycastHit_t1056001966 * get_address_of_U3ChitU3Ek__BackingField_0() { return &___U3ChitU3Ek__BackingField_0; }
	inline void set_U3ChitU3Ek__BackingField_0(RaycastHit_t1056001966  value)
	{
		___U3ChitU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CnearHitU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Ground_t4164855697, ___U3CnearHitU3Ek__BackingField_1)); }
	inline RaycastHit_t1056001966  get_U3CnearHitU3Ek__BackingField_1() const { return ___U3CnearHitU3Ek__BackingField_1; }
	inline RaycastHit_t1056001966 * get_address_of_U3CnearHitU3Ek__BackingField_1() { return &___U3CnearHitU3Ek__BackingField_1; }
	inline void set_U3CnearHitU3Ek__BackingField_1(RaycastHit_t1056001966  value)
	{
		___U3CnearHitU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CfarHitU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Ground_t4164855697, ___U3CfarHitU3Ek__BackingField_2)); }
	inline RaycastHit_t1056001966  get_U3CfarHitU3Ek__BackingField_2() const { return ___U3CfarHitU3Ek__BackingField_2; }
	inline RaycastHit_t1056001966 * get_address_of_U3CfarHitU3Ek__BackingField_2() { return &___U3CfarHitU3Ek__BackingField_2; }
	inline void set_U3CfarHitU3Ek__BackingField_2(RaycastHit_t1056001966  value)
	{
		___U3CfarHitU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CsecondaryHitU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Ground_t4164855697, ___U3CsecondaryHitU3Ek__BackingField_3)); }
	inline RaycastHit_t1056001966  get_U3CsecondaryHitU3Ek__BackingField_3() const { return ___U3CsecondaryHitU3Ek__BackingField_3; }
	inline RaycastHit_t1056001966 * get_address_of_U3CsecondaryHitU3Ek__BackingField_3() { return &___U3CsecondaryHitU3Ek__BackingField_3; }
	inline void set_U3CsecondaryHitU3Ek__BackingField_3(RaycastHit_t1056001966  value)
	{
		___U3CsecondaryHitU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CcollisionTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Ground_t4164855697, ___U3CcollisionTypeU3Ek__BackingField_4)); }
	inline SuperCollisionType_t119653226 * get_U3CcollisionTypeU3Ek__BackingField_4() const { return ___U3CcollisionTypeU3Ek__BackingField_4; }
	inline SuperCollisionType_t119653226 ** get_address_of_U3CcollisionTypeU3Ek__BackingField_4() { return &___U3CcollisionTypeU3Ek__BackingField_4; }
	inline void set_U3CcollisionTypeU3Ek__BackingField_4(SuperCollisionType_t119653226 * value)
	{
		___U3CcollisionTypeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcollisionTypeU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CtransformU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Ground_t4164855697, ___U3CtransformU3Ek__BackingField_5)); }
	inline Transform_t3600365921 * get_U3CtransformU3Ek__BackingField_5() const { return ___U3CtransformU3Ek__BackingField_5; }
	inline Transform_t3600365921 ** get_address_of_U3CtransformU3Ek__BackingField_5() { return &___U3CtransformU3Ek__BackingField_5; }
	inline void set_U3CtransformU3Ek__BackingField_5(Transform_t3600365921 * value)
	{
		___U3CtransformU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransformU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SuperCharacterController/Ground
struct Ground_t4164855697_marshaled_pinvoke
{
	RaycastHit_t1056001966  ___U3ChitU3Ek__BackingField_0;
	RaycastHit_t1056001966  ___U3CnearHitU3Ek__BackingField_1;
	RaycastHit_t1056001966  ___U3CfarHitU3Ek__BackingField_2;
	RaycastHit_t1056001966  ___U3CsecondaryHitU3Ek__BackingField_3;
	SuperCollisionType_t119653226 * ___U3CcollisionTypeU3Ek__BackingField_4;
	Transform_t3600365921 * ___U3CtransformU3Ek__BackingField_5;
};
// Native definition for COM marshalling of SuperCharacterController/Ground
struct Ground_t4164855697_marshaled_com
{
	RaycastHit_t1056001966  ___U3ChitU3Ek__BackingField_0;
	RaycastHit_t1056001966  ___U3CnearHitU3Ek__BackingField_1;
	RaycastHit_t1056001966  ___U3CfarHitU3Ek__BackingField_2;
	RaycastHit_t1056001966  ___U3CsecondaryHitU3Ek__BackingField_3;
	SuperCollisionType_t119653226 * ___U3CcollisionTypeU3Ek__BackingField_4;
	Transform_t3600365921 * ___U3CtransformU3Ek__BackingField_5;
};
#endif // GROUND_T4164855697_H
#ifndef SUPERGROUND_T3112734080_H
#define SUPERGROUND_T3112734080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperCharacterController/SuperGround
struct  SuperGround_t3112734080  : public RuntimeObject
{
public:
	// UnityEngine.LayerMask SuperCharacterController/SuperGround::walkable
	LayerMask_t3493934918  ___walkable_0;
	// SuperCharacterController SuperCharacterController/SuperGround::controller
	SuperCharacterController_t3568978942 * ___controller_1;
	// UnityEngine.QueryTriggerInteraction SuperCharacterController/SuperGround::triggerInteraction
	int32_t ___triggerInteraction_2;
	// SuperCharacterController/SuperGround/GroundHit SuperCharacterController/SuperGround::primaryGround
	GroundHit_t3864137795 * ___primaryGround_3;
	// SuperCharacterController/SuperGround/GroundHit SuperCharacterController/SuperGround::nearGround
	GroundHit_t3864137795 * ___nearGround_4;
	// SuperCharacterController/SuperGround/GroundHit SuperCharacterController/SuperGround::farGround
	GroundHit_t3864137795 * ___farGround_5;
	// SuperCharacterController/SuperGround/GroundHit SuperCharacterController/SuperGround::stepGround
	GroundHit_t3864137795 * ___stepGround_6;
	// SuperCharacterController/SuperGround/GroundHit SuperCharacterController/SuperGround::flushGround
	GroundHit_t3864137795 * ___flushGround_7;
	// SuperCollisionType SuperCharacterController/SuperGround::<superCollisionType>k__BackingField
	SuperCollisionType_t119653226 * ___U3CsuperCollisionTypeU3Ek__BackingField_8;
	// UnityEngine.Transform SuperCharacterController/SuperGround::<transform>k__BackingField
	Transform_t3600365921 * ___U3CtransformU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_walkable_0() { return static_cast<int32_t>(offsetof(SuperGround_t3112734080, ___walkable_0)); }
	inline LayerMask_t3493934918  get_walkable_0() const { return ___walkable_0; }
	inline LayerMask_t3493934918 * get_address_of_walkable_0() { return &___walkable_0; }
	inline void set_walkable_0(LayerMask_t3493934918  value)
	{
		___walkable_0 = value;
	}

	inline static int32_t get_offset_of_controller_1() { return static_cast<int32_t>(offsetof(SuperGround_t3112734080, ___controller_1)); }
	inline SuperCharacterController_t3568978942 * get_controller_1() const { return ___controller_1; }
	inline SuperCharacterController_t3568978942 ** get_address_of_controller_1() { return &___controller_1; }
	inline void set_controller_1(SuperCharacterController_t3568978942 * value)
	{
		___controller_1 = value;
		Il2CppCodeGenWriteBarrier((&___controller_1), value);
	}

	inline static int32_t get_offset_of_triggerInteraction_2() { return static_cast<int32_t>(offsetof(SuperGround_t3112734080, ___triggerInteraction_2)); }
	inline int32_t get_triggerInteraction_2() const { return ___triggerInteraction_2; }
	inline int32_t* get_address_of_triggerInteraction_2() { return &___triggerInteraction_2; }
	inline void set_triggerInteraction_2(int32_t value)
	{
		___triggerInteraction_2 = value;
	}

	inline static int32_t get_offset_of_primaryGround_3() { return static_cast<int32_t>(offsetof(SuperGround_t3112734080, ___primaryGround_3)); }
	inline GroundHit_t3864137795 * get_primaryGround_3() const { return ___primaryGround_3; }
	inline GroundHit_t3864137795 ** get_address_of_primaryGround_3() { return &___primaryGround_3; }
	inline void set_primaryGround_3(GroundHit_t3864137795 * value)
	{
		___primaryGround_3 = value;
		Il2CppCodeGenWriteBarrier((&___primaryGround_3), value);
	}

	inline static int32_t get_offset_of_nearGround_4() { return static_cast<int32_t>(offsetof(SuperGround_t3112734080, ___nearGround_4)); }
	inline GroundHit_t3864137795 * get_nearGround_4() const { return ___nearGround_4; }
	inline GroundHit_t3864137795 ** get_address_of_nearGround_4() { return &___nearGround_4; }
	inline void set_nearGround_4(GroundHit_t3864137795 * value)
	{
		___nearGround_4 = value;
		Il2CppCodeGenWriteBarrier((&___nearGround_4), value);
	}

	inline static int32_t get_offset_of_farGround_5() { return static_cast<int32_t>(offsetof(SuperGround_t3112734080, ___farGround_5)); }
	inline GroundHit_t3864137795 * get_farGround_5() const { return ___farGround_5; }
	inline GroundHit_t3864137795 ** get_address_of_farGround_5() { return &___farGround_5; }
	inline void set_farGround_5(GroundHit_t3864137795 * value)
	{
		___farGround_5 = value;
		Il2CppCodeGenWriteBarrier((&___farGround_5), value);
	}

	inline static int32_t get_offset_of_stepGround_6() { return static_cast<int32_t>(offsetof(SuperGround_t3112734080, ___stepGround_6)); }
	inline GroundHit_t3864137795 * get_stepGround_6() const { return ___stepGround_6; }
	inline GroundHit_t3864137795 ** get_address_of_stepGround_6() { return &___stepGround_6; }
	inline void set_stepGround_6(GroundHit_t3864137795 * value)
	{
		___stepGround_6 = value;
		Il2CppCodeGenWriteBarrier((&___stepGround_6), value);
	}

	inline static int32_t get_offset_of_flushGround_7() { return static_cast<int32_t>(offsetof(SuperGround_t3112734080, ___flushGround_7)); }
	inline GroundHit_t3864137795 * get_flushGround_7() const { return ___flushGround_7; }
	inline GroundHit_t3864137795 ** get_address_of_flushGround_7() { return &___flushGround_7; }
	inline void set_flushGround_7(GroundHit_t3864137795 * value)
	{
		___flushGround_7 = value;
		Il2CppCodeGenWriteBarrier((&___flushGround_7), value);
	}

	inline static int32_t get_offset_of_U3CsuperCollisionTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SuperGround_t3112734080, ___U3CsuperCollisionTypeU3Ek__BackingField_8)); }
	inline SuperCollisionType_t119653226 * get_U3CsuperCollisionTypeU3Ek__BackingField_8() const { return ___U3CsuperCollisionTypeU3Ek__BackingField_8; }
	inline SuperCollisionType_t119653226 ** get_address_of_U3CsuperCollisionTypeU3Ek__BackingField_8() { return &___U3CsuperCollisionTypeU3Ek__BackingField_8; }
	inline void set_U3CsuperCollisionTypeU3Ek__BackingField_8(SuperCollisionType_t119653226 * value)
	{
		___U3CsuperCollisionTypeU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsuperCollisionTypeU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CtransformU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SuperGround_t3112734080, ___U3CtransformU3Ek__BackingField_9)); }
	inline Transform_t3600365921 * get_U3CtransformU3Ek__BackingField_9() const { return ___U3CtransformU3Ek__BackingField_9; }
	inline Transform_t3600365921 ** get_address_of_U3CtransformU3Ek__BackingField_9() { return &___U3CtransformU3Ek__BackingField_9; }
	inline void set_U3CtransformU3Ek__BackingField_9(Transform_t3600365921 * value)
	{
		___U3CtransformU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransformU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPERGROUND_T3112734080_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef ENCIBM00858_T2458074995_H
#define ENCIBM00858_T2458074995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm00858
struct  ENCibm00858_t2458074995  : public CP858_t2383198942
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM00858_T2458074995_H
#ifndef ENCIBM862_T2537577824_H
#define ENCIBM862_T2537577824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm862
struct  ENCibm862_t2537577824  : public CP862_t864234704
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM862_T2537577824_H
#ifndef ENCIBM864_T3344146878_H
#define ENCIBM864_T3344146878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm864
struct  ENCibm864_t3344146878  : public CP864_t57665650
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM864_T3344146878_H
#ifndef ENCIBM866_T211978996_H
#define ENCIBM866_T211978996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm866
struct  ENCibm866_t211978996  : public CP866_t3189833532
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM866_T211978996_H
#ifndef ENCIBM869_T3747431405_H
#define ENCIBM869_T3747431405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm869
struct  ENCibm869_t3747431405  : public CP869_t3949348419
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM869_T3747431405_H
#ifndef ENCIBM870_T1374843946_H
#define ENCIBM870_T1374843946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm870
struct  ENCibm870_t1374843946  : public CP870_t2027099654
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM870_T1374843946_H
#ifndef ENCIBM875_T1778128473_H
#define ENCIBM875_T1778128473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Rare.ENCibm875
struct  ENCibm875_t1778128473  : public CP875_t1623815127
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM875_T1778128473_H
#ifndef ENCIBM437_T3410411110_H
#define ENCIBM437_T3410411110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm437
struct  ENCibm437_t3410411110  : public CP437_t3683438768
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM437_T3410411110_H
#ifndef ENCIBM850_T3813826705_H
#define ENCIBM850_T3813826705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm850
struct  ENCibm850_t3813826705  : public CP850_t1311178993
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM850_T3813826705_H
#ifndef ENCIBM860_T3814023313_H
#define ENCIBM860_T3814023313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm860
struct  ENCibm860_t3814023313  : public CP860_t1311113457
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM860_T3814023313_H
#ifndef ENCIBM861_T2247939372_H
#define ENCIBM861_T2247939372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm861
struct  ENCibm861_t2247939372  : public CP861_t2877197398
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM861_T2247939372_H
#ifndef ENCIBM863_T1085139958_H
#define ENCIBM863_T1085139958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm863
struct  ENCibm863_t1085139958  : public CP863_t1714397984
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM863_T1085139958_H
#ifndef ENCIBM865_T278570904_H
#define ENCIBM865_T278570904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCibm865
struct  ENCibm865_t278570904  : public CP865_t551598570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCIBM865_T278570904_H
#ifndef ENCISO_8859_15_T3881981915_H
#define ENCISO_8859_15_T3881981915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_15
struct  ENCiso_8859_15_t3881981915  : public CP28605_t1600022914
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_15_T3881981915_H
#ifndef ENCISO_8859_2_T815391552_H
#define ENCISO_8859_2_T815391552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_2
struct  ENCiso_8859_2_t815391552  : public CP28592_t3519602206
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_2_T815391552_H
#ifndef ENCISO_8859_3_T2381475493_H
#define ENCISO_8859_3_T2381475493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_3
struct  ENCiso_8859_3_t2381475493  : public CP28593_t3519602207
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_3_T2381475493_H
#ifndef ENCISO_8859_7_T412107025_H
#define ENCISO_8859_7_T412107025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCiso_8859_7
struct  ENCiso_8859_7_t412107025  : public CP28597_t3519602203
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCISO_8859_7_T412107025_H
#ifndef ENCMACINTOSH_T1087697298_H
#define ENCMACINTOSH_T1087697298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCmacintosh
struct  ENCmacintosh_t1087697298  : public CP10000_t776152390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCMACINTOSH_T1087697298_H
#ifndef ENCWINDOWS_1250_T3241285640_H
#define ENCWINDOWS_1250_T3241285640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCwindows_1250
struct  ENCwindows_1250_t3241285640  : public CP1250_t1787607523
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCWINDOWS_1250_T3241285640_H
#ifndef ENCWINDOWS_1252_T3241285642_H
#define ENCWINDOWS_1252_T3241285642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCwindows_1252
struct  ENCwindows_1252_t3241285642  : public CP1252_t2169944547
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCWINDOWS_1252_T3241285642_H
#ifndef ENCWINDOWS_1253_T3241285643_H
#define ENCWINDOWS_1253_T3241285643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCwindows_1253
struct  ENCwindows_1253_t3241285643  : public CP1253_t4126259683
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCWINDOWS_1253_T3241285643_H
#ifndef ENCX_MAC_ICELANDIC_T2411177152_H
#define ENCX_MAC_ICELANDIC_T2411177152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.West.ENCx_mac_icelandic
struct  ENCx_mac_icelandic_t2411177152  : public CP10079_t3879478589
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCX_MAC_ICELANDIC_T2411177152_H
#ifndef UPDATEDELEGATE_T1636609007_H
#define UPDATEDELEGATE_T1636609007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperCharacterController/UpdateDelegate
struct  UpdateDelegate_t1636609007  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDELEGATE_T1636609007_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef STATEMACHINEBEHAVIOUR_T957311111_H
#define STATEMACHINEBEHAVIOUR_T957311111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StateMachineBehaviour
struct  StateMachineBehaviour_t957311111  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMACHINEBEHAVIOUR_T957311111_H
#ifndef BASICHITCHECK_T2528769335_H
#define BASICHITCHECK_T2528769335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicHitCheck
struct  BasicHitCheck_t2528769335  : public StateMachineBehaviour_t957311111
{
public:
	// System.Single BasicHitCheck::colliderTestTime
	float ___colliderTestTime_4;
	// System.Boolean BasicHitCheck::collChecked
	bool ___collChecked_5;
	// ASCLBasicController BasicHitCheck::abilityController
	ASCLBasicController_t2851843984 * ___abilityController_6;

public:
	inline static int32_t get_offset_of_colliderTestTime_4() { return static_cast<int32_t>(offsetof(BasicHitCheck_t2528769335, ___colliderTestTime_4)); }
	inline float get_colliderTestTime_4() const { return ___colliderTestTime_4; }
	inline float* get_address_of_colliderTestTime_4() { return &___colliderTestTime_4; }
	inline void set_colliderTestTime_4(float value)
	{
		___colliderTestTime_4 = value;
	}

	inline static int32_t get_offset_of_collChecked_5() { return static_cast<int32_t>(offsetof(BasicHitCheck_t2528769335, ___collChecked_5)); }
	inline bool get_collChecked_5() const { return ___collChecked_5; }
	inline bool* get_address_of_collChecked_5() { return &___collChecked_5; }
	inline void set_collChecked_5(bool value)
	{
		___collChecked_5 = value;
	}

	inline static int32_t get_offset_of_abilityController_6() { return static_cast<int32_t>(offsetof(BasicHitCheck_t2528769335, ___abilityController_6)); }
	inline ASCLBasicController_t2851843984 * get_abilityController_6() const { return ___abilityController_6; }
	inline ASCLBasicController_t2851843984 ** get_address_of_abilityController_6() { return &___abilityController_6; }
	inline void set_abilityController_6(ASCLBasicController_t2851843984 * value)
	{
		___abilityController_6 = value;
		Il2CppCodeGenWriteBarrier((&___abilityController_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICHITCHECK_T2528769335_H
#ifndef ROOTMOTIONOFF_T4254465151_H
#define ROOTMOTIONOFF_T4254465151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RootMotionOff
struct  RootMotionOff_t4254465151  : public StateMachineBehaviour_t957311111
{
public:
	// System.Single RootMotionOff::ColliderTestTime
	float ___ColliderTestTime_4;

public:
	inline static int32_t get_offset_of_ColliderTestTime_4() { return static_cast<int32_t>(offsetof(RootMotionOff_t4254465151, ___ColliderTestTime_4)); }
	inline float get_ColliderTestTime_4() const { return ___ColliderTestTime_4; }
	inline float* get_address_of_ColliderTestTime_4() { return &___ColliderTestTime_4; }
	inline void set_ColliderTestTime_4(float value)
	{
		___ColliderTestTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOTMOTIONOFF_T4254465151_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ASCLBASICCONTROLLER_T2851843984_H
#define ASCLBASICCONTROLLER_T2851843984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ASCLBasicController
struct  ASCLBasicController_t2851843984  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator ASCLBasicController::animator
	Animator_t434523843 * ___animator_4;
	// UnityEngine.Collider ASCLBasicController::floorPlane
	Collider_t1773347010 * ___floorPlane_5;
	// UnityEngine.Collider ASCLBasicController::attackPlane
	Collider_t1773347010 * ___attackPlane_6;
	// Enemy[] ASCLBasicController::enemies
	EnemyU5BU5D_t2006687320* ___enemies_7;
	// UnityEngine.Transform ASCLBasicController::hitReport
	Transform_t3600365921 * ___hitReport_8;
	// UnityEngine.Transform ASCLBasicController::particleHit
	Transform_t3600365921 * ___particleHit_9;
	// System.Collections.Generic.List`1<Ability> ASCLBasicController::abilities
	List_1_t1638366214 * ___abilities_10;
	// System.Int32 ASCLBasicController::ahc
	int32_t ___ahc_11;
	// System.Boolean ASCLBasicController::hitCheck
	bool ___hitCheck_12;
	// System.Int32 ASCLBasicController::WeaponState
	int32_t ___WeaponState_13;
	// System.Boolean ASCLBasicController::wasAttacking
	bool ___wasAttacking_14;
	// UnityEngine.Renderer ASCLBasicController::movementTarget
	Renderer_t2627027031 * ___movementTarget_15;
	// UnityEngine.Transform ASCLBasicController::destFloor
	Transform_t3600365921 * ___destFloor_16;
	// System.Single ASCLBasicController::rotateSpeed
	float ___rotateSpeed_17;
	// UnityEngine.Vector3 ASCLBasicController::attackPos
	Vector3_t3722313464  ___attackPos_18;
	// UnityEngine.Vector3 ASCLBasicController::lookAtPos
	Vector3_t3722313464  ___lookAtPos_19;
	// System.Single ASCLBasicController::gravity
	float ___gravity_20;
	// System.Single ASCLBasicController::fallspeed
	float ___fallspeed_21;
	// System.Boolean ASCLBasicController::rightButtonDown
	bool ___rightButtonDown_22;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___animator_4)); }
	inline Animator_t434523843 * get_animator_4() const { return ___animator_4; }
	inline Animator_t434523843 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t434523843 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}

	inline static int32_t get_offset_of_floorPlane_5() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___floorPlane_5)); }
	inline Collider_t1773347010 * get_floorPlane_5() const { return ___floorPlane_5; }
	inline Collider_t1773347010 ** get_address_of_floorPlane_5() { return &___floorPlane_5; }
	inline void set_floorPlane_5(Collider_t1773347010 * value)
	{
		___floorPlane_5 = value;
		Il2CppCodeGenWriteBarrier((&___floorPlane_5), value);
	}

	inline static int32_t get_offset_of_attackPlane_6() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___attackPlane_6)); }
	inline Collider_t1773347010 * get_attackPlane_6() const { return ___attackPlane_6; }
	inline Collider_t1773347010 ** get_address_of_attackPlane_6() { return &___attackPlane_6; }
	inline void set_attackPlane_6(Collider_t1773347010 * value)
	{
		___attackPlane_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackPlane_6), value);
	}

	inline static int32_t get_offset_of_enemies_7() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___enemies_7)); }
	inline EnemyU5BU5D_t2006687320* get_enemies_7() const { return ___enemies_7; }
	inline EnemyU5BU5D_t2006687320** get_address_of_enemies_7() { return &___enemies_7; }
	inline void set_enemies_7(EnemyU5BU5D_t2006687320* value)
	{
		___enemies_7 = value;
		Il2CppCodeGenWriteBarrier((&___enemies_7), value);
	}

	inline static int32_t get_offset_of_hitReport_8() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___hitReport_8)); }
	inline Transform_t3600365921 * get_hitReport_8() const { return ___hitReport_8; }
	inline Transform_t3600365921 ** get_address_of_hitReport_8() { return &___hitReport_8; }
	inline void set_hitReport_8(Transform_t3600365921 * value)
	{
		___hitReport_8 = value;
		Il2CppCodeGenWriteBarrier((&___hitReport_8), value);
	}

	inline static int32_t get_offset_of_particleHit_9() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___particleHit_9)); }
	inline Transform_t3600365921 * get_particleHit_9() const { return ___particleHit_9; }
	inline Transform_t3600365921 ** get_address_of_particleHit_9() { return &___particleHit_9; }
	inline void set_particleHit_9(Transform_t3600365921 * value)
	{
		___particleHit_9 = value;
		Il2CppCodeGenWriteBarrier((&___particleHit_9), value);
	}

	inline static int32_t get_offset_of_abilities_10() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___abilities_10)); }
	inline List_1_t1638366214 * get_abilities_10() const { return ___abilities_10; }
	inline List_1_t1638366214 ** get_address_of_abilities_10() { return &___abilities_10; }
	inline void set_abilities_10(List_1_t1638366214 * value)
	{
		___abilities_10 = value;
		Il2CppCodeGenWriteBarrier((&___abilities_10), value);
	}

	inline static int32_t get_offset_of_ahc_11() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___ahc_11)); }
	inline int32_t get_ahc_11() const { return ___ahc_11; }
	inline int32_t* get_address_of_ahc_11() { return &___ahc_11; }
	inline void set_ahc_11(int32_t value)
	{
		___ahc_11 = value;
	}

	inline static int32_t get_offset_of_hitCheck_12() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___hitCheck_12)); }
	inline bool get_hitCheck_12() const { return ___hitCheck_12; }
	inline bool* get_address_of_hitCheck_12() { return &___hitCheck_12; }
	inline void set_hitCheck_12(bool value)
	{
		___hitCheck_12 = value;
	}

	inline static int32_t get_offset_of_WeaponState_13() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___WeaponState_13)); }
	inline int32_t get_WeaponState_13() const { return ___WeaponState_13; }
	inline int32_t* get_address_of_WeaponState_13() { return &___WeaponState_13; }
	inline void set_WeaponState_13(int32_t value)
	{
		___WeaponState_13 = value;
	}

	inline static int32_t get_offset_of_wasAttacking_14() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___wasAttacking_14)); }
	inline bool get_wasAttacking_14() const { return ___wasAttacking_14; }
	inline bool* get_address_of_wasAttacking_14() { return &___wasAttacking_14; }
	inline void set_wasAttacking_14(bool value)
	{
		___wasAttacking_14 = value;
	}

	inline static int32_t get_offset_of_movementTarget_15() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___movementTarget_15)); }
	inline Renderer_t2627027031 * get_movementTarget_15() const { return ___movementTarget_15; }
	inline Renderer_t2627027031 ** get_address_of_movementTarget_15() { return &___movementTarget_15; }
	inline void set_movementTarget_15(Renderer_t2627027031 * value)
	{
		___movementTarget_15 = value;
		Il2CppCodeGenWriteBarrier((&___movementTarget_15), value);
	}

	inline static int32_t get_offset_of_destFloor_16() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___destFloor_16)); }
	inline Transform_t3600365921 * get_destFloor_16() const { return ___destFloor_16; }
	inline Transform_t3600365921 ** get_address_of_destFloor_16() { return &___destFloor_16; }
	inline void set_destFloor_16(Transform_t3600365921 * value)
	{
		___destFloor_16 = value;
		Il2CppCodeGenWriteBarrier((&___destFloor_16), value);
	}

	inline static int32_t get_offset_of_rotateSpeed_17() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___rotateSpeed_17)); }
	inline float get_rotateSpeed_17() const { return ___rotateSpeed_17; }
	inline float* get_address_of_rotateSpeed_17() { return &___rotateSpeed_17; }
	inline void set_rotateSpeed_17(float value)
	{
		___rotateSpeed_17 = value;
	}

	inline static int32_t get_offset_of_attackPos_18() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___attackPos_18)); }
	inline Vector3_t3722313464  get_attackPos_18() const { return ___attackPos_18; }
	inline Vector3_t3722313464 * get_address_of_attackPos_18() { return &___attackPos_18; }
	inline void set_attackPos_18(Vector3_t3722313464  value)
	{
		___attackPos_18 = value;
	}

	inline static int32_t get_offset_of_lookAtPos_19() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___lookAtPos_19)); }
	inline Vector3_t3722313464  get_lookAtPos_19() const { return ___lookAtPos_19; }
	inline Vector3_t3722313464 * get_address_of_lookAtPos_19() { return &___lookAtPos_19; }
	inline void set_lookAtPos_19(Vector3_t3722313464  value)
	{
		___lookAtPos_19 = value;
	}

	inline static int32_t get_offset_of_gravity_20() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___gravity_20)); }
	inline float get_gravity_20() const { return ___gravity_20; }
	inline float* get_address_of_gravity_20() { return &___gravity_20; }
	inline void set_gravity_20(float value)
	{
		___gravity_20 = value;
	}

	inline static int32_t get_offset_of_fallspeed_21() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___fallspeed_21)); }
	inline float get_fallspeed_21() const { return ___fallspeed_21; }
	inline float* get_address_of_fallspeed_21() { return &___fallspeed_21; }
	inline void set_fallspeed_21(float value)
	{
		___fallspeed_21 = value;
	}

	inline static int32_t get_offset_of_rightButtonDown_22() { return static_cast<int32_t>(offsetof(ASCLBasicController_t2851843984, ___rightButtonDown_22)); }
	inline bool get_rightButtonDown_22() const { return ___rightButtonDown_22; }
	inline bool* get_address_of_rightButtonDown_22() { return &___rightButtonDown_22; }
	inline void set_rightButtonDown_22(bool value)
	{
		___rightButtonDown_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASCLBASICCONTROLLER_T2851843984_H
#ifndef BSPTREE_T2380928923_H
#define BSPTREE_T2380928923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BSPTree
struct  BSPTree_t2380928923  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean BSPTree::drawMeshTreeOnStart
	bool ___drawMeshTreeOnStart_4;
	// System.Int32 BSPTree::triangleCount
	int32_t ___triangleCount_5;
	// System.Int32 BSPTree::vertexCount
	int32_t ___vertexCount_6;
	// UnityEngine.Vector3[] BSPTree::vertices
	Vector3U5BU5D_t1718750761* ___vertices_7;
	// System.Int32[] BSPTree::tris
	Int32U5BU5D_t385246372* ___tris_8;
	// UnityEngine.Vector3[] BSPTree::triangleNormals
	Vector3U5BU5D_t1718750761* ___triangleNormals_9;
	// UnityEngine.Mesh BSPTree::mesh
	Mesh_t3648964284 * ___mesh_10;
	// BSPTree/Node BSPTree::tree
	Node_t1584791709 * ___tree_11;

public:
	inline static int32_t get_offset_of_drawMeshTreeOnStart_4() { return static_cast<int32_t>(offsetof(BSPTree_t2380928923, ___drawMeshTreeOnStart_4)); }
	inline bool get_drawMeshTreeOnStart_4() const { return ___drawMeshTreeOnStart_4; }
	inline bool* get_address_of_drawMeshTreeOnStart_4() { return &___drawMeshTreeOnStart_4; }
	inline void set_drawMeshTreeOnStart_4(bool value)
	{
		___drawMeshTreeOnStart_4 = value;
	}

	inline static int32_t get_offset_of_triangleCount_5() { return static_cast<int32_t>(offsetof(BSPTree_t2380928923, ___triangleCount_5)); }
	inline int32_t get_triangleCount_5() const { return ___triangleCount_5; }
	inline int32_t* get_address_of_triangleCount_5() { return &___triangleCount_5; }
	inline void set_triangleCount_5(int32_t value)
	{
		___triangleCount_5 = value;
	}

	inline static int32_t get_offset_of_vertexCount_6() { return static_cast<int32_t>(offsetof(BSPTree_t2380928923, ___vertexCount_6)); }
	inline int32_t get_vertexCount_6() const { return ___vertexCount_6; }
	inline int32_t* get_address_of_vertexCount_6() { return &___vertexCount_6; }
	inline void set_vertexCount_6(int32_t value)
	{
		___vertexCount_6 = value;
	}

	inline static int32_t get_offset_of_vertices_7() { return static_cast<int32_t>(offsetof(BSPTree_t2380928923, ___vertices_7)); }
	inline Vector3U5BU5D_t1718750761* get_vertices_7() const { return ___vertices_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vertices_7() { return &___vertices_7; }
	inline void set_vertices_7(Vector3U5BU5D_t1718750761* value)
	{
		___vertices_7 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_7), value);
	}

	inline static int32_t get_offset_of_tris_8() { return static_cast<int32_t>(offsetof(BSPTree_t2380928923, ___tris_8)); }
	inline Int32U5BU5D_t385246372* get_tris_8() const { return ___tris_8; }
	inline Int32U5BU5D_t385246372** get_address_of_tris_8() { return &___tris_8; }
	inline void set_tris_8(Int32U5BU5D_t385246372* value)
	{
		___tris_8 = value;
		Il2CppCodeGenWriteBarrier((&___tris_8), value);
	}

	inline static int32_t get_offset_of_triangleNormals_9() { return static_cast<int32_t>(offsetof(BSPTree_t2380928923, ___triangleNormals_9)); }
	inline Vector3U5BU5D_t1718750761* get_triangleNormals_9() const { return ___triangleNormals_9; }
	inline Vector3U5BU5D_t1718750761** get_address_of_triangleNormals_9() { return &___triangleNormals_9; }
	inline void set_triangleNormals_9(Vector3U5BU5D_t1718750761* value)
	{
		___triangleNormals_9 = value;
		Il2CppCodeGenWriteBarrier((&___triangleNormals_9), value);
	}

	inline static int32_t get_offset_of_mesh_10() { return static_cast<int32_t>(offsetof(BSPTree_t2380928923, ___mesh_10)); }
	inline Mesh_t3648964284 * get_mesh_10() const { return ___mesh_10; }
	inline Mesh_t3648964284 ** get_address_of_mesh_10() { return &___mesh_10; }
	inline void set_mesh_10(Mesh_t3648964284 * value)
	{
		___mesh_10 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_10), value);
	}

	inline static int32_t get_offset_of_tree_11() { return static_cast<int32_t>(offsetof(BSPTree_t2380928923, ___tree_11)); }
	inline Node_t1584791709 * get_tree_11() const { return ___tree_11; }
	inline Node_t1584791709 ** get_address_of_tree_11() { return &___tree_11; }
	inline void set_tree_11(Node_t1584791709 * value)
	{
		___tree_11 = value;
		Il2CppCodeGenWriteBarrier((&___tree_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSPTREE_T2380928923_H
#ifndef BRUTEFORCEMESH_T342700245_H
#define BRUTEFORCEMESH_T342700245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BruteForceMesh
struct  BruteForceMesh_t342700245  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 BruteForceMesh::triangleCount
	int32_t ___triangleCount_4;
	// UnityEngine.Vector3[] BruteForceMesh::vertices
	Vector3U5BU5D_t1718750761* ___vertices_5;
	// System.Int32[] BruteForceMesh::tris
	Int32U5BU5D_t385246372* ___tris_6;
	// UnityEngine.Vector3[] BruteForceMesh::triangleNormals
	Vector3U5BU5D_t1718750761* ___triangleNormals_7;
	// UnityEngine.Mesh BruteForceMesh::mesh
	Mesh_t3648964284 * ___mesh_8;

public:
	inline static int32_t get_offset_of_triangleCount_4() { return static_cast<int32_t>(offsetof(BruteForceMesh_t342700245, ___triangleCount_4)); }
	inline int32_t get_triangleCount_4() const { return ___triangleCount_4; }
	inline int32_t* get_address_of_triangleCount_4() { return &___triangleCount_4; }
	inline void set_triangleCount_4(int32_t value)
	{
		___triangleCount_4 = value;
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(BruteForceMesh_t342700245, ___vertices_5)); }
	inline Vector3U5BU5D_t1718750761* get_vertices_5() const { return ___vertices_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(Vector3U5BU5D_t1718750761* value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_5), value);
	}

	inline static int32_t get_offset_of_tris_6() { return static_cast<int32_t>(offsetof(BruteForceMesh_t342700245, ___tris_6)); }
	inline Int32U5BU5D_t385246372* get_tris_6() const { return ___tris_6; }
	inline Int32U5BU5D_t385246372** get_address_of_tris_6() { return &___tris_6; }
	inline void set_tris_6(Int32U5BU5D_t385246372* value)
	{
		___tris_6 = value;
		Il2CppCodeGenWriteBarrier((&___tris_6), value);
	}

	inline static int32_t get_offset_of_triangleNormals_7() { return static_cast<int32_t>(offsetof(BruteForceMesh_t342700245, ___triangleNormals_7)); }
	inline Vector3U5BU5D_t1718750761* get_triangleNormals_7() const { return ___triangleNormals_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_triangleNormals_7() { return &___triangleNormals_7; }
	inline void set_triangleNormals_7(Vector3U5BU5D_t1718750761* value)
	{
		___triangleNormals_7 = value;
		Il2CppCodeGenWriteBarrier((&___triangleNormals_7), value);
	}

	inline static int32_t get_offset_of_mesh_8() { return static_cast<int32_t>(offsetof(BruteForceMesh_t342700245, ___mesh_8)); }
	inline Mesh_t3648964284 * get_mesh_8() const { return ___mesh_8; }
	inline Mesh_t3648964284 ** get_address_of_mesh_8() { return &___mesh_8; }
	inline void set_mesh_8(Mesh_t3648964284 * value)
	{
		___mesh_8 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRUTEFORCEMESH_T342700245_H
#ifndef CAMCTRLDEMO_T3606798705_H
#define CAMCTRLDEMO_T3606798705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CamCTRLDemo
struct  CamCTRLDemo_t3606798705  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CamCTRLDemo::maxPosZf
	float ___maxPosZf_4;
	// System.Single CamCTRLDemo::minPosZf
	float ___minPosZf_5;
	// UnityEngine.Camera CamCTRLDemo::thisCam
	Camera_t4157153871 * ___thisCam_6;
	// System.Single CamCTRLDemo::CamLRSpeed
	float ___CamLRSpeed_7;

public:
	inline static int32_t get_offset_of_maxPosZf_4() { return static_cast<int32_t>(offsetof(CamCTRLDemo_t3606798705, ___maxPosZf_4)); }
	inline float get_maxPosZf_4() const { return ___maxPosZf_4; }
	inline float* get_address_of_maxPosZf_4() { return &___maxPosZf_4; }
	inline void set_maxPosZf_4(float value)
	{
		___maxPosZf_4 = value;
	}

	inline static int32_t get_offset_of_minPosZf_5() { return static_cast<int32_t>(offsetof(CamCTRLDemo_t3606798705, ___minPosZf_5)); }
	inline float get_minPosZf_5() const { return ___minPosZf_5; }
	inline float* get_address_of_minPosZf_5() { return &___minPosZf_5; }
	inline void set_minPosZf_5(float value)
	{
		___minPosZf_5 = value;
	}

	inline static int32_t get_offset_of_thisCam_6() { return static_cast<int32_t>(offsetof(CamCTRLDemo_t3606798705, ___thisCam_6)); }
	inline Camera_t4157153871 * get_thisCam_6() const { return ___thisCam_6; }
	inline Camera_t4157153871 ** get_address_of_thisCam_6() { return &___thisCam_6; }
	inline void set_thisCam_6(Camera_t4157153871 * value)
	{
		___thisCam_6 = value;
		Il2CppCodeGenWriteBarrier((&___thisCam_6), value);
	}

	inline static int32_t get_offset_of_CamLRSpeed_7() { return static_cast<int32_t>(offsetof(CamCTRLDemo_t3606798705, ___CamLRSpeed_7)); }
	inline float get_CamLRSpeed_7() const { return ___CamLRSpeed_7; }
	inline float* get_address_of_CamLRSpeed_7() { return &___CamLRSpeed_7; }
	inline void set_CamLRSpeed_7(float value)
	{
		___CamLRSpeed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMCTRLDEMO_T3606798705_H
#ifndef CAMTARGET_T1790837193_H
#define CAMTARGET_T1790837193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CamTarget
struct  CamTarget_t1790837193  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CamTarget::target
	Transform_t3600365921 * ___target_4;
	// System.Single CamTarget::camSpeed
	float ___camSpeed_5;
	// UnityEngine.Vector3 CamTarget::lerpPos
	Vector3_t3722313464  ___lerpPos_6;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(CamTarget_t1790837193, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_camSpeed_5() { return static_cast<int32_t>(offsetof(CamTarget_t1790837193, ___camSpeed_5)); }
	inline float get_camSpeed_5() const { return ___camSpeed_5; }
	inline float* get_address_of_camSpeed_5() { return &___camSpeed_5; }
	inline void set_camSpeed_5(float value)
	{
		___camSpeed_5 = value;
	}

	inline static int32_t get_offset_of_lerpPos_6() { return static_cast<int32_t>(offsetof(CamTarget_t1790837193, ___lerpPos_6)); }
	inline Vector3_t3722313464  get_lerpPos_6() const { return ___lerpPos_6; }
	inline Vector3_t3722313464 * get_address_of_lerpPos_6() { return &___lerpPos_6; }
	inline void set_lerpPos_6(Vector3_t3722313464  value)
	{
		___lerpPos_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMTARGET_T1790837193_H
#ifndef CAMERACONTROLLER_T3346819214_H
#define CAMERACONTROLLER_T3346819214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraController
struct  CameraController_t3346819214  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CameraController::cameraTarget
	GameObject_t1113636619 * ___cameraTarget_4;
	// System.Single CameraController::rotateSpeed
	float ___rotateSpeed_5;
	// System.Single CameraController::rotate
	float ___rotate_6;
	// System.Single CameraController::offsetDistance
	float ___offsetDistance_7;
	// System.Single CameraController::offsetHeight
	float ___offsetHeight_8;
	// System.Single CameraController::smoothing
	float ___smoothing_9;
	// UnityEngine.Vector3 CameraController::offset
	Vector3_t3722313464  ___offset_10;
	// System.Boolean CameraController::following
	bool ___following_11;
	// UnityEngine.Vector3 CameraController::lastPosition
	Vector3_t3722313464  ___lastPosition_12;

public:
	inline static int32_t get_offset_of_cameraTarget_4() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___cameraTarget_4)); }
	inline GameObject_t1113636619 * get_cameraTarget_4() const { return ___cameraTarget_4; }
	inline GameObject_t1113636619 ** get_address_of_cameraTarget_4() { return &___cameraTarget_4; }
	inline void set_cameraTarget_4(GameObject_t1113636619 * value)
	{
		___cameraTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTarget_4), value);
	}

	inline static int32_t get_offset_of_rotateSpeed_5() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___rotateSpeed_5)); }
	inline float get_rotateSpeed_5() const { return ___rotateSpeed_5; }
	inline float* get_address_of_rotateSpeed_5() { return &___rotateSpeed_5; }
	inline void set_rotateSpeed_5(float value)
	{
		___rotateSpeed_5 = value;
	}

	inline static int32_t get_offset_of_rotate_6() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___rotate_6)); }
	inline float get_rotate_6() const { return ___rotate_6; }
	inline float* get_address_of_rotate_6() { return &___rotate_6; }
	inline void set_rotate_6(float value)
	{
		___rotate_6 = value;
	}

	inline static int32_t get_offset_of_offsetDistance_7() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___offsetDistance_7)); }
	inline float get_offsetDistance_7() const { return ___offsetDistance_7; }
	inline float* get_address_of_offsetDistance_7() { return &___offsetDistance_7; }
	inline void set_offsetDistance_7(float value)
	{
		___offsetDistance_7 = value;
	}

	inline static int32_t get_offset_of_offsetHeight_8() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___offsetHeight_8)); }
	inline float get_offsetHeight_8() const { return ___offsetHeight_8; }
	inline float* get_address_of_offsetHeight_8() { return &___offsetHeight_8; }
	inline void set_offsetHeight_8(float value)
	{
		___offsetHeight_8 = value;
	}

	inline static int32_t get_offset_of_smoothing_9() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___smoothing_9)); }
	inline float get_smoothing_9() const { return ___smoothing_9; }
	inline float* get_address_of_smoothing_9() { return &___smoothing_9; }
	inline void set_smoothing_9(float value)
	{
		___smoothing_9 = value;
	}

	inline static int32_t get_offset_of_offset_10() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___offset_10)); }
	inline Vector3_t3722313464  get_offset_10() const { return ___offset_10; }
	inline Vector3_t3722313464 * get_address_of_offset_10() { return &___offset_10; }
	inline void set_offset_10(Vector3_t3722313464  value)
	{
		___offset_10 = value;
	}

	inline static int32_t get_offset_of_following_11() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___following_11)); }
	inline bool get_following_11() const { return ___following_11; }
	inline bool* get_address_of_following_11() { return &___following_11; }
	inline void set_following_11(bool value)
	{
		___following_11 = value;
	}

	inline static int32_t get_offset_of_lastPosition_12() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___lastPosition_12)); }
	inline Vector3_t3722313464  get_lastPosition_12() const { return ___lastPosition_12; }
	inline Vector3_t3722313464 * get_address_of_lastPosition_12() { return &___lastPosition_12; }
	inline void set_lastPosition_12(Vector3_t3722313464  value)
	{
		___lastPosition_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T3346819214_H
#ifndef CAMERASWITCH_T735968501_H
#define CAMERASWITCH_T735968501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSwitch
struct  CameraSwitch_t735968501  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] CameraSwitch::objects
	GameObjectU5BU5D_t3328599146* ___objects_4;
	// UnityEngine.UI.Text CameraSwitch::text
	Text_t1901882714 * ___text_5;
	// System.Int32 CameraSwitch::m_CurrentActiveObject
	int32_t ___m_CurrentActiveObject_6;

public:
	inline static int32_t get_offset_of_objects_4() { return static_cast<int32_t>(offsetof(CameraSwitch_t735968501, ___objects_4)); }
	inline GameObjectU5BU5D_t3328599146* get_objects_4() const { return ___objects_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_objects_4() { return &___objects_4; }
	inline void set_objects_4(GameObjectU5BU5D_t3328599146* value)
	{
		___objects_4 = value;
		Il2CppCodeGenWriteBarrier((&___objects_4), value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(CameraSwitch_t735968501, ___text_5)); }
	inline Text_t1901882714 * get_text_5() const { return ___text_5; }
	inline Text_t1901882714 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Text_t1901882714 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier((&___text_5), value);
	}

	inline static int32_t get_offset_of_m_CurrentActiveObject_6() { return static_cast<int32_t>(offsetof(CameraSwitch_t735968501, ___m_CurrentActiveObject_6)); }
	inline int32_t get_m_CurrentActiveObject_6() const { return ___m_CurrentActiveObject_6; }
	inline int32_t* get_address_of_m_CurrentActiveObject_6() { return &___m_CurrentActiveObject_6; }
	inline void set_m_CurrentActiveObject_6(int32_t value)
	{
		___m_CurrentActiveObject_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASWITCH_T735968501_H
#ifndef CARCAMERA_T1094971545_H
#define CARCAMERA_T1094971545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CarCamera
struct  CarCamera_t1094971545  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CarCamera::target
	Transform_t3600365921 * ___target_4;
	// System.Single CarCamera::height
	float ___height_5;
	// System.Single CarCamera::positionDamping
	float ___positionDamping_6;
	// System.Single CarCamera::velocityDamping
	float ___velocityDamping_7;
	// System.Single CarCamera::distance
	float ___distance_8;
	// UnityEngine.LayerMask CarCamera::ignoreLayers
	LayerMask_t3493934918  ___ignoreLayers_9;
	// UnityEngine.RaycastHit CarCamera::hit
	RaycastHit_t1056001966  ___hit_10;
	// UnityEngine.Vector3 CarCamera::prevVelocity
	Vector3_t3722313464  ___prevVelocity_11;
	// UnityEngine.LayerMask CarCamera::raycastLayers
	LayerMask_t3493934918  ___raycastLayers_12;
	// UnityEngine.Vector3 CarCamera::currentVelocity
	Vector3_t3722313464  ___currentVelocity_13;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(CarCamera_t1094971545, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(CarCamera_t1094971545, ___height_5)); }
	inline float get_height_5() const { return ___height_5; }
	inline float* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(float value)
	{
		___height_5 = value;
	}

	inline static int32_t get_offset_of_positionDamping_6() { return static_cast<int32_t>(offsetof(CarCamera_t1094971545, ___positionDamping_6)); }
	inline float get_positionDamping_6() const { return ___positionDamping_6; }
	inline float* get_address_of_positionDamping_6() { return &___positionDamping_6; }
	inline void set_positionDamping_6(float value)
	{
		___positionDamping_6 = value;
	}

	inline static int32_t get_offset_of_velocityDamping_7() { return static_cast<int32_t>(offsetof(CarCamera_t1094971545, ___velocityDamping_7)); }
	inline float get_velocityDamping_7() const { return ___velocityDamping_7; }
	inline float* get_address_of_velocityDamping_7() { return &___velocityDamping_7; }
	inline void set_velocityDamping_7(float value)
	{
		___velocityDamping_7 = value;
	}

	inline static int32_t get_offset_of_distance_8() { return static_cast<int32_t>(offsetof(CarCamera_t1094971545, ___distance_8)); }
	inline float get_distance_8() const { return ___distance_8; }
	inline float* get_address_of_distance_8() { return &___distance_8; }
	inline void set_distance_8(float value)
	{
		___distance_8 = value;
	}

	inline static int32_t get_offset_of_ignoreLayers_9() { return static_cast<int32_t>(offsetof(CarCamera_t1094971545, ___ignoreLayers_9)); }
	inline LayerMask_t3493934918  get_ignoreLayers_9() const { return ___ignoreLayers_9; }
	inline LayerMask_t3493934918 * get_address_of_ignoreLayers_9() { return &___ignoreLayers_9; }
	inline void set_ignoreLayers_9(LayerMask_t3493934918  value)
	{
		___ignoreLayers_9 = value;
	}

	inline static int32_t get_offset_of_hit_10() { return static_cast<int32_t>(offsetof(CarCamera_t1094971545, ___hit_10)); }
	inline RaycastHit_t1056001966  get_hit_10() const { return ___hit_10; }
	inline RaycastHit_t1056001966 * get_address_of_hit_10() { return &___hit_10; }
	inline void set_hit_10(RaycastHit_t1056001966  value)
	{
		___hit_10 = value;
	}

	inline static int32_t get_offset_of_prevVelocity_11() { return static_cast<int32_t>(offsetof(CarCamera_t1094971545, ___prevVelocity_11)); }
	inline Vector3_t3722313464  get_prevVelocity_11() const { return ___prevVelocity_11; }
	inline Vector3_t3722313464 * get_address_of_prevVelocity_11() { return &___prevVelocity_11; }
	inline void set_prevVelocity_11(Vector3_t3722313464  value)
	{
		___prevVelocity_11 = value;
	}

	inline static int32_t get_offset_of_raycastLayers_12() { return static_cast<int32_t>(offsetof(CarCamera_t1094971545, ___raycastLayers_12)); }
	inline LayerMask_t3493934918  get_raycastLayers_12() const { return ___raycastLayers_12; }
	inline LayerMask_t3493934918 * get_address_of_raycastLayers_12() { return &___raycastLayers_12; }
	inline void set_raycastLayers_12(LayerMask_t3493934918  value)
	{
		___raycastLayers_12 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_13() { return static_cast<int32_t>(offsetof(CarCamera_t1094971545, ___currentVelocity_13)); }
	inline Vector3_t3722313464  get_currentVelocity_13() const { return ___currentVelocity_13; }
	inline Vector3_t3722313464 * get_address_of_currentVelocity_13() { return &___currentVelocity_13; }
	inline void set_currentVelocity_13(Vector3_t3722313464  value)
	{
		___currentVelocity_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARCAMERA_T1094971545_H
#ifndef CHARACTERCONTROLLERCOLLCHECK_T3758802271_H
#define CHARACTERCONTROLLERCOLLCHECK_T3758802271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterControllerCollCheck
struct  CharacterControllerCollCheck_t3758802271  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.CharacterController CharacterControllerCollCheck::charControl
	CharacterController_t1138636865 * ___charControl_4;
	// System.Single CharacterControllerCollCheck::gravity
	float ___gravity_5;

public:
	inline static int32_t get_offset_of_charControl_4() { return static_cast<int32_t>(offsetof(CharacterControllerCollCheck_t3758802271, ___charControl_4)); }
	inline CharacterController_t1138636865 * get_charControl_4() const { return ___charControl_4; }
	inline CharacterController_t1138636865 ** get_address_of_charControl_4() { return &___charControl_4; }
	inline void set_charControl_4(CharacterController_t1138636865 * value)
	{
		___charControl_4 = value;
		Il2CppCodeGenWriteBarrier((&___charControl_4), value);
	}

	inline static int32_t get_offset_of_gravity_5() { return static_cast<int32_t>(offsetof(CharacterControllerCollCheck_t3758802271, ___gravity_5)); }
	inline float get_gravity_5() const { return ___gravity_5; }
	inline float* get_address_of_gravity_5() { return &___gravity_5; }
	inline void set_gravity_5(float value)
	{
		___gravity_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCONTROLLERCOLLCHECK_T3758802271_H
#ifndef CHARACTERDEMOCONTROLLER_T836415614_H
#define CHARACTERDEMOCONTROLLER_T836415614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterDemoController
struct  CharacterDemoController_t836415614  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator CharacterDemoController::animator
	Animator_t434523843 * ___animator_4;
	// UnityEngine.GameObject CharacterDemoController::floorPlane
	GameObject_t1113636619 * ___floorPlane_5;
	// System.Int32 CharacterDemoController::WeaponState
	int32_t ___WeaponState_6;
	// System.Boolean CharacterDemoController::wasAttacking
	bool ___wasAttacking_7;
	// System.Single CharacterDemoController::rotateSpeed
	float ___rotateSpeed_8;
	// UnityEngine.Vector3 CharacterDemoController::movementTargetPosition
	Vector3_t3722313464  ___movementTargetPosition_9;
	// UnityEngine.Vector3 CharacterDemoController::attackPos
	Vector3_t3722313464  ___attackPos_10;
	// UnityEngine.Vector3 CharacterDemoController::lookAtPos
	Vector3_t3722313464  ___lookAtPos_11;
	// System.Single CharacterDemoController::gravity
	float ___gravity_12;
	// UnityEngine.RaycastHit CharacterDemoController::hit
	RaycastHit_t1056001966  ___hit_13;
	// UnityEngine.Ray CharacterDemoController::ray
	Ray_t3785851493  ___ray_14;
	// System.Boolean CharacterDemoController::rightButtonDown
	bool ___rightButtonDown_15;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___animator_4)); }
	inline Animator_t434523843 * get_animator_4() const { return ___animator_4; }
	inline Animator_t434523843 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t434523843 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}

	inline static int32_t get_offset_of_floorPlane_5() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___floorPlane_5)); }
	inline GameObject_t1113636619 * get_floorPlane_5() const { return ___floorPlane_5; }
	inline GameObject_t1113636619 ** get_address_of_floorPlane_5() { return &___floorPlane_5; }
	inline void set_floorPlane_5(GameObject_t1113636619 * value)
	{
		___floorPlane_5 = value;
		Il2CppCodeGenWriteBarrier((&___floorPlane_5), value);
	}

	inline static int32_t get_offset_of_WeaponState_6() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___WeaponState_6)); }
	inline int32_t get_WeaponState_6() const { return ___WeaponState_6; }
	inline int32_t* get_address_of_WeaponState_6() { return &___WeaponState_6; }
	inline void set_WeaponState_6(int32_t value)
	{
		___WeaponState_6 = value;
	}

	inline static int32_t get_offset_of_wasAttacking_7() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___wasAttacking_7)); }
	inline bool get_wasAttacking_7() const { return ___wasAttacking_7; }
	inline bool* get_address_of_wasAttacking_7() { return &___wasAttacking_7; }
	inline void set_wasAttacking_7(bool value)
	{
		___wasAttacking_7 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_8() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___rotateSpeed_8)); }
	inline float get_rotateSpeed_8() const { return ___rotateSpeed_8; }
	inline float* get_address_of_rotateSpeed_8() { return &___rotateSpeed_8; }
	inline void set_rotateSpeed_8(float value)
	{
		___rotateSpeed_8 = value;
	}

	inline static int32_t get_offset_of_movementTargetPosition_9() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___movementTargetPosition_9)); }
	inline Vector3_t3722313464  get_movementTargetPosition_9() const { return ___movementTargetPosition_9; }
	inline Vector3_t3722313464 * get_address_of_movementTargetPosition_9() { return &___movementTargetPosition_9; }
	inline void set_movementTargetPosition_9(Vector3_t3722313464  value)
	{
		___movementTargetPosition_9 = value;
	}

	inline static int32_t get_offset_of_attackPos_10() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___attackPos_10)); }
	inline Vector3_t3722313464  get_attackPos_10() const { return ___attackPos_10; }
	inline Vector3_t3722313464 * get_address_of_attackPos_10() { return &___attackPos_10; }
	inline void set_attackPos_10(Vector3_t3722313464  value)
	{
		___attackPos_10 = value;
	}

	inline static int32_t get_offset_of_lookAtPos_11() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___lookAtPos_11)); }
	inline Vector3_t3722313464  get_lookAtPos_11() const { return ___lookAtPos_11; }
	inline Vector3_t3722313464 * get_address_of_lookAtPos_11() { return &___lookAtPos_11; }
	inline void set_lookAtPos_11(Vector3_t3722313464  value)
	{
		___lookAtPos_11 = value;
	}

	inline static int32_t get_offset_of_gravity_12() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___gravity_12)); }
	inline float get_gravity_12() const { return ___gravity_12; }
	inline float* get_address_of_gravity_12() { return &___gravity_12; }
	inline void set_gravity_12(float value)
	{
		___gravity_12 = value;
	}

	inline static int32_t get_offset_of_hit_13() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___hit_13)); }
	inline RaycastHit_t1056001966  get_hit_13() const { return ___hit_13; }
	inline RaycastHit_t1056001966 * get_address_of_hit_13() { return &___hit_13; }
	inline void set_hit_13(RaycastHit_t1056001966  value)
	{
		___hit_13 = value;
	}

	inline static int32_t get_offset_of_ray_14() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___ray_14)); }
	inline Ray_t3785851493  get_ray_14() const { return ___ray_14; }
	inline Ray_t3785851493 * get_address_of_ray_14() { return &___ray_14; }
	inline void set_ray_14(Ray_t3785851493  value)
	{
		___ray_14 = value;
	}

	inline static int32_t get_offset_of_rightButtonDown_15() { return static_cast<int32_t>(offsetof(CharacterDemoController_t836415614, ___rightButtonDown_15)); }
	inline bool get_rightButtonDown_15() const { return ___rightButtonDown_15; }
	inline bool* get_address_of_rightButtonDown_15() { return &___rightButtonDown_15; }
	inline void set_rightButtonDown_15(bool value)
	{
		___rightButtonDown_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERDEMOCONTROLLER_T836415614_H
#ifndef CHARACTERPHYSICSCONTROLLER_T3068241336_H
#define CHARACTERPHYSICSCONTROLLER_T3068241336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterPhysicsController
struct  CharacterPhysicsController_t3068241336  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator CharacterPhysicsController::animator
	Animator_t434523843 * ___animator_4;
	// UnityEngine.GameObject CharacterPhysicsController::floorPlane
	GameObject_t1113636619 * ___floorPlane_5;
	// System.Int32 CharacterPhysicsController::WeaponState
	int32_t ___WeaponState_6;
	// System.Boolean CharacterPhysicsController::wasAttacking
	bool ___wasAttacking_7;
	// System.Single CharacterPhysicsController::rotateSpeed
	float ___rotateSpeed_8;
	// UnityEngine.CharacterController CharacterPhysicsController::charcontroller
	CharacterController_t1138636865 * ___charcontroller_9;
	// UnityEngine.Vector3 CharacterPhysicsController::movementTargetPosition
	Vector3_t3722313464  ___movementTargetPosition_10;
	// UnityEngine.Vector3 CharacterPhysicsController::attackPos
	Vector3_t3722313464  ___attackPos_11;
	// UnityEngine.Vector3 CharacterPhysicsController::lookAtPos
	Vector3_t3722313464  ___lookAtPos_12;
	// System.Single CharacterPhysicsController::gravity
	float ___gravity_13;
	// System.Boolean CharacterPhysicsController::jumping
	bool ___jumping_14;
	// System.Boolean CharacterPhysicsController::grounded
	bool ___grounded_15;
	// UnityEngine.GameObject CharacterPhysicsController::contact
	GameObject_t1113636619 * ___contact_16;
	// System.Single CharacterPhysicsController::verticalVelocity
	float ___verticalVelocity_17;
	// UnityEngine.RaycastHit CharacterPhysicsController::hit
	RaycastHit_t1056001966  ___hit_18;
	// UnityEngine.Ray CharacterPhysicsController::ray
	Ray_t3785851493  ___ray_19;
	// System.Boolean CharacterPhysicsController::rightButtonDown
	bool ___rightButtonDown_20;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___animator_4)); }
	inline Animator_t434523843 * get_animator_4() const { return ___animator_4; }
	inline Animator_t434523843 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t434523843 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}

	inline static int32_t get_offset_of_floorPlane_5() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___floorPlane_5)); }
	inline GameObject_t1113636619 * get_floorPlane_5() const { return ___floorPlane_5; }
	inline GameObject_t1113636619 ** get_address_of_floorPlane_5() { return &___floorPlane_5; }
	inline void set_floorPlane_5(GameObject_t1113636619 * value)
	{
		___floorPlane_5 = value;
		Il2CppCodeGenWriteBarrier((&___floorPlane_5), value);
	}

	inline static int32_t get_offset_of_WeaponState_6() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___WeaponState_6)); }
	inline int32_t get_WeaponState_6() const { return ___WeaponState_6; }
	inline int32_t* get_address_of_WeaponState_6() { return &___WeaponState_6; }
	inline void set_WeaponState_6(int32_t value)
	{
		___WeaponState_6 = value;
	}

	inline static int32_t get_offset_of_wasAttacking_7() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___wasAttacking_7)); }
	inline bool get_wasAttacking_7() const { return ___wasAttacking_7; }
	inline bool* get_address_of_wasAttacking_7() { return &___wasAttacking_7; }
	inline void set_wasAttacking_7(bool value)
	{
		___wasAttacking_7 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_8() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___rotateSpeed_8)); }
	inline float get_rotateSpeed_8() const { return ___rotateSpeed_8; }
	inline float* get_address_of_rotateSpeed_8() { return &___rotateSpeed_8; }
	inline void set_rotateSpeed_8(float value)
	{
		___rotateSpeed_8 = value;
	}

	inline static int32_t get_offset_of_charcontroller_9() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___charcontroller_9)); }
	inline CharacterController_t1138636865 * get_charcontroller_9() const { return ___charcontroller_9; }
	inline CharacterController_t1138636865 ** get_address_of_charcontroller_9() { return &___charcontroller_9; }
	inline void set_charcontroller_9(CharacterController_t1138636865 * value)
	{
		___charcontroller_9 = value;
		Il2CppCodeGenWriteBarrier((&___charcontroller_9), value);
	}

	inline static int32_t get_offset_of_movementTargetPosition_10() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___movementTargetPosition_10)); }
	inline Vector3_t3722313464  get_movementTargetPosition_10() const { return ___movementTargetPosition_10; }
	inline Vector3_t3722313464 * get_address_of_movementTargetPosition_10() { return &___movementTargetPosition_10; }
	inline void set_movementTargetPosition_10(Vector3_t3722313464  value)
	{
		___movementTargetPosition_10 = value;
	}

	inline static int32_t get_offset_of_attackPos_11() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___attackPos_11)); }
	inline Vector3_t3722313464  get_attackPos_11() const { return ___attackPos_11; }
	inline Vector3_t3722313464 * get_address_of_attackPos_11() { return &___attackPos_11; }
	inline void set_attackPos_11(Vector3_t3722313464  value)
	{
		___attackPos_11 = value;
	}

	inline static int32_t get_offset_of_lookAtPos_12() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___lookAtPos_12)); }
	inline Vector3_t3722313464  get_lookAtPos_12() const { return ___lookAtPos_12; }
	inline Vector3_t3722313464 * get_address_of_lookAtPos_12() { return &___lookAtPos_12; }
	inline void set_lookAtPos_12(Vector3_t3722313464  value)
	{
		___lookAtPos_12 = value;
	}

	inline static int32_t get_offset_of_gravity_13() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___gravity_13)); }
	inline float get_gravity_13() const { return ___gravity_13; }
	inline float* get_address_of_gravity_13() { return &___gravity_13; }
	inline void set_gravity_13(float value)
	{
		___gravity_13 = value;
	}

	inline static int32_t get_offset_of_jumping_14() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___jumping_14)); }
	inline bool get_jumping_14() const { return ___jumping_14; }
	inline bool* get_address_of_jumping_14() { return &___jumping_14; }
	inline void set_jumping_14(bool value)
	{
		___jumping_14 = value;
	}

	inline static int32_t get_offset_of_grounded_15() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___grounded_15)); }
	inline bool get_grounded_15() const { return ___grounded_15; }
	inline bool* get_address_of_grounded_15() { return &___grounded_15; }
	inline void set_grounded_15(bool value)
	{
		___grounded_15 = value;
	}

	inline static int32_t get_offset_of_contact_16() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___contact_16)); }
	inline GameObject_t1113636619 * get_contact_16() const { return ___contact_16; }
	inline GameObject_t1113636619 ** get_address_of_contact_16() { return &___contact_16; }
	inline void set_contact_16(GameObject_t1113636619 * value)
	{
		___contact_16 = value;
		Il2CppCodeGenWriteBarrier((&___contact_16), value);
	}

	inline static int32_t get_offset_of_verticalVelocity_17() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___verticalVelocity_17)); }
	inline float get_verticalVelocity_17() const { return ___verticalVelocity_17; }
	inline float* get_address_of_verticalVelocity_17() { return &___verticalVelocity_17; }
	inline void set_verticalVelocity_17(float value)
	{
		___verticalVelocity_17 = value;
	}

	inline static int32_t get_offset_of_hit_18() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___hit_18)); }
	inline RaycastHit_t1056001966  get_hit_18() const { return ___hit_18; }
	inline RaycastHit_t1056001966 * get_address_of_hit_18() { return &___hit_18; }
	inline void set_hit_18(RaycastHit_t1056001966  value)
	{
		___hit_18 = value;
	}

	inline static int32_t get_offset_of_ray_19() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___ray_19)); }
	inline Ray_t3785851493  get_ray_19() const { return ___ray_19; }
	inline Ray_t3785851493 * get_address_of_ray_19() { return &___ray_19; }
	inline void set_ray_19(Ray_t3785851493  value)
	{
		___ray_19 = value;
	}

	inline static int32_t get_offset_of_rightButtonDown_20() { return static_cast<int32_t>(offsetof(CharacterPhysicsController_t3068241336, ___rightButtonDown_20)); }
	inline bool get_rightButtonDown_20() const { return ___rightButtonDown_20; }
	inline bool* get_address_of_rightButtonDown_20() { return &___rightButtonDown_20; }
	inline void set_rightButtonDown_20(bool value)
	{
		___rightButtonDown_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERPHYSICSCONTROLLER_T3068241336_H
#ifndef ENEMY_T1765729589_H
#define ENEMY_T1765729589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enemy
struct  Enemy_t1765729589  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D Enemy::cursorTexture
	Texture2D_t3840446185 * ___cursorTexture_4;
	// UnityEngine.CursorMode Enemy::cursorMode
	int32_t ___cursorMode_5;
	// UnityEngine.Vector2 Enemy::hotSpot
	Vector2_t2156229523  ___hotSpot_6;
	// System.Boolean Enemy::targeted
	bool ___targeted_7;

public:
	inline static int32_t get_offset_of_cursorTexture_4() { return static_cast<int32_t>(offsetof(Enemy_t1765729589, ___cursorTexture_4)); }
	inline Texture2D_t3840446185 * get_cursorTexture_4() const { return ___cursorTexture_4; }
	inline Texture2D_t3840446185 ** get_address_of_cursorTexture_4() { return &___cursorTexture_4; }
	inline void set_cursorTexture_4(Texture2D_t3840446185 * value)
	{
		___cursorTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___cursorTexture_4), value);
	}

	inline static int32_t get_offset_of_cursorMode_5() { return static_cast<int32_t>(offsetof(Enemy_t1765729589, ___cursorMode_5)); }
	inline int32_t get_cursorMode_5() const { return ___cursorMode_5; }
	inline int32_t* get_address_of_cursorMode_5() { return &___cursorMode_5; }
	inline void set_cursorMode_5(int32_t value)
	{
		___cursorMode_5 = value;
	}

	inline static int32_t get_offset_of_hotSpot_6() { return static_cast<int32_t>(offsetof(Enemy_t1765729589, ___hotSpot_6)); }
	inline Vector2_t2156229523  get_hotSpot_6() const { return ___hotSpot_6; }
	inline Vector2_t2156229523 * get_address_of_hotSpot_6() { return &___hotSpot_6; }
	inline void set_hotSpot_6(Vector2_t2156229523  value)
	{
		___hotSpot_6 = value;
	}

	inline static int32_t get_offset_of_targeted_7() { return static_cast<int32_t>(offsetof(Enemy_t1765729589, ___targeted_7)); }
	inline bool get_targeted_7() const { return ___targeted_7; }
	inline bool* get_address_of_targeted_7() { return &___targeted_7; }
	inline void set_targeted_7(bool value)
	{
		___targeted_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMY_T1765729589_H
#ifndef EXAMPLEWHEELCONTROLLER_T197115271_H
#define EXAMPLEWHEELCONTROLLER_T197115271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleWheelController
struct  ExampleWheelController_t197115271  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ExampleWheelController::acceleration
	float ___acceleration_4;
	// UnityEngine.Renderer ExampleWheelController::motionVectorRenderer
	Renderer_t2627027031 * ___motionVectorRenderer_5;
	// UnityEngine.Rigidbody ExampleWheelController::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_6;

public:
	inline static int32_t get_offset_of_acceleration_4() { return static_cast<int32_t>(offsetof(ExampleWheelController_t197115271, ___acceleration_4)); }
	inline float get_acceleration_4() const { return ___acceleration_4; }
	inline float* get_address_of_acceleration_4() { return &___acceleration_4; }
	inline void set_acceleration_4(float value)
	{
		___acceleration_4 = value;
	}

	inline static int32_t get_offset_of_motionVectorRenderer_5() { return static_cast<int32_t>(offsetof(ExampleWheelController_t197115271, ___motionVectorRenderer_5)); }
	inline Renderer_t2627027031 * get_motionVectorRenderer_5() const { return ___motionVectorRenderer_5; }
	inline Renderer_t2627027031 ** get_address_of_motionVectorRenderer_5() { return &___motionVectorRenderer_5; }
	inline void set_motionVectorRenderer_5(Renderer_t2627027031 * value)
	{
		___motionVectorRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___motionVectorRenderer_5), value);
	}

	inline static int32_t get_offset_of_m_Rigidbody_6() { return static_cast<int32_t>(offsetof(ExampleWheelController_t197115271, ___m_Rigidbody_6)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_6() const { return ___m_Rigidbody_6; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_6() { return &___m_Rigidbody_6; }
	inline void set_m_Rigidbody_6(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEWHEELCONTROLLER_T197115271_H
#ifndef GRAVITY_T379910146_H
#define GRAVITY_T379910146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gravity
struct  Gravity_t379910146  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Gravity::planet
	Transform_t3600365921 * ___planet_4;

public:
	inline static int32_t get_offset_of_planet_4() { return static_cast<int32_t>(offsetof(Gravity_t379910146, ___planet_4)); }
	inline Transform_t3600365921 * get_planet_4() const { return ___planet_4; }
	inline Transform_t3600365921 ** get_address_of_planet_4() { return &___planet_4; }
	inline void set_planet_4(Transform_t3600365921 * value)
	{
		___planet_4 = value;
		Il2CppCodeGenWriteBarrier((&___planet_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAVITY_T379910146_H
#ifndef HIT_T3558828664_H
#define HIT_T3558828664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Hit
struct  Hit_t3558828664  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.TextMesh Hit::yellow
	TextMesh_t1536577757 * ___yellow_4;
	// UnityEngine.TextMesh Hit::black
	TextMesh_t1536577757 * ___black_5;
	// System.Single Hit::startTime
	float ___startTime_6;
	// System.Single Hit::currentTime
	float ___currentTime_7;
	// System.Single Hit::lifespan
	float ___lifespan_8;
	// System.Single Hit::fadeTime
	float ___fadeTime_9;
	// System.Single Hit::fadeSpeed
	float ___fadeSpeed_10;
	// System.Single Hit::maxSize
	float ___maxSize_11;
	// System.Single Hit::speed
	float ___speed_12;
	// System.String Hit::text
	String_t* ___text_13;

public:
	inline static int32_t get_offset_of_yellow_4() { return static_cast<int32_t>(offsetof(Hit_t3558828664, ___yellow_4)); }
	inline TextMesh_t1536577757 * get_yellow_4() const { return ___yellow_4; }
	inline TextMesh_t1536577757 ** get_address_of_yellow_4() { return &___yellow_4; }
	inline void set_yellow_4(TextMesh_t1536577757 * value)
	{
		___yellow_4 = value;
		Il2CppCodeGenWriteBarrier((&___yellow_4), value);
	}

	inline static int32_t get_offset_of_black_5() { return static_cast<int32_t>(offsetof(Hit_t3558828664, ___black_5)); }
	inline TextMesh_t1536577757 * get_black_5() const { return ___black_5; }
	inline TextMesh_t1536577757 ** get_address_of_black_5() { return &___black_5; }
	inline void set_black_5(TextMesh_t1536577757 * value)
	{
		___black_5 = value;
		Il2CppCodeGenWriteBarrier((&___black_5), value);
	}

	inline static int32_t get_offset_of_startTime_6() { return static_cast<int32_t>(offsetof(Hit_t3558828664, ___startTime_6)); }
	inline float get_startTime_6() const { return ___startTime_6; }
	inline float* get_address_of_startTime_6() { return &___startTime_6; }
	inline void set_startTime_6(float value)
	{
		___startTime_6 = value;
	}

	inline static int32_t get_offset_of_currentTime_7() { return static_cast<int32_t>(offsetof(Hit_t3558828664, ___currentTime_7)); }
	inline float get_currentTime_7() const { return ___currentTime_7; }
	inline float* get_address_of_currentTime_7() { return &___currentTime_7; }
	inline void set_currentTime_7(float value)
	{
		___currentTime_7 = value;
	}

	inline static int32_t get_offset_of_lifespan_8() { return static_cast<int32_t>(offsetof(Hit_t3558828664, ___lifespan_8)); }
	inline float get_lifespan_8() const { return ___lifespan_8; }
	inline float* get_address_of_lifespan_8() { return &___lifespan_8; }
	inline void set_lifespan_8(float value)
	{
		___lifespan_8 = value;
	}

	inline static int32_t get_offset_of_fadeTime_9() { return static_cast<int32_t>(offsetof(Hit_t3558828664, ___fadeTime_9)); }
	inline float get_fadeTime_9() const { return ___fadeTime_9; }
	inline float* get_address_of_fadeTime_9() { return &___fadeTime_9; }
	inline void set_fadeTime_9(float value)
	{
		___fadeTime_9 = value;
	}

	inline static int32_t get_offset_of_fadeSpeed_10() { return static_cast<int32_t>(offsetof(Hit_t3558828664, ___fadeSpeed_10)); }
	inline float get_fadeSpeed_10() const { return ___fadeSpeed_10; }
	inline float* get_address_of_fadeSpeed_10() { return &___fadeSpeed_10; }
	inline void set_fadeSpeed_10(float value)
	{
		___fadeSpeed_10 = value;
	}

	inline static int32_t get_offset_of_maxSize_11() { return static_cast<int32_t>(offsetof(Hit_t3558828664, ___maxSize_11)); }
	inline float get_maxSize_11() const { return ___maxSize_11; }
	inline float* get_address_of_maxSize_11() { return &___maxSize_11; }
	inline void set_maxSize_11(float value)
	{
		___maxSize_11 = value;
	}

	inline static int32_t get_offset_of_speed_12() { return static_cast<int32_t>(offsetof(Hit_t3558828664, ___speed_12)); }
	inline float get_speed_12() const { return ___speed_12; }
	inline float* get_address_of_speed_12() { return &___speed_12; }
	inline void set_speed_12(float value)
	{
		___speed_12 = value;
	}

	inline static int32_t get_offset_of_text_13() { return static_cast<int32_t>(offsetof(Hit_t3558828664, ___text_13)); }
	inline String_t* get_text_13() const { return ___text_13; }
	inline String_t** get_address_of_text_13() { return &___text_13; }
	inline void set_text_13(String_t* value)
	{
		___text_13 = value;
		Il2CppCodeGenWriteBarrier((&___text_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIT_T3558828664_H
#ifndef LEVELRESET_T1558959187_H
#define LEVELRESET_T1558959187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelReset
struct  LevelReset_t1558959187  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELRESET_T1558959187_H
#ifndef MATH3D_T1427253148_H
#define MATH3D_T1427253148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Math3d
struct  Math3d_t1427253148  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Math3d_t1427253148_StaticFields
{
public:
	// UnityEngine.Transform Math3d::tempChild
	Transform_t3600365921 * ___tempChild_4;
	// UnityEngine.Transform Math3d::tempParent
	Transform_t3600365921 * ___tempParent_5;

public:
	inline static int32_t get_offset_of_tempChild_4() { return static_cast<int32_t>(offsetof(Math3d_t1427253148_StaticFields, ___tempChild_4)); }
	inline Transform_t3600365921 * get_tempChild_4() const { return ___tempChild_4; }
	inline Transform_t3600365921 ** get_address_of_tempChild_4() { return &___tempChild_4; }
	inline void set_tempChild_4(Transform_t3600365921 * value)
	{
		___tempChild_4 = value;
		Il2CppCodeGenWriteBarrier((&___tempChild_4), value);
	}

	inline static int32_t get_offset_of_tempParent_5() { return static_cast<int32_t>(offsetof(Math3d_t1427253148_StaticFields, ___tempParent_5)); }
	inline Transform_t3600365921 * get_tempParent_5() const { return ___tempParent_5; }
	inline Transform_t3600365921 ** get_address_of_tempParent_5() { return &___tempParent_5; }
	inline void set_tempParent_5(Transform_t3600365921 * value)
	{
		___tempParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___tempParent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATH3D_T1427253148_H
#ifndef MENUSCENELOADER_T2183500486_H
#define MENUSCENELOADER_T2183500486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuSceneLoader
struct  MenuSceneLoader_t2183500486  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MenuSceneLoader::menuUI
	GameObject_t1113636619 * ___menuUI_4;
	// UnityEngine.GameObject MenuSceneLoader::m_Go
	GameObject_t1113636619 * ___m_Go_5;

public:
	inline static int32_t get_offset_of_menuUI_4() { return static_cast<int32_t>(offsetof(MenuSceneLoader_t2183500486, ___menuUI_4)); }
	inline GameObject_t1113636619 * get_menuUI_4() const { return ___menuUI_4; }
	inline GameObject_t1113636619 ** get_address_of_menuUI_4() { return &___menuUI_4; }
	inline void set_menuUI_4(GameObject_t1113636619 * value)
	{
		___menuUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___menuUI_4), value);
	}

	inline static int32_t get_offset_of_m_Go_5() { return static_cast<int32_t>(offsetof(MenuSceneLoader_t2183500486, ___m_Go_5)); }
	inline GameObject_t1113636619 * get_m_Go_5() const { return ___m_Go_5; }
	inline GameObject_t1113636619 ** get_address_of_m_Go_5() { return &___m_Go_5; }
	inline void set_m_Go_5(GameObject_t1113636619 * value)
	{
		___m_Go_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Go_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUSCENELOADER_T2183500486_H
#ifndef MISSILE_T4280459806_H
#define MISSILE_T4280459806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Missile
struct  Missile_t4280459806  : public MonoBehaviour_t3962482529
{
public:
	// Enemy[] Missile::enemies
	EnemyU5BU5D_t2006687320* ___enemies_4;
	// System.Single Missile::speed
	float ___speed_5;
	// System.Single Missile::damage
	float ___damage_6;
	// ASCLBasicController Missile::abc
	ASCLBasicController_t2851843984 * ___abc_7;
	// System.Single Missile::lifespan
	float ___lifespan_8;
	// System.Single Missile::startTime
	float ___startTime_9;
	// UnityEngine.Transform Missile::hitReport
	Transform_t3600365921 * ___hitReport_10;
	// UnityEngine.Transform Missile::particleHit
	Transform_t3600365921 * ___particleHit_11;

public:
	inline static int32_t get_offset_of_enemies_4() { return static_cast<int32_t>(offsetof(Missile_t4280459806, ___enemies_4)); }
	inline EnemyU5BU5D_t2006687320* get_enemies_4() const { return ___enemies_4; }
	inline EnemyU5BU5D_t2006687320** get_address_of_enemies_4() { return &___enemies_4; }
	inline void set_enemies_4(EnemyU5BU5D_t2006687320* value)
	{
		___enemies_4 = value;
		Il2CppCodeGenWriteBarrier((&___enemies_4), value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(Missile_t4280459806, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_damage_6() { return static_cast<int32_t>(offsetof(Missile_t4280459806, ___damage_6)); }
	inline float get_damage_6() const { return ___damage_6; }
	inline float* get_address_of_damage_6() { return &___damage_6; }
	inline void set_damage_6(float value)
	{
		___damage_6 = value;
	}

	inline static int32_t get_offset_of_abc_7() { return static_cast<int32_t>(offsetof(Missile_t4280459806, ___abc_7)); }
	inline ASCLBasicController_t2851843984 * get_abc_7() const { return ___abc_7; }
	inline ASCLBasicController_t2851843984 ** get_address_of_abc_7() { return &___abc_7; }
	inline void set_abc_7(ASCLBasicController_t2851843984 * value)
	{
		___abc_7 = value;
		Il2CppCodeGenWriteBarrier((&___abc_7), value);
	}

	inline static int32_t get_offset_of_lifespan_8() { return static_cast<int32_t>(offsetof(Missile_t4280459806, ___lifespan_8)); }
	inline float get_lifespan_8() const { return ___lifespan_8; }
	inline float* get_address_of_lifespan_8() { return &___lifespan_8; }
	inline void set_lifespan_8(float value)
	{
		___lifespan_8 = value;
	}

	inline static int32_t get_offset_of_startTime_9() { return static_cast<int32_t>(offsetof(Missile_t4280459806, ___startTime_9)); }
	inline float get_startTime_9() const { return ___startTime_9; }
	inline float* get_address_of_startTime_9() { return &___startTime_9; }
	inline void set_startTime_9(float value)
	{
		___startTime_9 = value;
	}

	inline static int32_t get_offset_of_hitReport_10() { return static_cast<int32_t>(offsetof(Missile_t4280459806, ___hitReport_10)); }
	inline Transform_t3600365921 * get_hitReport_10() const { return ___hitReport_10; }
	inline Transform_t3600365921 ** get_address_of_hitReport_10() { return &___hitReport_10; }
	inline void set_hitReport_10(Transform_t3600365921 * value)
	{
		___hitReport_10 = value;
		Il2CppCodeGenWriteBarrier((&___hitReport_10), value);
	}

	inline static int32_t get_offset_of_particleHit_11() { return static_cast<int32_t>(offsetof(Missile_t4280459806, ___particleHit_11)); }
	inline Transform_t3600365921 * get_particleHit_11() const { return ___particleHit_11; }
	inline Transform_t3600365921 ** get_address_of_particleHit_11() { return &___particleHit_11; }
	inline void set_particleHit_11(Transform_t3600365921 * value)
	{
		___particleHit_11 = value;
		Il2CppCodeGenWriteBarrier((&___particleHit_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSILE_T4280459806_H
#ifndef MOUSEORBIT_T3431653639_H
#define MOUSEORBIT_T3431653639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MouseOrbit
struct  MouseOrbit_t3431653639  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform MouseOrbit::target
	Transform_t3600365921 * ___target_4;
	// System.Single MouseOrbit::distance
	float ___distance_5;
	// System.Single MouseOrbit::xSpeed
	float ___xSpeed_6;
	// System.Single MouseOrbit::ySpeed
	float ___ySpeed_7;
	// System.Single MouseOrbit::x
	float ___x_8;
	// System.Single MouseOrbit::y
	float ___y_9;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(MouseOrbit_t3431653639, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(MouseOrbit_t3431653639, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_xSpeed_6() { return static_cast<int32_t>(offsetof(MouseOrbit_t3431653639, ___xSpeed_6)); }
	inline float get_xSpeed_6() const { return ___xSpeed_6; }
	inline float* get_address_of_xSpeed_6() { return &___xSpeed_6; }
	inline void set_xSpeed_6(float value)
	{
		___xSpeed_6 = value;
	}

	inline static int32_t get_offset_of_ySpeed_7() { return static_cast<int32_t>(offsetof(MouseOrbit_t3431653639, ___ySpeed_7)); }
	inline float get_ySpeed_7() const { return ___ySpeed_7; }
	inline float* get_address_of_ySpeed_7() { return &___ySpeed_7; }
	inline void set_ySpeed_7(float value)
	{
		___ySpeed_7 = value;
	}

	inline static int32_t get_offset_of_x_8() { return static_cast<int32_t>(offsetof(MouseOrbit_t3431653639, ___x_8)); }
	inline float get_x_8() const { return ___x_8; }
	inline float* get_address_of_x_8() { return &___x_8; }
	inline void set_x_8(float value)
	{
		___x_8 = value;
	}

	inline static int32_t get_offset_of_y_9() { return static_cast<int32_t>(offsetof(MouseOrbit_t3431653639, ___y_9)); }
	inline float get_y_9() const { return ___y_9; }
	inline float* get_address_of_y_9() { return &___y_9; }
	inline void set_y_9(float value)
	{
		___y_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEORBIT_T3431653639_H
#ifndef MOVECTRLDEMO_T2262282185_H
#define MOVECTRLDEMO_T2262282185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveCTRLDemo
struct  MoveCTRLDemo_t2262282185  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MoveCTRLDemo::move
	float ___move_4;
	// System.Boolean MoveCTRLDemo::stop
	bool ___stop_5;
	// System.Single MoveCTRLDemo::blend
	float ___blend_6;
	// System.Single MoveCTRLDemo::delay
	float ___delay_7;
	// System.Single MoveCTRLDemo::AddRunSpeed
	float ___AddRunSpeed_8;
	// System.Single MoveCTRLDemo::AddWalkSpeed
	float ___AddWalkSpeed_9;
	// System.Boolean MoveCTRLDemo::hasAniComp
	bool ___hasAniComp_10;

public:
	inline static int32_t get_offset_of_move_4() { return static_cast<int32_t>(offsetof(MoveCTRLDemo_t2262282185, ___move_4)); }
	inline float get_move_4() const { return ___move_4; }
	inline float* get_address_of_move_4() { return &___move_4; }
	inline void set_move_4(float value)
	{
		___move_4 = value;
	}

	inline static int32_t get_offset_of_stop_5() { return static_cast<int32_t>(offsetof(MoveCTRLDemo_t2262282185, ___stop_5)); }
	inline bool get_stop_5() const { return ___stop_5; }
	inline bool* get_address_of_stop_5() { return &___stop_5; }
	inline void set_stop_5(bool value)
	{
		___stop_5 = value;
	}

	inline static int32_t get_offset_of_blend_6() { return static_cast<int32_t>(offsetof(MoveCTRLDemo_t2262282185, ___blend_6)); }
	inline float get_blend_6() const { return ___blend_6; }
	inline float* get_address_of_blend_6() { return &___blend_6; }
	inline void set_blend_6(float value)
	{
		___blend_6 = value;
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(MoveCTRLDemo_t2262282185, ___delay_7)); }
	inline float get_delay_7() const { return ___delay_7; }
	inline float* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(float value)
	{
		___delay_7 = value;
	}

	inline static int32_t get_offset_of_AddRunSpeed_8() { return static_cast<int32_t>(offsetof(MoveCTRLDemo_t2262282185, ___AddRunSpeed_8)); }
	inline float get_AddRunSpeed_8() const { return ___AddRunSpeed_8; }
	inline float* get_address_of_AddRunSpeed_8() { return &___AddRunSpeed_8; }
	inline void set_AddRunSpeed_8(float value)
	{
		___AddRunSpeed_8 = value;
	}

	inline static int32_t get_offset_of_AddWalkSpeed_9() { return static_cast<int32_t>(offsetof(MoveCTRLDemo_t2262282185, ___AddWalkSpeed_9)); }
	inline float get_AddWalkSpeed_9() const { return ___AddWalkSpeed_9; }
	inline float* get_address_of_AddWalkSpeed_9() { return &___AddWalkSpeed_9; }
	inline void set_AddWalkSpeed_9(float value)
	{
		___AddWalkSpeed_9 = value;
	}

	inline static int32_t get_offset_of_hasAniComp_10() { return static_cast<int32_t>(offsetof(MoveCTRLDemo_t2262282185, ___hasAniComp_10)); }
	inline bool get_hasAniComp_10() const { return ___hasAniComp_10; }
	inline bool* get_address_of_hasAniComp_10() { return &___hasAniComp_10; }
	inline void set_hasAniComp_10(bool value)
	{
		___hasAniComp_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVECTRLDEMO_T2262282185_H
#ifndef PAUSEMENU_T3916167947_H
#define PAUSEMENU_T3916167947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseMenu
struct  PauseMenu_t3916167947  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Toggle PauseMenu::m_MenuToggle
	Toggle_t2735377061 * ___m_MenuToggle_4;
	// System.Single PauseMenu::m_TimeScaleRef
	float ___m_TimeScaleRef_5;
	// System.Single PauseMenu::m_VolumeRef
	float ___m_VolumeRef_6;
	// System.Boolean PauseMenu::m_Paused
	bool ___m_Paused_7;

public:
	inline static int32_t get_offset_of_m_MenuToggle_4() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___m_MenuToggle_4)); }
	inline Toggle_t2735377061 * get_m_MenuToggle_4() const { return ___m_MenuToggle_4; }
	inline Toggle_t2735377061 ** get_address_of_m_MenuToggle_4() { return &___m_MenuToggle_4; }
	inline void set_m_MenuToggle_4(Toggle_t2735377061 * value)
	{
		___m_MenuToggle_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MenuToggle_4), value);
	}

	inline static int32_t get_offset_of_m_TimeScaleRef_5() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___m_TimeScaleRef_5)); }
	inline float get_m_TimeScaleRef_5() const { return ___m_TimeScaleRef_5; }
	inline float* get_address_of_m_TimeScaleRef_5() { return &___m_TimeScaleRef_5; }
	inline void set_m_TimeScaleRef_5(float value)
	{
		___m_TimeScaleRef_5 = value;
	}

	inline static int32_t get_offset_of_m_VolumeRef_6() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___m_VolumeRef_6)); }
	inline float get_m_VolumeRef_6() const { return ___m_VolumeRef_6; }
	inline float* get_address_of_m_VolumeRef_6() { return &___m_VolumeRef_6; }
	inline void set_m_VolumeRef_6(float value)
	{
		___m_VolumeRef_6 = value;
	}

	inline static int32_t get_offset_of_m_Paused_7() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___m_Paused_7)); }
	inline bool get_m_Paused_7() const { return ___m_Paused_7; }
	inline bool* get_address_of_m_Paused_7() { return &___m_Paused_7; }
	inline void set_m_Paused_7(bool value)
	{
		___m_Paused_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEMENU_T3916167947_H
#ifndef RADIALANGLER_T2211509914_H
#define RADIALANGLER_T2211509914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadialAngler
struct  RadialAngler_t2211509914  : public MonoBehaviour_t3962482529
{
public:
	// System.Single RadialAngler::angle
	float ___angle_4;
	// System.Single RadialAngler::oldAngle
	float ___oldAngle_5;
	// System.Single RadialAngler::range
	float ___range_6;
	// System.Single RadialAngler::oldRange
	float ___oldRange_7;
	// UnityEngine.Transform RadialAngler::diameter
	Transform_t3600365921 * ___diameter_8;
	// UnityEngine.SkinnedMeshRenderer RadialAngler::helper
	SkinnedMeshRenderer_t245602842 * ___helper_9;

public:
	inline static int32_t get_offset_of_angle_4() { return static_cast<int32_t>(offsetof(RadialAngler_t2211509914, ___angle_4)); }
	inline float get_angle_4() const { return ___angle_4; }
	inline float* get_address_of_angle_4() { return &___angle_4; }
	inline void set_angle_4(float value)
	{
		___angle_4 = value;
	}

	inline static int32_t get_offset_of_oldAngle_5() { return static_cast<int32_t>(offsetof(RadialAngler_t2211509914, ___oldAngle_5)); }
	inline float get_oldAngle_5() const { return ___oldAngle_5; }
	inline float* get_address_of_oldAngle_5() { return &___oldAngle_5; }
	inline void set_oldAngle_5(float value)
	{
		___oldAngle_5 = value;
	}

	inline static int32_t get_offset_of_range_6() { return static_cast<int32_t>(offsetof(RadialAngler_t2211509914, ___range_6)); }
	inline float get_range_6() const { return ___range_6; }
	inline float* get_address_of_range_6() { return &___range_6; }
	inline void set_range_6(float value)
	{
		___range_6 = value;
	}

	inline static int32_t get_offset_of_oldRange_7() { return static_cast<int32_t>(offsetof(RadialAngler_t2211509914, ___oldRange_7)); }
	inline float get_oldRange_7() const { return ___oldRange_7; }
	inline float* get_address_of_oldRange_7() { return &___oldRange_7; }
	inline void set_oldRange_7(float value)
	{
		___oldRange_7 = value;
	}

	inline static int32_t get_offset_of_diameter_8() { return static_cast<int32_t>(offsetof(RadialAngler_t2211509914, ___diameter_8)); }
	inline Transform_t3600365921 * get_diameter_8() const { return ___diameter_8; }
	inline Transform_t3600365921 ** get_address_of_diameter_8() { return &___diameter_8; }
	inline void set_diameter_8(Transform_t3600365921 * value)
	{
		___diameter_8 = value;
		Il2CppCodeGenWriteBarrier((&___diameter_8), value);
	}

	inline static int32_t get_offset_of_helper_9() { return static_cast<int32_t>(offsetof(RadialAngler_t2211509914, ___helper_9)); }
	inline SkinnedMeshRenderer_t245602842 * get_helper_9() const { return ___helper_9; }
	inline SkinnedMeshRenderer_t245602842 ** get_address_of_helper_9() { return &___helper_9; }
	inline void set_helper_9(SkinnedMeshRenderer_t245602842 * value)
	{
		___helper_9 = value;
		Il2CppCodeGenWriteBarrier((&___helper_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALANGLER_T2211509914_H
#ifndef RAYSHOT_T3786369421_H
#define RAYSHOT_T3786369421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RayShot
struct  RayShot_t3786369421  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.LineRenderer RayShot::line
	LineRenderer_t3154350270 * ___line_4;
	// UnityEngine.Transform RayShot::rayEnd
	Transform_t3600365921 * ___rayEnd_5;
	// UnityEngine.Transform RayShot::hitReport
	Transform_t3600365921 * ___hitReport_6;
	// ASCLBasicController RayShot::abc
	ASCLBasicController_t2851843984 * ___abc_7;
	// Enemy[] RayShot::enemies
	EnemyU5BU5D_t2006687320* ___enemies_8;
	// System.Single RayShot::damage
	float ___damage_9;
	// System.Single RayShot::lifespan
	float ___lifespan_10;
	// System.Single RayShot::startTime
	float ___startTime_11;
	// UnityEngine.Vector3 RayShot::endPos
	Vector3_t3722313464  ___endPos_12;

public:
	inline static int32_t get_offset_of_line_4() { return static_cast<int32_t>(offsetof(RayShot_t3786369421, ___line_4)); }
	inline LineRenderer_t3154350270 * get_line_4() const { return ___line_4; }
	inline LineRenderer_t3154350270 ** get_address_of_line_4() { return &___line_4; }
	inline void set_line_4(LineRenderer_t3154350270 * value)
	{
		___line_4 = value;
		Il2CppCodeGenWriteBarrier((&___line_4), value);
	}

	inline static int32_t get_offset_of_rayEnd_5() { return static_cast<int32_t>(offsetof(RayShot_t3786369421, ___rayEnd_5)); }
	inline Transform_t3600365921 * get_rayEnd_5() const { return ___rayEnd_5; }
	inline Transform_t3600365921 ** get_address_of_rayEnd_5() { return &___rayEnd_5; }
	inline void set_rayEnd_5(Transform_t3600365921 * value)
	{
		___rayEnd_5 = value;
		Il2CppCodeGenWriteBarrier((&___rayEnd_5), value);
	}

	inline static int32_t get_offset_of_hitReport_6() { return static_cast<int32_t>(offsetof(RayShot_t3786369421, ___hitReport_6)); }
	inline Transform_t3600365921 * get_hitReport_6() const { return ___hitReport_6; }
	inline Transform_t3600365921 ** get_address_of_hitReport_6() { return &___hitReport_6; }
	inline void set_hitReport_6(Transform_t3600365921 * value)
	{
		___hitReport_6 = value;
		Il2CppCodeGenWriteBarrier((&___hitReport_6), value);
	}

	inline static int32_t get_offset_of_abc_7() { return static_cast<int32_t>(offsetof(RayShot_t3786369421, ___abc_7)); }
	inline ASCLBasicController_t2851843984 * get_abc_7() const { return ___abc_7; }
	inline ASCLBasicController_t2851843984 ** get_address_of_abc_7() { return &___abc_7; }
	inline void set_abc_7(ASCLBasicController_t2851843984 * value)
	{
		___abc_7 = value;
		Il2CppCodeGenWriteBarrier((&___abc_7), value);
	}

	inline static int32_t get_offset_of_enemies_8() { return static_cast<int32_t>(offsetof(RayShot_t3786369421, ___enemies_8)); }
	inline EnemyU5BU5D_t2006687320* get_enemies_8() const { return ___enemies_8; }
	inline EnemyU5BU5D_t2006687320** get_address_of_enemies_8() { return &___enemies_8; }
	inline void set_enemies_8(EnemyU5BU5D_t2006687320* value)
	{
		___enemies_8 = value;
		Il2CppCodeGenWriteBarrier((&___enemies_8), value);
	}

	inline static int32_t get_offset_of_damage_9() { return static_cast<int32_t>(offsetof(RayShot_t3786369421, ___damage_9)); }
	inline float get_damage_9() const { return ___damage_9; }
	inline float* get_address_of_damage_9() { return &___damage_9; }
	inline void set_damage_9(float value)
	{
		___damage_9 = value;
	}

	inline static int32_t get_offset_of_lifespan_10() { return static_cast<int32_t>(offsetof(RayShot_t3786369421, ___lifespan_10)); }
	inline float get_lifespan_10() const { return ___lifespan_10; }
	inline float* get_address_of_lifespan_10() { return &___lifespan_10; }
	inline void set_lifespan_10(float value)
	{
		___lifespan_10 = value;
	}

	inline static int32_t get_offset_of_startTime_11() { return static_cast<int32_t>(offsetof(RayShot_t3786369421, ___startTime_11)); }
	inline float get_startTime_11() const { return ___startTime_11; }
	inline float* get_address_of_startTime_11() { return &___startTime_11; }
	inline void set_startTime_11(float value)
	{
		___startTime_11 = value;
	}

	inline static int32_t get_offset_of_endPos_12() { return static_cast<int32_t>(offsetof(RayShot_t3786369421, ___endPos_12)); }
	inline Vector3_t3722313464  get_endPos_12() const { return ___endPos_12; }
	inline Vector3_t3722313464 * get_address_of_endPos_12() { return &___endPos_12; }
	inline void set_endPos_12(Vector3_t3722313464  value)
	{
		___endPos_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYSHOT_T3786369421_H
#ifndef SCENEANDURLLOADER_T3512401881_H
#define SCENEANDURLLOADER_T3512401881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneAndURLLoader
struct  SceneAndURLLoader_t3512401881  : public MonoBehaviour_t3962482529
{
public:
	// PauseMenu SceneAndURLLoader::m_PauseMenu
	PauseMenu_t3916167947 * ___m_PauseMenu_4;

public:
	inline static int32_t get_offset_of_m_PauseMenu_4() { return static_cast<int32_t>(offsetof(SceneAndURLLoader_t3512401881, ___m_PauseMenu_4)); }
	inline PauseMenu_t3916167947 * get_m_PauseMenu_4() const { return ___m_PauseMenu_4; }
	inline PauseMenu_t3916167947 ** get_address_of_m_PauseMenu_4() { return &___m_PauseMenu_4; }
	inline void set_m_PauseMenu_4(PauseMenu_t3916167947 * value)
	{
		___m_PauseMenu_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PauseMenu_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEANDURLLOADER_T3512401881_H
#ifndef SIMPLESTATEMACHINE_T1535007871_H
#define SIMPLESTATEMACHINE_T1535007871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleStateMachine
struct  SimpleStateMachine_t1535007871  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SimpleStateMachine::DebugGui
	bool ___DebugGui_4;
	// UnityEngine.Vector2 SimpleStateMachine::DebugGuiPosition
	Vector2_t2156229523  ___DebugGuiPosition_5;
	// System.String SimpleStateMachine::DebugGuiTitle
	String_t* ___DebugGuiTitle_6;
	// System.Enum SimpleStateMachine::queueCommand
	Enum_t4135868527 * ___queueCommand_7;
	// System.Single SimpleStateMachine::timeEnteredState
	float ___timeEnteredState_8;
	// SimpleStateMachine/State SimpleStateMachine::state
	State_t3920417347 * ___state_9;
	// System.Enum SimpleStateMachine::lastState
	Enum_t4135868527 * ___lastState_10;
	// System.Collections.Generic.Dictionary`2<System.Enum,System.Collections.Generic.Dictionary`2<System.String,System.Delegate>> SimpleStateMachine::_cache
	Dictionary_2_t74011037 * ____cache_11;

public:
	inline static int32_t get_offset_of_DebugGui_4() { return static_cast<int32_t>(offsetof(SimpleStateMachine_t1535007871, ___DebugGui_4)); }
	inline bool get_DebugGui_4() const { return ___DebugGui_4; }
	inline bool* get_address_of_DebugGui_4() { return &___DebugGui_4; }
	inline void set_DebugGui_4(bool value)
	{
		___DebugGui_4 = value;
	}

	inline static int32_t get_offset_of_DebugGuiPosition_5() { return static_cast<int32_t>(offsetof(SimpleStateMachine_t1535007871, ___DebugGuiPosition_5)); }
	inline Vector2_t2156229523  get_DebugGuiPosition_5() const { return ___DebugGuiPosition_5; }
	inline Vector2_t2156229523 * get_address_of_DebugGuiPosition_5() { return &___DebugGuiPosition_5; }
	inline void set_DebugGuiPosition_5(Vector2_t2156229523  value)
	{
		___DebugGuiPosition_5 = value;
	}

	inline static int32_t get_offset_of_DebugGuiTitle_6() { return static_cast<int32_t>(offsetof(SimpleStateMachine_t1535007871, ___DebugGuiTitle_6)); }
	inline String_t* get_DebugGuiTitle_6() const { return ___DebugGuiTitle_6; }
	inline String_t** get_address_of_DebugGuiTitle_6() { return &___DebugGuiTitle_6; }
	inline void set_DebugGuiTitle_6(String_t* value)
	{
		___DebugGuiTitle_6 = value;
		Il2CppCodeGenWriteBarrier((&___DebugGuiTitle_6), value);
	}

	inline static int32_t get_offset_of_queueCommand_7() { return static_cast<int32_t>(offsetof(SimpleStateMachine_t1535007871, ___queueCommand_7)); }
	inline Enum_t4135868527 * get_queueCommand_7() const { return ___queueCommand_7; }
	inline Enum_t4135868527 ** get_address_of_queueCommand_7() { return &___queueCommand_7; }
	inline void set_queueCommand_7(Enum_t4135868527 * value)
	{
		___queueCommand_7 = value;
		Il2CppCodeGenWriteBarrier((&___queueCommand_7), value);
	}

	inline static int32_t get_offset_of_timeEnteredState_8() { return static_cast<int32_t>(offsetof(SimpleStateMachine_t1535007871, ___timeEnteredState_8)); }
	inline float get_timeEnteredState_8() const { return ___timeEnteredState_8; }
	inline float* get_address_of_timeEnteredState_8() { return &___timeEnteredState_8; }
	inline void set_timeEnteredState_8(float value)
	{
		___timeEnteredState_8 = value;
	}

	inline static int32_t get_offset_of_state_9() { return static_cast<int32_t>(offsetof(SimpleStateMachine_t1535007871, ___state_9)); }
	inline State_t3920417347 * get_state_9() const { return ___state_9; }
	inline State_t3920417347 ** get_address_of_state_9() { return &___state_9; }
	inline void set_state_9(State_t3920417347 * value)
	{
		___state_9 = value;
		Il2CppCodeGenWriteBarrier((&___state_9), value);
	}

	inline static int32_t get_offset_of_lastState_10() { return static_cast<int32_t>(offsetof(SimpleStateMachine_t1535007871, ___lastState_10)); }
	inline Enum_t4135868527 * get_lastState_10() const { return ___lastState_10; }
	inline Enum_t4135868527 ** get_address_of_lastState_10() { return &___lastState_10; }
	inline void set_lastState_10(Enum_t4135868527 * value)
	{
		___lastState_10 = value;
		Il2CppCodeGenWriteBarrier((&___lastState_10), value);
	}

	inline static int32_t get_offset_of__cache_11() { return static_cast<int32_t>(offsetof(SimpleStateMachine_t1535007871, ____cache_11)); }
	inline Dictionary_2_t74011037 * get__cache_11() const { return ____cache_11; }
	inline Dictionary_2_t74011037 ** get_address_of__cache_11() { return &____cache_11; }
	inline void set__cache_11(Dictionary_2_t74011037 * value)
	{
		____cache_11 = value;
		Il2CppCodeGenWriteBarrier((&____cache_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESTATEMACHINE_T1535007871_H
#ifndef SUPERCHARACTERCONTROLLER_T3568978942_H
#define SUPERCHARACTERCONTROLLER_T3568978942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperCharacterController
struct  SuperCharacterController_t3568978942  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 SuperCharacterController::debugMove
	Vector3_t3722313464  ___debugMove_4;
	// UnityEngine.QueryTriggerInteraction SuperCharacterController::triggerInteraction
	int32_t ___triggerInteraction_5;
	// System.Boolean SuperCharacterController::fixedTimeStep
	bool ___fixedTimeStep_6;
	// System.Int32 SuperCharacterController::fixedUpdatesPerSecond
	int32_t ___fixedUpdatesPerSecond_7;
	// System.Boolean SuperCharacterController::clampToMovingGround
	bool ___clampToMovingGround_8;
	// System.Boolean SuperCharacterController::debugSpheres
	bool ___debugSpheres_9;
	// System.Boolean SuperCharacterController::debugGrounding
	bool ___debugGrounding_10;
	// System.Boolean SuperCharacterController::debugPushbackMesssages
	bool ___debugPushbackMesssages_11;
	// CollisionSphere[] SuperCharacterController::spheres
	CollisionSphereU5BU5D_t1317006998* ___spheres_12;
	// UnityEngine.LayerMask SuperCharacterController::Walkable
	LayerMask_t3493934918  ___Walkable_13;
	// UnityEngine.Collider SuperCharacterController::ownCollider
	Collider_t1773347010 * ___ownCollider_14;
	// System.Single SuperCharacterController::radius
	float ___radius_15;
	// System.Single SuperCharacterController::<deltaTime>k__BackingField
	float ___U3CdeltaTimeU3Ek__BackingField_16;
	// SuperCharacterController/SuperGround SuperCharacterController::<currentGround>k__BackingField
	SuperGround_t3112734080 * ___U3CcurrentGroundU3Ek__BackingField_17;
	// CollisionSphere SuperCharacterController::<feet>k__BackingField
	CollisionSphere_t3916568399 * ___U3CfeetU3Ek__BackingField_18;
	// CollisionSphere SuperCharacterController::<head>k__BackingField
	CollisionSphere_t3916568399 * ___U3CheadU3Ek__BackingField_19;
	// System.Collections.Generic.List`1<SuperCollision> SuperCharacterController::<collisionData>k__BackingField
	List_1_t3251629602 * ___U3CcollisionDataU3Ek__BackingField_20;
	// UnityEngine.Transform SuperCharacterController::<currentlyClampedTo>k__BackingField
	Transform_t3600365921 * ___U3CcurrentlyClampedToU3Ek__BackingField_21;
	// System.Single SuperCharacterController::<heightScale>k__BackingField
	float ___U3CheightScaleU3Ek__BackingField_22;
	// System.Single SuperCharacterController::<radiusScale>k__BackingField
	float ___U3CradiusScaleU3Ek__BackingField_23;
	// System.Boolean SuperCharacterController::<manualUpdateOnly>k__BackingField
	bool ___U3CmanualUpdateOnlyU3Ek__BackingField_24;
	// SuperCharacterController/UpdateDelegate SuperCharacterController::AfterSingleUpdate
	UpdateDelegate_t1636609007 * ___AfterSingleUpdate_25;
	// UnityEngine.Vector3 SuperCharacterController::initialPosition
	Vector3_t3722313464  ___initialPosition_26;
	// UnityEngine.Vector3 SuperCharacterController::groundOffset
	Vector3_t3722313464  ___groundOffset_27;
	// UnityEngine.Vector3 SuperCharacterController::lastGroundPosition
	Vector3_t3722313464  ___lastGroundPosition_28;
	// System.Boolean SuperCharacterController::clamping
	bool ___clamping_29;
	// System.Boolean SuperCharacterController::slopeLimiting
	bool ___slopeLimiting_30;
	// System.Collections.Generic.List`1<UnityEngine.Collider> SuperCharacterController::ignoredColliders
	List_1_t3245421752 * ___ignoredColliders_31;
	// System.Collections.Generic.List`1<SuperCharacterController/IgnoredCollider> SuperCharacterController::ignoredColliderStack
	List_1_t2120843455 * ___ignoredColliderStack_32;
	// System.Int32 SuperCharacterController::TemporaryLayerIndex
	int32_t ___TemporaryLayerIndex_37;
	// System.Single SuperCharacterController::fixedDeltaTime
	float ___fixedDeltaTime_38;

public:
	inline static int32_t get_offset_of_debugMove_4() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___debugMove_4)); }
	inline Vector3_t3722313464  get_debugMove_4() const { return ___debugMove_4; }
	inline Vector3_t3722313464 * get_address_of_debugMove_4() { return &___debugMove_4; }
	inline void set_debugMove_4(Vector3_t3722313464  value)
	{
		___debugMove_4 = value;
	}

	inline static int32_t get_offset_of_triggerInteraction_5() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___triggerInteraction_5)); }
	inline int32_t get_triggerInteraction_5() const { return ___triggerInteraction_5; }
	inline int32_t* get_address_of_triggerInteraction_5() { return &___triggerInteraction_5; }
	inline void set_triggerInteraction_5(int32_t value)
	{
		___triggerInteraction_5 = value;
	}

	inline static int32_t get_offset_of_fixedTimeStep_6() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___fixedTimeStep_6)); }
	inline bool get_fixedTimeStep_6() const { return ___fixedTimeStep_6; }
	inline bool* get_address_of_fixedTimeStep_6() { return &___fixedTimeStep_6; }
	inline void set_fixedTimeStep_6(bool value)
	{
		___fixedTimeStep_6 = value;
	}

	inline static int32_t get_offset_of_fixedUpdatesPerSecond_7() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___fixedUpdatesPerSecond_7)); }
	inline int32_t get_fixedUpdatesPerSecond_7() const { return ___fixedUpdatesPerSecond_7; }
	inline int32_t* get_address_of_fixedUpdatesPerSecond_7() { return &___fixedUpdatesPerSecond_7; }
	inline void set_fixedUpdatesPerSecond_7(int32_t value)
	{
		___fixedUpdatesPerSecond_7 = value;
	}

	inline static int32_t get_offset_of_clampToMovingGround_8() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___clampToMovingGround_8)); }
	inline bool get_clampToMovingGround_8() const { return ___clampToMovingGround_8; }
	inline bool* get_address_of_clampToMovingGround_8() { return &___clampToMovingGround_8; }
	inline void set_clampToMovingGround_8(bool value)
	{
		___clampToMovingGround_8 = value;
	}

	inline static int32_t get_offset_of_debugSpheres_9() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___debugSpheres_9)); }
	inline bool get_debugSpheres_9() const { return ___debugSpheres_9; }
	inline bool* get_address_of_debugSpheres_9() { return &___debugSpheres_9; }
	inline void set_debugSpheres_9(bool value)
	{
		___debugSpheres_9 = value;
	}

	inline static int32_t get_offset_of_debugGrounding_10() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___debugGrounding_10)); }
	inline bool get_debugGrounding_10() const { return ___debugGrounding_10; }
	inline bool* get_address_of_debugGrounding_10() { return &___debugGrounding_10; }
	inline void set_debugGrounding_10(bool value)
	{
		___debugGrounding_10 = value;
	}

	inline static int32_t get_offset_of_debugPushbackMesssages_11() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___debugPushbackMesssages_11)); }
	inline bool get_debugPushbackMesssages_11() const { return ___debugPushbackMesssages_11; }
	inline bool* get_address_of_debugPushbackMesssages_11() { return &___debugPushbackMesssages_11; }
	inline void set_debugPushbackMesssages_11(bool value)
	{
		___debugPushbackMesssages_11 = value;
	}

	inline static int32_t get_offset_of_spheres_12() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___spheres_12)); }
	inline CollisionSphereU5BU5D_t1317006998* get_spheres_12() const { return ___spheres_12; }
	inline CollisionSphereU5BU5D_t1317006998** get_address_of_spheres_12() { return &___spheres_12; }
	inline void set_spheres_12(CollisionSphereU5BU5D_t1317006998* value)
	{
		___spheres_12 = value;
		Il2CppCodeGenWriteBarrier((&___spheres_12), value);
	}

	inline static int32_t get_offset_of_Walkable_13() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___Walkable_13)); }
	inline LayerMask_t3493934918  get_Walkable_13() const { return ___Walkable_13; }
	inline LayerMask_t3493934918 * get_address_of_Walkable_13() { return &___Walkable_13; }
	inline void set_Walkable_13(LayerMask_t3493934918  value)
	{
		___Walkable_13 = value;
	}

	inline static int32_t get_offset_of_ownCollider_14() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___ownCollider_14)); }
	inline Collider_t1773347010 * get_ownCollider_14() const { return ___ownCollider_14; }
	inline Collider_t1773347010 ** get_address_of_ownCollider_14() { return &___ownCollider_14; }
	inline void set_ownCollider_14(Collider_t1773347010 * value)
	{
		___ownCollider_14 = value;
		Il2CppCodeGenWriteBarrier((&___ownCollider_14), value);
	}

	inline static int32_t get_offset_of_radius_15() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___radius_15)); }
	inline float get_radius_15() const { return ___radius_15; }
	inline float* get_address_of_radius_15() { return &___radius_15; }
	inline void set_radius_15(float value)
	{
		___radius_15 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaTimeU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___U3CdeltaTimeU3Ek__BackingField_16)); }
	inline float get_U3CdeltaTimeU3Ek__BackingField_16() const { return ___U3CdeltaTimeU3Ek__BackingField_16; }
	inline float* get_address_of_U3CdeltaTimeU3Ek__BackingField_16() { return &___U3CdeltaTimeU3Ek__BackingField_16; }
	inline void set_U3CdeltaTimeU3Ek__BackingField_16(float value)
	{
		___U3CdeltaTimeU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentGroundU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___U3CcurrentGroundU3Ek__BackingField_17)); }
	inline SuperGround_t3112734080 * get_U3CcurrentGroundU3Ek__BackingField_17() const { return ___U3CcurrentGroundU3Ek__BackingField_17; }
	inline SuperGround_t3112734080 ** get_address_of_U3CcurrentGroundU3Ek__BackingField_17() { return &___U3CcurrentGroundU3Ek__BackingField_17; }
	inline void set_U3CcurrentGroundU3Ek__BackingField_17(SuperGround_t3112734080 * value)
	{
		___U3CcurrentGroundU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentGroundU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CfeetU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___U3CfeetU3Ek__BackingField_18)); }
	inline CollisionSphere_t3916568399 * get_U3CfeetU3Ek__BackingField_18() const { return ___U3CfeetU3Ek__BackingField_18; }
	inline CollisionSphere_t3916568399 ** get_address_of_U3CfeetU3Ek__BackingField_18() { return &___U3CfeetU3Ek__BackingField_18; }
	inline void set_U3CfeetU3Ek__BackingField_18(CollisionSphere_t3916568399 * value)
	{
		___U3CfeetU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfeetU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CheadU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___U3CheadU3Ek__BackingField_19)); }
	inline CollisionSphere_t3916568399 * get_U3CheadU3Ek__BackingField_19() const { return ___U3CheadU3Ek__BackingField_19; }
	inline CollisionSphere_t3916568399 ** get_address_of_U3CheadU3Ek__BackingField_19() { return &___U3CheadU3Ek__BackingField_19; }
	inline void set_U3CheadU3Ek__BackingField_19(CollisionSphere_t3916568399 * value)
	{
		___U3CheadU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CheadU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CcollisionDataU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___U3CcollisionDataU3Ek__BackingField_20)); }
	inline List_1_t3251629602 * get_U3CcollisionDataU3Ek__BackingField_20() const { return ___U3CcollisionDataU3Ek__BackingField_20; }
	inline List_1_t3251629602 ** get_address_of_U3CcollisionDataU3Ek__BackingField_20() { return &___U3CcollisionDataU3Ek__BackingField_20; }
	inline void set_U3CcollisionDataU3Ek__BackingField_20(List_1_t3251629602 * value)
	{
		___U3CcollisionDataU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcollisionDataU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CcurrentlyClampedToU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___U3CcurrentlyClampedToU3Ek__BackingField_21)); }
	inline Transform_t3600365921 * get_U3CcurrentlyClampedToU3Ek__BackingField_21() const { return ___U3CcurrentlyClampedToU3Ek__BackingField_21; }
	inline Transform_t3600365921 ** get_address_of_U3CcurrentlyClampedToU3Ek__BackingField_21() { return &___U3CcurrentlyClampedToU3Ek__BackingField_21; }
	inline void set_U3CcurrentlyClampedToU3Ek__BackingField_21(Transform_t3600365921 * value)
	{
		___U3CcurrentlyClampedToU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentlyClampedToU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CheightScaleU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___U3CheightScaleU3Ek__BackingField_22)); }
	inline float get_U3CheightScaleU3Ek__BackingField_22() const { return ___U3CheightScaleU3Ek__BackingField_22; }
	inline float* get_address_of_U3CheightScaleU3Ek__BackingField_22() { return &___U3CheightScaleU3Ek__BackingField_22; }
	inline void set_U3CheightScaleU3Ek__BackingField_22(float value)
	{
		___U3CheightScaleU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CradiusScaleU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___U3CradiusScaleU3Ek__BackingField_23)); }
	inline float get_U3CradiusScaleU3Ek__BackingField_23() const { return ___U3CradiusScaleU3Ek__BackingField_23; }
	inline float* get_address_of_U3CradiusScaleU3Ek__BackingField_23() { return &___U3CradiusScaleU3Ek__BackingField_23; }
	inline void set_U3CradiusScaleU3Ek__BackingField_23(float value)
	{
		___U3CradiusScaleU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CmanualUpdateOnlyU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___U3CmanualUpdateOnlyU3Ek__BackingField_24)); }
	inline bool get_U3CmanualUpdateOnlyU3Ek__BackingField_24() const { return ___U3CmanualUpdateOnlyU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CmanualUpdateOnlyU3Ek__BackingField_24() { return &___U3CmanualUpdateOnlyU3Ek__BackingField_24; }
	inline void set_U3CmanualUpdateOnlyU3Ek__BackingField_24(bool value)
	{
		___U3CmanualUpdateOnlyU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_AfterSingleUpdate_25() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___AfterSingleUpdate_25)); }
	inline UpdateDelegate_t1636609007 * get_AfterSingleUpdate_25() const { return ___AfterSingleUpdate_25; }
	inline UpdateDelegate_t1636609007 ** get_address_of_AfterSingleUpdate_25() { return &___AfterSingleUpdate_25; }
	inline void set_AfterSingleUpdate_25(UpdateDelegate_t1636609007 * value)
	{
		___AfterSingleUpdate_25 = value;
		Il2CppCodeGenWriteBarrier((&___AfterSingleUpdate_25), value);
	}

	inline static int32_t get_offset_of_initialPosition_26() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___initialPosition_26)); }
	inline Vector3_t3722313464  get_initialPosition_26() const { return ___initialPosition_26; }
	inline Vector3_t3722313464 * get_address_of_initialPosition_26() { return &___initialPosition_26; }
	inline void set_initialPosition_26(Vector3_t3722313464  value)
	{
		___initialPosition_26 = value;
	}

	inline static int32_t get_offset_of_groundOffset_27() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___groundOffset_27)); }
	inline Vector3_t3722313464  get_groundOffset_27() const { return ___groundOffset_27; }
	inline Vector3_t3722313464 * get_address_of_groundOffset_27() { return &___groundOffset_27; }
	inline void set_groundOffset_27(Vector3_t3722313464  value)
	{
		___groundOffset_27 = value;
	}

	inline static int32_t get_offset_of_lastGroundPosition_28() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___lastGroundPosition_28)); }
	inline Vector3_t3722313464  get_lastGroundPosition_28() const { return ___lastGroundPosition_28; }
	inline Vector3_t3722313464 * get_address_of_lastGroundPosition_28() { return &___lastGroundPosition_28; }
	inline void set_lastGroundPosition_28(Vector3_t3722313464  value)
	{
		___lastGroundPosition_28 = value;
	}

	inline static int32_t get_offset_of_clamping_29() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___clamping_29)); }
	inline bool get_clamping_29() const { return ___clamping_29; }
	inline bool* get_address_of_clamping_29() { return &___clamping_29; }
	inline void set_clamping_29(bool value)
	{
		___clamping_29 = value;
	}

	inline static int32_t get_offset_of_slopeLimiting_30() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___slopeLimiting_30)); }
	inline bool get_slopeLimiting_30() const { return ___slopeLimiting_30; }
	inline bool* get_address_of_slopeLimiting_30() { return &___slopeLimiting_30; }
	inline void set_slopeLimiting_30(bool value)
	{
		___slopeLimiting_30 = value;
	}

	inline static int32_t get_offset_of_ignoredColliders_31() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___ignoredColliders_31)); }
	inline List_1_t3245421752 * get_ignoredColliders_31() const { return ___ignoredColliders_31; }
	inline List_1_t3245421752 ** get_address_of_ignoredColliders_31() { return &___ignoredColliders_31; }
	inline void set_ignoredColliders_31(List_1_t3245421752 * value)
	{
		___ignoredColliders_31 = value;
		Il2CppCodeGenWriteBarrier((&___ignoredColliders_31), value);
	}

	inline static int32_t get_offset_of_ignoredColliderStack_32() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___ignoredColliderStack_32)); }
	inline List_1_t2120843455 * get_ignoredColliderStack_32() const { return ___ignoredColliderStack_32; }
	inline List_1_t2120843455 ** get_address_of_ignoredColliderStack_32() { return &___ignoredColliderStack_32; }
	inline void set_ignoredColliderStack_32(List_1_t2120843455 * value)
	{
		___ignoredColliderStack_32 = value;
		Il2CppCodeGenWriteBarrier((&___ignoredColliderStack_32), value);
	}

	inline static int32_t get_offset_of_TemporaryLayerIndex_37() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___TemporaryLayerIndex_37)); }
	inline int32_t get_TemporaryLayerIndex_37() const { return ___TemporaryLayerIndex_37; }
	inline int32_t* get_address_of_TemporaryLayerIndex_37() { return &___TemporaryLayerIndex_37; }
	inline void set_TemporaryLayerIndex_37(int32_t value)
	{
		___TemporaryLayerIndex_37 = value;
	}

	inline static int32_t get_offset_of_fixedDeltaTime_38() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942, ___fixedDeltaTime_38)); }
	inline float get_fixedDeltaTime_38() const { return ___fixedDeltaTime_38; }
	inline float* get_address_of_fixedDeltaTime_38() { return &___fixedDeltaTime_38; }
	inline void set_fixedDeltaTime_38(float value)
	{
		___fixedDeltaTime_38 = value;
	}
};

struct SuperCharacterController_t3568978942_StaticFields
{
public:
	// SuperCollisionType SuperCharacterController::defaultCollisionType
	SuperCollisionType_t119653226 * ___defaultCollisionType_39;

public:
	inline static int32_t get_offset_of_defaultCollisionType_39() { return static_cast<int32_t>(offsetof(SuperCharacterController_t3568978942_StaticFields, ___defaultCollisionType_39)); }
	inline SuperCollisionType_t119653226 * get_defaultCollisionType_39() const { return ___defaultCollisionType_39; }
	inline SuperCollisionType_t119653226 ** get_address_of_defaultCollisionType_39() { return &___defaultCollisionType_39; }
	inline void set_defaultCollisionType_39(SuperCollisionType_t119653226 * value)
	{
		___defaultCollisionType_39 = value;
		Il2CppCodeGenWriteBarrier((&___defaultCollisionType_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPERCHARACTERCONTROLLER_T3568978942_H
#ifndef SUPERCOLLISIONTYPE_T119653226_H
#define SUPERCOLLISIONTYPE_T119653226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperCollisionType
struct  SuperCollisionType_t119653226  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SuperCollisionType::StandAngle
	float ___StandAngle_4;
	// System.Single SuperCollisionType::SlopeLimit
	float ___SlopeLimit_5;

public:
	inline static int32_t get_offset_of_StandAngle_4() { return static_cast<int32_t>(offsetof(SuperCollisionType_t119653226, ___StandAngle_4)); }
	inline float get_StandAngle_4() const { return ___StandAngle_4; }
	inline float* get_address_of_StandAngle_4() { return &___StandAngle_4; }
	inline void set_StandAngle_4(float value)
	{
		___StandAngle_4 = value;
	}

	inline static int32_t get_offset_of_SlopeLimit_5() { return static_cast<int32_t>(offsetof(SuperCollisionType_t119653226, ___SlopeLimit_5)); }
	inline float get_SlopeLimit_5() const { return ___SlopeLimit_5; }
	inline float* get_address_of_SlopeLimit_5() { return &___SlopeLimit_5; }
	inline void set_SlopeLimit_5(float value)
	{
		___SlopeLimit_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPERCOLLISIONTYPE_T119653226_H
#ifndef SUPERSTATEMACHINE_T3068594726_H
#define SUPERSTATEMACHINE_T3068594726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperStateMachine
struct  SuperStateMachine_t3068594726  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SuperStateMachine::timeEnteredState
	float ___timeEnteredState_4;
	// SuperStateMachine/State SuperStateMachine::state
	State_t3931175374 * ___state_5;
	// System.Enum SuperStateMachine::lastState
	Enum_t4135868527 * ___lastState_6;
	// System.Collections.Generic.Dictionary`2<System.Enum,System.Collections.Generic.Dictionary`2<System.String,System.Delegate>> SuperStateMachine::_cache
	Dictionary_2_t74011037 * ____cache_7;

public:
	inline static int32_t get_offset_of_timeEnteredState_4() { return static_cast<int32_t>(offsetof(SuperStateMachine_t3068594726, ___timeEnteredState_4)); }
	inline float get_timeEnteredState_4() const { return ___timeEnteredState_4; }
	inline float* get_address_of_timeEnteredState_4() { return &___timeEnteredState_4; }
	inline void set_timeEnteredState_4(float value)
	{
		___timeEnteredState_4 = value;
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(SuperStateMachine_t3068594726, ___state_5)); }
	inline State_t3931175374 * get_state_5() const { return ___state_5; }
	inline State_t3931175374 ** get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(State_t3931175374 * value)
	{
		___state_5 = value;
		Il2CppCodeGenWriteBarrier((&___state_5), value);
	}

	inline static int32_t get_offset_of_lastState_6() { return static_cast<int32_t>(offsetof(SuperStateMachine_t3068594726, ___lastState_6)); }
	inline Enum_t4135868527 * get_lastState_6() const { return ___lastState_6; }
	inline Enum_t4135868527 ** get_address_of_lastState_6() { return &___lastState_6; }
	inline void set_lastState_6(Enum_t4135868527 * value)
	{
		___lastState_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastState_6), value);
	}

	inline static int32_t get_offset_of__cache_7() { return static_cast<int32_t>(offsetof(SuperStateMachine_t3068594726, ____cache_7)); }
	inline Dictionary_2_t74011037 * get__cache_7() const { return ____cache_7; }
	inline Dictionary_2_t74011037 ** get_address_of__cache_7() { return &____cache_7; }
	inline void set__cache_7(Dictionary_2_t74011037 * value)
	{
		____cache_7 = value;
		Il2CppCodeGenWriteBarrier((&____cache_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPERSTATEMACHINE_T3068594726_H
#ifndef SWITCHVIEW_T3536945271_H
#define SWITCHVIEW_T3536945271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwitchView
struct  SwitchView_t3536945271  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera SwitchView::mainCam
	Camera_t4157153871 * ___mainCam_4;
	// UnityEngine.Camera SwitchView::subCam01
	Camera_t4157153871 * ___subCam01_5;
	// UnityEngine.Camera SwitchView::subCam02
	Camera_t4157153871 * ___subCam02_6;
	// UnityEngine.Vector2 SwitchView::pos
	Vector2_t2156229523  ___pos_7;
	// UnityEngine.Vector2 SwitchView::size
	Vector2_t2156229523  ___size_8;

public:
	inline static int32_t get_offset_of_mainCam_4() { return static_cast<int32_t>(offsetof(SwitchView_t3536945271, ___mainCam_4)); }
	inline Camera_t4157153871 * get_mainCam_4() const { return ___mainCam_4; }
	inline Camera_t4157153871 ** get_address_of_mainCam_4() { return &___mainCam_4; }
	inline void set_mainCam_4(Camera_t4157153871 * value)
	{
		___mainCam_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainCam_4), value);
	}

	inline static int32_t get_offset_of_subCam01_5() { return static_cast<int32_t>(offsetof(SwitchView_t3536945271, ___subCam01_5)); }
	inline Camera_t4157153871 * get_subCam01_5() const { return ___subCam01_5; }
	inline Camera_t4157153871 ** get_address_of_subCam01_5() { return &___subCam01_5; }
	inline void set_subCam01_5(Camera_t4157153871 * value)
	{
		___subCam01_5 = value;
		Il2CppCodeGenWriteBarrier((&___subCam01_5), value);
	}

	inline static int32_t get_offset_of_subCam02_6() { return static_cast<int32_t>(offsetof(SwitchView_t3536945271, ___subCam02_6)); }
	inline Camera_t4157153871 * get_subCam02_6() const { return ___subCam02_6; }
	inline Camera_t4157153871 ** get_address_of_subCam02_6() { return &___subCam02_6; }
	inline void set_subCam02_6(Camera_t4157153871 * value)
	{
		___subCam02_6 = value;
		Il2CppCodeGenWriteBarrier((&___subCam02_6), value);
	}

	inline static int32_t get_offset_of_pos_7() { return static_cast<int32_t>(offsetof(SwitchView_t3536945271, ___pos_7)); }
	inline Vector2_t2156229523  get_pos_7() const { return ___pos_7; }
	inline Vector2_t2156229523 * get_address_of_pos_7() { return &___pos_7; }
	inline void set_pos_7(Vector2_t2156229523  value)
	{
		___pos_7 = value;
	}

	inline static int32_t get_offset_of_size_8() { return static_cast<int32_t>(offsetof(SwitchView_t3536945271, ___size_8)); }
	inline Vector2_t2156229523  get_size_8() const { return ___size_8; }
	inline Vector2_t2156229523 * get_address_of_size_8() { return &___size_8; }
	inline void set_size_8(Vector2_t2156229523  value)
	{
		___size_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHVIEW_T3536945271_H
#ifndef KILLPARTICLE_T3813571932_H
#define KILLPARTICLE_T3813571932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// killParticle
struct  killParticle_t3813571932  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.ParticleEmitter killParticle::pe
	ParticleEmitter_t3771232109 * ___pe_4;
	// System.Single killParticle::lifespan
	float ___lifespan_5;
	// System.Single killParticle::startTime
	float ___startTime_6;

public:
	inline static int32_t get_offset_of_pe_4() { return static_cast<int32_t>(offsetof(killParticle_t3813571932, ___pe_4)); }
	inline ParticleEmitter_t3771232109 * get_pe_4() const { return ___pe_4; }
	inline ParticleEmitter_t3771232109 ** get_address_of_pe_4() { return &___pe_4; }
	inline void set_pe_4(ParticleEmitter_t3771232109 * value)
	{
		___pe_4 = value;
		Il2CppCodeGenWriteBarrier((&___pe_4), value);
	}

	inline static int32_t get_offset_of_lifespan_5() { return static_cast<int32_t>(offsetof(killParticle_t3813571932, ___lifespan_5)); }
	inline float get_lifespan_5() const { return ___lifespan_5; }
	inline float* get_address_of_lifespan_5() { return &___lifespan_5; }
	inline void set_lifespan_5(float value)
	{
		___lifespan_5 = value;
	}

	inline static int32_t get_offset_of_startTime_6() { return static_cast<int32_t>(offsetof(killParticle_t3813571932, ___startTime_6)); }
	inline float get_startTime_6() const { return ___startTime_6; }
	inline float* get_address_of_startTime_6() { return &___startTime_6; }
	inline void set_startTime_6(float value)
	{
		___startTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KILLPARTICLE_T3813571932_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4200 = { sizeof (CP858_t2383198942), -1, sizeof(CP858_t2383198942_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4200[1] = 
{
	CP858_t2383198942_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4201 = { sizeof (ENCibm00858_t2458074995), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4202 = { sizeof (CP862_t864234704), -1, sizeof(CP862_t864234704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4202[1] = 
{
	CP862_t864234704_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4203 = { sizeof (ENCibm862_t2537577824), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4204 = { sizeof (CP864_t57665650), -1, sizeof(CP864_t57665650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4204[1] = 
{
	CP864_t57665650_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4205 = { sizeof (ENCibm864_t3344146878), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4206 = { sizeof (CP866_t3189833532), -1, sizeof(CP866_t3189833532_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4206[1] = 
{
	CP866_t3189833532_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4207 = { sizeof (ENCibm866_t211978996), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4208 = { sizeof (CP869_t3949348419), -1, sizeof(CP869_t3949348419_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4208[1] = 
{
	CP869_t3949348419_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4209 = { sizeof (ENCibm869_t3747431405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4210 = { sizeof (CP870_t2027099654), -1, sizeof(CP870_t2027099654_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4210[1] = 
{
	CP870_t2027099654_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4211 = { sizeof (ENCibm870_t1374843946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4212 = { sizeof (CP875_t1623815127), -1, sizeof(CP875_t1623815127_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4212[1] = 
{
	CP875_t1623815127_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4213 = { sizeof (ENCibm875_t1778128473), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4214 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255371), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4214[37] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U305B1E1067273E188A50BA4342B4BC09ABEE669CD_0(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U309885C8B0840E863B3A14261999895D896677D5A_1(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U30BBB1FA6CCD88EF7F8B73A4A7AF064FE4A421486_2(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U3271EF3D1C1F8136E08DBCAB443CE02538A4156F0_3(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U32E1C78D5F4666324672DFACDC9307935CF14E98F_4(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U33551F16F8A11003B8289B76B9E3D97969B68E7D9_5(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U336A0A1CE2E522AB1D22668B38F6126D2F6E17D44_6(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U337C03949C69BDDA042205E1A573B349E75F693B8_7(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U347DCC01E928630EBCC018C5E0285145985F92532_8(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U352623ED7F33E8C1C541F1C3101FA981B173D795E_9(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U35EF781A18260A43AA5EAF73D4DAAA126F7ACAB4A_10(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U3659C24C855D0E50CBD366B2774AF841E29202E70_11(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U36B2745F3AD679B4F2E844D23C3EE0BAD49E1528D_12(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U371D13FFA93C0CC3F813C0B17C0FDFAC1D1AAA8CE_13(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U37A827DA5CD16B7AC8DB4221BABFA420A2B8668E8_14(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U37AF949FCE93CBEA9FAE4D22F2BCD5756396C6BB9_15(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U382273BF74F02F3FDFABEED76BCA4B82DDB2C2761_16(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U38C32A1D05EF81F6F81E519ECE45CC6D67AEC5A32_17(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U38E958C1847104F390F9C9B64F0F39C98ED4AC63F_18(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U394F89F72CC0508D891A0C71DD7B57B703869A3FB_19(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U39E66CEC4FA557D4655353E0A4F5940FA0D71CF3A_20(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_U39EF46C677634CB82C0BD475E372C71C5F68C981A_21(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_A5A5BE77BD3D095389FABC21D18BB648787E6618_22(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_B0D57A37D962AF56458E04E1BAAB3C4BC1B146F5_23(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_B1D1CEE08985760776A35065014EAF3D4D3CDE8D_24(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_B2574B82126B684C0CB3CF93BF7473F0FBB34400_25(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_B6BD9A82204938ABFE97CF3FAB5A47824080EAA0_26(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_BC1CEF8131E7F0D8206029373157806126E21026_27(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_C14817C33562352073848F1A8EA633C19CF1A4F5_28(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_C392E993B9E622A36C30E0BB997A9F41293CD9EF_29(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_C74F2436F5FE448E6F8A9D1C5C95D8DD53B39F1E_30(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_C8E6884C6631D74572185DA8E1E35BD7BBCCD6C4_31(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_CFB85A64A0D1FAB523C3DE56096F8803CDFEA674_32(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_D257291FBF3DA6F5C38B3275534902BC9EDE92EA_33(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_D4795500A3D0F00D8EE85626648C3FBAFA4F70C3_34(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_DF4C38A5E59685BBEC109623762B6914BE00E866_35(),
	U3CPrivateImplementationDetailsU3E_t3057255371_StaticFields::get_offset_of_F632BE2E03B27F265F779A5D3D217E5C14AB6CB5_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4215 = { sizeof (__StaticArrayInitTypeSizeU3D512_t3317833662)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D512_t3317833662 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4216 = { sizeof (U3CModuleU3E_t692745564), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4217 = { sizeof (Consts_t1749595338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4217[41] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4218 = { sizeof (CP10000_t776152390), -1, sizeof(CP10000_t776152390_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4218[1] = 
{
	CP10000_t776152390_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4219 = { sizeof (ENCmacintosh_t1087697298), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4220 = { sizeof (CP10079_t3879478589), -1, sizeof(CP10079_t3879478589_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4220[1] = 
{
	CP10079_t3879478589_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4221 = { sizeof (ENCx_mac_icelandic_t2411177152), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4222 = { sizeof (CP1250_t1787607523), -1, sizeof(CP1250_t1787607523_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4222[1] = 
{
	CP1250_t1787607523_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4223 = { sizeof (ENCwindows_1250_t3241285640), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4224 = { sizeof (CP1252_t2169944547), -1, sizeof(CP1252_t2169944547_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4224[1] = 
{
	CP1252_t2169944547_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4225 = { sizeof (ENCwindows_1252_t3241285642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4226 = { sizeof (CP1253_t4126259683), -1, sizeof(CP1253_t4126259683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4226[1] = 
{
	CP1253_t4126259683_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4227 = { sizeof (ENCwindows_1253_t3241285643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4228 = { sizeof (CP28592_t3519602206), -1, sizeof(CP28592_t3519602206_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4228[1] = 
{
	CP28592_t3519602206_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4229 = { sizeof (ENCiso_8859_2_t815391552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4230 = { sizeof (CP28593_t3519602207), -1, sizeof(CP28593_t3519602207_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4230[1] = 
{
	CP28593_t3519602207_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4231 = { sizeof (ENCiso_8859_3_t2381475493), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4232 = { sizeof (CP28597_t3519602203), -1, sizeof(CP28597_t3519602203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4232[1] = 
{
	CP28597_t3519602203_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4233 = { sizeof (ENCiso_8859_7_t412107025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4234 = { sizeof (CP28605_t1600022914), -1, sizeof(CP28605_t1600022914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4234[1] = 
{
	CP28605_t1600022914_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4235 = { sizeof (ENCiso_8859_15_t3881981915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4236 = { sizeof (CP437_t3683438768), -1, sizeof(CP437_t3683438768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4236[1] = 
{
	CP437_t3683438768_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4237 = { sizeof (ENCibm437_t3410411110), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4238 = { sizeof (CP850_t1311178993), -1, sizeof(CP850_t1311178993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4238[1] = 
{
	CP850_t1311178993_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4239 = { sizeof (ENCibm850_t3813826705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4240 = { sizeof (CP860_t1311113457), -1, sizeof(CP860_t1311113457_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4240[1] = 
{
	CP860_t1311113457_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4241 = { sizeof (ENCibm860_t3814023313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4242 = { sizeof (CP861_t2877197398), -1, sizeof(CP861_t2877197398_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4242[1] = 
{
	CP861_t2877197398_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4243 = { sizeof (ENCibm861_t2247939372), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4244 = { sizeof (CP863_t1714397984), -1, sizeof(CP863_t1714397984_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4244[1] = 
{
	CP863_t1714397984_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4245 = { sizeof (ENCibm863_t1085139958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4246 = { sizeof (CP865_t551598570), -1, sizeof(CP865_t551598570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4246[1] = 
{
	CP865_t551598570_StaticFields::get_offset_of_ToChars_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4247 = { sizeof (ENCibm865_t278570904), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4248 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255372), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4248[15] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U305C64B887A4D766EEB5842345D6B609A0E3A91C9_0(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U3148F346330E294B4157ED412259A7E7F373E26A8_1(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U31F867F0E96DB3A56943B8CB2662D1DA624023FCD_2(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U33220DE2BE9769737429E0DE2C6D837CB7C5A02AD_3(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U3356CE585046545CD2D0B109FF8A85DB88EE29074_4(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U34FEA2A6324C0192B29C90830F5D578051A4B1B16_5(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U3598D9433A53523A59D462896B803E416AB0B1901_6(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U36E6F169B075CACDE575F1FEA42B1376D31D5A7E5_7(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U37089F9820A6F9CC830909BCD1FAF2EF0A540F348_8(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_U39ACD15FF06BC5A9B676BDBD6BFF4025F9CCE845D_9(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_A394B4578F1DC8407FC72C0514F23F5AE4EDEBA8_10(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_B676CC7861A57EFB9BD0D7BEA61CD8E2484C57ED_11(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_D5DB24A984219B0001B4B86CDE1E0DF54572D83A_12(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_E11338F644FF140C0E23D8B7526DB4D2D73FC3F7_13(),
	U3CPrivateImplementationDetailsU3E_t3057255372_StaticFields::get_offset_of_FBB979B39B3DE95C52CD45C1036F61ABCE6B17D4_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4249 = { sizeof (__StaticArrayInitTypeSizeU3D512_t3317833663)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D512_t3317833663 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4250 = { sizeof (U3CModuleU3E_t692745565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4251 = { sizeof (RayShot_t3786369421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4251[9] = 
{
	RayShot_t3786369421::get_offset_of_line_4(),
	RayShot_t3786369421::get_offset_of_rayEnd_5(),
	RayShot_t3786369421::get_offset_of_hitReport_6(),
	RayShot_t3786369421::get_offset_of_abc_7(),
	RayShot_t3786369421::get_offset_of_enemies_8(),
	RayShot_t3786369421::get_offset_of_damage_9(),
	RayShot_t3786369421::get_offset_of_lifespan_10(),
	RayShot_t3786369421::get_offset_of_startTime_11(),
	RayShot_t3786369421::get_offset_of_endPos_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4252 = { sizeof (Ability_t166291472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4252[2] = 
{
	Ability_t166291472::get_offset_of_weaponstate_0(),
	Ability_t166291472::get_offset_of_collChecks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4253 = { sizeof (AbilityCollision_t2262659755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4253[8] = 
{
	AbilityCollision_t2262659755::get_offset_of_type_0(),
	AbilityCollision_t2262659755::get_offset_of_position_1(),
	AbilityCollision_t2262659755::get_offset_of_rotation_2(),
	AbilityCollision_t2262659755::get_offset_of_range_3(),
	AbilityCollision_t2262659755::get_offset_of_angle_4(),
	AbilityCollision_t2262659755::get_offset_of_speed_5(),
	AbilityCollision_t2262659755::get_offset_of_damage_6(),
	AbilityCollision_t2262659755::get_offset_of_missile_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4254 = { sizeof (ASCLBasicController_t2851843984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4254[19] = 
{
	ASCLBasicController_t2851843984::get_offset_of_animator_4(),
	ASCLBasicController_t2851843984::get_offset_of_floorPlane_5(),
	ASCLBasicController_t2851843984::get_offset_of_attackPlane_6(),
	ASCLBasicController_t2851843984::get_offset_of_enemies_7(),
	ASCLBasicController_t2851843984::get_offset_of_hitReport_8(),
	ASCLBasicController_t2851843984::get_offset_of_particleHit_9(),
	ASCLBasicController_t2851843984::get_offset_of_abilities_10(),
	ASCLBasicController_t2851843984::get_offset_of_ahc_11(),
	ASCLBasicController_t2851843984::get_offset_of_hitCheck_12(),
	ASCLBasicController_t2851843984::get_offset_of_WeaponState_13(),
	ASCLBasicController_t2851843984::get_offset_of_wasAttacking_14(),
	ASCLBasicController_t2851843984::get_offset_of_movementTarget_15(),
	ASCLBasicController_t2851843984::get_offset_of_destFloor_16(),
	ASCLBasicController_t2851843984::get_offset_of_rotateSpeed_17(),
	ASCLBasicController_t2851843984::get_offset_of_attackPos_18(),
	ASCLBasicController_t2851843984::get_offset_of_lookAtPos_19(),
	ASCLBasicController_t2851843984::get_offset_of_gravity_20(),
	ASCLBasicController_t2851843984::get_offset_of_fallspeed_21(),
	ASCLBasicController_t2851843984::get_offset_of_rightButtonDown_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4255 = { sizeof (BasicHitCheck_t2528769335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4255[3] = 
{
	BasicHitCheck_t2528769335::get_offset_of_colliderTestTime_4(),
	BasicHitCheck_t2528769335::get_offset_of_collChecked_5(),
	BasicHitCheck_t2528769335::get_offset_of_abilityController_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4256 = { sizeof (CamTarget_t1790837193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4256[3] = 
{
	CamTarget_t1790837193::get_offset_of_target_4(),
	CamTarget_t1790837193::get_offset_of_camSpeed_5(),
	CamTarget_t1790837193::get_offset_of_lerpPos_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4257 = { sizeof (CarCamera_t1094971545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4257[10] = 
{
	CarCamera_t1094971545::get_offset_of_target_4(),
	CarCamera_t1094971545::get_offset_of_height_5(),
	CarCamera_t1094971545::get_offset_of_positionDamping_6(),
	CarCamera_t1094971545::get_offset_of_velocityDamping_7(),
	CarCamera_t1094971545::get_offset_of_distance_8(),
	CarCamera_t1094971545::get_offset_of_ignoreLayers_9(),
	CarCamera_t1094971545::get_offset_of_hit_10(),
	CarCamera_t1094971545::get_offset_of_prevVelocity_11(),
	CarCamera_t1094971545::get_offset_of_raycastLayers_12(),
	CarCamera_t1094971545::get_offset_of_currentVelocity_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4258 = { sizeof (CharacterControllerCollCheck_t3758802271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4258[2] = 
{
	CharacterControllerCollCheck_t3758802271::get_offset_of_charControl_4(),
	CharacterControllerCollCheck_t3758802271::get_offset_of_gravity_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4259 = { sizeof (CharacterDemoController_t836415614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4259[12] = 
{
	CharacterDemoController_t836415614::get_offset_of_animator_4(),
	CharacterDemoController_t836415614::get_offset_of_floorPlane_5(),
	CharacterDemoController_t836415614::get_offset_of_WeaponState_6(),
	CharacterDemoController_t836415614::get_offset_of_wasAttacking_7(),
	CharacterDemoController_t836415614::get_offset_of_rotateSpeed_8(),
	CharacterDemoController_t836415614::get_offset_of_movementTargetPosition_9(),
	CharacterDemoController_t836415614::get_offset_of_attackPos_10(),
	CharacterDemoController_t836415614::get_offset_of_lookAtPos_11(),
	CharacterDemoController_t836415614::get_offset_of_gravity_12(),
	CharacterDemoController_t836415614::get_offset_of_hit_13(),
	CharacterDemoController_t836415614::get_offset_of_ray_14(),
	CharacterDemoController_t836415614::get_offset_of_rightButtonDown_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4260 = { sizeof (CharacterPhysicsController_t3068241336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4260[17] = 
{
	CharacterPhysicsController_t3068241336::get_offset_of_animator_4(),
	CharacterPhysicsController_t3068241336::get_offset_of_floorPlane_5(),
	CharacterPhysicsController_t3068241336::get_offset_of_WeaponState_6(),
	CharacterPhysicsController_t3068241336::get_offset_of_wasAttacking_7(),
	CharacterPhysicsController_t3068241336::get_offset_of_rotateSpeed_8(),
	CharacterPhysicsController_t3068241336::get_offset_of_charcontroller_9(),
	CharacterPhysicsController_t3068241336::get_offset_of_movementTargetPosition_10(),
	CharacterPhysicsController_t3068241336::get_offset_of_attackPos_11(),
	CharacterPhysicsController_t3068241336::get_offset_of_lookAtPos_12(),
	CharacterPhysicsController_t3068241336::get_offset_of_gravity_13(),
	CharacterPhysicsController_t3068241336::get_offset_of_jumping_14(),
	CharacterPhysicsController_t3068241336::get_offset_of_grounded_15(),
	CharacterPhysicsController_t3068241336::get_offset_of_contact_16(),
	CharacterPhysicsController_t3068241336::get_offset_of_verticalVelocity_17(),
	CharacterPhysicsController_t3068241336::get_offset_of_hit_18(),
	CharacterPhysicsController_t3068241336::get_offset_of_ray_19(),
	CharacterPhysicsController_t3068241336::get_offset_of_rightButtonDown_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4261 = { sizeof (Enemy_t1765729589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4261[4] = 
{
	Enemy_t1765729589::get_offset_of_cursorTexture_4(),
	Enemy_t1765729589::get_offset_of_cursorMode_5(),
	Enemy_t1765729589::get_offset_of_hotSpot_6(),
	Enemy_t1765729589::get_offset_of_targeted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4262 = { sizeof (Hit_t3558828664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4262[10] = 
{
	Hit_t3558828664::get_offset_of_yellow_4(),
	Hit_t3558828664::get_offset_of_black_5(),
	Hit_t3558828664::get_offset_of_startTime_6(),
	Hit_t3558828664::get_offset_of_currentTime_7(),
	Hit_t3558828664::get_offset_of_lifespan_8(),
	Hit_t3558828664::get_offset_of_fadeTime_9(),
	Hit_t3558828664::get_offset_of_fadeSpeed_10(),
	Hit_t3558828664::get_offset_of_maxSize_11(),
	Hit_t3558828664::get_offset_of_speed_12(),
	Hit_t3558828664::get_offset_of_text_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4263 = { sizeof (killParticle_t3813571932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4263[3] = 
{
	killParticle_t3813571932::get_offset_of_pe_4(),
	killParticle_t3813571932::get_offset_of_lifespan_5(),
	killParticle_t3813571932::get_offset_of_startTime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4264 = { sizeof (Missile_t4280459806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4264[8] = 
{
	Missile_t4280459806::get_offset_of_enemies_4(),
	Missile_t4280459806::get_offset_of_speed_5(),
	Missile_t4280459806::get_offset_of_damage_6(),
	Missile_t4280459806::get_offset_of_abc_7(),
	Missile_t4280459806::get_offset_of_lifespan_8(),
	Missile_t4280459806::get_offset_of_startTime_9(),
	Missile_t4280459806::get_offset_of_hitReport_10(),
	Missile_t4280459806::get_offset_of_particleHit_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4265 = { sizeof (MouseOrbit_t3431653639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4265[6] = 
{
	MouseOrbit_t3431653639::get_offset_of_target_4(),
	MouseOrbit_t3431653639::get_offset_of_distance_5(),
	MouseOrbit_t3431653639::get_offset_of_xSpeed_6(),
	MouseOrbit_t3431653639::get_offset_of_ySpeed_7(),
	MouseOrbit_t3431653639::get_offset_of_x_8(),
	MouseOrbit_t3431653639::get_offset_of_y_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4266 = { sizeof (RadialAngler_t2211509914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4266[6] = 
{
	RadialAngler_t2211509914::get_offset_of_angle_4(),
	RadialAngler_t2211509914::get_offset_of_oldAngle_5(),
	RadialAngler_t2211509914::get_offset_of_range_6(),
	RadialAngler_t2211509914::get_offset_of_oldRange_7(),
	RadialAngler_t2211509914::get_offset_of_diameter_8(),
	RadialAngler_t2211509914::get_offset_of_helper_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4267 = { sizeof (RootMotionOff_t4254465151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4267[1] = 
{
	RootMotionOff_t4254465151::get_offset_of_ColliderTestTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4268 = { sizeof (SwitchView_t3536945271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4268[5] = 
{
	SwitchView_t3536945271::get_offset_of_mainCam_4(),
	SwitchView_t3536945271::get_offset_of_subCam01_5(),
	SwitchView_t3536945271::get_offset_of_subCam02_6(),
	SwitchView_t3536945271::get_offset_of_pos_7(),
	SwitchView_t3536945271::get_offset_of_size_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4269 = { sizeof (CamCTRLDemo_t3606798705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4269[4] = 
{
	CamCTRLDemo_t3606798705::get_offset_of_maxPosZf_4(),
	CamCTRLDemo_t3606798705::get_offset_of_minPosZf_5(),
	CamCTRLDemo_t3606798705::get_offset_of_thisCam_6(),
	CamCTRLDemo_t3606798705::get_offset_of_CamLRSpeed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4270 = { sizeof (MoveCTRLDemo_t2262282185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4270[7] = 
{
	MoveCTRLDemo_t2262282185::get_offset_of_move_4(),
	MoveCTRLDemo_t2262282185::get_offset_of_stop_5(),
	MoveCTRLDemo_t2262282185::get_offset_of_blend_6(),
	MoveCTRLDemo_t2262282185::get_offset_of_delay_7(),
	MoveCTRLDemo_t2262282185::get_offset_of_AddRunSpeed_8(),
	MoveCTRLDemo_t2262282185::get_offset_of_AddWalkSpeed_9(),
	MoveCTRLDemo_t2262282185::get_offset_of_hasAniComp_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4271 = { sizeof (ExampleWheelController_t197115271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4271[3] = 
{
	ExampleWheelController_t197115271::get_offset_of_acceleration_4(),
	ExampleWheelController_t197115271::get_offset_of_motionVectorRenderer_5(),
	ExampleWheelController_t197115271::get_offset_of_m_Rigidbody_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4272 = { sizeof (Uniforms_t1233092826), -1, sizeof(Uniforms_t1233092826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4272[1] = 
{
	Uniforms_t1233092826_StaticFields::get_offset_of__MotionAmount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4273 = { sizeof (CameraController_t3346819214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4273[9] = 
{
	CameraController_t3346819214::get_offset_of_cameraTarget_4(),
	CameraController_t3346819214::get_offset_of_rotateSpeed_5(),
	CameraController_t3346819214::get_offset_of_rotate_6(),
	CameraController_t3346819214::get_offset_of_offsetDistance_7(),
	CameraController_t3346819214::get_offset_of_offsetHeight_8(),
	CameraController_t3346819214::get_offset_of_smoothing_9(),
	CameraController_t3346819214::get_offset_of_offset_10(),
	CameraController_t3346819214::get_offset_of_following_11(),
	CameraController_t3346819214::get_offset_of_lastPosition_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4274 = { sizeof (Gravity_t379910146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4274[1] = 
{
	Gravity_t379910146::get_offset_of_planet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4275 = { sizeof (SimpleStateMachine_t1535007871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4275[8] = 
{
	SimpleStateMachine_t1535007871::get_offset_of_DebugGui_4(),
	SimpleStateMachine_t1535007871::get_offset_of_DebugGuiPosition_5(),
	SimpleStateMachine_t1535007871::get_offset_of_DebugGuiTitle_6(),
	SimpleStateMachine_t1535007871::get_offset_of_queueCommand_7(),
	SimpleStateMachine_t1535007871::get_offset_of_timeEnteredState_8(),
	SimpleStateMachine_t1535007871::get_offset_of_state_9(),
	SimpleStateMachine_t1535007871::get_offset_of_lastState_10(),
	SimpleStateMachine_t1535007871::get_offset_of__cache_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4276 = { sizeof (State_t3920417347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4276[7] = 
{
	State_t3920417347::get_offset_of_DoUpdate_0(),
	State_t3920417347::get_offset_of_DoFixedUpdate_1(),
	State_t3920417347::get_offset_of_DoLateUpdate_2(),
	State_t3920417347::get_offset_of_DoManualUpdate_3(),
	State_t3920417347::get_offset_of_enterState_4(),
	State_t3920417347::get_offset_of_exitState_5(),
	State_t3920417347::get_offset_of_currentState_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4277 = { sizeof (BruteForceMesh_t342700245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4277[5] = 
{
	BruteForceMesh_t342700245::get_offset_of_triangleCount_4(),
	BruteForceMesh_t342700245::get_offset_of_vertices_5(),
	BruteForceMesh_t342700245::get_offset_of_tris_6(),
	BruteForceMesh_t342700245::get_offset_of_triangleNormals_7(),
	BruteForceMesh_t342700245::get_offset_of_mesh_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4278 = { sizeof (BSPTree_t2380928923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4278[8] = 
{
	BSPTree_t2380928923::get_offset_of_drawMeshTreeOnStart_4(),
	BSPTree_t2380928923::get_offset_of_triangleCount_5(),
	BSPTree_t2380928923::get_offset_of_vertexCount_6(),
	BSPTree_t2380928923::get_offset_of_vertices_7(),
	BSPTree_t2380928923::get_offset_of_tris_8(),
	BSPTree_t2380928923::get_offset_of_triangleNormals_9(),
	BSPTree_t2380928923::get_offset_of_mesh_10(),
	BSPTree_t2380928923::get_offset_of_tree_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4279 = { sizeof (Node_t1584791709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4279[5] = 
{
	Node_t1584791709::get_offset_of_partitionPoint_0(),
	Node_t1584791709::get_offset_of_partitionNormal_1(),
	Node_t1584791709::get_offset_of_positiveChild_2(),
	Node_t1584791709::get_offset_of_negativeChild_3(),
	Node_t1584791709::get_offset_of_triangles_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4280 = { sizeof (SuperCharacterController_t3568978942), -1, sizeof(SuperCharacterController_t3568978942_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4280[36] = 
{
	SuperCharacterController_t3568978942::get_offset_of_debugMove_4(),
	SuperCharacterController_t3568978942::get_offset_of_triggerInteraction_5(),
	SuperCharacterController_t3568978942::get_offset_of_fixedTimeStep_6(),
	SuperCharacterController_t3568978942::get_offset_of_fixedUpdatesPerSecond_7(),
	SuperCharacterController_t3568978942::get_offset_of_clampToMovingGround_8(),
	SuperCharacterController_t3568978942::get_offset_of_debugSpheres_9(),
	SuperCharacterController_t3568978942::get_offset_of_debugGrounding_10(),
	SuperCharacterController_t3568978942::get_offset_of_debugPushbackMesssages_11(),
	SuperCharacterController_t3568978942::get_offset_of_spheres_12(),
	SuperCharacterController_t3568978942::get_offset_of_Walkable_13(),
	SuperCharacterController_t3568978942::get_offset_of_ownCollider_14(),
	SuperCharacterController_t3568978942::get_offset_of_radius_15(),
	SuperCharacterController_t3568978942::get_offset_of_U3CdeltaTimeU3Ek__BackingField_16(),
	SuperCharacterController_t3568978942::get_offset_of_U3CcurrentGroundU3Ek__BackingField_17(),
	SuperCharacterController_t3568978942::get_offset_of_U3CfeetU3Ek__BackingField_18(),
	SuperCharacterController_t3568978942::get_offset_of_U3CheadU3Ek__BackingField_19(),
	SuperCharacterController_t3568978942::get_offset_of_U3CcollisionDataU3Ek__BackingField_20(),
	SuperCharacterController_t3568978942::get_offset_of_U3CcurrentlyClampedToU3Ek__BackingField_21(),
	SuperCharacterController_t3568978942::get_offset_of_U3CheightScaleU3Ek__BackingField_22(),
	SuperCharacterController_t3568978942::get_offset_of_U3CradiusScaleU3Ek__BackingField_23(),
	SuperCharacterController_t3568978942::get_offset_of_U3CmanualUpdateOnlyU3Ek__BackingField_24(),
	SuperCharacterController_t3568978942::get_offset_of_AfterSingleUpdate_25(),
	SuperCharacterController_t3568978942::get_offset_of_initialPosition_26(),
	SuperCharacterController_t3568978942::get_offset_of_groundOffset_27(),
	SuperCharacterController_t3568978942::get_offset_of_lastGroundPosition_28(),
	SuperCharacterController_t3568978942::get_offset_of_clamping_29(),
	SuperCharacterController_t3568978942::get_offset_of_slopeLimiting_30(),
	SuperCharacterController_t3568978942::get_offset_of_ignoredColliders_31(),
	SuperCharacterController_t3568978942::get_offset_of_ignoredColliderStack_32(),
	0,
	0,
	0,
	0,
	SuperCharacterController_t3568978942::get_offset_of_TemporaryLayerIndex_37(),
	SuperCharacterController_t3568978942::get_offset_of_fixedDeltaTime_38(),
	SuperCharacterController_t3568978942_StaticFields::get_offset_of_defaultCollisionType_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4281 = { sizeof (Ground_t4164855697)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4281[6] = 
{
	Ground_t4164855697::get_offset_of_U3ChitU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Ground_t4164855697::get_offset_of_U3CnearHitU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Ground_t4164855697::get_offset_of_U3CfarHitU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Ground_t4164855697::get_offset_of_U3CsecondaryHitU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Ground_t4164855697::get_offset_of_U3CcollisionTypeU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Ground_t4164855697::get_offset_of_U3CtransformU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4282 = { sizeof (UpdateDelegate_t1636609007), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4283 = { sizeof (IgnoredCollider_t648768713)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4283[2] = 
{
	IgnoredCollider_t648768713::get_offset_of_collider_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IgnoredCollider_t648768713::get_offset_of_layer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4284 = { sizeof (SuperGround_t3112734080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4284[13] = 
{
	SuperGround_t3112734080::get_offset_of_walkable_0(),
	SuperGround_t3112734080::get_offset_of_controller_1(),
	SuperGround_t3112734080::get_offset_of_triggerInteraction_2(),
	SuperGround_t3112734080::get_offset_of_primaryGround_3(),
	SuperGround_t3112734080::get_offset_of_nearGround_4(),
	SuperGround_t3112734080::get_offset_of_farGround_5(),
	SuperGround_t3112734080::get_offset_of_stepGround_6(),
	SuperGround_t3112734080::get_offset_of_flushGround_7(),
	SuperGround_t3112734080::get_offset_of_U3CsuperCollisionTypeU3Ek__BackingField_8(),
	SuperGround_t3112734080::get_offset_of_U3CtransformU3Ek__BackingField_9(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4285 = { sizeof (GroundHit_t3864137795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4285[3] = 
{
	GroundHit_t3864137795::get_offset_of_U3CpointU3Ek__BackingField_0(),
	GroundHit_t3864137795::get_offset_of_U3CnormalU3Ek__BackingField_1(),
	GroundHit_t3864137795::get_offset_of_U3CdistanceU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4286 = { sizeof (CollisionSphere_t3916568399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4286[3] = 
{
	CollisionSphere_t3916568399::get_offset_of_offset_0(),
	CollisionSphere_t3916568399::get_offset_of_isFeet_1(),
	CollisionSphere_t3916568399::get_offset_of_isHead_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4287 = { sizeof (SuperCollision_t1779554860)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4287[5] = 
{
	SuperCollision_t1779554860::get_offset_of_collisionSphere_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SuperCollision_t1779554860::get_offset_of_superCollisionType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SuperCollision_t1779554860::get_offset_of_gameObject_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SuperCollision_t1779554860::get_offset_of_point_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SuperCollision_t1779554860::get_offset_of_normal_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4288 = { sizeof (SuperCollider_t154048620), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4289 = { sizeof (SuperCollisionType_t119653226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4289[2] = 
{
	SuperCollisionType_t119653226::get_offset_of_StandAngle_4(),
	SuperCollisionType_t119653226::get_offset_of_SlopeLimit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4290 = { sizeof (SuperMath_t2556666644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4291 = { sizeof (SuperStateMachine_t3068594726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4291[4] = 
{
	SuperStateMachine_t3068594726::get_offset_of_timeEnteredState_4(),
	SuperStateMachine_t3068594726::get_offset_of_state_5(),
	SuperStateMachine_t3068594726::get_offset_of_lastState_6(),
	SuperStateMachine_t3068594726::get_offset_of__cache_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4292 = { sizeof (State_t3931175374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4292[4] = 
{
	State_t3931175374::get_offset_of_DoSuperUpdate_0(),
	State_t3931175374::get_offset_of_enterState_1(),
	State_t3931175374::get_offset_of_exitState_2(),
	State_t3931175374::get_offset_of_currentState_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4293 = { sizeof (DebugDraw_t3647725267), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4294 = { sizeof (Math3d_t1427253148), -1, sizeof(Math3d_t1427253148_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4294[2] = 
{
	Math3d_t1427253148_StaticFields::get_offset_of_tempChild_4(),
	Math3d_t1427253148_StaticFields::get_offset_of_tempParent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4295 = { sizeof (MenuSceneLoader_t2183500486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4295[2] = 
{
	MenuSceneLoader_t2183500486::get_offset_of_menuUI_4(),
	MenuSceneLoader_t2183500486::get_offset_of_m_Go_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4296 = { sizeof (PauseMenu_t3916167947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4296[4] = 
{
	PauseMenu_t3916167947::get_offset_of_m_MenuToggle_4(),
	PauseMenu_t3916167947::get_offset_of_m_TimeScaleRef_5(),
	PauseMenu_t3916167947::get_offset_of_m_VolumeRef_6(),
	PauseMenu_t3916167947::get_offset_of_m_Paused_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4297 = { sizeof (SceneAndURLLoader_t3512401881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4297[1] = 
{
	SceneAndURLLoader_t3512401881::get_offset_of_m_PauseMenu_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4298 = { sizeof (CameraSwitch_t735968501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4298[3] = 
{
	CameraSwitch_t735968501::get_offset_of_objects_4(),
	CameraSwitch_t735968501::get_offset_of_text_5(),
	CameraSwitch_t735968501::get_offset_of_m_CurrentActiveObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4299 = { sizeof (LevelReset_t1558959187), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
