﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>
struct Dictionary_2_t125631422;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>
struct Dictionary_2_t1572824908;
// System.Collections.Generic.Dictionary`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>
struct Dictionary_2_t3095696878;
// System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture>
struct HashSet_1_t673836907;
// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase>
struct List_1_t4203178569;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4>
struct Func_2_t4093140010;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t4234922487;
// UnityEngine.Collision
struct Collision_t4262080450;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.GUIText
struct GUIText_t402233326;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t3089334924;
// UnityEngine.PostProcessing.AmbientOcclusionComponent
struct AmbientOcclusionComponent_t4130625043;
// UnityEngine.PostProcessing.AmbientOcclusionModel
struct AmbientOcclusionModel_t389471066;
// UnityEngine.PostProcessing.AntialiasingModel
struct AntialiasingModel_t1521139388;
// UnityEngine.PostProcessing.BloomComponent
struct BloomComponent_t3791419130;
// UnityEngine.PostProcessing.BloomModel
struct BloomModel_t2099727860;
// UnityEngine.PostProcessing.BuiltinDebugViewsComponent
struct BuiltinDebugViewsComponent_t2123147871;
// UnityEngine.PostProcessing.BuiltinDebugViewsModel
struct BuiltinDebugViewsModel_t1462618840;
// UnityEngine.PostProcessing.ChromaticAberrationComponent
struct ChromaticAberrationComponent_t1647263118;
// UnityEngine.PostProcessing.ChromaticAberrationModel
struct ChromaticAberrationModel_t3963399853;
// UnityEngine.PostProcessing.ColorGradingComponent
struct ColorGradingComponent_t1715259467;
// UnityEngine.PostProcessing.ColorGradingCurve
struct ColorGradingCurve_t2000571184;
// UnityEngine.PostProcessing.ColorGradingModel
struct ColorGradingModel_t1448048181;
// UnityEngine.PostProcessing.DepthOfFieldComponent
struct DepthOfFieldComponent_t554756766;
// UnityEngine.PostProcessing.DepthOfFieldModel
struct DepthOfFieldModel_t514067330;
// UnityEngine.PostProcessing.DitheringComponent
struct DitheringComponent_t277621267;
// UnityEngine.PostProcessing.DitheringModel
struct DitheringModel_t2429005396;
// UnityEngine.PostProcessing.EyeAdaptationComponent
struct EyeAdaptationComponent_t3394805121;
// UnityEngine.PostProcessing.EyeAdaptationModel
struct EyeAdaptationModel_t242823912;
// UnityEngine.PostProcessing.FogComponent
struct FogComponent_t3400726830;
// UnityEngine.PostProcessing.FogModel
struct FogModel_t3620688749;
// UnityEngine.PostProcessing.FxaaComponent
struct FxaaComponent_t1312385771;
// UnityEngine.PostProcessing.GrainComponent
struct GrainComponent_t866324317;
// UnityEngine.PostProcessing.GrainModel
struct GrainModel_t1152882488;
// UnityEngine.PostProcessing.MaterialFactory
struct MaterialFactory_t2445948724;
// UnityEngine.PostProcessing.MotionBlurComponent
struct MotionBlurComponent_t3686516877;
// UnityEngine.PostProcessing.MotionBlurModel
struct MotionBlurModel_t3080286123;
// UnityEngine.PostProcessing.PostProcessingContext
struct PostProcessingContext_t2014408948;
// UnityEngine.PostProcessing.PostProcessingProfile
struct PostProcessingProfile_t724195375;
// UnityEngine.PostProcessing.RenderTextureFactory
struct RenderTextureFactory_t1946967824;
// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent
struct ScreenSpaceReflectionComponent_t856094247;
// UnityEngine.PostProcessing.ScreenSpaceReflectionModel
struct ScreenSpaceReflectionModel_t3026344732;
// UnityEngine.PostProcessing.TaaComponent
struct TaaComponent_t3791749658;
// UnityEngine.PostProcessing.UserLutComponent
struct UserLutComponent_t2843161776;
// UnityEngine.PostProcessing.UserLutModel
struct UserLutModel_t1670108080;
// UnityEngine.PostProcessing.VignetteComponent
struct VignetteComponent_t3243642943;
// UnityEngine.PostProcessing.VignetteModel
struct VignetteModel_t2845517177;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.SphereCollider
struct SphereCollider_t2077223608;
// UnityEngine.SpringJoint
struct SpringJoint_t1912369980;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t2999697109;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityStandardAssets.Effects.ExplosionFireAndDebris
struct ExplosionFireAndDebris_t2411343565;
// UnityStandardAssets.Effects.ExplosionPhysicsForce
struct ExplosionPhysicsForce_t3982641844;
// UnityStandardAssets.Effects.Explosive
struct Explosive_t792321375;
// UnityStandardAssets.Effects.ParticleSystemMultiplier
struct ParticleSystemMultiplier_t2770350653;
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
struct DemoParticleSystem_t1195446716;
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList
struct DemoParticleSystemList_t4288938153;
// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem[]
struct DemoParticleSystemU5BU5D_t58401749;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[]
struct ReplacementDefinitionU5BU5D_t2596446823;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct ReplacementList_t1887104210;
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct Vector3andSpace_t219844479;
// UnityStandardAssets.Utility.DragRigidbody
struct DragRigidbody_t1600652016;
// UnityStandardAssets.Utility.FOVKick
struct FOVKick_t120370150;
// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t1895875871;
// UnityStandardAssets.Utility.ObjectResetter
struct ObjectResetter_t639177103;
// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct ParticleSystemDestroyer_t558680695;
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct Entries_t3168066469;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_t2725803170;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry[]
struct EntryU5BU5D_t3574483607;
// UnityStandardAssets.Utility.WaypointCircuit
struct WaypointCircuit_t445075330;
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct WaypointList_t2584574554;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef COLORGRADINGCURVE_T2000571184_H
#define COLORGRADINGCURVE_T2000571184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingCurve
struct  ColorGradingCurve_t2000571184  : public RuntimeObject
{
public:
	// UnityEngine.AnimationCurve UnityEngine.PostProcessing.ColorGradingCurve::curve
	AnimationCurve_t3046754366 * ___curve_0;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingCurve::m_Loop
	bool ___m_Loop_1;
	// System.Single UnityEngine.PostProcessing.ColorGradingCurve::m_ZeroValue
	float ___m_ZeroValue_2;
	// System.Single UnityEngine.PostProcessing.ColorGradingCurve::m_Range
	float ___m_Range_3;
	// UnityEngine.AnimationCurve UnityEngine.PostProcessing.ColorGradingCurve::m_InternalLoopingCurve
	AnimationCurve_t3046754366 * ___m_InternalLoopingCurve_4;

public:
	inline static int32_t get_offset_of_curve_0() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t2000571184, ___curve_0)); }
	inline AnimationCurve_t3046754366 * get_curve_0() const { return ___curve_0; }
	inline AnimationCurve_t3046754366 ** get_address_of_curve_0() { return &___curve_0; }
	inline void set_curve_0(AnimationCurve_t3046754366 * value)
	{
		___curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___curve_0), value);
	}

	inline static int32_t get_offset_of_m_Loop_1() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t2000571184, ___m_Loop_1)); }
	inline bool get_m_Loop_1() const { return ___m_Loop_1; }
	inline bool* get_address_of_m_Loop_1() { return &___m_Loop_1; }
	inline void set_m_Loop_1(bool value)
	{
		___m_Loop_1 = value;
	}

	inline static int32_t get_offset_of_m_ZeroValue_2() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t2000571184, ___m_ZeroValue_2)); }
	inline float get_m_ZeroValue_2() const { return ___m_ZeroValue_2; }
	inline float* get_address_of_m_ZeroValue_2() { return &___m_ZeroValue_2; }
	inline void set_m_ZeroValue_2(float value)
	{
		___m_ZeroValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Range_3() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t2000571184, ___m_Range_3)); }
	inline float get_m_Range_3() const { return ___m_Range_3; }
	inline float* get_address_of_m_Range_3() { return &___m_Range_3; }
	inline void set_m_Range_3(float value)
	{
		___m_Range_3 = value;
	}

	inline static int32_t get_offset_of_m_InternalLoopingCurve_4() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t2000571184, ___m_InternalLoopingCurve_4)); }
	inline AnimationCurve_t3046754366 * get_m_InternalLoopingCurve_4() const { return ___m_InternalLoopingCurve_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_m_InternalLoopingCurve_4() { return &___m_InternalLoopingCurve_4; }
	inline void set_m_InternalLoopingCurve_4(AnimationCurve_t3046754366 * value)
	{
		___m_InternalLoopingCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalLoopingCurve_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGCURVE_T2000571184_H
#ifndef GRAPHICSUTILS_T2852986763_H
#define GRAPHICSUTILS_T2852986763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GraphicsUtils
struct  GraphicsUtils_t2852986763  : public RuntimeObject
{
public:

public:
};

struct GraphicsUtils_t2852986763_StaticFields
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.GraphicsUtils::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_0;
	// UnityEngine.Mesh UnityEngine.PostProcessing.GraphicsUtils::s_Quad
	Mesh_t3648964284 * ___s_Quad_1;

public:
	inline static int32_t get_offset_of_s_WhiteTexture_0() { return static_cast<int32_t>(offsetof(GraphicsUtils_t2852986763_StaticFields, ___s_WhiteTexture_0)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_0() const { return ___s_WhiteTexture_0; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_0() { return &___s_WhiteTexture_0; }
	inline void set_s_WhiteTexture_0(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_0), value);
	}

	inline static int32_t get_offset_of_s_Quad_1() { return static_cast<int32_t>(offsetof(GraphicsUtils_t2852986763_StaticFields, ___s_Quad_1)); }
	inline Mesh_t3648964284 * get_s_Quad_1() const { return ___s_Quad_1; }
	inline Mesh_t3648964284 ** get_address_of_s_Quad_1() { return &___s_Quad_1; }
	inline void set_s_Quad_1(Mesh_t3648964284 * value)
	{
		___s_Quad_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Quad_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHICSUTILS_T2852986763_H
#ifndef MATERIALFACTORY_T2445948724_H
#define MATERIALFACTORY_T2445948724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MaterialFactory
struct  MaterialFactory_t2445948724  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> UnityEngine.PostProcessing.MaterialFactory::m_Materials
	Dictionary_2_t125631422 * ___m_Materials_0;

public:
	inline static int32_t get_offset_of_m_Materials_0() { return static_cast<int32_t>(offsetof(MaterialFactory_t2445948724, ___m_Materials_0)); }
	inline Dictionary_2_t125631422 * get_m_Materials_0() const { return ___m_Materials_0; }
	inline Dictionary_2_t125631422 ** get_address_of_m_Materials_0() { return &___m_Materials_0; }
	inline void set_m_Materials_0(Dictionary_2_t125631422 * value)
	{
		___m_Materials_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Materials_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALFACTORY_T2445948724_H
#ifndef POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#define POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentBase
struct  PostProcessingComponentBase_t2731103827  : public RuntimeObject
{
public:
	// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingComponentBase::context
	PostProcessingContext_t2014408948 * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(PostProcessingComponentBase_t2731103827, ___context_0)); }
	inline PostProcessingContext_t2014408948 * get_context_0() const { return ___context_0; }
	inline PostProcessingContext_t2014408948 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(PostProcessingContext_t2014408948 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifndef POSTPROCESSINGCONTEXT_T2014408948_H
#define POSTPROCESSINGCONTEXT_T2014408948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingContext
struct  PostProcessingContext_t2014408948  : public RuntimeObject
{
public:
	// UnityEngine.PostProcessing.PostProcessingProfile UnityEngine.PostProcessing.PostProcessingContext::profile
	PostProcessingProfile_t724195375 * ___profile_0;
	// UnityEngine.Camera UnityEngine.PostProcessing.PostProcessingContext::camera
	Camera_t4157153871 * ___camera_1;
	// UnityEngine.PostProcessing.MaterialFactory UnityEngine.PostProcessing.PostProcessingContext::materialFactory
	MaterialFactory_t2445948724 * ___materialFactory_2;
	// UnityEngine.PostProcessing.RenderTextureFactory UnityEngine.PostProcessing.PostProcessingContext::renderTextureFactory
	RenderTextureFactory_t1946967824 * ___renderTextureFactory_3;
	// System.Boolean UnityEngine.PostProcessing.PostProcessingContext::<interrupted>k__BackingField
	bool ___U3CinterruptedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_profile_0() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___profile_0)); }
	inline PostProcessingProfile_t724195375 * get_profile_0() const { return ___profile_0; }
	inline PostProcessingProfile_t724195375 ** get_address_of_profile_0() { return &___profile_0; }
	inline void set_profile_0(PostProcessingProfile_t724195375 * value)
	{
		___profile_0 = value;
		Il2CppCodeGenWriteBarrier((&___profile_0), value);
	}

	inline static int32_t get_offset_of_camera_1() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___camera_1)); }
	inline Camera_t4157153871 * get_camera_1() const { return ___camera_1; }
	inline Camera_t4157153871 ** get_address_of_camera_1() { return &___camera_1; }
	inline void set_camera_1(Camera_t4157153871 * value)
	{
		___camera_1 = value;
		Il2CppCodeGenWriteBarrier((&___camera_1), value);
	}

	inline static int32_t get_offset_of_materialFactory_2() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___materialFactory_2)); }
	inline MaterialFactory_t2445948724 * get_materialFactory_2() const { return ___materialFactory_2; }
	inline MaterialFactory_t2445948724 ** get_address_of_materialFactory_2() { return &___materialFactory_2; }
	inline void set_materialFactory_2(MaterialFactory_t2445948724 * value)
	{
		___materialFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&___materialFactory_2), value);
	}

	inline static int32_t get_offset_of_renderTextureFactory_3() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___renderTextureFactory_3)); }
	inline RenderTextureFactory_t1946967824 * get_renderTextureFactory_3() const { return ___renderTextureFactory_3; }
	inline RenderTextureFactory_t1946967824 ** get_address_of_renderTextureFactory_3() { return &___renderTextureFactory_3; }
	inline void set_renderTextureFactory_3(RenderTextureFactory_t1946967824 * value)
	{
		___renderTextureFactory_3 = value;
		Il2CppCodeGenWriteBarrier((&___renderTextureFactory_3), value);
	}

	inline static int32_t get_offset_of_U3CinterruptedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___U3CinterruptedU3Ek__BackingField_4)); }
	inline bool get_U3CinterruptedU3Ek__BackingField_4() const { return ___U3CinterruptedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CinterruptedU3Ek__BackingField_4() { return &___U3CinterruptedU3Ek__BackingField_4; }
	inline void set_U3CinterruptedU3Ek__BackingField_4(bool value)
	{
		___U3CinterruptedU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCONTEXT_T2014408948_H
#ifndef POSTPROCESSINGMODEL_T540111976_H
#define POSTPROCESSINGMODEL_T540111976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingModel
struct  PostProcessingModel_t540111976  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.PostProcessing.PostProcessingModel::m_Enabled
	bool ___m_Enabled_0;

public:
	inline static int32_t get_offset_of_m_Enabled_0() { return static_cast<int32_t>(offsetof(PostProcessingModel_t540111976, ___m_Enabled_0)); }
	inline bool get_m_Enabled_0() const { return ___m_Enabled_0; }
	inline bool* get_address_of_m_Enabled_0() { return &___m_Enabled_0; }
	inline void set_m_Enabled_0(bool value)
	{
		___m_Enabled_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGMODEL_T540111976_H
#ifndef RENDERTEXTUREFACTORY_T1946967824_H
#define RENDERTEXTUREFACTORY_T1946967824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.RenderTextureFactory
struct  RenderTextureFactory_t1946967824  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture> UnityEngine.PostProcessing.RenderTextureFactory::m_TemporaryRTs
	HashSet_1_t673836907 * ___m_TemporaryRTs_0;

public:
	inline static int32_t get_offset_of_m_TemporaryRTs_0() { return static_cast<int32_t>(offsetof(RenderTextureFactory_t1946967824, ___m_TemporaryRTs_0)); }
	inline HashSet_1_t673836907 * get_m_TemporaryRTs_0() const { return ___m_TemporaryRTs_0; }
	inline HashSet_1_t673836907 ** get_address_of_m_TemporaryRTs_0() { return &___m_TemporaryRTs_0; }
	inline void set_m_TemporaryRTs_0(HashSet_1_t673836907 * value)
	{
		___m_TemporaryRTs_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TemporaryRTs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFACTORY_T1946967824_H
#ifndef U3CSTARTU3ED__4_T3367237624_H
#define U3CSTARTU3ED__4_T3367237624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4
struct  U3CStartU3Ed__4_t3367237624  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Effects.ExplosionFireAndDebris UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::<>4__this
	ExplosionFireAndDebris_t2411343565 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::<multiplier>5__2
	float ___U3CmultiplierU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3367237624, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3367237624, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3367237624, ___U3CU3E4__this_2)); }
	inline ExplosionFireAndDebris_t2411343565 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ExplosionFireAndDebris_t2411343565 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ExplosionFireAndDebris_t2411343565 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CmultiplierU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3367237624, ___U3CmultiplierU3E5__2_3)); }
	inline float get_U3CmultiplierU3E5__2_3() const { return ___U3CmultiplierU3E5__2_3; }
	inline float* get_address_of_U3CmultiplierU3E5__2_3() { return &___U3CmultiplierU3E5__2_3; }
	inline void set_U3CmultiplierU3E5__2_3(float value)
	{
		___U3CmultiplierU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__4_T3367237624_H
#ifndef U3CSTARTU3ED__1_T3900213119_H
#define U3CSTARTU3ED__1_T3900213119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1
struct  U3CStartU3Ed__1_t3900213119  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Effects.ExplosionPhysicsForce UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::<>4__this
	ExplosionPhysicsForce_t3982641844 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__1_t3900213119, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__1_t3900213119, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__1_t3900213119, ___U3CU3E4__this_2)); }
	inline ExplosionPhysicsForce_t3982641844 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ExplosionPhysicsForce_t3982641844 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ExplosionPhysicsForce_t3982641844 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__1_T3900213119_H
#ifndef U3CONCOLLISIONENTERU3ED__8_T3187962944_H
#define U3CONCOLLISIONENTERU3ED__8_T3187962944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8
struct  U3COnCollisionEnterU3Ed__8_t3187962944  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Effects.Explosive UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::<>4__this
	Explosive_t792321375 * ___U3CU3E4__this_2;
	// UnityEngine.Collision UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::col
	Collision_t4262080450 * ___col_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ed__8_t3187962944, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ed__8_t3187962944, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ed__8_t3187962944, ___U3CU3E4__this_2)); }
	inline Explosive_t792321375 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Explosive_t792321375 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Explosive_t792321375 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_col_3() { return static_cast<int32_t>(offsetof(U3COnCollisionEnterU3Ed__8_t3187962944, ___col_3)); }
	inline Collision_t4262080450 * get_col_3() const { return ___col_3; }
	inline Collision_t4262080450 ** get_address_of_col_3() { return &___col_3; }
	inline void set_col_3(Collision_t4262080450 * value)
	{
		___col_3 = value;
		Il2CppCodeGenWriteBarrier((&___col_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONCOLLISIONENTERU3ED__8_T3187962944_H
#ifndef DEMOPARTICLESYSTEMLIST_T4288938153_H
#define DEMOPARTICLESYSTEMLIST_T4288938153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList
struct  DemoParticleSystemList_t4288938153  : public RuntimeObject
{
public:
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem[] UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList::items
	DemoParticleSystemU5BU5D_t58401749* ___items_0;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(DemoParticleSystemList_t4288938153, ___items_0)); }
	inline DemoParticleSystemU5BU5D_t58401749* get_items_0() const { return ___items_0; }
	inline DemoParticleSystemU5BU5D_t58401749** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(DemoParticleSystemU5BU5D_t58401749* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOPARTICLESYSTEMLIST_T4288938153_H
#ifndef REPLACEMENTDEFINITION_T2693741842_H
#define REPLACEMENTDEFINITION_T2693741842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct  ReplacementDefinition_t2693741842  : public RuntimeObject
{
public:
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::original
	Shader_t4151988712 * ___original_0;
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::replacement
	Shader_t4151988712 * ___replacement_1;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___original_0)); }
	inline Shader_t4151988712 * get_original_0() const { return ___original_0; }
	inline Shader_t4151988712 ** get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(Shader_t4151988712 * value)
	{
		___original_0 = value;
		Il2CppCodeGenWriteBarrier((&___original_0), value);
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___replacement_1)); }
	inline Shader_t4151988712 * get_replacement_1() const { return ___replacement_1; }
	inline Shader_t4151988712 ** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(Shader_t4151988712 * value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTDEFINITION_T2693741842_H
#ifndef REPLACEMENTLIST_T1887104210_H
#define REPLACEMENTLIST_T1887104210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct  ReplacementList_t1887104210  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[] UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList::items
	ReplacementDefinitionU5BU5D_t2596446823* ___items_0;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(ReplacementList_t1887104210, ___items_0)); }
	inline ReplacementDefinitionU5BU5D_t2596446823* get_items_0() const { return ___items_0; }
	inline ReplacementDefinitionU5BU5D_t2596446823** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(ReplacementDefinitionU5BU5D_t2596446823* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTLIST_T1887104210_H
#ifndef U3CDRAGOBJECTU3ED__8_T4113469309_H
#define U3CDRAGOBJECTU3ED__8_T4113469309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8
struct  U3CDragObjectU3Ed__8_t4113469309  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.DragRigidbody UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<>4__this
	DragRigidbody_t1600652016 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::distance
	float ___distance_3;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<oldDrag>5__2
	float ___U3ColdDragU3E5__2_4;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<oldAngularDrag>5__3
	float ___U3ColdAngularDragU3E5__3_5;
	// UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<mainCamera>5__4
	Camera_t4157153871 * ___U3CmainCameraU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3CU3E4__this_2)); }
	inline DragRigidbody_t1600652016 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DragRigidbody_t1600652016 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DragRigidbody_t1600652016 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_U3ColdDragU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3ColdDragU3E5__2_4)); }
	inline float get_U3ColdDragU3E5__2_4() const { return ___U3ColdDragU3E5__2_4; }
	inline float* get_address_of_U3ColdDragU3E5__2_4() { return &___U3ColdDragU3E5__2_4; }
	inline void set_U3ColdDragU3E5__2_4(float value)
	{
		___U3ColdDragU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3ColdAngularDragU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3ColdAngularDragU3E5__3_5)); }
	inline float get_U3ColdAngularDragU3E5__3_5() const { return ___U3ColdAngularDragU3E5__3_5; }
	inline float* get_address_of_U3ColdAngularDragU3E5__3_5() { return &___U3ColdAngularDragU3E5__3_5; }
	inline void set_U3ColdAngularDragU3E5__3_5(float value)
	{
		___U3ColdAngularDragU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CmainCameraU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3CmainCameraU3E5__4_6)); }
	inline Camera_t4157153871 * get_U3CmainCameraU3E5__4_6() const { return ___U3CmainCameraU3E5__4_6; }
	inline Camera_t4157153871 ** get_address_of_U3CmainCameraU3E5__4_6() { return &___U3CmainCameraU3E5__4_6; }
	inline void set_U3CmainCameraU3E5__4_6(Camera_t4157153871 * value)
	{
		___U3CmainCameraU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmainCameraU3E5__4_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAGOBJECTU3ED__8_T4113469309_H
#ifndef FOVKICK_T120370150_H
#define FOVKICK_T120370150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick
struct  FOVKick_t120370150  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.FOVKick::Camera
	Camera_t4157153871 * ___Camera_0;
	// System.Single UnityStandardAssets.Utility.FOVKick::originalFov
	float ___originalFov_1;
	// System.Single UnityStandardAssets.Utility.FOVKick::FOVIncrease
	float ___FOVIncrease_2;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToIncrease
	float ___TimeToIncrease_3;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToDecrease
	float ___TimeToDecrease_4;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.FOVKick::IncreaseCurve
	AnimationCurve_t3046754366 * ___IncreaseCurve_5;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_originalFov_1() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___originalFov_1)); }
	inline float get_originalFov_1() const { return ___originalFov_1; }
	inline float* get_address_of_originalFov_1() { return &___originalFov_1; }
	inline void set_originalFov_1(float value)
	{
		___originalFov_1 = value;
	}

	inline static int32_t get_offset_of_FOVIncrease_2() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___FOVIncrease_2)); }
	inline float get_FOVIncrease_2() const { return ___FOVIncrease_2; }
	inline float* get_address_of_FOVIncrease_2() { return &___FOVIncrease_2; }
	inline void set_FOVIncrease_2(float value)
	{
		___FOVIncrease_2 = value;
	}

	inline static int32_t get_offset_of_TimeToIncrease_3() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToIncrease_3)); }
	inline float get_TimeToIncrease_3() const { return ___TimeToIncrease_3; }
	inline float* get_address_of_TimeToIncrease_3() { return &___TimeToIncrease_3; }
	inline void set_TimeToIncrease_3(float value)
	{
		___TimeToIncrease_3 = value;
	}

	inline static int32_t get_offset_of_TimeToDecrease_4() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToDecrease_4)); }
	inline float get_TimeToDecrease_4() const { return ___TimeToDecrease_4; }
	inline float* get_address_of_TimeToDecrease_4() { return &___TimeToDecrease_4; }
	inline void set_TimeToDecrease_4(float value)
	{
		___TimeToDecrease_4 = value;
	}

	inline static int32_t get_offset_of_IncreaseCurve_5() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___IncreaseCurve_5)); }
	inline AnimationCurve_t3046754366 * get_IncreaseCurve_5() const { return ___IncreaseCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_IncreaseCurve_5() { return &___IncreaseCurve_5; }
	inline void set_IncreaseCurve_5(AnimationCurve_t3046754366 * value)
	{
		___IncreaseCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___IncreaseCurve_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOVKICK_T120370150_H
#ifndef U3CFOVKICKDOWNU3ED__10_T494950160_H
#define U3CFOVKICKDOWNU3ED__10_T494950160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10
struct  U3CFOVKickDownU3Ed__10_t494950160  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<>4__this
	FOVKick_t120370150 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<t>5__2
	float ___U3CtU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t494950160, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t494950160, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t494950160, ___U3CU3E4__this_2)); }
	inline FOVKick_t120370150 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FOVKick_t120370150 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FOVKick_t120370150 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t494950160, ___U3CtU3E5__2_3)); }
	inline float get_U3CtU3E5__2_3() const { return ___U3CtU3E5__2_3; }
	inline float* get_address_of_U3CtU3E5__2_3() { return &___U3CtU3E5__2_3; }
	inline void set_U3CtU3E5__2_3(float value)
	{
		___U3CtU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKDOWNU3ED__10_T494950160_H
#ifndef U3CFOVKICKUPU3ED__9_T1152317412_H
#define U3CFOVKICKUPU3ED__9_T1152317412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9
struct  U3CFOVKickUpU3Ed__9_t1152317412  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<>4__this
	FOVKick_t120370150 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<t>5__2
	float ___U3CtU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t1152317412, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t1152317412, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t1152317412, ___U3CU3E4__this_2)); }
	inline FOVKick_t120370150 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FOVKick_t120370150 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FOVKick_t120370150 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t1152317412, ___U3CtU3E5__2_3)); }
	inline float get_U3CtU3E5__2_3() const { return ___U3CtU3E5__2_3; }
	inline float* get_address_of_U3CtU3E5__2_3() { return &___U3CtU3E5__2_3; }
	inline void set_U3CtU3E5__2_3(float value)
	{
		___U3CtU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKUPU3ED__9_T1152317412_H
#ifndef LERPCONTROLLEDBOB_T1895875871_H
#define LERPCONTROLLEDBOB_T1895875871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob
struct  LerpControlledBob_t1895875871  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobDuration
	float ___BobDuration_0;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobAmount
	float ___BobAmount_1;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::m_Offset
	float ___m_Offset_2;

public:
	inline static int32_t get_offset_of_BobDuration_0() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobDuration_0)); }
	inline float get_BobDuration_0() const { return ___BobDuration_0; }
	inline float* get_address_of_BobDuration_0() { return &___BobDuration_0; }
	inline void set_BobDuration_0(float value)
	{
		___BobDuration_0 = value;
	}

	inline static int32_t get_offset_of_BobAmount_1() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobAmount_1)); }
	inline float get_BobAmount_1() const { return ___BobAmount_1; }
	inline float* get_address_of_BobAmount_1() { return &___BobAmount_1; }
	inline void set_BobAmount_1(float value)
	{
		___BobAmount_1 = value;
	}

	inline static int32_t get_offset_of_m_Offset_2() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___m_Offset_2)); }
	inline float get_m_Offset_2() const { return ___m_Offset_2; }
	inline float* get_address_of_m_Offset_2() { return &___m_Offset_2; }
	inline void set_m_Offset_2(float value)
	{
		___m_Offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LERPCONTROLLEDBOB_T1895875871_H
#ifndef U3CDOBOBCYCLEU3ED__4_T3986606630_H
#define U3CDOBOBCYCLEU3ED__4_T3986606630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4
struct  U3CDoBobCycleU3Ed__4_t3986606630  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<>4__this
	LerpControlledBob_t1895875871 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<t>5__2
	float ___U3CtU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_t3986606630, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_t3986606630, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_t3986606630, ___U3CU3E4__this_2)); }
	inline LerpControlledBob_t1895875871 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LerpControlledBob_t1895875871 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LerpControlledBob_t1895875871 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_t3986606630, ___U3CtU3E5__2_3)); }
	inline float get_U3CtU3E5__2_3() const { return ___U3CtU3E5__2_3; }
	inline float* get_address_of_U3CtU3E5__2_3() { return &___U3CtU3E5__2_3; }
	inline void set_U3CtU3E5__2_3(float value)
	{
		___U3CtU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBOBCYCLEU3ED__4_T3986606630_H
#ifndef U3CRESETCOROUTINEU3ED__6_T3318545224_H
#define U3CRESETCOROUTINEU3ED__6_T3318545224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6
struct  U3CResetCoroutineU3Ed__6_t3318545224  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::delay
	float ___delay_2;
	// UnityStandardAssets.Utility.ObjectResetter UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::<>4__this
	ObjectResetter_t639177103 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_t3318545224, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_t3318545224, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_t3318545224, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_t3318545224, ___U3CU3E4__this_3)); }
	inline ObjectResetter_t639177103 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ObjectResetter_t639177103 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ObjectResetter_t639177103 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESETCOROUTINEU3ED__6_T3318545224_H
#ifndef U3CSTARTU3ED__4_T3571467226_H
#define U3CSTARTU3ED__4_T3571467226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4
struct  U3CStartU3Ed__4_t3571467226  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.ParticleSystemDestroyer UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<>4__this
	ParticleSystemDestroyer_t558680695 * ___U3CU3E4__this_2;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<systems>5__2
	ParticleSystemU5BU5D_t3089334924* ___U3CsystemsU3E5__2_3;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<stopTime>5__3
	float ___U3CstopTimeU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3571467226, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3571467226, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3571467226, ___U3CU3E4__this_2)); }
	inline ParticleSystemDestroyer_t558680695 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ParticleSystemDestroyer_t558680695 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ParticleSystemDestroyer_t558680695 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CsystemsU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3571467226, ___U3CsystemsU3E5__2_3)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U3CsystemsU3E5__2_3() const { return ___U3CsystemsU3E5__2_3; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U3CsystemsU3E5__2_3() { return &___U3CsystemsU3E5__2_3; }
	inline void set_U3CsystemsU3E5__2_3(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U3CsystemsU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsystemsU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CstopTimeU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3571467226, ___U3CstopTimeU3E5__3_4)); }
	inline float get_U3CstopTimeU3E5__3_4() const { return ___U3CstopTimeU3E5__3_4; }
	inline float* get_address_of_U3CstopTimeU3E5__3_4() { return &___U3CstopTimeU3E5__3_4; }
	inline void set_U3CstopTimeU3E5__3_4(float value)
	{
		___U3CstopTimeU3E5__3_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__4_T3571467226_H
#ifndef U3CACTIVATEU3ED__5_T4263739043_H
#define U3CACTIVATEU3ED__5_T4263739043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5
struct  U3CActivateU3Ed__5_t4263739043  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::entry
	Entry_t2725803170 * ___entry_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CActivateU3Ed__5_t4263739043, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CActivateU3Ed__5_t4263739043, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_entry_2() { return static_cast<int32_t>(offsetof(U3CActivateU3Ed__5_t4263739043, ___entry_2)); }
	inline Entry_t2725803170 * get_entry_2() const { return ___entry_2; }
	inline Entry_t2725803170 ** get_address_of_entry_2() { return &___entry_2; }
	inline void set_entry_2(Entry_t2725803170 * value)
	{
		___entry_2 = value;
		Il2CppCodeGenWriteBarrier((&___entry_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIVATEU3ED__5_T4263739043_H
#ifndef U3CDEACTIVATEU3ED__6_T2861800224_H
#define U3CDEACTIVATEU3ED__6_T2861800224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6
struct  U3CDeactivateU3Ed__6_t2861800224  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::entry
	Entry_t2725803170 * ___entry_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ed__6_t2861800224, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ed__6_t2861800224, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_entry_2() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ed__6_t2861800224, ___entry_2)); }
	inline Entry_t2725803170 * get_entry_2() const { return ___entry_2; }
	inline Entry_t2725803170 ** get_address_of_entry_2() { return &___entry_2; }
	inline void set_entry_2(Entry_t2725803170 * value)
	{
		___entry_2 = value;
		Il2CppCodeGenWriteBarrier((&___entry_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDEACTIVATEU3ED__6_T2861800224_H
#ifndef U3CRELOADLEVELU3ED__7_T2023328258_H
#define U3CRELOADLEVELU3ED__7_T2023328258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7
struct  U3CReloadLevelU3Ed__7_t2023328258  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::entry
	Entry_t2725803170 * ___entry_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ed__7_t2023328258, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ed__7_t2023328258, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_entry_2() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ed__7_t2023328258, ___entry_2)); }
	inline Entry_t2725803170 * get_entry_2() const { return ___entry_2; }
	inline Entry_t2725803170 ** get_address_of_entry_2() { return &___entry_2; }
	inline void set_entry_2(Entry_t2725803170 * value)
	{
		___entry_2 = value;
		Il2CppCodeGenWriteBarrier((&___entry_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELOADLEVELU3ED__7_T2023328258_H
#ifndef ENTRIES_T3168066469_H
#define ENTRIES_T3168066469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct  Entries_t3168066469  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry[] UnityStandardAssets.Utility.TimedObjectActivator/Entries::entries
	EntryU5BU5D_t3574483607* ___entries_0;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(Entries_t3168066469, ___entries_0)); }
	inline EntryU5BU5D_t3574483607* get_entries_0() const { return ___entries_0; }
	inline EntryU5BU5D_t3574483607** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(EntryU5BU5D_t3574483607* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRIES_T3168066469_H
#ifndef WAYPOINTLIST_T2584574554_H
#define WAYPOINTLIST_T2584574554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct  WaypointList_t2584574554  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointCircuit/WaypointList::circuit
	WaypointCircuit_t445075330 * ___circuit_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit/WaypointList::items
	TransformU5BU5D_t807237628* ___items_1;

public:
	inline static int32_t get_offset_of_circuit_0() { return static_cast<int32_t>(offsetof(WaypointList_t2584574554, ___circuit_0)); }
	inline WaypointCircuit_t445075330 * get_circuit_0() const { return ___circuit_0; }
	inline WaypointCircuit_t445075330 ** get_address_of_circuit_0() { return &___circuit_0; }
	inline void set_circuit_0(WaypointCircuit_t445075330 * value)
	{
		___circuit_0 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_0), value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(WaypointList_t2584574554, ___items_1)); }
	inline TransformU5BU5D_t807237628* get_items_1() const { return ___items_1; }
	inline TransformU5BU5D_t807237628** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(TransformU5BU5D_t807237628* value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier((&___items_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTLIST_T2584574554_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef BASICSETTINGS_T838098426_H
#define BASICSETTINGS_T838098426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/BasicSettings
struct  BasicSettings_t838098426 
{
public:
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::postExposure
	float ___postExposure_0;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::temperature
	float ___temperature_1;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::tint
	float ___tint_2;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::hueShift
	float ___hueShift_3;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::saturation
	float ___saturation_4;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::contrast
	float ___contrast_5;

public:
	inline static int32_t get_offset_of_postExposure_0() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___postExposure_0)); }
	inline float get_postExposure_0() const { return ___postExposure_0; }
	inline float* get_address_of_postExposure_0() { return &___postExposure_0; }
	inline void set_postExposure_0(float value)
	{
		___postExposure_0 = value;
	}

	inline static int32_t get_offset_of_temperature_1() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___temperature_1)); }
	inline float get_temperature_1() const { return ___temperature_1; }
	inline float* get_address_of_temperature_1() { return &___temperature_1; }
	inline void set_temperature_1(float value)
	{
		___temperature_1 = value;
	}

	inline static int32_t get_offset_of_tint_2() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___tint_2)); }
	inline float get_tint_2() const { return ___tint_2; }
	inline float* get_address_of_tint_2() { return &___tint_2; }
	inline void set_tint_2(float value)
	{
		___tint_2 = value;
	}

	inline static int32_t get_offset_of_hueShift_3() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___hueShift_3)); }
	inline float get_hueShift_3() const { return ___hueShift_3; }
	inline float* get_address_of_hueShift_3() { return &___hueShift_3; }
	inline void set_hueShift_3(float value)
	{
		___hueShift_3 = value;
	}

	inline static int32_t get_offset_of_saturation_4() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___saturation_4)); }
	inline float get_saturation_4() const { return ___saturation_4; }
	inline float* get_address_of_saturation_4() { return &___saturation_4; }
	inline void set_saturation_4(float value)
	{
		___saturation_4 = value;
	}

	inline static int32_t get_offset_of_contrast_5() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___contrast_5)); }
	inline float get_contrast_5() const { return ___contrast_5; }
	inline float* get_address_of_contrast_5() { return &___contrast_5; }
	inline void set_contrast_5(float value)
	{
		___contrast_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICSETTINGS_T838098426_H
#ifndef CURVESSETTINGS_T2830270037_H
#define CURVESSETTINGS_T2830270037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct  CurvesSettings_t2830270037 
{
public:
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::master
	ColorGradingCurve_t2000571184 * ___master_0;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::red
	ColorGradingCurve_t2000571184 * ___red_1;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::green
	ColorGradingCurve_t2000571184 * ___green_2;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::blue
	ColorGradingCurve_t2000571184 * ___blue_3;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::hueVShue
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::hueVSsat
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::satVSsat
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::lumVSsat
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurrentEditingCurve
	int32_t ___e_CurrentEditingCurve_8;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveY
	bool ___e_CurveY_9;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveR
	bool ___e_CurveR_10;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveG
	bool ___e_CurveG_11;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveB
	bool ___e_CurveB_12;

public:
	inline static int32_t get_offset_of_master_0() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___master_0)); }
	inline ColorGradingCurve_t2000571184 * get_master_0() const { return ___master_0; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_master_0() { return &___master_0; }
	inline void set_master_0(ColorGradingCurve_t2000571184 * value)
	{
		___master_0 = value;
		Il2CppCodeGenWriteBarrier((&___master_0), value);
	}

	inline static int32_t get_offset_of_red_1() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___red_1)); }
	inline ColorGradingCurve_t2000571184 * get_red_1() const { return ___red_1; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_red_1() { return &___red_1; }
	inline void set_red_1(ColorGradingCurve_t2000571184 * value)
	{
		___red_1 = value;
		Il2CppCodeGenWriteBarrier((&___red_1), value);
	}

	inline static int32_t get_offset_of_green_2() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___green_2)); }
	inline ColorGradingCurve_t2000571184 * get_green_2() const { return ___green_2; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_green_2() { return &___green_2; }
	inline void set_green_2(ColorGradingCurve_t2000571184 * value)
	{
		___green_2 = value;
		Il2CppCodeGenWriteBarrier((&___green_2), value);
	}

	inline static int32_t get_offset_of_blue_3() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___blue_3)); }
	inline ColorGradingCurve_t2000571184 * get_blue_3() const { return ___blue_3; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_blue_3() { return &___blue_3; }
	inline void set_blue_3(ColorGradingCurve_t2000571184 * value)
	{
		___blue_3 = value;
		Il2CppCodeGenWriteBarrier((&___blue_3), value);
	}

	inline static int32_t get_offset_of_hueVShue_4() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___hueVShue_4)); }
	inline ColorGradingCurve_t2000571184 * get_hueVShue_4() const { return ___hueVShue_4; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_hueVShue_4() { return &___hueVShue_4; }
	inline void set_hueVShue_4(ColorGradingCurve_t2000571184 * value)
	{
		___hueVShue_4 = value;
		Il2CppCodeGenWriteBarrier((&___hueVShue_4), value);
	}

	inline static int32_t get_offset_of_hueVSsat_5() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___hueVSsat_5)); }
	inline ColorGradingCurve_t2000571184 * get_hueVSsat_5() const { return ___hueVSsat_5; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_hueVSsat_5() { return &___hueVSsat_5; }
	inline void set_hueVSsat_5(ColorGradingCurve_t2000571184 * value)
	{
		___hueVSsat_5 = value;
		Il2CppCodeGenWriteBarrier((&___hueVSsat_5), value);
	}

	inline static int32_t get_offset_of_satVSsat_6() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___satVSsat_6)); }
	inline ColorGradingCurve_t2000571184 * get_satVSsat_6() const { return ___satVSsat_6; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_satVSsat_6() { return &___satVSsat_6; }
	inline void set_satVSsat_6(ColorGradingCurve_t2000571184 * value)
	{
		___satVSsat_6 = value;
		Il2CppCodeGenWriteBarrier((&___satVSsat_6), value);
	}

	inline static int32_t get_offset_of_lumVSsat_7() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___lumVSsat_7)); }
	inline ColorGradingCurve_t2000571184 * get_lumVSsat_7() const { return ___lumVSsat_7; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_lumVSsat_7() { return &___lumVSsat_7; }
	inline void set_lumVSsat_7(ColorGradingCurve_t2000571184 * value)
	{
		___lumVSsat_7 = value;
		Il2CppCodeGenWriteBarrier((&___lumVSsat_7), value);
	}

	inline static int32_t get_offset_of_e_CurrentEditingCurve_8() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurrentEditingCurve_8)); }
	inline int32_t get_e_CurrentEditingCurve_8() const { return ___e_CurrentEditingCurve_8; }
	inline int32_t* get_address_of_e_CurrentEditingCurve_8() { return &___e_CurrentEditingCurve_8; }
	inline void set_e_CurrentEditingCurve_8(int32_t value)
	{
		___e_CurrentEditingCurve_8 = value;
	}

	inline static int32_t get_offset_of_e_CurveY_9() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveY_9)); }
	inline bool get_e_CurveY_9() const { return ___e_CurveY_9; }
	inline bool* get_address_of_e_CurveY_9() { return &___e_CurveY_9; }
	inline void set_e_CurveY_9(bool value)
	{
		___e_CurveY_9 = value;
	}

	inline static int32_t get_offset_of_e_CurveR_10() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveR_10)); }
	inline bool get_e_CurveR_10() const { return ___e_CurveR_10; }
	inline bool* get_address_of_e_CurveR_10() { return &___e_CurveR_10; }
	inline void set_e_CurveR_10(bool value)
	{
		___e_CurveR_10 = value;
	}

	inline static int32_t get_offset_of_e_CurveG_11() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveG_11)); }
	inline bool get_e_CurveG_11() const { return ___e_CurveG_11; }
	inline bool* get_address_of_e_CurveG_11() { return &___e_CurveG_11; }
	inline void set_e_CurveG_11(bool value)
	{
		___e_CurveG_11 = value;
	}

	inline static int32_t get_offset_of_e_CurveB_12() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveB_12)); }
	inline bool get_e_CurveB_12() const { return ___e_CurveB_12; }
	inline bool* get_address_of_e_CurveB_12() { return &___e_CurveB_12; }
	inline void set_e_CurveB_12(bool value)
	{
		___e_CurveB_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct CurvesSettings_t2830270037_marshaled_pinvoke
{
	ColorGradingCurve_t2000571184 * ___master_0;
	ColorGradingCurve_t2000571184 * ___red_1;
	ColorGradingCurve_t2000571184 * ___green_2;
	ColorGradingCurve_t2000571184 * ___blue_3;
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	int32_t ___e_CurrentEditingCurve_8;
	int32_t ___e_CurveY_9;
	int32_t ___e_CurveR_10;
	int32_t ___e_CurveG_11;
	int32_t ___e_CurveB_12;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct CurvesSettings_t2830270037_marshaled_com
{
	ColorGradingCurve_t2000571184 * ___master_0;
	ColorGradingCurve_t2000571184 * ___red_1;
	ColorGradingCurve_t2000571184 * ___green_2;
	ColorGradingCurve_t2000571184 * ___blue_3;
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	int32_t ___e_CurrentEditingCurve_8;
	int32_t ___e_CurveY_9;
	int32_t ___e_CurveR_10;
	int32_t ___e_CurveG_11;
	int32_t ___e_CurveB_12;
};
#endif // CURVESSETTINGS_T2830270037_H
#ifndef SETTINGS_T2313396630_H
#define SETTINGS_T2313396630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringModel/Settings
struct  Settings_t2313396630 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Settings_t2313396630__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T2313396630_H
#ifndef SETTINGS_T224798599_H
#define SETTINGS_T224798599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogModel/Settings
struct  Settings_t224798599 
{
public:
	// System.Boolean UnityEngine.PostProcessing.FogModel/Settings::excludeSkybox
	bool ___excludeSkybox_0;

public:
	inline static int32_t get_offset_of_excludeSkybox_0() { return static_cast<int32_t>(offsetof(Settings_t224798599, ___excludeSkybox_0)); }
	inline bool get_excludeSkybox_0() const { return ___excludeSkybox_0; }
	inline bool* get_address_of_excludeSkybox_0() { return &___excludeSkybox_0; }
	inline void set_excludeSkybox_0(bool value)
	{
		___excludeSkybox_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.FogModel/Settings
struct Settings_t224798599_marshaled_pinvoke
{
	int32_t ___excludeSkybox_0;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.FogModel/Settings
struct Settings_t224798599_marshaled_com
{
	int32_t ___excludeSkybox_0;
};
#endif // SETTINGS_T224798599_H
#ifndef SETTINGS_T4123292438_H
#define SETTINGS_T4123292438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainModel/Settings
struct  Settings_t4123292438 
{
public:
	// System.Boolean UnityEngine.PostProcessing.GrainModel/Settings::colored
	bool ___colored_0;
	// System.Single UnityEngine.PostProcessing.GrainModel/Settings::intensity
	float ___intensity_1;
	// System.Single UnityEngine.PostProcessing.GrainModel/Settings::size
	float ___size_2;
	// System.Single UnityEngine.PostProcessing.GrainModel/Settings::luminanceContribution
	float ___luminanceContribution_3;

public:
	inline static int32_t get_offset_of_colored_0() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___colored_0)); }
	inline bool get_colored_0() const { return ___colored_0; }
	inline bool* get_address_of_colored_0() { return &___colored_0; }
	inline void set_colored_0(bool value)
	{
		___colored_0 = value;
	}

	inline static int32_t get_offset_of_intensity_1() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___intensity_1)); }
	inline float get_intensity_1() const { return ___intensity_1; }
	inline float* get_address_of_intensity_1() { return &___intensity_1; }
	inline void set_intensity_1(float value)
	{
		___intensity_1 = value;
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___size_2)); }
	inline float get_size_2() const { return ___size_2; }
	inline float* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(float value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_luminanceContribution_3() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___luminanceContribution_3)); }
	inline float get_luminanceContribution_3() const { return ___luminanceContribution_3; }
	inline float* get_address_of_luminanceContribution_3() { return &___luminanceContribution_3; }
	inline void set_luminanceContribution_3(float value)
	{
		___luminanceContribution_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.GrainModel/Settings
struct Settings_t4123292438_marshaled_pinvoke
{
	int32_t ___colored_0;
	float ___intensity_1;
	float ___size_2;
	float ___luminanceContribution_3;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.GrainModel/Settings
struct Settings_t4123292438_marshaled_com
{
	int32_t ___colored_0;
	float ___intensity_1;
	float ___size_2;
	float ___luminanceContribution_3;
};
#endif // SETTINGS_T4123292438_H
#ifndef SETTINGS_T4282162361_H
#define SETTINGS_T4282162361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurModel/Settings
struct  Settings_t4282162361 
{
public:
	// System.Single UnityEngine.PostProcessing.MotionBlurModel/Settings::shutterAngle
	float ___shutterAngle_0;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurModel/Settings::sampleCount
	int32_t ___sampleCount_1;
	// System.Single UnityEngine.PostProcessing.MotionBlurModel/Settings::frameBlending
	float ___frameBlending_2;

public:
	inline static int32_t get_offset_of_shutterAngle_0() { return static_cast<int32_t>(offsetof(Settings_t4282162361, ___shutterAngle_0)); }
	inline float get_shutterAngle_0() const { return ___shutterAngle_0; }
	inline float* get_address_of_shutterAngle_0() { return &___shutterAngle_0; }
	inline void set_shutterAngle_0(float value)
	{
		___shutterAngle_0 = value;
	}

	inline static int32_t get_offset_of_sampleCount_1() { return static_cast<int32_t>(offsetof(Settings_t4282162361, ___sampleCount_1)); }
	inline int32_t get_sampleCount_1() const { return ___sampleCount_1; }
	inline int32_t* get_address_of_sampleCount_1() { return &___sampleCount_1; }
	inline void set_sampleCount_1(int32_t value)
	{
		___sampleCount_1 = value;
	}

	inline static int32_t get_offset_of_frameBlending_2() { return static_cast<int32_t>(offsetof(Settings_t4282162361, ___frameBlending_2)); }
	inline float get_frameBlending_2() const { return ___frameBlending_2; }
	inline float* get_address_of_frameBlending_2() { return &___frameBlending_2; }
	inline void set_frameBlending_2(float value)
	{
		___frameBlending_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T4282162361_H
#ifndef INTENSITYSETTINGS_T1721872184_H
#define INTENSITYSETTINGS_T1721872184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings
struct  IntensitySettings_t1721872184 
{
public:
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::reflectionMultiplier
	float ___reflectionMultiplier_0;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::fadeDistance
	float ___fadeDistance_1;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::fresnelFade
	float ___fresnelFade_2;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::fresnelFadePower
	float ___fresnelFadePower_3;

public:
	inline static int32_t get_offset_of_reflectionMultiplier_0() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___reflectionMultiplier_0)); }
	inline float get_reflectionMultiplier_0() const { return ___reflectionMultiplier_0; }
	inline float* get_address_of_reflectionMultiplier_0() { return &___reflectionMultiplier_0; }
	inline void set_reflectionMultiplier_0(float value)
	{
		___reflectionMultiplier_0 = value;
	}

	inline static int32_t get_offset_of_fadeDistance_1() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___fadeDistance_1)); }
	inline float get_fadeDistance_1() const { return ___fadeDistance_1; }
	inline float* get_address_of_fadeDistance_1() { return &___fadeDistance_1; }
	inline void set_fadeDistance_1(float value)
	{
		___fadeDistance_1 = value;
	}

	inline static int32_t get_offset_of_fresnelFade_2() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___fresnelFade_2)); }
	inline float get_fresnelFade_2() const { return ___fresnelFade_2; }
	inline float* get_address_of_fresnelFade_2() { return &___fresnelFade_2; }
	inline void set_fresnelFade_2(float value)
	{
		___fresnelFade_2 = value;
	}

	inline static int32_t get_offset_of_fresnelFadePower_3() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___fresnelFadePower_3)); }
	inline float get_fresnelFadePower_3() const { return ___fresnelFadePower_3; }
	inline float* get_address_of_fresnelFadePower_3() { return &___fresnelFadePower_3; }
	inline void set_fresnelFadePower_3(float value)
	{
		___fresnelFadePower_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTENSITYSETTINGS_T1721872184_H
#ifndef SCREENEDGEMASK_T4063288584_H
#define SCREENEDGEMASK_T4063288584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ScreenEdgeMask
struct  ScreenEdgeMask_t4063288584 
{
public:
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ScreenEdgeMask::intensity
	float ___intensity_0;

public:
	inline static int32_t get_offset_of_intensity_0() { return static_cast<int32_t>(offsetof(ScreenEdgeMask_t4063288584, ___intensity_0)); }
	inline float get_intensity_0() const { return ___intensity_0; }
	inline float* get_address_of_intensity_0() { return &___intensity_0; }
	inline void set_intensity_0(float value)
	{
		___intensity_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENEDGEMASK_T4063288584_H
#ifndef SETTINGS_T3006579223_H
#define SETTINGS_T3006579223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutModel/Settings
struct  Settings_t3006579223 
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.UserLutModel/Settings::lut
	Texture2D_t3840446185 * ___lut_0;
	// System.Single UnityEngine.PostProcessing.UserLutModel/Settings::contribution
	float ___contribution_1;

public:
	inline static int32_t get_offset_of_lut_0() { return static_cast<int32_t>(offsetof(Settings_t3006579223, ___lut_0)); }
	inline Texture2D_t3840446185 * get_lut_0() const { return ___lut_0; }
	inline Texture2D_t3840446185 ** get_address_of_lut_0() { return &___lut_0; }
	inline void set_lut_0(Texture2D_t3840446185 * value)
	{
		___lut_0 = value;
		Il2CppCodeGenWriteBarrier((&___lut_0), value);
	}

	inline static int32_t get_offset_of_contribution_1() { return static_cast<int32_t>(offsetof(Settings_t3006579223, ___contribution_1)); }
	inline float get_contribution_1() const { return ___contribution_1; }
	inline float* get_address_of_contribution_1() { return &___contribution_1; }
	inline void set_contribution_1(float value)
	{
		___contribution_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.UserLutModel/Settings
struct Settings_t3006579223_marshaled_pinvoke
{
	Texture2D_t3840446185 * ___lut_0;
	float ___contribution_1;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.UserLutModel/Settings
struct Settings_t3006579223_marshaled_com
{
	Texture2D_t3840446185 * ___lut_0;
	float ___contribution_1;
};
#endif // SETTINGS_T3006579223_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef CHANNELMIXERSETTINGS_T898701698_H
#define CHANNELMIXERSETTINGS_T898701698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings
struct  ChannelMixerSettings_t898701698 
{
public:
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::red
	Vector3_t3722313464  ___red_0;
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::green
	Vector3_t3722313464  ___green_1;
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::blue
	Vector3_t3722313464  ___blue_2;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::currentEditingChannel
	int32_t ___currentEditingChannel_3;

public:
	inline static int32_t get_offset_of_red_0() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___red_0)); }
	inline Vector3_t3722313464  get_red_0() const { return ___red_0; }
	inline Vector3_t3722313464 * get_address_of_red_0() { return &___red_0; }
	inline void set_red_0(Vector3_t3722313464  value)
	{
		___red_0 = value;
	}

	inline static int32_t get_offset_of_green_1() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___green_1)); }
	inline Vector3_t3722313464  get_green_1() const { return ___green_1; }
	inline Vector3_t3722313464 * get_address_of_green_1() { return &___green_1; }
	inline void set_green_1(Vector3_t3722313464  value)
	{
		___green_1 = value;
	}

	inline static int32_t get_offset_of_blue_2() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___blue_2)); }
	inline Vector3_t3722313464  get_blue_2() const { return ___blue_2; }
	inline Vector3_t3722313464 * get_address_of_blue_2() { return &___blue_2; }
	inline void set_blue_2(Vector3_t3722313464  value)
	{
		___blue_2 = value;
	}

	inline static int32_t get_offset_of_currentEditingChannel_3() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___currentEditingChannel_3)); }
	inline int32_t get_currentEditingChannel_3() const { return ___currentEditingChannel_3; }
	inline int32_t* get_address_of_currentEditingChannel_3() { return &___currentEditingChannel_3; }
	inline void set_currentEditingChannel_3(int32_t value)
	{
		___currentEditingChannel_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELMIXERSETTINGS_T898701698_H
#ifndef COLORWHEELMODE_T1939415375_H
#define COLORWHEELMODE_T1939415375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode
struct  ColorWheelMode_t1939415375 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorWheelMode_t1939415375, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWHEELMODE_T1939415375_H
#ifndef LINEARWHEELSSETTINGS_T3897781309_H
#define LINEARWHEELSSETTINGS_T3897781309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings
struct  LinearWheelsSettings_t3897781309 
{
public:
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::lift
	Color_t2555686324  ___lift_0;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::gamma
	Color_t2555686324  ___gamma_1;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::gain
	Color_t2555686324  ___gain_2;

public:
	inline static int32_t get_offset_of_lift_0() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___lift_0)); }
	inline Color_t2555686324  get_lift_0() const { return ___lift_0; }
	inline Color_t2555686324 * get_address_of_lift_0() { return &___lift_0; }
	inline void set_lift_0(Color_t2555686324  value)
	{
		___lift_0 = value;
	}

	inline static int32_t get_offset_of_gamma_1() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___gamma_1)); }
	inline Color_t2555686324  get_gamma_1() const { return ___gamma_1; }
	inline Color_t2555686324 * get_address_of_gamma_1() { return &___gamma_1; }
	inline void set_gamma_1(Color_t2555686324  value)
	{
		___gamma_1 = value;
	}

	inline static int32_t get_offset_of_gain_2() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___gain_2)); }
	inline Color_t2555686324  get_gain_2() const { return ___gain_2; }
	inline Color_t2555686324 * get_address_of_gain_2() { return &___gain_2; }
	inline void set_gain_2(Color_t2555686324  value)
	{
		___gain_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEARWHEELSSETTINGS_T3897781309_H
#ifndef LOGWHEELSSETTINGS_T1545220311_H
#define LOGWHEELSSETTINGS_T1545220311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings
struct  LogWheelsSettings_t1545220311 
{
public:
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::slope
	Color_t2555686324  ___slope_0;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::power
	Color_t2555686324  ___power_1;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::offset
	Color_t2555686324  ___offset_2;

public:
	inline static int32_t get_offset_of_slope_0() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___slope_0)); }
	inline Color_t2555686324  get_slope_0() const { return ___slope_0; }
	inline Color_t2555686324 * get_address_of_slope_0() { return &___slope_0; }
	inline void set_slope_0(Color_t2555686324  value)
	{
		___slope_0 = value;
	}

	inline static int32_t get_offset_of_power_1() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___power_1)); }
	inline Color_t2555686324  get_power_1() const { return ___power_1; }
	inline Color_t2555686324 * get_address_of_power_1() { return &___power_1; }
	inline void set_power_1(Color_t2555686324  value)
	{
		___power_1 = value;
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___offset_2)); }
	inline Color_t2555686324  get_offset_2() const { return ___offset_2; }
	inline Color_t2555686324 * get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(Color_t2555686324  value)
	{
		___offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGWHEELSSETTINGS_T1545220311_H
#ifndef TONEMAPPER_T1404353651_H
#define TONEMAPPER_T1404353651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/Tonemapper
struct  Tonemapper_t1404353651 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/Tonemapper::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Tonemapper_t1404353651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPER_T1404353651_H
#ifndef KERNELSIZE_T2406218613_H
#define KERNELSIZE_T2406218613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldModel/KernelSize
struct  KernelSize_t2406218613 
{
public:
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldModel/KernelSize::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KernelSize_t2406218613, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNELSIZE_T2406218613_H
#ifndef DITHERINGMODEL_T2429005396_H
#define DITHERINGMODEL_T2429005396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringModel
struct  DitheringModel_t2429005396  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.DitheringModel/Settings UnityEngine.PostProcessing.DitheringModel::m_Settings
	Settings_t2313396630  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(DitheringModel_t2429005396, ___m_Settings_1)); }
	inline Settings_t2313396630  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t2313396630 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t2313396630  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DITHERINGMODEL_T2429005396_H
#ifndef EYEADAPTATIONTYPE_T3053468307_H
#define EYEADAPTATIONTYPE_T3053468307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationModel/EyeAdaptationType
struct  EyeAdaptationType_t3053468307 
{
public:
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationModel/EyeAdaptationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EyeAdaptationType_t3053468307, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONTYPE_T3053468307_H
#ifndef FOGMODEL_T3620688749_H
#define FOGMODEL_T3620688749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogModel
struct  FogModel_t3620688749  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.FogModel/Settings UnityEngine.PostProcessing.FogModel::m_Settings
	Settings_t224798599  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(FogModel_t3620688749, ___m_Settings_1)); }
	inline Settings_t224798599  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t224798599 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t224798599  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOGMODEL_T3620688749_H
#ifndef GRAINMODEL_T1152882488_H
#define GRAINMODEL_T1152882488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainModel
struct  GrainModel_t1152882488  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.GrainModel/Settings UnityEngine.PostProcessing.GrainModel::m_Settings
	Settings_t4123292438  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(GrainModel_t1152882488, ___m_Settings_1)); }
	inline Settings_t4123292438  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t4123292438 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t4123292438  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAINMODEL_T1152882488_H
#ifndef MOTIONBLURMODEL_T3080286123_H
#define MOTIONBLURMODEL_T3080286123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurModel
struct  MotionBlurModel_t3080286123  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.MotionBlurModel/Settings UnityEngine.PostProcessing.MotionBlurModel::m_Settings
	Settings_t4282162361  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(MotionBlurModel_t3080286123, ___m_Settings_1)); }
	inline Settings_t4282162361  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t4282162361 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t4282162361  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLURMODEL_T3080286123_H
#ifndef SSRREFLECTIONBLENDTYPE_T3026770880_H
#define SSRREFLECTIONBLENDTYPE_T3026770880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRReflectionBlendType
struct  SSRReflectionBlendType_t3026770880 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRReflectionBlendType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SSRReflectionBlendType_t3026770880, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSRREFLECTIONBLENDTYPE_T3026770880_H
#ifndef SSRRESOLUTION_T161222554_H
#define SSRRESOLUTION_T161222554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRResolution
struct  SSRResolution_t161222554 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRResolution::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SSRResolution_t161222554, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSRRESOLUTION_T161222554_H
#ifndef USERLUTMODEL_T1670108080_H
#define USERLUTMODEL_T1670108080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutModel
struct  UserLutModel_t1670108080  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.UserLutModel/Settings UnityEngine.PostProcessing.UserLutModel::m_Settings
	Settings_t3006579223  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(UserLutModel_t1670108080, ___m_Settings_1)); }
	inline Settings_t3006579223  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t3006579223 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t3006579223  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERLUTMODEL_T1670108080_H
#ifndef MODE_T3936508933_H
#define MODE_T3936508933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteModel/Mode
struct  Mode_t3936508933 
{
public:
	// System.Int32 UnityEngine.PostProcessing.VignetteModel/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3936508933, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3936508933_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef ALIGNMODE_T432429434_H
#define ALIGNMODE_T432429434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode
struct  AlignMode_t432429434 
{
public:
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlignMode_t432429434, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMODE_T432429434_H
#ifndef MODE_T2106834709_H
#define MODE_T2106834709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode
struct  Mode_t2106834709 
{
public:
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t2106834709, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T2106834709_H
#ifndef MODE_T3024470803_H
#define MODE_T3024470803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger/Mode
struct  Mode_t3024470803 
{
public:
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3024470803, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3024470803_H
#ifndef CAMERAREFOCUS_T4263235746_H
#define CAMERAREFOCUS_T4263235746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CameraRefocus
struct  CameraRefocus_t4263235746  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.CameraRefocus::Camera
	Camera_t4157153871 * ___Camera_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::Lookatpoint
	Vector3_t3722313464  ___Lookatpoint_1;
	// UnityEngine.Transform UnityStandardAssets.Utility.CameraRefocus::Parent
	Transform_t3600365921 * ___Parent_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::m_OrigCameraPos
	Vector3_t3722313464  ___m_OrigCameraPos_3;
	// System.Boolean UnityStandardAssets.Utility.CameraRefocus::m_Refocus
	bool ___m_Refocus_4;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_Lookatpoint_1() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Lookatpoint_1)); }
	inline Vector3_t3722313464  get_Lookatpoint_1() const { return ___Lookatpoint_1; }
	inline Vector3_t3722313464 * get_address_of_Lookatpoint_1() { return &___Lookatpoint_1; }
	inline void set_Lookatpoint_1(Vector3_t3722313464  value)
	{
		___Lookatpoint_1 = value;
	}

	inline static int32_t get_offset_of_Parent_2() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Parent_2)); }
	inline Transform_t3600365921 * get_Parent_2() const { return ___Parent_2; }
	inline Transform_t3600365921 ** get_address_of_Parent_2() { return &___Parent_2; }
	inline void set_Parent_2(Transform_t3600365921 * value)
	{
		___Parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_2), value);
	}

	inline static int32_t get_offset_of_m_OrigCameraPos_3() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_OrigCameraPos_3)); }
	inline Vector3_t3722313464  get_m_OrigCameraPos_3() const { return ___m_OrigCameraPos_3; }
	inline Vector3_t3722313464 * get_address_of_m_OrigCameraPos_3() { return &___m_OrigCameraPos_3; }
	inline void set_m_OrigCameraPos_3(Vector3_t3722313464  value)
	{
		___m_OrigCameraPos_3 = value;
	}

	inline static int32_t get_offset_of_m_Refocus_4() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_Refocus_4)); }
	inline bool get_m_Refocus_4() const { return ___m_Refocus_4; }
	inline bool* get_address_of_m_Refocus_4() { return &___m_Refocus_4; }
	inline void set_m_Refocus_4(bool value)
	{
		___m_Refocus_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAREFOCUS_T4263235746_H
#ifndef CURVECONTROLLEDBOB_T2679313829_H
#define CURVECONTROLLEDBOB_T2679313829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CurveControlledBob
struct  CurveControlledBob_t2679313829  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::HorizontalBobRange
	float ___HorizontalBobRange_0;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticalBobRange
	float ___VerticalBobRange_1;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.CurveControlledBob::Bobcurve
	AnimationCurve_t3046754366 * ___Bobcurve_2;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticaltoHorizontalRatio
	float ___VerticaltoHorizontalRatio_3;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionX
	float ___m_CyclePositionX_4;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionY
	float ___m_CyclePositionY_5;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_BobBaseInterval
	float ___m_BobBaseInterval_6;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_7;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_Time
	float ___m_Time_8;

public:
	inline static int32_t get_offset_of_HorizontalBobRange_0() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___HorizontalBobRange_0)); }
	inline float get_HorizontalBobRange_0() const { return ___HorizontalBobRange_0; }
	inline float* get_address_of_HorizontalBobRange_0() { return &___HorizontalBobRange_0; }
	inline void set_HorizontalBobRange_0(float value)
	{
		___HorizontalBobRange_0 = value;
	}

	inline static int32_t get_offset_of_VerticalBobRange_1() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticalBobRange_1)); }
	inline float get_VerticalBobRange_1() const { return ___VerticalBobRange_1; }
	inline float* get_address_of_VerticalBobRange_1() { return &___VerticalBobRange_1; }
	inline void set_VerticalBobRange_1(float value)
	{
		___VerticalBobRange_1 = value;
	}

	inline static int32_t get_offset_of_Bobcurve_2() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___Bobcurve_2)); }
	inline AnimationCurve_t3046754366 * get_Bobcurve_2() const { return ___Bobcurve_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_Bobcurve_2() { return &___Bobcurve_2; }
	inline void set_Bobcurve_2(AnimationCurve_t3046754366 * value)
	{
		___Bobcurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___Bobcurve_2), value);
	}

	inline static int32_t get_offset_of_VerticaltoHorizontalRatio_3() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticaltoHorizontalRatio_3)); }
	inline float get_VerticaltoHorizontalRatio_3() const { return ___VerticaltoHorizontalRatio_3; }
	inline float* get_address_of_VerticaltoHorizontalRatio_3() { return &___VerticaltoHorizontalRatio_3; }
	inline void set_VerticaltoHorizontalRatio_3(float value)
	{
		___VerticaltoHorizontalRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionX_4() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionX_4)); }
	inline float get_m_CyclePositionX_4() const { return ___m_CyclePositionX_4; }
	inline float* get_address_of_m_CyclePositionX_4() { return &___m_CyclePositionX_4; }
	inline void set_m_CyclePositionX_4(float value)
	{
		___m_CyclePositionX_4 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionY_5() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionY_5)); }
	inline float get_m_CyclePositionY_5() const { return ___m_CyclePositionY_5; }
	inline float* get_address_of_m_CyclePositionY_5() { return &___m_CyclePositionY_5; }
	inline void set_m_CyclePositionY_5(float value)
	{
		___m_CyclePositionY_5 = value;
	}

	inline static int32_t get_offset_of_m_BobBaseInterval_6() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_BobBaseInterval_6)); }
	inline float get_m_BobBaseInterval_6() const { return ___m_BobBaseInterval_6; }
	inline float* get_address_of_m_BobBaseInterval_6() { return &___m_BobBaseInterval_6; }
	inline void set_m_BobBaseInterval_6(float value)
	{
		___m_BobBaseInterval_6 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_7() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_OriginalCameraPosition_7)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_7() const { return ___m_OriginalCameraPosition_7; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_7() { return &___m_OriginalCameraPosition_7; }
	inline void set_m_OriginalCameraPosition_7(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_Time_8() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_Time_8)); }
	inline float get_m_Time_8() const { return ___m_Time_8; }
	inline float* get_address_of_m_Time_8() { return &___m_Time_8; }
	inline void set_m_Time_8(float value)
	{
		___m_Time_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVECONTROLLEDBOB_T2679313829_H
#ifndef BUILDTARGETGROUP_T72322187_H
#define BUILDTARGETGROUP_T72322187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
struct  BuildTargetGroup_t72322187 
{
public:
	// System.Int32 UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BuildTargetGroup_t72322187, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDTARGETGROUP_T72322187_H
#ifndef ACTION_T837364808_H
#define ACTION_T837364808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Action
struct  Action_t837364808 
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/Action::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Action_t837364808, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T837364808_H
#ifndef ROUTEPOINT_T3880028948_H
#define ROUTEPOINT_T3880028948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
struct  RoutePoint_t3880028948 
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::direction
	Vector3_t3722313464  ___direction_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(RoutePoint_t3880028948, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(RoutePoint_t3880028948, ___direction_1)); }
	inline Vector3_t3722313464  get_direction_1() const { return ___direction_1; }
	inline Vector3_t3722313464 * get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(Vector3_t3722313464  value)
	{
		___direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUTEPOINT_T3880028948_H
#ifndef PROGRESSSTYLE_T3254572979_H
#define PROGRESSSTYLE_T3254572979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle
struct  ProgressStyle_t3254572979 
{
public:
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProgressStyle_t3254572979, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSSTYLE_T3254572979_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef COLORWHEELSSETTINGS_T3120867866_H
#define COLORWHEELSSETTINGS_T3120867866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings
struct  ColorWheelsSettings_t3120867866 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::mode
	int32_t ___mode_0;
	// UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::log
	LogWheelsSettings_t1545220311  ___log_1;
	// UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::linear
	LinearWheelsSettings_t3897781309  ___linear_2;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_log_1() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___log_1)); }
	inline LogWheelsSettings_t1545220311  get_log_1() const { return ___log_1; }
	inline LogWheelsSettings_t1545220311 * get_address_of_log_1() { return &___log_1; }
	inline void set_log_1(LogWheelsSettings_t1545220311  value)
	{
		___log_1 = value;
	}

	inline static int32_t get_offset_of_linear_2() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___linear_2)); }
	inline LinearWheelsSettings_t3897781309  get_linear_2() const { return ___linear_2; }
	inline LinearWheelsSettings_t3897781309 * get_address_of_linear_2() { return &___linear_2; }
	inline void set_linear_2(LinearWheelsSettings_t3897781309  value)
	{
		___linear_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWHEELSSETTINGS_T3120867866_H
#ifndef TONEMAPPINGSETTINGS_T4154044775_H
#define TONEMAPPINGSETTINGS_T4154044775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings
struct  TonemappingSettings_t4154044775 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/Tonemapper UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::tonemapper
	int32_t ___tonemapper_0;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralBlackIn
	float ___neutralBlackIn_1;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteIn
	float ___neutralWhiteIn_2;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralBlackOut
	float ___neutralBlackOut_3;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteOut
	float ___neutralWhiteOut_4;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteLevel
	float ___neutralWhiteLevel_5;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteClip
	float ___neutralWhiteClip_6;

public:
	inline static int32_t get_offset_of_tonemapper_0() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___tonemapper_0)); }
	inline int32_t get_tonemapper_0() const { return ___tonemapper_0; }
	inline int32_t* get_address_of_tonemapper_0() { return &___tonemapper_0; }
	inline void set_tonemapper_0(int32_t value)
	{
		___tonemapper_0 = value;
	}

	inline static int32_t get_offset_of_neutralBlackIn_1() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralBlackIn_1)); }
	inline float get_neutralBlackIn_1() const { return ___neutralBlackIn_1; }
	inline float* get_address_of_neutralBlackIn_1() { return &___neutralBlackIn_1; }
	inline void set_neutralBlackIn_1(float value)
	{
		___neutralBlackIn_1 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteIn_2() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteIn_2)); }
	inline float get_neutralWhiteIn_2() const { return ___neutralWhiteIn_2; }
	inline float* get_address_of_neutralWhiteIn_2() { return &___neutralWhiteIn_2; }
	inline void set_neutralWhiteIn_2(float value)
	{
		___neutralWhiteIn_2 = value;
	}

	inline static int32_t get_offset_of_neutralBlackOut_3() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralBlackOut_3)); }
	inline float get_neutralBlackOut_3() const { return ___neutralBlackOut_3; }
	inline float* get_address_of_neutralBlackOut_3() { return &___neutralBlackOut_3; }
	inline void set_neutralBlackOut_3(float value)
	{
		___neutralBlackOut_3 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteOut_4() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteOut_4)); }
	inline float get_neutralWhiteOut_4() const { return ___neutralWhiteOut_4; }
	inline float* get_address_of_neutralWhiteOut_4() { return &___neutralWhiteOut_4; }
	inline void set_neutralWhiteOut_4(float value)
	{
		___neutralWhiteOut_4 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteLevel_5() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteLevel_5)); }
	inline float get_neutralWhiteLevel_5() const { return ___neutralWhiteLevel_5; }
	inline float* get_address_of_neutralWhiteLevel_5() { return &___neutralWhiteLevel_5; }
	inline void set_neutralWhiteLevel_5(float value)
	{
		___neutralWhiteLevel_5 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteClip_6() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteClip_6)); }
	inline float get_neutralWhiteClip_6() const { return ___neutralWhiteClip_6; }
	inline float* get_address_of_neutralWhiteClip_6() { return &___neutralWhiteClip_6; }
	inline void set_neutralWhiteClip_6(float value)
	{
		___neutralWhiteClip_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPINGSETTINGS_T4154044775_H
#ifndef SETTINGS_T2195468135_H
#define SETTINGS_T2195468135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldModel/Settings
struct  Settings_t2195468135 
{
public:
	// System.Single UnityEngine.PostProcessing.DepthOfFieldModel/Settings::focusDistance
	float ___focusDistance_0;
	// System.Single UnityEngine.PostProcessing.DepthOfFieldModel/Settings::aperture
	float ___aperture_1;
	// System.Single UnityEngine.PostProcessing.DepthOfFieldModel/Settings::focalLength
	float ___focalLength_2;
	// System.Boolean UnityEngine.PostProcessing.DepthOfFieldModel/Settings::useCameraFov
	bool ___useCameraFov_3;
	// UnityEngine.PostProcessing.DepthOfFieldModel/KernelSize UnityEngine.PostProcessing.DepthOfFieldModel/Settings::kernelSize
	int32_t ___kernelSize_4;

public:
	inline static int32_t get_offset_of_focusDistance_0() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___focusDistance_0)); }
	inline float get_focusDistance_0() const { return ___focusDistance_0; }
	inline float* get_address_of_focusDistance_0() { return &___focusDistance_0; }
	inline void set_focusDistance_0(float value)
	{
		___focusDistance_0 = value;
	}

	inline static int32_t get_offset_of_aperture_1() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___aperture_1)); }
	inline float get_aperture_1() const { return ___aperture_1; }
	inline float* get_address_of_aperture_1() { return &___aperture_1; }
	inline void set_aperture_1(float value)
	{
		___aperture_1 = value;
	}

	inline static int32_t get_offset_of_focalLength_2() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___focalLength_2)); }
	inline float get_focalLength_2() const { return ___focalLength_2; }
	inline float* get_address_of_focalLength_2() { return &___focalLength_2; }
	inline void set_focalLength_2(float value)
	{
		___focalLength_2 = value;
	}

	inline static int32_t get_offset_of_useCameraFov_3() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___useCameraFov_3)); }
	inline bool get_useCameraFov_3() const { return ___useCameraFov_3; }
	inline bool* get_address_of_useCameraFov_3() { return &___useCameraFov_3; }
	inline void set_useCameraFov_3(bool value)
	{
		___useCameraFov_3 = value;
	}

	inline static int32_t get_offset_of_kernelSize_4() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___kernelSize_4)); }
	inline int32_t get_kernelSize_4() const { return ___kernelSize_4; }
	inline int32_t* get_address_of_kernelSize_4() { return &___kernelSize_4; }
	inline void set_kernelSize_4(int32_t value)
	{
		___kernelSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.DepthOfFieldModel/Settings
struct Settings_t2195468135_marshaled_pinvoke
{
	float ___focusDistance_0;
	float ___aperture_1;
	float ___focalLength_2;
	int32_t ___useCameraFov_3;
	int32_t ___kernelSize_4;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.DepthOfFieldModel/Settings
struct Settings_t2195468135_marshaled_com
{
	float ___focusDistance_0;
	float ___aperture_1;
	float ___focalLength_2;
	int32_t ___useCameraFov_3;
	int32_t ___kernelSize_4;
};
#endif // SETTINGS_T2195468135_H
#ifndef SETTINGS_T2874244444_H
#define SETTINGS_T2874244444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationModel/Settings
struct  Settings_t2874244444 
{
public:
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::lowPercent
	float ___lowPercent_0;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::highPercent
	float ___highPercent_1;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::minLuminance
	float ___minLuminance_2;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::maxLuminance
	float ___maxLuminance_3;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::keyValue
	float ___keyValue_4;
	// System.Boolean UnityEngine.PostProcessing.EyeAdaptationModel/Settings::dynamicKeyValue
	bool ___dynamicKeyValue_5;
	// UnityEngine.PostProcessing.EyeAdaptationModel/EyeAdaptationType UnityEngine.PostProcessing.EyeAdaptationModel/Settings::adaptationType
	int32_t ___adaptationType_6;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::speedUp
	float ___speedUp_7;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::speedDown
	float ___speedDown_8;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationModel/Settings::logMin
	int32_t ___logMin_9;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationModel/Settings::logMax
	int32_t ___logMax_10;

public:
	inline static int32_t get_offset_of_lowPercent_0() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___lowPercent_0)); }
	inline float get_lowPercent_0() const { return ___lowPercent_0; }
	inline float* get_address_of_lowPercent_0() { return &___lowPercent_0; }
	inline void set_lowPercent_0(float value)
	{
		___lowPercent_0 = value;
	}

	inline static int32_t get_offset_of_highPercent_1() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___highPercent_1)); }
	inline float get_highPercent_1() const { return ___highPercent_1; }
	inline float* get_address_of_highPercent_1() { return &___highPercent_1; }
	inline void set_highPercent_1(float value)
	{
		___highPercent_1 = value;
	}

	inline static int32_t get_offset_of_minLuminance_2() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___minLuminance_2)); }
	inline float get_minLuminance_2() const { return ___minLuminance_2; }
	inline float* get_address_of_minLuminance_2() { return &___minLuminance_2; }
	inline void set_minLuminance_2(float value)
	{
		___minLuminance_2 = value;
	}

	inline static int32_t get_offset_of_maxLuminance_3() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___maxLuminance_3)); }
	inline float get_maxLuminance_3() const { return ___maxLuminance_3; }
	inline float* get_address_of_maxLuminance_3() { return &___maxLuminance_3; }
	inline void set_maxLuminance_3(float value)
	{
		___maxLuminance_3 = value;
	}

	inline static int32_t get_offset_of_keyValue_4() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___keyValue_4)); }
	inline float get_keyValue_4() const { return ___keyValue_4; }
	inline float* get_address_of_keyValue_4() { return &___keyValue_4; }
	inline void set_keyValue_4(float value)
	{
		___keyValue_4 = value;
	}

	inline static int32_t get_offset_of_dynamicKeyValue_5() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___dynamicKeyValue_5)); }
	inline bool get_dynamicKeyValue_5() const { return ___dynamicKeyValue_5; }
	inline bool* get_address_of_dynamicKeyValue_5() { return &___dynamicKeyValue_5; }
	inline void set_dynamicKeyValue_5(bool value)
	{
		___dynamicKeyValue_5 = value;
	}

	inline static int32_t get_offset_of_adaptationType_6() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___adaptationType_6)); }
	inline int32_t get_adaptationType_6() const { return ___adaptationType_6; }
	inline int32_t* get_address_of_adaptationType_6() { return &___adaptationType_6; }
	inline void set_adaptationType_6(int32_t value)
	{
		___adaptationType_6 = value;
	}

	inline static int32_t get_offset_of_speedUp_7() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___speedUp_7)); }
	inline float get_speedUp_7() const { return ___speedUp_7; }
	inline float* get_address_of_speedUp_7() { return &___speedUp_7; }
	inline void set_speedUp_7(float value)
	{
		___speedUp_7 = value;
	}

	inline static int32_t get_offset_of_speedDown_8() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___speedDown_8)); }
	inline float get_speedDown_8() const { return ___speedDown_8; }
	inline float* get_address_of_speedDown_8() { return &___speedDown_8; }
	inline void set_speedDown_8(float value)
	{
		___speedDown_8 = value;
	}

	inline static int32_t get_offset_of_logMin_9() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___logMin_9)); }
	inline int32_t get_logMin_9() const { return ___logMin_9; }
	inline int32_t* get_address_of_logMin_9() { return &___logMin_9; }
	inline void set_logMin_9(int32_t value)
	{
		___logMin_9 = value;
	}

	inline static int32_t get_offset_of_logMax_10() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___logMax_10)); }
	inline int32_t get_logMax_10() const { return ___logMax_10; }
	inline int32_t* get_address_of_logMax_10() { return &___logMax_10; }
	inline void set_logMax_10(int32_t value)
	{
		___logMax_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.EyeAdaptationModel/Settings
struct Settings_t2874244444_marshaled_pinvoke
{
	float ___lowPercent_0;
	float ___highPercent_1;
	float ___minLuminance_2;
	float ___maxLuminance_3;
	float ___keyValue_4;
	int32_t ___dynamicKeyValue_5;
	int32_t ___adaptationType_6;
	float ___speedUp_7;
	float ___speedDown_8;
	int32_t ___logMin_9;
	int32_t ___logMax_10;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.EyeAdaptationModel/Settings
struct Settings_t2874244444_marshaled_com
{
	float ___lowPercent_0;
	float ___highPercent_1;
	float ___minLuminance_2;
	float ___maxLuminance_3;
	float ___keyValue_4;
	int32_t ___dynamicKeyValue_5;
	int32_t ___adaptationType_6;
	float ___speedUp_7;
	float ___speedDown_8;
	int32_t ___logMin_9;
	int32_t ___logMax_10;
};
#endif // SETTINGS_T2874244444_H
#ifndef REFLECTIONSETTINGS_T282755190_H
#define REFLECTIONSETTINGS_T282755190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
struct  ReflectionSettings_t282755190 
{
public:
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRReflectionBlendType UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::blendType
	int32_t ___blendType_0;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRResolution UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::reflectionQuality
	int32_t ___reflectionQuality_1;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::maxDistance
	float ___maxDistance_2;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::iterationCount
	int32_t ___iterationCount_3;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::stepSize
	int32_t ___stepSize_4;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::widthModifier
	float ___widthModifier_5;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::reflectionBlur
	float ___reflectionBlur_6;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::reflectBackfaces
	bool ___reflectBackfaces_7;

public:
	inline static int32_t get_offset_of_blendType_0() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___blendType_0)); }
	inline int32_t get_blendType_0() const { return ___blendType_0; }
	inline int32_t* get_address_of_blendType_0() { return &___blendType_0; }
	inline void set_blendType_0(int32_t value)
	{
		___blendType_0 = value;
	}

	inline static int32_t get_offset_of_reflectionQuality_1() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___reflectionQuality_1)); }
	inline int32_t get_reflectionQuality_1() const { return ___reflectionQuality_1; }
	inline int32_t* get_address_of_reflectionQuality_1() { return &___reflectionQuality_1; }
	inline void set_reflectionQuality_1(int32_t value)
	{
		___reflectionQuality_1 = value;
	}

	inline static int32_t get_offset_of_maxDistance_2() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___maxDistance_2)); }
	inline float get_maxDistance_2() const { return ___maxDistance_2; }
	inline float* get_address_of_maxDistance_2() { return &___maxDistance_2; }
	inline void set_maxDistance_2(float value)
	{
		___maxDistance_2 = value;
	}

	inline static int32_t get_offset_of_iterationCount_3() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___iterationCount_3)); }
	inline int32_t get_iterationCount_3() const { return ___iterationCount_3; }
	inline int32_t* get_address_of_iterationCount_3() { return &___iterationCount_3; }
	inline void set_iterationCount_3(int32_t value)
	{
		___iterationCount_3 = value;
	}

	inline static int32_t get_offset_of_stepSize_4() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___stepSize_4)); }
	inline int32_t get_stepSize_4() const { return ___stepSize_4; }
	inline int32_t* get_address_of_stepSize_4() { return &___stepSize_4; }
	inline void set_stepSize_4(int32_t value)
	{
		___stepSize_4 = value;
	}

	inline static int32_t get_offset_of_widthModifier_5() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___widthModifier_5)); }
	inline float get_widthModifier_5() const { return ___widthModifier_5; }
	inline float* get_address_of_widthModifier_5() { return &___widthModifier_5; }
	inline void set_widthModifier_5(float value)
	{
		___widthModifier_5 = value;
	}

	inline static int32_t get_offset_of_reflectionBlur_6() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___reflectionBlur_6)); }
	inline float get_reflectionBlur_6() const { return ___reflectionBlur_6; }
	inline float* get_address_of_reflectionBlur_6() { return &___reflectionBlur_6; }
	inline void set_reflectionBlur_6(float value)
	{
		___reflectionBlur_6 = value;
	}

	inline static int32_t get_offset_of_reflectBackfaces_7() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___reflectBackfaces_7)); }
	inline bool get_reflectBackfaces_7() const { return ___reflectBackfaces_7; }
	inline bool* get_address_of_reflectBackfaces_7() { return &___reflectBackfaces_7; }
	inline void set_reflectBackfaces_7(bool value)
	{
		___reflectBackfaces_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
struct ReflectionSettings_t282755190_marshaled_pinvoke
{
	int32_t ___blendType_0;
	int32_t ___reflectionQuality_1;
	float ___maxDistance_2;
	int32_t ___iterationCount_3;
	int32_t ___stepSize_4;
	float ___widthModifier_5;
	float ___reflectionBlur_6;
	int32_t ___reflectBackfaces_7;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
struct ReflectionSettings_t282755190_marshaled_com
{
	int32_t ___blendType_0;
	int32_t ___reflectionQuality_1;
	float ___maxDistance_2;
	int32_t ___iterationCount_3;
	int32_t ___stepSize_4;
	float ___widthModifier_5;
	float ___reflectionBlur_6;
	int32_t ___reflectBackfaces_7;
};
#endif // REFLECTIONSETTINGS_T282755190_H
#ifndef SETTINGS_T1354494600_H
#define SETTINGS_T1354494600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteModel/Settings
struct  Settings_t1354494600 
{
public:
	// UnityEngine.PostProcessing.VignetteModel/Mode UnityEngine.PostProcessing.VignetteModel/Settings::mode
	int32_t ___mode_0;
	// UnityEngine.Color UnityEngine.PostProcessing.VignetteModel/Settings::color
	Color_t2555686324  ___color_1;
	// UnityEngine.Vector2 UnityEngine.PostProcessing.VignetteModel/Settings::center
	Vector2_t2156229523  ___center_2;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::intensity
	float ___intensity_3;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::smoothness
	float ___smoothness_4;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::roundness
	float ___roundness_5;
	// UnityEngine.Texture UnityEngine.PostProcessing.VignetteModel/Settings::mask
	Texture_t3661962703 * ___mask_6;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::opacity
	float ___opacity_7;
	// System.Boolean UnityEngine.PostProcessing.VignetteModel/Settings::rounded
	bool ___rounded_8;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___color_1)); }
	inline Color_t2555686324  get_color_1() const { return ___color_1; }
	inline Color_t2555686324 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t2555686324  value)
	{
		___color_1 = value;
	}

	inline static int32_t get_offset_of_center_2() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___center_2)); }
	inline Vector2_t2156229523  get_center_2() const { return ___center_2; }
	inline Vector2_t2156229523 * get_address_of_center_2() { return &___center_2; }
	inline void set_center_2(Vector2_t2156229523  value)
	{
		___center_2 = value;
	}

	inline static int32_t get_offset_of_intensity_3() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___intensity_3)); }
	inline float get_intensity_3() const { return ___intensity_3; }
	inline float* get_address_of_intensity_3() { return &___intensity_3; }
	inline void set_intensity_3(float value)
	{
		___intensity_3 = value;
	}

	inline static int32_t get_offset_of_smoothness_4() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___smoothness_4)); }
	inline float get_smoothness_4() const { return ___smoothness_4; }
	inline float* get_address_of_smoothness_4() { return &___smoothness_4; }
	inline void set_smoothness_4(float value)
	{
		___smoothness_4 = value;
	}

	inline static int32_t get_offset_of_roundness_5() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___roundness_5)); }
	inline float get_roundness_5() const { return ___roundness_5; }
	inline float* get_address_of_roundness_5() { return &___roundness_5; }
	inline void set_roundness_5(float value)
	{
		___roundness_5 = value;
	}

	inline static int32_t get_offset_of_mask_6() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___mask_6)); }
	inline Texture_t3661962703 * get_mask_6() const { return ___mask_6; }
	inline Texture_t3661962703 ** get_address_of_mask_6() { return &___mask_6; }
	inline void set_mask_6(Texture_t3661962703 * value)
	{
		___mask_6 = value;
		Il2CppCodeGenWriteBarrier((&___mask_6), value);
	}

	inline static int32_t get_offset_of_opacity_7() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___opacity_7)); }
	inline float get_opacity_7() const { return ___opacity_7; }
	inline float* get_address_of_opacity_7() { return &___opacity_7; }
	inline void set_opacity_7(float value)
	{
		___opacity_7 = value;
	}

	inline static int32_t get_offset_of_rounded_8() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___rounded_8)); }
	inline bool get_rounded_8() const { return ___rounded_8; }
	inline bool* get_address_of_rounded_8() { return &___rounded_8; }
	inline void set_rounded_8(bool value)
	{
		___rounded_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.VignetteModel/Settings
struct Settings_t1354494600_marshaled_pinvoke
{
	int32_t ___mode_0;
	Color_t2555686324  ___color_1;
	Vector2_t2156229523  ___center_2;
	float ___intensity_3;
	float ___smoothness_4;
	float ___roundness_5;
	Texture_t3661962703 * ___mask_6;
	float ___opacity_7;
	int32_t ___rounded_8;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.VignetteModel/Settings
struct Settings_t1354494600_marshaled_com
{
	int32_t ___mode_0;
	Color_t2555686324  ___color_1;
	Vector2_t2156229523  ___center_2;
	float ___intensity_3;
	float ___smoothness_4;
	float ___roundness_5;
	Texture_t3661962703 * ___mask_6;
	float ___opacity_7;
	int32_t ___rounded_8;
};
#endif // SETTINGS_T1354494600_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef DEMOPARTICLESYSTEM_T1195446716_H
#define DEMOPARTICLESYSTEM_T1195446716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem
struct  DemoParticleSystem_t1195446716  : public RuntimeObject
{
public:
	// UnityEngine.Transform UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::transform
	Transform_t3600365921 * ___transform_0;
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/Mode UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::mode
	int32_t ___mode_1;
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/AlignMode UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::align
	int32_t ___align_2;
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::maxCount
	int32_t ___maxCount_3;
	// System.Single UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::minDist
	float ___minDist_4;
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::camOffset
	int32_t ___camOffset_5;
	// System.String UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::instructionText
	String_t* ___instructionText_6;

public:
	inline static int32_t get_offset_of_transform_0() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___transform_0)); }
	inline Transform_t3600365921 * get_transform_0() const { return ___transform_0; }
	inline Transform_t3600365921 ** get_address_of_transform_0() { return &___transform_0; }
	inline void set_transform_0(Transform_t3600365921 * value)
	{
		___transform_0 = value;
		Il2CppCodeGenWriteBarrier((&___transform_0), value);
	}

	inline static int32_t get_offset_of_mode_1() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___mode_1)); }
	inline int32_t get_mode_1() const { return ___mode_1; }
	inline int32_t* get_address_of_mode_1() { return &___mode_1; }
	inline void set_mode_1(int32_t value)
	{
		___mode_1 = value;
	}

	inline static int32_t get_offset_of_align_2() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___align_2)); }
	inline int32_t get_align_2() const { return ___align_2; }
	inline int32_t* get_address_of_align_2() { return &___align_2; }
	inline void set_align_2(int32_t value)
	{
		___align_2 = value;
	}

	inline static int32_t get_offset_of_maxCount_3() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___maxCount_3)); }
	inline int32_t get_maxCount_3() const { return ___maxCount_3; }
	inline int32_t* get_address_of_maxCount_3() { return &___maxCount_3; }
	inline void set_maxCount_3(int32_t value)
	{
		___maxCount_3 = value;
	}

	inline static int32_t get_offset_of_minDist_4() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___minDist_4)); }
	inline float get_minDist_4() const { return ___minDist_4; }
	inline float* get_address_of_minDist_4() { return &___minDist_4; }
	inline void set_minDist_4(float value)
	{
		___minDist_4 = value;
	}

	inline static int32_t get_offset_of_camOffset_5() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___camOffset_5)); }
	inline int32_t get_camOffset_5() const { return ___camOffset_5; }
	inline int32_t* get_address_of_camOffset_5() { return &___camOffset_5; }
	inline void set_camOffset_5(int32_t value)
	{
		___camOffset_5 = value;
	}

	inline static int32_t get_offset_of_instructionText_6() { return static_cast<int32_t>(offsetof(DemoParticleSystem_t1195446716, ___instructionText_6)); }
	inline String_t* get_instructionText_6() const { return ___instructionText_6; }
	inline String_t** get_address_of_instructionText_6() { return &___instructionText_6; }
	inline void set_instructionText_6(String_t* value)
	{
		___instructionText_6 = value;
		Il2CppCodeGenWriteBarrier((&___instructionText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOPARTICLESYSTEM_T1195446716_H
#ifndef VECTOR3ANDSPACE_T219844479_H
#define VECTOR3ANDSPACE_T219844479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct  Vector3andSpace_t219844479  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::value
	Vector3_t3722313464  ___value_0;
	// UnityEngine.Space UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::space
	int32_t ___space_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___value_0)); }
	inline Vector3_t3722313464  get_value_0() const { return ___value_0; }
	inline Vector3_t3722313464 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t3722313464  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_space_1() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___space_1)); }
	inline int32_t get_space_1() const { return ___space_1; }
	inline int32_t* get_address_of_space_1() { return &___space_1; }
	inline void set_space_1(int32_t value)
	{
		___space_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ANDSPACE_T219844479_H
#ifndef ENTRY_T2725803170_H
#define ENTRY_T2725803170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct  Entry_t2725803170  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityStandardAssets.Utility.TimedObjectActivator/Entry::target
	GameObject_t1113636619 * ___target_0;
	// UnityStandardAssets.Utility.TimedObjectActivator/Action UnityStandardAssets.Utility.TimedObjectActivator/Entry::action
	int32_t ___action_1;
	// System.Single UnityStandardAssets.Utility.TimedObjectActivator/Entry::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___target_0)); }
	inline GameObject_t1113636619 * get_target_0() const { return ___target_0; }
	inline GameObject_t1113636619 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t1113636619 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___action_1)); }
	inline int32_t get_action_1() const { return ___action_1; }
	inline int32_t* get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(int32_t value)
	{
		___action_1 = value;
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2725803170_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef SETTINGS_T451872061_H
#define SETTINGS_T451872061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/Settings
struct  Settings_t451872061 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::tonemapping
	TonemappingSettings_t4154044775  ___tonemapping_0;
	// UnityEngine.PostProcessing.ColorGradingModel/BasicSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::basic
	BasicSettings_t838098426  ___basic_1;
	// UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::channelMixer
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::colorWheels
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	// UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::curves
	CurvesSettings_t2830270037  ___curves_4;

public:
	inline static int32_t get_offset_of_tonemapping_0() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___tonemapping_0)); }
	inline TonemappingSettings_t4154044775  get_tonemapping_0() const { return ___tonemapping_0; }
	inline TonemappingSettings_t4154044775 * get_address_of_tonemapping_0() { return &___tonemapping_0; }
	inline void set_tonemapping_0(TonemappingSettings_t4154044775  value)
	{
		___tonemapping_0 = value;
	}

	inline static int32_t get_offset_of_basic_1() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___basic_1)); }
	inline BasicSettings_t838098426  get_basic_1() const { return ___basic_1; }
	inline BasicSettings_t838098426 * get_address_of_basic_1() { return &___basic_1; }
	inline void set_basic_1(BasicSettings_t838098426  value)
	{
		___basic_1 = value;
	}

	inline static int32_t get_offset_of_channelMixer_2() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___channelMixer_2)); }
	inline ChannelMixerSettings_t898701698  get_channelMixer_2() const { return ___channelMixer_2; }
	inline ChannelMixerSettings_t898701698 * get_address_of_channelMixer_2() { return &___channelMixer_2; }
	inline void set_channelMixer_2(ChannelMixerSettings_t898701698  value)
	{
		___channelMixer_2 = value;
	}

	inline static int32_t get_offset_of_colorWheels_3() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___colorWheels_3)); }
	inline ColorWheelsSettings_t3120867866  get_colorWheels_3() const { return ___colorWheels_3; }
	inline ColorWheelsSettings_t3120867866 * get_address_of_colorWheels_3() { return &___colorWheels_3; }
	inline void set_colorWheels_3(ColorWheelsSettings_t3120867866  value)
	{
		___colorWheels_3 = value;
	}

	inline static int32_t get_offset_of_curves_4() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___curves_4)); }
	inline CurvesSettings_t2830270037  get_curves_4() const { return ___curves_4; }
	inline CurvesSettings_t2830270037 * get_address_of_curves_4() { return &___curves_4; }
	inline void set_curves_4(CurvesSettings_t2830270037  value)
	{
		___curves_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ColorGradingModel/Settings
struct Settings_t451872061_marshaled_pinvoke
{
	TonemappingSettings_t4154044775  ___tonemapping_0;
	BasicSettings_t838098426  ___basic_1;
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	CurvesSettings_t2830270037_marshaled_pinvoke ___curves_4;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ColorGradingModel/Settings
struct Settings_t451872061_marshaled_com
{
	TonemappingSettings_t4154044775  ___tonemapping_0;
	BasicSettings_t838098426  ___basic_1;
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	CurvesSettings_t2830270037_marshaled_com ___curves_4;
};
#endif // SETTINGS_T451872061_H
#ifndef DEPTHOFFIELDMODEL_T514067330_H
#define DEPTHOFFIELDMODEL_T514067330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldModel
struct  DepthOfFieldModel_t514067330  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.DepthOfFieldModel/Settings UnityEngine.PostProcessing.DepthOfFieldModel::m_Settings
	Settings_t2195468135  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(DepthOfFieldModel_t514067330, ___m_Settings_1)); }
	inline Settings_t2195468135  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t2195468135 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t2195468135  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELDMODEL_T514067330_H
#ifndef EYEADAPTATIONMODEL_T242823912_H
#define EYEADAPTATIONMODEL_T242823912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationModel
struct  EyeAdaptationModel_t242823912  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.EyeAdaptationModel/Settings UnityEngine.PostProcessing.EyeAdaptationModel::m_Settings
	Settings_t2874244444  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(EyeAdaptationModel_t242823912, ___m_Settings_1)); }
	inline Settings_t2874244444  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t2874244444 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t2874244444  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONMODEL_T242823912_H
#ifndef POSTPROCESSINGPROFILE_T724195375_H
#define POSTPROCESSINGPROFILE_T724195375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingProfile
struct  PostProcessingProfile_t724195375  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel UnityEngine.PostProcessing.PostProcessingProfile::debugViews
	BuiltinDebugViewsModel_t1462618840 * ___debugViews_4;
	// UnityEngine.PostProcessing.FogModel UnityEngine.PostProcessing.PostProcessingProfile::fog
	FogModel_t3620688749 * ___fog_5;
	// UnityEngine.PostProcessing.AntialiasingModel UnityEngine.PostProcessing.PostProcessingProfile::antialiasing
	AntialiasingModel_t1521139388 * ___antialiasing_6;
	// UnityEngine.PostProcessing.AmbientOcclusionModel UnityEngine.PostProcessing.PostProcessingProfile::ambientOcclusion
	AmbientOcclusionModel_t389471066 * ___ambientOcclusion_7;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel UnityEngine.PostProcessing.PostProcessingProfile::screenSpaceReflection
	ScreenSpaceReflectionModel_t3026344732 * ___screenSpaceReflection_8;
	// UnityEngine.PostProcessing.DepthOfFieldModel UnityEngine.PostProcessing.PostProcessingProfile::depthOfField
	DepthOfFieldModel_t514067330 * ___depthOfField_9;
	// UnityEngine.PostProcessing.MotionBlurModel UnityEngine.PostProcessing.PostProcessingProfile::motionBlur
	MotionBlurModel_t3080286123 * ___motionBlur_10;
	// UnityEngine.PostProcessing.EyeAdaptationModel UnityEngine.PostProcessing.PostProcessingProfile::eyeAdaptation
	EyeAdaptationModel_t242823912 * ___eyeAdaptation_11;
	// UnityEngine.PostProcessing.BloomModel UnityEngine.PostProcessing.PostProcessingProfile::bloom
	BloomModel_t2099727860 * ___bloom_12;
	// UnityEngine.PostProcessing.ColorGradingModel UnityEngine.PostProcessing.PostProcessingProfile::colorGrading
	ColorGradingModel_t1448048181 * ___colorGrading_13;
	// UnityEngine.PostProcessing.UserLutModel UnityEngine.PostProcessing.PostProcessingProfile::userLut
	UserLutModel_t1670108080 * ___userLut_14;
	// UnityEngine.PostProcessing.ChromaticAberrationModel UnityEngine.PostProcessing.PostProcessingProfile::chromaticAberration
	ChromaticAberrationModel_t3963399853 * ___chromaticAberration_15;
	// UnityEngine.PostProcessing.GrainModel UnityEngine.PostProcessing.PostProcessingProfile::grain
	GrainModel_t1152882488 * ___grain_16;
	// UnityEngine.PostProcessing.VignetteModel UnityEngine.PostProcessing.PostProcessingProfile::vignette
	VignetteModel_t2845517177 * ___vignette_17;
	// UnityEngine.PostProcessing.DitheringModel UnityEngine.PostProcessing.PostProcessingProfile::dithering
	DitheringModel_t2429005396 * ___dithering_18;

public:
	inline static int32_t get_offset_of_debugViews_4() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___debugViews_4)); }
	inline BuiltinDebugViewsModel_t1462618840 * get_debugViews_4() const { return ___debugViews_4; }
	inline BuiltinDebugViewsModel_t1462618840 ** get_address_of_debugViews_4() { return &___debugViews_4; }
	inline void set_debugViews_4(BuiltinDebugViewsModel_t1462618840 * value)
	{
		___debugViews_4 = value;
		Il2CppCodeGenWriteBarrier((&___debugViews_4), value);
	}

	inline static int32_t get_offset_of_fog_5() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___fog_5)); }
	inline FogModel_t3620688749 * get_fog_5() const { return ___fog_5; }
	inline FogModel_t3620688749 ** get_address_of_fog_5() { return &___fog_5; }
	inline void set_fog_5(FogModel_t3620688749 * value)
	{
		___fog_5 = value;
		Il2CppCodeGenWriteBarrier((&___fog_5), value);
	}

	inline static int32_t get_offset_of_antialiasing_6() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___antialiasing_6)); }
	inline AntialiasingModel_t1521139388 * get_antialiasing_6() const { return ___antialiasing_6; }
	inline AntialiasingModel_t1521139388 ** get_address_of_antialiasing_6() { return &___antialiasing_6; }
	inline void set_antialiasing_6(AntialiasingModel_t1521139388 * value)
	{
		___antialiasing_6 = value;
		Il2CppCodeGenWriteBarrier((&___antialiasing_6), value);
	}

	inline static int32_t get_offset_of_ambientOcclusion_7() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___ambientOcclusion_7)); }
	inline AmbientOcclusionModel_t389471066 * get_ambientOcclusion_7() const { return ___ambientOcclusion_7; }
	inline AmbientOcclusionModel_t389471066 ** get_address_of_ambientOcclusion_7() { return &___ambientOcclusion_7; }
	inline void set_ambientOcclusion_7(AmbientOcclusionModel_t389471066 * value)
	{
		___ambientOcclusion_7 = value;
		Il2CppCodeGenWriteBarrier((&___ambientOcclusion_7), value);
	}

	inline static int32_t get_offset_of_screenSpaceReflection_8() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___screenSpaceReflection_8)); }
	inline ScreenSpaceReflectionModel_t3026344732 * get_screenSpaceReflection_8() const { return ___screenSpaceReflection_8; }
	inline ScreenSpaceReflectionModel_t3026344732 ** get_address_of_screenSpaceReflection_8() { return &___screenSpaceReflection_8; }
	inline void set_screenSpaceReflection_8(ScreenSpaceReflectionModel_t3026344732 * value)
	{
		___screenSpaceReflection_8 = value;
		Il2CppCodeGenWriteBarrier((&___screenSpaceReflection_8), value);
	}

	inline static int32_t get_offset_of_depthOfField_9() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___depthOfField_9)); }
	inline DepthOfFieldModel_t514067330 * get_depthOfField_9() const { return ___depthOfField_9; }
	inline DepthOfFieldModel_t514067330 ** get_address_of_depthOfField_9() { return &___depthOfField_9; }
	inline void set_depthOfField_9(DepthOfFieldModel_t514067330 * value)
	{
		___depthOfField_9 = value;
		Il2CppCodeGenWriteBarrier((&___depthOfField_9), value);
	}

	inline static int32_t get_offset_of_motionBlur_10() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___motionBlur_10)); }
	inline MotionBlurModel_t3080286123 * get_motionBlur_10() const { return ___motionBlur_10; }
	inline MotionBlurModel_t3080286123 ** get_address_of_motionBlur_10() { return &___motionBlur_10; }
	inline void set_motionBlur_10(MotionBlurModel_t3080286123 * value)
	{
		___motionBlur_10 = value;
		Il2CppCodeGenWriteBarrier((&___motionBlur_10), value);
	}

	inline static int32_t get_offset_of_eyeAdaptation_11() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___eyeAdaptation_11)); }
	inline EyeAdaptationModel_t242823912 * get_eyeAdaptation_11() const { return ___eyeAdaptation_11; }
	inline EyeAdaptationModel_t242823912 ** get_address_of_eyeAdaptation_11() { return &___eyeAdaptation_11; }
	inline void set_eyeAdaptation_11(EyeAdaptationModel_t242823912 * value)
	{
		___eyeAdaptation_11 = value;
		Il2CppCodeGenWriteBarrier((&___eyeAdaptation_11), value);
	}

	inline static int32_t get_offset_of_bloom_12() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___bloom_12)); }
	inline BloomModel_t2099727860 * get_bloom_12() const { return ___bloom_12; }
	inline BloomModel_t2099727860 ** get_address_of_bloom_12() { return &___bloom_12; }
	inline void set_bloom_12(BloomModel_t2099727860 * value)
	{
		___bloom_12 = value;
		Il2CppCodeGenWriteBarrier((&___bloom_12), value);
	}

	inline static int32_t get_offset_of_colorGrading_13() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___colorGrading_13)); }
	inline ColorGradingModel_t1448048181 * get_colorGrading_13() const { return ___colorGrading_13; }
	inline ColorGradingModel_t1448048181 ** get_address_of_colorGrading_13() { return &___colorGrading_13; }
	inline void set_colorGrading_13(ColorGradingModel_t1448048181 * value)
	{
		___colorGrading_13 = value;
		Il2CppCodeGenWriteBarrier((&___colorGrading_13), value);
	}

	inline static int32_t get_offset_of_userLut_14() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___userLut_14)); }
	inline UserLutModel_t1670108080 * get_userLut_14() const { return ___userLut_14; }
	inline UserLutModel_t1670108080 ** get_address_of_userLut_14() { return &___userLut_14; }
	inline void set_userLut_14(UserLutModel_t1670108080 * value)
	{
		___userLut_14 = value;
		Il2CppCodeGenWriteBarrier((&___userLut_14), value);
	}

	inline static int32_t get_offset_of_chromaticAberration_15() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___chromaticAberration_15)); }
	inline ChromaticAberrationModel_t3963399853 * get_chromaticAberration_15() const { return ___chromaticAberration_15; }
	inline ChromaticAberrationModel_t3963399853 ** get_address_of_chromaticAberration_15() { return &___chromaticAberration_15; }
	inline void set_chromaticAberration_15(ChromaticAberrationModel_t3963399853 * value)
	{
		___chromaticAberration_15 = value;
		Il2CppCodeGenWriteBarrier((&___chromaticAberration_15), value);
	}

	inline static int32_t get_offset_of_grain_16() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___grain_16)); }
	inline GrainModel_t1152882488 * get_grain_16() const { return ___grain_16; }
	inline GrainModel_t1152882488 ** get_address_of_grain_16() { return &___grain_16; }
	inline void set_grain_16(GrainModel_t1152882488 * value)
	{
		___grain_16 = value;
		Il2CppCodeGenWriteBarrier((&___grain_16), value);
	}

	inline static int32_t get_offset_of_vignette_17() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___vignette_17)); }
	inline VignetteModel_t2845517177 * get_vignette_17() const { return ___vignette_17; }
	inline VignetteModel_t2845517177 ** get_address_of_vignette_17() { return &___vignette_17; }
	inline void set_vignette_17(VignetteModel_t2845517177 * value)
	{
		___vignette_17 = value;
		Il2CppCodeGenWriteBarrier((&___vignette_17), value);
	}

	inline static int32_t get_offset_of_dithering_18() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___dithering_18)); }
	inline DitheringModel_t2429005396 * get_dithering_18() const { return ___dithering_18; }
	inline DitheringModel_t2429005396 ** get_address_of_dithering_18() { return &___dithering_18; }
	inline void set_dithering_18(DitheringModel_t2429005396 * value)
	{
		___dithering_18 = value;
		Il2CppCodeGenWriteBarrier((&___dithering_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGPROFILE_T724195375_H
#ifndef SETTINGS_T1995791524_H
#define SETTINGS_T1995791524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
struct  Settings_t1995791524 
{
public:
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::reflection
	ReflectionSettings_t282755190  ___reflection_0;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::intensity
	IntensitySettings_t1721872184  ___intensity_1;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ScreenEdgeMask UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::screenEdgeMask
	ScreenEdgeMask_t4063288584  ___screenEdgeMask_2;

public:
	inline static int32_t get_offset_of_reflection_0() { return static_cast<int32_t>(offsetof(Settings_t1995791524, ___reflection_0)); }
	inline ReflectionSettings_t282755190  get_reflection_0() const { return ___reflection_0; }
	inline ReflectionSettings_t282755190 * get_address_of_reflection_0() { return &___reflection_0; }
	inline void set_reflection_0(ReflectionSettings_t282755190  value)
	{
		___reflection_0 = value;
	}

	inline static int32_t get_offset_of_intensity_1() { return static_cast<int32_t>(offsetof(Settings_t1995791524, ___intensity_1)); }
	inline IntensitySettings_t1721872184  get_intensity_1() const { return ___intensity_1; }
	inline IntensitySettings_t1721872184 * get_address_of_intensity_1() { return &___intensity_1; }
	inline void set_intensity_1(IntensitySettings_t1721872184  value)
	{
		___intensity_1 = value;
	}

	inline static int32_t get_offset_of_screenEdgeMask_2() { return static_cast<int32_t>(offsetof(Settings_t1995791524, ___screenEdgeMask_2)); }
	inline ScreenEdgeMask_t4063288584  get_screenEdgeMask_2() const { return ___screenEdgeMask_2; }
	inline ScreenEdgeMask_t4063288584 * get_address_of_screenEdgeMask_2() { return &___screenEdgeMask_2; }
	inline void set_screenEdgeMask_2(ScreenEdgeMask_t4063288584  value)
	{
		___screenEdgeMask_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
struct Settings_t1995791524_marshaled_pinvoke
{
	ReflectionSettings_t282755190_marshaled_pinvoke ___reflection_0;
	IntensitySettings_t1721872184  ___intensity_1;
	ScreenEdgeMask_t4063288584  ___screenEdgeMask_2;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
struct Settings_t1995791524_marshaled_com
{
	ReflectionSettings_t282755190_marshaled_com ___reflection_0;
	IntensitySettings_t1721872184  ___intensity_1;
	ScreenEdgeMask_t4063288584  ___screenEdgeMask_2;
};
#endif // SETTINGS_T1995791524_H
#ifndef VIGNETTEMODEL_T2845517177_H
#define VIGNETTEMODEL_T2845517177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteModel
struct  VignetteModel_t2845517177  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.VignetteModel/Settings UnityEngine.PostProcessing.VignetteModel::m_Settings
	Settings_t1354494600  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(VignetteModel_t2845517177, ___m_Settings_1)); }
	inline Settings_t1354494600  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t1354494600 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t1354494600  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTEMODEL_T2845517177_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SCREENSPACEREFLECTIONMODEL_T3026344732_H
#define SCREENSPACEREFLECTIONMODEL_T3026344732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel
struct  ScreenSpaceReflectionModel_t3026344732  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings UnityEngine.PostProcessing.ScreenSpaceReflectionModel::m_Settings
	Settings_t1995791524  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionModel_t3026344732, ___m_Settings_1)); }
	inline Settings_t1995791524  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t1995791524 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t1995791524  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONMODEL_T3026344732_H
#ifndef POSTPROCESSINGBEHAVIOUR_T3229946336_H
#define POSTPROCESSINGBEHAVIOUR_T3229946336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingBehaviour
struct  PostProcessingBehaviour_t3229946336  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.PostProcessing.PostProcessingProfile UnityEngine.PostProcessing.PostProcessingBehaviour::profile
	PostProcessingProfile_t724195375 * ___profile_4;
	// System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4> UnityEngine.PostProcessing.PostProcessingBehaviour::jitteredMatrixFunc
	Func_2_t4093140010 * ___jitteredMatrixFunc_5;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>> UnityEngine.PostProcessing.PostProcessingBehaviour::m_CommandBuffers
	Dictionary_2_t1572824908 * ___m_CommandBuffers_6;
	// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase> UnityEngine.PostProcessing.PostProcessingBehaviour::m_Components
	List_1_t4203178569 * ___m_Components_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean> UnityEngine.PostProcessing.PostProcessingBehaviour::m_ComponentStates
	Dictionary_2_t3095696878 * ___m_ComponentStates_8;
	// UnityEngine.PostProcessing.MaterialFactory UnityEngine.PostProcessing.PostProcessingBehaviour::m_MaterialFactory
	MaterialFactory_t2445948724 * ___m_MaterialFactory_9;
	// UnityEngine.PostProcessing.RenderTextureFactory UnityEngine.PostProcessing.PostProcessingBehaviour::m_RenderTextureFactory
	RenderTextureFactory_t1946967824 * ___m_RenderTextureFactory_10;
	// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingBehaviour::m_Context
	PostProcessingContext_t2014408948 * ___m_Context_11;
	// UnityEngine.Camera UnityEngine.PostProcessing.PostProcessingBehaviour::m_Camera
	Camera_t4157153871 * ___m_Camera_12;
	// UnityEngine.PostProcessing.PostProcessingProfile UnityEngine.PostProcessing.PostProcessingBehaviour::m_PreviousProfile
	PostProcessingProfile_t724195375 * ___m_PreviousProfile_13;
	// System.Boolean UnityEngine.PostProcessing.PostProcessingBehaviour::m_RenderingInSceneView
	bool ___m_RenderingInSceneView_14;
	// UnityEngine.PostProcessing.BuiltinDebugViewsComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_DebugViews
	BuiltinDebugViewsComponent_t2123147871 * ___m_DebugViews_15;
	// UnityEngine.PostProcessing.AmbientOcclusionComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_AmbientOcclusion
	AmbientOcclusionComponent_t4130625043 * ___m_AmbientOcclusion_16;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_ScreenSpaceReflection
	ScreenSpaceReflectionComponent_t856094247 * ___m_ScreenSpaceReflection_17;
	// UnityEngine.PostProcessing.FogComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_FogComponent
	FogComponent_t3400726830 * ___m_FogComponent_18;
	// UnityEngine.PostProcessing.MotionBlurComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_MotionBlur
	MotionBlurComponent_t3686516877 * ___m_MotionBlur_19;
	// UnityEngine.PostProcessing.TaaComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Taa
	TaaComponent_t3791749658 * ___m_Taa_20;
	// UnityEngine.PostProcessing.EyeAdaptationComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_EyeAdaptation
	EyeAdaptationComponent_t3394805121 * ___m_EyeAdaptation_21;
	// UnityEngine.PostProcessing.DepthOfFieldComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_DepthOfField
	DepthOfFieldComponent_t554756766 * ___m_DepthOfField_22;
	// UnityEngine.PostProcessing.BloomComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Bloom
	BloomComponent_t3791419130 * ___m_Bloom_23;
	// UnityEngine.PostProcessing.ChromaticAberrationComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_ChromaticAberration
	ChromaticAberrationComponent_t1647263118 * ___m_ChromaticAberration_24;
	// UnityEngine.PostProcessing.ColorGradingComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_ColorGrading
	ColorGradingComponent_t1715259467 * ___m_ColorGrading_25;
	// UnityEngine.PostProcessing.UserLutComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_UserLut
	UserLutComponent_t2843161776 * ___m_UserLut_26;
	// UnityEngine.PostProcessing.GrainComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Grain
	GrainComponent_t866324317 * ___m_Grain_27;
	// UnityEngine.PostProcessing.VignetteComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Vignette
	VignetteComponent_t3243642943 * ___m_Vignette_28;
	// UnityEngine.PostProcessing.DitheringComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Dithering
	DitheringComponent_t277621267 * ___m_Dithering_29;
	// UnityEngine.PostProcessing.FxaaComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Fxaa
	FxaaComponent_t1312385771 * ___m_Fxaa_30;
	// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase> UnityEngine.PostProcessing.PostProcessingBehaviour::m_ComponentsToEnable
	List_1_t4203178569 * ___m_ComponentsToEnable_31;
	// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase> UnityEngine.PostProcessing.PostProcessingBehaviour::m_ComponentsToDisable
	List_1_t4203178569 * ___m_ComponentsToDisable_32;

public:
	inline static int32_t get_offset_of_profile_4() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___profile_4)); }
	inline PostProcessingProfile_t724195375 * get_profile_4() const { return ___profile_4; }
	inline PostProcessingProfile_t724195375 ** get_address_of_profile_4() { return &___profile_4; }
	inline void set_profile_4(PostProcessingProfile_t724195375 * value)
	{
		___profile_4 = value;
		Il2CppCodeGenWriteBarrier((&___profile_4), value);
	}

	inline static int32_t get_offset_of_jitteredMatrixFunc_5() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___jitteredMatrixFunc_5)); }
	inline Func_2_t4093140010 * get_jitteredMatrixFunc_5() const { return ___jitteredMatrixFunc_5; }
	inline Func_2_t4093140010 ** get_address_of_jitteredMatrixFunc_5() { return &___jitteredMatrixFunc_5; }
	inline void set_jitteredMatrixFunc_5(Func_2_t4093140010 * value)
	{
		___jitteredMatrixFunc_5 = value;
		Il2CppCodeGenWriteBarrier((&___jitteredMatrixFunc_5), value);
	}

	inline static int32_t get_offset_of_m_CommandBuffers_6() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_CommandBuffers_6)); }
	inline Dictionary_2_t1572824908 * get_m_CommandBuffers_6() const { return ___m_CommandBuffers_6; }
	inline Dictionary_2_t1572824908 ** get_address_of_m_CommandBuffers_6() { return &___m_CommandBuffers_6; }
	inline void set_m_CommandBuffers_6(Dictionary_2_t1572824908 * value)
	{
		___m_CommandBuffers_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CommandBuffers_6), value);
	}

	inline static int32_t get_offset_of_m_Components_7() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Components_7)); }
	inline List_1_t4203178569 * get_m_Components_7() const { return ___m_Components_7; }
	inline List_1_t4203178569 ** get_address_of_m_Components_7() { return &___m_Components_7; }
	inline void set_m_Components_7(List_1_t4203178569 * value)
	{
		___m_Components_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Components_7), value);
	}

	inline static int32_t get_offset_of_m_ComponentStates_8() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ComponentStates_8)); }
	inline Dictionary_2_t3095696878 * get_m_ComponentStates_8() const { return ___m_ComponentStates_8; }
	inline Dictionary_2_t3095696878 ** get_address_of_m_ComponentStates_8() { return &___m_ComponentStates_8; }
	inline void set_m_ComponentStates_8(Dictionary_2_t3095696878 * value)
	{
		___m_ComponentStates_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentStates_8), value);
	}

	inline static int32_t get_offset_of_m_MaterialFactory_9() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_MaterialFactory_9)); }
	inline MaterialFactory_t2445948724 * get_m_MaterialFactory_9() const { return ___m_MaterialFactory_9; }
	inline MaterialFactory_t2445948724 ** get_address_of_m_MaterialFactory_9() { return &___m_MaterialFactory_9; }
	inline void set_m_MaterialFactory_9(MaterialFactory_t2445948724 * value)
	{
		___m_MaterialFactory_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaterialFactory_9), value);
	}

	inline static int32_t get_offset_of_m_RenderTextureFactory_10() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_RenderTextureFactory_10)); }
	inline RenderTextureFactory_t1946967824 * get_m_RenderTextureFactory_10() const { return ___m_RenderTextureFactory_10; }
	inline RenderTextureFactory_t1946967824 ** get_address_of_m_RenderTextureFactory_10() { return &___m_RenderTextureFactory_10; }
	inline void set_m_RenderTextureFactory_10(RenderTextureFactory_t1946967824 * value)
	{
		___m_RenderTextureFactory_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_RenderTextureFactory_10), value);
	}

	inline static int32_t get_offset_of_m_Context_11() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Context_11)); }
	inline PostProcessingContext_t2014408948 * get_m_Context_11() const { return ___m_Context_11; }
	inline PostProcessingContext_t2014408948 ** get_address_of_m_Context_11() { return &___m_Context_11; }
	inline void set_m_Context_11(PostProcessingContext_t2014408948 * value)
	{
		___m_Context_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Context_11), value);
	}

	inline static int32_t get_offset_of_m_Camera_12() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Camera_12)); }
	inline Camera_t4157153871 * get_m_Camera_12() const { return ___m_Camera_12; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_12() { return &___m_Camera_12; }
	inline void set_m_Camera_12(Camera_t4157153871 * value)
	{
		___m_Camera_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_12), value);
	}

	inline static int32_t get_offset_of_m_PreviousProfile_13() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_PreviousProfile_13)); }
	inline PostProcessingProfile_t724195375 * get_m_PreviousProfile_13() const { return ___m_PreviousProfile_13; }
	inline PostProcessingProfile_t724195375 ** get_address_of_m_PreviousProfile_13() { return &___m_PreviousProfile_13; }
	inline void set_m_PreviousProfile_13(PostProcessingProfile_t724195375 * value)
	{
		___m_PreviousProfile_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_PreviousProfile_13), value);
	}

	inline static int32_t get_offset_of_m_RenderingInSceneView_14() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_RenderingInSceneView_14)); }
	inline bool get_m_RenderingInSceneView_14() const { return ___m_RenderingInSceneView_14; }
	inline bool* get_address_of_m_RenderingInSceneView_14() { return &___m_RenderingInSceneView_14; }
	inline void set_m_RenderingInSceneView_14(bool value)
	{
		___m_RenderingInSceneView_14 = value;
	}

	inline static int32_t get_offset_of_m_DebugViews_15() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_DebugViews_15)); }
	inline BuiltinDebugViewsComponent_t2123147871 * get_m_DebugViews_15() const { return ___m_DebugViews_15; }
	inline BuiltinDebugViewsComponent_t2123147871 ** get_address_of_m_DebugViews_15() { return &___m_DebugViews_15; }
	inline void set_m_DebugViews_15(BuiltinDebugViewsComponent_t2123147871 * value)
	{
		___m_DebugViews_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugViews_15), value);
	}

	inline static int32_t get_offset_of_m_AmbientOcclusion_16() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_AmbientOcclusion_16)); }
	inline AmbientOcclusionComponent_t4130625043 * get_m_AmbientOcclusion_16() const { return ___m_AmbientOcclusion_16; }
	inline AmbientOcclusionComponent_t4130625043 ** get_address_of_m_AmbientOcclusion_16() { return &___m_AmbientOcclusion_16; }
	inline void set_m_AmbientOcclusion_16(AmbientOcclusionComponent_t4130625043 * value)
	{
		___m_AmbientOcclusion_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_AmbientOcclusion_16), value);
	}

	inline static int32_t get_offset_of_m_ScreenSpaceReflection_17() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ScreenSpaceReflection_17)); }
	inline ScreenSpaceReflectionComponent_t856094247 * get_m_ScreenSpaceReflection_17() const { return ___m_ScreenSpaceReflection_17; }
	inline ScreenSpaceReflectionComponent_t856094247 ** get_address_of_m_ScreenSpaceReflection_17() { return &___m_ScreenSpaceReflection_17; }
	inline void set_m_ScreenSpaceReflection_17(ScreenSpaceReflectionComponent_t856094247 * value)
	{
		___m_ScreenSpaceReflection_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScreenSpaceReflection_17), value);
	}

	inline static int32_t get_offset_of_m_FogComponent_18() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_FogComponent_18)); }
	inline FogComponent_t3400726830 * get_m_FogComponent_18() const { return ___m_FogComponent_18; }
	inline FogComponent_t3400726830 ** get_address_of_m_FogComponent_18() { return &___m_FogComponent_18; }
	inline void set_m_FogComponent_18(FogComponent_t3400726830 * value)
	{
		___m_FogComponent_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_FogComponent_18), value);
	}

	inline static int32_t get_offset_of_m_MotionBlur_19() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_MotionBlur_19)); }
	inline MotionBlurComponent_t3686516877 * get_m_MotionBlur_19() const { return ___m_MotionBlur_19; }
	inline MotionBlurComponent_t3686516877 ** get_address_of_m_MotionBlur_19() { return &___m_MotionBlur_19; }
	inline void set_m_MotionBlur_19(MotionBlurComponent_t3686516877 * value)
	{
		___m_MotionBlur_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_MotionBlur_19), value);
	}

	inline static int32_t get_offset_of_m_Taa_20() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Taa_20)); }
	inline TaaComponent_t3791749658 * get_m_Taa_20() const { return ___m_Taa_20; }
	inline TaaComponent_t3791749658 ** get_address_of_m_Taa_20() { return &___m_Taa_20; }
	inline void set_m_Taa_20(TaaComponent_t3791749658 * value)
	{
		___m_Taa_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_Taa_20), value);
	}

	inline static int32_t get_offset_of_m_EyeAdaptation_21() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_EyeAdaptation_21)); }
	inline EyeAdaptationComponent_t3394805121 * get_m_EyeAdaptation_21() const { return ___m_EyeAdaptation_21; }
	inline EyeAdaptationComponent_t3394805121 ** get_address_of_m_EyeAdaptation_21() { return &___m_EyeAdaptation_21; }
	inline void set_m_EyeAdaptation_21(EyeAdaptationComponent_t3394805121 * value)
	{
		___m_EyeAdaptation_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_EyeAdaptation_21), value);
	}

	inline static int32_t get_offset_of_m_DepthOfField_22() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_DepthOfField_22)); }
	inline DepthOfFieldComponent_t554756766 * get_m_DepthOfField_22() const { return ___m_DepthOfField_22; }
	inline DepthOfFieldComponent_t554756766 ** get_address_of_m_DepthOfField_22() { return &___m_DepthOfField_22; }
	inline void set_m_DepthOfField_22(DepthOfFieldComponent_t554756766 * value)
	{
		___m_DepthOfField_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_DepthOfField_22), value);
	}

	inline static int32_t get_offset_of_m_Bloom_23() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Bloom_23)); }
	inline BloomComponent_t3791419130 * get_m_Bloom_23() const { return ___m_Bloom_23; }
	inline BloomComponent_t3791419130 ** get_address_of_m_Bloom_23() { return &___m_Bloom_23; }
	inline void set_m_Bloom_23(BloomComponent_t3791419130 * value)
	{
		___m_Bloom_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_Bloom_23), value);
	}

	inline static int32_t get_offset_of_m_ChromaticAberration_24() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ChromaticAberration_24)); }
	inline ChromaticAberrationComponent_t1647263118 * get_m_ChromaticAberration_24() const { return ___m_ChromaticAberration_24; }
	inline ChromaticAberrationComponent_t1647263118 ** get_address_of_m_ChromaticAberration_24() { return &___m_ChromaticAberration_24; }
	inline void set_m_ChromaticAberration_24(ChromaticAberrationComponent_t1647263118 * value)
	{
		___m_ChromaticAberration_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChromaticAberration_24), value);
	}

	inline static int32_t get_offset_of_m_ColorGrading_25() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ColorGrading_25)); }
	inline ColorGradingComponent_t1715259467 * get_m_ColorGrading_25() const { return ___m_ColorGrading_25; }
	inline ColorGradingComponent_t1715259467 ** get_address_of_m_ColorGrading_25() { return &___m_ColorGrading_25; }
	inline void set_m_ColorGrading_25(ColorGradingComponent_t1715259467 * value)
	{
		___m_ColorGrading_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorGrading_25), value);
	}

	inline static int32_t get_offset_of_m_UserLut_26() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_UserLut_26)); }
	inline UserLutComponent_t2843161776 * get_m_UserLut_26() const { return ___m_UserLut_26; }
	inline UserLutComponent_t2843161776 ** get_address_of_m_UserLut_26() { return &___m_UserLut_26; }
	inline void set_m_UserLut_26(UserLutComponent_t2843161776 * value)
	{
		___m_UserLut_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_UserLut_26), value);
	}

	inline static int32_t get_offset_of_m_Grain_27() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Grain_27)); }
	inline GrainComponent_t866324317 * get_m_Grain_27() const { return ___m_Grain_27; }
	inline GrainComponent_t866324317 ** get_address_of_m_Grain_27() { return &___m_Grain_27; }
	inline void set_m_Grain_27(GrainComponent_t866324317 * value)
	{
		___m_Grain_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Grain_27), value);
	}

	inline static int32_t get_offset_of_m_Vignette_28() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Vignette_28)); }
	inline VignetteComponent_t3243642943 * get_m_Vignette_28() const { return ___m_Vignette_28; }
	inline VignetteComponent_t3243642943 ** get_address_of_m_Vignette_28() { return &___m_Vignette_28; }
	inline void set_m_Vignette_28(VignetteComponent_t3243642943 * value)
	{
		___m_Vignette_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Vignette_28), value);
	}

	inline static int32_t get_offset_of_m_Dithering_29() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Dithering_29)); }
	inline DitheringComponent_t277621267 * get_m_Dithering_29() const { return ___m_Dithering_29; }
	inline DitheringComponent_t277621267 ** get_address_of_m_Dithering_29() { return &___m_Dithering_29; }
	inline void set_m_Dithering_29(DitheringComponent_t277621267 * value)
	{
		___m_Dithering_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dithering_29), value);
	}

	inline static int32_t get_offset_of_m_Fxaa_30() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Fxaa_30)); }
	inline FxaaComponent_t1312385771 * get_m_Fxaa_30() const { return ___m_Fxaa_30; }
	inline FxaaComponent_t1312385771 ** get_address_of_m_Fxaa_30() { return &___m_Fxaa_30; }
	inline void set_m_Fxaa_30(FxaaComponent_t1312385771 * value)
	{
		___m_Fxaa_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fxaa_30), value);
	}

	inline static int32_t get_offset_of_m_ComponentsToEnable_31() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ComponentsToEnable_31)); }
	inline List_1_t4203178569 * get_m_ComponentsToEnable_31() const { return ___m_ComponentsToEnable_31; }
	inline List_1_t4203178569 ** get_address_of_m_ComponentsToEnable_31() { return &___m_ComponentsToEnable_31; }
	inline void set_m_ComponentsToEnable_31(List_1_t4203178569 * value)
	{
		___m_ComponentsToEnable_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentsToEnable_31), value);
	}

	inline static int32_t get_offset_of_m_ComponentsToDisable_32() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ComponentsToDisable_32)); }
	inline List_1_t4203178569 * get_m_ComponentsToDisable_32() const { return ___m_ComponentsToDisable_32; }
	inline List_1_t4203178569 ** get_address_of_m_ComponentsToDisable_32() { return &___m_ComponentsToDisable_32; }
	inline void set_m_ComponentsToDisable_32(List_1_t4203178569 * value)
	{
		___m_ComponentsToDisable_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentsToDisable_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGBEHAVIOUR_T3229946336_H
#ifndef AFTERBURNERPHYSICSFORCE_T498893161_H
#define AFTERBURNERPHYSICSFORCE_T498893161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.AfterburnerPhysicsForce
struct  AfterburnerPhysicsForce_t498893161  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectAngle
	float ___effectAngle_4;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectWidth
	float ___effectWidth_5;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::effectDistance
	float ___effectDistance_6;
	// System.Single UnityStandardAssets.Effects.AfterburnerPhysicsForce::force
	float ___force_7;
	// UnityEngine.Collider[] UnityStandardAssets.Effects.AfterburnerPhysicsForce::m_Cols
	ColliderU5BU5D_t4234922487* ___m_Cols_8;
	// UnityEngine.SphereCollider UnityStandardAssets.Effects.AfterburnerPhysicsForce::m_Sphere
	SphereCollider_t2077223608 * ___m_Sphere_9;

public:
	inline static int32_t get_offset_of_effectAngle_4() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___effectAngle_4)); }
	inline float get_effectAngle_4() const { return ___effectAngle_4; }
	inline float* get_address_of_effectAngle_4() { return &___effectAngle_4; }
	inline void set_effectAngle_4(float value)
	{
		___effectAngle_4 = value;
	}

	inline static int32_t get_offset_of_effectWidth_5() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___effectWidth_5)); }
	inline float get_effectWidth_5() const { return ___effectWidth_5; }
	inline float* get_address_of_effectWidth_5() { return &___effectWidth_5; }
	inline void set_effectWidth_5(float value)
	{
		___effectWidth_5 = value;
	}

	inline static int32_t get_offset_of_effectDistance_6() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___effectDistance_6)); }
	inline float get_effectDistance_6() const { return ___effectDistance_6; }
	inline float* get_address_of_effectDistance_6() { return &___effectDistance_6; }
	inline void set_effectDistance_6(float value)
	{
		___effectDistance_6 = value;
	}

	inline static int32_t get_offset_of_force_7() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___force_7)); }
	inline float get_force_7() const { return ___force_7; }
	inline float* get_address_of_force_7() { return &___force_7; }
	inline void set_force_7(float value)
	{
		___force_7 = value;
	}

	inline static int32_t get_offset_of_m_Cols_8() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___m_Cols_8)); }
	inline ColliderU5BU5D_t4234922487* get_m_Cols_8() const { return ___m_Cols_8; }
	inline ColliderU5BU5D_t4234922487** get_address_of_m_Cols_8() { return &___m_Cols_8; }
	inline void set_m_Cols_8(ColliderU5BU5D_t4234922487* value)
	{
		___m_Cols_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cols_8), value);
	}

	inline static int32_t get_offset_of_m_Sphere_9() { return static_cast<int32_t>(offsetof(AfterburnerPhysicsForce_t498893161, ___m_Sphere_9)); }
	inline SphereCollider_t2077223608 * get_m_Sphere_9() const { return ___m_Sphere_9; }
	inline SphereCollider_t2077223608 ** get_address_of_m_Sphere_9() { return &___m_Sphere_9; }
	inline void set_m_Sphere_9(SphereCollider_t2077223608 * value)
	{
		___m_Sphere_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sphere_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AFTERBURNERPHYSICSFORCE_T498893161_H
#ifndef EXPLOSIONFIREANDDEBRIS_T2411343565_H
#define EXPLOSIONFIREANDDEBRIS_T2411343565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionFireAndDebris
struct  ExplosionFireAndDebris_t2411343565  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform[] UnityStandardAssets.Effects.ExplosionFireAndDebris::debrisPrefabs
	TransformU5BU5D_t807237628* ___debrisPrefabs_4;
	// UnityEngine.Transform UnityStandardAssets.Effects.ExplosionFireAndDebris::firePrefab
	Transform_t3600365921 * ___firePrefab_5;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris::numDebrisPieces
	int32_t ___numDebrisPieces_6;
	// System.Int32 UnityStandardAssets.Effects.ExplosionFireAndDebris::numFires
	int32_t ___numFires_7;

public:
	inline static int32_t get_offset_of_debrisPrefabs_4() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_t2411343565, ___debrisPrefabs_4)); }
	inline TransformU5BU5D_t807237628* get_debrisPrefabs_4() const { return ___debrisPrefabs_4; }
	inline TransformU5BU5D_t807237628** get_address_of_debrisPrefabs_4() { return &___debrisPrefabs_4; }
	inline void set_debrisPrefabs_4(TransformU5BU5D_t807237628* value)
	{
		___debrisPrefabs_4 = value;
		Il2CppCodeGenWriteBarrier((&___debrisPrefabs_4), value);
	}

	inline static int32_t get_offset_of_firePrefab_5() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_t2411343565, ___firePrefab_5)); }
	inline Transform_t3600365921 * get_firePrefab_5() const { return ___firePrefab_5; }
	inline Transform_t3600365921 ** get_address_of_firePrefab_5() { return &___firePrefab_5; }
	inline void set_firePrefab_5(Transform_t3600365921 * value)
	{
		___firePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___firePrefab_5), value);
	}

	inline static int32_t get_offset_of_numDebrisPieces_6() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_t2411343565, ___numDebrisPieces_6)); }
	inline int32_t get_numDebrisPieces_6() const { return ___numDebrisPieces_6; }
	inline int32_t* get_address_of_numDebrisPieces_6() { return &___numDebrisPieces_6; }
	inline void set_numDebrisPieces_6(int32_t value)
	{
		___numDebrisPieces_6 = value;
	}

	inline static int32_t get_offset_of_numFires_7() { return static_cast<int32_t>(offsetof(ExplosionFireAndDebris_t2411343565, ___numFires_7)); }
	inline int32_t get_numFires_7() const { return ___numFires_7; }
	inline int32_t* get_address_of_numFires_7() { return &___numFires_7; }
	inline void set_numFires_7(int32_t value)
	{
		___numFires_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSIONFIREANDDEBRIS_T2411343565_H
#ifndef EXPLOSIONPHYSICSFORCE_T3982641844_H
#define EXPLOSIONPHYSICSFORCE_T3982641844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExplosionPhysicsForce
struct  ExplosionPhysicsForce_t3982641844  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.ExplosionPhysicsForce::explosionForce
	float ___explosionForce_4;

public:
	inline static int32_t get_offset_of_explosionForce_4() { return static_cast<int32_t>(offsetof(ExplosionPhysicsForce_t3982641844, ___explosionForce_4)); }
	inline float get_explosionForce_4() const { return ___explosionForce_4; }
	inline float* get_address_of_explosionForce_4() { return &___explosionForce_4; }
	inline void set_explosionForce_4(float value)
	{
		___explosionForce_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSIONPHYSICSFORCE_T3982641844_H
#ifndef EXPLOSIVE_T792321375_H
#define EXPLOSIVE_T792321375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.Explosive
struct  Explosive_t792321375  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Effects.Explosive::explosionPrefab
	Transform_t3600365921 * ___explosionPrefab_4;
	// System.Single UnityStandardAssets.Effects.Explosive::detonationImpactVelocity
	float ___detonationImpactVelocity_5;
	// System.Single UnityStandardAssets.Effects.Explosive::sizeMultiplier
	float ___sizeMultiplier_6;
	// System.Boolean UnityStandardAssets.Effects.Explosive::reset
	bool ___reset_7;
	// System.Single UnityStandardAssets.Effects.Explosive::resetTimeDelay
	float ___resetTimeDelay_8;
	// System.Boolean UnityStandardAssets.Effects.Explosive::m_Exploded
	bool ___m_Exploded_9;
	// UnityStandardAssets.Utility.ObjectResetter UnityStandardAssets.Effects.Explosive::m_ObjectResetter
	ObjectResetter_t639177103 * ___m_ObjectResetter_10;

public:
	inline static int32_t get_offset_of_explosionPrefab_4() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___explosionPrefab_4)); }
	inline Transform_t3600365921 * get_explosionPrefab_4() const { return ___explosionPrefab_4; }
	inline Transform_t3600365921 ** get_address_of_explosionPrefab_4() { return &___explosionPrefab_4; }
	inline void set_explosionPrefab_4(Transform_t3600365921 * value)
	{
		___explosionPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___explosionPrefab_4), value);
	}

	inline static int32_t get_offset_of_detonationImpactVelocity_5() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___detonationImpactVelocity_5)); }
	inline float get_detonationImpactVelocity_5() const { return ___detonationImpactVelocity_5; }
	inline float* get_address_of_detonationImpactVelocity_5() { return &___detonationImpactVelocity_5; }
	inline void set_detonationImpactVelocity_5(float value)
	{
		___detonationImpactVelocity_5 = value;
	}

	inline static int32_t get_offset_of_sizeMultiplier_6() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___sizeMultiplier_6)); }
	inline float get_sizeMultiplier_6() const { return ___sizeMultiplier_6; }
	inline float* get_address_of_sizeMultiplier_6() { return &___sizeMultiplier_6; }
	inline void set_sizeMultiplier_6(float value)
	{
		___sizeMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_reset_7() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___reset_7)); }
	inline bool get_reset_7() const { return ___reset_7; }
	inline bool* get_address_of_reset_7() { return &___reset_7; }
	inline void set_reset_7(bool value)
	{
		___reset_7 = value;
	}

	inline static int32_t get_offset_of_resetTimeDelay_8() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___resetTimeDelay_8)); }
	inline float get_resetTimeDelay_8() const { return ___resetTimeDelay_8; }
	inline float* get_address_of_resetTimeDelay_8() { return &___resetTimeDelay_8; }
	inline void set_resetTimeDelay_8(float value)
	{
		___resetTimeDelay_8 = value;
	}

	inline static int32_t get_offset_of_m_Exploded_9() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___m_Exploded_9)); }
	inline bool get_m_Exploded_9() const { return ___m_Exploded_9; }
	inline bool* get_address_of_m_Exploded_9() { return &___m_Exploded_9; }
	inline void set_m_Exploded_9(bool value)
	{
		___m_Exploded_9 = value;
	}

	inline static int32_t get_offset_of_m_ObjectResetter_10() { return static_cast<int32_t>(offsetof(Explosive_t792321375, ___m_ObjectResetter_10)); }
	inline ObjectResetter_t639177103 * get_m_ObjectResetter_10() const { return ___m_ObjectResetter_10; }
	inline ObjectResetter_t639177103 ** get_address_of_m_ObjectResetter_10() { return &___m_ObjectResetter_10; }
	inline void set_m_ObjectResetter_10(ObjectResetter_t639177103 * value)
	{
		___m_ObjectResetter_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectResetter_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSIVE_T792321375_H
#ifndef EXTINGUISHABLEPARTICLESYSTEM_T4259708998_H
#define EXTINGUISHABLEPARTICLESYSTEM_T4259708998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ExtinguishableParticleSystem
struct  ExtinguishableParticleSystem_t4259708998  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.ExtinguishableParticleSystem::multiplier
	float ___multiplier_4;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Effects.ExtinguishableParticleSystem::m_Systems
	ParticleSystemU5BU5D_t3089334924* ___m_Systems_5;

public:
	inline static int32_t get_offset_of_multiplier_4() { return static_cast<int32_t>(offsetof(ExtinguishableParticleSystem_t4259708998, ___multiplier_4)); }
	inline float get_multiplier_4() const { return ___multiplier_4; }
	inline float* get_address_of_multiplier_4() { return &___multiplier_4; }
	inline void set_multiplier_4(float value)
	{
		___multiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_Systems_5() { return static_cast<int32_t>(offsetof(ExtinguishableParticleSystem_t4259708998, ___m_Systems_5)); }
	inline ParticleSystemU5BU5D_t3089334924* get_m_Systems_5() const { return ___m_Systems_5; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_m_Systems_5() { return &___m_Systems_5; }
	inline void set_m_Systems_5(ParticleSystemU5BU5D_t3089334924* value)
	{
		___m_Systems_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Systems_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTINGUISHABLEPARTICLESYSTEM_T4259708998_H
#ifndef PARTICLESCENECONTROLS_T306931203_H
#define PARTICLESCENECONTROLS_T306931203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.ParticleSceneControls
struct  ParticleSceneControls_t306931203  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList UnityStandardAssets.SceneUtils.ParticleSceneControls::demoParticles
	DemoParticleSystemList_t4288938153 * ___demoParticles_4;
	// System.Single UnityStandardAssets.SceneUtils.ParticleSceneControls::spawnOffset
	float ___spawnOffset_5;
	// System.Single UnityStandardAssets.SceneUtils.ParticleSceneControls::multiply
	float ___multiply_6;
	// System.Boolean UnityStandardAssets.SceneUtils.ParticleSceneControls::clearOnChange
	bool ___clearOnChange_7;
	// UnityEngine.UI.Text UnityStandardAssets.SceneUtils.ParticleSceneControls::titleText
	Text_t1901882714 * ___titleText_8;
	// UnityEngine.Transform UnityStandardAssets.SceneUtils.ParticleSceneControls::sceneCamera
	Transform_t3600365921 * ___sceneCamera_9;
	// UnityEngine.UI.Text UnityStandardAssets.SceneUtils.ParticleSceneControls::instructionText
	Text_t1901882714 * ___instructionText_10;
	// UnityEngine.UI.Button UnityStandardAssets.SceneUtils.ParticleSceneControls::previousButton
	Button_t4055032469 * ___previousButton_11;
	// UnityEngine.UI.Button UnityStandardAssets.SceneUtils.ParticleSceneControls::nextButton
	Button_t4055032469 * ___nextButton_12;
	// UnityEngine.UI.GraphicRaycaster UnityStandardAssets.SceneUtils.ParticleSceneControls::graphicRaycaster
	GraphicRaycaster_t2999697109 * ___graphicRaycaster_13;
	// UnityEngine.EventSystems.EventSystem UnityStandardAssets.SceneUtils.ParticleSceneControls::eventSystem
	EventSystem_t1003666588 * ___eventSystem_14;
	// UnityStandardAssets.Effects.ParticleSystemMultiplier UnityStandardAssets.SceneUtils.ParticleSceneControls::m_ParticleMultiplier
	ParticleSystemMultiplier_t2770350653 * ___m_ParticleMultiplier_15;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityStandardAssets.SceneUtils.ParticleSceneControls::m_CurrentParticleList
	List_1_t777473367 * ___m_CurrentParticleList_16;
	// UnityEngine.Transform UnityStandardAssets.SceneUtils.ParticleSceneControls::m_Instance
	Transform_t3600365921 * ___m_Instance_17;
	// UnityEngine.Vector3 UnityStandardAssets.SceneUtils.ParticleSceneControls::m_CamOffsetVelocity
	Vector3_t3722313464  ___m_CamOffsetVelocity_19;
	// UnityEngine.Vector3 UnityStandardAssets.SceneUtils.ParticleSceneControls::m_LastPos
	Vector3_t3722313464  ___m_LastPos_20;

public:
	inline static int32_t get_offset_of_demoParticles_4() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___demoParticles_4)); }
	inline DemoParticleSystemList_t4288938153 * get_demoParticles_4() const { return ___demoParticles_4; }
	inline DemoParticleSystemList_t4288938153 ** get_address_of_demoParticles_4() { return &___demoParticles_4; }
	inline void set_demoParticles_4(DemoParticleSystemList_t4288938153 * value)
	{
		___demoParticles_4 = value;
		Il2CppCodeGenWriteBarrier((&___demoParticles_4), value);
	}

	inline static int32_t get_offset_of_spawnOffset_5() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___spawnOffset_5)); }
	inline float get_spawnOffset_5() const { return ___spawnOffset_5; }
	inline float* get_address_of_spawnOffset_5() { return &___spawnOffset_5; }
	inline void set_spawnOffset_5(float value)
	{
		___spawnOffset_5 = value;
	}

	inline static int32_t get_offset_of_multiply_6() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___multiply_6)); }
	inline float get_multiply_6() const { return ___multiply_6; }
	inline float* get_address_of_multiply_6() { return &___multiply_6; }
	inline void set_multiply_6(float value)
	{
		___multiply_6 = value;
	}

	inline static int32_t get_offset_of_clearOnChange_7() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___clearOnChange_7)); }
	inline bool get_clearOnChange_7() const { return ___clearOnChange_7; }
	inline bool* get_address_of_clearOnChange_7() { return &___clearOnChange_7; }
	inline void set_clearOnChange_7(bool value)
	{
		___clearOnChange_7 = value;
	}

	inline static int32_t get_offset_of_titleText_8() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___titleText_8)); }
	inline Text_t1901882714 * get_titleText_8() const { return ___titleText_8; }
	inline Text_t1901882714 ** get_address_of_titleText_8() { return &___titleText_8; }
	inline void set_titleText_8(Text_t1901882714 * value)
	{
		___titleText_8 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_8), value);
	}

	inline static int32_t get_offset_of_sceneCamera_9() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___sceneCamera_9)); }
	inline Transform_t3600365921 * get_sceneCamera_9() const { return ___sceneCamera_9; }
	inline Transform_t3600365921 ** get_address_of_sceneCamera_9() { return &___sceneCamera_9; }
	inline void set_sceneCamera_9(Transform_t3600365921 * value)
	{
		___sceneCamera_9 = value;
		Il2CppCodeGenWriteBarrier((&___sceneCamera_9), value);
	}

	inline static int32_t get_offset_of_instructionText_10() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___instructionText_10)); }
	inline Text_t1901882714 * get_instructionText_10() const { return ___instructionText_10; }
	inline Text_t1901882714 ** get_address_of_instructionText_10() { return &___instructionText_10; }
	inline void set_instructionText_10(Text_t1901882714 * value)
	{
		___instructionText_10 = value;
		Il2CppCodeGenWriteBarrier((&___instructionText_10), value);
	}

	inline static int32_t get_offset_of_previousButton_11() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___previousButton_11)); }
	inline Button_t4055032469 * get_previousButton_11() const { return ___previousButton_11; }
	inline Button_t4055032469 ** get_address_of_previousButton_11() { return &___previousButton_11; }
	inline void set_previousButton_11(Button_t4055032469 * value)
	{
		___previousButton_11 = value;
		Il2CppCodeGenWriteBarrier((&___previousButton_11), value);
	}

	inline static int32_t get_offset_of_nextButton_12() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___nextButton_12)); }
	inline Button_t4055032469 * get_nextButton_12() const { return ___nextButton_12; }
	inline Button_t4055032469 ** get_address_of_nextButton_12() { return &___nextButton_12; }
	inline void set_nextButton_12(Button_t4055032469 * value)
	{
		___nextButton_12 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_12), value);
	}

	inline static int32_t get_offset_of_graphicRaycaster_13() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___graphicRaycaster_13)); }
	inline GraphicRaycaster_t2999697109 * get_graphicRaycaster_13() const { return ___graphicRaycaster_13; }
	inline GraphicRaycaster_t2999697109 ** get_address_of_graphicRaycaster_13() { return &___graphicRaycaster_13; }
	inline void set_graphicRaycaster_13(GraphicRaycaster_t2999697109 * value)
	{
		___graphicRaycaster_13 = value;
		Il2CppCodeGenWriteBarrier((&___graphicRaycaster_13), value);
	}

	inline static int32_t get_offset_of_eventSystem_14() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___eventSystem_14)); }
	inline EventSystem_t1003666588 * get_eventSystem_14() const { return ___eventSystem_14; }
	inline EventSystem_t1003666588 ** get_address_of_eventSystem_14() { return &___eventSystem_14; }
	inline void set_eventSystem_14(EventSystem_t1003666588 * value)
	{
		___eventSystem_14 = value;
		Il2CppCodeGenWriteBarrier((&___eventSystem_14), value);
	}

	inline static int32_t get_offset_of_m_ParticleMultiplier_15() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___m_ParticleMultiplier_15)); }
	inline ParticleSystemMultiplier_t2770350653 * get_m_ParticleMultiplier_15() const { return ___m_ParticleMultiplier_15; }
	inline ParticleSystemMultiplier_t2770350653 ** get_address_of_m_ParticleMultiplier_15() { return &___m_ParticleMultiplier_15; }
	inline void set_m_ParticleMultiplier_15(ParticleSystemMultiplier_t2770350653 * value)
	{
		___m_ParticleMultiplier_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleMultiplier_15), value);
	}

	inline static int32_t get_offset_of_m_CurrentParticleList_16() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___m_CurrentParticleList_16)); }
	inline List_1_t777473367 * get_m_CurrentParticleList_16() const { return ___m_CurrentParticleList_16; }
	inline List_1_t777473367 ** get_address_of_m_CurrentParticleList_16() { return &___m_CurrentParticleList_16; }
	inline void set_m_CurrentParticleList_16(List_1_t777473367 * value)
	{
		___m_CurrentParticleList_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentParticleList_16), value);
	}

	inline static int32_t get_offset_of_m_Instance_17() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___m_Instance_17)); }
	inline Transform_t3600365921 * get_m_Instance_17() const { return ___m_Instance_17; }
	inline Transform_t3600365921 ** get_address_of_m_Instance_17() { return &___m_Instance_17; }
	inline void set_m_Instance_17(Transform_t3600365921 * value)
	{
		___m_Instance_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Instance_17), value);
	}

	inline static int32_t get_offset_of_m_CamOffsetVelocity_19() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___m_CamOffsetVelocity_19)); }
	inline Vector3_t3722313464  get_m_CamOffsetVelocity_19() const { return ___m_CamOffsetVelocity_19; }
	inline Vector3_t3722313464 * get_address_of_m_CamOffsetVelocity_19() { return &___m_CamOffsetVelocity_19; }
	inline void set_m_CamOffsetVelocity_19(Vector3_t3722313464  value)
	{
		___m_CamOffsetVelocity_19 = value;
	}

	inline static int32_t get_offset_of_m_LastPos_20() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203, ___m_LastPos_20)); }
	inline Vector3_t3722313464  get_m_LastPos_20() const { return ___m_LastPos_20; }
	inline Vector3_t3722313464 * get_address_of_m_LastPos_20() { return &___m_LastPos_20; }
	inline void set_m_LastPos_20(Vector3_t3722313464  value)
	{
		___m_LastPos_20 = value;
	}
};

struct ParticleSceneControls_t306931203_StaticFields
{
public:
	// System.Int32 UnityStandardAssets.SceneUtils.ParticleSceneControls::s_SelectedIndex
	int32_t ___s_SelectedIndex_18;
	// UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem UnityStandardAssets.SceneUtils.ParticleSceneControls::s_Selected
	DemoParticleSystem_t1195446716 * ___s_Selected_21;

public:
	inline static int32_t get_offset_of_s_SelectedIndex_18() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203_StaticFields, ___s_SelectedIndex_18)); }
	inline int32_t get_s_SelectedIndex_18() const { return ___s_SelectedIndex_18; }
	inline int32_t* get_address_of_s_SelectedIndex_18() { return &___s_SelectedIndex_18; }
	inline void set_s_SelectedIndex_18(int32_t value)
	{
		___s_SelectedIndex_18 = value;
	}

	inline static int32_t get_offset_of_s_Selected_21() { return static_cast<int32_t>(offsetof(ParticleSceneControls_t306931203_StaticFields, ___s_Selected_21)); }
	inline DemoParticleSystem_t1195446716 * get_s_Selected_21() const { return ___s_Selected_21; }
	inline DemoParticleSystem_t1195446716 ** get_address_of_s_Selected_21() { return &___s_Selected_21; }
	inline void set_s_Selected_21(DemoParticleSystem_t1195446716 * value)
	{
		___s_Selected_21 = value;
		Il2CppCodeGenWriteBarrier((&___s_Selected_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESCENECONTROLS_T306931203_H
#ifndef PLACETARGETWITHMOUSE_T3823755269_H
#define PLACETARGETWITHMOUSE_T3823755269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.PlaceTargetWithMouse
struct  PlaceTargetWithMouse_t3823755269  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::surfaceOffset
	float ___surfaceOffset_4;
	// UnityEngine.GameObject UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::setTargetOn
	GameObject_t1113636619 * ___setTargetOn_5;

public:
	inline static int32_t get_offset_of_surfaceOffset_4() { return static_cast<int32_t>(offsetof(PlaceTargetWithMouse_t3823755269, ___surfaceOffset_4)); }
	inline float get_surfaceOffset_4() const { return ___surfaceOffset_4; }
	inline float* get_address_of_surfaceOffset_4() { return &___surfaceOffset_4; }
	inline void set_surfaceOffset_4(float value)
	{
		___surfaceOffset_4 = value;
	}

	inline static int32_t get_offset_of_setTargetOn_5() { return static_cast<int32_t>(offsetof(PlaceTargetWithMouse_t3823755269, ___setTargetOn_5)); }
	inline GameObject_t1113636619 * get_setTargetOn_5() const { return ___setTargetOn_5; }
	inline GameObject_t1113636619 ** get_address_of_setTargetOn_5() { return &___setTargetOn_5; }
	inline void set_setTargetOn_5(GameObject_t1113636619 * value)
	{
		___setTargetOn_5 = value;
		Il2CppCodeGenWriteBarrier((&___setTargetOn_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACETARGETWITHMOUSE_T3823755269_H
#ifndef SLOWMOBUTTON_T57138989_H
#define SLOWMOBUTTON_T57138989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.SceneUtils.SlowMoButton
struct  SlowMoButton_t57138989  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite UnityStandardAssets.SceneUtils.SlowMoButton::FullSpeedTex
	Sprite_t280657092 * ___FullSpeedTex_4;
	// UnityEngine.Sprite UnityStandardAssets.SceneUtils.SlowMoButton::SlowSpeedTex
	Sprite_t280657092 * ___SlowSpeedTex_5;
	// System.Single UnityStandardAssets.SceneUtils.SlowMoButton::fullSpeed
	float ___fullSpeed_6;
	// System.Single UnityStandardAssets.SceneUtils.SlowMoButton::slowSpeed
	float ___slowSpeed_7;
	// UnityEngine.UI.Button UnityStandardAssets.SceneUtils.SlowMoButton::button
	Button_t4055032469 * ___button_8;
	// System.Boolean UnityStandardAssets.SceneUtils.SlowMoButton::m_SlowMo
	bool ___m_SlowMo_9;

public:
	inline static int32_t get_offset_of_FullSpeedTex_4() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___FullSpeedTex_4)); }
	inline Sprite_t280657092 * get_FullSpeedTex_4() const { return ___FullSpeedTex_4; }
	inline Sprite_t280657092 ** get_address_of_FullSpeedTex_4() { return &___FullSpeedTex_4; }
	inline void set_FullSpeedTex_4(Sprite_t280657092 * value)
	{
		___FullSpeedTex_4 = value;
		Il2CppCodeGenWriteBarrier((&___FullSpeedTex_4), value);
	}

	inline static int32_t get_offset_of_SlowSpeedTex_5() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___SlowSpeedTex_5)); }
	inline Sprite_t280657092 * get_SlowSpeedTex_5() const { return ___SlowSpeedTex_5; }
	inline Sprite_t280657092 ** get_address_of_SlowSpeedTex_5() { return &___SlowSpeedTex_5; }
	inline void set_SlowSpeedTex_5(Sprite_t280657092 * value)
	{
		___SlowSpeedTex_5 = value;
		Il2CppCodeGenWriteBarrier((&___SlowSpeedTex_5), value);
	}

	inline static int32_t get_offset_of_fullSpeed_6() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___fullSpeed_6)); }
	inline float get_fullSpeed_6() const { return ___fullSpeed_6; }
	inline float* get_address_of_fullSpeed_6() { return &___fullSpeed_6; }
	inline void set_fullSpeed_6(float value)
	{
		___fullSpeed_6 = value;
	}

	inline static int32_t get_offset_of_slowSpeed_7() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___slowSpeed_7)); }
	inline float get_slowSpeed_7() const { return ___slowSpeed_7; }
	inline float* get_address_of_slowSpeed_7() { return &___slowSpeed_7; }
	inline void set_slowSpeed_7(float value)
	{
		___slowSpeed_7 = value;
	}

	inline static int32_t get_offset_of_button_8() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___button_8)); }
	inline Button_t4055032469 * get_button_8() const { return ___button_8; }
	inline Button_t4055032469 ** get_address_of_button_8() { return &___button_8; }
	inline void set_button_8(Button_t4055032469 * value)
	{
		___button_8 = value;
		Il2CppCodeGenWriteBarrier((&___button_8), value);
	}

	inline static int32_t get_offset_of_m_SlowMo_9() { return static_cast<int32_t>(offsetof(SlowMoButton_t57138989, ___m_SlowMo_9)); }
	inline bool get_m_SlowMo_9() const { return ___m_SlowMo_9; }
	inline bool* get_address_of_m_SlowMo_9() { return &___m_SlowMo_9; }
	inline void set_m_SlowMo_9(bool value)
	{
		___m_SlowMo_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOWMOBUTTON_T57138989_H
#ifndef ACTIVATETRIGGER_T3349759092_H
#define ACTIVATETRIGGER_T3349759092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger
struct  ActivateTrigger_t3349759092  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.ActivateTrigger/Mode UnityStandardAssets.Utility.ActivateTrigger::action
	int32_t ___action_4;
	// UnityEngine.Object UnityStandardAssets.Utility.ActivateTrigger::target
	Object_t631007953 * ___target_5;
	// UnityEngine.GameObject UnityStandardAssets.Utility.ActivateTrigger::source
	GameObject_t1113636619 * ___source_6;
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger::triggerCount
	int32_t ___triggerCount_7;
	// System.Boolean UnityStandardAssets.Utility.ActivateTrigger::repeatTrigger
	bool ___repeatTrigger_8;

public:
	inline static int32_t get_offset_of_action_4() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___action_4)); }
	inline int32_t get_action_4() const { return ___action_4; }
	inline int32_t* get_address_of_action_4() { return &___action_4; }
	inline void set_action_4(int32_t value)
	{
		___action_4 = value;
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___target_5)); }
	inline Object_t631007953 * get_target_5() const { return ___target_5; }
	inline Object_t631007953 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Object_t631007953 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((&___target_5), value);
	}

	inline static int32_t get_offset_of_source_6() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___source_6)); }
	inline GameObject_t1113636619 * get_source_6() const { return ___source_6; }
	inline GameObject_t1113636619 ** get_address_of_source_6() { return &___source_6; }
	inline void set_source_6(GameObject_t1113636619 * value)
	{
		___source_6 = value;
		Il2CppCodeGenWriteBarrier((&___source_6), value);
	}

	inline static int32_t get_offset_of_triggerCount_7() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___triggerCount_7)); }
	inline int32_t get_triggerCount_7() const { return ___triggerCount_7; }
	inline int32_t* get_address_of_triggerCount_7() { return &___triggerCount_7; }
	inline void set_triggerCount_7(int32_t value)
	{
		___triggerCount_7 = value;
	}

	inline static int32_t get_offset_of_repeatTrigger_8() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___repeatTrigger_8)); }
	inline bool get_repeatTrigger_8() const { return ___repeatTrigger_8; }
	inline bool* get_address_of_repeatTrigger_8() { return &___repeatTrigger_8; }
	inline void set_repeatTrigger_8(bool value)
	{
		___repeatTrigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATETRIGGER_T3349759092_H
#ifndef AUTOMOBILESHADERSWITCH_T568447889_H
#define AUTOMOBILESHADERSWITCH_T568447889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch
struct  AutoMobileShaderSwitch_t568447889  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList UnityStandardAssets.Utility.AutoMobileShaderSwitch::m_ReplacementList
	ReplacementList_t1887104210 * ___m_ReplacementList_4;

public:
	inline static int32_t get_offset_of_m_ReplacementList_4() { return static_cast<int32_t>(offsetof(AutoMobileShaderSwitch_t568447889, ___m_ReplacementList_4)); }
	inline ReplacementList_t1887104210 * get_m_ReplacementList_4() const { return ___m_ReplacementList_4; }
	inline ReplacementList_t1887104210 ** get_address_of_m_ReplacementList_4() { return &___m_ReplacementList_4; }
	inline void set_m_ReplacementList_4(ReplacementList_t1887104210 * value)
	{
		___m_ReplacementList_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReplacementList_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOBILESHADERSWITCH_T568447889_H
#ifndef AUTOMOVEANDROTATE_T2437913015_H
#define AUTOMOVEANDROTATE_T2437913015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate
struct  AutoMoveAndRotate_t2437913015  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::moveUnitsPerSecond
	Vector3andSpace_t219844479 * ___moveUnitsPerSecond_4;
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::rotateDegreesPerSecond
	Vector3andSpace_t219844479 * ___rotateDegreesPerSecond_5;
	// System.Boolean UnityStandardAssets.Utility.AutoMoveAndRotate::ignoreTimescale
	bool ___ignoreTimescale_6;
	// System.Single UnityStandardAssets.Utility.AutoMoveAndRotate::m_LastRealTime
	float ___m_LastRealTime_7;

public:
	inline static int32_t get_offset_of_moveUnitsPerSecond_4() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___moveUnitsPerSecond_4)); }
	inline Vector3andSpace_t219844479 * get_moveUnitsPerSecond_4() const { return ___moveUnitsPerSecond_4; }
	inline Vector3andSpace_t219844479 ** get_address_of_moveUnitsPerSecond_4() { return &___moveUnitsPerSecond_4; }
	inline void set_moveUnitsPerSecond_4(Vector3andSpace_t219844479 * value)
	{
		___moveUnitsPerSecond_4 = value;
		Il2CppCodeGenWriteBarrier((&___moveUnitsPerSecond_4), value);
	}

	inline static int32_t get_offset_of_rotateDegreesPerSecond_5() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___rotateDegreesPerSecond_5)); }
	inline Vector3andSpace_t219844479 * get_rotateDegreesPerSecond_5() const { return ___rotateDegreesPerSecond_5; }
	inline Vector3andSpace_t219844479 ** get_address_of_rotateDegreesPerSecond_5() { return &___rotateDegreesPerSecond_5; }
	inline void set_rotateDegreesPerSecond_5(Vector3andSpace_t219844479 * value)
	{
		___rotateDegreesPerSecond_5 = value;
		Il2CppCodeGenWriteBarrier((&___rotateDegreesPerSecond_5), value);
	}

	inline static int32_t get_offset_of_ignoreTimescale_6() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___ignoreTimescale_6)); }
	inline bool get_ignoreTimescale_6() const { return ___ignoreTimescale_6; }
	inline bool* get_address_of_ignoreTimescale_6() { return &___ignoreTimescale_6; }
	inline void set_ignoreTimescale_6(bool value)
	{
		___ignoreTimescale_6 = value;
	}

	inline static int32_t get_offset_of_m_LastRealTime_7() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___m_LastRealTime_7)); }
	inline float get_m_LastRealTime_7() const { return ___m_LastRealTime_7; }
	inline float* get_address_of_m_LastRealTime_7() { return &___m_LastRealTime_7; }
	inline void set_m_LastRealTime_7(float value)
	{
		___m_LastRealTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOVEANDROTATE_T2437913015_H
#ifndef DRAGRIGIDBODY_T1600652016_H
#define DRAGRIGIDBODY_T1600652016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody
struct  DragRigidbody_t1600652016  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpringJoint UnityStandardAssets.Utility.DragRigidbody::m_SpringJoint
	SpringJoint_t1912369980 * ___m_SpringJoint_10;

public:
	inline static int32_t get_offset_of_m_SpringJoint_10() { return static_cast<int32_t>(offsetof(DragRigidbody_t1600652016, ___m_SpringJoint_10)); }
	inline SpringJoint_t1912369980 * get_m_SpringJoint_10() const { return ___m_SpringJoint_10; }
	inline SpringJoint_t1912369980 ** get_address_of_m_SpringJoint_10() { return &___m_SpringJoint_10; }
	inline void set_m_SpringJoint_10(SpringJoint_t1912369980 * value)
	{
		___m_SpringJoint_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpringJoint_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGRIGIDBODY_T1600652016_H
#ifndef DYNAMICSHADOWSETTINGS_T59119858_H
#define DYNAMICSHADOWSETTINGS_T59119858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DynamicShadowSettings
struct  DynamicShadowSettings_t59119858  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Light UnityStandardAssets.Utility.DynamicShadowSettings::sunLight
	Light_t3756812086 * ___sunLight_4;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minHeight
	float ___minHeight_5;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowDistance
	float ___minShadowDistance_6;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowBias
	float ___minShadowBias_7;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxHeight
	float ___maxHeight_8;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowDistance
	float ___maxShadowDistance_9;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowBias
	float ___maxShadowBias_10;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::adaptTime
	float ___adaptTime_11;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_SmoothHeight
	float ___m_SmoothHeight_12;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_ChangeSpeed
	float ___m_ChangeSpeed_13;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_OriginalStrength
	float ___m_OriginalStrength_14;

public:
	inline static int32_t get_offset_of_sunLight_4() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___sunLight_4)); }
	inline Light_t3756812086 * get_sunLight_4() const { return ___sunLight_4; }
	inline Light_t3756812086 ** get_address_of_sunLight_4() { return &___sunLight_4; }
	inline void set_sunLight_4(Light_t3756812086 * value)
	{
		___sunLight_4 = value;
		Il2CppCodeGenWriteBarrier((&___sunLight_4), value);
	}

	inline static int32_t get_offset_of_minHeight_5() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minHeight_5)); }
	inline float get_minHeight_5() const { return ___minHeight_5; }
	inline float* get_address_of_minHeight_5() { return &___minHeight_5; }
	inline void set_minHeight_5(float value)
	{
		___minHeight_5 = value;
	}

	inline static int32_t get_offset_of_minShadowDistance_6() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowDistance_6)); }
	inline float get_minShadowDistance_6() const { return ___minShadowDistance_6; }
	inline float* get_address_of_minShadowDistance_6() { return &___minShadowDistance_6; }
	inline void set_minShadowDistance_6(float value)
	{
		___minShadowDistance_6 = value;
	}

	inline static int32_t get_offset_of_minShadowBias_7() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowBias_7)); }
	inline float get_minShadowBias_7() const { return ___minShadowBias_7; }
	inline float* get_address_of_minShadowBias_7() { return &___minShadowBias_7; }
	inline void set_minShadowBias_7(float value)
	{
		___minShadowBias_7 = value;
	}

	inline static int32_t get_offset_of_maxHeight_8() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxHeight_8)); }
	inline float get_maxHeight_8() const { return ___maxHeight_8; }
	inline float* get_address_of_maxHeight_8() { return &___maxHeight_8; }
	inline void set_maxHeight_8(float value)
	{
		___maxHeight_8 = value;
	}

	inline static int32_t get_offset_of_maxShadowDistance_9() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowDistance_9)); }
	inline float get_maxShadowDistance_9() const { return ___maxShadowDistance_9; }
	inline float* get_address_of_maxShadowDistance_9() { return &___maxShadowDistance_9; }
	inline void set_maxShadowDistance_9(float value)
	{
		___maxShadowDistance_9 = value;
	}

	inline static int32_t get_offset_of_maxShadowBias_10() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowBias_10)); }
	inline float get_maxShadowBias_10() const { return ___maxShadowBias_10; }
	inline float* get_address_of_maxShadowBias_10() { return &___maxShadowBias_10; }
	inline void set_maxShadowBias_10(float value)
	{
		___maxShadowBias_10 = value;
	}

	inline static int32_t get_offset_of_adaptTime_11() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___adaptTime_11)); }
	inline float get_adaptTime_11() const { return ___adaptTime_11; }
	inline float* get_address_of_adaptTime_11() { return &___adaptTime_11; }
	inline void set_adaptTime_11(float value)
	{
		___adaptTime_11 = value;
	}

	inline static int32_t get_offset_of_m_SmoothHeight_12() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_SmoothHeight_12)); }
	inline float get_m_SmoothHeight_12() const { return ___m_SmoothHeight_12; }
	inline float* get_address_of_m_SmoothHeight_12() { return &___m_SmoothHeight_12; }
	inline void set_m_SmoothHeight_12(float value)
	{
		___m_SmoothHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChangeSpeed_13() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_ChangeSpeed_13)); }
	inline float get_m_ChangeSpeed_13() const { return ___m_ChangeSpeed_13; }
	inline float* get_address_of_m_ChangeSpeed_13() { return &___m_ChangeSpeed_13; }
	inline void set_m_ChangeSpeed_13(float value)
	{
		___m_ChangeSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_OriginalStrength_14() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_OriginalStrength_14)); }
	inline float get_m_OriginalStrength_14() const { return ___m_OriginalStrength_14; }
	inline float* get_address_of_m_OriginalStrength_14() { return &___m_OriginalStrength_14; }
	inline void set_m_OriginalStrength_14(float value)
	{
		___m_OriginalStrength_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICSHADOWSETTINGS_T59119858_H
#ifndef FPSCOUNTER_T2351221284_H
#define FPSCOUNTER_T2351221284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FPSCounter
struct  FPSCounter_t2351221284  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_FpsAccumulator
	int32_t ___m_FpsAccumulator_5;
	// System.Single UnityStandardAssets.Utility.FPSCounter::m_FpsNextPeriod
	float ___m_FpsNextPeriod_6;
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_CurrentFps
	int32_t ___m_CurrentFps_7;
	// UnityEngine.UI.Text UnityStandardAssets.Utility.FPSCounter::m_Text
	Text_t1901882714 * ___m_Text_9;

public:
	inline static int32_t get_offset_of_m_FpsAccumulator_5() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsAccumulator_5)); }
	inline int32_t get_m_FpsAccumulator_5() const { return ___m_FpsAccumulator_5; }
	inline int32_t* get_address_of_m_FpsAccumulator_5() { return &___m_FpsAccumulator_5; }
	inline void set_m_FpsAccumulator_5(int32_t value)
	{
		___m_FpsAccumulator_5 = value;
	}

	inline static int32_t get_offset_of_m_FpsNextPeriod_6() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsNextPeriod_6)); }
	inline float get_m_FpsNextPeriod_6() const { return ___m_FpsNextPeriod_6; }
	inline float* get_address_of_m_FpsNextPeriod_6() { return &___m_FpsNextPeriod_6; }
	inline void set_m_FpsNextPeriod_6(float value)
	{
		___m_FpsNextPeriod_6 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFps_7() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_CurrentFps_7)); }
	inline int32_t get_m_CurrentFps_7() const { return ___m_CurrentFps_7; }
	inline int32_t* get_address_of_m_CurrentFps_7() { return &___m_CurrentFps_7; }
	inline void set_m_CurrentFps_7(int32_t value)
	{
		___m_CurrentFps_7 = value;
	}

	inline static int32_t get_offset_of_m_Text_9() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_Text_9)); }
	inline Text_t1901882714 * get_m_Text_9() const { return ___m_Text_9; }
	inline Text_t1901882714 ** get_address_of_m_Text_9() { return &___m_Text_9; }
	inline void set_m_Text_9(Text_t1901882714 * value)
	{
		___m_Text_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTER_T2351221284_H
#ifndef FOLLOWTARGET_T166153614_H
#define FOLLOWTARGET_T166153614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FollowTarget
struct  FollowTarget_t166153614  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.FollowTarget::target
	Transform_t3600365921 * ___target_4;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.FollowTarget::offset
	Vector3_t3722313464  ___offset_5;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___offset_5)); }
	inline Vector3_t3722313464  get_offset_5() const { return ___offset_5; }
	inline Vector3_t3722313464 * get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(Vector3_t3722313464  value)
	{
		___offset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_T166153614_H
#ifndef OBJECTRESETTER_T639177103_H
#define OBJECTRESETTER_T639177103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter
struct  ObjectResetter_t639177103  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.ObjectResetter::originalPosition
	Vector3_t3722313464  ___originalPosition_4;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.ObjectResetter::originalRotation
	Quaternion_t2301928331  ___originalRotation_5;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityStandardAssets.Utility.ObjectResetter::originalStructure
	List_1_t777473367 * ___originalStructure_6;
	// UnityEngine.Rigidbody UnityStandardAssets.Utility.ObjectResetter::Rigidbody
	Rigidbody_t3916780224 * ___Rigidbody_7;

public:
	inline static int32_t get_offset_of_originalPosition_4() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalPosition_4)); }
	inline Vector3_t3722313464  get_originalPosition_4() const { return ___originalPosition_4; }
	inline Vector3_t3722313464 * get_address_of_originalPosition_4() { return &___originalPosition_4; }
	inline void set_originalPosition_4(Vector3_t3722313464  value)
	{
		___originalPosition_4 = value;
	}

	inline static int32_t get_offset_of_originalRotation_5() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalRotation_5)); }
	inline Quaternion_t2301928331  get_originalRotation_5() const { return ___originalRotation_5; }
	inline Quaternion_t2301928331 * get_address_of_originalRotation_5() { return &___originalRotation_5; }
	inline void set_originalRotation_5(Quaternion_t2301928331  value)
	{
		___originalRotation_5 = value;
	}

	inline static int32_t get_offset_of_originalStructure_6() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalStructure_6)); }
	inline List_1_t777473367 * get_originalStructure_6() const { return ___originalStructure_6; }
	inline List_1_t777473367 ** get_address_of_originalStructure_6() { return &___originalStructure_6; }
	inline void set_originalStructure_6(List_1_t777473367 * value)
	{
		___originalStructure_6 = value;
		Il2CppCodeGenWriteBarrier((&___originalStructure_6), value);
	}

	inline static int32_t get_offset_of_Rigidbody_7() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___Rigidbody_7)); }
	inline Rigidbody_t3916780224 * get_Rigidbody_7() const { return ___Rigidbody_7; }
	inline Rigidbody_t3916780224 ** get_address_of_Rigidbody_7() { return &___Rigidbody_7; }
	inline void set_Rigidbody_7(Rigidbody_t3916780224 * value)
	{
		___Rigidbody_7 = value;
		Il2CppCodeGenWriteBarrier((&___Rigidbody_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTRESETTER_T639177103_H
#ifndef PARTICLESYSTEMDESTROYER_T558680695_H
#define PARTICLESYSTEMDESTROYER_T558680695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct  ParticleSystemDestroyer_t558680695  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::minDuration
	float ___minDuration_4;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::maxDuration
	float ___maxDuration_5;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::m_MaxLifetime
	float ___m_MaxLifetime_6;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer::m_EarlyStop
	bool ___m_EarlyStop_7;

public:
	inline static int32_t get_offset_of_minDuration_4() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___minDuration_4)); }
	inline float get_minDuration_4() const { return ___minDuration_4; }
	inline float* get_address_of_minDuration_4() { return &___minDuration_4; }
	inline void set_minDuration_4(float value)
	{
		___minDuration_4 = value;
	}

	inline static int32_t get_offset_of_maxDuration_5() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___maxDuration_5)); }
	inline float get_maxDuration_5() const { return ___maxDuration_5; }
	inline float* get_address_of_maxDuration_5() { return &___maxDuration_5; }
	inline void set_maxDuration_5(float value)
	{
		___maxDuration_5 = value;
	}

	inline static int32_t get_offset_of_m_MaxLifetime_6() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_MaxLifetime_6)); }
	inline float get_m_MaxLifetime_6() const { return ___m_MaxLifetime_6; }
	inline float* get_address_of_m_MaxLifetime_6() { return &___m_MaxLifetime_6; }
	inline void set_m_MaxLifetime_6(float value)
	{
		___m_MaxLifetime_6 = value;
	}

	inline static int32_t get_offset_of_m_EarlyStop_7() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_EarlyStop_7)); }
	inline bool get_m_EarlyStop_7() const { return ___m_EarlyStop_7; }
	inline bool* get_address_of_m_EarlyStop_7() { return &___m_EarlyStop_7; }
	inline void set_m_EarlyStop_7(bool value)
	{
		___m_EarlyStop_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMDESTROYER_T558680695_H
#ifndef PLATFORMSPECIFICCONTENT_T1404549723_H
#define PLATFORMSPECIFICCONTENT_T1404549723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent
struct  PlatformSpecificContent_t1404549723  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup UnityStandardAssets.Utility.PlatformSpecificContent::m_BuildTargetGroup
	int32_t ___m_BuildTargetGroup_4;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.PlatformSpecificContent::m_Content
	GameObjectU5BU5D_t3328599146* ___m_Content_5;
	// UnityEngine.MonoBehaviour[] UnityStandardAssets.Utility.PlatformSpecificContent::m_MonoBehaviours
	MonoBehaviourU5BU5D_t2007329276* ___m_MonoBehaviours_6;
	// System.Boolean UnityStandardAssets.Utility.PlatformSpecificContent::m_ChildrenOfThisObject
	bool ___m_ChildrenOfThisObject_7;

public:
	inline static int32_t get_offset_of_m_BuildTargetGroup_4() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_BuildTargetGroup_4)); }
	inline int32_t get_m_BuildTargetGroup_4() const { return ___m_BuildTargetGroup_4; }
	inline int32_t* get_address_of_m_BuildTargetGroup_4() { return &___m_BuildTargetGroup_4; }
	inline void set_m_BuildTargetGroup_4(int32_t value)
	{
		___m_BuildTargetGroup_4 = value;
	}

	inline static int32_t get_offset_of_m_Content_5() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_Content_5)); }
	inline GameObjectU5BU5D_t3328599146* get_m_Content_5() const { return ___m_Content_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_Content_5() { return &___m_Content_5; }
	inline void set_m_Content_5(GameObjectU5BU5D_t3328599146* value)
	{
		___m_Content_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_5), value);
	}

	inline static int32_t get_offset_of_m_MonoBehaviours_6() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_MonoBehaviours_6)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_m_MonoBehaviours_6() const { return ___m_MonoBehaviours_6; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_m_MonoBehaviours_6() { return &___m_MonoBehaviours_6; }
	inline void set_m_MonoBehaviours_6(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___m_MonoBehaviours_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MonoBehaviours_6), value);
	}

	inline static int32_t get_offset_of_m_ChildrenOfThisObject_7() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_ChildrenOfThisObject_7)); }
	inline bool get_m_ChildrenOfThisObject_7() const { return ___m_ChildrenOfThisObject_7; }
	inline bool* get_address_of_m_ChildrenOfThisObject_7() { return &___m_ChildrenOfThisObject_7; }
	inline void set_m_ChildrenOfThisObject_7(bool value)
	{
		___m_ChildrenOfThisObject_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMSPECIFICCONTENT_T1404549723_H
#ifndef SIMPLEACTIVATORMENU_T1387811551_H
#define SIMPLEACTIVATORMENU_T1387811551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleActivatorMenu
struct  SimpleActivatorMenu_t1387811551  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUIText UnityStandardAssets.Utility.SimpleActivatorMenu::camSwitchButton
	GUIText_t402233326 * ___camSwitchButton_4;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.SimpleActivatorMenu::objects
	GameObjectU5BU5D_t3328599146* ___objects_5;
	// System.Int32 UnityStandardAssets.Utility.SimpleActivatorMenu::m_CurrentActiveObject
	int32_t ___m_CurrentActiveObject_6;

public:
	inline static int32_t get_offset_of_camSwitchButton_4() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___camSwitchButton_4)); }
	inline GUIText_t402233326 * get_camSwitchButton_4() const { return ___camSwitchButton_4; }
	inline GUIText_t402233326 ** get_address_of_camSwitchButton_4() { return &___camSwitchButton_4; }
	inline void set_camSwitchButton_4(GUIText_t402233326 * value)
	{
		___camSwitchButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___camSwitchButton_4), value);
	}

	inline static int32_t get_offset_of_objects_5() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___objects_5)); }
	inline GameObjectU5BU5D_t3328599146* get_objects_5() const { return ___objects_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_objects_5() { return &___objects_5; }
	inline void set_objects_5(GameObjectU5BU5D_t3328599146* value)
	{
		___objects_5 = value;
		Il2CppCodeGenWriteBarrier((&___objects_5), value);
	}

	inline static int32_t get_offset_of_m_CurrentActiveObject_6() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___m_CurrentActiveObject_6)); }
	inline int32_t get_m_CurrentActiveObject_6() const { return ___m_CurrentActiveObject_6; }
	inline int32_t* get_address_of_m_CurrentActiveObject_6() { return &___m_CurrentActiveObject_6; }
	inline void set_m_CurrentActiveObject_6(int32_t value)
	{
		___m_CurrentActiveObject_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEACTIVATORMENU_T1387811551_H
#ifndef SIMPLEMOUSEROTATOR_T2364742953_H
#define SIMPLEMOUSEROTATOR_T2364742953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleMouseRotator
struct  SimpleMouseRotator_t2364742953  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.Utility.SimpleMouseRotator::rotationRange
	Vector2_t2156229523  ___rotationRange_4;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::rotationSpeed
	float ___rotationSpeed_5;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::dampingTime
	float ___dampingTime_6;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroVerticalOnMobile
	bool ___autoZeroVerticalOnMobile_7;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroHorizontalOnMobile
	bool ___autoZeroHorizontalOnMobile_8;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::relative
	bool ___relative_9;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_TargetAngles
	Vector3_t3722313464  ___m_TargetAngles_10;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowAngles
	Vector3_t3722313464  ___m_FollowAngles_11;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowVelocity
	Vector3_t3722313464  ___m_FollowVelocity_12;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.SimpleMouseRotator::m_OriginalRotation
	Quaternion_t2301928331  ___m_OriginalRotation_13;

public:
	inline static int32_t get_offset_of_rotationRange_4() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___rotationRange_4)); }
	inline Vector2_t2156229523  get_rotationRange_4() const { return ___rotationRange_4; }
	inline Vector2_t2156229523 * get_address_of_rotationRange_4() { return &___rotationRange_4; }
	inline void set_rotationRange_4(Vector2_t2156229523  value)
	{
		___rotationRange_4 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_5() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___rotationSpeed_5)); }
	inline float get_rotationSpeed_5() const { return ___rotationSpeed_5; }
	inline float* get_address_of_rotationSpeed_5() { return &___rotationSpeed_5; }
	inline void set_rotationSpeed_5(float value)
	{
		___rotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_dampingTime_6() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___dampingTime_6)); }
	inline float get_dampingTime_6() const { return ___dampingTime_6; }
	inline float* get_address_of_dampingTime_6() { return &___dampingTime_6; }
	inline void set_dampingTime_6(float value)
	{
		___dampingTime_6 = value;
	}

	inline static int32_t get_offset_of_autoZeroVerticalOnMobile_7() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___autoZeroVerticalOnMobile_7)); }
	inline bool get_autoZeroVerticalOnMobile_7() const { return ___autoZeroVerticalOnMobile_7; }
	inline bool* get_address_of_autoZeroVerticalOnMobile_7() { return &___autoZeroVerticalOnMobile_7; }
	inline void set_autoZeroVerticalOnMobile_7(bool value)
	{
		___autoZeroVerticalOnMobile_7 = value;
	}

	inline static int32_t get_offset_of_autoZeroHorizontalOnMobile_8() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___autoZeroHorizontalOnMobile_8)); }
	inline bool get_autoZeroHorizontalOnMobile_8() const { return ___autoZeroHorizontalOnMobile_8; }
	inline bool* get_address_of_autoZeroHorizontalOnMobile_8() { return &___autoZeroHorizontalOnMobile_8; }
	inline void set_autoZeroHorizontalOnMobile_8(bool value)
	{
		___autoZeroHorizontalOnMobile_8 = value;
	}

	inline static int32_t get_offset_of_relative_9() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___relative_9)); }
	inline bool get_relative_9() const { return ___relative_9; }
	inline bool* get_address_of_relative_9() { return &___relative_9; }
	inline void set_relative_9(bool value)
	{
		___relative_9 = value;
	}

	inline static int32_t get_offset_of_m_TargetAngles_10() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_TargetAngles_10)); }
	inline Vector3_t3722313464  get_m_TargetAngles_10() const { return ___m_TargetAngles_10; }
	inline Vector3_t3722313464 * get_address_of_m_TargetAngles_10() { return &___m_TargetAngles_10; }
	inline void set_m_TargetAngles_10(Vector3_t3722313464  value)
	{
		___m_TargetAngles_10 = value;
	}

	inline static int32_t get_offset_of_m_FollowAngles_11() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_FollowAngles_11)); }
	inline Vector3_t3722313464  get_m_FollowAngles_11() const { return ___m_FollowAngles_11; }
	inline Vector3_t3722313464 * get_address_of_m_FollowAngles_11() { return &___m_FollowAngles_11; }
	inline void set_m_FollowAngles_11(Vector3_t3722313464  value)
	{
		___m_FollowAngles_11 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_12() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_FollowVelocity_12)); }
	inline Vector3_t3722313464  get_m_FollowVelocity_12() const { return ___m_FollowVelocity_12; }
	inline Vector3_t3722313464 * get_address_of_m_FollowVelocity_12() { return &___m_FollowVelocity_12; }
	inline void set_m_FollowVelocity_12(Vector3_t3722313464  value)
	{
		___m_FollowVelocity_12 = value;
	}

	inline static int32_t get_offset_of_m_OriginalRotation_13() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_OriginalRotation_13)); }
	inline Quaternion_t2301928331  get_m_OriginalRotation_13() const { return ___m_OriginalRotation_13; }
	inline Quaternion_t2301928331 * get_address_of_m_OriginalRotation_13() { return &___m_OriginalRotation_13; }
	inline void set_m_OriginalRotation_13(Quaternion_t2301928331  value)
	{
		___m_OriginalRotation_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMOUSEROTATOR_T2364742953_H
#ifndef SMOOTHFOLLOW_T4204731361_H
#define SMOOTHFOLLOW_T4204731361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SmoothFollow
struct  SmoothFollow_t4204731361  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.SmoothFollow::target
	Transform_t3600365921 * ___target_4;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::distance
	float ___distance_5;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::height
	float ___height_6;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::rotationDamping
	float ___rotationDamping_7;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::heightDamping
	float ___heightDamping_8;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_height_6() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___height_6)); }
	inline float get_height_6() const { return ___height_6; }
	inline float* get_address_of_height_6() { return &___height_6; }
	inline void set_height_6(float value)
	{
		___height_6 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_7() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___rotationDamping_7)); }
	inline float get_rotationDamping_7() const { return ___rotationDamping_7; }
	inline float* get_address_of_rotationDamping_7() { return &___rotationDamping_7; }
	inline void set_rotationDamping_7(float value)
	{
		___rotationDamping_7 = value;
	}

	inline static int32_t get_offset_of_heightDamping_8() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___heightDamping_8)); }
	inline float get_heightDamping_8() const { return ___heightDamping_8; }
	inline float* get_address_of_heightDamping_8() { return &___heightDamping_8; }
	inline void set_heightDamping_8(float value)
	{
		___heightDamping_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW_T4204731361_H
#ifndef TIMEDOBJECTACTIVATOR_T1846709985_H
#define TIMEDOBJECTACTIVATOR_T1846709985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator
struct  TimedObjectActivator_t1846709985  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entries UnityStandardAssets.Utility.TimedObjectActivator::entries
	Entries_t3168066469 * ___entries_4;

public:
	inline static int32_t get_offset_of_entries_4() { return static_cast<int32_t>(offsetof(TimedObjectActivator_t1846709985, ___entries_4)); }
	inline Entries_t3168066469 * get_entries_4() const { return ___entries_4; }
	inline Entries_t3168066469 ** get_address_of_entries_4() { return &___entries_4; }
	inline void set_entries_4(Entries_t3168066469 * value)
	{
		___entries_4 = value;
		Il2CppCodeGenWriteBarrier((&___entries_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTACTIVATOR_T1846709985_H
#ifndef TIMEDOBJECTDESTRUCTOR_T3438860414_H
#define TIMEDOBJECTDESTRUCTOR_T3438860414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectDestructor
struct  TimedObjectDestructor_t3438860414  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Utility.TimedObjectDestructor::m_TimeOut
	float ___m_TimeOut_4;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectDestructor::m_DetachChildren
	bool ___m_DetachChildren_5;

public:
	inline static int32_t get_offset_of_m_TimeOut_4() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3438860414, ___m_TimeOut_4)); }
	inline float get_m_TimeOut_4() const { return ___m_TimeOut_4; }
	inline float* get_address_of_m_TimeOut_4() { return &___m_TimeOut_4; }
	inline void set_m_TimeOut_4(float value)
	{
		___m_TimeOut_4 = value;
	}

	inline static int32_t get_offset_of_m_DetachChildren_5() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3438860414, ___m_DetachChildren_5)); }
	inline bool get_m_DetachChildren_5() const { return ___m_DetachChildren_5; }
	inline bool* get_address_of_m_DetachChildren_5() { return &___m_DetachChildren_5; }
	inline void set_m_DetachChildren_5(bool value)
	{
		___m_DetachChildren_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTDESTRUCTOR_T3438860414_H
#ifndef WAYPOINTCIRCUIT_T445075330_H
#define WAYPOINTCIRCUIT_T445075330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit
struct  WaypointCircuit_t445075330  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit/WaypointList UnityStandardAssets.Utility.WaypointCircuit::waypointList
	WaypointList_t2584574554 * ___waypointList_4;
	// System.Boolean UnityStandardAssets.Utility.WaypointCircuit::smoothRoute
	bool ___smoothRoute_5;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::numPoints
	int32_t ___numPoints_6;
	// UnityEngine.Vector3[] UnityStandardAssets.Utility.WaypointCircuit::points
	Vector3U5BU5D_t1718750761* ___points_7;
	// System.Single[] UnityStandardAssets.Utility.WaypointCircuit::distances
	SingleU5BU5D_t1444911251* ___distances_8;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::editorVisualisationSubsteps
	float ___editorVisualisationSubsteps_9;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_10;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p0n
	int32_t ___p0n_11;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p1n
	int32_t ___p1n_12;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p2n
	int32_t ___p2n_13;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p3n
	int32_t ___p3n_14;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::i
	float ___i_15;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P0
	Vector3_t3722313464  ___P0_16;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P1
	Vector3_t3722313464  ___P1_17;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P2
	Vector3_t3722313464  ___P2_18;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P3
	Vector3_t3722313464  ___P3_19;

public:
	inline static int32_t get_offset_of_waypointList_4() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___waypointList_4)); }
	inline WaypointList_t2584574554 * get_waypointList_4() const { return ___waypointList_4; }
	inline WaypointList_t2584574554 ** get_address_of_waypointList_4() { return &___waypointList_4; }
	inline void set_waypointList_4(WaypointList_t2584574554 * value)
	{
		___waypointList_4 = value;
		Il2CppCodeGenWriteBarrier((&___waypointList_4), value);
	}

	inline static int32_t get_offset_of_smoothRoute_5() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___smoothRoute_5)); }
	inline bool get_smoothRoute_5() const { return ___smoothRoute_5; }
	inline bool* get_address_of_smoothRoute_5() { return &___smoothRoute_5; }
	inline void set_smoothRoute_5(bool value)
	{
		___smoothRoute_5 = value;
	}

	inline static int32_t get_offset_of_numPoints_6() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___numPoints_6)); }
	inline int32_t get_numPoints_6() const { return ___numPoints_6; }
	inline int32_t* get_address_of_numPoints_6() { return &___numPoints_6; }
	inline void set_numPoints_6(int32_t value)
	{
		___numPoints_6 = value;
	}

	inline static int32_t get_offset_of_points_7() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___points_7)); }
	inline Vector3U5BU5D_t1718750761* get_points_7() const { return ___points_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_points_7() { return &___points_7; }
	inline void set_points_7(Vector3U5BU5D_t1718750761* value)
	{
		___points_7 = value;
		Il2CppCodeGenWriteBarrier((&___points_7), value);
	}

	inline static int32_t get_offset_of_distances_8() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___distances_8)); }
	inline SingleU5BU5D_t1444911251* get_distances_8() const { return ___distances_8; }
	inline SingleU5BU5D_t1444911251** get_address_of_distances_8() { return &___distances_8; }
	inline void set_distances_8(SingleU5BU5D_t1444911251* value)
	{
		___distances_8 = value;
		Il2CppCodeGenWriteBarrier((&___distances_8), value);
	}

	inline static int32_t get_offset_of_editorVisualisationSubsteps_9() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___editorVisualisationSubsteps_9)); }
	inline float get_editorVisualisationSubsteps_9() const { return ___editorVisualisationSubsteps_9; }
	inline float* get_address_of_editorVisualisationSubsteps_9() { return &___editorVisualisationSubsteps_9; }
	inline void set_editorVisualisationSubsteps_9(float value)
	{
		___editorVisualisationSubsteps_9 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___U3CLengthU3Ek__BackingField_10)); }
	inline float get_U3CLengthU3Ek__BackingField_10() const { return ___U3CLengthU3Ek__BackingField_10; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_10() { return &___U3CLengthU3Ek__BackingField_10; }
	inline void set_U3CLengthU3Ek__BackingField_10(float value)
	{
		___U3CLengthU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_p0n_11() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p0n_11)); }
	inline int32_t get_p0n_11() const { return ___p0n_11; }
	inline int32_t* get_address_of_p0n_11() { return &___p0n_11; }
	inline void set_p0n_11(int32_t value)
	{
		___p0n_11 = value;
	}

	inline static int32_t get_offset_of_p1n_12() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p1n_12)); }
	inline int32_t get_p1n_12() const { return ___p1n_12; }
	inline int32_t* get_address_of_p1n_12() { return &___p1n_12; }
	inline void set_p1n_12(int32_t value)
	{
		___p1n_12 = value;
	}

	inline static int32_t get_offset_of_p2n_13() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p2n_13)); }
	inline int32_t get_p2n_13() const { return ___p2n_13; }
	inline int32_t* get_address_of_p2n_13() { return &___p2n_13; }
	inline void set_p2n_13(int32_t value)
	{
		___p2n_13 = value;
	}

	inline static int32_t get_offset_of_p3n_14() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p3n_14)); }
	inline int32_t get_p3n_14() const { return ___p3n_14; }
	inline int32_t* get_address_of_p3n_14() { return &___p3n_14; }
	inline void set_p3n_14(int32_t value)
	{
		___p3n_14 = value;
	}

	inline static int32_t get_offset_of_i_15() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___i_15)); }
	inline float get_i_15() const { return ___i_15; }
	inline float* get_address_of_i_15() { return &___i_15; }
	inline void set_i_15(float value)
	{
		___i_15 = value;
	}

	inline static int32_t get_offset_of_P0_16() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P0_16)); }
	inline Vector3_t3722313464  get_P0_16() const { return ___P0_16; }
	inline Vector3_t3722313464 * get_address_of_P0_16() { return &___P0_16; }
	inline void set_P0_16(Vector3_t3722313464  value)
	{
		___P0_16 = value;
	}

	inline static int32_t get_offset_of_P1_17() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P1_17)); }
	inline Vector3_t3722313464  get_P1_17() const { return ___P1_17; }
	inline Vector3_t3722313464 * get_address_of_P1_17() { return &___P1_17; }
	inline void set_P1_17(Vector3_t3722313464  value)
	{
		___P1_17 = value;
	}

	inline static int32_t get_offset_of_P2_18() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P2_18)); }
	inline Vector3_t3722313464  get_P2_18() const { return ___P2_18; }
	inline Vector3_t3722313464 * get_address_of_P2_18() { return &___P2_18; }
	inline void set_P2_18(Vector3_t3722313464  value)
	{
		___P2_18 = value;
	}

	inline static int32_t get_offset_of_P3_19() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P3_19)); }
	inline Vector3_t3722313464  get_P3_19() const { return ___P3_19; }
	inline Vector3_t3722313464 * get_address_of_P3_19() { return &___P3_19; }
	inline void set_P3_19(Vector3_t3722313464  value)
	{
		___P3_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTCIRCUIT_T445075330_H
#ifndef WAYPOINTPROGRESSTRACKER_T1841386251_H
#define WAYPOINTPROGRESSTRACKER_T1841386251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker
struct  WaypointProgressTracker_t1841386251  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointProgressTracker::circuit
	WaypointCircuit_t445075330 * ___circuit_4;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetOffset
	float ___lookAheadForTargetOffset_5;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetFactor
	float ___lookAheadForTargetFactor_6;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedOffset
	float ___lookAheadForSpeedOffset_7;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedFactor
	float ___lookAheadForSpeedFactor_8;
	// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle UnityStandardAssets.Utility.WaypointProgressTracker::progressStyle
	int32_t ___progressStyle_9;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::pointToPointThreshold
	float ___pointToPointThreshold_10;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<targetPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CtargetPointU3Ek__BackingField_11;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<speedPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CspeedPointU3Ek__BackingField_12;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<progressPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CprogressPointU3Ek__BackingField_13;
	// UnityEngine.Transform UnityStandardAssets.Utility.WaypointProgressTracker::target
	Transform_t3600365921 * ___target_14;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::progressDistance
	float ___progressDistance_15;
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker::progressNum
	int32_t ___progressNum_16;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointProgressTracker::lastPosition
	Vector3_t3722313464  ___lastPosition_17;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::speed
	float ___speed_18;

public:
	inline static int32_t get_offset_of_circuit_4() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___circuit_4)); }
	inline WaypointCircuit_t445075330 * get_circuit_4() const { return ___circuit_4; }
	inline WaypointCircuit_t445075330 ** get_address_of_circuit_4() { return &___circuit_4; }
	inline void set_circuit_4(WaypointCircuit_t445075330 * value)
	{
		___circuit_4 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_4), value);
	}

	inline static int32_t get_offset_of_lookAheadForTargetOffset_5() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForTargetOffset_5)); }
	inline float get_lookAheadForTargetOffset_5() const { return ___lookAheadForTargetOffset_5; }
	inline float* get_address_of_lookAheadForTargetOffset_5() { return &___lookAheadForTargetOffset_5; }
	inline void set_lookAheadForTargetOffset_5(float value)
	{
		___lookAheadForTargetOffset_5 = value;
	}

	inline static int32_t get_offset_of_lookAheadForTargetFactor_6() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForTargetFactor_6)); }
	inline float get_lookAheadForTargetFactor_6() const { return ___lookAheadForTargetFactor_6; }
	inline float* get_address_of_lookAheadForTargetFactor_6() { return &___lookAheadForTargetFactor_6; }
	inline void set_lookAheadForTargetFactor_6(float value)
	{
		___lookAheadForTargetFactor_6 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedOffset_7() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForSpeedOffset_7)); }
	inline float get_lookAheadForSpeedOffset_7() const { return ___lookAheadForSpeedOffset_7; }
	inline float* get_address_of_lookAheadForSpeedOffset_7() { return &___lookAheadForSpeedOffset_7; }
	inline void set_lookAheadForSpeedOffset_7(float value)
	{
		___lookAheadForSpeedOffset_7 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedFactor_8() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForSpeedFactor_8)); }
	inline float get_lookAheadForSpeedFactor_8() const { return ___lookAheadForSpeedFactor_8; }
	inline float* get_address_of_lookAheadForSpeedFactor_8() { return &___lookAheadForSpeedFactor_8; }
	inline void set_lookAheadForSpeedFactor_8(float value)
	{
		___lookAheadForSpeedFactor_8 = value;
	}

	inline static int32_t get_offset_of_progressStyle_9() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressStyle_9)); }
	inline int32_t get_progressStyle_9() const { return ___progressStyle_9; }
	inline int32_t* get_address_of_progressStyle_9() { return &___progressStyle_9; }
	inline void set_progressStyle_9(int32_t value)
	{
		___progressStyle_9 = value;
	}

	inline static int32_t get_offset_of_pointToPointThreshold_10() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___pointToPointThreshold_10)); }
	inline float get_pointToPointThreshold_10() const { return ___pointToPointThreshold_10; }
	inline float* get_address_of_pointToPointThreshold_10() { return &___pointToPointThreshold_10; }
	inline void set_pointToPointThreshold_10(float value)
	{
		___pointToPointThreshold_10 = value;
	}

	inline static int32_t get_offset_of_U3CtargetPointU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CtargetPointU3Ek__BackingField_11)); }
	inline RoutePoint_t3880028948  get_U3CtargetPointU3Ek__BackingField_11() const { return ___U3CtargetPointU3Ek__BackingField_11; }
	inline RoutePoint_t3880028948 * get_address_of_U3CtargetPointU3Ek__BackingField_11() { return &___U3CtargetPointU3Ek__BackingField_11; }
	inline void set_U3CtargetPointU3Ek__BackingField_11(RoutePoint_t3880028948  value)
	{
		___U3CtargetPointU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CspeedPointU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CspeedPointU3Ek__BackingField_12)); }
	inline RoutePoint_t3880028948  get_U3CspeedPointU3Ek__BackingField_12() const { return ___U3CspeedPointU3Ek__BackingField_12; }
	inline RoutePoint_t3880028948 * get_address_of_U3CspeedPointU3Ek__BackingField_12() { return &___U3CspeedPointU3Ek__BackingField_12; }
	inline void set_U3CspeedPointU3Ek__BackingField_12(RoutePoint_t3880028948  value)
	{
		___U3CspeedPointU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CprogressPointU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CprogressPointU3Ek__BackingField_13)); }
	inline RoutePoint_t3880028948  get_U3CprogressPointU3Ek__BackingField_13() const { return ___U3CprogressPointU3Ek__BackingField_13; }
	inline RoutePoint_t3880028948 * get_address_of_U3CprogressPointU3Ek__BackingField_13() { return &___U3CprogressPointU3Ek__BackingField_13; }
	inline void set_U3CprogressPointU3Ek__BackingField_13(RoutePoint_t3880028948  value)
	{
		___U3CprogressPointU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_target_14() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___target_14)); }
	inline Transform_t3600365921 * get_target_14() const { return ___target_14; }
	inline Transform_t3600365921 ** get_address_of_target_14() { return &___target_14; }
	inline void set_target_14(Transform_t3600365921 * value)
	{
		___target_14 = value;
		Il2CppCodeGenWriteBarrier((&___target_14), value);
	}

	inline static int32_t get_offset_of_progressDistance_15() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressDistance_15)); }
	inline float get_progressDistance_15() const { return ___progressDistance_15; }
	inline float* get_address_of_progressDistance_15() { return &___progressDistance_15; }
	inline void set_progressDistance_15(float value)
	{
		___progressDistance_15 = value;
	}

	inline static int32_t get_offset_of_progressNum_16() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressNum_16)); }
	inline int32_t get_progressNum_16() const { return ___progressNum_16; }
	inline int32_t* get_address_of_progressNum_16() { return &___progressNum_16; }
	inline void set_progressNum_16(int32_t value)
	{
		___progressNum_16 = value;
	}

	inline static int32_t get_offset_of_lastPosition_17() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lastPosition_17)); }
	inline Vector3_t3722313464  get_lastPosition_17() const { return ___lastPosition_17; }
	inline Vector3_t3722313464 * get_address_of_lastPosition_17() { return &___lastPosition_17; }
	inline void set_lastPosition_17(Vector3_t3722313464  value)
	{
		___lastPosition_17 = value;
	}

	inline static int32_t get_offset_of_speed_18() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___speed_18)); }
	inline float get_speed_18() const { return ___speed_18; }
	inline float* get_address_of_speed_18() { return &___speed_18; }
	inline void set_speed_18(float value)
	{
		___speed_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTPROGRESSTRACKER_T1841386251_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4400 = { sizeof (LogWheelsSettings_t1545220311)+ sizeof (RuntimeObject), sizeof(LogWheelsSettings_t1545220311 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4400[3] = 
{
	LogWheelsSettings_t1545220311::get_offset_of_slope_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogWheelsSettings_t1545220311::get_offset_of_power_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogWheelsSettings_t1545220311::get_offset_of_offset_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4401 = { sizeof (LinearWheelsSettings_t3897781309)+ sizeof (RuntimeObject), sizeof(LinearWheelsSettings_t3897781309 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4401[3] = 
{
	LinearWheelsSettings_t3897781309::get_offset_of_lift_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LinearWheelsSettings_t3897781309::get_offset_of_gamma_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LinearWheelsSettings_t3897781309::get_offset_of_gain_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4402 = { sizeof (ColorWheelMode_t1939415375)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4402[3] = 
{
	ColorWheelMode_t1939415375::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4403 = { sizeof (ColorWheelsSettings_t3120867866)+ sizeof (RuntimeObject), sizeof(ColorWheelsSettings_t3120867866 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4403[3] = 
{
	ColorWheelsSettings_t3120867866::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorWheelsSettings_t3120867866::get_offset_of_log_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorWheelsSettings_t3120867866::get_offset_of_linear_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4404 = { sizeof (CurvesSettings_t2830270037)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4404[13] = 
{
	CurvesSettings_t2830270037::get_offset_of_master_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_red_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_green_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_blue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_hueVShue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_hueVSsat_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_satVSsat_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_lumVSsat_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_e_CurrentEditingCurve_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_e_CurveY_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_e_CurveR_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_e_CurveG_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_e_CurveB_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4405 = { sizeof (Settings_t451872061)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4405[5] = 
{
	Settings_t451872061::get_offset_of_tonemapping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t451872061::get_offset_of_basic_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t451872061::get_offset_of_channelMixer_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t451872061::get_offset_of_colorWheels_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t451872061::get_offset_of_curves_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4406 = { sizeof (DepthOfFieldModel_t514067330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4406[1] = 
{
	DepthOfFieldModel_t514067330::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4407 = { sizeof (KernelSize_t2406218613)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4407[5] = 
{
	KernelSize_t2406218613::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4408 = { sizeof (Settings_t2195468135)+ sizeof (RuntimeObject), sizeof(Settings_t2195468135_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4408[5] = 
{
	Settings_t2195468135::get_offset_of_focusDistance_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2195468135::get_offset_of_aperture_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2195468135::get_offset_of_focalLength_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2195468135::get_offset_of_useCameraFov_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2195468135::get_offset_of_kernelSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4409 = { sizeof (DitheringModel_t2429005396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4409[1] = 
{
	DitheringModel_t2429005396::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4410 = { sizeof (Settings_t2313396630)+ sizeof (RuntimeObject), sizeof(Settings_t2313396630 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4411 = { sizeof (EyeAdaptationModel_t242823912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4411[1] = 
{
	EyeAdaptationModel_t242823912::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4412 = { sizeof (EyeAdaptationType_t3053468307)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4412[3] = 
{
	EyeAdaptationType_t3053468307::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4413 = { sizeof (Settings_t2874244444)+ sizeof (RuntimeObject), sizeof(Settings_t2874244444_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4413[11] = 
{
	Settings_t2874244444::get_offset_of_lowPercent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_highPercent_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_minLuminance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_maxLuminance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_keyValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_dynamicKeyValue_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_adaptationType_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_speedUp_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_speedDown_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_logMin_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_logMax_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4414 = { sizeof (FogModel_t3620688749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4414[1] = 
{
	FogModel_t3620688749::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4415 = { sizeof (Settings_t224798599)+ sizeof (RuntimeObject), sizeof(Settings_t224798599_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4415[1] = 
{
	Settings_t224798599::get_offset_of_excludeSkybox_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4416 = { sizeof (GrainModel_t1152882488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4416[1] = 
{
	GrainModel_t1152882488::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4417 = { sizeof (Settings_t4123292438)+ sizeof (RuntimeObject), sizeof(Settings_t4123292438_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4417[4] = 
{
	Settings_t4123292438::get_offset_of_colored_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4123292438::get_offset_of_intensity_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4123292438::get_offset_of_size_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4123292438::get_offset_of_luminanceContribution_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4418 = { sizeof (MotionBlurModel_t3080286123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4418[1] = 
{
	MotionBlurModel_t3080286123::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4419 = { sizeof (Settings_t4282162361)+ sizeof (RuntimeObject), sizeof(Settings_t4282162361 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4419[3] = 
{
	Settings_t4282162361::get_offset_of_shutterAngle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4282162361::get_offset_of_sampleCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4282162361::get_offset_of_frameBlending_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4420 = { sizeof (ScreenSpaceReflectionModel_t3026344732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4420[1] = 
{
	ScreenSpaceReflectionModel_t3026344732::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4421 = { sizeof (SSRResolution_t161222554)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4421[3] = 
{
	SSRResolution_t161222554::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4422 = { sizeof (SSRReflectionBlendType_t3026770880)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4422[3] = 
{
	SSRReflectionBlendType_t3026770880::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4423 = { sizeof (IntensitySettings_t1721872184)+ sizeof (RuntimeObject), sizeof(IntensitySettings_t1721872184 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4423[4] = 
{
	IntensitySettings_t1721872184::get_offset_of_reflectionMultiplier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntensitySettings_t1721872184::get_offset_of_fadeDistance_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntensitySettings_t1721872184::get_offset_of_fresnelFade_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntensitySettings_t1721872184::get_offset_of_fresnelFadePower_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4424 = { sizeof (ReflectionSettings_t282755190)+ sizeof (RuntimeObject), sizeof(ReflectionSettings_t282755190_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4424[8] = 
{
	ReflectionSettings_t282755190::get_offset_of_blendType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_reflectionQuality_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_maxDistance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_iterationCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_stepSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_widthModifier_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_reflectionBlur_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_reflectBackfaces_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4425 = { sizeof (ScreenEdgeMask_t4063288584)+ sizeof (RuntimeObject), sizeof(ScreenEdgeMask_t4063288584 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4425[1] = 
{
	ScreenEdgeMask_t4063288584::get_offset_of_intensity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4426 = { sizeof (Settings_t1995791524)+ sizeof (RuntimeObject), sizeof(Settings_t1995791524_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4426[3] = 
{
	Settings_t1995791524::get_offset_of_reflection_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1995791524::get_offset_of_intensity_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1995791524::get_offset_of_screenEdgeMask_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4427 = { sizeof (UserLutModel_t1670108080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4427[1] = 
{
	UserLutModel_t1670108080::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4428 = { sizeof (Settings_t3006579223)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4428[2] = 
{
	Settings_t3006579223::get_offset_of_lut_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3006579223::get_offset_of_contribution_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4429 = { sizeof (VignetteModel_t2845517177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4429[1] = 
{
	VignetteModel_t2845517177::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4430 = { sizeof (Mode_t3936508933)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4430[3] = 
{
	Mode_t3936508933::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4431 = { sizeof (Settings_t1354494600)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4431[9] = 
{
	Settings_t1354494600::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_color_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_center_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_intensity_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_smoothness_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_roundness_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_mask_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_opacity_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_rounded_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4432 = { sizeof (PostProcessingBehaviour_t3229946336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4432[29] = 
{
	PostProcessingBehaviour_t3229946336::get_offset_of_profile_4(),
	PostProcessingBehaviour_t3229946336::get_offset_of_jitteredMatrixFunc_5(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_CommandBuffers_6(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Components_7(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ComponentStates_8(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_MaterialFactory_9(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_RenderTextureFactory_10(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Context_11(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Camera_12(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_PreviousProfile_13(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_RenderingInSceneView_14(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_DebugViews_15(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_AmbientOcclusion_16(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ScreenSpaceReflection_17(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_FogComponent_18(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_MotionBlur_19(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Taa_20(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_EyeAdaptation_21(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_DepthOfField_22(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Bloom_23(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ChromaticAberration_24(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ColorGrading_25(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_UserLut_26(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Grain_27(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Vignette_28(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Dithering_29(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Fxaa_30(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ComponentsToEnable_31(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ComponentsToDisable_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4433 = { sizeof (PostProcessingComponentBase_t2731103827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4433[1] = 
{
	PostProcessingComponentBase_t2731103827::get_offset_of_context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4434 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4434[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4435 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4436 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4437 = { sizeof (PostProcessingContext_t2014408948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4437[5] = 
{
	PostProcessingContext_t2014408948::get_offset_of_profile_0(),
	PostProcessingContext_t2014408948::get_offset_of_camera_1(),
	PostProcessingContext_t2014408948::get_offset_of_materialFactory_2(),
	PostProcessingContext_t2014408948::get_offset_of_renderTextureFactory_3(),
	PostProcessingContext_t2014408948::get_offset_of_U3CinterruptedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4438 = { sizeof (PostProcessingModel_t540111976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4438[1] = 
{
	PostProcessingModel_t540111976::get_offset_of_m_Enabled_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4439 = { sizeof (PostProcessingProfile_t724195375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4439[15] = 
{
	PostProcessingProfile_t724195375::get_offset_of_debugViews_4(),
	PostProcessingProfile_t724195375::get_offset_of_fog_5(),
	PostProcessingProfile_t724195375::get_offset_of_antialiasing_6(),
	PostProcessingProfile_t724195375::get_offset_of_ambientOcclusion_7(),
	PostProcessingProfile_t724195375::get_offset_of_screenSpaceReflection_8(),
	PostProcessingProfile_t724195375::get_offset_of_depthOfField_9(),
	PostProcessingProfile_t724195375::get_offset_of_motionBlur_10(),
	PostProcessingProfile_t724195375::get_offset_of_eyeAdaptation_11(),
	PostProcessingProfile_t724195375::get_offset_of_bloom_12(),
	PostProcessingProfile_t724195375::get_offset_of_colorGrading_13(),
	PostProcessingProfile_t724195375::get_offset_of_userLut_14(),
	PostProcessingProfile_t724195375::get_offset_of_chromaticAberration_15(),
	PostProcessingProfile_t724195375::get_offset_of_grain_16(),
	PostProcessingProfile_t724195375::get_offset_of_vignette_17(),
	PostProcessingProfile_t724195375::get_offset_of_dithering_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4440 = { sizeof (ColorGradingCurve_t2000571184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4440[5] = 
{
	ColorGradingCurve_t2000571184::get_offset_of_curve_0(),
	ColorGradingCurve_t2000571184::get_offset_of_m_Loop_1(),
	ColorGradingCurve_t2000571184::get_offset_of_m_ZeroValue_2(),
	ColorGradingCurve_t2000571184::get_offset_of_m_Range_3(),
	ColorGradingCurve_t2000571184::get_offset_of_m_InternalLoopingCurve_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4441 = { sizeof (GraphicsUtils_t2852986763), -1, sizeof(GraphicsUtils_t2852986763_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4441[2] = 
{
	GraphicsUtils_t2852986763_StaticFields::get_offset_of_s_WhiteTexture_0(),
	GraphicsUtils_t2852986763_StaticFields::get_offset_of_s_Quad_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4442 = { sizeof (MaterialFactory_t2445948724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4442[1] = 
{
	MaterialFactory_t2445948724::get_offset_of_m_Materials_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4443 = { sizeof (RenderTextureFactory_t1946967824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4443[1] = 
{
	RenderTextureFactory_t1946967824::get_offset_of_m_TemporaryRTs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4444 = { sizeof (ActivateTrigger_t3349759092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4444[5] = 
{
	ActivateTrigger_t3349759092::get_offset_of_action_4(),
	ActivateTrigger_t3349759092::get_offset_of_target_5(),
	ActivateTrigger_t3349759092::get_offset_of_source_6(),
	ActivateTrigger_t3349759092::get_offset_of_triggerCount_7(),
	ActivateTrigger_t3349759092::get_offset_of_repeatTrigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4445 = { sizeof (Mode_t3024470803)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4445[7] = 
{
	Mode_t3024470803::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4446 = { sizeof (AutoMobileShaderSwitch_t568447889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4446[1] = 
{
	AutoMobileShaderSwitch_t568447889::get_offset_of_m_ReplacementList_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4447 = { sizeof (ReplacementDefinition_t2693741842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4447[2] = 
{
	ReplacementDefinition_t2693741842::get_offset_of_original_0(),
	ReplacementDefinition_t2693741842::get_offset_of_replacement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4448 = { sizeof (ReplacementList_t1887104210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4448[1] = 
{
	ReplacementList_t1887104210::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4449 = { sizeof (AutoMoveAndRotate_t2437913015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4449[4] = 
{
	AutoMoveAndRotate_t2437913015::get_offset_of_moveUnitsPerSecond_4(),
	AutoMoveAndRotate_t2437913015::get_offset_of_rotateDegreesPerSecond_5(),
	AutoMoveAndRotate_t2437913015::get_offset_of_ignoreTimescale_6(),
	AutoMoveAndRotate_t2437913015::get_offset_of_m_LastRealTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4450 = { sizeof (Vector3andSpace_t219844479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4450[2] = 
{
	Vector3andSpace_t219844479::get_offset_of_value_0(),
	Vector3andSpace_t219844479::get_offset_of_space_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4451 = { sizeof (CameraRefocus_t4263235746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4451[5] = 
{
	CameraRefocus_t4263235746::get_offset_of_Camera_0(),
	CameraRefocus_t4263235746::get_offset_of_Lookatpoint_1(),
	CameraRefocus_t4263235746::get_offset_of_Parent_2(),
	CameraRefocus_t4263235746::get_offset_of_m_OrigCameraPos_3(),
	CameraRefocus_t4263235746::get_offset_of_m_Refocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4452 = { sizeof (CurveControlledBob_t2679313829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4452[9] = 
{
	CurveControlledBob_t2679313829::get_offset_of_HorizontalBobRange_0(),
	CurveControlledBob_t2679313829::get_offset_of_VerticalBobRange_1(),
	CurveControlledBob_t2679313829::get_offset_of_Bobcurve_2(),
	CurveControlledBob_t2679313829::get_offset_of_VerticaltoHorizontalRatio_3(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionX_4(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionY_5(),
	CurveControlledBob_t2679313829::get_offset_of_m_BobBaseInterval_6(),
	CurveControlledBob_t2679313829::get_offset_of_m_OriginalCameraPosition_7(),
	CurveControlledBob_t2679313829::get_offset_of_m_Time_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4453 = { sizeof (DragRigidbody_t1600652016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4453[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DragRigidbody_t1600652016::get_offset_of_m_SpringJoint_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4454 = { sizeof (U3CDragObjectU3Ed__8_t4113469309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4454[7] = 
{
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3CU3E1__state_0(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3CU3E2__current_1(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3CU3E4__this_2(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_distance_3(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3ColdDragU3E5__2_4(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3ColdAngularDragU3E5__3_5(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3CmainCameraU3E5__4_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4455 = { sizeof (DynamicShadowSettings_t59119858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4455[11] = 
{
	DynamicShadowSettings_t59119858::get_offset_of_sunLight_4(),
	DynamicShadowSettings_t59119858::get_offset_of_minHeight_5(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowDistance_6(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowBias_7(),
	DynamicShadowSettings_t59119858::get_offset_of_maxHeight_8(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowDistance_9(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowBias_10(),
	DynamicShadowSettings_t59119858::get_offset_of_adaptTime_11(),
	DynamicShadowSettings_t59119858::get_offset_of_m_SmoothHeight_12(),
	DynamicShadowSettings_t59119858::get_offset_of_m_ChangeSpeed_13(),
	DynamicShadowSettings_t59119858::get_offset_of_m_OriginalStrength_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4456 = { sizeof (FollowTarget_t166153614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4456[2] = 
{
	FollowTarget_t166153614::get_offset_of_target_4(),
	FollowTarget_t166153614::get_offset_of_offset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4457 = { sizeof (FOVKick_t120370150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4457[6] = 
{
	FOVKick_t120370150::get_offset_of_Camera_0(),
	FOVKick_t120370150::get_offset_of_originalFov_1(),
	FOVKick_t120370150::get_offset_of_FOVIncrease_2(),
	FOVKick_t120370150::get_offset_of_TimeToIncrease_3(),
	FOVKick_t120370150::get_offset_of_TimeToDecrease_4(),
	FOVKick_t120370150::get_offset_of_IncreaseCurve_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4458 = { sizeof (U3CFOVKickUpU3Ed__9_t1152317412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4458[4] = 
{
	U3CFOVKickUpU3Ed__9_t1152317412::get_offset_of_U3CU3E1__state_0(),
	U3CFOVKickUpU3Ed__9_t1152317412::get_offset_of_U3CU3E2__current_1(),
	U3CFOVKickUpU3Ed__9_t1152317412::get_offset_of_U3CU3E4__this_2(),
	U3CFOVKickUpU3Ed__9_t1152317412::get_offset_of_U3CtU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4459 = { sizeof (U3CFOVKickDownU3Ed__10_t494950160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4459[4] = 
{
	U3CFOVKickDownU3Ed__10_t494950160::get_offset_of_U3CU3E1__state_0(),
	U3CFOVKickDownU3Ed__10_t494950160::get_offset_of_U3CU3E2__current_1(),
	U3CFOVKickDownU3Ed__10_t494950160::get_offset_of_U3CU3E4__this_2(),
	U3CFOVKickDownU3Ed__10_t494950160::get_offset_of_U3CtU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4460 = { sizeof (FPSCounter_t2351221284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4460[6] = 
{
	0,
	FPSCounter_t2351221284::get_offset_of_m_FpsAccumulator_5(),
	FPSCounter_t2351221284::get_offset_of_m_FpsNextPeriod_6(),
	FPSCounter_t2351221284::get_offset_of_m_CurrentFps_7(),
	0,
	FPSCounter_t2351221284::get_offset_of_m_Text_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4461 = { sizeof (LerpControlledBob_t1895875871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4461[3] = 
{
	LerpControlledBob_t1895875871::get_offset_of_BobDuration_0(),
	LerpControlledBob_t1895875871::get_offset_of_BobAmount_1(),
	LerpControlledBob_t1895875871::get_offset_of_m_Offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4462 = { sizeof (U3CDoBobCycleU3Ed__4_t3986606630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4462[4] = 
{
	U3CDoBobCycleU3Ed__4_t3986606630::get_offset_of_U3CU3E1__state_0(),
	U3CDoBobCycleU3Ed__4_t3986606630::get_offset_of_U3CU3E2__current_1(),
	U3CDoBobCycleU3Ed__4_t3986606630::get_offset_of_U3CU3E4__this_2(),
	U3CDoBobCycleU3Ed__4_t3986606630::get_offset_of_U3CtU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4463 = { sizeof (ObjectResetter_t639177103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4463[4] = 
{
	ObjectResetter_t639177103::get_offset_of_originalPosition_4(),
	ObjectResetter_t639177103::get_offset_of_originalRotation_5(),
	ObjectResetter_t639177103::get_offset_of_originalStructure_6(),
	ObjectResetter_t639177103::get_offset_of_Rigidbody_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4464 = { sizeof (U3CResetCoroutineU3Ed__6_t3318545224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4464[4] = 
{
	U3CResetCoroutineU3Ed__6_t3318545224::get_offset_of_U3CU3E1__state_0(),
	U3CResetCoroutineU3Ed__6_t3318545224::get_offset_of_U3CU3E2__current_1(),
	U3CResetCoroutineU3Ed__6_t3318545224::get_offset_of_delay_2(),
	U3CResetCoroutineU3Ed__6_t3318545224::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4465 = { sizeof (ParticleSystemDestroyer_t558680695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4465[4] = 
{
	ParticleSystemDestroyer_t558680695::get_offset_of_minDuration_4(),
	ParticleSystemDestroyer_t558680695::get_offset_of_maxDuration_5(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_MaxLifetime_6(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_EarlyStop_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4466 = { sizeof (U3CStartU3Ed__4_t3571467226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4466[5] = 
{
	U3CStartU3Ed__4_t3571467226::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__4_t3571467226::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__4_t3571467226::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__4_t3571467226::get_offset_of_U3CsystemsU3E5__2_3(),
	U3CStartU3Ed__4_t3571467226::get_offset_of_U3CstopTimeU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4467 = { sizeof (PlatformSpecificContent_t1404549723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4467[4] = 
{
	PlatformSpecificContent_t1404549723::get_offset_of_m_BuildTargetGroup_4(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_Content_5(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_MonoBehaviours_6(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_ChildrenOfThisObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4468 = { sizeof (BuildTargetGroup_t72322187)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4468[3] = 
{
	BuildTargetGroup_t72322187::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4469 = { sizeof (SimpleActivatorMenu_t1387811551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4469[3] = 
{
	SimpleActivatorMenu_t1387811551::get_offset_of_camSwitchButton_4(),
	SimpleActivatorMenu_t1387811551::get_offset_of_objects_5(),
	SimpleActivatorMenu_t1387811551::get_offset_of_m_CurrentActiveObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4470 = { sizeof (SimpleMouseRotator_t2364742953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4470[10] = 
{
	SimpleMouseRotator_t2364742953::get_offset_of_rotationRange_4(),
	SimpleMouseRotator_t2364742953::get_offset_of_rotationSpeed_5(),
	SimpleMouseRotator_t2364742953::get_offset_of_dampingTime_6(),
	SimpleMouseRotator_t2364742953::get_offset_of_autoZeroVerticalOnMobile_7(),
	SimpleMouseRotator_t2364742953::get_offset_of_autoZeroHorizontalOnMobile_8(),
	SimpleMouseRotator_t2364742953::get_offset_of_relative_9(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_TargetAngles_10(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_FollowAngles_11(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_FollowVelocity_12(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_OriginalRotation_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4471 = { sizeof (SmoothFollow_t4204731361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4471[5] = 
{
	SmoothFollow_t4204731361::get_offset_of_target_4(),
	SmoothFollow_t4204731361::get_offset_of_distance_5(),
	SmoothFollow_t4204731361::get_offset_of_height_6(),
	SmoothFollow_t4204731361::get_offset_of_rotationDamping_7(),
	SmoothFollow_t4204731361::get_offset_of_heightDamping_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4472 = { sizeof (TimedObjectActivator_t1846709985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4472[1] = 
{
	TimedObjectActivator_t1846709985::get_offset_of_entries_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4473 = { sizeof (Action_t837364808)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4473[6] = 
{
	Action_t837364808::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4474 = { sizeof (Entry_t2725803170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4474[3] = 
{
	Entry_t2725803170::get_offset_of_target_0(),
	Entry_t2725803170::get_offset_of_action_1(),
	Entry_t2725803170::get_offset_of_delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4475 = { sizeof (Entries_t3168066469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4475[1] = 
{
	Entries_t3168066469::get_offset_of_entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4476 = { sizeof (U3CActivateU3Ed__5_t4263739043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4476[3] = 
{
	U3CActivateU3Ed__5_t4263739043::get_offset_of_U3CU3E1__state_0(),
	U3CActivateU3Ed__5_t4263739043::get_offset_of_U3CU3E2__current_1(),
	U3CActivateU3Ed__5_t4263739043::get_offset_of_entry_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4477 = { sizeof (U3CDeactivateU3Ed__6_t2861800224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4477[3] = 
{
	U3CDeactivateU3Ed__6_t2861800224::get_offset_of_U3CU3E1__state_0(),
	U3CDeactivateU3Ed__6_t2861800224::get_offset_of_U3CU3E2__current_1(),
	U3CDeactivateU3Ed__6_t2861800224::get_offset_of_entry_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4478 = { sizeof (U3CReloadLevelU3Ed__7_t2023328258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4478[3] = 
{
	U3CReloadLevelU3Ed__7_t2023328258::get_offset_of_U3CU3E1__state_0(),
	U3CReloadLevelU3Ed__7_t2023328258::get_offset_of_U3CU3E2__current_1(),
	U3CReloadLevelU3Ed__7_t2023328258::get_offset_of_entry_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4479 = { sizeof (TimedObjectDestructor_t3438860414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4479[2] = 
{
	TimedObjectDestructor_t3438860414::get_offset_of_m_TimeOut_4(),
	TimedObjectDestructor_t3438860414::get_offset_of_m_DetachChildren_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4480 = { sizeof (WaypointCircuit_t445075330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4480[16] = 
{
	WaypointCircuit_t445075330::get_offset_of_waypointList_4(),
	WaypointCircuit_t445075330::get_offset_of_smoothRoute_5(),
	WaypointCircuit_t445075330::get_offset_of_numPoints_6(),
	WaypointCircuit_t445075330::get_offset_of_points_7(),
	WaypointCircuit_t445075330::get_offset_of_distances_8(),
	WaypointCircuit_t445075330::get_offset_of_editorVisualisationSubsteps_9(),
	WaypointCircuit_t445075330::get_offset_of_U3CLengthU3Ek__BackingField_10(),
	WaypointCircuit_t445075330::get_offset_of_p0n_11(),
	WaypointCircuit_t445075330::get_offset_of_p1n_12(),
	WaypointCircuit_t445075330::get_offset_of_p2n_13(),
	WaypointCircuit_t445075330::get_offset_of_p3n_14(),
	WaypointCircuit_t445075330::get_offset_of_i_15(),
	WaypointCircuit_t445075330::get_offset_of_P0_16(),
	WaypointCircuit_t445075330::get_offset_of_P1_17(),
	WaypointCircuit_t445075330::get_offset_of_P2_18(),
	WaypointCircuit_t445075330::get_offset_of_P3_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4481 = { sizeof (WaypointList_t2584574554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4481[2] = 
{
	WaypointList_t2584574554::get_offset_of_circuit_0(),
	WaypointList_t2584574554::get_offset_of_items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4482 = { sizeof (RoutePoint_t3880028948)+ sizeof (RuntimeObject), sizeof(RoutePoint_t3880028948 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4482[2] = 
{
	RoutePoint_t3880028948::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoutePoint_t3880028948::get_offset_of_direction_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4483 = { sizeof (WaypointProgressTracker_t1841386251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4483[15] = 
{
	WaypointProgressTracker_t1841386251::get_offset_of_circuit_4(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForTargetOffset_5(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForTargetFactor_6(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForSpeedOffset_7(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForSpeedFactor_8(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressStyle_9(),
	WaypointProgressTracker_t1841386251::get_offset_of_pointToPointThreshold_10(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CtargetPointU3Ek__BackingField_11(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CspeedPointU3Ek__BackingField_12(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CprogressPointU3Ek__BackingField_13(),
	WaypointProgressTracker_t1841386251::get_offset_of_target_14(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressDistance_15(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressNum_16(),
	WaypointProgressTracker_t1841386251::get_offset_of_lastPosition_17(),
	WaypointProgressTracker_t1841386251::get_offset_of_speed_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4484 = { sizeof (ProgressStyle_t3254572979)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4484[3] = 
{
	ProgressStyle_t3254572979::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4485 = { sizeof (ParticleSceneControls_t306931203), -1, sizeof(ParticleSceneControls_t306931203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4485[18] = 
{
	ParticleSceneControls_t306931203::get_offset_of_demoParticles_4(),
	ParticleSceneControls_t306931203::get_offset_of_spawnOffset_5(),
	ParticleSceneControls_t306931203::get_offset_of_multiply_6(),
	ParticleSceneControls_t306931203::get_offset_of_clearOnChange_7(),
	ParticleSceneControls_t306931203::get_offset_of_titleText_8(),
	ParticleSceneControls_t306931203::get_offset_of_sceneCamera_9(),
	ParticleSceneControls_t306931203::get_offset_of_instructionText_10(),
	ParticleSceneControls_t306931203::get_offset_of_previousButton_11(),
	ParticleSceneControls_t306931203::get_offset_of_nextButton_12(),
	ParticleSceneControls_t306931203::get_offset_of_graphicRaycaster_13(),
	ParticleSceneControls_t306931203::get_offset_of_eventSystem_14(),
	ParticleSceneControls_t306931203::get_offset_of_m_ParticleMultiplier_15(),
	ParticleSceneControls_t306931203::get_offset_of_m_CurrentParticleList_16(),
	ParticleSceneControls_t306931203::get_offset_of_m_Instance_17(),
	ParticleSceneControls_t306931203_StaticFields::get_offset_of_s_SelectedIndex_18(),
	ParticleSceneControls_t306931203::get_offset_of_m_CamOffsetVelocity_19(),
	ParticleSceneControls_t306931203::get_offset_of_m_LastPos_20(),
	ParticleSceneControls_t306931203_StaticFields::get_offset_of_s_Selected_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4486 = { sizeof (Mode_t2106834709)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4486[4] = 
{
	Mode_t2106834709::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4487 = { sizeof (AlignMode_t432429434)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4487[3] = 
{
	AlignMode_t432429434::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4488 = { sizeof (DemoParticleSystem_t1195446716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4488[7] = 
{
	DemoParticleSystem_t1195446716::get_offset_of_transform_0(),
	DemoParticleSystem_t1195446716::get_offset_of_mode_1(),
	DemoParticleSystem_t1195446716::get_offset_of_align_2(),
	DemoParticleSystem_t1195446716::get_offset_of_maxCount_3(),
	DemoParticleSystem_t1195446716::get_offset_of_minDist_4(),
	DemoParticleSystem_t1195446716::get_offset_of_camOffset_5(),
	DemoParticleSystem_t1195446716::get_offset_of_instructionText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4489 = { sizeof (DemoParticleSystemList_t4288938153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4489[1] = 
{
	DemoParticleSystemList_t4288938153::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4490 = { sizeof (PlaceTargetWithMouse_t3823755269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4490[2] = 
{
	PlaceTargetWithMouse_t3823755269::get_offset_of_surfaceOffset_4(),
	PlaceTargetWithMouse_t3823755269::get_offset_of_setTargetOn_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4491 = { sizeof (SlowMoButton_t57138989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4491[6] = 
{
	SlowMoButton_t57138989::get_offset_of_FullSpeedTex_4(),
	SlowMoButton_t57138989::get_offset_of_SlowSpeedTex_5(),
	SlowMoButton_t57138989::get_offset_of_fullSpeed_6(),
	SlowMoButton_t57138989::get_offset_of_slowSpeed_7(),
	SlowMoButton_t57138989::get_offset_of_button_8(),
	SlowMoButton_t57138989::get_offset_of_m_SlowMo_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4492 = { sizeof (AfterburnerPhysicsForce_t498893161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4492[6] = 
{
	AfterburnerPhysicsForce_t498893161::get_offset_of_effectAngle_4(),
	AfterburnerPhysicsForce_t498893161::get_offset_of_effectWidth_5(),
	AfterburnerPhysicsForce_t498893161::get_offset_of_effectDistance_6(),
	AfterburnerPhysicsForce_t498893161::get_offset_of_force_7(),
	AfterburnerPhysicsForce_t498893161::get_offset_of_m_Cols_8(),
	AfterburnerPhysicsForce_t498893161::get_offset_of_m_Sphere_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4493 = { sizeof (ExplosionFireAndDebris_t2411343565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4493[4] = 
{
	ExplosionFireAndDebris_t2411343565::get_offset_of_debrisPrefabs_4(),
	ExplosionFireAndDebris_t2411343565::get_offset_of_firePrefab_5(),
	ExplosionFireAndDebris_t2411343565::get_offset_of_numDebrisPieces_6(),
	ExplosionFireAndDebris_t2411343565::get_offset_of_numFires_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4494 = { sizeof (U3CStartU3Ed__4_t3367237624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4494[4] = 
{
	U3CStartU3Ed__4_t3367237624::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__4_t3367237624::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__4_t3367237624::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__4_t3367237624::get_offset_of_U3CmultiplierU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4495 = { sizeof (ExplosionPhysicsForce_t3982641844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4495[1] = 
{
	ExplosionPhysicsForce_t3982641844::get_offset_of_explosionForce_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4496 = { sizeof (U3CStartU3Ed__1_t3900213119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4496[3] = 
{
	U3CStartU3Ed__1_t3900213119::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__1_t3900213119::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__1_t3900213119::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4497 = { sizeof (Explosive_t792321375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4497[7] = 
{
	Explosive_t792321375::get_offset_of_explosionPrefab_4(),
	Explosive_t792321375::get_offset_of_detonationImpactVelocity_5(),
	Explosive_t792321375::get_offset_of_sizeMultiplier_6(),
	Explosive_t792321375::get_offset_of_reset_7(),
	Explosive_t792321375::get_offset_of_resetTimeDelay_8(),
	Explosive_t792321375::get_offset_of_m_Exploded_9(),
	Explosive_t792321375::get_offset_of_m_ObjectResetter_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4498 = { sizeof (U3COnCollisionEnterU3Ed__8_t3187962944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4498[4] = 
{
	U3COnCollisionEnterU3Ed__8_t3187962944::get_offset_of_U3CU3E1__state_0(),
	U3COnCollisionEnterU3Ed__8_t3187962944::get_offset_of_U3CU3E2__current_1(),
	U3COnCollisionEnterU3Ed__8_t3187962944::get_offset_of_U3CU3E4__this_2(),
	U3COnCollisionEnterU3Ed__8_t3187962944::get_offset_of_col_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4499 = { sizeof (ExtinguishableParticleSystem_t4259708998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4499[2] = 
{
	ExtinguishableParticleSystem_t4259708998::get_offset_of_multiplier_4(),
	ExtinguishableParticleSystem_t4259708998::get_offset_of_m_Systems_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
