﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AICharacterControl
struct AICharacterControl_t2137571320;
// Appear
struct Appear_t3088471322;
// AssemblyCSharp.PixelBubble
struct PixelBubble_t3317741694;
// DestroySelf
struct DestroySelf_t3777301674;
// EnemyHealth
struct EnemyHealth_t797421206;
// ImportedAnimations
struct ImportedAnimations_t3877126586;
// PlayerHealth
struct PlayerHealth_t2068385516;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Dictionary_2_t3872604895;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
struct Dictionary_2_t2541822629;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>
struct Dictionary_2_t310742658;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>
struct Dictionary_2_t75641268;
// System.Collections.Generic.List`1<AssemblyCSharp.PixelBubble>
struct List_1_t494849140;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.AnimationClip>
struct List_1_t3790580729;
// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct List_1_t1232140387;
// System.Collections.Generic.List`1<UnityEngine.SpriteRenderer>
struct List_1_t412733603;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1276799816;
// UnityEngine.AnimationClip
struct AnimationClip_t2318505987;
// UnityEngine.AnimationClip[]
struct AnimationClipU5BU5D_t1636626578;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AnimatorOverrideController
struct AnimatorOverrideController_t3582062219;
// UnityEngine.Animator[]
struct AnimatorU5BU5D_t608880338;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t197597763;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t3972987605;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t3089334924;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer
struct RayHitComparer_t2205555946;
// UnityStandardAssets.Characters.FirstPerson.MouseLook
struct MouseLook_t2859678661;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController
struct RigidbodyFirstPersonController_t1207297146;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings
struct AdvancedSettings_t778418834;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings
struct MovementSettings_t1096092444;
// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct ThirdPersonCharacter_t1711070432;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_t4087348596;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct AxisMapping_t3982445645;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_t2597455733;
// UnityStandardAssets.Utility.CurveControlledBob
struct CurveControlledBob_t2679313829;
// UnityStandardAssets.Utility.FOVKick
struct FOVKick_t120370150;
// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t1895875871;
// UnityStandardAssets.Vehicles.Ball.Ball
struct Ball_t2378314638;
// UnityStandardAssets.Water.PlanarReflection
struct PlanarReflection_t439636033;
// UnityStandardAssets.Water.WaterBase
struct WaterBase_t3883863317;
// UnityStandardAssets._2D.PlatformerCharacter2D
struct PlatformerCharacter2D_t675295753;




#ifndef U3CMODULEU3E_T692745566_H
#define U3CMODULEU3E_T692745566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745566 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745566_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSOUNDDELAYU3ED__33_T301855376_H
#define U3CSOUNDDELAYU3ED__33_T301855376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AICharacterControl/<SoundDelay>d__33
struct  U3CSoundDelayU3Ed__33_t301855376  : public RuntimeObject
{
public:
	// System.Int32 AICharacterControl/<SoundDelay>d__33::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AICharacterControl/<SoundDelay>d__33::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AICharacterControl AICharacterControl/<SoundDelay>d__33::<>4__this
	AICharacterControl_t2137571320 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__33_t301855376, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__33_t301855376, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__33_t301855376, ___U3CU3E4__this_2)); }
	inline AICharacterControl_t2137571320 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AICharacterControl_t2137571320 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AICharacterControl_t2137571320 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSOUNDDELAYU3ED__33_T301855376_H
#ifndef U3CWAITINSECONDSU3ED__7_T2879902402_H
#define U3CWAITINSECONDSU3ED__7_T2879902402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Appear/<WaitInSeconds>d__7
struct  U3CWaitInSecondsU3Ed__7_t2879902402  : public RuntimeObject
{
public:
	// System.Int32 Appear/<WaitInSeconds>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Appear/<WaitInSeconds>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Appear/<WaitInSeconds>d__7::vseconds
	float ___vseconds_2;
	// System.String Appear/<WaitInSeconds>d__7::vChoice
	String_t* ___vChoice_3;
	// Appear Appear/<WaitInSeconds>d__7::<>4__this
	Appear_t3088471322 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitInSecondsU3Ed__7_t2879902402, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitInSecondsU3Ed__7_t2879902402, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_vseconds_2() { return static_cast<int32_t>(offsetof(U3CWaitInSecondsU3Ed__7_t2879902402, ___vseconds_2)); }
	inline float get_vseconds_2() const { return ___vseconds_2; }
	inline float* get_address_of_vseconds_2() { return &___vseconds_2; }
	inline void set_vseconds_2(float value)
	{
		___vseconds_2 = value;
	}

	inline static int32_t get_offset_of_vChoice_3() { return static_cast<int32_t>(offsetof(U3CWaitInSecondsU3Ed__7_t2879902402, ___vChoice_3)); }
	inline String_t* get_vChoice_3() const { return ___vChoice_3; }
	inline String_t** get_address_of_vChoice_3() { return &___vChoice_3; }
	inline void set_vChoice_3(String_t* value)
	{
		___vChoice_3 = value;
		Il2CppCodeGenWriteBarrier((&___vChoice_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CWaitInSecondsU3Ed__7_t2879902402, ___U3CU3E4__this_4)); }
	inline Appear_t3088471322 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline Appear_t3088471322 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(Appear_t3088471322 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITINSECONDSU3ED__7_T2879902402_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef RAYHITCOMPARER_T2205555946_H
#define RAYHITCOMPARER_T2205555946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer
struct  RayHitComparer_t2205555946  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYHITCOMPARER_T2205555946_H
#ifndef ADVANCEDSETTINGS_T778418834_H
#define ADVANCEDSETTINGS_T778418834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings
struct  AdvancedSettings_t778418834  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::groundCheckDistance
	float ___groundCheckDistance_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::stickToGroundHelperDistance
	float ___stickToGroundHelperDistance_1;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::slowDownRate
	float ___slowDownRate_2;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::airControl
	bool ___airControl_3;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::shellOffset
	float ___shellOffset_4;

public:
	inline static int32_t get_offset_of_groundCheckDistance_0() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___groundCheckDistance_0)); }
	inline float get_groundCheckDistance_0() const { return ___groundCheckDistance_0; }
	inline float* get_address_of_groundCheckDistance_0() { return &___groundCheckDistance_0; }
	inline void set_groundCheckDistance_0(float value)
	{
		___groundCheckDistance_0 = value;
	}

	inline static int32_t get_offset_of_stickToGroundHelperDistance_1() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___stickToGroundHelperDistance_1)); }
	inline float get_stickToGroundHelperDistance_1() const { return ___stickToGroundHelperDistance_1; }
	inline float* get_address_of_stickToGroundHelperDistance_1() { return &___stickToGroundHelperDistance_1; }
	inline void set_stickToGroundHelperDistance_1(float value)
	{
		___stickToGroundHelperDistance_1 = value;
	}

	inline static int32_t get_offset_of_slowDownRate_2() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___slowDownRate_2)); }
	inline float get_slowDownRate_2() const { return ___slowDownRate_2; }
	inline float* get_address_of_slowDownRate_2() { return &___slowDownRate_2; }
	inline void set_slowDownRate_2(float value)
	{
		___slowDownRate_2 = value;
	}

	inline static int32_t get_offset_of_airControl_3() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___airControl_3)); }
	inline bool get_airControl_3() const { return ___airControl_3; }
	inline bool* get_address_of_airControl_3() { return &___airControl_3; }
	inline void set_airControl_3(bool value)
	{
		___airControl_3 = value;
	}

	inline static int32_t get_offset_of_shellOffset_4() { return static_cast<int32_t>(offsetof(AdvancedSettings_t778418834, ___shellOffset_4)); }
	inline float get_shellOffset_4() const { return ___shellOffset_4; }
	inline float* get_address_of_shellOffset_4() { return &___shellOffset_4; }
	inline void set_shellOffset_4(float value)
	{
		___shellOffset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDSETTINGS_T778418834_H
#ifndef CROSSPLATFORMINPUTMANAGER_T191731427_H
#define CROSSPLATFORMINPUTMANAGER_T191731427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
struct  CrossPlatformInputManager_t191731427  : public RuntimeObject
{
public:

public:
};

struct CrossPlatformInputManager_t191731427_StaticFields
{
public:
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::activeInput
	VirtualInput_t2597455733 * ___activeInput_0;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_TouchInput
	VirtualInput_t2597455733 * ___s_TouchInput_1;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_HardwareInput
	VirtualInput_t2597455733 * ___s_HardwareInput_2;

public:
	inline static int32_t get_offset_of_activeInput_0() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___activeInput_0)); }
	inline VirtualInput_t2597455733 * get_activeInput_0() const { return ___activeInput_0; }
	inline VirtualInput_t2597455733 ** get_address_of_activeInput_0() { return &___activeInput_0; }
	inline void set_activeInput_0(VirtualInput_t2597455733 * value)
	{
		___activeInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeInput_0), value);
	}

	inline static int32_t get_offset_of_s_TouchInput_1() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___s_TouchInput_1)); }
	inline VirtualInput_t2597455733 * get_s_TouchInput_1() const { return ___s_TouchInput_1; }
	inline VirtualInput_t2597455733 ** get_address_of_s_TouchInput_1() { return &___s_TouchInput_1; }
	inline void set_s_TouchInput_1(VirtualInput_t2597455733 * value)
	{
		___s_TouchInput_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TouchInput_1), value);
	}

	inline static int32_t get_offset_of_s_HardwareInput_2() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___s_HardwareInput_2)); }
	inline VirtualInput_t2597455733 * get_s_HardwareInput_2() const { return ___s_HardwareInput_2; }
	inline VirtualInput_t2597455733 ** get_address_of_s_HardwareInput_2() { return &___s_HardwareInput_2; }
	inline void set_s_HardwareInput_2(VirtualInput_t2597455733 * value)
	{
		___s_HardwareInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_HardwareInput_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMINPUTMANAGER_T191731427_H
#ifndef VIRTUALAXIS_T4087348596_H
#define VIRTUALAXIS_T4087348596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct  VirtualAxis_t4087348596  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::m_Value
	float ___m_Value_1;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___U3CmatchWithInputManagerU3Ek__BackingField_2)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_2() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return &___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_2(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALAXIS_T4087348596_H
#ifndef VIRTUALBUTTON_T2756566330_H
#define VIRTUALBUTTON_T2756566330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
struct  VirtualButton_t2756566330  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_1;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_LastPressedFrame
	int32_t ___m_LastPressedFrame_2;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_ReleasedFrame
	int32_t ___m_ReleasedFrame_3;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_Pressed
	bool ___m_Pressed_4;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___U3CmatchWithInputManagerU3Ek__BackingField_1)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_1() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return &___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_1(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_LastPressedFrame_2() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_LastPressedFrame_2)); }
	inline int32_t get_m_LastPressedFrame_2() const { return ___m_LastPressedFrame_2; }
	inline int32_t* get_address_of_m_LastPressedFrame_2() { return &___m_LastPressedFrame_2; }
	inline void set_m_LastPressedFrame_2(int32_t value)
	{
		___m_LastPressedFrame_2 = value;
	}

	inline static int32_t get_offset_of_m_ReleasedFrame_3() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_ReleasedFrame_3)); }
	inline int32_t get_m_ReleasedFrame_3() const { return ___m_ReleasedFrame_3; }
	inline int32_t* get_address_of_m_ReleasedFrame_3() { return &___m_ReleasedFrame_3; }
	inline void set_m_ReleasedFrame_3(int32_t value)
	{
		___m_ReleasedFrame_3 = value;
	}

	inline static int32_t get_offset_of_m_Pressed_4() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_Pressed_4)); }
	inline bool get_m_Pressed_4() const { return ___m_Pressed_4; }
	inline bool* get_address_of_m_Pressed_4() { return &___m_Pressed_4; }
	inline void set_m_Pressed_4(bool value)
	{
		___m_Pressed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T2756566330_H
#ifndef IMAGEEFFECTS_T1214077586_H
#define IMAGEEFFECTS_T1214077586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ImageEffects
struct  ImageEffects_t1214077586  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTS_T1214077586_H
#ifndef QUADS_T1152577304_H
#define QUADS_T1152577304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Quads
struct  Quads_t1152577304  : public RuntimeObject
{
public:

public:
};

struct Quads_t1152577304_StaticFields
{
public:
	// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Quads::meshes
	MeshU5BU5D_t3972987605* ___meshes_0;
	// System.Int32 UnityStandardAssets.ImageEffects.Quads::currentQuads
	int32_t ___currentQuads_1;

public:
	inline static int32_t get_offset_of_meshes_0() { return static_cast<int32_t>(offsetof(Quads_t1152577304_StaticFields, ___meshes_0)); }
	inline MeshU5BU5D_t3972987605* get_meshes_0() const { return ___meshes_0; }
	inline MeshU5BU5D_t3972987605** get_address_of_meshes_0() { return &___meshes_0; }
	inline void set_meshes_0(MeshU5BU5D_t3972987605* value)
	{
		___meshes_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_0), value);
	}

	inline static int32_t get_offset_of_currentQuads_1() { return static_cast<int32_t>(offsetof(Quads_t1152577304_StaticFields, ___currentQuads_1)); }
	inline int32_t get_currentQuads_1() const { return ___currentQuads_1; }
	inline int32_t* get_address_of_currentQuads_1() { return &___currentQuads_1; }
	inline void set_currentQuads_1(int32_t value)
	{
		___currentQuads_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUADS_T1152577304_H
#ifndef TRIANGLES_T2090031681_H
#define TRIANGLES_T2090031681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Triangles
struct  Triangles_t2090031681  : public RuntimeObject
{
public:

public:
};

struct Triangles_t2090031681_StaticFields
{
public:
	// UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Triangles::meshes
	MeshU5BU5D_t3972987605* ___meshes_0;
	// System.Int32 UnityStandardAssets.ImageEffects.Triangles::currentTris
	int32_t ___currentTris_1;

public:
	inline static int32_t get_offset_of_meshes_0() { return static_cast<int32_t>(offsetof(Triangles_t2090031681_StaticFields, ___meshes_0)); }
	inline MeshU5BU5D_t3972987605* get_meshes_0() const { return ___meshes_0; }
	inline MeshU5BU5D_t3972987605** get_address_of_meshes_0() { return &___meshes_0; }
	inline void set_meshes_0(MeshU5BU5D_t3972987605* value)
	{
		___meshes_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_0), value);
	}

	inline static int32_t get_offset_of_currentTris_1() { return static_cast<int32_t>(offsetof(Triangles_t2090031681_StaticFields, ___currentTris_1)); }
	inline int32_t get_currentTris_1() const { return ___currentTris_1; }
	inline int32_t* get_address_of_currentTris_1() { return &___currentTris_1; }
	inline void set_currentTris_1(int32_t value)
	{
		___currentTris_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGLES_T2090031681_H
#ifndef MESHCONTAINER_T140041298_H
#define MESHCONTAINER_T140041298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.MeshContainer
struct  MeshContainer_t140041298  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityStandardAssets.Water.MeshContainer::mesh
	Mesh_t3648964284 * ___mesh_0;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::vertices
	Vector3U5BU5D_t1718750761* ___vertices_1;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::normals
	Vector3U5BU5D_t1718750761* ___normals_2;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(MeshContainer_t140041298, ___mesh_0)); }
	inline Mesh_t3648964284 * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t3648964284 ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t3648964284 * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_0), value);
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(MeshContainer_t140041298, ___vertices_1)); }
	inline Vector3U5BU5D_t1718750761* get_vertices_1() const { return ___vertices_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(Vector3U5BU5D_t1718750761* value)
	{
		___vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_1), value);
	}

	inline static int32_t get_offset_of_normals_2() { return static_cast<int32_t>(offsetof(MeshContainer_t140041298, ___normals_2)); }
	inline Vector3U5BU5D_t1718750761* get_normals_2() const { return ___normals_2; }
	inline Vector3U5BU5D_t1718750761** get_address_of_normals_2() { return &___normals_2; }
	inline void set_normals_2(Vector3U5BU5D_t1718750761* value)
	{
		___normals_2 = value;
		Il2CppCodeGenWriteBarrier((&___normals_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCONTAINER_T140041298_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T2710994320_H
#define __STATICARRAYINITTYPESIZEU3D12_T2710994320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t2710994320 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t2710994320__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T2710994320_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_T3517759981_H
#define __STATICARRAYINITTYPESIZEU3D24_T3517759981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t3517759981 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t3517759981__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_T3517759981_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255373_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255373  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::51A7A390CD6DE245186881400B18C9D822EFE240
	__StaticArrayInitTypeSizeU3D12_t2710994320  ___51A7A390CD6DE245186881400B18C9D822EFE240_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::C90F38A020811481753795774EB5AF353F414C59
	__StaticArrayInitTypeSizeU3D24_t3517759981  ___C90F38A020811481753795774EB5AF353F414C59_1;

public:
	inline static int32_t get_offset_of_U351A7A390CD6DE245186881400B18C9D822EFE240_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields, ___51A7A390CD6DE245186881400B18C9D822EFE240_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t2710994320  get_U351A7A390CD6DE245186881400B18C9D822EFE240_0() const { return ___51A7A390CD6DE245186881400B18C9D822EFE240_0; }
	inline __StaticArrayInitTypeSizeU3D12_t2710994320 * get_address_of_U351A7A390CD6DE245186881400B18C9D822EFE240_0() { return &___51A7A390CD6DE245186881400B18C9D822EFE240_0; }
	inline void set_U351A7A390CD6DE245186881400B18C9D822EFE240_0(__StaticArrayInitTypeSizeU3D12_t2710994320  value)
	{
		___51A7A390CD6DE245186881400B18C9D822EFE240_0 = value;
	}

	inline static int32_t get_offset_of_C90F38A020811481753795774EB5AF353F414C59_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields, ___C90F38A020811481753795774EB5AF353F414C59_1)); }
	inline __StaticArrayInitTypeSizeU3D24_t3517759981  get_C90F38A020811481753795774EB5AF353F414C59_1() const { return ___C90F38A020811481753795774EB5AF353F414C59_1; }
	inline __StaticArrayInitTypeSizeU3D24_t3517759981 * get_address_of_C90F38A020811481753795774EB5AF353F414C59_1() { return &___C90F38A020811481753795774EB5AF353F414C59_1; }
	inline void set_C90F38A020811481753795774EB5AF353F414C59_1(__StaticArrayInitTypeSizeU3D24_t3517759981  value)
	{
		___C90F38A020811481753795774EB5AF353F414C59_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255373_H
#ifndef BUBBLETYPE_T1488104874_H
#define BUBBLETYPE_T1488104874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BubbleType
struct  BubbleType_t1488104874 
{
public:
	// System.Int32 BubbleType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BubbleType_t1488104874, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUBBLETYPE_T1488104874_H
#ifndef COLLISIONFLAGS_T1776808576_H
#define COLLISIONFLAGS_T1776808576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CollisionFlags
struct  CollisionFlags_t1776808576 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CollisionFlags_t1776808576, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONFLAGS_T1776808576_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef UPDATETYPE_T2449601881_H
#define UPDATETYPE_T2449601881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType
struct  UpdateType_t2449601881 
{
public:
	// System.Int32 UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t2449601881, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T2449601881_H
#ifndef MOUSELOOK_T2859678661_H
#define MOUSELOOK_T2859678661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.MouseLook
struct  MouseLook_t2859678661  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::XSensitivity
	float ___XSensitivity_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::YSensitivity
	float ___YSensitivity_1;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::clampVerticalRotation
	bool ___clampVerticalRotation_2;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::MinimumX
	float ___MinimumX_3;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::MaximumX
	float ___MaximumX_4;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::smooth
	bool ___smooth_5;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::smoothTime
	float ___smoothTime_6;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::lockCursor
	bool ___lockCursor_7;
	// UnityEngine.Quaternion UnityStandardAssets.Characters.FirstPerson.MouseLook::m_CharacterTargetRot
	Quaternion_t2301928331  ___m_CharacterTargetRot_8;
	// UnityEngine.Quaternion UnityStandardAssets.Characters.FirstPerson.MouseLook::m_CameraTargetRot
	Quaternion_t2301928331  ___m_CameraTargetRot_9;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::m_cursorIsLocked
	bool ___m_cursorIsLocked_10;

public:
	inline static int32_t get_offset_of_XSensitivity_0() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___XSensitivity_0)); }
	inline float get_XSensitivity_0() const { return ___XSensitivity_0; }
	inline float* get_address_of_XSensitivity_0() { return &___XSensitivity_0; }
	inline void set_XSensitivity_0(float value)
	{
		___XSensitivity_0 = value;
	}

	inline static int32_t get_offset_of_YSensitivity_1() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___YSensitivity_1)); }
	inline float get_YSensitivity_1() const { return ___YSensitivity_1; }
	inline float* get_address_of_YSensitivity_1() { return &___YSensitivity_1; }
	inline void set_YSensitivity_1(float value)
	{
		___YSensitivity_1 = value;
	}

	inline static int32_t get_offset_of_clampVerticalRotation_2() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___clampVerticalRotation_2)); }
	inline bool get_clampVerticalRotation_2() const { return ___clampVerticalRotation_2; }
	inline bool* get_address_of_clampVerticalRotation_2() { return &___clampVerticalRotation_2; }
	inline void set_clampVerticalRotation_2(bool value)
	{
		___clampVerticalRotation_2 = value;
	}

	inline static int32_t get_offset_of_MinimumX_3() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___MinimumX_3)); }
	inline float get_MinimumX_3() const { return ___MinimumX_3; }
	inline float* get_address_of_MinimumX_3() { return &___MinimumX_3; }
	inline void set_MinimumX_3(float value)
	{
		___MinimumX_3 = value;
	}

	inline static int32_t get_offset_of_MaximumX_4() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___MaximumX_4)); }
	inline float get_MaximumX_4() const { return ___MaximumX_4; }
	inline float* get_address_of_MaximumX_4() { return &___MaximumX_4; }
	inline void set_MaximumX_4(float value)
	{
		___MaximumX_4 = value;
	}

	inline static int32_t get_offset_of_smooth_5() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___smooth_5)); }
	inline bool get_smooth_5() const { return ___smooth_5; }
	inline bool* get_address_of_smooth_5() { return &___smooth_5; }
	inline void set_smooth_5(bool value)
	{
		___smooth_5 = value;
	}

	inline static int32_t get_offset_of_smoothTime_6() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___smoothTime_6)); }
	inline float get_smoothTime_6() const { return ___smoothTime_6; }
	inline float* get_address_of_smoothTime_6() { return &___smoothTime_6; }
	inline void set_smoothTime_6(float value)
	{
		___smoothTime_6 = value;
	}

	inline static int32_t get_offset_of_lockCursor_7() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___lockCursor_7)); }
	inline bool get_lockCursor_7() const { return ___lockCursor_7; }
	inline bool* get_address_of_lockCursor_7() { return &___lockCursor_7; }
	inline void set_lockCursor_7(bool value)
	{
		___lockCursor_7 = value;
	}

	inline static int32_t get_offset_of_m_CharacterTargetRot_8() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___m_CharacterTargetRot_8)); }
	inline Quaternion_t2301928331  get_m_CharacterTargetRot_8() const { return ___m_CharacterTargetRot_8; }
	inline Quaternion_t2301928331 * get_address_of_m_CharacterTargetRot_8() { return &___m_CharacterTargetRot_8; }
	inline void set_m_CharacterTargetRot_8(Quaternion_t2301928331  value)
	{
		___m_CharacterTargetRot_8 = value;
	}

	inline static int32_t get_offset_of_m_CameraTargetRot_9() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___m_CameraTargetRot_9)); }
	inline Quaternion_t2301928331  get_m_CameraTargetRot_9() const { return ___m_CameraTargetRot_9; }
	inline Quaternion_t2301928331 * get_address_of_m_CameraTargetRot_9() { return &___m_CameraTargetRot_9; }
	inline void set_m_CameraTargetRot_9(Quaternion_t2301928331  value)
	{
		___m_CameraTargetRot_9 = value;
	}

	inline static int32_t get_offset_of_m_cursorIsLocked_10() { return static_cast<int32_t>(offsetof(MouseLook_t2859678661, ___m_cursorIsLocked_10)); }
	inline bool get_m_cursorIsLocked_10() const { return ___m_cursorIsLocked_10; }
	inline bool* get_address_of_m_cursorIsLocked_10() { return &___m_cursorIsLocked_10; }
	inline void set_m_cursorIsLocked_10(bool value)
	{
		___m_cursorIsLocked_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSELOOK_T2859678661_H
#ifndef ACTIVEINPUTMETHOD_T139315314_H
#define ACTIVEINPUTMETHOD_T139315314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
struct  ActiveInputMethod_t139315314 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActiveInputMethod_t139315314, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEINPUTMETHOD_T139315314_H
#ifndef AXISOPTION_T3128671669_H
#define AXISOPTION_T3128671669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
struct  AxisOption_t3128671669 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOption_t3128671669, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T3128671669_H
#ifndef MAPPINGTYPE_T2039944511_H
#define MAPPINGTYPE_T2039944511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
struct  MappingType_t2039944511 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MappingType_t2039944511, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_T2039944511_H
#ifndef AXISOPTIONS_T3101732129_H
#define AXISOPTIONS_T3101732129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
struct  AxisOptions_t3101732129 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOptions_t3101732129, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTIONS_T3101732129_H
#ifndef AXISOPTION_T1372819835_H
#define AXISOPTION_T1372819835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
struct  AxisOption_t1372819835 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOption_t1372819835, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T1372819835_H
#ifndef CONTROLSTYLE_T1372986211_H
#define CONTROLSTYLE_T1372986211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
struct  ControlStyle_t1372986211 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlStyle_t1372986211, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTYLE_T1372986211_H
#ifndef VIRTUALINPUT_T2597455733_H
#define VIRTUALINPUT_T2597455733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct  VirtualInput_t2597455733  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::<virtualMousePosition>k__BackingField
	Vector3_t3722313464  ___U3CvirtualMousePositionU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualAxes
	Dictionary_2_t3872604895 * ___m_VirtualAxes_1;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualButtons
	Dictionary_2_t2541822629 * ___m_VirtualButtons_2;
	// System.Collections.Generic.List`1<System.String> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_AlwaysUseVirtual
	List_1_t3319525431 * ___m_AlwaysUseVirtual_3;

public:
	inline static int32_t get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___U3CvirtualMousePositionU3Ek__BackingField_0)); }
	inline Vector3_t3722313464  get_U3CvirtualMousePositionU3Ek__BackingField_0() const { return ___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline Vector3_t3722313464 * get_address_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return &___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline void set_U3CvirtualMousePositionU3Ek__BackingField_0(Vector3_t3722313464  value)
	{
		___U3CvirtualMousePositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualAxes_1() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_VirtualAxes_1)); }
	inline Dictionary_2_t3872604895 * get_m_VirtualAxes_1() const { return ___m_VirtualAxes_1; }
	inline Dictionary_2_t3872604895 ** get_address_of_m_VirtualAxes_1() { return &___m_VirtualAxes_1; }
	inline void set_m_VirtualAxes_1(Dictionary_2_t3872604895 * value)
	{
		___m_VirtualAxes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualAxes_1), value);
	}

	inline static int32_t get_offset_of_m_VirtualButtons_2() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_VirtualButtons_2)); }
	inline Dictionary_2_t2541822629 * get_m_VirtualButtons_2() const { return ___m_VirtualButtons_2; }
	inline Dictionary_2_t2541822629 ** get_address_of_m_VirtualButtons_2() { return &___m_VirtualButtons_2; }
	inline void set_m_VirtualButtons_2(Dictionary_2_t2541822629 * value)
	{
		___m_VirtualButtons_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualButtons_2), value);
	}

	inline static int32_t get_offset_of_m_AlwaysUseVirtual_3() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_AlwaysUseVirtual_3)); }
	inline List_1_t3319525431 * get_m_AlwaysUseVirtual_3() const { return ___m_AlwaysUseVirtual_3; }
	inline List_1_t3319525431 ** get_address_of_m_AlwaysUseVirtual_3() { return &___m_AlwaysUseVirtual_3; }
	inline void set_m_AlwaysUseVirtual_3(List_1_t3319525431 * value)
	{
		___m_AlwaysUseVirtual_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlwaysUseVirtual_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALINPUT_T2597455733_H
#ifndef BLOOMQUALITY_T3369172721_H
#define BLOOMQUALITY_T3369172721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/BloomQuality
struct  BloomQuality_t3369172721 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/BloomQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BloomQuality_t3369172721, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMQUALITY_T3369172721_H
#ifndef BLOOMSCREENBLENDMODE_T2012607685_H
#define BLOOMSCREENBLENDMODE_T2012607685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode
struct  BloomScreenBlendMode_t2012607685 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BloomScreenBlendMode_t2012607685, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMSCREENBLENDMODE_T2012607685_H
#ifndef HDRBLOOMMODE_T3774419504_H
#define HDRBLOOMMODE_T3774419504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode
struct  HDRBloomMode_t3774419504 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HDRBloomMode_t3774419504, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HDRBLOOMMODE_T3774419504_H
#ifndef LENSFLARESTYLE_T630413071_H
#define LENSFLARESTYLE_T630413071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle
struct  LensFlareStyle_t630413071 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LensFlareStyle_t630413071, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSFLARESTYLE_T630413071_H
#ifndef TWEAKMODE_T747557136_H
#define TWEAKMODE_T747557136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom/TweakMode
struct  TweakMode_t747557136 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom/TweakMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TweakMode_t747557136, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEAKMODE_T747557136_H
#ifndef OVERLAYBLENDMODE_T429753458_H
#define OVERLAYBLENDMODE_T429753458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode
struct  OverlayBlendMode_t429753458 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OverlayBlendMode_t429753458, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERLAYBLENDMODE_T429753458_H
#ifndef SSAOSAMPLES_T2619211009_H
#define SSAOSAMPLES_T2619211009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples
struct  SSAOSamples_t2619211009 
{
public:
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SSAOSamples_t2619211009, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSAOSAMPLES_T2619211009_H
#ifndef WATERMODE_T293960580_H
#define WATERMODE_T293960580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Water/WaterMode
struct  WaterMode_t293960580 
{
public:
	// System.Int32 UnityStandardAssets.Water.Water/WaterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WaterMode_t293960580, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERMODE_T293960580_H
#ifndef WATERQUALITY_T1541080576_H
#define WATERQUALITY_T1541080576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterQuality
struct  WaterQuality_t1541080576 
{
public:
	// System.Int32 UnityStandardAssets.Water.WaterQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WaterQuality_t1541080576, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERQUALITY_T1541080576_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef MOVEMENTSETTINGS_T1096092444_H
#define MOVEMENTSETTINGS_T1096092444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings
struct  MovementSettings_t1096092444  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::ForwardSpeed
	float ___ForwardSpeed_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::BackwardSpeed
	float ___BackwardSpeed_1;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::StrafeSpeed
	float ___StrafeSpeed_2;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::RunMultiplier
	float ___RunMultiplier_3;
	// UnityEngine.KeyCode UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::RunKey
	int32_t ___RunKey_4;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::JumpForce
	float ___JumpForce_5;
	// UnityEngine.AnimationCurve UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::SlopeCurveModifier
	AnimationCurve_t3046754366 * ___SlopeCurveModifier_6;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::CurrentTargetSpeed
	float ___CurrentTargetSpeed_7;

public:
	inline static int32_t get_offset_of_ForwardSpeed_0() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___ForwardSpeed_0)); }
	inline float get_ForwardSpeed_0() const { return ___ForwardSpeed_0; }
	inline float* get_address_of_ForwardSpeed_0() { return &___ForwardSpeed_0; }
	inline void set_ForwardSpeed_0(float value)
	{
		___ForwardSpeed_0 = value;
	}

	inline static int32_t get_offset_of_BackwardSpeed_1() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___BackwardSpeed_1)); }
	inline float get_BackwardSpeed_1() const { return ___BackwardSpeed_1; }
	inline float* get_address_of_BackwardSpeed_1() { return &___BackwardSpeed_1; }
	inline void set_BackwardSpeed_1(float value)
	{
		___BackwardSpeed_1 = value;
	}

	inline static int32_t get_offset_of_StrafeSpeed_2() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___StrafeSpeed_2)); }
	inline float get_StrafeSpeed_2() const { return ___StrafeSpeed_2; }
	inline float* get_address_of_StrafeSpeed_2() { return &___StrafeSpeed_2; }
	inline void set_StrafeSpeed_2(float value)
	{
		___StrafeSpeed_2 = value;
	}

	inline static int32_t get_offset_of_RunMultiplier_3() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___RunMultiplier_3)); }
	inline float get_RunMultiplier_3() const { return ___RunMultiplier_3; }
	inline float* get_address_of_RunMultiplier_3() { return &___RunMultiplier_3; }
	inline void set_RunMultiplier_3(float value)
	{
		___RunMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_RunKey_4() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___RunKey_4)); }
	inline int32_t get_RunKey_4() const { return ___RunKey_4; }
	inline int32_t* get_address_of_RunKey_4() { return &___RunKey_4; }
	inline void set_RunKey_4(int32_t value)
	{
		___RunKey_4 = value;
	}

	inline static int32_t get_offset_of_JumpForce_5() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___JumpForce_5)); }
	inline float get_JumpForce_5() const { return ___JumpForce_5; }
	inline float* get_address_of_JumpForce_5() { return &___JumpForce_5; }
	inline void set_JumpForce_5(float value)
	{
		___JumpForce_5 = value;
	}

	inline static int32_t get_offset_of_SlopeCurveModifier_6() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___SlopeCurveModifier_6)); }
	inline AnimationCurve_t3046754366 * get_SlopeCurveModifier_6() const { return ___SlopeCurveModifier_6; }
	inline AnimationCurve_t3046754366 ** get_address_of_SlopeCurveModifier_6() { return &___SlopeCurveModifier_6; }
	inline void set_SlopeCurveModifier_6(AnimationCurve_t3046754366 * value)
	{
		___SlopeCurveModifier_6 = value;
		Il2CppCodeGenWriteBarrier((&___SlopeCurveModifier_6), value);
	}

	inline static int32_t get_offset_of_CurrentTargetSpeed_7() { return static_cast<int32_t>(offsetof(MovementSettings_t1096092444, ___CurrentTargetSpeed_7)); }
	inline float get_CurrentTargetSpeed_7() const { return ___CurrentTargetSpeed_7; }
	inline float* get_address_of_CurrentTargetSpeed_7() { return &___CurrentTargetSpeed_7; }
	inline void set_CurrentTargetSpeed_7(float value)
	{
		___CurrentTargetSpeed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTSETTINGS_T1096092444_H
#ifndef MOBILEINPUT_T2025745297_H
#define MOBILEINPUT_T2025745297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
struct  MobileInput_t2025745297  : public VirtualInput_t2597455733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T2025745297_H
#ifndef STANDALONEINPUT_T1343950252_H
#define STANDALONEINPUT_T1343950252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
struct  StandaloneInput_t1343950252  : public VirtualInput_t2597455733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUT_T1343950252_H
#ifndef AXISMAPPING_T3982445645_H
#define AXISMAPPING_T3982445645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct  AxisMapping_t3982445645  : public RuntimeObject
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::type
	int32_t ___type_0;
	// System.String UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::axisName
	String_t* ___axisName_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AxisMapping_t3982445645, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_axisName_1() { return static_cast<int32_t>(offsetof(AxisMapping_t3982445645, ___axisName_1)); }
	inline String_t* get_axisName_1() const { return ___axisName_1; }
	inline String_t** get_address_of_axisName_1() { return &___axisName_1; }
	inline void set_axisName_1(String_t* value)
	{
		___axisName_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISMAPPING_T3982445645_H
#ifndef IMPORTEDANIMATIONS_T3877126586_H
#define IMPORTEDANIMATIONS_T3877126586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImportedAnimations
struct  ImportedAnimations_t3877126586  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AnimationClip> ImportedAnimations::animations
	List_1_t3790580729 * ___animations_4;

public:
	inline static int32_t get_offset_of_animations_4() { return static_cast<int32_t>(offsetof(ImportedAnimations_t3877126586, ___animations_4)); }
	inline List_1_t3790580729 * get_animations_4() const { return ___animations_4; }
	inline List_1_t3790580729 ** get_address_of_animations_4() { return &___animations_4; }
	inline void set_animations_4(List_1_t3790580729 * value)
	{
		___animations_4 = value;
		Il2CppCodeGenWriteBarrier((&___animations_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTEDANIMATIONS_T3877126586_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef AICHARACTERCONTROL_T2137571320_H
#define AICHARACTERCONTROL_T2137571320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AICharacterControl
struct  AICharacterControl_t2137571320  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AI.NavMeshAgent AICharacterControl::<agent>k__BackingField
	NavMeshAgent_t1276799816 * ___U3CagentU3Ek__BackingField_4;
	// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter AICharacterControl::<character>k__BackingField
	ThirdPersonCharacter_t1711070432 * ___U3CcharacterU3Ek__BackingField_5;
	// UnityEngine.Transform AICharacterControl::target
	Transform_t3600365921 * ___target_6;
	// System.Single AICharacterControl::enemyWalkSpeed
	float ___enemyWalkSpeed_7;
	// System.Single AICharacterControl::enemyRunSpeed
	float ___enemyRunSpeed_8;
	// UnityEngine.Transform AICharacterControl::player
	Transform_t3600365921 * ___player_9;
	// System.Single AICharacterControl::attackRate
	float ___attackRate_11;
	// System.Single AICharacterControl::soundDelay
	float ___soundDelay_12;
	// System.Single AICharacterControl::damageAmount
	float ___damageAmount_13;
	// PlayerHealth AICharacterControl::PH
	PlayerHealth_t2068385516 * ___PH_14;
	// System.Single AICharacterControl::nextAttack
	float ___nextAttack_15;
	// System.Single AICharacterControl::playerDistance
	float ___playerDistance_16;
	// UnityEngine.Animator AICharacterControl::anim
	Animator_t434523843 * ___anim_17;
	// System.Boolean AICharacterControl::playerInRange
	bool ___playerInRange_18;
	// UnityEngine.AudioSource AICharacterControl::AS
	AudioSource_t3935305588 * ___AS_19;
	// UnityEngine.AudioClip[] AICharacterControl::playerHurtClips
	AudioClipU5BU5D_t143221404* ___playerHurtClips_20;

public:
	inline static int32_t get_offset_of_U3CagentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___U3CagentU3Ek__BackingField_4)); }
	inline NavMeshAgent_t1276799816 * get_U3CagentU3Ek__BackingField_4() const { return ___U3CagentU3Ek__BackingField_4; }
	inline NavMeshAgent_t1276799816 ** get_address_of_U3CagentU3Ek__BackingField_4() { return &___U3CagentU3Ek__BackingField_4; }
	inline void set_U3CagentU3Ek__BackingField_4(NavMeshAgent_t1276799816 * value)
	{
		___U3CagentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CagentU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcharacterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___U3CcharacterU3Ek__BackingField_5)); }
	inline ThirdPersonCharacter_t1711070432 * get_U3CcharacterU3Ek__BackingField_5() const { return ___U3CcharacterU3Ek__BackingField_5; }
	inline ThirdPersonCharacter_t1711070432 ** get_address_of_U3CcharacterU3Ek__BackingField_5() { return &___U3CcharacterU3Ek__BackingField_5; }
	inline void set_U3CcharacterU3Ek__BackingField_5(ThirdPersonCharacter_t1711070432 * value)
	{
		___U3CcharacterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcharacterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___target_6)); }
	inline Transform_t3600365921 * get_target_6() const { return ___target_6; }
	inline Transform_t3600365921 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t3600365921 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_enemyWalkSpeed_7() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___enemyWalkSpeed_7)); }
	inline float get_enemyWalkSpeed_7() const { return ___enemyWalkSpeed_7; }
	inline float* get_address_of_enemyWalkSpeed_7() { return &___enemyWalkSpeed_7; }
	inline void set_enemyWalkSpeed_7(float value)
	{
		___enemyWalkSpeed_7 = value;
	}

	inline static int32_t get_offset_of_enemyRunSpeed_8() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___enemyRunSpeed_8)); }
	inline float get_enemyRunSpeed_8() const { return ___enemyRunSpeed_8; }
	inline float* get_address_of_enemyRunSpeed_8() { return &___enemyRunSpeed_8; }
	inline void set_enemyRunSpeed_8(float value)
	{
		___enemyRunSpeed_8 = value;
	}

	inline static int32_t get_offset_of_player_9() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___player_9)); }
	inline Transform_t3600365921 * get_player_9() const { return ___player_9; }
	inline Transform_t3600365921 ** get_address_of_player_9() { return &___player_9; }
	inline void set_player_9(Transform_t3600365921 * value)
	{
		___player_9 = value;
		Il2CppCodeGenWriteBarrier((&___player_9), value);
	}

	inline static int32_t get_offset_of_attackRate_11() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___attackRate_11)); }
	inline float get_attackRate_11() const { return ___attackRate_11; }
	inline float* get_address_of_attackRate_11() { return &___attackRate_11; }
	inline void set_attackRate_11(float value)
	{
		___attackRate_11 = value;
	}

	inline static int32_t get_offset_of_soundDelay_12() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___soundDelay_12)); }
	inline float get_soundDelay_12() const { return ___soundDelay_12; }
	inline float* get_address_of_soundDelay_12() { return &___soundDelay_12; }
	inline void set_soundDelay_12(float value)
	{
		___soundDelay_12 = value;
	}

	inline static int32_t get_offset_of_damageAmount_13() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___damageAmount_13)); }
	inline float get_damageAmount_13() const { return ___damageAmount_13; }
	inline float* get_address_of_damageAmount_13() { return &___damageAmount_13; }
	inline void set_damageAmount_13(float value)
	{
		___damageAmount_13 = value;
	}

	inline static int32_t get_offset_of_PH_14() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___PH_14)); }
	inline PlayerHealth_t2068385516 * get_PH_14() const { return ___PH_14; }
	inline PlayerHealth_t2068385516 ** get_address_of_PH_14() { return &___PH_14; }
	inline void set_PH_14(PlayerHealth_t2068385516 * value)
	{
		___PH_14 = value;
		Il2CppCodeGenWriteBarrier((&___PH_14), value);
	}

	inline static int32_t get_offset_of_nextAttack_15() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___nextAttack_15)); }
	inline float get_nextAttack_15() const { return ___nextAttack_15; }
	inline float* get_address_of_nextAttack_15() { return &___nextAttack_15; }
	inline void set_nextAttack_15(float value)
	{
		___nextAttack_15 = value;
	}

	inline static int32_t get_offset_of_playerDistance_16() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___playerDistance_16)); }
	inline float get_playerDistance_16() const { return ___playerDistance_16; }
	inline float* get_address_of_playerDistance_16() { return &___playerDistance_16; }
	inline void set_playerDistance_16(float value)
	{
		___playerDistance_16 = value;
	}

	inline static int32_t get_offset_of_anim_17() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___anim_17)); }
	inline Animator_t434523843 * get_anim_17() const { return ___anim_17; }
	inline Animator_t434523843 ** get_address_of_anim_17() { return &___anim_17; }
	inline void set_anim_17(Animator_t434523843 * value)
	{
		___anim_17 = value;
		Il2CppCodeGenWriteBarrier((&___anim_17), value);
	}

	inline static int32_t get_offset_of_playerInRange_18() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___playerInRange_18)); }
	inline bool get_playerInRange_18() const { return ___playerInRange_18; }
	inline bool* get_address_of_playerInRange_18() { return &___playerInRange_18; }
	inline void set_playerInRange_18(bool value)
	{
		___playerInRange_18 = value;
	}

	inline static int32_t get_offset_of_AS_19() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___AS_19)); }
	inline AudioSource_t3935305588 * get_AS_19() const { return ___AS_19; }
	inline AudioSource_t3935305588 ** get_address_of_AS_19() { return &___AS_19; }
	inline void set_AS_19(AudioSource_t3935305588 * value)
	{
		___AS_19 = value;
		Il2CppCodeGenWriteBarrier((&___AS_19), value);
	}

	inline static int32_t get_offset_of_playerHurtClips_20() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320, ___playerHurtClips_20)); }
	inline AudioClipU5BU5D_t143221404* get_playerHurtClips_20() const { return ___playerHurtClips_20; }
	inline AudioClipU5BU5D_t143221404** get_address_of_playerHurtClips_20() { return &___playerHurtClips_20; }
	inline void set_playerHurtClips_20(AudioClipU5BU5D_t143221404* value)
	{
		___playerHurtClips_20 = value;
		Il2CppCodeGenWriteBarrier((&___playerHurtClips_20), value);
	}
};

struct AICharacterControl_t2137571320_StaticFields
{
public:
	// System.Boolean AICharacterControl::isPlayerAlive
	bool ___isPlayerAlive_10;

public:
	inline static int32_t get_offset_of_isPlayerAlive_10() { return static_cast<int32_t>(offsetof(AICharacterControl_t2137571320_StaticFields, ___isPlayerAlive_10)); }
	inline bool get_isPlayerAlive_10() const { return ___isPlayerAlive_10; }
	inline bool* get_address_of_isPlayerAlive_10() { return &___isPlayerAlive_10; }
	inline void set_isPlayerAlive_10(bool value)
	{
		___isPlayerAlive_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AICHARACTERCONTROL_T2137571320_H
#ifndef APPEAR_T3088471322_H
#define APPEAR_T3088471322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Appear
struct  Appear_t3088471322  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.SpriteRenderer> Appear::vImages
	List_1_t412733603 * ___vImages_4;
	// System.Single Appear::valpha
	float ___valpha_5;
	// System.Boolean Appear::vchoice
	bool ___vchoice_6;
	// System.Single Appear::vTimer
	float ___vTimer_7;
	// System.Single Appear::cTime
	float ___cTime_8;
	// System.Boolean Appear::needtoclick
	bool ___needtoclick_9;

public:
	inline static int32_t get_offset_of_vImages_4() { return static_cast<int32_t>(offsetof(Appear_t3088471322, ___vImages_4)); }
	inline List_1_t412733603 * get_vImages_4() const { return ___vImages_4; }
	inline List_1_t412733603 ** get_address_of_vImages_4() { return &___vImages_4; }
	inline void set_vImages_4(List_1_t412733603 * value)
	{
		___vImages_4 = value;
		Il2CppCodeGenWriteBarrier((&___vImages_4), value);
	}

	inline static int32_t get_offset_of_valpha_5() { return static_cast<int32_t>(offsetof(Appear_t3088471322, ___valpha_5)); }
	inline float get_valpha_5() const { return ___valpha_5; }
	inline float* get_address_of_valpha_5() { return &___valpha_5; }
	inline void set_valpha_5(float value)
	{
		___valpha_5 = value;
	}

	inline static int32_t get_offset_of_vchoice_6() { return static_cast<int32_t>(offsetof(Appear_t3088471322, ___vchoice_6)); }
	inline bool get_vchoice_6() const { return ___vchoice_6; }
	inline bool* get_address_of_vchoice_6() { return &___vchoice_6; }
	inline void set_vchoice_6(bool value)
	{
		___vchoice_6 = value;
	}

	inline static int32_t get_offset_of_vTimer_7() { return static_cast<int32_t>(offsetof(Appear_t3088471322, ___vTimer_7)); }
	inline float get_vTimer_7() const { return ___vTimer_7; }
	inline float* get_address_of_vTimer_7() { return &___vTimer_7; }
	inline void set_vTimer_7(float value)
	{
		___vTimer_7 = value;
	}

	inline static int32_t get_offset_of_cTime_8() { return static_cast<int32_t>(offsetof(Appear_t3088471322, ___cTime_8)); }
	inline float get_cTime_8() const { return ___cTime_8; }
	inline float* get_address_of_cTime_8() { return &___cTime_8; }
	inline void set_cTime_8(float value)
	{
		___cTime_8 = value;
	}

	inline static int32_t get_offset_of_needtoclick_9() { return static_cast<int32_t>(offsetof(Appear_t3088471322, ___needtoclick_9)); }
	inline bool get_needtoclick_9() const { return ___needtoclick_9; }
	inline bool* get_address_of_needtoclick_9() { return &___needtoclick_9; }
	inline void set_needtoclick_9(bool value)
	{
		___needtoclick_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPEAR_T3088471322_H
#ifndef BLOODPARTICLESPAWN_T4269314575_H
#define BLOODPARTICLESPAWN_T4269314575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BloodParticleSpawn
struct  BloodParticleSpawn_t4269314575  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform BloodParticleSpawn::swordParent
	Transform_t3600365921 * ___swordParent_4;
	// UnityEngine.GameObject BloodParticleSpawn::bloodFlowParticle
	GameObject_t1113636619 * ___bloodFlowParticle_5;
	// System.Boolean BloodParticleSpawn::isTouching
	bool ___isTouching_6;

public:
	inline static int32_t get_offset_of_swordParent_4() { return static_cast<int32_t>(offsetof(BloodParticleSpawn_t4269314575, ___swordParent_4)); }
	inline Transform_t3600365921 * get_swordParent_4() const { return ___swordParent_4; }
	inline Transform_t3600365921 ** get_address_of_swordParent_4() { return &___swordParent_4; }
	inline void set_swordParent_4(Transform_t3600365921 * value)
	{
		___swordParent_4 = value;
		Il2CppCodeGenWriteBarrier((&___swordParent_4), value);
	}

	inline static int32_t get_offset_of_bloodFlowParticle_5() { return static_cast<int32_t>(offsetof(BloodParticleSpawn_t4269314575, ___bloodFlowParticle_5)); }
	inline GameObject_t1113636619 * get_bloodFlowParticle_5() const { return ___bloodFlowParticle_5; }
	inline GameObject_t1113636619 ** get_address_of_bloodFlowParticle_5() { return &___bloodFlowParticle_5; }
	inline void set_bloodFlowParticle_5(GameObject_t1113636619 * value)
	{
		___bloodFlowParticle_5 = value;
		Il2CppCodeGenWriteBarrier((&___bloodFlowParticle_5), value);
	}

	inline static int32_t get_offset_of_isTouching_6() { return static_cast<int32_t>(offsetof(BloodParticleSpawn_t4269314575, ___isTouching_6)); }
	inline bool get_isTouching_6() const { return ___isTouching_6; }
	inline bool* get_address_of_isTouching_6() { return &___isTouching_6; }
	inline void set_isTouching_6(bool value)
	{
		___isTouching_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOODPARTICLESPAWN_T4269314575_H
#ifndef DIALOGBUBBLE_T2812184622_H
#define DIALOGBUBBLE_T2812184622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogBubble
struct  DialogBubble_t2812184622  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Ray DialogBubble::ray
	Ray_t3785851493  ___ray_4;
	// UnityEngine.RaycastHit DialogBubble::hit
	RaycastHit_t1056001966  ___hit_5;
	// UnityEngine.GameObject DialogBubble::vCurrentBubble
	GameObject_t1113636619 * ___vCurrentBubble_6;
	// System.Boolean DialogBubble::IsTalking
	bool ___IsTalking_7;
	// System.Collections.Generic.List`1<AssemblyCSharp.PixelBubble> DialogBubble::vBubble
	List_1_t494849140 * ___vBubble_8;
	// AssemblyCSharp.PixelBubble DialogBubble::vActiveBubble
	PixelBubble_t3317741694 * ___vActiveBubble_9;

public:
	inline static int32_t get_offset_of_ray_4() { return static_cast<int32_t>(offsetof(DialogBubble_t2812184622, ___ray_4)); }
	inline Ray_t3785851493  get_ray_4() const { return ___ray_4; }
	inline Ray_t3785851493 * get_address_of_ray_4() { return &___ray_4; }
	inline void set_ray_4(Ray_t3785851493  value)
	{
		___ray_4 = value;
	}

	inline static int32_t get_offset_of_hit_5() { return static_cast<int32_t>(offsetof(DialogBubble_t2812184622, ___hit_5)); }
	inline RaycastHit_t1056001966  get_hit_5() const { return ___hit_5; }
	inline RaycastHit_t1056001966 * get_address_of_hit_5() { return &___hit_5; }
	inline void set_hit_5(RaycastHit_t1056001966  value)
	{
		___hit_5 = value;
	}

	inline static int32_t get_offset_of_vCurrentBubble_6() { return static_cast<int32_t>(offsetof(DialogBubble_t2812184622, ___vCurrentBubble_6)); }
	inline GameObject_t1113636619 * get_vCurrentBubble_6() const { return ___vCurrentBubble_6; }
	inline GameObject_t1113636619 ** get_address_of_vCurrentBubble_6() { return &___vCurrentBubble_6; }
	inline void set_vCurrentBubble_6(GameObject_t1113636619 * value)
	{
		___vCurrentBubble_6 = value;
		Il2CppCodeGenWriteBarrier((&___vCurrentBubble_6), value);
	}

	inline static int32_t get_offset_of_IsTalking_7() { return static_cast<int32_t>(offsetof(DialogBubble_t2812184622, ___IsTalking_7)); }
	inline bool get_IsTalking_7() const { return ___IsTalking_7; }
	inline bool* get_address_of_IsTalking_7() { return &___IsTalking_7; }
	inline void set_IsTalking_7(bool value)
	{
		___IsTalking_7 = value;
	}

	inline static int32_t get_offset_of_vBubble_8() { return static_cast<int32_t>(offsetof(DialogBubble_t2812184622, ___vBubble_8)); }
	inline List_1_t494849140 * get_vBubble_8() const { return ___vBubble_8; }
	inline List_1_t494849140 ** get_address_of_vBubble_8() { return &___vBubble_8; }
	inline void set_vBubble_8(List_1_t494849140 * value)
	{
		___vBubble_8 = value;
		Il2CppCodeGenWriteBarrier((&___vBubble_8), value);
	}

	inline static int32_t get_offset_of_vActiveBubble_9() { return static_cast<int32_t>(offsetof(DialogBubble_t2812184622, ___vActiveBubble_9)); }
	inline PixelBubble_t3317741694 * get_vActiveBubble_9() const { return ___vActiveBubble_9; }
	inline PixelBubble_t3317741694 ** get_address_of_vActiveBubble_9() { return &___vActiveBubble_9; }
	inline void set_vActiveBubble_9(PixelBubble_t3317741694 * value)
	{
		___vActiveBubble_9 = value;
		Il2CppCodeGenWriteBarrier((&___vActiveBubble_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGBUBBLE_T2812184622_H
#ifndef DROPDOWNMANAGER_T1610485569_H
#define DROPDOWNMANAGER_T1610485569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DropdownManager
struct  DropdownManager_t1610485569  : public MonoBehaviour_t3962482529
{
public:
	// ImportedAnimations DropdownManager::animationData
	ImportedAnimations_t3877126586 * ___animationData_4;
	// UnityEngine.UI.Dropdown DropdownManager::dropdown
	Dropdown_t2274391225 * ___dropdown_5;
	// UnityEngine.Animator DropdownManager::animator
	Animator_t434523843 * ___animator_6;
	// UnityEngine.AnimationClip DropdownManager::idle
	AnimationClip_t2318505987 * ___idle_7;
	// UnityEngine.UI.Text DropdownManager::animName
	Text_t1901882714 * ___animName_8;
	// UnityEngine.UI.Text DropdownManager::animLenght
	Text_t1901882714 * ___animLenght_9;
	// System.String DropdownManager::animNameText
	String_t* ___animNameText_10;
	// System.String DropdownManager::animLengthText
	String_t* ___animLengthText_11;
	// UnityEngine.GameObject[] DropdownManager::copAttach
	GameObjectU5BU5D_t3328599146* ___copAttach_12;
	// System.String DropdownManager::copPrefix
	String_t* ___copPrefix_13;
	// UnityEngine.GameObject[] DropdownManager::brawAttach
	GameObjectU5BU5D_t3328599146* ___brawAttach_14;
	// System.String DropdownManager::brawPrefix
	String_t* ___brawPrefix_15;
	// UnityEngine.AnimationClip[] DropdownManager::m_animations
	AnimationClipU5BU5D_t1636626578* ___m_animations_16;
	// UnityEngine.AnimatorOverrideController DropdownManager::animatorOverride
	AnimatorOverrideController_t3582062219 * ___animatorOverride_17;
	// System.Int32 DropdownManager::currentAnimation
	int32_t ___currentAnimation_18;
	// System.Int32 DropdownManager::animLoop
	int32_t ___animLoop_21;

public:
	inline static int32_t get_offset_of_animationData_4() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___animationData_4)); }
	inline ImportedAnimations_t3877126586 * get_animationData_4() const { return ___animationData_4; }
	inline ImportedAnimations_t3877126586 ** get_address_of_animationData_4() { return &___animationData_4; }
	inline void set_animationData_4(ImportedAnimations_t3877126586 * value)
	{
		___animationData_4 = value;
		Il2CppCodeGenWriteBarrier((&___animationData_4), value);
	}

	inline static int32_t get_offset_of_dropdown_5() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___dropdown_5)); }
	inline Dropdown_t2274391225 * get_dropdown_5() const { return ___dropdown_5; }
	inline Dropdown_t2274391225 ** get_address_of_dropdown_5() { return &___dropdown_5; }
	inline void set_dropdown_5(Dropdown_t2274391225 * value)
	{
		___dropdown_5 = value;
		Il2CppCodeGenWriteBarrier((&___dropdown_5), value);
	}

	inline static int32_t get_offset_of_animator_6() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___animator_6)); }
	inline Animator_t434523843 * get_animator_6() const { return ___animator_6; }
	inline Animator_t434523843 ** get_address_of_animator_6() { return &___animator_6; }
	inline void set_animator_6(Animator_t434523843 * value)
	{
		___animator_6 = value;
		Il2CppCodeGenWriteBarrier((&___animator_6), value);
	}

	inline static int32_t get_offset_of_idle_7() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___idle_7)); }
	inline AnimationClip_t2318505987 * get_idle_7() const { return ___idle_7; }
	inline AnimationClip_t2318505987 ** get_address_of_idle_7() { return &___idle_7; }
	inline void set_idle_7(AnimationClip_t2318505987 * value)
	{
		___idle_7 = value;
		Il2CppCodeGenWriteBarrier((&___idle_7), value);
	}

	inline static int32_t get_offset_of_animName_8() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___animName_8)); }
	inline Text_t1901882714 * get_animName_8() const { return ___animName_8; }
	inline Text_t1901882714 ** get_address_of_animName_8() { return &___animName_8; }
	inline void set_animName_8(Text_t1901882714 * value)
	{
		___animName_8 = value;
		Il2CppCodeGenWriteBarrier((&___animName_8), value);
	}

	inline static int32_t get_offset_of_animLenght_9() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___animLenght_9)); }
	inline Text_t1901882714 * get_animLenght_9() const { return ___animLenght_9; }
	inline Text_t1901882714 ** get_address_of_animLenght_9() { return &___animLenght_9; }
	inline void set_animLenght_9(Text_t1901882714 * value)
	{
		___animLenght_9 = value;
		Il2CppCodeGenWriteBarrier((&___animLenght_9), value);
	}

	inline static int32_t get_offset_of_animNameText_10() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___animNameText_10)); }
	inline String_t* get_animNameText_10() const { return ___animNameText_10; }
	inline String_t** get_address_of_animNameText_10() { return &___animNameText_10; }
	inline void set_animNameText_10(String_t* value)
	{
		___animNameText_10 = value;
		Il2CppCodeGenWriteBarrier((&___animNameText_10), value);
	}

	inline static int32_t get_offset_of_animLengthText_11() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___animLengthText_11)); }
	inline String_t* get_animLengthText_11() const { return ___animLengthText_11; }
	inline String_t** get_address_of_animLengthText_11() { return &___animLengthText_11; }
	inline void set_animLengthText_11(String_t* value)
	{
		___animLengthText_11 = value;
		Il2CppCodeGenWriteBarrier((&___animLengthText_11), value);
	}

	inline static int32_t get_offset_of_copAttach_12() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___copAttach_12)); }
	inline GameObjectU5BU5D_t3328599146* get_copAttach_12() const { return ___copAttach_12; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_copAttach_12() { return &___copAttach_12; }
	inline void set_copAttach_12(GameObjectU5BU5D_t3328599146* value)
	{
		___copAttach_12 = value;
		Il2CppCodeGenWriteBarrier((&___copAttach_12), value);
	}

	inline static int32_t get_offset_of_copPrefix_13() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___copPrefix_13)); }
	inline String_t* get_copPrefix_13() const { return ___copPrefix_13; }
	inline String_t** get_address_of_copPrefix_13() { return &___copPrefix_13; }
	inline void set_copPrefix_13(String_t* value)
	{
		___copPrefix_13 = value;
		Il2CppCodeGenWriteBarrier((&___copPrefix_13), value);
	}

	inline static int32_t get_offset_of_brawAttach_14() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___brawAttach_14)); }
	inline GameObjectU5BU5D_t3328599146* get_brawAttach_14() const { return ___brawAttach_14; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_brawAttach_14() { return &___brawAttach_14; }
	inline void set_brawAttach_14(GameObjectU5BU5D_t3328599146* value)
	{
		___brawAttach_14 = value;
		Il2CppCodeGenWriteBarrier((&___brawAttach_14), value);
	}

	inline static int32_t get_offset_of_brawPrefix_15() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___brawPrefix_15)); }
	inline String_t* get_brawPrefix_15() const { return ___brawPrefix_15; }
	inline String_t** get_address_of_brawPrefix_15() { return &___brawPrefix_15; }
	inline void set_brawPrefix_15(String_t* value)
	{
		___brawPrefix_15 = value;
		Il2CppCodeGenWriteBarrier((&___brawPrefix_15), value);
	}

	inline static int32_t get_offset_of_m_animations_16() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___m_animations_16)); }
	inline AnimationClipU5BU5D_t1636626578* get_m_animations_16() const { return ___m_animations_16; }
	inline AnimationClipU5BU5D_t1636626578** get_address_of_m_animations_16() { return &___m_animations_16; }
	inline void set_m_animations_16(AnimationClipU5BU5D_t1636626578* value)
	{
		___m_animations_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_animations_16), value);
	}

	inline static int32_t get_offset_of_animatorOverride_17() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___animatorOverride_17)); }
	inline AnimatorOverrideController_t3582062219 * get_animatorOverride_17() const { return ___animatorOverride_17; }
	inline AnimatorOverrideController_t3582062219 ** get_address_of_animatorOverride_17() { return &___animatorOverride_17; }
	inline void set_animatorOverride_17(AnimatorOverrideController_t3582062219 * value)
	{
		___animatorOverride_17 = value;
		Il2CppCodeGenWriteBarrier((&___animatorOverride_17), value);
	}

	inline static int32_t get_offset_of_currentAnimation_18() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___currentAnimation_18)); }
	inline int32_t get_currentAnimation_18() const { return ___currentAnimation_18; }
	inline int32_t* get_address_of_currentAnimation_18() { return &___currentAnimation_18; }
	inline void set_currentAnimation_18(int32_t value)
	{
		___currentAnimation_18 = value;
	}

	inline static int32_t get_offset_of_animLoop_21() { return static_cast<int32_t>(offsetof(DropdownManager_t1610485569, ___animLoop_21)); }
	inline int32_t get_animLoop_21() const { return ___animLoop_21; }
	inline int32_t* get_address_of_animLoop_21() { return &___animLoop_21; }
	inline void set_animLoop_21(int32_t value)
	{
		___animLoop_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNMANAGER_T1610485569_H
#ifndef ENEMYHEALTH_T797421206_H
#define ENEMYHEALTH_T797421206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyHealth
struct  EnemyHealth_t797421206  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 EnemyHealth::currentHealth
	int32_t ___currentHealth_4;
	// System.Int32 EnemyHealth::maxHealth
	int32_t ___maxHealth_5;
	// UnityEngine.GameObject EnemyHealth::ragdoll
	GameObject_t1113636619 * ___ragdoll_6;
	// UnityEngine.GameObject EnemyHealth::key
	GameObject_t1113636619 * ___key_7;
	// DestroySelf EnemyHealth::DS
	DestroySelf_t3777301674 * ___DS_8;

public:
	inline static int32_t get_offset_of_currentHealth_4() { return static_cast<int32_t>(offsetof(EnemyHealth_t797421206, ___currentHealth_4)); }
	inline int32_t get_currentHealth_4() const { return ___currentHealth_4; }
	inline int32_t* get_address_of_currentHealth_4() { return &___currentHealth_4; }
	inline void set_currentHealth_4(int32_t value)
	{
		___currentHealth_4 = value;
	}

	inline static int32_t get_offset_of_maxHealth_5() { return static_cast<int32_t>(offsetof(EnemyHealth_t797421206, ___maxHealth_5)); }
	inline int32_t get_maxHealth_5() const { return ___maxHealth_5; }
	inline int32_t* get_address_of_maxHealth_5() { return &___maxHealth_5; }
	inline void set_maxHealth_5(int32_t value)
	{
		___maxHealth_5 = value;
	}

	inline static int32_t get_offset_of_ragdoll_6() { return static_cast<int32_t>(offsetof(EnemyHealth_t797421206, ___ragdoll_6)); }
	inline GameObject_t1113636619 * get_ragdoll_6() const { return ___ragdoll_6; }
	inline GameObject_t1113636619 ** get_address_of_ragdoll_6() { return &___ragdoll_6; }
	inline void set_ragdoll_6(GameObject_t1113636619 * value)
	{
		___ragdoll_6 = value;
		Il2CppCodeGenWriteBarrier((&___ragdoll_6), value);
	}

	inline static int32_t get_offset_of_key_7() { return static_cast<int32_t>(offsetof(EnemyHealth_t797421206, ___key_7)); }
	inline GameObject_t1113636619 * get_key_7() const { return ___key_7; }
	inline GameObject_t1113636619 ** get_address_of_key_7() { return &___key_7; }
	inline void set_key_7(GameObject_t1113636619 * value)
	{
		___key_7 = value;
		Il2CppCodeGenWriteBarrier((&___key_7), value);
	}

	inline static int32_t get_offset_of_DS_8() { return static_cast<int32_t>(offsetof(EnemyHealth_t797421206, ___DS_8)); }
	inline DestroySelf_t3777301674 * get_DS_8() const { return ___DS_8; }
	inline DestroySelf_t3777301674 ** get_address_of_DS_8() { return &___DS_8; }
	inline void set_DS_8(DestroySelf_t3777301674 * value)
	{
		___DS_8 = value;
		Il2CppCodeGenWriteBarrier((&___DS_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYHEALTH_T797421206_H
#ifndef FOOTSTEPS_T730116851_H
#define FOOTSTEPS_T730116851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FootSteps
struct  FootSteps_t730116851  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip[] FootSteps::clips
	AudioClipU5BU5D_t143221404* ___clips_4;
	// UnityEngine.AudioSource FootSteps::audioSource
	AudioSource_t3935305588 * ___audioSource_5;
	// UnityEngine.AudioClip[] FootSteps::swordSounds
	AudioClipU5BU5D_t143221404* ___swordSounds_6;
	// UnityEngine.AudioClip[] FootSteps::enemyPunchSounds
	AudioClipU5BU5D_t143221404* ___enemyPunchSounds_7;
	// UnityEngine.AudioClip[] FootSteps::enemyStepSounds
	AudioClipU5BU5D_t143221404* ___enemyStepSounds_8;

public:
	inline static int32_t get_offset_of_clips_4() { return static_cast<int32_t>(offsetof(FootSteps_t730116851, ___clips_4)); }
	inline AudioClipU5BU5D_t143221404* get_clips_4() const { return ___clips_4; }
	inline AudioClipU5BU5D_t143221404** get_address_of_clips_4() { return &___clips_4; }
	inline void set_clips_4(AudioClipU5BU5D_t143221404* value)
	{
		___clips_4 = value;
		Il2CppCodeGenWriteBarrier((&___clips_4), value);
	}

	inline static int32_t get_offset_of_audioSource_5() { return static_cast<int32_t>(offsetof(FootSteps_t730116851, ___audioSource_5)); }
	inline AudioSource_t3935305588 * get_audioSource_5() const { return ___audioSource_5; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_5() { return &___audioSource_5; }
	inline void set_audioSource_5(AudioSource_t3935305588 * value)
	{
		___audioSource_5 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_5), value);
	}

	inline static int32_t get_offset_of_swordSounds_6() { return static_cast<int32_t>(offsetof(FootSteps_t730116851, ___swordSounds_6)); }
	inline AudioClipU5BU5D_t143221404* get_swordSounds_6() const { return ___swordSounds_6; }
	inline AudioClipU5BU5D_t143221404** get_address_of_swordSounds_6() { return &___swordSounds_6; }
	inline void set_swordSounds_6(AudioClipU5BU5D_t143221404* value)
	{
		___swordSounds_6 = value;
		Il2CppCodeGenWriteBarrier((&___swordSounds_6), value);
	}

	inline static int32_t get_offset_of_enemyPunchSounds_7() { return static_cast<int32_t>(offsetof(FootSteps_t730116851, ___enemyPunchSounds_7)); }
	inline AudioClipU5BU5D_t143221404* get_enemyPunchSounds_7() const { return ___enemyPunchSounds_7; }
	inline AudioClipU5BU5D_t143221404** get_address_of_enemyPunchSounds_7() { return &___enemyPunchSounds_7; }
	inline void set_enemyPunchSounds_7(AudioClipU5BU5D_t143221404* value)
	{
		___enemyPunchSounds_7 = value;
		Il2CppCodeGenWriteBarrier((&___enemyPunchSounds_7), value);
	}

	inline static int32_t get_offset_of_enemyStepSounds_8() { return static_cast<int32_t>(offsetof(FootSteps_t730116851, ___enemyStepSounds_8)); }
	inline AudioClipU5BU5D_t143221404* get_enemyStepSounds_8() const { return ___enemyStepSounds_8; }
	inline AudioClipU5BU5D_t143221404** get_address_of_enemyStepSounds_8() { return &___enemyStepSounds_8; }
	inline void set_enemyStepSounds_8(AudioClipU5BU5D_t143221404* value)
	{
		___enemyStepSounds_8 = value;
		Il2CppCodeGenWriteBarrier((&___enemyStepSounds_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOOTSTEPS_T730116851_H
#ifndef KNIGHT_AC_T2269464005_H
#define KNIGHT_AC_T2269464005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Knight_AC
struct  Knight_AC_t2269464005  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator Knight_AC::anim
	Animator_t434523843 * ___anim_4;
	// System.Single Knight_AC::nextSwing
	float ___nextSwing_5;
	// System.Boolean Knight_AC::enemyInRange
	bool ___enemyInRange_6;
	// EnemyHealth Knight_AC::EH
	EnemyHealth_t797421206 * ___EH_7;
	// System.Single Knight_AC::swingRate
	float ___swingRate_8;
	// System.Int32 Knight_AC::playerDamage
	int32_t ___playerDamage_9;

public:
	inline static int32_t get_offset_of_anim_4() { return static_cast<int32_t>(offsetof(Knight_AC_t2269464005, ___anim_4)); }
	inline Animator_t434523843 * get_anim_4() const { return ___anim_4; }
	inline Animator_t434523843 ** get_address_of_anim_4() { return &___anim_4; }
	inline void set_anim_4(Animator_t434523843 * value)
	{
		___anim_4 = value;
		Il2CppCodeGenWriteBarrier((&___anim_4), value);
	}

	inline static int32_t get_offset_of_nextSwing_5() { return static_cast<int32_t>(offsetof(Knight_AC_t2269464005, ___nextSwing_5)); }
	inline float get_nextSwing_5() const { return ___nextSwing_5; }
	inline float* get_address_of_nextSwing_5() { return &___nextSwing_5; }
	inline void set_nextSwing_5(float value)
	{
		___nextSwing_5 = value;
	}

	inline static int32_t get_offset_of_enemyInRange_6() { return static_cast<int32_t>(offsetof(Knight_AC_t2269464005, ___enemyInRange_6)); }
	inline bool get_enemyInRange_6() const { return ___enemyInRange_6; }
	inline bool* get_address_of_enemyInRange_6() { return &___enemyInRange_6; }
	inline void set_enemyInRange_6(bool value)
	{
		___enemyInRange_6 = value;
	}

	inline static int32_t get_offset_of_EH_7() { return static_cast<int32_t>(offsetof(Knight_AC_t2269464005, ___EH_7)); }
	inline EnemyHealth_t797421206 * get_EH_7() const { return ___EH_7; }
	inline EnemyHealth_t797421206 ** get_address_of_EH_7() { return &___EH_7; }
	inline void set_EH_7(EnemyHealth_t797421206 * value)
	{
		___EH_7 = value;
		Il2CppCodeGenWriteBarrier((&___EH_7), value);
	}

	inline static int32_t get_offset_of_swingRate_8() { return static_cast<int32_t>(offsetof(Knight_AC_t2269464005, ___swingRate_8)); }
	inline float get_swingRate_8() const { return ___swingRate_8; }
	inline float* get_address_of_swingRate_8() { return &___swingRate_8; }
	inline void set_swingRate_8(float value)
	{
		___swingRate_8 = value;
	}

	inline static int32_t get_offset_of_playerDamage_9() { return static_cast<int32_t>(offsetof(Knight_AC_t2269464005, ___playerDamage_9)); }
	inline int32_t get_playerDamage_9() const { return ___playerDamage_9; }
	inline int32_t* get_address_of_playerDamage_9() { return &___playerDamage_9; }
	inline void set_playerDamage_9(int32_t value)
	{
		___playerDamage_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNIGHT_AC_T2269464005_H
#ifndef KNIGHT_ATTACK_T3463072348_H
#define KNIGHT_ATTACK_T3463072348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Knight_Attack
struct  Knight_Attack_t3463072348  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNIGHT_ATTACK_T3463072348_H
#ifndef MAINMENUCONTROLLER_T905567592_H
#define MAINMENUCONTROLLER_T905567592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenuController
struct  MainMenuController_t905567592  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator MainMenuController::anim
	Animator_t434523843 * ___anim_4;
	// System.String MainMenuController::newGameSceneName
	String_t* ___newGameSceneName_5;
	// System.Int32 MainMenuController::quickSaveSlotID
	int32_t ___quickSaveSlotID_6;
	// UnityEngine.GameObject MainMenuController::MainOptionsPanel
	GameObject_t1113636619 * ___MainOptionsPanel_7;
	// UnityEngine.GameObject MainMenuController::StartGameOptionsPanel
	GameObject_t1113636619 * ___StartGameOptionsPanel_8;
	// UnityEngine.GameObject MainMenuController::GamePanel
	GameObject_t1113636619 * ___GamePanel_9;
	// UnityEngine.GameObject MainMenuController::ControlsPanel
	GameObject_t1113636619 * ___ControlsPanel_10;
	// UnityEngine.GameObject MainMenuController::GfxPanel
	GameObject_t1113636619 * ___GfxPanel_11;
	// UnityEngine.GameObject MainMenuController::LoadGamePanel
	GameObject_t1113636619 * ___LoadGamePanel_12;

public:
	inline static int32_t get_offset_of_anim_4() { return static_cast<int32_t>(offsetof(MainMenuController_t905567592, ___anim_4)); }
	inline Animator_t434523843 * get_anim_4() const { return ___anim_4; }
	inline Animator_t434523843 ** get_address_of_anim_4() { return &___anim_4; }
	inline void set_anim_4(Animator_t434523843 * value)
	{
		___anim_4 = value;
		Il2CppCodeGenWriteBarrier((&___anim_4), value);
	}

	inline static int32_t get_offset_of_newGameSceneName_5() { return static_cast<int32_t>(offsetof(MainMenuController_t905567592, ___newGameSceneName_5)); }
	inline String_t* get_newGameSceneName_5() const { return ___newGameSceneName_5; }
	inline String_t** get_address_of_newGameSceneName_5() { return &___newGameSceneName_5; }
	inline void set_newGameSceneName_5(String_t* value)
	{
		___newGameSceneName_5 = value;
		Il2CppCodeGenWriteBarrier((&___newGameSceneName_5), value);
	}

	inline static int32_t get_offset_of_quickSaveSlotID_6() { return static_cast<int32_t>(offsetof(MainMenuController_t905567592, ___quickSaveSlotID_6)); }
	inline int32_t get_quickSaveSlotID_6() const { return ___quickSaveSlotID_6; }
	inline int32_t* get_address_of_quickSaveSlotID_6() { return &___quickSaveSlotID_6; }
	inline void set_quickSaveSlotID_6(int32_t value)
	{
		___quickSaveSlotID_6 = value;
	}

	inline static int32_t get_offset_of_MainOptionsPanel_7() { return static_cast<int32_t>(offsetof(MainMenuController_t905567592, ___MainOptionsPanel_7)); }
	inline GameObject_t1113636619 * get_MainOptionsPanel_7() const { return ___MainOptionsPanel_7; }
	inline GameObject_t1113636619 ** get_address_of_MainOptionsPanel_7() { return &___MainOptionsPanel_7; }
	inline void set_MainOptionsPanel_7(GameObject_t1113636619 * value)
	{
		___MainOptionsPanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___MainOptionsPanel_7), value);
	}

	inline static int32_t get_offset_of_StartGameOptionsPanel_8() { return static_cast<int32_t>(offsetof(MainMenuController_t905567592, ___StartGameOptionsPanel_8)); }
	inline GameObject_t1113636619 * get_StartGameOptionsPanel_8() const { return ___StartGameOptionsPanel_8; }
	inline GameObject_t1113636619 ** get_address_of_StartGameOptionsPanel_8() { return &___StartGameOptionsPanel_8; }
	inline void set_StartGameOptionsPanel_8(GameObject_t1113636619 * value)
	{
		___StartGameOptionsPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___StartGameOptionsPanel_8), value);
	}

	inline static int32_t get_offset_of_GamePanel_9() { return static_cast<int32_t>(offsetof(MainMenuController_t905567592, ___GamePanel_9)); }
	inline GameObject_t1113636619 * get_GamePanel_9() const { return ___GamePanel_9; }
	inline GameObject_t1113636619 ** get_address_of_GamePanel_9() { return &___GamePanel_9; }
	inline void set_GamePanel_9(GameObject_t1113636619 * value)
	{
		___GamePanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___GamePanel_9), value);
	}

	inline static int32_t get_offset_of_ControlsPanel_10() { return static_cast<int32_t>(offsetof(MainMenuController_t905567592, ___ControlsPanel_10)); }
	inline GameObject_t1113636619 * get_ControlsPanel_10() const { return ___ControlsPanel_10; }
	inline GameObject_t1113636619 ** get_address_of_ControlsPanel_10() { return &___ControlsPanel_10; }
	inline void set_ControlsPanel_10(GameObject_t1113636619 * value)
	{
		___ControlsPanel_10 = value;
		Il2CppCodeGenWriteBarrier((&___ControlsPanel_10), value);
	}

	inline static int32_t get_offset_of_GfxPanel_11() { return static_cast<int32_t>(offsetof(MainMenuController_t905567592, ___GfxPanel_11)); }
	inline GameObject_t1113636619 * get_GfxPanel_11() const { return ___GfxPanel_11; }
	inline GameObject_t1113636619 ** get_address_of_GfxPanel_11() { return &___GfxPanel_11; }
	inline void set_GfxPanel_11(GameObject_t1113636619 * value)
	{
		___GfxPanel_11 = value;
		Il2CppCodeGenWriteBarrier((&___GfxPanel_11), value);
	}

	inline static int32_t get_offset_of_LoadGamePanel_12() { return static_cast<int32_t>(offsetof(MainMenuController_t905567592, ___LoadGamePanel_12)); }
	inline GameObject_t1113636619 * get_LoadGamePanel_12() const { return ___LoadGamePanel_12; }
	inline GameObject_t1113636619 ** get_address_of_LoadGamePanel_12() { return &___LoadGamePanel_12; }
	inline void set_LoadGamePanel_12(GameObject_t1113636619 * value)
	{
		___LoadGamePanel_12 = value;
		Il2CppCodeGenWriteBarrier((&___LoadGamePanel_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENUCONTROLLER_T905567592_H
#ifndef MOVEMENTSCRIPT_T3129111103_H
#define MOVEMENTSCRIPT_T3129111103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MovementScript
struct  MovementScript_t3129111103  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera MovementScript::cam
	Camera_t4157153871 * ___cam_4;
	// UnityEngine.Animator MovementScript::animator
	Animator_t434523843 * ___animator_5;
	// System.Boolean MovementScript::useCustomRotation
	bool ___useCustomRotation_6;
	// System.Single MovementScript::rotationYSpeed
	float ___rotationYSpeed_7;
	// System.Single MovementScript::rotationXSpeed
	float ___rotationXSpeed_8;
	// System.Single MovementScript::defaultYMaxSpeed
	float ___defaultYMaxSpeed_11;
	// System.Single MovementScript::defaultXMaxSpeed
	float ___defaultXMaxSpeed_12;

public:
	inline static int32_t get_offset_of_cam_4() { return static_cast<int32_t>(offsetof(MovementScript_t3129111103, ___cam_4)); }
	inline Camera_t4157153871 * get_cam_4() const { return ___cam_4; }
	inline Camera_t4157153871 ** get_address_of_cam_4() { return &___cam_4; }
	inline void set_cam_4(Camera_t4157153871 * value)
	{
		___cam_4 = value;
		Il2CppCodeGenWriteBarrier((&___cam_4), value);
	}

	inline static int32_t get_offset_of_animator_5() { return static_cast<int32_t>(offsetof(MovementScript_t3129111103, ___animator_5)); }
	inline Animator_t434523843 * get_animator_5() const { return ___animator_5; }
	inline Animator_t434523843 ** get_address_of_animator_5() { return &___animator_5; }
	inline void set_animator_5(Animator_t434523843 * value)
	{
		___animator_5 = value;
		Il2CppCodeGenWriteBarrier((&___animator_5), value);
	}

	inline static int32_t get_offset_of_useCustomRotation_6() { return static_cast<int32_t>(offsetof(MovementScript_t3129111103, ___useCustomRotation_6)); }
	inline bool get_useCustomRotation_6() const { return ___useCustomRotation_6; }
	inline bool* get_address_of_useCustomRotation_6() { return &___useCustomRotation_6; }
	inline void set_useCustomRotation_6(bool value)
	{
		___useCustomRotation_6 = value;
	}

	inline static int32_t get_offset_of_rotationYSpeed_7() { return static_cast<int32_t>(offsetof(MovementScript_t3129111103, ___rotationYSpeed_7)); }
	inline float get_rotationYSpeed_7() const { return ___rotationYSpeed_7; }
	inline float* get_address_of_rotationYSpeed_7() { return &___rotationYSpeed_7; }
	inline void set_rotationYSpeed_7(float value)
	{
		___rotationYSpeed_7 = value;
	}

	inline static int32_t get_offset_of_rotationXSpeed_8() { return static_cast<int32_t>(offsetof(MovementScript_t3129111103, ___rotationXSpeed_8)); }
	inline float get_rotationXSpeed_8() const { return ___rotationXSpeed_8; }
	inline float* get_address_of_rotationXSpeed_8() { return &___rotationXSpeed_8; }
	inline void set_rotationXSpeed_8(float value)
	{
		___rotationXSpeed_8 = value;
	}

	inline static int32_t get_offset_of_defaultYMaxSpeed_11() { return static_cast<int32_t>(offsetof(MovementScript_t3129111103, ___defaultYMaxSpeed_11)); }
	inline float get_defaultYMaxSpeed_11() const { return ___defaultYMaxSpeed_11; }
	inline float* get_address_of_defaultYMaxSpeed_11() { return &___defaultYMaxSpeed_11; }
	inline void set_defaultYMaxSpeed_11(float value)
	{
		___defaultYMaxSpeed_11 = value;
	}

	inline static int32_t get_offset_of_defaultXMaxSpeed_12() { return static_cast<int32_t>(offsetof(MovementScript_t3129111103, ___defaultXMaxSpeed_12)); }
	inline float get_defaultXMaxSpeed_12() const { return ___defaultXMaxSpeed_12; }
	inline float* get_address_of_defaultXMaxSpeed_12() { return &___defaultXMaxSpeed_12; }
	inline void set_defaultXMaxSpeed_12(float value)
	{
		___defaultXMaxSpeed_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTSCRIPT_T3129111103_H
#ifndef PLAYMANAGER_T4074907982_H
#define PLAYMANAGER_T4074907982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayManager
struct  PlayManager_t4074907982  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator[] PlayManager::playerGroup
	AnimatorU5BU5D_t608880338* ___playerGroup_4;
	// System.String[] PlayManager::animClipNameGroup
	StringU5BU5D_t1281789340* ___animClipNameGroup_5;
	// System.Int32 PlayManager::currentNumber
	int32_t ___currentNumber_6;

public:
	inline static int32_t get_offset_of_playerGroup_4() { return static_cast<int32_t>(offsetof(PlayManager_t4074907982, ___playerGroup_4)); }
	inline AnimatorU5BU5D_t608880338* get_playerGroup_4() const { return ___playerGroup_4; }
	inline AnimatorU5BU5D_t608880338** get_address_of_playerGroup_4() { return &___playerGroup_4; }
	inline void set_playerGroup_4(AnimatorU5BU5D_t608880338* value)
	{
		___playerGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerGroup_4), value);
	}

	inline static int32_t get_offset_of_animClipNameGroup_5() { return static_cast<int32_t>(offsetof(PlayManager_t4074907982, ___animClipNameGroup_5)); }
	inline StringU5BU5D_t1281789340* get_animClipNameGroup_5() const { return ___animClipNameGroup_5; }
	inline StringU5BU5D_t1281789340** get_address_of_animClipNameGroup_5() { return &___animClipNameGroup_5; }
	inline void set_animClipNameGroup_5(StringU5BU5D_t1281789340* value)
	{
		___animClipNameGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&___animClipNameGroup_5), value);
	}

	inline static int32_t get_offset_of_currentNumber_6() { return static_cast<int32_t>(offsetof(PlayManager_t4074907982, ___currentNumber_6)); }
	inline int32_t get_currentNumber_6() const { return ___currentNumber_6; }
	inline int32_t* get_address_of_currentNumber_6() { return &___currentNumber_6; }
	inline void set_currentNumber_6(int32_t value)
	{
		___currentNumber_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMANAGER_T4074907982_H
#ifndef SWITCHSCENE_T643452349_H
#define SWITCHSCENE_T643452349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwitchScene
struct  SwitchScene_t643452349  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHSCENE_T643452349_H
#ifndef ABSTRACTTARGETFOLLOWER_T1919708159_H
#define ABSTRACTTARGETFOLLOWER_T1919708159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.AbstractTargetFollower
struct  AbstractTargetFollower_t1919708159  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::m_Target
	Transform_t3600365921 * ___m_Target_4;
	// System.Boolean UnityStandardAssets.Cameras.AbstractTargetFollower::m_AutoTargetPlayer
	bool ___m_AutoTargetPlayer_5;
	// UnityStandardAssets.Cameras.AbstractTargetFollower/UpdateType UnityStandardAssets.Cameras.AbstractTargetFollower::m_UpdateType
	int32_t ___m_UpdateType_6;
	// UnityEngine.Rigidbody UnityStandardAssets.Cameras.AbstractTargetFollower::targetRigidbody
	Rigidbody_t3916780224 * ___targetRigidbody_7;

public:
	inline static int32_t get_offset_of_m_Target_4() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t1919708159, ___m_Target_4)); }
	inline Transform_t3600365921 * get_m_Target_4() const { return ___m_Target_4; }
	inline Transform_t3600365921 ** get_address_of_m_Target_4() { return &___m_Target_4; }
	inline void set_m_Target_4(Transform_t3600365921 * value)
	{
		___m_Target_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_4), value);
	}

	inline static int32_t get_offset_of_m_AutoTargetPlayer_5() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t1919708159, ___m_AutoTargetPlayer_5)); }
	inline bool get_m_AutoTargetPlayer_5() const { return ___m_AutoTargetPlayer_5; }
	inline bool* get_address_of_m_AutoTargetPlayer_5() { return &___m_AutoTargetPlayer_5; }
	inline void set_m_AutoTargetPlayer_5(bool value)
	{
		___m_AutoTargetPlayer_5 = value;
	}

	inline static int32_t get_offset_of_m_UpdateType_6() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t1919708159, ___m_UpdateType_6)); }
	inline int32_t get_m_UpdateType_6() const { return ___m_UpdateType_6; }
	inline int32_t* get_address_of_m_UpdateType_6() { return &___m_UpdateType_6; }
	inline void set_m_UpdateType_6(int32_t value)
	{
		___m_UpdateType_6 = value;
	}

	inline static int32_t get_offset_of_targetRigidbody_7() { return static_cast<int32_t>(offsetof(AbstractTargetFollower_t1919708159, ___targetRigidbody_7)); }
	inline Rigidbody_t3916780224 * get_targetRigidbody_7() const { return ___targetRigidbody_7; }
	inline Rigidbody_t3916780224 ** get_address_of_targetRigidbody_7() { return &___targetRigidbody_7; }
	inline void set_targetRigidbody_7(Rigidbody_t3916780224 * value)
	{
		___targetRigidbody_7 = value;
		Il2CppCodeGenWriteBarrier((&___targetRigidbody_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTARGETFOLLOWER_T1919708159_H
#ifndef PROTECTCAMERAFROMWALLCLIP_T303409715_H
#define PROTECTCAMERAFROMWALLCLIP_T303409715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.ProtectCameraFromWallClip
struct  ProtectCameraFromWallClip_t303409715  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::clipMoveTime
	float ___clipMoveTime_4;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::returnTime
	float ___returnTime_5;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::sphereCastRadius
	float ___sphereCastRadius_6;
	// System.Boolean UnityStandardAssets.Cameras.ProtectCameraFromWallClip::visualiseInEditor
	bool ___visualiseInEditor_7;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::closestDistance
	float ___closestDistance_8;
	// System.Boolean UnityStandardAssets.Cameras.ProtectCameraFromWallClip::<protecting>k__BackingField
	bool ___U3CprotectingU3Ek__BackingField_9;
	// System.String UnityStandardAssets.Cameras.ProtectCameraFromWallClip::dontClipTag
	String_t* ___dontClipTag_10;
	// UnityEngine.Transform UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_Cam
	Transform_t3600365921 * ___m_Cam_11;
	// UnityEngine.Transform UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_Pivot
	Transform_t3600365921 * ___m_Pivot_12;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_OriginalDist
	float ___m_OriginalDist_13;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_MoveVelocity
	float ___m_MoveVelocity_14;
	// System.Single UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_CurrentDist
	float ___m_CurrentDist_15;
	// UnityEngine.Ray UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_Ray
	Ray_t3785851493  ___m_Ray_16;
	// UnityEngine.RaycastHit[] UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_Hits
	RaycastHitU5BU5D_t1690781147* ___m_Hits_17;
	// UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer UnityStandardAssets.Cameras.ProtectCameraFromWallClip::m_RayHitComparer
	RayHitComparer_t2205555946 * ___m_RayHitComparer_18;

public:
	inline static int32_t get_offset_of_clipMoveTime_4() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___clipMoveTime_4)); }
	inline float get_clipMoveTime_4() const { return ___clipMoveTime_4; }
	inline float* get_address_of_clipMoveTime_4() { return &___clipMoveTime_4; }
	inline void set_clipMoveTime_4(float value)
	{
		___clipMoveTime_4 = value;
	}

	inline static int32_t get_offset_of_returnTime_5() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___returnTime_5)); }
	inline float get_returnTime_5() const { return ___returnTime_5; }
	inline float* get_address_of_returnTime_5() { return &___returnTime_5; }
	inline void set_returnTime_5(float value)
	{
		___returnTime_5 = value;
	}

	inline static int32_t get_offset_of_sphereCastRadius_6() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___sphereCastRadius_6)); }
	inline float get_sphereCastRadius_6() const { return ___sphereCastRadius_6; }
	inline float* get_address_of_sphereCastRadius_6() { return &___sphereCastRadius_6; }
	inline void set_sphereCastRadius_6(float value)
	{
		___sphereCastRadius_6 = value;
	}

	inline static int32_t get_offset_of_visualiseInEditor_7() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___visualiseInEditor_7)); }
	inline bool get_visualiseInEditor_7() const { return ___visualiseInEditor_7; }
	inline bool* get_address_of_visualiseInEditor_7() { return &___visualiseInEditor_7; }
	inline void set_visualiseInEditor_7(bool value)
	{
		___visualiseInEditor_7 = value;
	}

	inline static int32_t get_offset_of_closestDistance_8() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___closestDistance_8)); }
	inline float get_closestDistance_8() const { return ___closestDistance_8; }
	inline float* get_address_of_closestDistance_8() { return &___closestDistance_8; }
	inline void set_closestDistance_8(float value)
	{
		___closestDistance_8 = value;
	}

	inline static int32_t get_offset_of_U3CprotectingU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___U3CprotectingU3Ek__BackingField_9)); }
	inline bool get_U3CprotectingU3Ek__BackingField_9() const { return ___U3CprotectingU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CprotectingU3Ek__BackingField_9() { return &___U3CprotectingU3Ek__BackingField_9; }
	inline void set_U3CprotectingU3Ek__BackingField_9(bool value)
	{
		___U3CprotectingU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_dontClipTag_10() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___dontClipTag_10)); }
	inline String_t* get_dontClipTag_10() const { return ___dontClipTag_10; }
	inline String_t** get_address_of_dontClipTag_10() { return &___dontClipTag_10; }
	inline void set_dontClipTag_10(String_t* value)
	{
		___dontClipTag_10 = value;
		Il2CppCodeGenWriteBarrier((&___dontClipTag_10), value);
	}

	inline static int32_t get_offset_of_m_Cam_11() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_Cam_11)); }
	inline Transform_t3600365921 * get_m_Cam_11() const { return ___m_Cam_11; }
	inline Transform_t3600365921 ** get_address_of_m_Cam_11() { return &___m_Cam_11; }
	inline void set_m_Cam_11(Transform_t3600365921 * value)
	{
		___m_Cam_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_11), value);
	}

	inline static int32_t get_offset_of_m_Pivot_12() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_Pivot_12)); }
	inline Transform_t3600365921 * get_m_Pivot_12() const { return ___m_Pivot_12; }
	inline Transform_t3600365921 ** get_address_of_m_Pivot_12() { return &___m_Pivot_12; }
	inline void set_m_Pivot_12(Transform_t3600365921 * value)
	{
		___m_Pivot_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pivot_12), value);
	}

	inline static int32_t get_offset_of_m_OriginalDist_13() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_OriginalDist_13)); }
	inline float get_m_OriginalDist_13() const { return ___m_OriginalDist_13; }
	inline float* get_address_of_m_OriginalDist_13() { return &___m_OriginalDist_13; }
	inline void set_m_OriginalDist_13(float value)
	{
		___m_OriginalDist_13 = value;
	}

	inline static int32_t get_offset_of_m_MoveVelocity_14() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_MoveVelocity_14)); }
	inline float get_m_MoveVelocity_14() const { return ___m_MoveVelocity_14; }
	inline float* get_address_of_m_MoveVelocity_14() { return &___m_MoveVelocity_14; }
	inline void set_m_MoveVelocity_14(float value)
	{
		___m_MoveVelocity_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentDist_15() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_CurrentDist_15)); }
	inline float get_m_CurrentDist_15() const { return ___m_CurrentDist_15; }
	inline float* get_address_of_m_CurrentDist_15() { return &___m_CurrentDist_15; }
	inline void set_m_CurrentDist_15(float value)
	{
		___m_CurrentDist_15 = value;
	}

	inline static int32_t get_offset_of_m_Ray_16() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_Ray_16)); }
	inline Ray_t3785851493  get_m_Ray_16() const { return ___m_Ray_16; }
	inline Ray_t3785851493 * get_address_of_m_Ray_16() { return &___m_Ray_16; }
	inline void set_m_Ray_16(Ray_t3785851493  value)
	{
		___m_Ray_16 = value;
	}

	inline static int32_t get_offset_of_m_Hits_17() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_Hits_17)); }
	inline RaycastHitU5BU5D_t1690781147* get_m_Hits_17() const { return ___m_Hits_17; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_m_Hits_17() { return &___m_Hits_17; }
	inline void set_m_Hits_17(RaycastHitU5BU5D_t1690781147* value)
	{
		___m_Hits_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hits_17), value);
	}

	inline static int32_t get_offset_of_m_RayHitComparer_18() { return static_cast<int32_t>(offsetof(ProtectCameraFromWallClip_t303409715, ___m_RayHitComparer_18)); }
	inline RayHitComparer_t2205555946 * get_m_RayHitComparer_18() const { return ___m_RayHitComparer_18; }
	inline RayHitComparer_t2205555946 ** get_address_of_m_RayHitComparer_18() { return &___m_RayHitComparer_18; }
	inline void set_m_RayHitComparer_18(RayHitComparer_t2205555946 * value)
	{
		___m_RayHitComparer_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_RayHitComparer_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTCAMERAFROMWALLCLIP_T303409715_H
#ifndef FIRSTPERSONCONTROLLER_T2020989554_H
#define FIRSTPERSONCONTROLLER_T2020989554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.FirstPersonController
struct  FirstPersonController_t2020989554  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_IsWalking
	bool ___m_IsWalking_4;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_WalkSpeed
	float ___m_WalkSpeed_5;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_RunSpeed
	float ___m_RunSpeed_6;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_RunstepLenghten
	float ___m_RunstepLenghten_7;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpSpeed
	float ___m_JumpSpeed_8;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StickToGroundForce
	float ___m_StickToGroundForce_9;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_GravityMultiplier
	float ___m_GravityMultiplier_10;
	// UnityStandardAssets.Characters.FirstPerson.MouseLook UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_MouseLook
	MouseLook_t2859678661 * ___m_MouseLook_11;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_UseFovKick
	bool ___m_UseFovKick_12;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_FovKick
	FOVKick_t120370150 * ___m_FovKick_13;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_UseHeadBob
	bool ___m_UseHeadBob_14;
	// UnityStandardAssets.Utility.CurveControlledBob UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_HeadBob
	CurveControlledBob_t2679313829 * ___m_HeadBob_15;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpBob
	LerpControlledBob_t1895875871 * ___m_JumpBob_16;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StepInterval
	float ___m_StepInterval_17;
	// UnityEngine.AudioClip[] UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_FootstepSounds
	AudioClipU5BU5D_t143221404* ___m_FootstepSounds_18;
	// UnityEngine.AudioClip UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpSound
	AudioClip_t3680889665 * ___m_JumpSound_19;
	// UnityEngine.AudioClip UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_LandSound
	AudioClip_t3680889665 * ___m_LandSound_20;
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Camera
	Camera_t4157153871 * ___m_Camera_21;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Jump
	bool ___m_Jump_22;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_YRotation
	float ___m_YRotation_23;
	// UnityEngine.Vector2 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Input
	Vector2_t2156229523  ___m_Input_24;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_MoveDir
	Vector3_t3722313464  ___m_MoveDir_25;
	// UnityEngine.CharacterController UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_CharacterController
	CharacterController_t1138636865 * ___m_CharacterController_26;
	// UnityEngine.CollisionFlags UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_CollisionFlags
	int32_t ___m_CollisionFlags_27;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_28;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_29;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StepCycle
	float ___m_StepCycle_30;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_NextStep
	float ___m_NextStep_31;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Jumping
	bool ___m_Jumping_32;
	// UnityEngine.AudioSource UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_AudioSource
	AudioSource_t3935305588 * ___m_AudioSource_33;

public:
	inline static int32_t get_offset_of_m_IsWalking_4() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_IsWalking_4)); }
	inline bool get_m_IsWalking_4() const { return ___m_IsWalking_4; }
	inline bool* get_address_of_m_IsWalking_4() { return &___m_IsWalking_4; }
	inline void set_m_IsWalking_4(bool value)
	{
		___m_IsWalking_4 = value;
	}

	inline static int32_t get_offset_of_m_WalkSpeed_5() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_WalkSpeed_5)); }
	inline float get_m_WalkSpeed_5() const { return ___m_WalkSpeed_5; }
	inline float* get_address_of_m_WalkSpeed_5() { return &___m_WalkSpeed_5; }
	inline void set_m_WalkSpeed_5(float value)
	{
		___m_WalkSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_RunSpeed_6() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_RunSpeed_6)); }
	inline float get_m_RunSpeed_6() const { return ___m_RunSpeed_6; }
	inline float* get_address_of_m_RunSpeed_6() { return &___m_RunSpeed_6; }
	inline void set_m_RunSpeed_6(float value)
	{
		___m_RunSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_RunstepLenghten_7() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_RunstepLenghten_7)); }
	inline float get_m_RunstepLenghten_7() const { return ___m_RunstepLenghten_7; }
	inline float* get_address_of_m_RunstepLenghten_7() { return &___m_RunstepLenghten_7; }
	inline void set_m_RunstepLenghten_7(float value)
	{
		___m_RunstepLenghten_7 = value;
	}

	inline static int32_t get_offset_of_m_JumpSpeed_8() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_JumpSpeed_8)); }
	inline float get_m_JumpSpeed_8() const { return ___m_JumpSpeed_8; }
	inline float* get_address_of_m_JumpSpeed_8() { return &___m_JumpSpeed_8; }
	inline void set_m_JumpSpeed_8(float value)
	{
		___m_JumpSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_StickToGroundForce_9() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_StickToGroundForce_9)); }
	inline float get_m_StickToGroundForce_9() const { return ___m_StickToGroundForce_9; }
	inline float* get_address_of_m_StickToGroundForce_9() { return &___m_StickToGroundForce_9; }
	inline void set_m_StickToGroundForce_9(float value)
	{
		___m_StickToGroundForce_9 = value;
	}

	inline static int32_t get_offset_of_m_GravityMultiplier_10() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_GravityMultiplier_10)); }
	inline float get_m_GravityMultiplier_10() const { return ___m_GravityMultiplier_10; }
	inline float* get_address_of_m_GravityMultiplier_10() { return &___m_GravityMultiplier_10; }
	inline void set_m_GravityMultiplier_10(float value)
	{
		___m_GravityMultiplier_10 = value;
	}

	inline static int32_t get_offset_of_m_MouseLook_11() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_MouseLook_11)); }
	inline MouseLook_t2859678661 * get_m_MouseLook_11() const { return ___m_MouseLook_11; }
	inline MouseLook_t2859678661 ** get_address_of_m_MouseLook_11() { return &___m_MouseLook_11; }
	inline void set_m_MouseLook_11(MouseLook_t2859678661 * value)
	{
		___m_MouseLook_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseLook_11), value);
	}

	inline static int32_t get_offset_of_m_UseFovKick_12() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_UseFovKick_12)); }
	inline bool get_m_UseFovKick_12() const { return ___m_UseFovKick_12; }
	inline bool* get_address_of_m_UseFovKick_12() { return &___m_UseFovKick_12; }
	inline void set_m_UseFovKick_12(bool value)
	{
		___m_UseFovKick_12 = value;
	}

	inline static int32_t get_offset_of_m_FovKick_13() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_FovKick_13)); }
	inline FOVKick_t120370150 * get_m_FovKick_13() const { return ___m_FovKick_13; }
	inline FOVKick_t120370150 ** get_address_of_m_FovKick_13() { return &___m_FovKick_13; }
	inline void set_m_FovKick_13(FOVKick_t120370150 * value)
	{
		___m_FovKick_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_FovKick_13), value);
	}

	inline static int32_t get_offset_of_m_UseHeadBob_14() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_UseHeadBob_14)); }
	inline bool get_m_UseHeadBob_14() const { return ___m_UseHeadBob_14; }
	inline bool* get_address_of_m_UseHeadBob_14() { return &___m_UseHeadBob_14; }
	inline void set_m_UseHeadBob_14(bool value)
	{
		___m_UseHeadBob_14 = value;
	}

	inline static int32_t get_offset_of_m_HeadBob_15() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_HeadBob_15)); }
	inline CurveControlledBob_t2679313829 * get_m_HeadBob_15() const { return ___m_HeadBob_15; }
	inline CurveControlledBob_t2679313829 ** get_address_of_m_HeadBob_15() { return &___m_HeadBob_15; }
	inline void set_m_HeadBob_15(CurveControlledBob_t2679313829 * value)
	{
		___m_HeadBob_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_HeadBob_15), value);
	}

	inline static int32_t get_offset_of_m_JumpBob_16() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_JumpBob_16)); }
	inline LerpControlledBob_t1895875871 * get_m_JumpBob_16() const { return ___m_JumpBob_16; }
	inline LerpControlledBob_t1895875871 ** get_address_of_m_JumpBob_16() { return &___m_JumpBob_16; }
	inline void set_m_JumpBob_16(LerpControlledBob_t1895875871 * value)
	{
		___m_JumpBob_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_JumpBob_16), value);
	}

	inline static int32_t get_offset_of_m_StepInterval_17() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_StepInterval_17)); }
	inline float get_m_StepInterval_17() const { return ___m_StepInterval_17; }
	inline float* get_address_of_m_StepInterval_17() { return &___m_StepInterval_17; }
	inline void set_m_StepInterval_17(float value)
	{
		___m_StepInterval_17 = value;
	}

	inline static int32_t get_offset_of_m_FootstepSounds_18() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_FootstepSounds_18)); }
	inline AudioClipU5BU5D_t143221404* get_m_FootstepSounds_18() const { return ___m_FootstepSounds_18; }
	inline AudioClipU5BU5D_t143221404** get_address_of_m_FootstepSounds_18() { return &___m_FootstepSounds_18; }
	inline void set_m_FootstepSounds_18(AudioClipU5BU5D_t143221404* value)
	{
		___m_FootstepSounds_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_FootstepSounds_18), value);
	}

	inline static int32_t get_offset_of_m_JumpSound_19() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_JumpSound_19)); }
	inline AudioClip_t3680889665 * get_m_JumpSound_19() const { return ___m_JumpSound_19; }
	inline AudioClip_t3680889665 ** get_address_of_m_JumpSound_19() { return &___m_JumpSound_19; }
	inline void set_m_JumpSound_19(AudioClip_t3680889665 * value)
	{
		___m_JumpSound_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_JumpSound_19), value);
	}

	inline static int32_t get_offset_of_m_LandSound_20() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_LandSound_20)); }
	inline AudioClip_t3680889665 * get_m_LandSound_20() const { return ___m_LandSound_20; }
	inline AudioClip_t3680889665 ** get_address_of_m_LandSound_20() { return &___m_LandSound_20; }
	inline void set_m_LandSound_20(AudioClip_t3680889665 * value)
	{
		___m_LandSound_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_LandSound_20), value);
	}

	inline static int32_t get_offset_of_m_Camera_21() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Camera_21)); }
	inline Camera_t4157153871 * get_m_Camera_21() const { return ___m_Camera_21; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_21() { return &___m_Camera_21; }
	inline void set_m_Camera_21(Camera_t4157153871 * value)
	{
		___m_Camera_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_21), value);
	}

	inline static int32_t get_offset_of_m_Jump_22() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Jump_22)); }
	inline bool get_m_Jump_22() const { return ___m_Jump_22; }
	inline bool* get_address_of_m_Jump_22() { return &___m_Jump_22; }
	inline void set_m_Jump_22(bool value)
	{
		___m_Jump_22 = value;
	}

	inline static int32_t get_offset_of_m_YRotation_23() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_YRotation_23)); }
	inline float get_m_YRotation_23() const { return ___m_YRotation_23; }
	inline float* get_address_of_m_YRotation_23() { return &___m_YRotation_23; }
	inline void set_m_YRotation_23(float value)
	{
		___m_YRotation_23 = value;
	}

	inline static int32_t get_offset_of_m_Input_24() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Input_24)); }
	inline Vector2_t2156229523  get_m_Input_24() const { return ___m_Input_24; }
	inline Vector2_t2156229523 * get_address_of_m_Input_24() { return &___m_Input_24; }
	inline void set_m_Input_24(Vector2_t2156229523  value)
	{
		___m_Input_24 = value;
	}

	inline static int32_t get_offset_of_m_MoveDir_25() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_MoveDir_25)); }
	inline Vector3_t3722313464  get_m_MoveDir_25() const { return ___m_MoveDir_25; }
	inline Vector3_t3722313464 * get_address_of_m_MoveDir_25() { return &___m_MoveDir_25; }
	inline void set_m_MoveDir_25(Vector3_t3722313464  value)
	{
		___m_MoveDir_25 = value;
	}

	inline static int32_t get_offset_of_m_CharacterController_26() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_CharacterController_26)); }
	inline CharacterController_t1138636865 * get_m_CharacterController_26() const { return ___m_CharacterController_26; }
	inline CharacterController_t1138636865 ** get_address_of_m_CharacterController_26() { return &___m_CharacterController_26; }
	inline void set_m_CharacterController_26(CharacterController_t1138636865 * value)
	{
		___m_CharacterController_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_CharacterController_26), value);
	}

	inline static int32_t get_offset_of_m_CollisionFlags_27() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_CollisionFlags_27)); }
	inline int32_t get_m_CollisionFlags_27() const { return ___m_CollisionFlags_27; }
	inline int32_t* get_address_of_m_CollisionFlags_27() { return &___m_CollisionFlags_27; }
	inline void set_m_CollisionFlags_27(int32_t value)
	{
		___m_CollisionFlags_27 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_28() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_PreviouslyGrounded_28)); }
	inline bool get_m_PreviouslyGrounded_28() const { return ___m_PreviouslyGrounded_28; }
	inline bool* get_address_of_m_PreviouslyGrounded_28() { return &___m_PreviouslyGrounded_28; }
	inline void set_m_PreviouslyGrounded_28(bool value)
	{
		___m_PreviouslyGrounded_28 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_29() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_OriginalCameraPosition_29)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_29() const { return ___m_OriginalCameraPosition_29; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_29() { return &___m_OriginalCameraPosition_29; }
	inline void set_m_OriginalCameraPosition_29(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_29 = value;
	}

	inline static int32_t get_offset_of_m_StepCycle_30() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_StepCycle_30)); }
	inline float get_m_StepCycle_30() const { return ___m_StepCycle_30; }
	inline float* get_address_of_m_StepCycle_30() { return &___m_StepCycle_30; }
	inline void set_m_StepCycle_30(float value)
	{
		___m_StepCycle_30 = value;
	}

	inline static int32_t get_offset_of_m_NextStep_31() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_NextStep_31)); }
	inline float get_m_NextStep_31() const { return ___m_NextStep_31; }
	inline float* get_address_of_m_NextStep_31() { return &___m_NextStep_31; }
	inline void set_m_NextStep_31(float value)
	{
		___m_NextStep_31 = value;
	}

	inline static int32_t get_offset_of_m_Jumping_32() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_Jumping_32)); }
	inline bool get_m_Jumping_32() const { return ___m_Jumping_32; }
	inline bool* get_address_of_m_Jumping_32() { return &___m_Jumping_32; }
	inline void set_m_Jumping_32(bool value)
	{
		___m_Jumping_32 = value;
	}

	inline static int32_t get_offset_of_m_AudioSource_33() { return static_cast<int32_t>(offsetof(FirstPersonController_t2020989554, ___m_AudioSource_33)); }
	inline AudioSource_t3935305588 * get_m_AudioSource_33() const { return ___m_AudioSource_33; }
	inline AudioSource_t3935305588 ** get_address_of_m_AudioSource_33() { return &___m_AudioSource_33; }
	inline void set_m_AudioSource_33(AudioSource_t3935305588 * value)
	{
		___m_AudioSource_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_AudioSource_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRSTPERSONCONTROLLER_T2020989554_H
#ifndef HEADBOB_T3275031667_H
#define HEADBOB_T3275031667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.HeadBob
struct  HeadBob_t3275031667  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.HeadBob::Camera
	Camera_t4157153871 * ___Camera_4;
	// UnityStandardAssets.Utility.CurveControlledBob UnityStandardAssets.Characters.FirstPerson.HeadBob::motionBob
	CurveControlledBob_t2679313829 * ___motionBob_5;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Characters.FirstPerson.HeadBob::jumpAndLandingBob
	LerpControlledBob_t1895875871 * ___jumpAndLandingBob_6;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController UnityStandardAssets.Characters.FirstPerson.HeadBob::rigidbodyFirstPersonController
	RigidbodyFirstPersonController_t1207297146 * ___rigidbodyFirstPersonController_7;
	// System.Single UnityStandardAssets.Characters.FirstPerson.HeadBob::StrideInterval
	float ___StrideInterval_8;
	// System.Single UnityStandardAssets.Characters.FirstPerson.HeadBob::RunningStrideLengthen
	float ___RunningStrideLengthen_9;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.HeadBob::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_10;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.HeadBob::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_11;

public:
	inline static int32_t get_offset_of_Camera_4() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___Camera_4)); }
	inline Camera_t4157153871 * get_Camera_4() const { return ___Camera_4; }
	inline Camera_t4157153871 ** get_address_of_Camera_4() { return &___Camera_4; }
	inline void set_Camera_4(Camera_t4157153871 * value)
	{
		___Camera_4 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_4), value);
	}

	inline static int32_t get_offset_of_motionBob_5() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___motionBob_5)); }
	inline CurveControlledBob_t2679313829 * get_motionBob_5() const { return ___motionBob_5; }
	inline CurveControlledBob_t2679313829 ** get_address_of_motionBob_5() { return &___motionBob_5; }
	inline void set_motionBob_5(CurveControlledBob_t2679313829 * value)
	{
		___motionBob_5 = value;
		Il2CppCodeGenWriteBarrier((&___motionBob_5), value);
	}

	inline static int32_t get_offset_of_jumpAndLandingBob_6() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___jumpAndLandingBob_6)); }
	inline LerpControlledBob_t1895875871 * get_jumpAndLandingBob_6() const { return ___jumpAndLandingBob_6; }
	inline LerpControlledBob_t1895875871 ** get_address_of_jumpAndLandingBob_6() { return &___jumpAndLandingBob_6; }
	inline void set_jumpAndLandingBob_6(LerpControlledBob_t1895875871 * value)
	{
		___jumpAndLandingBob_6 = value;
		Il2CppCodeGenWriteBarrier((&___jumpAndLandingBob_6), value);
	}

	inline static int32_t get_offset_of_rigidbodyFirstPersonController_7() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___rigidbodyFirstPersonController_7)); }
	inline RigidbodyFirstPersonController_t1207297146 * get_rigidbodyFirstPersonController_7() const { return ___rigidbodyFirstPersonController_7; }
	inline RigidbodyFirstPersonController_t1207297146 ** get_address_of_rigidbodyFirstPersonController_7() { return &___rigidbodyFirstPersonController_7; }
	inline void set_rigidbodyFirstPersonController_7(RigidbodyFirstPersonController_t1207297146 * value)
	{
		___rigidbodyFirstPersonController_7 = value;
		Il2CppCodeGenWriteBarrier((&___rigidbodyFirstPersonController_7), value);
	}

	inline static int32_t get_offset_of_StrideInterval_8() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___StrideInterval_8)); }
	inline float get_StrideInterval_8() const { return ___StrideInterval_8; }
	inline float* get_address_of_StrideInterval_8() { return &___StrideInterval_8; }
	inline void set_StrideInterval_8(float value)
	{
		___StrideInterval_8 = value;
	}

	inline static int32_t get_offset_of_RunningStrideLengthen_9() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___RunningStrideLengthen_9)); }
	inline float get_RunningStrideLengthen_9() const { return ___RunningStrideLengthen_9; }
	inline float* get_address_of_RunningStrideLengthen_9() { return &___RunningStrideLengthen_9; }
	inline void set_RunningStrideLengthen_9(float value)
	{
		___RunningStrideLengthen_9 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_10() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___m_PreviouslyGrounded_10)); }
	inline bool get_m_PreviouslyGrounded_10() const { return ___m_PreviouslyGrounded_10; }
	inline bool* get_address_of_m_PreviouslyGrounded_10() { return &___m_PreviouslyGrounded_10; }
	inline void set_m_PreviouslyGrounded_10(bool value)
	{
		___m_PreviouslyGrounded_10 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_11() { return static_cast<int32_t>(offsetof(HeadBob_t3275031667, ___m_OriginalCameraPosition_11)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_11() const { return ___m_OriginalCameraPosition_11; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_11() { return &___m_OriginalCameraPosition_11; }
	inline void set_m_OriginalCameraPosition_11(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADBOB_T3275031667_H
#ifndef RIGIDBODYFIRSTPERSONCONTROLLER_T1207297146_H
#define RIGIDBODYFIRSTPERSONCONTROLLER_T1207297146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController
struct  RigidbodyFirstPersonController_t1207297146  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::cam
	Camera_t4157153871 * ___cam_4;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::movementSettings
	MovementSettings_t1096092444 * ___movementSettings_5;
	// UnityStandardAssets.Characters.FirstPerson.MouseLook UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::mouseLook
	MouseLook_t2859678661 * ___mouseLook_6;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::advancedSettings
	AdvancedSettings_t778418834 * ___advancedSettings_7;
	// UnityEngine.Rigidbody UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_RigidBody
	Rigidbody_t3916780224 * ___m_RigidBody_8;
	// UnityEngine.CapsuleCollider UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Capsule
	CapsuleCollider_t197597763 * ___m_Capsule_9;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_YRotation
	float ___m_YRotation_10;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_GroundContactNormal
	Vector3_t3722313464  ___m_GroundContactNormal_11;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Jump
	bool ___m_Jump_12;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_13;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Jumping
	bool ___m_Jumping_14;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_IsGrounded
	bool ___m_IsGrounded_15;

public:
	inline static int32_t get_offset_of_cam_4() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___cam_4)); }
	inline Camera_t4157153871 * get_cam_4() const { return ___cam_4; }
	inline Camera_t4157153871 ** get_address_of_cam_4() { return &___cam_4; }
	inline void set_cam_4(Camera_t4157153871 * value)
	{
		___cam_4 = value;
		Il2CppCodeGenWriteBarrier((&___cam_4), value);
	}

	inline static int32_t get_offset_of_movementSettings_5() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___movementSettings_5)); }
	inline MovementSettings_t1096092444 * get_movementSettings_5() const { return ___movementSettings_5; }
	inline MovementSettings_t1096092444 ** get_address_of_movementSettings_5() { return &___movementSettings_5; }
	inline void set_movementSettings_5(MovementSettings_t1096092444 * value)
	{
		___movementSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___movementSettings_5), value);
	}

	inline static int32_t get_offset_of_mouseLook_6() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___mouseLook_6)); }
	inline MouseLook_t2859678661 * get_mouseLook_6() const { return ___mouseLook_6; }
	inline MouseLook_t2859678661 ** get_address_of_mouseLook_6() { return &___mouseLook_6; }
	inline void set_mouseLook_6(MouseLook_t2859678661 * value)
	{
		___mouseLook_6 = value;
		Il2CppCodeGenWriteBarrier((&___mouseLook_6), value);
	}

	inline static int32_t get_offset_of_advancedSettings_7() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___advancedSettings_7)); }
	inline AdvancedSettings_t778418834 * get_advancedSettings_7() const { return ___advancedSettings_7; }
	inline AdvancedSettings_t778418834 ** get_address_of_advancedSettings_7() { return &___advancedSettings_7; }
	inline void set_advancedSettings_7(AdvancedSettings_t778418834 * value)
	{
		___advancedSettings_7 = value;
		Il2CppCodeGenWriteBarrier((&___advancedSettings_7), value);
	}

	inline static int32_t get_offset_of_m_RigidBody_8() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_RigidBody_8)); }
	inline Rigidbody_t3916780224 * get_m_RigidBody_8() const { return ___m_RigidBody_8; }
	inline Rigidbody_t3916780224 ** get_address_of_m_RigidBody_8() { return &___m_RigidBody_8; }
	inline void set_m_RigidBody_8(Rigidbody_t3916780224 * value)
	{
		___m_RigidBody_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_RigidBody_8), value);
	}

	inline static int32_t get_offset_of_m_Capsule_9() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_Capsule_9)); }
	inline CapsuleCollider_t197597763 * get_m_Capsule_9() const { return ___m_Capsule_9; }
	inline CapsuleCollider_t197597763 ** get_address_of_m_Capsule_9() { return &___m_Capsule_9; }
	inline void set_m_Capsule_9(CapsuleCollider_t197597763 * value)
	{
		___m_Capsule_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Capsule_9), value);
	}

	inline static int32_t get_offset_of_m_YRotation_10() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_YRotation_10)); }
	inline float get_m_YRotation_10() const { return ___m_YRotation_10; }
	inline float* get_address_of_m_YRotation_10() { return &___m_YRotation_10; }
	inline void set_m_YRotation_10(float value)
	{
		___m_YRotation_10 = value;
	}

	inline static int32_t get_offset_of_m_GroundContactNormal_11() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_GroundContactNormal_11)); }
	inline Vector3_t3722313464  get_m_GroundContactNormal_11() const { return ___m_GroundContactNormal_11; }
	inline Vector3_t3722313464 * get_address_of_m_GroundContactNormal_11() { return &___m_GroundContactNormal_11; }
	inline void set_m_GroundContactNormal_11(Vector3_t3722313464  value)
	{
		___m_GroundContactNormal_11 = value;
	}

	inline static int32_t get_offset_of_m_Jump_12() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_Jump_12)); }
	inline bool get_m_Jump_12() const { return ___m_Jump_12; }
	inline bool* get_address_of_m_Jump_12() { return &___m_Jump_12; }
	inline void set_m_Jump_12(bool value)
	{
		___m_Jump_12 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_13() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_PreviouslyGrounded_13)); }
	inline bool get_m_PreviouslyGrounded_13() const { return ___m_PreviouslyGrounded_13; }
	inline bool* get_address_of_m_PreviouslyGrounded_13() { return &___m_PreviouslyGrounded_13; }
	inline void set_m_PreviouslyGrounded_13(bool value)
	{
		___m_PreviouslyGrounded_13 = value;
	}

	inline static int32_t get_offset_of_m_Jumping_14() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_Jumping_14)); }
	inline bool get_m_Jumping_14() const { return ___m_Jumping_14; }
	inline bool* get_address_of_m_Jumping_14() { return &___m_Jumping_14; }
	inline void set_m_Jumping_14(bool value)
	{
		___m_Jumping_14 = value;
	}

	inline static int32_t get_offset_of_m_IsGrounded_15() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1207297146, ___m_IsGrounded_15)); }
	inline bool get_m_IsGrounded_15() const { return ___m_IsGrounded_15; }
	inline bool* get_address_of_m_IsGrounded_15() { return &___m_IsGrounded_15; }
	inline void set_m_IsGrounded_15(bool value)
	{
		___m_IsGrounded_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYFIRSTPERSONCONTROLLER_T1207297146_H
#ifndef AICHARACTERCONTROL_T2972373937_H
#define AICHARACTERCONTROL_T2972373937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.AICharacterControl
struct  AICharacterControl_t2972373937  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AI.NavMeshAgent UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::<agent>k__BackingField
	NavMeshAgent_t1276799816 * ___U3CagentU3Ek__BackingField_4;
	// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::<character>k__BackingField
	ThirdPersonCharacter_t1711070432 * ___U3CcharacterU3Ek__BackingField_5;
	// UnityEngine.Transform UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::target
	Transform_t3600365921 * ___target_6;

public:
	inline static int32_t get_offset_of_U3CagentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AICharacterControl_t2972373937, ___U3CagentU3Ek__BackingField_4)); }
	inline NavMeshAgent_t1276799816 * get_U3CagentU3Ek__BackingField_4() const { return ___U3CagentU3Ek__BackingField_4; }
	inline NavMeshAgent_t1276799816 ** get_address_of_U3CagentU3Ek__BackingField_4() { return &___U3CagentU3Ek__BackingField_4; }
	inline void set_U3CagentU3Ek__BackingField_4(NavMeshAgent_t1276799816 * value)
	{
		___U3CagentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CagentU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcharacterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AICharacterControl_t2972373937, ___U3CcharacterU3Ek__BackingField_5)); }
	inline ThirdPersonCharacter_t1711070432 * get_U3CcharacterU3Ek__BackingField_5() const { return ___U3CcharacterU3Ek__BackingField_5; }
	inline ThirdPersonCharacter_t1711070432 ** get_address_of_U3CcharacterU3Ek__BackingField_5() { return &___U3CcharacterU3Ek__BackingField_5; }
	inline void set_U3CcharacterU3Ek__BackingField_5(ThirdPersonCharacter_t1711070432 * value)
	{
		___U3CcharacterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcharacterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(AICharacterControl_t2972373937, ___target_6)); }
	inline Transform_t3600365921 * get_target_6() const { return ___target_6; }
	inline Transform_t3600365921 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t3600365921 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AICHARACTERCONTROL_T2972373937_H
#ifndef THIRDPERSONCHARACTER_T1711070432_H
#define THIRDPERSONCHARACTER_T1711070432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct  ThirdPersonCharacter_t1711070432  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_MovingTurnSpeed
	float ___m_MovingTurnSpeed_4;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_StationaryTurnSpeed
	float ___m_StationaryTurnSpeed_5;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_JumpPower
	float ___m_JumpPower_6;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GravityMultiplier
	float ___m_GravityMultiplier_7;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_RunCycleLegOffset
	float ___m_RunCycleLegOffset_8;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_MoveSpeedMultiplier
	float ___m_MoveSpeedMultiplier_9;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_AnimSpeedMultiplier
	float ___m_AnimSpeedMultiplier_10;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GroundCheckDistance
	float ___m_GroundCheckDistance_11;
	// UnityEngine.Rigidbody UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_12;
	// UnityEngine.Animator UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Animator
	Animator_t434523843 * ___m_Animator_13;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_IsGrounded
	bool ___m_IsGrounded_14;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_OrigGroundCheckDistance
	float ___m_OrigGroundCheckDistance_15;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_TurnAmount
	float ___m_TurnAmount_17;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_ForwardAmount
	float ___m_ForwardAmount_18;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GroundNormal
	Vector3_t3722313464  ___m_GroundNormal_19;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_CapsuleHeight
	float ___m_CapsuleHeight_20;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_CapsuleCenter
	Vector3_t3722313464  ___m_CapsuleCenter_21;
	// UnityEngine.CapsuleCollider UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Capsule
	CapsuleCollider_t197597763 * ___m_Capsule_22;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Crouching
	bool ___m_Crouching_23;

public:
	inline static int32_t get_offset_of_m_MovingTurnSpeed_4() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_MovingTurnSpeed_4)); }
	inline float get_m_MovingTurnSpeed_4() const { return ___m_MovingTurnSpeed_4; }
	inline float* get_address_of_m_MovingTurnSpeed_4() { return &___m_MovingTurnSpeed_4; }
	inline void set_m_MovingTurnSpeed_4(float value)
	{
		___m_MovingTurnSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_StationaryTurnSpeed_5() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_StationaryTurnSpeed_5)); }
	inline float get_m_StationaryTurnSpeed_5() const { return ___m_StationaryTurnSpeed_5; }
	inline float* get_address_of_m_StationaryTurnSpeed_5() { return &___m_StationaryTurnSpeed_5; }
	inline void set_m_StationaryTurnSpeed_5(float value)
	{
		___m_StationaryTurnSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_JumpPower_6() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_JumpPower_6)); }
	inline float get_m_JumpPower_6() const { return ___m_JumpPower_6; }
	inline float* get_address_of_m_JumpPower_6() { return &___m_JumpPower_6; }
	inline void set_m_JumpPower_6(float value)
	{
		___m_JumpPower_6 = value;
	}

	inline static int32_t get_offset_of_m_GravityMultiplier_7() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_GravityMultiplier_7)); }
	inline float get_m_GravityMultiplier_7() const { return ___m_GravityMultiplier_7; }
	inline float* get_address_of_m_GravityMultiplier_7() { return &___m_GravityMultiplier_7; }
	inline void set_m_GravityMultiplier_7(float value)
	{
		___m_GravityMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_m_RunCycleLegOffset_8() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_RunCycleLegOffset_8)); }
	inline float get_m_RunCycleLegOffset_8() const { return ___m_RunCycleLegOffset_8; }
	inline float* get_address_of_m_RunCycleLegOffset_8() { return &___m_RunCycleLegOffset_8; }
	inline void set_m_RunCycleLegOffset_8(float value)
	{
		___m_RunCycleLegOffset_8 = value;
	}

	inline static int32_t get_offset_of_m_MoveSpeedMultiplier_9() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_MoveSpeedMultiplier_9)); }
	inline float get_m_MoveSpeedMultiplier_9() const { return ___m_MoveSpeedMultiplier_9; }
	inline float* get_address_of_m_MoveSpeedMultiplier_9() { return &___m_MoveSpeedMultiplier_9; }
	inline void set_m_MoveSpeedMultiplier_9(float value)
	{
		___m_MoveSpeedMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_m_AnimSpeedMultiplier_10() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_AnimSpeedMultiplier_10)); }
	inline float get_m_AnimSpeedMultiplier_10() const { return ___m_AnimSpeedMultiplier_10; }
	inline float* get_address_of_m_AnimSpeedMultiplier_10() { return &___m_AnimSpeedMultiplier_10; }
	inline void set_m_AnimSpeedMultiplier_10(float value)
	{
		___m_AnimSpeedMultiplier_10 = value;
	}

	inline static int32_t get_offset_of_m_GroundCheckDistance_11() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_GroundCheckDistance_11)); }
	inline float get_m_GroundCheckDistance_11() const { return ___m_GroundCheckDistance_11; }
	inline float* get_address_of_m_GroundCheckDistance_11() { return &___m_GroundCheckDistance_11; }
	inline void set_m_GroundCheckDistance_11(float value)
	{
		___m_GroundCheckDistance_11 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_12() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Rigidbody_12)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_12() const { return ___m_Rigidbody_12; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_12() { return &___m_Rigidbody_12; }
	inline void set_m_Rigidbody_12(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_12), value);
	}

	inline static int32_t get_offset_of_m_Animator_13() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Animator_13)); }
	inline Animator_t434523843 * get_m_Animator_13() const { return ___m_Animator_13; }
	inline Animator_t434523843 ** get_address_of_m_Animator_13() { return &___m_Animator_13; }
	inline void set_m_Animator_13(Animator_t434523843 * value)
	{
		___m_Animator_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_13), value);
	}

	inline static int32_t get_offset_of_m_IsGrounded_14() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_IsGrounded_14)); }
	inline bool get_m_IsGrounded_14() const { return ___m_IsGrounded_14; }
	inline bool* get_address_of_m_IsGrounded_14() { return &___m_IsGrounded_14; }
	inline void set_m_IsGrounded_14(bool value)
	{
		___m_IsGrounded_14 = value;
	}

	inline static int32_t get_offset_of_m_OrigGroundCheckDistance_15() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_OrigGroundCheckDistance_15)); }
	inline float get_m_OrigGroundCheckDistance_15() const { return ___m_OrigGroundCheckDistance_15; }
	inline float* get_address_of_m_OrigGroundCheckDistance_15() { return &___m_OrigGroundCheckDistance_15; }
	inline void set_m_OrigGroundCheckDistance_15(float value)
	{
		___m_OrigGroundCheckDistance_15 = value;
	}

	inline static int32_t get_offset_of_m_TurnAmount_17() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_TurnAmount_17)); }
	inline float get_m_TurnAmount_17() const { return ___m_TurnAmount_17; }
	inline float* get_address_of_m_TurnAmount_17() { return &___m_TurnAmount_17; }
	inline void set_m_TurnAmount_17(float value)
	{
		___m_TurnAmount_17 = value;
	}

	inline static int32_t get_offset_of_m_ForwardAmount_18() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_ForwardAmount_18)); }
	inline float get_m_ForwardAmount_18() const { return ___m_ForwardAmount_18; }
	inline float* get_address_of_m_ForwardAmount_18() { return &___m_ForwardAmount_18; }
	inline void set_m_ForwardAmount_18(float value)
	{
		___m_ForwardAmount_18 = value;
	}

	inline static int32_t get_offset_of_m_GroundNormal_19() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_GroundNormal_19)); }
	inline Vector3_t3722313464  get_m_GroundNormal_19() const { return ___m_GroundNormal_19; }
	inline Vector3_t3722313464 * get_address_of_m_GroundNormal_19() { return &___m_GroundNormal_19; }
	inline void set_m_GroundNormal_19(Vector3_t3722313464  value)
	{
		___m_GroundNormal_19 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleHeight_20() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_CapsuleHeight_20)); }
	inline float get_m_CapsuleHeight_20() const { return ___m_CapsuleHeight_20; }
	inline float* get_address_of_m_CapsuleHeight_20() { return &___m_CapsuleHeight_20; }
	inline void set_m_CapsuleHeight_20(float value)
	{
		___m_CapsuleHeight_20 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleCenter_21() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_CapsuleCenter_21)); }
	inline Vector3_t3722313464  get_m_CapsuleCenter_21() const { return ___m_CapsuleCenter_21; }
	inline Vector3_t3722313464 * get_address_of_m_CapsuleCenter_21() { return &___m_CapsuleCenter_21; }
	inline void set_m_CapsuleCenter_21(Vector3_t3722313464  value)
	{
		___m_CapsuleCenter_21 = value;
	}

	inline static int32_t get_offset_of_m_Capsule_22() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Capsule_22)); }
	inline CapsuleCollider_t197597763 * get_m_Capsule_22() const { return ___m_Capsule_22; }
	inline CapsuleCollider_t197597763 ** get_address_of_m_Capsule_22() { return &___m_Capsule_22; }
	inline void set_m_Capsule_22(CapsuleCollider_t197597763 * value)
	{
		___m_Capsule_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Capsule_22), value);
	}

	inline static int32_t get_offset_of_m_Crouching_23() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Crouching_23)); }
	inline bool get_m_Crouching_23() const { return ___m_Crouching_23; }
	inline bool* get_address_of_m_Crouching_23() { return &___m_Crouching_23; }
	inline void set_m_Crouching_23(bool value)
	{
		___m_Crouching_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONCHARACTER_T1711070432_H
#ifndef THIRDPERSONUSERCONTROL_T1527285130_H
#define THIRDPERSONUSERCONTROL_T1527285130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl
struct  ThirdPersonUserControl_t1527285130  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Character
	ThirdPersonCharacter_t1711070432 * ___m_Character_4;
	// UnityEngine.Transform UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Cam
	Transform_t3600365921 * ___m_Cam_5;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_CamForward
	Vector3_t3722313464  ___m_CamForward_6;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Move
	Vector3_t3722313464  ___m_Move_7;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Jump
	bool ___m_Jump_8;

public:
	inline static int32_t get_offset_of_m_Character_4() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Character_4)); }
	inline ThirdPersonCharacter_t1711070432 * get_m_Character_4() const { return ___m_Character_4; }
	inline ThirdPersonCharacter_t1711070432 ** get_address_of_m_Character_4() { return &___m_Character_4; }
	inline void set_m_Character_4(ThirdPersonCharacter_t1711070432 * value)
	{
		___m_Character_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Character_4), value);
	}

	inline static int32_t get_offset_of_m_Cam_5() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Cam_5)); }
	inline Transform_t3600365921 * get_m_Cam_5() const { return ___m_Cam_5; }
	inline Transform_t3600365921 ** get_address_of_m_Cam_5() { return &___m_Cam_5; }
	inline void set_m_Cam_5(Transform_t3600365921 * value)
	{
		___m_Cam_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_5), value);
	}

	inline static int32_t get_offset_of_m_CamForward_6() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_CamForward_6)); }
	inline Vector3_t3722313464  get_m_CamForward_6() const { return ___m_CamForward_6; }
	inline Vector3_t3722313464 * get_address_of_m_CamForward_6() { return &___m_CamForward_6; }
	inline void set_m_CamForward_6(Vector3_t3722313464  value)
	{
		___m_CamForward_6 = value;
	}

	inline static int32_t get_offset_of_m_Move_7() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Move_7)); }
	inline Vector3_t3722313464  get_m_Move_7() const { return ___m_Move_7; }
	inline Vector3_t3722313464 * get_address_of_m_Move_7() { return &___m_Move_7; }
	inline void set_m_Move_7(Vector3_t3722313464  value)
	{
		___m_Move_7 = value;
	}

	inline static int32_t get_offset_of_m_Jump_8() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Jump_8)); }
	inline bool get_m_Jump_8() const { return ___m_Jump_8; }
	inline bool* get_address_of_m_Jump_8() { return &___m_Jump_8; }
	inline void set_m_Jump_8(bool value)
	{
		___m_Jump_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONUSERCONTROL_T1527285130_H
#ifndef AXISTOUCHBUTTON_T3522881333_H
#define AXISTOUCHBUTTON_T3522881333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct  AxisTouchButton_t3522881333  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisName
	String_t* ___axisName_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisValue
	float ___axisValue_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::responseSpeed
	float ___responseSpeed_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::returnToCentreSpeed
	float ___returnToCentreSpeed_7;
	// UnityStandardAssets.CrossPlatformInput.AxisTouchButton UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_PairedWith
	AxisTouchButton_t3522881333 * ___m_PairedWith_8;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_Axis
	VirtualAxis_t4087348596 * ___m_Axis_9;

public:
	inline static int32_t get_offset_of_axisName_4() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___axisName_4)); }
	inline String_t* get_axisName_4() const { return ___axisName_4; }
	inline String_t** get_address_of_axisName_4() { return &___axisName_4; }
	inline void set_axisName_4(String_t* value)
	{
		___axisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_4), value);
	}

	inline static int32_t get_offset_of_axisValue_5() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___axisValue_5)); }
	inline float get_axisValue_5() const { return ___axisValue_5; }
	inline float* get_address_of_axisValue_5() { return &___axisValue_5; }
	inline void set_axisValue_5(float value)
	{
		___axisValue_5 = value;
	}

	inline static int32_t get_offset_of_responseSpeed_6() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___responseSpeed_6)); }
	inline float get_responseSpeed_6() const { return ___responseSpeed_6; }
	inline float* get_address_of_responseSpeed_6() { return &___responseSpeed_6; }
	inline void set_responseSpeed_6(float value)
	{
		___responseSpeed_6 = value;
	}

	inline static int32_t get_offset_of_returnToCentreSpeed_7() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___returnToCentreSpeed_7)); }
	inline float get_returnToCentreSpeed_7() const { return ___returnToCentreSpeed_7; }
	inline float* get_address_of_returnToCentreSpeed_7() { return &___returnToCentreSpeed_7; }
	inline void set_returnToCentreSpeed_7(float value)
	{
		___returnToCentreSpeed_7 = value;
	}

	inline static int32_t get_offset_of_m_PairedWith_8() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___m_PairedWith_8)); }
	inline AxisTouchButton_t3522881333 * get_m_PairedWith_8() const { return ___m_PairedWith_8; }
	inline AxisTouchButton_t3522881333 ** get_address_of_m_PairedWith_8() { return &___m_PairedWith_8; }
	inline void set_m_PairedWith_8(AxisTouchButton_t3522881333 * value)
	{
		___m_PairedWith_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_PairedWith_8), value);
	}

	inline static int32_t get_offset_of_m_Axis_9() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___m_Axis_9)); }
	inline VirtualAxis_t4087348596 * get_m_Axis_9() const { return ___m_Axis_9; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_Axis_9() { return &___m_Axis_9; }
	inline void set_m_Axis_9(VirtualAxis_t4087348596 * value)
	{
		___m_Axis_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Axis_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTOUCHBUTTON_T3522881333_H
#ifndef BUTTONHANDLER_T823762219_H
#define BUTTONHANDLER_T823762219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct  ButtonHandler_t823762219  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.ButtonHandler::Name
	String_t* ___Name_4;

public:
	inline static int32_t get_offset_of_Name_4() { return static_cast<int32_t>(offsetof(ButtonHandler_t823762219, ___Name_4)); }
	inline String_t* get_Name_4() const { return ___Name_4; }
	inline String_t** get_address_of_Name_4() { return &___Name_4; }
	inline void set_Name_4(String_t* value)
	{
		___Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___Name_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONHANDLER_T823762219_H
#ifndef INPUTAXISSCROLLBAR_T457958266_H
#define INPUTAXISSCROLLBAR_T457958266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct  InputAxisScrollbar_t457958266  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::axis
	String_t* ___axis_4;

public:
	inline static int32_t get_offset_of_axis_4() { return static_cast<int32_t>(offsetof(InputAxisScrollbar_t457958266, ___axis_4)); }
	inline String_t* get_axis_4() const { return ___axis_4; }
	inline String_t** get_address_of_axis_4() { return &___axis_4; }
	inline void set_axis_4(String_t* value)
	{
		___axis_4 = value;
		Il2CppCodeGenWriteBarrier((&___axis_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTAXISSCROLLBAR_T457958266_H
#ifndef JOYSTICK_T2204371675_H
#define JOYSTICK_T2204371675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick
struct  Joystick_t2204371675  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick::MovementRange
	int32_t ___MovementRange_4;
	// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption UnityStandardAssets.CrossPlatformInput.Joystick::axesToUse
	int32_t ___axesToUse_5;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::horizontalAxisName
	String_t* ___horizontalAxisName_6;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::verticalAxisName
	String_t* ___verticalAxisName_7;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.Joystick::m_StartPos
	Vector3_t3722313464  ___m_StartPos_8;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseX
	bool ___m_UseX_9;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseY
	bool ___m_UseY_10;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_HorizontalVirtualAxis
	VirtualAxis_t4087348596 * ___m_HorizontalVirtualAxis_11;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_VerticalVirtualAxis
	VirtualAxis_t4087348596 * ___m_VerticalVirtualAxis_12;

public:
	inline static int32_t get_offset_of_MovementRange_4() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___MovementRange_4)); }
	inline int32_t get_MovementRange_4() const { return ___MovementRange_4; }
	inline int32_t* get_address_of_MovementRange_4() { return &___MovementRange_4; }
	inline void set_MovementRange_4(int32_t value)
	{
		___MovementRange_4 = value;
	}

	inline static int32_t get_offset_of_axesToUse_5() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___axesToUse_5)); }
	inline int32_t get_axesToUse_5() const { return ___axesToUse_5; }
	inline int32_t* get_address_of_axesToUse_5() { return &___axesToUse_5; }
	inline void set_axesToUse_5(int32_t value)
	{
		___axesToUse_5 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_6() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___horizontalAxisName_6)); }
	inline String_t* get_horizontalAxisName_6() const { return ___horizontalAxisName_6; }
	inline String_t** get_address_of_horizontalAxisName_6() { return &___horizontalAxisName_6; }
	inline void set_horizontalAxisName_6(String_t* value)
	{
		___horizontalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_6), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_7() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___verticalAxisName_7)); }
	inline String_t* get_verticalAxisName_7() const { return ___verticalAxisName_7; }
	inline String_t** get_address_of_verticalAxisName_7() { return &___verticalAxisName_7; }
	inline void set_verticalAxisName_7(String_t* value)
	{
		___verticalAxisName_7 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_7), value);
	}

	inline static int32_t get_offset_of_m_StartPos_8() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_StartPos_8)); }
	inline Vector3_t3722313464  get_m_StartPos_8() const { return ___m_StartPos_8; }
	inline Vector3_t3722313464 * get_address_of_m_StartPos_8() { return &___m_StartPos_8; }
	inline void set_m_StartPos_8(Vector3_t3722313464  value)
	{
		___m_StartPos_8 = value;
	}

	inline static int32_t get_offset_of_m_UseX_9() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_UseX_9)); }
	inline bool get_m_UseX_9() const { return ___m_UseX_9; }
	inline bool* get_address_of_m_UseX_9() { return &___m_UseX_9; }
	inline void set_m_UseX_9(bool value)
	{
		___m_UseX_9 = value;
	}

	inline static int32_t get_offset_of_m_UseY_10() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_UseY_10)); }
	inline bool get_m_UseY_10() const { return ___m_UseY_10; }
	inline bool* get_address_of_m_UseY_10() { return &___m_UseY_10; }
	inline void set_m_UseY_10(bool value)
	{
		___m_UseY_10 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_11() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_HorizontalVirtualAxis_11)); }
	inline VirtualAxis_t4087348596 * get_m_HorizontalVirtualAxis_11() const { return ___m_HorizontalVirtualAxis_11; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_HorizontalVirtualAxis_11() { return &___m_HorizontalVirtualAxis_11; }
	inline void set_m_HorizontalVirtualAxis_11(VirtualAxis_t4087348596 * value)
	{
		___m_HorizontalVirtualAxis_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_11), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_12() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_VerticalVirtualAxis_12)); }
	inline VirtualAxis_t4087348596 * get_m_VerticalVirtualAxis_12() const { return ___m_VerticalVirtualAxis_12; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_VerticalVirtualAxis_12() { return &___m_VerticalVirtualAxis_12; }
	inline void set_m_VerticalVirtualAxis_12(VirtualAxis_t4087348596 * value)
	{
		___m_VerticalVirtualAxis_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T2204371675_H
#ifndef MOBILECONTROLRIG_T1964600252_H
#define MOBILECONTROLRIG_T1964600252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct  MobileControlRig_t1964600252  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILECONTROLRIG_T1964600252_H
#ifndef TILTINPUT_T1639936653_H
#define TILTINPUT_T1639936653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct  TiltInput_t1639936653  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping UnityStandardAssets.CrossPlatformInput.TiltInput::mapping
	AxisMapping_t3982445645 * ___mapping_4;
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions UnityStandardAssets.CrossPlatformInput.TiltInput::tiltAroundAxis
	int32_t ___tiltAroundAxis_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::fullTiltAngle
	float ___fullTiltAngle_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::centreAngleOffset
	float ___centreAngleOffset_7;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TiltInput::m_SteerAxis
	VirtualAxis_t4087348596 * ___m_SteerAxis_8;

public:
	inline static int32_t get_offset_of_mapping_4() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___mapping_4)); }
	inline AxisMapping_t3982445645 * get_mapping_4() const { return ___mapping_4; }
	inline AxisMapping_t3982445645 ** get_address_of_mapping_4() { return &___mapping_4; }
	inline void set_mapping_4(AxisMapping_t3982445645 * value)
	{
		___mapping_4 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_4), value);
	}

	inline static int32_t get_offset_of_tiltAroundAxis_5() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___tiltAroundAxis_5)); }
	inline int32_t get_tiltAroundAxis_5() const { return ___tiltAroundAxis_5; }
	inline int32_t* get_address_of_tiltAroundAxis_5() { return &___tiltAroundAxis_5; }
	inline void set_tiltAroundAxis_5(int32_t value)
	{
		___tiltAroundAxis_5 = value;
	}

	inline static int32_t get_offset_of_fullTiltAngle_6() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___fullTiltAngle_6)); }
	inline float get_fullTiltAngle_6() const { return ___fullTiltAngle_6; }
	inline float* get_address_of_fullTiltAngle_6() { return &___fullTiltAngle_6; }
	inline void set_fullTiltAngle_6(float value)
	{
		___fullTiltAngle_6 = value;
	}

	inline static int32_t get_offset_of_centreAngleOffset_7() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___centreAngleOffset_7)); }
	inline float get_centreAngleOffset_7() const { return ___centreAngleOffset_7; }
	inline float* get_address_of_centreAngleOffset_7() { return &___centreAngleOffset_7; }
	inline void set_centreAngleOffset_7(float value)
	{
		___centreAngleOffset_7 = value;
	}

	inline static int32_t get_offset_of_m_SteerAxis_8() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___m_SteerAxis_8)); }
	inline VirtualAxis_t4087348596 * get_m_SteerAxis_8() const { return ___m_SteerAxis_8; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_SteerAxis_8() { return &___m_SteerAxis_8; }
	inline void set_m_SteerAxis_8(VirtualAxis_t4087348596 * value)
	{
		___m_SteerAxis_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SteerAxis_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTINPUT_T1639936653_H
#ifndef TOUCHPAD_T539039257_H
#define TOUCHPAD_T539039257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad
struct  TouchPad_t539039257  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption UnityStandardAssets.CrossPlatformInput.TouchPad::axesToUse
	int32_t ___axesToUse_4;
	// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle UnityStandardAssets.CrossPlatformInput.TouchPad::controlStyle
	int32_t ___controlStyle_5;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::horizontalAxisName
	String_t* ___horizontalAxisName_6;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::verticalAxisName
	String_t* ___verticalAxisName_7;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Xsensitivity
	float ___Xsensitivity_8;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Ysensitivity
	float ___Ysensitivity_9;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_StartPos
	Vector3_t3722313464  ___m_StartPos_10;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousDelta
	Vector2_t2156229523  ___m_PreviousDelta_11;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_JoytickOutput
	Vector3_t3722313464  ___m_JoytickOutput_12;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseX
	bool ___m_UseX_13;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseY
	bool ___m_UseY_14;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_HorizontalVirtualAxis
	VirtualAxis_t4087348596 * ___m_HorizontalVirtualAxis_15;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_VerticalVirtualAxis
	VirtualAxis_t4087348596 * ___m_VerticalVirtualAxis_16;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_Dragging
	bool ___m_Dragging_17;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Id
	int32_t ___m_Id_18;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousTouchPos
	Vector2_t2156229523  ___m_PreviousTouchPos_19;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Center
	Vector3_t3722313464  ___m_Center_20;
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.TouchPad::m_Image
	Image_t2670269651 * ___m_Image_21;

public:
	inline static int32_t get_offset_of_axesToUse_4() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___axesToUse_4)); }
	inline int32_t get_axesToUse_4() const { return ___axesToUse_4; }
	inline int32_t* get_address_of_axesToUse_4() { return &___axesToUse_4; }
	inline void set_axesToUse_4(int32_t value)
	{
		___axesToUse_4 = value;
	}

	inline static int32_t get_offset_of_controlStyle_5() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___controlStyle_5)); }
	inline int32_t get_controlStyle_5() const { return ___controlStyle_5; }
	inline int32_t* get_address_of_controlStyle_5() { return &___controlStyle_5; }
	inline void set_controlStyle_5(int32_t value)
	{
		___controlStyle_5 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_6() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___horizontalAxisName_6)); }
	inline String_t* get_horizontalAxisName_6() const { return ___horizontalAxisName_6; }
	inline String_t** get_address_of_horizontalAxisName_6() { return &___horizontalAxisName_6; }
	inline void set_horizontalAxisName_6(String_t* value)
	{
		___horizontalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_6), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_7() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___verticalAxisName_7)); }
	inline String_t* get_verticalAxisName_7() const { return ___verticalAxisName_7; }
	inline String_t** get_address_of_verticalAxisName_7() { return &___verticalAxisName_7; }
	inline void set_verticalAxisName_7(String_t* value)
	{
		___verticalAxisName_7 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_7), value);
	}

	inline static int32_t get_offset_of_Xsensitivity_8() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___Xsensitivity_8)); }
	inline float get_Xsensitivity_8() const { return ___Xsensitivity_8; }
	inline float* get_address_of_Xsensitivity_8() { return &___Xsensitivity_8; }
	inline void set_Xsensitivity_8(float value)
	{
		___Xsensitivity_8 = value;
	}

	inline static int32_t get_offset_of_Ysensitivity_9() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___Ysensitivity_9)); }
	inline float get_Ysensitivity_9() const { return ___Ysensitivity_9; }
	inline float* get_address_of_Ysensitivity_9() { return &___Ysensitivity_9; }
	inline void set_Ysensitivity_9(float value)
	{
		___Ysensitivity_9 = value;
	}

	inline static int32_t get_offset_of_m_StartPos_10() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_StartPos_10)); }
	inline Vector3_t3722313464  get_m_StartPos_10() const { return ___m_StartPos_10; }
	inline Vector3_t3722313464 * get_address_of_m_StartPos_10() { return &___m_StartPos_10; }
	inline void set_m_StartPos_10(Vector3_t3722313464  value)
	{
		___m_StartPos_10 = value;
	}

	inline static int32_t get_offset_of_m_PreviousDelta_11() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_PreviousDelta_11)); }
	inline Vector2_t2156229523  get_m_PreviousDelta_11() const { return ___m_PreviousDelta_11; }
	inline Vector2_t2156229523 * get_address_of_m_PreviousDelta_11() { return &___m_PreviousDelta_11; }
	inline void set_m_PreviousDelta_11(Vector2_t2156229523  value)
	{
		___m_PreviousDelta_11 = value;
	}

	inline static int32_t get_offset_of_m_JoytickOutput_12() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_JoytickOutput_12)); }
	inline Vector3_t3722313464  get_m_JoytickOutput_12() const { return ___m_JoytickOutput_12; }
	inline Vector3_t3722313464 * get_address_of_m_JoytickOutput_12() { return &___m_JoytickOutput_12; }
	inline void set_m_JoytickOutput_12(Vector3_t3722313464  value)
	{
		___m_JoytickOutput_12 = value;
	}

	inline static int32_t get_offset_of_m_UseX_13() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_UseX_13)); }
	inline bool get_m_UseX_13() const { return ___m_UseX_13; }
	inline bool* get_address_of_m_UseX_13() { return &___m_UseX_13; }
	inline void set_m_UseX_13(bool value)
	{
		___m_UseX_13 = value;
	}

	inline static int32_t get_offset_of_m_UseY_14() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_UseY_14)); }
	inline bool get_m_UseY_14() const { return ___m_UseY_14; }
	inline bool* get_address_of_m_UseY_14() { return &___m_UseY_14; }
	inline void set_m_UseY_14(bool value)
	{
		___m_UseY_14 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_15() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_HorizontalVirtualAxis_15)); }
	inline VirtualAxis_t4087348596 * get_m_HorizontalVirtualAxis_15() const { return ___m_HorizontalVirtualAxis_15; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_HorizontalVirtualAxis_15() { return &___m_HorizontalVirtualAxis_15; }
	inline void set_m_HorizontalVirtualAxis_15(VirtualAxis_t4087348596 * value)
	{
		___m_HorizontalVirtualAxis_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_15), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_16() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_VerticalVirtualAxis_16)); }
	inline VirtualAxis_t4087348596 * get_m_VerticalVirtualAxis_16() const { return ___m_VerticalVirtualAxis_16; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_VerticalVirtualAxis_16() { return &___m_VerticalVirtualAxis_16; }
	inline void set_m_VerticalVirtualAxis_16(VirtualAxis_t4087348596 * value)
	{
		___m_VerticalVirtualAxis_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_16), value);
	}

	inline static int32_t get_offset_of_m_Dragging_17() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Dragging_17)); }
	inline bool get_m_Dragging_17() const { return ___m_Dragging_17; }
	inline bool* get_address_of_m_Dragging_17() { return &___m_Dragging_17; }
	inline void set_m_Dragging_17(bool value)
	{
		___m_Dragging_17 = value;
	}

	inline static int32_t get_offset_of_m_Id_18() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Id_18)); }
	inline int32_t get_m_Id_18() const { return ___m_Id_18; }
	inline int32_t* get_address_of_m_Id_18() { return &___m_Id_18; }
	inline void set_m_Id_18(int32_t value)
	{
		___m_Id_18 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTouchPos_19() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_PreviousTouchPos_19)); }
	inline Vector2_t2156229523  get_m_PreviousTouchPos_19() const { return ___m_PreviousTouchPos_19; }
	inline Vector2_t2156229523 * get_address_of_m_PreviousTouchPos_19() { return &___m_PreviousTouchPos_19; }
	inline void set_m_PreviousTouchPos_19(Vector2_t2156229523  value)
	{
		___m_PreviousTouchPos_19 = value;
	}

	inline static int32_t get_offset_of_m_Center_20() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Center_20)); }
	inline Vector3_t3722313464  get_m_Center_20() const { return ___m_Center_20; }
	inline Vector3_t3722313464 * get_address_of_m_Center_20() { return &___m_Center_20; }
	inline void set_m_Center_20(Vector3_t3722313464  value)
	{
		___m_Center_20 = value;
	}

	inline static int32_t get_offset_of_m_Image_21() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Image_21)); }
	inline Image_t2670269651 * get_m_Image_21() const { return ___m_Image_21; }
	inline Image_t2670269651 ** get_address_of_m_Image_21() { return &___m_Image_21; }
	inline void set_m_Image_21(Image_t2670269651 * value)
	{
		___m_Image_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_T539039257_H
#ifndef FIRELIGHT_T2068143130_H
#define FIRELIGHT_T2068143130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.FireLight
struct  FireLight_t2068143130  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.FireLight::m_Rnd
	float ___m_Rnd_4;
	// System.Boolean UnityStandardAssets.Effects.FireLight::m_Burning
	bool ___m_Burning_5;
	// UnityEngine.Light UnityStandardAssets.Effects.FireLight::m_Light
	Light_t3756812086 * ___m_Light_6;

public:
	inline static int32_t get_offset_of_m_Rnd_4() { return static_cast<int32_t>(offsetof(FireLight_t2068143130, ___m_Rnd_4)); }
	inline float get_m_Rnd_4() const { return ___m_Rnd_4; }
	inline float* get_address_of_m_Rnd_4() { return &___m_Rnd_4; }
	inline void set_m_Rnd_4(float value)
	{
		___m_Rnd_4 = value;
	}

	inline static int32_t get_offset_of_m_Burning_5() { return static_cast<int32_t>(offsetof(FireLight_t2068143130, ___m_Burning_5)); }
	inline bool get_m_Burning_5() const { return ___m_Burning_5; }
	inline bool* get_address_of_m_Burning_5() { return &___m_Burning_5; }
	inline void set_m_Burning_5(bool value)
	{
		___m_Burning_5 = value;
	}

	inline static int32_t get_offset_of_m_Light_6() { return static_cast<int32_t>(offsetof(FireLight_t2068143130, ___m_Light_6)); }
	inline Light_t3756812086 * get_m_Light_6() const { return ___m_Light_6; }
	inline Light_t3756812086 ** get_address_of_m_Light_6() { return &___m_Light_6; }
	inline void set_m_Light_6(Light_t3756812086 * value)
	{
		___m_Light_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Light_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRELIGHT_T2068143130_H
#ifndef HOSE_T3016386068_H
#define HOSE_T3016386068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.Hose
struct  Hose_t3016386068  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.Hose::maxPower
	float ___maxPower_4;
	// System.Single UnityStandardAssets.Effects.Hose::minPower
	float ___minPower_5;
	// System.Single UnityStandardAssets.Effects.Hose::changeSpeed
	float ___changeSpeed_6;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Effects.Hose::hoseWaterSystems
	ParticleSystemU5BU5D_t3089334924* ___hoseWaterSystems_7;
	// UnityEngine.Renderer UnityStandardAssets.Effects.Hose::systemRenderer
	Renderer_t2627027031 * ___systemRenderer_8;
	// System.Single UnityStandardAssets.Effects.Hose::m_Power
	float ___m_Power_9;

public:
	inline static int32_t get_offset_of_maxPower_4() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___maxPower_4)); }
	inline float get_maxPower_4() const { return ___maxPower_4; }
	inline float* get_address_of_maxPower_4() { return &___maxPower_4; }
	inline void set_maxPower_4(float value)
	{
		___maxPower_4 = value;
	}

	inline static int32_t get_offset_of_minPower_5() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___minPower_5)); }
	inline float get_minPower_5() const { return ___minPower_5; }
	inline float* get_address_of_minPower_5() { return &___minPower_5; }
	inline void set_minPower_5(float value)
	{
		___minPower_5 = value;
	}

	inline static int32_t get_offset_of_changeSpeed_6() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___changeSpeed_6)); }
	inline float get_changeSpeed_6() const { return ___changeSpeed_6; }
	inline float* get_address_of_changeSpeed_6() { return &___changeSpeed_6; }
	inline void set_changeSpeed_6(float value)
	{
		___changeSpeed_6 = value;
	}

	inline static int32_t get_offset_of_hoseWaterSystems_7() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___hoseWaterSystems_7)); }
	inline ParticleSystemU5BU5D_t3089334924* get_hoseWaterSystems_7() const { return ___hoseWaterSystems_7; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_hoseWaterSystems_7() { return &___hoseWaterSystems_7; }
	inline void set_hoseWaterSystems_7(ParticleSystemU5BU5D_t3089334924* value)
	{
		___hoseWaterSystems_7 = value;
		Il2CppCodeGenWriteBarrier((&___hoseWaterSystems_7), value);
	}

	inline static int32_t get_offset_of_systemRenderer_8() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___systemRenderer_8)); }
	inline Renderer_t2627027031 * get_systemRenderer_8() const { return ___systemRenderer_8; }
	inline Renderer_t2627027031 ** get_address_of_systemRenderer_8() { return &___systemRenderer_8; }
	inline void set_systemRenderer_8(Renderer_t2627027031 * value)
	{
		___systemRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___systemRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Power_9() { return static_cast<int32_t>(offsetof(Hose_t3016386068, ___m_Power_9)); }
	inline float get_m_Power_9() const { return ___m_Power_9; }
	inline float* get_address_of_m_Power_9() { return &___m_Power_9; }
	inline void set_m_Power_9(float value)
	{
		___m_Power_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOSE_T3016386068_H
#ifndef PARTICLESYSTEMMULTIPLIER_T2770350653_H
#define PARTICLESYSTEMMULTIPLIER_T2770350653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.ParticleSystemMultiplier
struct  ParticleSystemMultiplier_t2770350653  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.ParticleSystemMultiplier::multiplier
	float ___multiplier_4;

public:
	inline static int32_t get_offset_of_multiplier_4() { return static_cast<int32_t>(offsetof(ParticleSystemMultiplier_t2770350653, ___multiplier_4)); }
	inline float get_multiplier_4() const { return ___multiplier_4; }
	inline float* get_address_of_multiplier_4() { return &___multiplier_4; }
	inline void set_multiplier_4(float value)
	{
		___multiplier_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMMULTIPLIER_T2770350653_H
#ifndef SMOKEPARTICLES_T494565528_H
#define SMOKEPARTICLES_T494565528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.SmokeParticles
struct  SmokeParticles_t494565528  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip[] UnityStandardAssets.Effects.SmokeParticles::extinguishSounds
	AudioClipU5BU5D_t143221404* ___extinguishSounds_4;

public:
	inline static int32_t get_offset_of_extinguishSounds_4() { return static_cast<int32_t>(offsetof(SmokeParticles_t494565528, ___extinguishSounds_4)); }
	inline AudioClipU5BU5D_t143221404* get_extinguishSounds_4() const { return ___extinguishSounds_4; }
	inline AudioClipU5BU5D_t143221404** get_address_of_extinguishSounds_4() { return &___extinguishSounds_4; }
	inline void set_extinguishSounds_4(AudioClipU5BU5D_t143221404* value)
	{
		___extinguishSounds_4 = value;
		Il2CppCodeGenWriteBarrier((&___extinguishSounds_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOKEPARTICLES_T494565528_H
#ifndef WATERHOSEPARTICLES_T1340502520_H
#define WATERHOSEPARTICLES_T1340502520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Effects.WaterHoseParticles
struct  WaterHoseParticles_t1340502520  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Effects.WaterHoseParticles::force
	float ___force_5;
	// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent> UnityStandardAssets.Effects.WaterHoseParticles::m_CollisionEvents
	List_1_t1232140387 * ___m_CollisionEvents_6;
	// UnityEngine.ParticleSystem UnityStandardAssets.Effects.WaterHoseParticles::m_ParticleSystem
	ParticleSystem_t1800779281 * ___m_ParticleSystem_7;

public:
	inline static int32_t get_offset_of_force_5() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t1340502520, ___force_5)); }
	inline float get_force_5() const { return ___force_5; }
	inline float* get_address_of_force_5() { return &___force_5; }
	inline void set_force_5(float value)
	{
		___force_5 = value;
	}

	inline static int32_t get_offset_of_m_CollisionEvents_6() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t1340502520, ___m_CollisionEvents_6)); }
	inline List_1_t1232140387 * get_m_CollisionEvents_6() const { return ___m_CollisionEvents_6; }
	inline List_1_t1232140387 ** get_address_of_m_CollisionEvents_6() { return &___m_CollisionEvents_6; }
	inline void set_m_CollisionEvents_6(List_1_t1232140387 * value)
	{
		___m_CollisionEvents_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CollisionEvents_6), value);
	}

	inline static int32_t get_offset_of_m_ParticleSystem_7() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t1340502520, ___m_ParticleSystem_7)); }
	inline ParticleSystem_t1800779281 * get_m_ParticleSystem_7() const { return ___m_ParticleSystem_7; }
	inline ParticleSystem_t1800779281 ** get_address_of_m_ParticleSystem_7() { return &___m_ParticleSystem_7; }
	inline void set_m_ParticleSystem_7(ParticleSystem_t1800779281 * value)
	{
		___m_ParticleSystem_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_7), value);
	}
};

struct WaterHoseParticles_t1340502520_StaticFields
{
public:
	// System.Single UnityStandardAssets.Effects.WaterHoseParticles::lastSoundTime
	float ___lastSoundTime_4;

public:
	inline static int32_t get_offset_of_lastSoundTime_4() { return static_cast<int32_t>(offsetof(WaterHoseParticles_t1340502520_StaticFields, ___lastSoundTime_4)); }
	inline float get_lastSoundTime_4() const { return ___lastSoundTime_4; }
	inline float* get_address_of_lastSoundTime_4() { return &___lastSoundTime_4; }
	inline void set_lastSoundTime_4(float value)
	{
		___lastSoundTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERHOSEPARTICLES_T1340502520_H
#ifndef IMAGEEFFECTBASE_T2026006575_H
#define IMAGEEFFECTBASE_T2026006575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ImageEffectBase
struct  ImageEffectBase_t2026006575  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ImageEffectBase::shader
	Shader_t4151988712 * ___shader_4;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::m_Material
	Material_t340375123 * ___m_Material_5;

public:
	inline static int32_t get_offset_of_shader_4() { return static_cast<int32_t>(offsetof(ImageEffectBase_t2026006575, ___shader_4)); }
	inline Shader_t4151988712 * get_shader_4() const { return ___shader_4; }
	inline Shader_t4151988712 ** get_address_of_shader_4() { return &___shader_4; }
	inline void set_shader_4(Shader_t4151988712 * value)
	{
		___shader_4 = value;
		Il2CppCodeGenWriteBarrier((&___shader_4), value);
	}

	inline static int32_t get_offset_of_m_Material_5() { return static_cast<int32_t>(offsetof(ImageEffectBase_t2026006575, ___m_Material_5)); }
	inline Material_t340375123 * get_m_Material_5() const { return ___m_Material_5; }
	inline Material_t340375123 ** get_address_of_m_Material_5() { return &___m_Material_5; }
	inline void set_m_Material_5(Material_t340375123 * value)
	{
		___m_Material_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEEFFECTBASE_T2026006575_H
#ifndef POSTEFFECTSBASE_T2404315739_H
#define POSTEFFECTSBASE_T2404315739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.PostEffectsBase
struct  PostEffectsBase_t2404315739  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportHDRTextures
	bool ___supportHDRTextures_4;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::supportDX11
	bool ___supportDX11_5;
	// System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::isSupported
	bool ___isSupported_6;

public:
	inline static int32_t get_offset_of_supportHDRTextures_4() { return static_cast<int32_t>(offsetof(PostEffectsBase_t2404315739, ___supportHDRTextures_4)); }
	inline bool get_supportHDRTextures_4() const { return ___supportHDRTextures_4; }
	inline bool* get_address_of_supportHDRTextures_4() { return &___supportHDRTextures_4; }
	inline void set_supportHDRTextures_4(bool value)
	{
		___supportHDRTextures_4 = value;
	}

	inline static int32_t get_offset_of_supportDX11_5() { return static_cast<int32_t>(offsetof(PostEffectsBase_t2404315739, ___supportDX11_5)); }
	inline bool get_supportDX11_5() const { return ___supportDX11_5; }
	inline bool* get_address_of_supportDX11_5() { return &___supportDX11_5; }
	inline void set_supportDX11_5(bool value)
	{
		___supportDX11_5 = value;
	}

	inline static int32_t get_offset_of_isSupported_6() { return static_cast<int32_t>(offsetof(PostEffectsBase_t2404315739, ___isSupported_6)); }
	inline bool get_isSupported_6() const { return ___isSupported_6; }
	inline bool* get_address_of_isSupported_6() { return &___isSupported_6; }
	inline void set_isSupported_6(bool value)
	{
		___isSupported_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTEFFECTSBASE_T2404315739_H
#ifndef POSTEFFECTSHELPER_T675066462_H
#define POSTEFFECTSHELPER_T675066462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.PostEffectsHelper
struct  PostEffectsHelper_t675066462  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTEFFECTSHELPER_T675066462_H
#ifndef SCREENSPACEAMBIENTOCCLUSION_T1675618705_H
#define SCREENSPACEAMBIENTOCCLUSION_T1675618705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion
struct  ScreenSpaceAmbientOcclusion_t1675618705  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Radius
	float ___m_Radius_4;
	// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion/SSAOSamples UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_SampleCount
	int32_t ___m_SampleCount_5;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_OcclusionIntensity
	float ___m_OcclusionIntensity_6;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Blur
	int32_t ___m_Blur_7;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Downsampling
	int32_t ___m_Downsampling_8;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_OcclusionAttenuation
	float ___m_OcclusionAttenuation_9;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_MinZ
	float ___m_MinZ_10;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_SSAOShader
	Shader_t4151988712 * ___m_SSAOShader_11;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_SSAOMaterial
	Material_t340375123 * ___m_SSAOMaterial_12;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_RandomTexture
	Texture2D_t3840446185 * ___m_RandomTexture_13;
	// System.Boolean UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::m_Supported
	bool ___m_Supported_14;

public:
	inline static int32_t get_offset_of_m_Radius_4() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_Radius_4)); }
	inline float get_m_Radius_4() const { return ___m_Radius_4; }
	inline float* get_address_of_m_Radius_4() { return &___m_Radius_4; }
	inline void set_m_Radius_4(float value)
	{
		___m_Radius_4 = value;
	}

	inline static int32_t get_offset_of_m_SampleCount_5() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_SampleCount_5)); }
	inline int32_t get_m_SampleCount_5() const { return ___m_SampleCount_5; }
	inline int32_t* get_address_of_m_SampleCount_5() { return &___m_SampleCount_5; }
	inline void set_m_SampleCount_5(int32_t value)
	{
		___m_SampleCount_5 = value;
	}

	inline static int32_t get_offset_of_m_OcclusionIntensity_6() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_OcclusionIntensity_6)); }
	inline float get_m_OcclusionIntensity_6() const { return ___m_OcclusionIntensity_6; }
	inline float* get_address_of_m_OcclusionIntensity_6() { return &___m_OcclusionIntensity_6; }
	inline void set_m_OcclusionIntensity_6(float value)
	{
		___m_OcclusionIntensity_6 = value;
	}

	inline static int32_t get_offset_of_m_Blur_7() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_Blur_7)); }
	inline int32_t get_m_Blur_7() const { return ___m_Blur_7; }
	inline int32_t* get_address_of_m_Blur_7() { return &___m_Blur_7; }
	inline void set_m_Blur_7(int32_t value)
	{
		___m_Blur_7 = value;
	}

	inline static int32_t get_offset_of_m_Downsampling_8() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_Downsampling_8)); }
	inline int32_t get_m_Downsampling_8() const { return ___m_Downsampling_8; }
	inline int32_t* get_address_of_m_Downsampling_8() { return &___m_Downsampling_8; }
	inline void set_m_Downsampling_8(int32_t value)
	{
		___m_Downsampling_8 = value;
	}

	inline static int32_t get_offset_of_m_OcclusionAttenuation_9() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_OcclusionAttenuation_9)); }
	inline float get_m_OcclusionAttenuation_9() const { return ___m_OcclusionAttenuation_9; }
	inline float* get_address_of_m_OcclusionAttenuation_9() { return &___m_OcclusionAttenuation_9; }
	inline void set_m_OcclusionAttenuation_9(float value)
	{
		___m_OcclusionAttenuation_9 = value;
	}

	inline static int32_t get_offset_of_m_MinZ_10() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_MinZ_10)); }
	inline float get_m_MinZ_10() const { return ___m_MinZ_10; }
	inline float* get_address_of_m_MinZ_10() { return &___m_MinZ_10; }
	inline void set_m_MinZ_10(float value)
	{
		___m_MinZ_10 = value;
	}

	inline static int32_t get_offset_of_m_SSAOShader_11() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_SSAOShader_11)); }
	inline Shader_t4151988712 * get_m_SSAOShader_11() const { return ___m_SSAOShader_11; }
	inline Shader_t4151988712 ** get_address_of_m_SSAOShader_11() { return &___m_SSAOShader_11; }
	inline void set_m_SSAOShader_11(Shader_t4151988712 * value)
	{
		___m_SSAOShader_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_SSAOShader_11), value);
	}

	inline static int32_t get_offset_of_m_SSAOMaterial_12() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_SSAOMaterial_12)); }
	inline Material_t340375123 * get_m_SSAOMaterial_12() const { return ___m_SSAOMaterial_12; }
	inline Material_t340375123 ** get_address_of_m_SSAOMaterial_12() { return &___m_SSAOMaterial_12; }
	inline void set_m_SSAOMaterial_12(Material_t340375123 * value)
	{
		___m_SSAOMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_SSAOMaterial_12), value);
	}

	inline static int32_t get_offset_of_m_RandomTexture_13() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_RandomTexture_13)); }
	inline Texture2D_t3840446185 * get_m_RandomTexture_13() const { return ___m_RandomTexture_13; }
	inline Texture2D_t3840446185 ** get_address_of_m_RandomTexture_13() { return &___m_RandomTexture_13; }
	inline void set_m_RandomTexture_13(Texture2D_t3840446185 * value)
	{
		___m_RandomTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_RandomTexture_13), value);
	}

	inline static int32_t get_offset_of_m_Supported_14() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientOcclusion_t1675618705, ___m_Supported_14)); }
	inline bool get_m_Supported_14() const { return ___m_Supported_14; }
	inline bool* get_address_of_m_Supported_14() { return &___m_Supported_14; }
	inline void set_m_Supported_14(bool value)
	{
		___m_Supported_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEAMBIENTOCCLUSION_T1675618705_H
#ifndef BALL_T2378314638_H
#define BALL_T2378314638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Ball.Ball
struct  Ball_t2378314638  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_MovePower
	float ___m_MovePower_4;
	// System.Boolean UnityStandardAssets.Vehicles.Ball.Ball::m_UseTorque
	bool ___m_UseTorque_5;
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_MaxAngularVelocity
	float ___m_MaxAngularVelocity_6;
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_JumpPower
	float ___m_JumpPower_7;
	// UnityEngine.Rigidbody UnityStandardAssets.Vehicles.Ball.Ball::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_9;

public:
	inline static int32_t get_offset_of_m_MovePower_4() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_MovePower_4)); }
	inline float get_m_MovePower_4() const { return ___m_MovePower_4; }
	inline float* get_address_of_m_MovePower_4() { return &___m_MovePower_4; }
	inline void set_m_MovePower_4(float value)
	{
		___m_MovePower_4 = value;
	}

	inline static int32_t get_offset_of_m_UseTorque_5() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_UseTorque_5)); }
	inline bool get_m_UseTorque_5() const { return ___m_UseTorque_5; }
	inline bool* get_address_of_m_UseTorque_5() { return &___m_UseTorque_5; }
	inline void set_m_UseTorque_5(bool value)
	{
		___m_UseTorque_5 = value;
	}

	inline static int32_t get_offset_of_m_MaxAngularVelocity_6() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_MaxAngularVelocity_6)); }
	inline float get_m_MaxAngularVelocity_6() const { return ___m_MaxAngularVelocity_6; }
	inline float* get_address_of_m_MaxAngularVelocity_6() { return &___m_MaxAngularVelocity_6; }
	inline void set_m_MaxAngularVelocity_6(float value)
	{
		___m_MaxAngularVelocity_6 = value;
	}

	inline static int32_t get_offset_of_m_JumpPower_7() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_JumpPower_7)); }
	inline float get_m_JumpPower_7() const { return ___m_JumpPower_7; }
	inline float* get_address_of_m_JumpPower_7() { return &___m_JumpPower_7; }
	inline void set_m_JumpPower_7(float value)
	{
		___m_JumpPower_7 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_9() { return static_cast<int32_t>(offsetof(Ball_t2378314638, ___m_Rigidbody_9)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_9() const { return ___m_Rigidbody_9; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_9() { return &___m_Rigidbody_9; }
	inline void set_m_Rigidbody_9(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALL_T2378314638_H
#ifndef BALLUSERCONTROL_T2574698008_H
#define BALLUSERCONTROL_T2574698008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Ball.BallUserControl
struct  BallUserControl_t2574698008  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Vehicles.Ball.Ball UnityStandardAssets.Vehicles.Ball.BallUserControl::ball
	Ball_t2378314638 * ___ball_4;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Ball.BallUserControl::move
	Vector3_t3722313464  ___move_5;
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Ball.BallUserControl::cam
	Transform_t3600365921 * ___cam_6;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Ball.BallUserControl::camForward
	Vector3_t3722313464  ___camForward_7;
	// System.Boolean UnityStandardAssets.Vehicles.Ball.BallUserControl::jump
	bool ___jump_8;

public:
	inline static int32_t get_offset_of_ball_4() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___ball_4)); }
	inline Ball_t2378314638 * get_ball_4() const { return ___ball_4; }
	inline Ball_t2378314638 ** get_address_of_ball_4() { return &___ball_4; }
	inline void set_ball_4(Ball_t2378314638 * value)
	{
		___ball_4 = value;
		Il2CppCodeGenWriteBarrier((&___ball_4), value);
	}

	inline static int32_t get_offset_of_move_5() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___move_5)); }
	inline Vector3_t3722313464  get_move_5() const { return ___move_5; }
	inline Vector3_t3722313464 * get_address_of_move_5() { return &___move_5; }
	inline void set_move_5(Vector3_t3722313464  value)
	{
		___move_5 = value;
	}

	inline static int32_t get_offset_of_cam_6() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___cam_6)); }
	inline Transform_t3600365921 * get_cam_6() const { return ___cam_6; }
	inline Transform_t3600365921 ** get_address_of_cam_6() { return &___cam_6; }
	inline void set_cam_6(Transform_t3600365921 * value)
	{
		___cam_6 = value;
		Il2CppCodeGenWriteBarrier((&___cam_6), value);
	}

	inline static int32_t get_offset_of_camForward_7() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___camForward_7)); }
	inline Vector3_t3722313464  get_camForward_7() const { return ___camForward_7; }
	inline Vector3_t3722313464 * get_address_of_camForward_7() { return &___camForward_7; }
	inline void set_camForward_7(Vector3_t3722313464  value)
	{
		___camForward_7 = value;
	}

	inline static int32_t get_offset_of_jump_8() { return static_cast<int32_t>(offsetof(BallUserControl_t2574698008, ___jump_8)); }
	inline bool get_jump_8() const { return ___jump_8; }
	inline bool* get_address_of_jump_8() { return &___jump_8; }
	inline void set_jump_8(bool value)
	{
		___jump_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLUSERCONTROL_T2574698008_H
#ifndef DISPLACE_T1352130581_H
#define DISPLACE_T1352130581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Displace
struct  Displace_t1352130581  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLACE_T1352130581_H
#ifndef PLANARREFLECTION_T439636033_H
#define PLANARREFLECTION_T439636033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.PlanarReflection
struct  PlanarReflection_t439636033  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.LayerMask UnityStandardAssets.Water.PlanarReflection::reflectionMask
	LayerMask_t3493934918  ___reflectionMask_4;
	// System.Boolean UnityStandardAssets.Water.PlanarReflection::reflectSkybox
	bool ___reflectSkybox_5;
	// UnityEngine.Color UnityStandardAssets.Water.PlanarReflection::clearColor
	Color_t2555686324  ___clearColor_6;
	// System.String UnityStandardAssets.Water.PlanarReflection::reflectionSampler
	String_t* ___reflectionSampler_7;
	// System.Single UnityStandardAssets.Water.PlanarReflection::clipPlaneOffset
	float ___clipPlaneOffset_8;
	// UnityEngine.Vector3 UnityStandardAssets.Water.PlanarReflection::m_Oldpos
	Vector3_t3722313464  ___m_Oldpos_9;
	// UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::m_ReflectionCamera
	Camera_t4157153871 * ___m_ReflectionCamera_10;
	// UnityEngine.Material UnityStandardAssets.Water.PlanarReflection::m_SharedMaterial
	Material_t340375123 * ___m_SharedMaterial_11;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean> UnityStandardAssets.Water.PlanarReflection::m_HelperCameras
	Dictionary_2_t310742658 * ___m_HelperCameras_12;

public:
	inline static int32_t get_offset_of_reflectionMask_4() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___reflectionMask_4)); }
	inline LayerMask_t3493934918  get_reflectionMask_4() const { return ___reflectionMask_4; }
	inline LayerMask_t3493934918 * get_address_of_reflectionMask_4() { return &___reflectionMask_4; }
	inline void set_reflectionMask_4(LayerMask_t3493934918  value)
	{
		___reflectionMask_4 = value;
	}

	inline static int32_t get_offset_of_reflectSkybox_5() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___reflectSkybox_5)); }
	inline bool get_reflectSkybox_5() const { return ___reflectSkybox_5; }
	inline bool* get_address_of_reflectSkybox_5() { return &___reflectSkybox_5; }
	inline void set_reflectSkybox_5(bool value)
	{
		___reflectSkybox_5 = value;
	}

	inline static int32_t get_offset_of_clearColor_6() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___clearColor_6)); }
	inline Color_t2555686324  get_clearColor_6() const { return ___clearColor_6; }
	inline Color_t2555686324 * get_address_of_clearColor_6() { return &___clearColor_6; }
	inline void set_clearColor_6(Color_t2555686324  value)
	{
		___clearColor_6 = value;
	}

	inline static int32_t get_offset_of_reflectionSampler_7() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___reflectionSampler_7)); }
	inline String_t* get_reflectionSampler_7() const { return ___reflectionSampler_7; }
	inline String_t** get_address_of_reflectionSampler_7() { return &___reflectionSampler_7; }
	inline void set_reflectionSampler_7(String_t* value)
	{
		___reflectionSampler_7 = value;
		Il2CppCodeGenWriteBarrier((&___reflectionSampler_7), value);
	}

	inline static int32_t get_offset_of_clipPlaneOffset_8() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___clipPlaneOffset_8)); }
	inline float get_clipPlaneOffset_8() const { return ___clipPlaneOffset_8; }
	inline float* get_address_of_clipPlaneOffset_8() { return &___clipPlaneOffset_8; }
	inline void set_clipPlaneOffset_8(float value)
	{
		___clipPlaneOffset_8 = value;
	}

	inline static int32_t get_offset_of_m_Oldpos_9() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_Oldpos_9)); }
	inline Vector3_t3722313464  get_m_Oldpos_9() const { return ___m_Oldpos_9; }
	inline Vector3_t3722313464 * get_address_of_m_Oldpos_9() { return &___m_Oldpos_9; }
	inline void set_m_Oldpos_9(Vector3_t3722313464  value)
	{
		___m_Oldpos_9 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCamera_10() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_ReflectionCamera_10)); }
	inline Camera_t4157153871 * get_m_ReflectionCamera_10() const { return ___m_ReflectionCamera_10; }
	inline Camera_t4157153871 ** get_address_of_m_ReflectionCamera_10() { return &___m_ReflectionCamera_10; }
	inline void set_m_ReflectionCamera_10(Camera_t4157153871 * value)
	{
		___m_ReflectionCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionCamera_10), value);
	}

	inline static int32_t get_offset_of_m_SharedMaterial_11() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_SharedMaterial_11)); }
	inline Material_t340375123 * get_m_SharedMaterial_11() const { return ___m_SharedMaterial_11; }
	inline Material_t340375123 ** get_address_of_m_SharedMaterial_11() { return &___m_SharedMaterial_11; }
	inline void set_m_SharedMaterial_11(Material_t340375123 * value)
	{
		___m_SharedMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_SharedMaterial_11), value);
	}

	inline static int32_t get_offset_of_m_HelperCameras_12() { return static_cast<int32_t>(offsetof(PlanarReflection_t439636033, ___m_HelperCameras_12)); }
	inline Dictionary_2_t310742658 * get_m_HelperCameras_12() const { return ___m_HelperCameras_12; }
	inline Dictionary_2_t310742658 ** get_address_of_m_HelperCameras_12() { return &___m_HelperCameras_12; }
	inline void set_m_HelperCameras_12(Dictionary_2_t310742658 * value)
	{
		___m_HelperCameras_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_HelperCameras_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANARREFLECTION_T439636033_H
#ifndef SPECULARLIGHTING_T2163114238_H
#define SPECULARLIGHTING_T2163114238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.SpecularLighting
struct  SpecularLighting_t2163114238  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Water.SpecularLighting::specularLight
	Transform_t3600365921 * ___specularLight_4;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.SpecularLighting::m_WaterBase
	WaterBase_t3883863317 * ___m_WaterBase_5;

public:
	inline static int32_t get_offset_of_specularLight_4() { return static_cast<int32_t>(offsetof(SpecularLighting_t2163114238, ___specularLight_4)); }
	inline Transform_t3600365921 * get_specularLight_4() const { return ___specularLight_4; }
	inline Transform_t3600365921 ** get_address_of_specularLight_4() { return &___specularLight_4; }
	inline void set_specularLight_4(Transform_t3600365921 * value)
	{
		___specularLight_4 = value;
		Il2CppCodeGenWriteBarrier((&___specularLight_4), value);
	}

	inline static int32_t get_offset_of_m_WaterBase_5() { return static_cast<int32_t>(offsetof(SpecularLighting_t2163114238, ___m_WaterBase_5)); }
	inline WaterBase_t3883863317 * get_m_WaterBase_5() const { return ___m_WaterBase_5; }
	inline WaterBase_t3883863317 ** get_address_of_m_WaterBase_5() { return &___m_WaterBase_5; }
	inline void set_m_WaterBase_5(WaterBase_t3883863317 * value)
	{
		___m_WaterBase_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaterBase_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECULARLIGHTING_T2163114238_H
#ifndef WATER_T936588004_H
#define WATER_T936588004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Water
struct  Water_t936588004  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::waterMode
	int32_t ___waterMode_4;
	// System.Boolean UnityStandardAssets.Water.Water::disablePixelLights
	bool ___disablePixelLights_5;
	// System.Int32 UnityStandardAssets.Water.Water::textureSize
	int32_t ___textureSize_6;
	// System.Single UnityStandardAssets.Water.Water::clipPlaneOffset
	float ___clipPlaneOffset_7;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::reflectLayers
	LayerMask_t3493934918  ___reflectLayers_8;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::refractLayers
	LayerMask_t3493934918  ___refractLayers_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_ReflectionCameras
	Dictionary_2_t75641268 * ___m_ReflectionCameras_10;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_RefractionCameras
	Dictionary_2_t75641268 * ___m_RefractionCameras_11;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_ReflectionTexture
	RenderTexture_t2108887433 * ___m_ReflectionTexture_12;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_RefractionTexture
	RenderTexture_t2108887433 * ___m_RefractionTexture_13;
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::m_HardwareWaterSupport
	int32_t ___m_HardwareWaterSupport_14;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldReflectionTextureSize
	int32_t ___m_OldReflectionTextureSize_15;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldRefractionTextureSize
	int32_t ___m_OldRefractionTextureSize_16;

public:
	inline static int32_t get_offset_of_waterMode_4() { return static_cast<int32_t>(offsetof(Water_t936588004, ___waterMode_4)); }
	inline int32_t get_waterMode_4() const { return ___waterMode_4; }
	inline int32_t* get_address_of_waterMode_4() { return &___waterMode_4; }
	inline void set_waterMode_4(int32_t value)
	{
		___waterMode_4 = value;
	}

	inline static int32_t get_offset_of_disablePixelLights_5() { return static_cast<int32_t>(offsetof(Water_t936588004, ___disablePixelLights_5)); }
	inline bool get_disablePixelLights_5() const { return ___disablePixelLights_5; }
	inline bool* get_address_of_disablePixelLights_5() { return &___disablePixelLights_5; }
	inline void set_disablePixelLights_5(bool value)
	{
		___disablePixelLights_5 = value;
	}

	inline static int32_t get_offset_of_textureSize_6() { return static_cast<int32_t>(offsetof(Water_t936588004, ___textureSize_6)); }
	inline int32_t get_textureSize_6() const { return ___textureSize_6; }
	inline int32_t* get_address_of_textureSize_6() { return &___textureSize_6; }
	inline void set_textureSize_6(int32_t value)
	{
		___textureSize_6 = value;
	}

	inline static int32_t get_offset_of_clipPlaneOffset_7() { return static_cast<int32_t>(offsetof(Water_t936588004, ___clipPlaneOffset_7)); }
	inline float get_clipPlaneOffset_7() const { return ___clipPlaneOffset_7; }
	inline float* get_address_of_clipPlaneOffset_7() { return &___clipPlaneOffset_7; }
	inline void set_clipPlaneOffset_7(float value)
	{
		___clipPlaneOffset_7 = value;
	}

	inline static int32_t get_offset_of_reflectLayers_8() { return static_cast<int32_t>(offsetof(Water_t936588004, ___reflectLayers_8)); }
	inline LayerMask_t3493934918  get_reflectLayers_8() const { return ___reflectLayers_8; }
	inline LayerMask_t3493934918 * get_address_of_reflectLayers_8() { return &___reflectLayers_8; }
	inline void set_reflectLayers_8(LayerMask_t3493934918  value)
	{
		___reflectLayers_8 = value;
	}

	inline static int32_t get_offset_of_refractLayers_9() { return static_cast<int32_t>(offsetof(Water_t936588004, ___refractLayers_9)); }
	inline LayerMask_t3493934918  get_refractLayers_9() const { return ___refractLayers_9; }
	inline LayerMask_t3493934918 * get_address_of_refractLayers_9() { return &___refractLayers_9; }
	inline void set_refractLayers_9(LayerMask_t3493934918  value)
	{
		___refractLayers_9 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCameras_10() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_ReflectionCameras_10)); }
	inline Dictionary_2_t75641268 * get_m_ReflectionCameras_10() const { return ___m_ReflectionCameras_10; }
	inline Dictionary_2_t75641268 ** get_address_of_m_ReflectionCameras_10() { return &___m_ReflectionCameras_10; }
	inline void set_m_ReflectionCameras_10(Dictionary_2_t75641268 * value)
	{
		___m_ReflectionCameras_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionCameras_10), value);
	}

	inline static int32_t get_offset_of_m_RefractionCameras_11() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_RefractionCameras_11)); }
	inline Dictionary_2_t75641268 * get_m_RefractionCameras_11() const { return ___m_RefractionCameras_11; }
	inline Dictionary_2_t75641268 ** get_address_of_m_RefractionCameras_11() { return &___m_RefractionCameras_11; }
	inline void set_m_RefractionCameras_11(Dictionary_2_t75641268 * value)
	{
		___m_RefractionCameras_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionCameras_11), value);
	}

	inline static int32_t get_offset_of_m_ReflectionTexture_12() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_ReflectionTexture_12)); }
	inline RenderTexture_t2108887433 * get_m_ReflectionTexture_12() const { return ___m_ReflectionTexture_12; }
	inline RenderTexture_t2108887433 ** get_address_of_m_ReflectionTexture_12() { return &___m_ReflectionTexture_12; }
	inline void set_m_ReflectionTexture_12(RenderTexture_t2108887433 * value)
	{
		___m_ReflectionTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionTexture_12), value);
	}

	inline static int32_t get_offset_of_m_RefractionTexture_13() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_RefractionTexture_13)); }
	inline RenderTexture_t2108887433 * get_m_RefractionTexture_13() const { return ___m_RefractionTexture_13; }
	inline RenderTexture_t2108887433 ** get_address_of_m_RefractionTexture_13() { return &___m_RefractionTexture_13; }
	inline void set_m_RefractionTexture_13(RenderTexture_t2108887433 * value)
	{
		___m_RefractionTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionTexture_13), value);
	}

	inline static int32_t get_offset_of_m_HardwareWaterSupport_14() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_HardwareWaterSupport_14)); }
	inline int32_t get_m_HardwareWaterSupport_14() const { return ___m_HardwareWaterSupport_14; }
	inline int32_t* get_address_of_m_HardwareWaterSupport_14() { return &___m_HardwareWaterSupport_14; }
	inline void set_m_HardwareWaterSupport_14(int32_t value)
	{
		___m_HardwareWaterSupport_14 = value;
	}

	inline static int32_t get_offset_of_m_OldReflectionTextureSize_15() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_OldReflectionTextureSize_15)); }
	inline int32_t get_m_OldReflectionTextureSize_15() const { return ___m_OldReflectionTextureSize_15; }
	inline int32_t* get_address_of_m_OldReflectionTextureSize_15() { return &___m_OldReflectionTextureSize_15; }
	inline void set_m_OldReflectionTextureSize_15(int32_t value)
	{
		___m_OldReflectionTextureSize_15 = value;
	}

	inline static int32_t get_offset_of_m_OldRefractionTextureSize_16() { return static_cast<int32_t>(offsetof(Water_t936588004, ___m_OldRefractionTextureSize_16)); }
	inline int32_t get_m_OldRefractionTextureSize_16() const { return ___m_OldRefractionTextureSize_16; }
	inline int32_t* get_address_of_m_OldRefractionTextureSize_16() { return &___m_OldRefractionTextureSize_16; }
	inline void set_m_OldRefractionTextureSize_16(int32_t value)
	{
		___m_OldRefractionTextureSize_16 = value;
	}
};

struct Water_t936588004_StaticFields
{
public:
	// System.Boolean UnityStandardAssets.Water.Water::s_InsideWater
	bool ___s_InsideWater_17;

public:
	inline static int32_t get_offset_of_s_InsideWater_17() { return static_cast<int32_t>(offsetof(Water_t936588004_StaticFields, ___s_InsideWater_17)); }
	inline bool get_s_InsideWater_17() const { return ___s_InsideWater_17; }
	inline bool* get_address_of_s_InsideWater_17() { return &___s_InsideWater_17; }
	inline void set_s_InsideWater_17(bool value)
	{
		___s_InsideWater_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATER_T936588004_H
#ifndef WATERBASE_T3883863317_H
#define WATERBASE_T3883863317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterBase
struct  WaterBase_t3883863317  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material UnityStandardAssets.Water.WaterBase::sharedMaterial
	Material_t340375123 * ___sharedMaterial_4;
	// UnityStandardAssets.Water.WaterQuality UnityStandardAssets.Water.WaterBase::waterQuality
	int32_t ___waterQuality_5;
	// System.Boolean UnityStandardAssets.Water.WaterBase::edgeBlend
	bool ___edgeBlend_6;

public:
	inline static int32_t get_offset_of_sharedMaterial_4() { return static_cast<int32_t>(offsetof(WaterBase_t3883863317, ___sharedMaterial_4)); }
	inline Material_t340375123 * get_sharedMaterial_4() const { return ___sharedMaterial_4; }
	inline Material_t340375123 ** get_address_of_sharedMaterial_4() { return &___sharedMaterial_4; }
	inline void set_sharedMaterial_4(Material_t340375123 * value)
	{
		___sharedMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___sharedMaterial_4), value);
	}

	inline static int32_t get_offset_of_waterQuality_5() { return static_cast<int32_t>(offsetof(WaterBase_t3883863317, ___waterQuality_5)); }
	inline int32_t get_waterQuality_5() const { return ___waterQuality_5; }
	inline int32_t* get_address_of_waterQuality_5() { return &___waterQuality_5; }
	inline void set_waterQuality_5(int32_t value)
	{
		___waterQuality_5 = value;
	}

	inline static int32_t get_offset_of_edgeBlend_6() { return static_cast<int32_t>(offsetof(WaterBase_t3883863317, ___edgeBlend_6)); }
	inline bool get_edgeBlend_6() const { return ___edgeBlend_6; }
	inline bool* get_address_of_edgeBlend_6() { return &___edgeBlend_6; }
	inline void set_edgeBlend_6(bool value)
	{
		___edgeBlend_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERBASE_T3883863317_H
#ifndef WATERBASIC_T418026961_H
#define WATERBASIC_T418026961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterBasic
struct  WaterBasic_t418026961  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERBASIC_T418026961_H
#ifndef WATERTILE_T2536246541_H
#define WATERTILE_T2536246541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterTile
struct  WaterTile_t2536246541  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Water.PlanarReflection UnityStandardAssets.Water.WaterTile::reflection
	PlanarReflection_t439636033 * ___reflection_4;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.WaterTile::waterBase
	WaterBase_t3883863317 * ___waterBase_5;

public:
	inline static int32_t get_offset_of_reflection_4() { return static_cast<int32_t>(offsetof(WaterTile_t2536246541, ___reflection_4)); }
	inline PlanarReflection_t439636033 * get_reflection_4() const { return ___reflection_4; }
	inline PlanarReflection_t439636033 ** get_address_of_reflection_4() { return &___reflection_4; }
	inline void set_reflection_4(PlanarReflection_t439636033 * value)
	{
		___reflection_4 = value;
		Il2CppCodeGenWriteBarrier((&___reflection_4), value);
	}

	inline static int32_t get_offset_of_waterBase_5() { return static_cast<int32_t>(offsetof(WaterTile_t2536246541, ___waterBase_5)); }
	inline WaterBase_t3883863317 * get_waterBase_5() const { return ___waterBase_5; }
	inline WaterBase_t3883863317 ** get_address_of_waterBase_5() { return &___waterBase_5; }
	inline void set_waterBase_5(WaterBase_t3883863317 * value)
	{
		___waterBase_5 = value;
		Il2CppCodeGenWriteBarrier((&___waterBase_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERTILE_T2536246541_H
#ifndef CAMERA2DFOLLOW_T3335230098_H
#define CAMERA2DFOLLOW_T3335230098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets._2D.Camera2DFollow
struct  Camera2DFollow_t3335230098  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets._2D.Camera2DFollow::target
	Transform_t3600365921 * ___target_4;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::damping
	float ___damping_5;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::lookAheadFactor
	float ___lookAheadFactor_6;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::lookAheadReturnSpeed
	float ___lookAheadReturnSpeed_7;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::lookAheadMoveThreshold
	float ___lookAheadMoveThreshold_8;
	// System.Single UnityStandardAssets._2D.Camera2DFollow::m_OffsetZ
	float ___m_OffsetZ_9;
	// UnityEngine.Vector3 UnityStandardAssets._2D.Camera2DFollow::m_LastTargetPosition
	Vector3_t3722313464  ___m_LastTargetPosition_10;
	// UnityEngine.Vector3 UnityStandardAssets._2D.Camera2DFollow::m_CurrentVelocity
	Vector3_t3722313464  ___m_CurrentVelocity_11;
	// UnityEngine.Vector3 UnityStandardAssets._2D.Camera2DFollow::m_LookAheadPos
	Vector3_t3722313464  ___m_LookAheadPos_12;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_damping_5() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___damping_5)); }
	inline float get_damping_5() const { return ___damping_5; }
	inline float* get_address_of_damping_5() { return &___damping_5; }
	inline void set_damping_5(float value)
	{
		___damping_5 = value;
	}

	inline static int32_t get_offset_of_lookAheadFactor_6() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___lookAheadFactor_6)); }
	inline float get_lookAheadFactor_6() const { return ___lookAheadFactor_6; }
	inline float* get_address_of_lookAheadFactor_6() { return &___lookAheadFactor_6; }
	inline void set_lookAheadFactor_6(float value)
	{
		___lookAheadFactor_6 = value;
	}

	inline static int32_t get_offset_of_lookAheadReturnSpeed_7() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___lookAheadReturnSpeed_7)); }
	inline float get_lookAheadReturnSpeed_7() const { return ___lookAheadReturnSpeed_7; }
	inline float* get_address_of_lookAheadReturnSpeed_7() { return &___lookAheadReturnSpeed_7; }
	inline void set_lookAheadReturnSpeed_7(float value)
	{
		___lookAheadReturnSpeed_7 = value;
	}

	inline static int32_t get_offset_of_lookAheadMoveThreshold_8() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___lookAheadMoveThreshold_8)); }
	inline float get_lookAheadMoveThreshold_8() const { return ___lookAheadMoveThreshold_8; }
	inline float* get_address_of_lookAheadMoveThreshold_8() { return &___lookAheadMoveThreshold_8; }
	inline void set_lookAheadMoveThreshold_8(float value)
	{
		___lookAheadMoveThreshold_8 = value;
	}

	inline static int32_t get_offset_of_m_OffsetZ_9() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___m_OffsetZ_9)); }
	inline float get_m_OffsetZ_9() const { return ___m_OffsetZ_9; }
	inline float* get_address_of_m_OffsetZ_9() { return &___m_OffsetZ_9; }
	inline void set_m_OffsetZ_9(float value)
	{
		___m_OffsetZ_9 = value;
	}

	inline static int32_t get_offset_of_m_LastTargetPosition_10() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___m_LastTargetPosition_10)); }
	inline Vector3_t3722313464  get_m_LastTargetPosition_10() const { return ___m_LastTargetPosition_10; }
	inline Vector3_t3722313464 * get_address_of_m_LastTargetPosition_10() { return &___m_LastTargetPosition_10; }
	inline void set_m_LastTargetPosition_10(Vector3_t3722313464  value)
	{
		___m_LastTargetPosition_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentVelocity_11() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___m_CurrentVelocity_11)); }
	inline Vector3_t3722313464  get_m_CurrentVelocity_11() const { return ___m_CurrentVelocity_11; }
	inline Vector3_t3722313464 * get_address_of_m_CurrentVelocity_11() { return &___m_CurrentVelocity_11; }
	inline void set_m_CurrentVelocity_11(Vector3_t3722313464  value)
	{
		___m_CurrentVelocity_11 = value;
	}

	inline static int32_t get_offset_of_m_LookAheadPos_12() { return static_cast<int32_t>(offsetof(Camera2DFollow_t3335230098, ___m_LookAheadPos_12)); }
	inline Vector3_t3722313464  get_m_LookAheadPos_12() const { return ___m_LookAheadPos_12; }
	inline Vector3_t3722313464 * get_address_of_m_LookAheadPos_12() { return &___m_LookAheadPos_12; }
	inline void set_m_LookAheadPos_12(Vector3_t3722313464  value)
	{
		___m_LookAheadPos_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA2DFOLLOW_T3335230098_H
#ifndef CAMERAFOLLOW_T1399352937_H
#define CAMERAFOLLOW_T1399352937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets._2D.CameraFollow
struct  CameraFollow_t1399352937  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets._2D.CameraFollow::xMargin
	float ___xMargin_4;
	// System.Single UnityStandardAssets._2D.CameraFollow::yMargin
	float ___yMargin_5;
	// System.Single UnityStandardAssets._2D.CameraFollow::xSmooth
	float ___xSmooth_6;
	// System.Single UnityStandardAssets._2D.CameraFollow::ySmooth
	float ___ySmooth_7;
	// UnityEngine.Vector2 UnityStandardAssets._2D.CameraFollow::maxXAndY
	Vector2_t2156229523  ___maxXAndY_8;
	// UnityEngine.Vector2 UnityStandardAssets._2D.CameraFollow::minXAndY
	Vector2_t2156229523  ___minXAndY_9;
	// UnityEngine.Transform UnityStandardAssets._2D.CameraFollow::m_Player
	Transform_t3600365921 * ___m_Player_10;

public:
	inline static int32_t get_offset_of_xMargin_4() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___xMargin_4)); }
	inline float get_xMargin_4() const { return ___xMargin_4; }
	inline float* get_address_of_xMargin_4() { return &___xMargin_4; }
	inline void set_xMargin_4(float value)
	{
		___xMargin_4 = value;
	}

	inline static int32_t get_offset_of_yMargin_5() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___yMargin_5)); }
	inline float get_yMargin_5() const { return ___yMargin_5; }
	inline float* get_address_of_yMargin_5() { return &___yMargin_5; }
	inline void set_yMargin_5(float value)
	{
		___yMargin_5 = value;
	}

	inline static int32_t get_offset_of_xSmooth_6() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___xSmooth_6)); }
	inline float get_xSmooth_6() const { return ___xSmooth_6; }
	inline float* get_address_of_xSmooth_6() { return &___xSmooth_6; }
	inline void set_xSmooth_6(float value)
	{
		___xSmooth_6 = value;
	}

	inline static int32_t get_offset_of_ySmooth_7() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___ySmooth_7)); }
	inline float get_ySmooth_7() const { return ___ySmooth_7; }
	inline float* get_address_of_ySmooth_7() { return &___ySmooth_7; }
	inline void set_ySmooth_7(float value)
	{
		___ySmooth_7 = value;
	}

	inline static int32_t get_offset_of_maxXAndY_8() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___maxXAndY_8)); }
	inline Vector2_t2156229523  get_maxXAndY_8() const { return ___maxXAndY_8; }
	inline Vector2_t2156229523 * get_address_of_maxXAndY_8() { return &___maxXAndY_8; }
	inline void set_maxXAndY_8(Vector2_t2156229523  value)
	{
		___maxXAndY_8 = value;
	}

	inline static int32_t get_offset_of_minXAndY_9() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___minXAndY_9)); }
	inline Vector2_t2156229523  get_minXAndY_9() const { return ___minXAndY_9; }
	inline Vector2_t2156229523 * get_address_of_minXAndY_9() { return &___minXAndY_9; }
	inline void set_minXAndY_9(Vector2_t2156229523  value)
	{
		___minXAndY_9 = value;
	}

	inline static int32_t get_offset_of_m_Player_10() { return static_cast<int32_t>(offsetof(CameraFollow_t1399352937, ___m_Player_10)); }
	inline Transform_t3600365921 * get_m_Player_10() const { return ___m_Player_10; }
	inline Transform_t3600365921 ** get_address_of_m_Player_10() { return &___m_Player_10; }
	inline void set_m_Player_10(Transform_t3600365921 * value)
	{
		___m_Player_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Player_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFOLLOW_T1399352937_H
#ifndef PLATFORMER2DUSERCONTROL_T4130129562_H
#define PLATFORMER2DUSERCONTROL_T4130129562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets._2D.Platformer2DUserControl
struct  Platformer2DUserControl_t4130129562  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets._2D.PlatformerCharacter2D UnityStandardAssets._2D.Platformer2DUserControl::m_Character
	PlatformerCharacter2D_t675295753 * ___m_Character_4;
	// System.Boolean UnityStandardAssets._2D.Platformer2DUserControl::m_Jump
	bool ___m_Jump_5;

public:
	inline static int32_t get_offset_of_m_Character_4() { return static_cast<int32_t>(offsetof(Platformer2DUserControl_t4130129562, ___m_Character_4)); }
	inline PlatformerCharacter2D_t675295753 * get_m_Character_4() const { return ___m_Character_4; }
	inline PlatformerCharacter2D_t675295753 ** get_address_of_m_Character_4() { return &___m_Character_4; }
	inline void set_m_Character_4(PlatformerCharacter2D_t675295753 * value)
	{
		___m_Character_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Character_4), value);
	}

	inline static int32_t get_offset_of_m_Jump_5() { return static_cast<int32_t>(offsetof(Platformer2DUserControl_t4130129562, ___m_Jump_5)); }
	inline bool get_m_Jump_5() const { return ___m_Jump_5; }
	inline bool* get_address_of_m_Jump_5() { return &___m_Jump_5; }
	inline void set_m_Jump_5(bool value)
	{
		___m_Jump_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMER2DUSERCONTROL_T4130129562_H
#ifndef PLATFORMERCHARACTER2D_T675295753_H
#define PLATFORMERCHARACTER2D_T675295753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets._2D.PlatformerCharacter2D
struct  PlatformerCharacter2D_t675295753  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets._2D.PlatformerCharacter2D::m_MaxSpeed
	float ___m_MaxSpeed_4;
	// System.Single UnityStandardAssets._2D.PlatformerCharacter2D::m_JumpForce
	float ___m_JumpForce_5;
	// System.Single UnityStandardAssets._2D.PlatformerCharacter2D::m_CrouchSpeed
	float ___m_CrouchSpeed_6;
	// System.Boolean UnityStandardAssets._2D.PlatformerCharacter2D::m_AirControl
	bool ___m_AirControl_7;
	// UnityEngine.LayerMask UnityStandardAssets._2D.PlatformerCharacter2D::m_WhatIsGround
	LayerMask_t3493934918  ___m_WhatIsGround_8;
	// UnityEngine.Transform UnityStandardAssets._2D.PlatformerCharacter2D::m_GroundCheck
	Transform_t3600365921 * ___m_GroundCheck_9;
	// System.Boolean UnityStandardAssets._2D.PlatformerCharacter2D::m_Grounded
	bool ___m_Grounded_11;
	// UnityEngine.Transform UnityStandardAssets._2D.PlatformerCharacter2D::m_CeilingCheck
	Transform_t3600365921 * ___m_CeilingCheck_12;
	// UnityEngine.Animator UnityStandardAssets._2D.PlatformerCharacter2D::m_Anim
	Animator_t434523843 * ___m_Anim_14;
	// UnityEngine.Rigidbody2D UnityStandardAssets._2D.PlatformerCharacter2D::m_Rigidbody2D
	Rigidbody2D_t939494601 * ___m_Rigidbody2D_15;
	// System.Boolean UnityStandardAssets._2D.PlatformerCharacter2D::m_FacingRight
	bool ___m_FacingRight_16;

public:
	inline static int32_t get_offset_of_m_MaxSpeed_4() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_MaxSpeed_4)); }
	inline float get_m_MaxSpeed_4() const { return ___m_MaxSpeed_4; }
	inline float* get_address_of_m_MaxSpeed_4() { return &___m_MaxSpeed_4; }
	inline void set_m_MaxSpeed_4(float value)
	{
		___m_MaxSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_JumpForce_5() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_JumpForce_5)); }
	inline float get_m_JumpForce_5() const { return ___m_JumpForce_5; }
	inline float* get_address_of_m_JumpForce_5() { return &___m_JumpForce_5; }
	inline void set_m_JumpForce_5(float value)
	{
		___m_JumpForce_5 = value;
	}

	inline static int32_t get_offset_of_m_CrouchSpeed_6() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_CrouchSpeed_6)); }
	inline float get_m_CrouchSpeed_6() const { return ___m_CrouchSpeed_6; }
	inline float* get_address_of_m_CrouchSpeed_6() { return &___m_CrouchSpeed_6; }
	inline void set_m_CrouchSpeed_6(float value)
	{
		___m_CrouchSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_AirControl_7() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_AirControl_7)); }
	inline bool get_m_AirControl_7() const { return ___m_AirControl_7; }
	inline bool* get_address_of_m_AirControl_7() { return &___m_AirControl_7; }
	inline void set_m_AirControl_7(bool value)
	{
		___m_AirControl_7 = value;
	}

	inline static int32_t get_offset_of_m_WhatIsGround_8() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_WhatIsGround_8)); }
	inline LayerMask_t3493934918  get_m_WhatIsGround_8() const { return ___m_WhatIsGround_8; }
	inline LayerMask_t3493934918 * get_address_of_m_WhatIsGround_8() { return &___m_WhatIsGround_8; }
	inline void set_m_WhatIsGround_8(LayerMask_t3493934918  value)
	{
		___m_WhatIsGround_8 = value;
	}

	inline static int32_t get_offset_of_m_GroundCheck_9() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_GroundCheck_9)); }
	inline Transform_t3600365921 * get_m_GroundCheck_9() const { return ___m_GroundCheck_9; }
	inline Transform_t3600365921 ** get_address_of_m_GroundCheck_9() { return &___m_GroundCheck_9; }
	inline void set_m_GroundCheck_9(Transform_t3600365921 * value)
	{
		___m_GroundCheck_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroundCheck_9), value);
	}

	inline static int32_t get_offset_of_m_Grounded_11() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_Grounded_11)); }
	inline bool get_m_Grounded_11() const { return ___m_Grounded_11; }
	inline bool* get_address_of_m_Grounded_11() { return &___m_Grounded_11; }
	inline void set_m_Grounded_11(bool value)
	{
		___m_Grounded_11 = value;
	}

	inline static int32_t get_offset_of_m_CeilingCheck_12() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_CeilingCheck_12)); }
	inline Transform_t3600365921 * get_m_CeilingCheck_12() const { return ___m_CeilingCheck_12; }
	inline Transform_t3600365921 ** get_address_of_m_CeilingCheck_12() { return &___m_CeilingCheck_12; }
	inline void set_m_CeilingCheck_12(Transform_t3600365921 * value)
	{
		___m_CeilingCheck_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_CeilingCheck_12), value);
	}

	inline static int32_t get_offset_of_m_Anim_14() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_Anim_14)); }
	inline Animator_t434523843 * get_m_Anim_14() const { return ___m_Anim_14; }
	inline Animator_t434523843 ** get_address_of_m_Anim_14() { return &___m_Anim_14; }
	inline void set_m_Anim_14(Animator_t434523843 * value)
	{
		___m_Anim_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Anim_14), value);
	}

	inline static int32_t get_offset_of_m_Rigidbody2D_15() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_Rigidbody2D_15)); }
	inline Rigidbody2D_t939494601 * get_m_Rigidbody2D_15() const { return ___m_Rigidbody2D_15; }
	inline Rigidbody2D_t939494601 ** get_address_of_m_Rigidbody2D_15() { return &___m_Rigidbody2D_15; }
	inline void set_m_Rigidbody2D_15(Rigidbody2D_t939494601 * value)
	{
		___m_Rigidbody2D_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody2D_15), value);
	}

	inline static int32_t get_offset_of_m_FacingRight_16() { return static_cast<int32_t>(offsetof(PlatformerCharacter2D_t675295753, ___m_FacingRight_16)); }
	inline bool get_m_FacingRight_16() const { return ___m_FacingRight_16; }
	inline bool* get_address_of_m_FacingRight_16() { return &___m_FacingRight_16; }
	inline void set_m_FacingRight_16(bool value)
	{
		___m_FacingRight_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMERCHARACTER2D_T675295753_H
#ifndef RESTARTER_T269523250_H
#define RESTARTER_T269523250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets._2D.Restarter
struct  Restarter_t269523250  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTARTER_T269523250_H
#ifndef LOOKATTARGET_T3260877718_H
#define LOOKATTARGET_T3260877718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.LookatTarget
struct  LookatTarget_t3260877718  : public AbstractTargetFollower_t1919708159
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.Cameras.LookatTarget::m_RotationRange
	Vector2_t2156229523  ___m_RotationRange_8;
	// System.Single UnityStandardAssets.Cameras.LookatTarget::m_FollowSpeed
	float ___m_FollowSpeed_9;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.LookatTarget::m_FollowAngles
	Vector3_t3722313464  ___m_FollowAngles_10;
	// UnityEngine.Quaternion UnityStandardAssets.Cameras.LookatTarget::m_OriginalRotation
	Quaternion_t2301928331  ___m_OriginalRotation_11;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.LookatTarget::m_FollowVelocity
	Vector3_t3722313464  ___m_FollowVelocity_12;

public:
	inline static int32_t get_offset_of_m_RotationRange_8() { return static_cast<int32_t>(offsetof(LookatTarget_t3260877718, ___m_RotationRange_8)); }
	inline Vector2_t2156229523  get_m_RotationRange_8() const { return ___m_RotationRange_8; }
	inline Vector2_t2156229523 * get_address_of_m_RotationRange_8() { return &___m_RotationRange_8; }
	inline void set_m_RotationRange_8(Vector2_t2156229523  value)
	{
		___m_RotationRange_8 = value;
	}

	inline static int32_t get_offset_of_m_FollowSpeed_9() { return static_cast<int32_t>(offsetof(LookatTarget_t3260877718, ___m_FollowSpeed_9)); }
	inline float get_m_FollowSpeed_9() const { return ___m_FollowSpeed_9; }
	inline float* get_address_of_m_FollowSpeed_9() { return &___m_FollowSpeed_9; }
	inline void set_m_FollowSpeed_9(float value)
	{
		___m_FollowSpeed_9 = value;
	}

	inline static int32_t get_offset_of_m_FollowAngles_10() { return static_cast<int32_t>(offsetof(LookatTarget_t3260877718, ___m_FollowAngles_10)); }
	inline Vector3_t3722313464  get_m_FollowAngles_10() const { return ___m_FollowAngles_10; }
	inline Vector3_t3722313464 * get_address_of_m_FollowAngles_10() { return &___m_FollowAngles_10; }
	inline void set_m_FollowAngles_10(Vector3_t3722313464  value)
	{
		___m_FollowAngles_10 = value;
	}

	inline static int32_t get_offset_of_m_OriginalRotation_11() { return static_cast<int32_t>(offsetof(LookatTarget_t3260877718, ___m_OriginalRotation_11)); }
	inline Quaternion_t2301928331  get_m_OriginalRotation_11() const { return ___m_OriginalRotation_11; }
	inline Quaternion_t2301928331 * get_address_of_m_OriginalRotation_11() { return &___m_OriginalRotation_11; }
	inline void set_m_OriginalRotation_11(Quaternion_t2301928331  value)
	{
		___m_OriginalRotation_11 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_12() { return static_cast<int32_t>(offsetof(LookatTarget_t3260877718, ___m_FollowVelocity_12)); }
	inline Vector3_t3722313464  get_m_FollowVelocity_12() const { return ___m_FollowVelocity_12; }
	inline Vector3_t3722313464 * get_address_of_m_FollowVelocity_12() { return &___m_FollowVelocity_12; }
	inline void set_m_FollowVelocity_12(Vector3_t3722313464  value)
	{
		___m_FollowVelocity_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKATTARGET_T3260877718_H
#ifndef PIVOTBASEDCAMERARIG_T3786953582_H
#define PIVOTBASEDCAMERARIG_T3786953582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.PivotBasedCameraRig
struct  PivotBasedCameraRig_t3786953582  : public AbstractTargetFollower_t1919708159
{
public:
	// UnityEngine.Transform UnityStandardAssets.Cameras.PivotBasedCameraRig::m_Cam
	Transform_t3600365921 * ___m_Cam_8;
	// UnityEngine.Transform UnityStandardAssets.Cameras.PivotBasedCameraRig::m_Pivot
	Transform_t3600365921 * ___m_Pivot_9;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.PivotBasedCameraRig::m_LastTargetPosition
	Vector3_t3722313464  ___m_LastTargetPosition_10;

public:
	inline static int32_t get_offset_of_m_Cam_8() { return static_cast<int32_t>(offsetof(PivotBasedCameraRig_t3786953582, ___m_Cam_8)); }
	inline Transform_t3600365921 * get_m_Cam_8() const { return ___m_Cam_8; }
	inline Transform_t3600365921 ** get_address_of_m_Cam_8() { return &___m_Cam_8; }
	inline void set_m_Cam_8(Transform_t3600365921 * value)
	{
		___m_Cam_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_8), value);
	}

	inline static int32_t get_offset_of_m_Pivot_9() { return static_cast<int32_t>(offsetof(PivotBasedCameraRig_t3786953582, ___m_Pivot_9)); }
	inline Transform_t3600365921 * get_m_Pivot_9() const { return ___m_Pivot_9; }
	inline Transform_t3600365921 ** get_address_of_m_Pivot_9() { return &___m_Pivot_9; }
	inline void set_m_Pivot_9(Transform_t3600365921 * value)
	{
		___m_Pivot_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Pivot_9), value);
	}

	inline static int32_t get_offset_of_m_LastTargetPosition_10() { return static_cast<int32_t>(offsetof(PivotBasedCameraRig_t3786953582, ___m_LastTargetPosition_10)); }
	inline Vector3_t3722313464  get_m_LastTargetPosition_10() const { return ___m_LastTargetPosition_10; }
	inline Vector3_t3722313464 * get_address_of_m_LastTargetPosition_10() { return &___m_LastTargetPosition_10; }
	inline void set_m_LastTargetPosition_10(Vector3_t3722313464  value)
	{
		___m_LastTargetPosition_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOTBASEDCAMERARIG_T3786953582_H
#ifndef TARGETFIELDOFVIEW_T3060904718_H
#define TARGETFIELDOFVIEW_T3060904718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.TargetFieldOfView
struct  TargetFieldOfView_t3060904718  : public AbstractTargetFollower_t1919708159
{
public:
	// System.Single UnityStandardAssets.Cameras.TargetFieldOfView::m_FovAdjustTime
	float ___m_FovAdjustTime_8;
	// System.Single UnityStandardAssets.Cameras.TargetFieldOfView::m_ZoomAmountMultiplier
	float ___m_ZoomAmountMultiplier_9;
	// System.Boolean UnityStandardAssets.Cameras.TargetFieldOfView::m_IncludeEffectsInSize
	bool ___m_IncludeEffectsInSize_10;
	// System.Single UnityStandardAssets.Cameras.TargetFieldOfView::m_BoundSize
	float ___m_BoundSize_11;
	// System.Single UnityStandardAssets.Cameras.TargetFieldOfView::m_FovAdjustVelocity
	float ___m_FovAdjustVelocity_12;
	// UnityEngine.Camera UnityStandardAssets.Cameras.TargetFieldOfView::m_Cam
	Camera_t4157153871 * ___m_Cam_13;
	// UnityEngine.Transform UnityStandardAssets.Cameras.TargetFieldOfView::m_LastTarget
	Transform_t3600365921 * ___m_LastTarget_14;

public:
	inline static int32_t get_offset_of_m_FovAdjustTime_8() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_FovAdjustTime_8)); }
	inline float get_m_FovAdjustTime_8() const { return ___m_FovAdjustTime_8; }
	inline float* get_address_of_m_FovAdjustTime_8() { return &___m_FovAdjustTime_8; }
	inline void set_m_FovAdjustTime_8(float value)
	{
		___m_FovAdjustTime_8 = value;
	}

	inline static int32_t get_offset_of_m_ZoomAmountMultiplier_9() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_ZoomAmountMultiplier_9)); }
	inline float get_m_ZoomAmountMultiplier_9() const { return ___m_ZoomAmountMultiplier_9; }
	inline float* get_address_of_m_ZoomAmountMultiplier_9() { return &___m_ZoomAmountMultiplier_9; }
	inline void set_m_ZoomAmountMultiplier_9(float value)
	{
		___m_ZoomAmountMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_m_IncludeEffectsInSize_10() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_IncludeEffectsInSize_10)); }
	inline bool get_m_IncludeEffectsInSize_10() const { return ___m_IncludeEffectsInSize_10; }
	inline bool* get_address_of_m_IncludeEffectsInSize_10() { return &___m_IncludeEffectsInSize_10; }
	inline void set_m_IncludeEffectsInSize_10(bool value)
	{
		___m_IncludeEffectsInSize_10 = value;
	}

	inline static int32_t get_offset_of_m_BoundSize_11() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_BoundSize_11)); }
	inline float get_m_BoundSize_11() const { return ___m_BoundSize_11; }
	inline float* get_address_of_m_BoundSize_11() { return &___m_BoundSize_11; }
	inline void set_m_BoundSize_11(float value)
	{
		___m_BoundSize_11 = value;
	}

	inline static int32_t get_offset_of_m_FovAdjustVelocity_12() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_FovAdjustVelocity_12)); }
	inline float get_m_FovAdjustVelocity_12() const { return ___m_FovAdjustVelocity_12; }
	inline float* get_address_of_m_FovAdjustVelocity_12() { return &___m_FovAdjustVelocity_12; }
	inline void set_m_FovAdjustVelocity_12(float value)
	{
		___m_FovAdjustVelocity_12 = value;
	}

	inline static int32_t get_offset_of_m_Cam_13() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_Cam_13)); }
	inline Camera_t4157153871 * get_m_Cam_13() const { return ___m_Cam_13; }
	inline Camera_t4157153871 ** get_address_of_m_Cam_13() { return &___m_Cam_13; }
	inline void set_m_Cam_13(Camera_t4157153871 * value)
	{
		___m_Cam_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_13), value);
	}

	inline static int32_t get_offset_of_m_LastTarget_14() { return static_cast<int32_t>(offsetof(TargetFieldOfView_t3060904718, ___m_LastTarget_14)); }
	inline Transform_t3600365921 * get_m_LastTarget_14() const { return ___m_LastTarget_14; }
	inline Transform_t3600365921 ** get_address_of_m_LastTarget_14() { return &___m_LastTarget_14; }
	inline void set_m_LastTarget_14(Transform_t3600365921 * value)
	{
		___m_LastTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastTarget_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFIELDOFVIEW_T3060904718_H
#ifndef BLOOM_T1125654350_H
#define BLOOM_T1125654350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.Bloom
struct  Bloom_t1125654350  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.Bloom/TweakMode UnityStandardAssets.ImageEffects.Bloom::tweakMode
	int32_t ___tweakMode_7;
	// UnityStandardAssets.ImageEffects.Bloom/BloomScreenBlendMode UnityStandardAssets.ImageEffects.Bloom::screenBlendMode
	int32_t ___screenBlendMode_8;
	// UnityStandardAssets.ImageEffects.Bloom/HDRBloomMode UnityStandardAssets.ImageEffects.Bloom::hdr
	int32_t ___hdr_9;
	// System.Boolean UnityStandardAssets.ImageEffects.Bloom::doHdr
	bool ___doHdr_10;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::sepBlurSpread
	float ___sepBlurSpread_11;
	// UnityStandardAssets.ImageEffects.Bloom/BloomQuality UnityStandardAssets.ImageEffects.Bloom::quality
	int32_t ___quality_12;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::bloomIntensity
	float ___bloomIntensity_13;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::bloomThreshold
	float ___bloomThreshold_14;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::bloomThresholdColor
	Color_t2555686324  ___bloomThresholdColor_15;
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom::bloomBlurIterations
	int32_t ___bloomBlurIterations_16;
	// System.Int32 UnityStandardAssets.ImageEffects.Bloom::hollywoodFlareBlurIterations
	int32_t ___hollywoodFlareBlurIterations_17;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::flareRotation
	float ___flareRotation_18;
	// UnityStandardAssets.ImageEffects.Bloom/LensFlareStyle UnityStandardAssets.ImageEffects.Bloom::lensflareMode
	int32_t ___lensflareMode_19;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::hollyStretchWidth
	float ___hollyStretchWidth_20;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::lensflareIntensity
	float ___lensflareIntensity_21;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::lensflareThreshold
	float ___lensflareThreshold_22;
	// System.Single UnityStandardAssets.ImageEffects.Bloom::lensFlareSaturation
	float ___lensFlareSaturation_23;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorA
	Color_t2555686324  ___flareColorA_24;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorB
	Color_t2555686324  ___flareColorB_25;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorC
	Color_t2555686324  ___flareColorC_26;
	// UnityEngine.Color UnityStandardAssets.ImageEffects.Bloom::flareColorD
	Color_t2555686324  ___flareColorD_27;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.Bloom::lensFlareVignetteMask
	Texture2D_t3840446185 * ___lensFlareVignetteMask_28;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::lensFlareShader
	Shader_t4151988712 * ___lensFlareShader_29;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::lensFlareMaterial
	Material_t340375123 * ___lensFlareMaterial_30;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::screenBlendShader
	Shader_t4151988712 * ___screenBlendShader_31;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::screenBlend
	Material_t340375123 * ___screenBlend_32;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::blurAndFlaresShader
	Shader_t4151988712 * ___blurAndFlaresShader_33;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::blurAndFlaresMaterial
	Material_t340375123 * ___blurAndFlaresMaterial_34;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.Bloom::brightPassFilterShader
	Shader_t4151988712 * ___brightPassFilterShader_35;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.Bloom::brightPassFilterMaterial
	Material_t340375123 * ___brightPassFilterMaterial_36;

public:
	inline static int32_t get_offset_of_tweakMode_7() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___tweakMode_7)); }
	inline int32_t get_tweakMode_7() const { return ___tweakMode_7; }
	inline int32_t* get_address_of_tweakMode_7() { return &___tweakMode_7; }
	inline void set_tweakMode_7(int32_t value)
	{
		___tweakMode_7 = value;
	}

	inline static int32_t get_offset_of_screenBlendMode_8() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___screenBlendMode_8)); }
	inline int32_t get_screenBlendMode_8() const { return ___screenBlendMode_8; }
	inline int32_t* get_address_of_screenBlendMode_8() { return &___screenBlendMode_8; }
	inline void set_screenBlendMode_8(int32_t value)
	{
		___screenBlendMode_8 = value;
	}

	inline static int32_t get_offset_of_hdr_9() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___hdr_9)); }
	inline int32_t get_hdr_9() const { return ___hdr_9; }
	inline int32_t* get_address_of_hdr_9() { return &___hdr_9; }
	inline void set_hdr_9(int32_t value)
	{
		___hdr_9 = value;
	}

	inline static int32_t get_offset_of_doHdr_10() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___doHdr_10)); }
	inline bool get_doHdr_10() const { return ___doHdr_10; }
	inline bool* get_address_of_doHdr_10() { return &___doHdr_10; }
	inline void set_doHdr_10(bool value)
	{
		___doHdr_10 = value;
	}

	inline static int32_t get_offset_of_sepBlurSpread_11() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___sepBlurSpread_11)); }
	inline float get_sepBlurSpread_11() const { return ___sepBlurSpread_11; }
	inline float* get_address_of_sepBlurSpread_11() { return &___sepBlurSpread_11; }
	inline void set_sepBlurSpread_11(float value)
	{
		___sepBlurSpread_11 = value;
	}

	inline static int32_t get_offset_of_quality_12() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___quality_12)); }
	inline int32_t get_quality_12() const { return ___quality_12; }
	inline int32_t* get_address_of_quality_12() { return &___quality_12; }
	inline void set_quality_12(int32_t value)
	{
		___quality_12 = value;
	}

	inline static int32_t get_offset_of_bloomIntensity_13() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___bloomIntensity_13)); }
	inline float get_bloomIntensity_13() const { return ___bloomIntensity_13; }
	inline float* get_address_of_bloomIntensity_13() { return &___bloomIntensity_13; }
	inline void set_bloomIntensity_13(float value)
	{
		___bloomIntensity_13 = value;
	}

	inline static int32_t get_offset_of_bloomThreshold_14() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___bloomThreshold_14)); }
	inline float get_bloomThreshold_14() const { return ___bloomThreshold_14; }
	inline float* get_address_of_bloomThreshold_14() { return &___bloomThreshold_14; }
	inline void set_bloomThreshold_14(float value)
	{
		___bloomThreshold_14 = value;
	}

	inline static int32_t get_offset_of_bloomThresholdColor_15() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___bloomThresholdColor_15)); }
	inline Color_t2555686324  get_bloomThresholdColor_15() const { return ___bloomThresholdColor_15; }
	inline Color_t2555686324 * get_address_of_bloomThresholdColor_15() { return &___bloomThresholdColor_15; }
	inline void set_bloomThresholdColor_15(Color_t2555686324  value)
	{
		___bloomThresholdColor_15 = value;
	}

	inline static int32_t get_offset_of_bloomBlurIterations_16() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___bloomBlurIterations_16)); }
	inline int32_t get_bloomBlurIterations_16() const { return ___bloomBlurIterations_16; }
	inline int32_t* get_address_of_bloomBlurIterations_16() { return &___bloomBlurIterations_16; }
	inline void set_bloomBlurIterations_16(int32_t value)
	{
		___bloomBlurIterations_16 = value;
	}

	inline static int32_t get_offset_of_hollywoodFlareBlurIterations_17() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___hollywoodFlareBlurIterations_17)); }
	inline int32_t get_hollywoodFlareBlurIterations_17() const { return ___hollywoodFlareBlurIterations_17; }
	inline int32_t* get_address_of_hollywoodFlareBlurIterations_17() { return &___hollywoodFlareBlurIterations_17; }
	inline void set_hollywoodFlareBlurIterations_17(int32_t value)
	{
		___hollywoodFlareBlurIterations_17 = value;
	}

	inline static int32_t get_offset_of_flareRotation_18() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___flareRotation_18)); }
	inline float get_flareRotation_18() const { return ___flareRotation_18; }
	inline float* get_address_of_flareRotation_18() { return &___flareRotation_18; }
	inline void set_flareRotation_18(float value)
	{
		___flareRotation_18 = value;
	}

	inline static int32_t get_offset_of_lensflareMode_19() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensflareMode_19)); }
	inline int32_t get_lensflareMode_19() const { return ___lensflareMode_19; }
	inline int32_t* get_address_of_lensflareMode_19() { return &___lensflareMode_19; }
	inline void set_lensflareMode_19(int32_t value)
	{
		___lensflareMode_19 = value;
	}

	inline static int32_t get_offset_of_hollyStretchWidth_20() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___hollyStretchWidth_20)); }
	inline float get_hollyStretchWidth_20() const { return ___hollyStretchWidth_20; }
	inline float* get_address_of_hollyStretchWidth_20() { return &___hollyStretchWidth_20; }
	inline void set_hollyStretchWidth_20(float value)
	{
		___hollyStretchWidth_20 = value;
	}

	inline static int32_t get_offset_of_lensflareIntensity_21() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensflareIntensity_21)); }
	inline float get_lensflareIntensity_21() const { return ___lensflareIntensity_21; }
	inline float* get_address_of_lensflareIntensity_21() { return &___lensflareIntensity_21; }
	inline void set_lensflareIntensity_21(float value)
	{
		___lensflareIntensity_21 = value;
	}

	inline static int32_t get_offset_of_lensflareThreshold_22() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensflareThreshold_22)); }
	inline float get_lensflareThreshold_22() const { return ___lensflareThreshold_22; }
	inline float* get_address_of_lensflareThreshold_22() { return &___lensflareThreshold_22; }
	inline void set_lensflareThreshold_22(float value)
	{
		___lensflareThreshold_22 = value;
	}

	inline static int32_t get_offset_of_lensFlareSaturation_23() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensFlareSaturation_23)); }
	inline float get_lensFlareSaturation_23() const { return ___lensFlareSaturation_23; }
	inline float* get_address_of_lensFlareSaturation_23() { return &___lensFlareSaturation_23; }
	inline void set_lensFlareSaturation_23(float value)
	{
		___lensFlareSaturation_23 = value;
	}

	inline static int32_t get_offset_of_flareColorA_24() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___flareColorA_24)); }
	inline Color_t2555686324  get_flareColorA_24() const { return ___flareColorA_24; }
	inline Color_t2555686324 * get_address_of_flareColorA_24() { return &___flareColorA_24; }
	inline void set_flareColorA_24(Color_t2555686324  value)
	{
		___flareColorA_24 = value;
	}

	inline static int32_t get_offset_of_flareColorB_25() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___flareColorB_25)); }
	inline Color_t2555686324  get_flareColorB_25() const { return ___flareColorB_25; }
	inline Color_t2555686324 * get_address_of_flareColorB_25() { return &___flareColorB_25; }
	inline void set_flareColorB_25(Color_t2555686324  value)
	{
		___flareColorB_25 = value;
	}

	inline static int32_t get_offset_of_flareColorC_26() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___flareColorC_26)); }
	inline Color_t2555686324  get_flareColorC_26() const { return ___flareColorC_26; }
	inline Color_t2555686324 * get_address_of_flareColorC_26() { return &___flareColorC_26; }
	inline void set_flareColorC_26(Color_t2555686324  value)
	{
		___flareColorC_26 = value;
	}

	inline static int32_t get_offset_of_flareColorD_27() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___flareColorD_27)); }
	inline Color_t2555686324  get_flareColorD_27() const { return ___flareColorD_27; }
	inline Color_t2555686324 * get_address_of_flareColorD_27() { return &___flareColorD_27; }
	inline void set_flareColorD_27(Color_t2555686324  value)
	{
		___flareColorD_27 = value;
	}

	inline static int32_t get_offset_of_lensFlareVignetteMask_28() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensFlareVignetteMask_28)); }
	inline Texture2D_t3840446185 * get_lensFlareVignetteMask_28() const { return ___lensFlareVignetteMask_28; }
	inline Texture2D_t3840446185 ** get_address_of_lensFlareVignetteMask_28() { return &___lensFlareVignetteMask_28; }
	inline void set_lensFlareVignetteMask_28(Texture2D_t3840446185 * value)
	{
		___lensFlareVignetteMask_28 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareVignetteMask_28), value);
	}

	inline static int32_t get_offset_of_lensFlareShader_29() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensFlareShader_29)); }
	inline Shader_t4151988712 * get_lensFlareShader_29() const { return ___lensFlareShader_29; }
	inline Shader_t4151988712 ** get_address_of_lensFlareShader_29() { return &___lensFlareShader_29; }
	inline void set_lensFlareShader_29(Shader_t4151988712 * value)
	{
		___lensFlareShader_29 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareShader_29), value);
	}

	inline static int32_t get_offset_of_lensFlareMaterial_30() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___lensFlareMaterial_30)); }
	inline Material_t340375123 * get_lensFlareMaterial_30() const { return ___lensFlareMaterial_30; }
	inline Material_t340375123 ** get_address_of_lensFlareMaterial_30() { return &___lensFlareMaterial_30; }
	inline void set_lensFlareMaterial_30(Material_t340375123 * value)
	{
		___lensFlareMaterial_30 = value;
		Il2CppCodeGenWriteBarrier((&___lensFlareMaterial_30), value);
	}

	inline static int32_t get_offset_of_screenBlendShader_31() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___screenBlendShader_31)); }
	inline Shader_t4151988712 * get_screenBlendShader_31() const { return ___screenBlendShader_31; }
	inline Shader_t4151988712 ** get_address_of_screenBlendShader_31() { return &___screenBlendShader_31; }
	inline void set_screenBlendShader_31(Shader_t4151988712 * value)
	{
		___screenBlendShader_31 = value;
		Il2CppCodeGenWriteBarrier((&___screenBlendShader_31), value);
	}

	inline static int32_t get_offset_of_screenBlend_32() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___screenBlend_32)); }
	inline Material_t340375123 * get_screenBlend_32() const { return ___screenBlend_32; }
	inline Material_t340375123 ** get_address_of_screenBlend_32() { return &___screenBlend_32; }
	inline void set_screenBlend_32(Material_t340375123 * value)
	{
		___screenBlend_32 = value;
		Il2CppCodeGenWriteBarrier((&___screenBlend_32), value);
	}

	inline static int32_t get_offset_of_blurAndFlaresShader_33() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___blurAndFlaresShader_33)); }
	inline Shader_t4151988712 * get_blurAndFlaresShader_33() const { return ___blurAndFlaresShader_33; }
	inline Shader_t4151988712 ** get_address_of_blurAndFlaresShader_33() { return &___blurAndFlaresShader_33; }
	inline void set_blurAndFlaresShader_33(Shader_t4151988712 * value)
	{
		___blurAndFlaresShader_33 = value;
		Il2CppCodeGenWriteBarrier((&___blurAndFlaresShader_33), value);
	}

	inline static int32_t get_offset_of_blurAndFlaresMaterial_34() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___blurAndFlaresMaterial_34)); }
	inline Material_t340375123 * get_blurAndFlaresMaterial_34() const { return ___blurAndFlaresMaterial_34; }
	inline Material_t340375123 ** get_address_of_blurAndFlaresMaterial_34() { return &___blurAndFlaresMaterial_34; }
	inline void set_blurAndFlaresMaterial_34(Material_t340375123 * value)
	{
		___blurAndFlaresMaterial_34 = value;
		Il2CppCodeGenWriteBarrier((&___blurAndFlaresMaterial_34), value);
	}

	inline static int32_t get_offset_of_brightPassFilterShader_35() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___brightPassFilterShader_35)); }
	inline Shader_t4151988712 * get_brightPassFilterShader_35() const { return ___brightPassFilterShader_35; }
	inline Shader_t4151988712 ** get_address_of_brightPassFilterShader_35() { return &___brightPassFilterShader_35; }
	inline void set_brightPassFilterShader_35(Shader_t4151988712 * value)
	{
		___brightPassFilterShader_35 = value;
		Il2CppCodeGenWriteBarrier((&___brightPassFilterShader_35), value);
	}

	inline static int32_t get_offset_of_brightPassFilterMaterial_36() { return static_cast<int32_t>(offsetof(Bloom_t1125654350, ___brightPassFilterMaterial_36)); }
	inline Material_t340375123 * get_brightPassFilterMaterial_36() const { return ___brightPassFilterMaterial_36; }
	inline Material_t340375123 ** get_address_of_brightPassFilterMaterial_36() { return &___brightPassFilterMaterial_36; }
	inline void set_brightPassFilterMaterial_36(Material_t340375123 * value)
	{
		___brightPassFilterMaterial_36 = value;
		Il2CppCodeGenWriteBarrier((&___brightPassFilterMaterial_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOM_T1125654350_H
#ifndef SCREENOVERLAY_T3772274400_H
#define SCREENOVERLAY_T3772274400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenOverlay
struct  ScreenOverlay_t3772274400  : public PostEffectsBase_t2404315739
{
public:
	// UnityStandardAssets.ImageEffects.ScreenOverlay/OverlayBlendMode UnityStandardAssets.ImageEffects.ScreenOverlay::blendMode
	int32_t ___blendMode_7;
	// System.Single UnityStandardAssets.ImageEffects.ScreenOverlay::intensity
	float ___intensity_8;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ScreenOverlay::texture
	Texture2D_t3840446185 * ___texture_9;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ScreenOverlay::overlayShader
	Shader_t4151988712 * ___overlayShader_10;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenOverlay::overlayMaterial
	Material_t340375123 * ___overlayMaterial_11;

public:
	inline static int32_t get_offset_of_blendMode_7() { return static_cast<int32_t>(offsetof(ScreenOverlay_t3772274400, ___blendMode_7)); }
	inline int32_t get_blendMode_7() const { return ___blendMode_7; }
	inline int32_t* get_address_of_blendMode_7() { return &___blendMode_7; }
	inline void set_blendMode_7(int32_t value)
	{
		___blendMode_7 = value;
	}

	inline static int32_t get_offset_of_intensity_8() { return static_cast<int32_t>(offsetof(ScreenOverlay_t3772274400, ___intensity_8)); }
	inline float get_intensity_8() const { return ___intensity_8; }
	inline float* get_address_of_intensity_8() { return &___intensity_8; }
	inline void set_intensity_8(float value)
	{
		___intensity_8 = value;
	}

	inline static int32_t get_offset_of_texture_9() { return static_cast<int32_t>(offsetof(ScreenOverlay_t3772274400, ___texture_9)); }
	inline Texture2D_t3840446185 * get_texture_9() const { return ___texture_9; }
	inline Texture2D_t3840446185 ** get_address_of_texture_9() { return &___texture_9; }
	inline void set_texture_9(Texture2D_t3840446185 * value)
	{
		___texture_9 = value;
		Il2CppCodeGenWriteBarrier((&___texture_9), value);
	}

	inline static int32_t get_offset_of_overlayShader_10() { return static_cast<int32_t>(offsetof(ScreenOverlay_t3772274400, ___overlayShader_10)); }
	inline Shader_t4151988712 * get_overlayShader_10() const { return ___overlayShader_10; }
	inline Shader_t4151988712 ** get_address_of_overlayShader_10() { return &___overlayShader_10; }
	inline void set_overlayShader_10(Shader_t4151988712 * value)
	{
		___overlayShader_10 = value;
		Il2CppCodeGenWriteBarrier((&___overlayShader_10), value);
	}

	inline static int32_t get_offset_of_overlayMaterial_11() { return static_cast<int32_t>(offsetof(ScreenOverlay_t3772274400, ___overlayMaterial_11)); }
	inline Material_t340375123 * get_overlayMaterial_11() const { return ___overlayMaterial_11; }
	inline Material_t340375123 ** get_address_of_overlayMaterial_11() { return &___overlayMaterial_11; }
	inline void set_overlayMaterial_11(Material_t340375123 * value)
	{
		___overlayMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&___overlayMaterial_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENOVERLAY_T3772274400_H
#ifndef SCREENSPACEAMBIENTOBSCURANCE_T1844081910_H
#define SCREENSPACEAMBIENTOBSCURANCE_T1844081910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance
struct  ScreenSpaceAmbientObscurance_t1844081910  : public PostEffectsBase_t2404315739
{
public:
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::intensity
	float ___intensity_7;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::radius
	float ___radius_8;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::blurIterations
	int32_t ___blurIterations_9;
	// System.Single UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::blurFilterDistance
	float ___blurFilterDistance_10;
	// System.Int32 UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::downsample
	int32_t ___downsample_11;
	// UnityEngine.Texture2D UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::rand
	Texture2D_t3840446185 * ___rand_12;
	// UnityEngine.Shader UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::aoShader
	Shader_t4151988712 * ___aoShader_13;
	// UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::aoMaterial
	Material_t340375123 * ___aoMaterial_14;

public:
	inline static int32_t get_offset_of_intensity_7() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___intensity_7)); }
	inline float get_intensity_7() const { return ___intensity_7; }
	inline float* get_address_of_intensity_7() { return &___intensity_7; }
	inline void set_intensity_7(float value)
	{
		___intensity_7 = value;
	}

	inline static int32_t get_offset_of_radius_8() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___radius_8)); }
	inline float get_radius_8() const { return ___radius_8; }
	inline float* get_address_of_radius_8() { return &___radius_8; }
	inline void set_radius_8(float value)
	{
		___radius_8 = value;
	}

	inline static int32_t get_offset_of_blurIterations_9() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___blurIterations_9)); }
	inline int32_t get_blurIterations_9() const { return ___blurIterations_9; }
	inline int32_t* get_address_of_blurIterations_9() { return &___blurIterations_9; }
	inline void set_blurIterations_9(int32_t value)
	{
		___blurIterations_9 = value;
	}

	inline static int32_t get_offset_of_blurFilterDistance_10() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___blurFilterDistance_10)); }
	inline float get_blurFilterDistance_10() const { return ___blurFilterDistance_10; }
	inline float* get_address_of_blurFilterDistance_10() { return &___blurFilterDistance_10; }
	inline void set_blurFilterDistance_10(float value)
	{
		___blurFilterDistance_10 = value;
	}

	inline static int32_t get_offset_of_downsample_11() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___downsample_11)); }
	inline int32_t get_downsample_11() const { return ___downsample_11; }
	inline int32_t* get_address_of_downsample_11() { return &___downsample_11; }
	inline void set_downsample_11(int32_t value)
	{
		___downsample_11 = value;
	}

	inline static int32_t get_offset_of_rand_12() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___rand_12)); }
	inline Texture2D_t3840446185 * get_rand_12() const { return ___rand_12; }
	inline Texture2D_t3840446185 ** get_address_of_rand_12() { return &___rand_12; }
	inline void set_rand_12(Texture2D_t3840446185 * value)
	{
		___rand_12 = value;
		Il2CppCodeGenWriteBarrier((&___rand_12), value);
	}

	inline static int32_t get_offset_of_aoShader_13() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___aoShader_13)); }
	inline Shader_t4151988712 * get_aoShader_13() const { return ___aoShader_13; }
	inline Shader_t4151988712 ** get_address_of_aoShader_13() { return &___aoShader_13; }
	inline void set_aoShader_13(Shader_t4151988712 * value)
	{
		___aoShader_13 = value;
		Il2CppCodeGenWriteBarrier((&___aoShader_13), value);
	}

	inline static int32_t get_offset_of_aoMaterial_14() { return static_cast<int32_t>(offsetof(ScreenSpaceAmbientObscurance_t1844081910, ___aoMaterial_14)); }
	inline Material_t340375123 * get_aoMaterial_14() const { return ___aoMaterial_14; }
	inline Material_t340375123 ** get_address_of_aoMaterial_14() { return &___aoMaterial_14; }
	inline void set_aoMaterial_14(Material_t340375123 * value)
	{
		___aoMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___aoMaterial_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEAMBIENTOBSCURANCE_T1844081910_H
#ifndef GERSTNERDISPLACE_T2537102777_H
#define GERSTNERDISPLACE_T2537102777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.GerstnerDisplace
struct  GerstnerDisplace_t2537102777  : public Displace_t1352130581
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GERSTNERDISPLACE_T2537102777_H
#ifndef AUTOCAM_T137911967_H
#define AUTOCAM_T137911967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.AutoCam
struct  AutoCam_t137911967  : public PivotBasedCameraRig_t3786953582
{
public:
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_MoveSpeed
	float ___m_MoveSpeed_11;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_TurnSpeed
	float ___m_TurnSpeed_12;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_RollSpeed
	float ___m_RollSpeed_13;
	// System.Boolean UnityStandardAssets.Cameras.AutoCam::m_FollowVelocity
	bool ___m_FollowVelocity_14;
	// System.Boolean UnityStandardAssets.Cameras.AutoCam::m_FollowTilt
	bool ___m_FollowTilt_15;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_SpinTurnLimit
	float ___m_SpinTurnLimit_16;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_TargetVelocityLowerLimit
	float ___m_TargetVelocityLowerLimit_17;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_SmoothTurnTime
	float ___m_SmoothTurnTime_18;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_LastFlatAngle
	float ___m_LastFlatAngle_19;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_CurrentTurnAmount
	float ___m_CurrentTurnAmount_20;
	// System.Single UnityStandardAssets.Cameras.AutoCam::m_TurnSpeedVelocityChange
	float ___m_TurnSpeedVelocityChange_21;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.AutoCam::m_RollUp
	Vector3_t3722313464  ___m_RollUp_22;

public:
	inline static int32_t get_offset_of_m_MoveSpeed_11() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_MoveSpeed_11)); }
	inline float get_m_MoveSpeed_11() const { return ___m_MoveSpeed_11; }
	inline float* get_address_of_m_MoveSpeed_11() { return &___m_MoveSpeed_11; }
	inline void set_m_MoveSpeed_11(float value)
	{
		___m_MoveSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_TurnSpeed_12() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_TurnSpeed_12)); }
	inline float get_m_TurnSpeed_12() const { return ___m_TurnSpeed_12; }
	inline float* get_address_of_m_TurnSpeed_12() { return &___m_TurnSpeed_12; }
	inline void set_m_TurnSpeed_12(float value)
	{
		___m_TurnSpeed_12 = value;
	}

	inline static int32_t get_offset_of_m_RollSpeed_13() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_RollSpeed_13)); }
	inline float get_m_RollSpeed_13() const { return ___m_RollSpeed_13; }
	inline float* get_address_of_m_RollSpeed_13() { return &___m_RollSpeed_13; }
	inline void set_m_RollSpeed_13(float value)
	{
		___m_RollSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_14() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_FollowVelocity_14)); }
	inline bool get_m_FollowVelocity_14() const { return ___m_FollowVelocity_14; }
	inline bool* get_address_of_m_FollowVelocity_14() { return &___m_FollowVelocity_14; }
	inline void set_m_FollowVelocity_14(bool value)
	{
		___m_FollowVelocity_14 = value;
	}

	inline static int32_t get_offset_of_m_FollowTilt_15() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_FollowTilt_15)); }
	inline bool get_m_FollowTilt_15() const { return ___m_FollowTilt_15; }
	inline bool* get_address_of_m_FollowTilt_15() { return &___m_FollowTilt_15; }
	inline void set_m_FollowTilt_15(bool value)
	{
		___m_FollowTilt_15 = value;
	}

	inline static int32_t get_offset_of_m_SpinTurnLimit_16() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_SpinTurnLimit_16)); }
	inline float get_m_SpinTurnLimit_16() const { return ___m_SpinTurnLimit_16; }
	inline float* get_address_of_m_SpinTurnLimit_16() { return &___m_SpinTurnLimit_16; }
	inline void set_m_SpinTurnLimit_16(float value)
	{
		___m_SpinTurnLimit_16 = value;
	}

	inline static int32_t get_offset_of_m_TargetVelocityLowerLimit_17() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_TargetVelocityLowerLimit_17)); }
	inline float get_m_TargetVelocityLowerLimit_17() const { return ___m_TargetVelocityLowerLimit_17; }
	inline float* get_address_of_m_TargetVelocityLowerLimit_17() { return &___m_TargetVelocityLowerLimit_17; }
	inline void set_m_TargetVelocityLowerLimit_17(float value)
	{
		___m_TargetVelocityLowerLimit_17 = value;
	}

	inline static int32_t get_offset_of_m_SmoothTurnTime_18() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_SmoothTurnTime_18)); }
	inline float get_m_SmoothTurnTime_18() const { return ___m_SmoothTurnTime_18; }
	inline float* get_address_of_m_SmoothTurnTime_18() { return &___m_SmoothTurnTime_18; }
	inline void set_m_SmoothTurnTime_18(float value)
	{
		___m_SmoothTurnTime_18 = value;
	}

	inline static int32_t get_offset_of_m_LastFlatAngle_19() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_LastFlatAngle_19)); }
	inline float get_m_LastFlatAngle_19() const { return ___m_LastFlatAngle_19; }
	inline float* get_address_of_m_LastFlatAngle_19() { return &___m_LastFlatAngle_19; }
	inline void set_m_LastFlatAngle_19(float value)
	{
		___m_LastFlatAngle_19 = value;
	}

	inline static int32_t get_offset_of_m_CurrentTurnAmount_20() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_CurrentTurnAmount_20)); }
	inline float get_m_CurrentTurnAmount_20() const { return ___m_CurrentTurnAmount_20; }
	inline float* get_address_of_m_CurrentTurnAmount_20() { return &___m_CurrentTurnAmount_20; }
	inline void set_m_CurrentTurnAmount_20(float value)
	{
		___m_CurrentTurnAmount_20 = value;
	}

	inline static int32_t get_offset_of_m_TurnSpeedVelocityChange_21() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_TurnSpeedVelocityChange_21)); }
	inline float get_m_TurnSpeedVelocityChange_21() const { return ___m_TurnSpeedVelocityChange_21; }
	inline float* get_address_of_m_TurnSpeedVelocityChange_21() { return &___m_TurnSpeedVelocityChange_21; }
	inline void set_m_TurnSpeedVelocityChange_21(float value)
	{
		___m_TurnSpeedVelocityChange_21 = value;
	}

	inline static int32_t get_offset_of_m_RollUp_22() { return static_cast<int32_t>(offsetof(AutoCam_t137911967, ___m_RollUp_22)); }
	inline Vector3_t3722313464  get_m_RollUp_22() const { return ___m_RollUp_22; }
	inline Vector3_t3722313464 * get_address_of_m_RollUp_22() { return &___m_RollUp_22; }
	inline void set_m_RollUp_22(Vector3_t3722313464  value)
	{
		___m_RollUp_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOCAM_T137911967_H
#ifndef FREELOOKCAM_T2000732766_H
#define FREELOOKCAM_T2000732766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.FreeLookCam
struct  FreeLookCam_t2000732766  : public PivotBasedCameraRig_t3786953582
{
public:
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_MoveSpeed
	float ___m_MoveSpeed_11;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_TurnSpeed
	float ___m_TurnSpeed_12;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_TurnSmoothing
	float ___m_TurnSmoothing_13;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_TiltMax
	float ___m_TiltMax_14;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_TiltMin
	float ___m_TiltMin_15;
	// System.Boolean UnityStandardAssets.Cameras.FreeLookCam::m_LockCursor
	bool ___m_LockCursor_16;
	// System.Boolean UnityStandardAssets.Cameras.FreeLookCam::m_VerticalAutoReturn
	bool ___m_VerticalAutoReturn_17;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_LookAngle
	float ___m_LookAngle_18;
	// System.Single UnityStandardAssets.Cameras.FreeLookCam::m_TiltAngle
	float ___m_TiltAngle_19;
	// UnityEngine.Vector3 UnityStandardAssets.Cameras.FreeLookCam::m_PivotEulers
	Vector3_t3722313464  ___m_PivotEulers_21;
	// UnityEngine.Quaternion UnityStandardAssets.Cameras.FreeLookCam::m_PivotTargetRot
	Quaternion_t2301928331  ___m_PivotTargetRot_22;
	// UnityEngine.Quaternion UnityStandardAssets.Cameras.FreeLookCam::m_TransformTargetRot
	Quaternion_t2301928331  ___m_TransformTargetRot_23;

public:
	inline static int32_t get_offset_of_m_MoveSpeed_11() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_MoveSpeed_11)); }
	inline float get_m_MoveSpeed_11() const { return ___m_MoveSpeed_11; }
	inline float* get_address_of_m_MoveSpeed_11() { return &___m_MoveSpeed_11; }
	inline void set_m_MoveSpeed_11(float value)
	{
		___m_MoveSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_TurnSpeed_12() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TurnSpeed_12)); }
	inline float get_m_TurnSpeed_12() const { return ___m_TurnSpeed_12; }
	inline float* get_address_of_m_TurnSpeed_12() { return &___m_TurnSpeed_12; }
	inline void set_m_TurnSpeed_12(float value)
	{
		___m_TurnSpeed_12 = value;
	}

	inline static int32_t get_offset_of_m_TurnSmoothing_13() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TurnSmoothing_13)); }
	inline float get_m_TurnSmoothing_13() const { return ___m_TurnSmoothing_13; }
	inline float* get_address_of_m_TurnSmoothing_13() { return &___m_TurnSmoothing_13; }
	inline void set_m_TurnSmoothing_13(float value)
	{
		___m_TurnSmoothing_13 = value;
	}

	inline static int32_t get_offset_of_m_TiltMax_14() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TiltMax_14)); }
	inline float get_m_TiltMax_14() const { return ___m_TiltMax_14; }
	inline float* get_address_of_m_TiltMax_14() { return &___m_TiltMax_14; }
	inline void set_m_TiltMax_14(float value)
	{
		___m_TiltMax_14 = value;
	}

	inline static int32_t get_offset_of_m_TiltMin_15() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TiltMin_15)); }
	inline float get_m_TiltMin_15() const { return ___m_TiltMin_15; }
	inline float* get_address_of_m_TiltMin_15() { return &___m_TiltMin_15; }
	inline void set_m_TiltMin_15(float value)
	{
		___m_TiltMin_15 = value;
	}

	inline static int32_t get_offset_of_m_LockCursor_16() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_LockCursor_16)); }
	inline bool get_m_LockCursor_16() const { return ___m_LockCursor_16; }
	inline bool* get_address_of_m_LockCursor_16() { return &___m_LockCursor_16; }
	inline void set_m_LockCursor_16(bool value)
	{
		___m_LockCursor_16 = value;
	}

	inline static int32_t get_offset_of_m_VerticalAutoReturn_17() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_VerticalAutoReturn_17)); }
	inline bool get_m_VerticalAutoReturn_17() const { return ___m_VerticalAutoReturn_17; }
	inline bool* get_address_of_m_VerticalAutoReturn_17() { return &___m_VerticalAutoReturn_17; }
	inline void set_m_VerticalAutoReturn_17(bool value)
	{
		___m_VerticalAutoReturn_17 = value;
	}

	inline static int32_t get_offset_of_m_LookAngle_18() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_LookAngle_18)); }
	inline float get_m_LookAngle_18() const { return ___m_LookAngle_18; }
	inline float* get_address_of_m_LookAngle_18() { return &___m_LookAngle_18; }
	inline void set_m_LookAngle_18(float value)
	{
		___m_LookAngle_18 = value;
	}

	inline static int32_t get_offset_of_m_TiltAngle_19() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TiltAngle_19)); }
	inline float get_m_TiltAngle_19() const { return ___m_TiltAngle_19; }
	inline float* get_address_of_m_TiltAngle_19() { return &___m_TiltAngle_19; }
	inline void set_m_TiltAngle_19(float value)
	{
		___m_TiltAngle_19 = value;
	}

	inline static int32_t get_offset_of_m_PivotEulers_21() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_PivotEulers_21)); }
	inline Vector3_t3722313464  get_m_PivotEulers_21() const { return ___m_PivotEulers_21; }
	inline Vector3_t3722313464 * get_address_of_m_PivotEulers_21() { return &___m_PivotEulers_21; }
	inline void set_m_PivotEulers_21(Vector3_t3722313464  value)
	{
		___m_PivotEulers_21 = value;
	}

	inline static int32_t get_offset_of_m_PivotTargetRot_22() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_PivotTargetRot_22)); }
	inline Quaternion_t2301928331  get_m_PivotTargetRot_22() const { return ___m_PivotTargetRot_22; }
	inline Quaternion_t2301928331 * get_address_of_m_PivotTargetRot_22() { return &___m_PivotTargetRot_22; }
	inline void set_m_PivotTargetRot_22(Quaternion_t2301928331  value)
	{
		___m_PivotTargetRot_22 = value;
	}

	inline static int32_t get_offset_of_m_TransformTargetRot_23() { return static_cast<int32_t>(offsetof(FreeLookCam_t2000732766, ___m_TransformTargetRot_23)); }
	inline Quaternion_t2301928331  get_m_TransformTargetRot_23() const { return ___m_TransformTargetRot_23; }
	inline Quaternion_t2301928331 * get_address_of_m_TransformTargetRot_23() { return &___m_TransformTargetRot_23; }
	inline void set_m_TransformTargetRot_23(Quaternion_t2301928331  value)
	{
		___m_TransformTargetRot_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FREELOOKCAM_T2000732766_H
#ifndef HANDHELDCAM_T450595784_H
#define HANDHELDCAM_T450595784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Cameras.HandHeldCam
struct  HandHeldCam_t450595784  : public LookatTarget_t3260877718
{
public:
	// System.Single UnityStandardAssets.Cameras.HandHeldCam::m_SwaySpeed
	float ___m_SwaySpeed_13;
	// System.Single UnityStandardAssets.Cameras.HandHeldCam::m_BaseSwayAmount
	float ___m_BaseSwayAmount_14;
	// System.Single UnityStandardAssets.Cameras.HandHeldCam::m_TrackingSwayAmount
	float ___m_TrackingSwayAmount_15;
	// System.Single UnityStandardAssets.Cameras.HandHeldCam::m_TrackingBias
	float ___m_TrackingBias_16;

public:
	inline static int32_t get_offset_of_m_SwaySpeed_13() { return static_cast<int32_t>(offsetof(HandHeldCam_t450595784, ___m_SwaySpeed_13)); }
	inline float get_m_SwaySpeed_13() const { return ___m_SwaySpeed_13; }
	inline float* get_address_of_m_SwaySpeed_13() { return &___m_SwaySpeed_13; }
	inline void set_m_SwaySpeed_13(float value)
	{
		___m_SwaySpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_BaseSwayAmount_14() { return static_cast<int32_t>(offsetof(HandHeldCam_t450595784, ___m_BaseSwayAmount_14)); }
	inline float get_m_BaseSwayAmount_14() const { return ___m_BaseSwayAmount_14; }
	inline float* get_address_of_m_BaseSwayAmount_14() { return &___m_BaseSwayAmount_14; }
	inline void set_m_BaseSwayAmount_14(float value)
	{
		___m_BaseSwayAmount_14 = value;
	}

	inline static int32_t get_offset_of_m_TrackingSwayAmount_15() { return static_cast<int32_t>(offsetof(HandHeldCam_t450595784, ___m_TrackingSwayAmount_15)); }
	inline float get_m_TrackingSwayAmount_15() const { return ___m_TrackingSwayAmount_15; }
	inline float* get_address_of_m_TrackingSwayAmount_15() { return &___m_TrackingSwayAmount_15; }
	inline void set_m_TrackingSwayAmount_15(float value)
	{
		___m_TrackingSwayAmount_15 = value;
	}

	inline static int32_t get_offset_of_m_TrackingBias_16() { return static_cast<int32_t>(offsetof(HandHeldCam_t450595784, ___m_TrackingBias_16)); }
	inline float get_m_TrackingBias_16() const { return ___m_TrackingBias_16; }
	inline float* get_address_of_m_TrackingBias_16() { return &___m_TrackingBias_16; }
	inline void set_m_TrackingBias_16(float value)
	{
		___m_TrackingBias_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDHELDCAM_T450595784_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4500 = { sizeof (FireLight_t2068143130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4500[3] = 
{
	FireLight_t2068143130::get_offset_of_m_Rnd_4(),
	FireLight_t2068143130::get_offset_of_m_Burning_5(),
	FireLight_t2068143130::get_offset_of_m_Light_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4501 = { sizeof (Hose_t3016386068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4501[6] = 
{
	Hose_t3016386068::get_offset_of_maxPower_4(),
	Hose_t3016386068::get_offset_of_minPower_5(),
	Hose_t3016386068::get_offset_of_changeSpeed_6(),
	Hose_t3016386068::get_offset_of_hoseWaterSystems_7(),
	Hose_t3016386068::get_offset_of_systemRenderer_8(),
	Hose_t3016386068::get_offset_of_m_Power_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4502 = { sizeof (ParticleSystemMultiplier_t2770350653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4502[1] = 
{
	ParticleSystemMultiplier_t2770350653::get_offset_of_multiplier_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4503 = { sizeof (SmokeParticles_t494565528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4503[1] = 
{
	SmokeParticles_t494565528::get_offset_of_extinguishSounds_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4504 = { sizeof (WaterHoseParticles_t1340502520), -1, sizeof(WaterHoseParticles_t1340502520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4504[4] = 
{
	WaterHoseParticles_t1340502520_StaticFields::get_offset_of_lastSoundTime_4(),
	WaterHoseParticles_t1340502520::get_offset_of_force_5(),
	WaterHoseParticles_t1340502520::get_offset_of_m_CollisionEvents_6(),
	WaterHoseParticles_t1340502520::get_offset_of_m_ParticleSystem_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4505 = { sizeof (WaterBasic_t418026961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4506 = { sizeof (Displace_t1352130581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4507 = { sizeof (GerstnerDisplace_t2537102777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4508 = { sizeof (MeshContainer_t140041298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4508[3] = 
{
	MeshContainer_t140041298::get_offset_of_mesh_0(),
	MeshContainer_t140041298::get_offset_of_vertices_1(),
	MeshContainer_t140041298::get_offset_of_normals_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4509 = { sizeof (PlanarReflection_t439636033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4509[9] = 
{
	PlanarReflection_t439636033::get_offset_of_reflectionMask_4(),
	PlanarReflection_t439636033::get_offset_of_reflectSkybox_5(),
	PlanarReflection_t439636033::get_offset_of_clearColor_6(),
	PlanarReflection_t439636033::get_offset_of_reflectionSampler_7(),
	PlanarReflection_t439636033::get_offset_of_clipPlaneOffset_8(),
	PlanarReflection_t439636033::get_offset_of_m_Oldpos_9(),
	PlanarReflection_t439636033::get_offset_of_m_ReflectionCamera_10(),
	PlanarReflection_t439636033::get_offset_of_m_SharedMaterial_11(),
	PlanarReflection_t439636033::get_offset_of_m_HelperCameras_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4510 = { sizeof (SpecularLighting_t2163114238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4510[2] = 
{
	SpecularLighting_t2163114238::get_offset_of_specularLight_4(),
	SpecularLighting_t2163114238::get_offset_of_m_WaterBase_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4511 = { sizeof (Water_t936588004), -1, sizeof(Water_t936588004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4511[14] = 
{
	Water_t936588004::get_offset_of_waterMode_4(),
	Water_t936588004::get_offset_of_disablePixelLights_5(),
	Water_t936588004::get_offset_of_textureSize_6(),
	Water_t936588004::get_offset_of_clipPlaneOffset_7(),
	Water_t936588004::get_offset_of_reflectLayers_8(),
	Water_t936588004::get_offset_of_refractLayers_9(),
	Water_t936588004::get_offset_of_m_ReflectionCameras_10(),
	Water_t936588004::get_offset_of_m_RefractionCameras_11(),
	Water_t936588004::get_offset_of_m_ReflectionTexture_12(),
	Water_t936588004::get_offset_of_m_RefractionTexture_13(),
	Water_t936588004::get_offset_of_m_HardwareWaterSupport_14(),
	Water_t936588004::get_offset_of_m_OldReflectionTextureSize_15(),
	Water_t936588004::get_offset_of_m_OldRefractionTextureSize_16(),
	Water_t936588004_StaticFields::get_offset_of_s_InsideWater_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4512 = { sizeof (WaterMode_t293960580)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4512[4] = 
{
	WaterMode_t293960580::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4513 = { sizeof (WaterQuality_t1541080576)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4513[4] = 
{
	WaterQuality_t1541080576::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4514 = { sizeof (WaterBase_t3883863317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4514[3] = 
{
	WaterBase_t3883863317::get_offset_of_sharedMaterial_4(),
	WaterBase_t3883863317::get_offset_of_waterQuality_5(),
	WaterBase_t3883863317::get_offset_of_edgeBlend_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4515 = { sizeof (WaterTile_t2536246541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4515[2] = 
{
	WaterTile_t2536246541::get_offset_of_reflection_4(),
	WaterTile_t2536246541::get_offset_of_waterBase_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4516 = { sizeof (Bloom_t1125654350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4516[30] = 
{
	Bloom_t1125654350::get_offset_of_tweakMode_7(),
	Bloom_t1125654350::get_offset_of_screenBlendMode_8(),
	Bloom_t1125654350::get_offset_of_hdr_9(),
	Bloom_t1125654350::get_offset_of_doHdr_10(),
	Bloom_t1125654350::get_offset_of_sepBlurSpread_11(),
	Bloom_t1125654350::get_offset_of_quality_12(),
	Bloom_t1125654350::get_offset_of_bloomIntensity_13(),
	Bloom_t1125654350::get_offset_of_bloomThreshold_14(),
	Bloom_t1125654350::get_offset_of_bloomThresholdColor_15(),
	Bloom_t1125654350::get_offset_of_bloomBlurIterations_16(),
	Bloom_t1125654350::get_offset_of_hollywoodFlareBlurIterations_17(),
	Bloom_t1125654350::get_offset_of_flareRotation_18(),
	Bloom_t1125654350::get_offset_of_lensflareMode_19(),
	Bloom_t1125654350::get_offset_of_hollyStretchWidth_20(),
	Bloom_t1125654350::get_offset_of_lensflareIntensity_21(),
	Bloom_t1125654350::get_offset_of_lensflareThreshold_22(),
	Bloom_t1125654350::get_offset_of_lensFlareSaturation_23(),
	Bloom_t1125654350::get_offset_of_flareColorA_24(),
	Bloom_t1125654350::get_offset_of_flareColorB_25(),
	Bloom_t1125654350::get_offset_of_flareColorC_26(),
	Bloom_t1125654350::get_offset_of_flareColorD_27(),
	Bloom_t1125654350::get_offset_of_lensFlareVignetteMask_28(),
	Bloom_t1125654350::get_offset_of_lensFlareShader_29(),
	Bloom_t1125654350::get_offset_of_lensFlareMaterial_30(),
	Bloom_t1125654350::get_offset_of_screenBlendShader_31(),
	Bloom_t1125654350::get_offset_of_screenBlend_32(),
	Bloom_t1125654350::get_offset_of_blurAndFlaresShader_33(),
	Bloom_t1125654350::get_offset_of_blurAndFlaresMaterial_34(),
	Bloom_t1125654350::get_offset_of_brightPassFilterShader_35(),
	Bloom_t1125654350::get_offset_of_brightPassFilterMaterial_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4517 = { sizeof (LensFlareStyle_t630413071)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4517[4] = 
{
	LensFlareStyle_t630413071::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4518 = { sizeof (TweakMode_t747557136)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4518[3] = 
{
	TweakMode_t747557136::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4519 = { sizeof (HDRBloomMode_t3774419504)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4519[4] = 
{
	HDRBloomMode_t3774419504::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4520 = { sizeof (BloomScreenBlendMode_t2012607685)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4520[3] = 
{
	BloomScreenBlendMode_t2012607685::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4521 = { sizeof (BloomQuality_t3369172721)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4521[3] = 
{
	BloomQuality_t3369172721::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4522 = { sizeof (ImageEffectBase_t2026006575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4522[2] = 
{
	ImageEffectBase_t2026006575::get_offset_of_shader_4(),
	ImageEffectBase_t2026006575::get_offset_of_m_Material_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4523 = { sizeof (ImageEffects_t1214077586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4524 = { sizeof (PostEffectsBase_t2404315739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4524[3] = 
{
	PostEffectsBase_t2404315739::get_offset_of_supportHDRTextures_4(),
	PostEffectsBase_t2404315739::get_offset_of_supportDX11_5(),
	PostEffectsBase_t2404315739::get_offset_of_isSupported_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4525 = { sizeof (PostEffectsHelper_t675066462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4526 = { sizeof (Quads_t1152577304), -1, sizeof(Quads_t1152577304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4526[2] = 
{
	Quads_t1152577304_StaticFields::get_offset_of_meshes_0(),
	Quads_t1152577304_StaticFields::get_offset_of_currentQuads_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4527 = { sizeof (ScreenOverlay_t3772274400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4527[5] = 
{
	ScreenOverlay_t3772274400::get_offset_of_blendMode_7(),
	ScreenOverlay_t3772274400::get_offset_of_intensity_8(),
	ScreenOverlay_t3772274400::get_offset_of_texture_9(),
	ScreenOverlay_t3772274400::get_offset_of_overlayShader_10(),
	ScreenOverlay_t3772274400::get_offset_of_overlayMaterial_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4528 = { sizeof (OverlayBlendMode_t429753458)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4528[6] = 
{
	OverlayBlendMode_t429753458::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4529 = { sizeof (ScreenSpaceAmbientObscurance_t1844081910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4529[8] = 
{
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_intensity_7(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_radius_8(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_blurIterations_9(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_blurFilterDistance_10(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_downsample_11(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_rand_12(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_aoShader_13(),
	ScreenSpaceAmbientObscurance_t1844081910::get_offset_of_aoMaterial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4530 = { sizeof (ScreenSpaceAmbientOcclusion_t1675618705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4530[11] = 
{
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_Radius_4(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_SampleCount_5(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_OcclusionIntensity_6(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_Blur_7(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_Downsampling_8(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_OcclusionAttenuation_9(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_MinZ_10(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_SSAOShader_11(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_SSAOMaterial_12(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_RandomTexture_13(),
	ScreenSpaceAmbientOcclusion_t1675618705::get_offset_of_m_Supported_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4531 = { sizeof (SSAOSamples_t2619211009)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4531[4] = 
{
	SSAOSamples_t2619211009::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4532 = { sizeof (Triangles_t2090031681), -1, sizeof(Triangles_t2090031681_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4532[2] = 
{
	Triangles_t2090031681_StaticFields::get_offset_of_meshes_0(),
	Triangles_t2090031681_StaticFields::get_offset_of_currentTris_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4533 = { sizeof (AxisTouchButton_t3522881333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4533[6] = 
{
	AxisTouchButton_t3522881333::get_offset_of_axisName_4(),
	AxisTouchButton_t3522881333::get_offset_of_axisValue_5(),
	AxisTouchButton_t3522881333::get_offset_of_responseSpeed_6(),
	AxisTouchButton_t3522881333::get_offset_of_returnToCentreSpeed_7(),
	AxisTouchButton_t3522881333::get_offset_of_m_PairedWith_8(),
	AxisTouchButton_t3522881333::get_offset_of_m_Axis_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4534 = { sizeof (ButtonHandler_t823762219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4534[1] = 
{
	ButtonHandler_t823762219::get_offset_of_Name_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4535 = { sizeof (CrossPlatformInputManager_t191731427), -1, sizeof(CrossPlatformInputManager_t191731427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4535[3] = 
{
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4536 = { sizeof (ActiveInputMethod_t139315314)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4536[3] = 
{
	ActiveInputMethod_t139315314::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4537 = { sizeof (VirtualAxis_t4087348596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4537[3] = 
{
	VirtualAxis_t4087348596::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_t4087348596::get_offset_of_m_Value_1(),
	VirtualAxis_t4087348596::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4538 = { sizeof (VirtualButton_t2756566330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4538[5] = 
{
	VirtualButton_t2756566330::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_t2756566330::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_t2756566330::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_t2756566330::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_t2756566330::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4539 = { sizeof (InputAxisScrollbar_t457958266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4539[1] = 
{
	InputAxisScrollbar_t457958266::get_offset_of_axis_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4540 = { sizeof (Joystick_t2204371675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4540[9] = 
{
	Joystick_t2204371675::get_offset_of_MovementRange_4(),
	Joystick_t2204371675::get_offset_of_axesToUse_5(),
	Joystick_t2204371675::get_offset_of_horizontalAxisName_6(),
	Joystick_t2204371675::get_offset_of_verticalAxisName_7(),
	Joystick_t2204371675::get_offset_of_m_StartPos_8(),
	Joystick_t2204371675::get_offset_of_m_UseX_9(),
	Joystick_t2204371675::get_offset_of_m_UseY_10(),
	Joystick_t2204371675::get_offset_of_m_HorizontalVirtualAxis_11(),
	Joystick_t2204371675::get_offset_of_m_VerticalVirtualAxis_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4541 = { sizeof (AxisOption_t3128671669)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4541[4] = 
{
	AxisOption_t3128671669::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4542 = { sizeof (MobileControlRig_t1964600252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4543 = { sizeof (TiltInput_t1639936653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4543[5] = 
{
	TiltInput_t1639936653::get_offset_of_mapping_4(),
	TiltInput_t1639936653::get_offset_of_tiltAroundAxis_5(),
	TiltInput_t1639936653::get_offset_of_fullTiltAngle_6(),
	TiltInput_t1639936653::get_offset_of_centreAngleOffset_7(),
	TiltInput_t1639936653::get_offset_of_m_SteerAxis_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4544 = { sizeof (AxisOptions_t3101732129)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4544[3] = 
{
	AxisOptions_t3101732129::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4545 = { sizeof (AxisMapping_t3982445645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4545[2] = 
{
	AxisMapping_t3982445645::get_offset_of_type_0(),
	AxisMapping_t3982445645::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4546 = { sizeof (MappingType_t2039944511)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4546[5] = 
{
	MappingType_t2039944511::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4547 = { sizeof (TouchPad_t539039257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4547[18] = 
{
	TouchPad_t539039257::get_offset_of_axesToUse_4(),
	TouchPad_t539039257::get_offset_of_controlStyle_5(),
	TouchPad_t539039257::get_offset_of_horizontalAxisName_6(),
	TouchPad_t539039257::get_offset_of_verticalAxisName_7(),
	TouchPad_t539039257::get_offset_of_Xsensitivity_8(),
	TouchPad_t539039257::get_offset_of_Ysensitivity_9(),
	TouchPad_t539039257::get_offset_of_m_StartPos_10(),
	TouchPad_t539039257::get_offset_of_m_PreviousDelta_11(),
	TouchPad_t539039257::get_offset_of_m_JoytickOutput_12(),
	TouchPad_t539039257::get_offset_of_m_UseX_13(),
	TouchPad_t539039257::get_offset_of_m_UseY_14(),
	TouchPad_t539039257::get_offset_of_m_HorizontalVirtualAxis_15(),
	TouchPad_t539039257::get_offset_of_m_VerticalVirtualAxis_16(),
	TouchPad_t539039257::get_offset_of_m_Dragging_17(),
	TouchPad_t539039257::get_offset_of_m_Id_18(),
	TouchPad_t539039257::get_offset_of_m_PreviousTouchPos_19(),
	TouchPad_t539039257::get_offset_of_m_Center_20(),
	TouchPad_t539039257::get_offset_of_m_Image_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4548 = { sizeof (AxisOption_t1372819835)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4548[4] = 
{
	AxisOption_t1372819835::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4549 = { sizeof (ControlStyle_t1372986211)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4549[4] = 
{
	ControlStyle_t1372986211::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4550 = { sizeof (VirtualInput_t2597455733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4550[4] = 
{
	VirtualInput_t2597455733::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_t2597455733::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_t2597455733::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_t2597455733::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4551 = { sizeof (MobileInput_t2025745297), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4552 = { sizeof (StandaloneInput_t1343950252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4553 = { sizeof (Ball_t2378314638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4553[6] = 
{
	Ball_t2378314638::get_offset_of_m_MovePower_4(),
	Ball_t2378314638::get_offset_of_m_UseTorque_5(),
	Ball_t2378314638::get_offset_of_m_MaxAngularVelocity_6(),
	Ball_t2378314638::get_offset_of_m_JumpPower_7(),
	0,
	Ball_t2378314638::get_offset_of_m_Rigidbody_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4554 = { sizeof (BallUserControl_t2574698008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4554[5] = 
{
	BallUserControl_t2574698008::get_offset_of_ball_4(),
	BallUserControl_t2574698008::get_offset_of_move_5(),
	BallUserControl_t2574698008::get_offset_of_cam_6(),
	BallUserControl_t2574698008::get_offset_of_camForward_7(),
	BallUserControl_t2574698008::get_offset_of_jump_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4555 = { sizeof (AICharacterControl_t2972373937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4555[3] = 
{
	AICharacterControl_t2972373937::get_offset_of_U3CagentU3Ek__BackingField_4(),
	AICharacterControl_t2972373937::get_offset_of_U3CcharacterU3Ek__BackingField_5(),
	AICharacterControl_t2972373937::get_offset_of_target_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4556 = { sizeof (ThirdPersonCharacter_t1711070432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4556[20] = 
{
	ThirdPersonCharacter_t1711070432::get_offset_of_m_MovingTurnSpeed_4(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_StationaryTurnSpeed_5(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_JumpPower_6(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_GravityMultiplier_7(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_RunCycleLegOffset_8(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_MoveSpeedMultiplier_9(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_AnimSpeedMultiplier_10(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_GroundCheckDistance_11(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Rigidbody_12(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Animator_13(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_IsGrounded_14(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_OrigGroundCheckDistance_15(),
	0,
	ThirdPersonCharacter_t1711070432::get_offset_of_m_TurnAmount_17(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_ForwardAmount_18(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_GroundNormal_19(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_CapsuleHeight_20(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_CapsuleCenter_21(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Capsule_22(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Crouching_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4557 = { sizeof (ThirdPersonUserControl_t1527285130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4557[5] = 
{
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Character_4(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Cam_5(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_CamForward_6(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Move_7(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Jump_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4558 = { sizeof (FirstPersonController_t2020989554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4558[30] = 
{
	FirstPersonController_t2020989554::get_offset_of_m_IsWalking_4(),
	FirstPersonController_t2020989554::get_offset_of_m_WalkSpeed_5(),
	FirstPersonController_t2020989554::get_offset_of_m_RunSpeed_6(),
	FirstPersonController_t2020989554::get_offset_of_m_RunstepLenghten_7(),
	FirstPersonController_t2020989554::get_offset_of_m_JumpSpeed_8(),
	FirstPersonController_t2020989554::get_offset_of_m_StickToGroundForce_9(),
	FirstPersonController_t2020989554::get_offset_of_m_GravityMultiplier_10(),
	FirstPersonController_t2020989554::get_offset_of_m_MouseLook_11(),
	FirstPersonController_t2020989554::get_offset_of_m_UseFovKick_12(),
	FirstPersonController_t2020989554::get_offset_of_m_FovKick_13(),
	FirstPersonController_t2020989554::get_offset_of_m_UseHeadBob_14(),
	FirstPersonController_t2020989554::get_offset_of_m_HeadBob_15(),
	FirstPersonController_t2020989554::get_offset_of_m_JumpBob_16(),
	FirstPersonController_t2020989554::get_offset_of_m_StepInterval_17(),
	FirstPersonController_t2020989554::get_offset_of_m_FootstepSounds_18(),
	FirstPersonController_t2020989554::get_offset_of_m_JumpSound_19(),
	FirstPersonController_t2020989554::get_offset_of_m_LandSound_20(),
	FirstPersonController_t2020989554::get_offset_of_m_Camera_21(),
	FirstPersonController_t2020989554::get_offset_of_m_Jump_22(),
	FirstPersonController_t2020989554::get_offset_of_m_YRotation_23(),
	FirstPersonController_t2020989554::get_offset_of_m_Input_24(),
	FirstPersonController_t2020989554::get_offset_of_m_MoveDir_25(),
	FirstPersonController_t2020989554::get_offset_of_m_CharacterController_26(),
	FirstPersonController_t2020989554::get_offset_of_m_CollisionFlags_27(),
	FirstPersonController_t2020989554::get_offset_of_m_PreviouslyGrounded_28(),
	FirstPersonController_t2020989554::get_offset_of_m_OriginalCameraPosition_29(),
	FirstPersonController_t2020989554::get_offset_of_m_StepCycle_30(),
	FirstPersonController_t2020989554::get_offset_of_m_NextStep_31(),
	FirstPersonController_t2020989554::get_offset_of_m_Jumping_32(),
	FirstPersonController_t2020989554::get_offset_of_m_AudioSource_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559 = { sizeof (HeadBob_t3275031667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4559[8] = 
{
	HeadBob_t3275031667::get_offset_of_Camera_4(),
	HeadBob_t3275031667::get_offset_of_motionBob_5(),
	HeadBob_t3275031667::get_offset_of_jumpAndLandingBob_6(),
	HeadBob_t3275031667::get_offset_of_rigidbodyFirstPersonController_7(),
	HeadBob_t3275031667::get_offset_of_StrideInterval_8(),
	HeadBob_t3275031667::get_offset_of_RunningStrideLengthen_9(),
	HeadBob_t3275031667::get_offset_of_m_PreviouslyGrounded_10(),
	HeadBob_t3275031667::get_offset_of_m_OriginalCameraPosition_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560 = { sizeof (MouseLook_t2859678661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4560[11] = 
{
	MouseLook_t2859678661::get_offset_of_XSensitivity_0(),
	MouseLook_t2859678661::get_offset_of_YSensitivity_1(),
	MouseLook_t2859678661::get_offset_of_clampVerticalRotation_2(),
	MouseLook_t2859678661::get_offset_of_MinimumX_3(),
	MouseLook_t2859678661::get_offset_of_MaximumX_4(),
	MouseLook_t2859678661::get_offset_of_smooth_5(),
	MouseLook_t2859678661::get_offset_of_smoothTime_6(),
	MouseLook_t2859678661::get_offset_of_lockCursor_7(),
	MouseLook_t2859678661::get_offset_of_m_CharacterTargetRot_8(),
	MouseLook_t2859678661::get_offset_of_m_CameraTargetRot_9(),
	MouseLook_t2859678661::get_offset_of_m_cursorIsLocked_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561 = { sizeof (RigidbodyFirstPersonController_t1207297146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4561[12] = 
{
	RigidbodyFirstPersonController_t1207297146::get_offset_of_cam_4(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_movementSettings_5(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_mouseLook_6(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_advancedSettings_7(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_RigidBody_8(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_Capsule_9(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_YRotation_10(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_GroundContactNormal_11(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_Jump_12(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_PreviouslyGrounded_13(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_Jumping_14(),
	RigidbodyFirstPersonController_t1207297146::get_offset_of_m_IsGrounded_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562 = { sizeof (MovementSettings_t1096092444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4562[8] = 
{
	MovementSettings_t1096092444::get_offset_of_ForwardSpeed_0(),
	MovementSettings_t1096092444::get_offset_of_BackwardSpeed_1(),
	MovementSettings_t1096092444::get_offset_of_StrafeSpeed_2(),
	MovementSettings_t1096092444::get_offset_of_RunMultiplier_3(),
	MovementSettings_t1096092444::get_offset_of_RunKey_4(),
	MovementSettings_t1096092444::get_offset_of_JumpForce_5(),
	MovementSettings_t1096092444::get_offset_of_SlopeCurveModifier_6(),
	MovementSettings_t1096092444::get_offset_of_CurrentTargetSpeed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563 = { sizeof (AdvancedSettings_t778418834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4563[5] = 
{
	AdvancedSettings_t778418834::get_offset_of_groundCheckDistance_0(),
	AdvancedSettings_t778418834::get_offset_of_stickToGroundHelperDistance_1(),
	AdvancedSettings_t778418834::get_offset_of_slowDownRate_2(),
	AdvancedSettings_t778418834::get_offset_of_airControl_3(),
	AdvancedSettings_t778418834::get_offset_of_shellOffset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564 = { sizeof (AbstractTargetFollower_t1919708159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4564[4] = 
{
	AbstractTargetFollower_t1919708159::get_offset_of_m_Target_4(),
	AbstractTargetFollower_t1919708159::get_offset_of_m_AutoTargetPlayer_5(),
	AbstractTargetFollower_t1919708159::get_offset_of_m_UpdateType_6(),
	AbstractTargetFollower_t1919708159::get_offset_of_targetRigidbody_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565 = { sizeof (UpdateType_t2449601881)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4565[4] = 
{
	UpdateType_t2449601881::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566 = { sizeof (AutoCam_t137911967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4566[12] = 
{
	AutoCam_t137911967::get_offset_of_m_MoveSpeed_11(),
	AutoCam_t137911967::get_offset_of_m_TurnSpeed_12(),
	AutoCam_t137911967::get_offset_of_m_RollSpeed_13(),
	AutoCam_t137911967::get_offset_of_m_FollowVelocity_14(),
	AutoCam_t137911967::get_offset_of_m_FollowTilt_15(),
	AutoCam_t137911967::get_offset_of_m_SpinTurnLimit_16(),
	AutoCam_t137911967::get_offset_of_m_TargetVelocityLowerLimit_17(),
	AutoCam_t137911967::get_offset_of_m_SmoothTurnTime_18(),
	AutoCam_t137911967::get_offset_of_m_LastFlatAngle_19(),
	AutoCam_t137911967::get_offset_of_m_CurrentTurnAmount_20(),
	AutoCam_t137911967::get_offset_of_m_TurnSpeedVelocityChange_21(),
	AutoCam_t137911967::get_offset_of_m_RollUp_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567 = { sizeof (FreeLookCam_t2000732766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4567[13] = 
{
	FreeLookCam_t2000732766::get_offset_of_m_MoveSpeed_11(),
	FreeLookCam_t2000732766::get_offset_of_m_TurnSpeed_12(),
	FreeLookCam_t2000732766::get_offset_of_m_TurnSmoothing_13(),
	FreeLookCam_t2000732766::get_offset_of_m_TiltMax_14(),
	FreeLookCam_t2000732766::get_offset_of_m_TiltMin_15(),
	FreeLookCam_t2000732766::get_offset_of_m_LockCursor_16(),
	FreeLookCam_t2000732766::get_offset_of_m_VerticalAutoReturn_17(),
	FreeLookCam_t2000732766::get_offset_of_m_LookAngle_18(),
	FreeLookCam_t2000732766::get_offset_of_m_TiltAngle_19(),
	0,
	FreeLookCam_t2000732766::get_offset_of_m_PivotEulers_21(),
	FreeLookCam_t2000732766::get_offset_of_m_PivotTargetRot_22(),
	FreeLookCam_t2000732766::get_offset_of_m_TransformTargetRot_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568 = { sizeof (HandHeldCam_t450595784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4568[4] = 
{
	HandHeldCam_t450595784::get_offset_of_m_SwaySpeed_13(),
	HandHeldCam_t450595784::get_offset_of_m_BaseSwayAmount_14(),
	HandHeldCam_t450595784::get_offset_of_m_TrackingSwayAmount_15(),
	HandHeldCam_t450595784::get_offset_of_m_TrackingBias_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569 = { sizeof (LookatTarget_t3260877718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4569[5] = 
{
	LookatTarget_t3260877718::get_offset_of_m_RotationRange_8(),
	LookatTarget_t3260877718::get_offset_of_m_FollowSpeed_9(),
	LookatTarget_t3260877718::get_offset_of_m_FollowAngles_10(),
	LookatTarget_t3260877718::get_offset_of_m_OriginalRotation_11(),
	LookatTarget_t3260877718::get_offset_of_m_FollowVelocity_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570 = { sizeof (PivotBasedCameraRig_t3786953582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4570[3] = 
{
	PivotBasedCameraRig_t3786953582::get_offset_of_m_Cam_8(),
	PivotBasedCameraRig_t3786953582::get_offset_of_m_Pivot_9(),
	PivotBasedCameraRig_t3786953582::get_offset_of_m_LastTargetPosition_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571 = { sizeof (ProtectCameraFromWallClip_t303409715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4571[15] = 
{
	ProtectCameraFromWallClip_t303409715::get_offset_of_clipMoveTime_4(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_returnTime_5(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_sphereCastRadius_6(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_visualiseInEditor_7(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_closestDistance_8(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_U3CprotectingU3Ek__BackingField_9(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_dontClipTag_10(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_Cam_11(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_Pivot_12(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_OriginalDist_13(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_MoveVelocity_14(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_CurrentDist_15(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_Ray_16(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_Hits_17(),
	ProtectCameraFromWallClip_t303409715::get_offset_of_m_RayHitComparer_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572 = { sizeof (RayHitComparer_t2205555946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573 = { sizeof (TargetFieldOfView_t3060904718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4573[7] = 
{
	TargetFieldOfView_t3060904718::get_offset_of_m_FovAdjustTime_8(),
	TargetFieldOfView_t3060904718::get_offset_of_m_ZoomAmountMultiplier_9(),
	TargetFieldOfView_t3060904718::get_offset_of_m_IncludeEffectsInSize_10(),
	TargetFieldOfView_t3060904718::get_offset_of_m_BoundSize_11(),
	TargetFieldOfView_t3060904718::get_offset_of_m_FovAdjustVelocity_12(),
	TargetFieldOfView_t3060904718::get_offset_of_m_Cam_13(),
	TargetFieldOfView_t3060904718::get_offset_of_m_LastTarget_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574 = { sizeof (Camera2DFollow_t3335230098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4574[9] = 
{
	Camera2DFollow_t3335230098::get_offset_of_target_4(),
	Camera2DFollow_t3335230098::get_offset_of_damping_5(),
	Camera2DFollow_t3335230098::get_offset_of_lookAheadFactor_6(),
	Camera2DFollow_t3335230098::get_offset_of_lookAheadReturnSpeed_7(),
	Camera2DFollow_t3335230098::get_offset_of_lookAheadMoveThreshold_8(),
	Camera2DFollow_t3335230098::get_offset_of_m_OffsetZ_9(),
	Camera2DFollow_t3335230098::get_offset_of_m_LastTargetPosition_10(),
	Camera2DFollow_t3335230098::get_offset_of_m_CurrentVelocity_11(),
	Camera2DFollow_t3335230098::get_offset_of_m_LookAheadPos_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575 = { sizeof (CameraFollow_t1399352937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4575[7] = 
{
	CameraFollow_t1399352937::get_offset_of_xMargin_4(),
	CameraFollow_t1399352937::get_offset_of_yMargin_5(),
	CameraFollow_t1399352937::get_offset_of_xSmooth_6(),
	CameraFollow_t1399352937::get_offset_of_ySmooth_7(),
	CameraFollow_t1399352937::get_offset_of_maxXAndY_8(),
	CameraFollow_t1399352937::get_offset_of_minXAndY_9(),
	CameraFollow_t1399352937::get_offset_of_m_Player_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576 = { sizeof (Platformer2DUserControl_t4130129562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4576[2] = 
{
	Platformer2DUserControl_t4130129562::get_offset_of_m_Character_4(),
	Platformer2DUserControl_t4130129562::get_offset_of_m_Jump_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577 = { sizeof (PlatformerCharacter2D_t675295753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4577[13] = 
{
	PlatformerCharacter2D_t675295753::get_offset_of_m_MaxSpeed_4(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_JumpForce_5(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_CrouchSpeed_6(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_AirControl_7(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_WhatIsGround_8(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_GroundCheck_9(),
	0,
	PlatformerCharacter2D_t675295753::get_offset_of_m_Grounded_11(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_CeilingCheck_12(),
	0,
	PlatformerCharacter2D_t675295753::get_offset_of_m_Anim_14(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_Rigidbody2D_15(),
	PlatformerCharacter2D_t675295753::get_offset_of_m_FacingRight_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578 = { sizeof (Restarter_t269523250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255373), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4579[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields::get_offset_of_U351A7A390CD6DE245186881400B18C9D822EFE240_0(),
	U3CPrivateImplementationDetailsU3E_t3057255373_StaticFields::get_offset_of_C90F38A020811481753795774EB5AF353F414C59_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580 = { sizeof (__StaticArrayInitTypeSizeU3D12_t2710994320)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t2710994320 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581 = { sizeof (__StaticArrayInitTypeSizeU3D24_t3517759981)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t3517759981 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582 = { sizeof (U3CModuleU3E_t692745566), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583 = { sizeof (Appear_t3088471322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4583[6] = 
{
	Appear_t3088471322::get_offset_of_vImages_4(),
	Appear_t3088471322::get_offset_of_valpha_5(),
	Appear_t3088471322::get_offset_of_vchoice_6(),
	Appear_t3088471322::get_offset_of_vTimer_7(),
	Appear_t3088471322::get_offset_of_cTime_8(),
	Appear_t3088471322::get_offset_of_needtoclick_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584 = { sizeof (U3CWaitInSecondsU3Ed__7_t2879902402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4584[5] = 
{
	U3CWaitInSecondsU3Ed__7_t2879902402::get_offset_of_U3CU3E1__state_0(),
	U3CWaitInSecondsU3Ed__7_t2879902402::get_offset_of_U3CU3E2__current_1(),
	U3CWaitInSecondsU3Ed__7_t2879902402::get_offset_of_vseconds_2(),
	U3CWaitInSecondsU3Ed__7_t2879902402::get_offset_of_vChoice_3(),
	U3CWaitInSecondsU3Ed__7_t2879902402::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585 = { sizeof (DialogBubble_t2812184622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4585[6] = 
{
	DialogBubble_t2812184622::get_offset_of_ray_4(),
	DialogBubble_t2812184622::get_offset_of_hit_5(),
	DialogBubble_t2812184622::get_offset_of_vCurrentBubble_6(),
	DialogBubble_t2812184622::get_offset_of_IsTalking_7(),
	DialogBubble_t2812184622::get_offset_of_vBubble_8(),
	DialogBubble_t2812184622::get_offset_of_vActiveBubble_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586 = { sizeof (BubbleType_t1488104874)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4586[3] = 
{
	BubbleType_t1488104874::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587 = { sizeof (SwitchScene_t643452349), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588 = { sizeof (DropdownManager_t1610485569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4588[18] = 
{
	DropdownManager_t1610485569::get_offset_of_animationData_4(),
	DropdownManager_t1610485569::get_offset_of_dropdown_5(),
	DropdownManager_t1610485569::get_offset_of_animator_6(),
	DropdownManager_t1610485569::get_offset_of_idle_7(),
	DropdownManager_t1610485569::get_offset_of_animName_8(),
	DropdownManager_t1610485569::get_offset_of_animLenght_9(),
	DropdownManager_t1610485569::get_offset_of_animNameText_10(),
	DropdownManager_t1610485569::get_offset_of_animLengthText_11(),
	DropdownManager_t1610485569::get_offset_of_copAttach_12(),
	DropdownManager_t1610485569::get_offset_of_copPrefix_13(),
	DropdownManager_t1610485569::get_offset_of_brawAttach_14(),
	DropdownManager_t1610485569::get_offset_of_brawPrefix_15(),
	DropdownManager_t1610485569::get_offset_of_m_animations_16(),
	DropdownManager_t1610485569::get_offset_of_animatorOverride_17(),
	DropdownManager_t1610485569::get_offset_of_currentAnimation_18(),
	0,
	0,
	DropdownManager_t1610485569::get_offset_of_animLoop_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589 = { sizeof (MovementScript_t3129111103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4589[9] = 
{
	MovementScript_t3129111103::get_offset_of_cam_4(),
	MovementScript_t3129111103::get_offset_of_animator_5(),
	MovementScript_t3129111103::get_offset_of_useCustomRotation_6(),
	MovementScript_t3129111103::get_offset_of_rotationYSpeed_7(),
	MovementScript_t3129111103::get_offset_of_rotationXSpeed_8(),
	0,
	0,
	MovementScript_t3129111103::get_offset_of_defaultYMaxSpeed_11(),
	MovementScript_t3129111103::get_offset_of_defaultXMaxSpeed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590 = { sizeof (ImportedAnimations_t3877126586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4590[1] = 
{
	ImportedAnimations_t3877126586::get_offset_of_animations_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591 = { sizeof (PlayManager_t4074907982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4591[3] = 
{
	PlayManager_t4074907982::get_offset_of_playerGroup_4(),
	PlayManager_t4074907982::get_offset_of_animClipNameGroup_5(),
	PlayManager_t4074907982::get_offset_of_currentNumber_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592 = { sizeof (AICharacterControl_t2137571320), -1, sizeof(AICharacterControl_t2137571320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4592[17] = 
{
	AICharacterControl_t2137571320::get_offset_of_U3CagentU3Ek__BackingField_4(),
	AICharacterControl_t2137571320::get_offset_of_U3CcharacterU3Ek__BackingField_5(),
	AICharacterControl_t2137571320::get_offset_of_target_6(),
	AICharacterControl_t2137571320::get_offset_of_enemyWalkSpeed_7(),
	AICharacterControl_t2137571320::get_offset_of_enemyRunSpeed_8(),
	AICharacterControl_t2137571320::get_offset_of_player_9(),
	AICharacterControl_t2137571320_StaticFields::get_offset_of_isPlayerAlive_10(),
	AICharacterControl_t2137571320::get_offset_of_attackRate_11(),
	AICharacterControl_t2137571320::get_offset_of_soundDelay_12(),
	AICharacterControl_t2137571320::get_offset_of_damageAmount_13(),
	AICharacterControl_t2137571320::get_offset_of_PH_14(),
	AICharacterControl_t2137571320::get_offset_of_nextAttack_15(),
	AICharacterControl_t2137571320::get_offset_of_playerDistance_16(),
	AICharacterControl_t2137571320::get_offset_of_anim_17(),
	AICharacterControl_t2137571320::get_offset_of_playerInRange_18(),
	AICharacterControl_t2137571320::get_offset_of_AS_19(),
	AICharacterControl_t2137571320::get_offset_of_playerHurtClips_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593 = { sizeof (U3CSoundDelayU3Ed__33_t301855376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4593[3] = 
{
	U3CSoundDelayU3Ed__33_t301855376::get_offset_of_U3CU3E1__state_0(),
	U3CSoundDelayU3Ed__33_t301855376::get_offset_of_U3CU3E2__current_1(),
	U3CSoundDelayU3Ed__33_t301855376::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594 = { sizeof (BloodParticleSpawn_t4269314575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4594[3] = 
{
	BloodParticleSpawn_t4269314575::get_offset_of_swordParent_4(),
	BloodParticleSpawn_t4269314575::get_offset_of_bloodFlowParticle_5(),
	BloodParticleSpawn_t4269314575::get_offset_of_isTouching_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595 = { sizeof (MainMenuController_t905567592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4595[9] = 
{
	MainMenuController_t905567592::get_offset_of_anim_4(),
	MainMenuController_t905567592::get_offset_of_newGameSceneName_5(),
	MainMenuController_t905567592::get_offset_of_quickSaveSlotID_6(),
	MainMenuController_t905567592::get_offset_of_MainOptionsPanel_7(),
	MainMenuController_t905567592::get_offset_of_StartGameOptionsPanel_8(),
	MainMenuController_t905567592::get_offset_of_GamePanel_9(),
	MainMenuController_t905567592::get_offset_of_ControlsPanel_10(),
	MainMenuController_t905567592::get_offset_of_GfxPanel_11(),
	MainMenuController_t905567592::get_offset_of_LoadGamePanel_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596 = { sizeof (EnemyHealth_t797421206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4596[5] = 
{
	EnemyHealth_t797421206::get_offset_of_currentHealth_4(),
	EnemyHealth_t797421206::get_offset_of_maxHealth_5(),
	EnemyHealth_t797421206::get_offset_of_ragdoll_6(),
	EnemyHealth_t797421206::get_offset_of_key_7(),
	EnemyHealth_t797421206::get_offset_of_DS_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597 = { sizeof (FootSteps_t730116851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4597[5] = 
{
	FootSteps_t730116851::get_offset_of_clips_4(),
	FootSteps_t730116851::get_offset_of_audioSource_5(),
	FootSteps_t730116851::get_offset_of_swordSounds_6(),
	FootSteps_t730116851::get_offset_of_enemyPunchSounds_7(),
	FootSteps_t730116851::get_offset_of_enemyStepSounds_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598 = { sizeof (Knight_AC_t2269464005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4598[6] = 
{
	Knight_AC_t2269464005::get_offset_of_anim_4(),
	Knight_AC_t2269464005::get_offset_of_nextSwing_5(),
	Knight_AC_t2269464005::get_offset_of_enemyInRange_6(),
	Knight_AC_t2269464005::get_offset_of_EH_7(),
	Knight_AC_t2269464005::get_offset_of_swingRate_8(),
	Knight_AC_t2269464005::get_offset_of_playerDamage_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599 = { sizeof (Knight_Attack_t3463072348), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
