﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DigitalRuby.RainMaker.BaseRainScript
struct BaseRainScript_t980278882;
// DigitalRuby.RainMaker.LoopingAudioSource
struct LoopingAudioSource_t2892973928;
// DigitalRuby.RainMaker.RainScript
struct RainScript_t4186780704;
// DigitalRuby.RainMaker.RainScript2D
struct RainScript2D_t3497865708;
// RPGCharacterAnims.RPGCharacterControllerFREE
struct RPGCharacterControllerFREE_t3229142430;
// RPGCharacterAnims.RPGCharacterInputController
struct RPGCharacterInputController_t2111465464;
// RPGCharacterAnims.RPGCharacterMovementController
struct RPGCharacterMovementController_t3656691424;
// SuperCharacterController
struct SuperCharacterController_t3568978942;
// SuperStateMachine/State
struct State_t3931175374;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Enum,System.Collections.Generic.Dictionary`2<System.String,System.Delegate>>
struct Dictionary_2_t74011037;
// System.Collections.Generic.List`1<Runemark.DarkFantasyKit.Readme/Section>
struct List_1_t4166117572;
// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct List_1_t1232140387;
// System.Enum
struct Enum_t4135868527;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Void
struct Void_t1185182177;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1276799816;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t1033194329;
// UnityEngine.ComputeShader
struct ComputeShader_t317220254;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t3069227754;
// UnityEngine.PostProcessing.AmbientOcclusionModel
struct AmbientOcclusionModel_t389471066;
// UnityEngine.PostProcessing.AntialiasingModel
struct AntialiasingModel_t1521139388;
// UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings[]
struct FxaaConsoleSettingsU5BU5D_t1126991587;
// UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings[]
struct FxaaQualitySettingsU5BU5D_t3129287688;
// UnityEngine.PostProcessing.BloomModel
struct BloomModel_t2099727860;
// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray
struct ArrowArray_t303178545;
// UnityEngine.PostProcessing.BuiltinDebugViewsModel
struct BuiltinDebugViewsModel_t1462618840;
// UnityEngine.PostProcessing.ChromaticAberrationModel
struct ChromaticAberrationModel_t3963399853;
// UnityEngine.PostProcessing.ColorGradingCurve
struct ColorGradingCurve_t2000571184;
// UnityEngine.PostProcessing.ColorGradingModel
struct ColorGradingModel_t1448048181;
// UnityEngine.PostProcessing.DepthOfFieldModel
struct DepthOfFieldModel_t514067330;
// UnityEngine.PostProcessing.DitheringModel
struct DitheringModel_t2429005396;
// UnityEngine.PostProcessing.EyeAdaptationModel
struct EyeAdaptationModel_t242823912;
// UnityEngine.PostProcessing.FogModel
struct FogModel_t3620688749;
// UnityEngine.PostProcessing.GrainModel
struct GrainModel_t1152882488;
// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter
struct FrameBlendingFilter_t2699796096;
// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame[]
struct FrameU5BU5D_t1363420656;
// UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter
struct ReconstructionFilter_t705677647;
// UnityEngine.PostProcessing.MotionBlurModel
struct MotionBlurModel_t3080286123;
// UnityEngine.PostProcessing.PostProcessingContext
struct PostProcessingContext_t2014408948;
// UnityEngine.PostProcessing.ScreenSpaceReflectionModel
struct ScreenSpaceReflectionModel_t3026344732;
// UnityEngine.PostProcessing.UserLutModel
struct UserLutModel_t1670108080;
// UnityEngine.PostProcessing.VignetteModel
struct VignetteModel_t2845517177;
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t1615831949;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t4111643188;
// UnityEngine.Rendering.RenderTargetIdentifier[]
struct RenderTargetIdentifierU5BU5D_t2742279485;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t149664596;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// UnityEngine.WindZone
struct WindZone_t1835817526;

struct RenderTargetIdentifier_t2079184500 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LOOPINGAUDIOSOURCE_T2892973928_H
#define LOOPINGAUDIOSOURCE_T2892973928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.RainMaker.LoopingAudioSource
struct  LoopingAudioSource_t2892973928  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DigitalRuby.RainMaker.LoopingAudioSource::<AudioSource>k__BackingField
	AudioSource_t3935305588 * ___U3CAudioSourceU3Ek__BackingField_0;
	// System.Single DigitalRuby.RainMaker.LoopingAudioSource::<TargetVolume>k__BackingField
	float ___U3CTargetVolumeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CAudioSourceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LoopingAudioSource_t2892973928, ___U3CAudioSourceU3Ek__BackingField_0)); }
	inline AudioSource_t3935305588 * get_U3CAudioSourceU3Ek__BackingField_0() const { return ___U3CAudioSourceU3Ek__BackingField_0; }
	inline AudioSource_t3935305588 ** get_address_of_U3CAudioSourceU3Ek__BackingField_0() { return &___U3CAudioSourceU3Ek__BackingField_0; }
	inline void set_U3CAudioSourceU3Ek__BackingField_0(AudioSource_t3935305588 * value)
	{
		___U3CAudioSourceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudioSourceU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTargetVolumeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LoopingAudioSource_t2892973928, ___U3CTargetVolumeU3Ek__BackingField_1)); }
	inline float get_U3CTargetVolumeU3Ek__BackingField_1() const { return ___U3CTargetVolumeU3Ek__BackingField_1; }
	inline float* get_address_of_U3CTargetVolumeU3Ek__BackingField_1() { return &___U3CTargetVolumeU3Ek__BackingField_1; }
	inline void set_U3CTargetVolumeU3Ek__BackingField_1(float value)
	{
		___U3CTargetVolumeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPINGAUDIOSOURCE_T2892973928_H
#ifndef U3C_DODGEU3ED__10_T2712610854_H
#define U3C_DODGEU3ED__10_T2712610854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGCharacterControllerFREE/<_Dodge>d__10
struct  U3C_DodgeU3Ed__10_t2712610854  : public RuntimeObject
{
public:
	// System.Int32 RPGCharacterAnims.RPGCharacterControllerFREE/<_Dodge>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RPGCharacterAnims.RPGCharacterControllerFREE/<_Dodge>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RPGCharacterAnims.RPGCharacterControllerFREE RPGCharacterAnims.RPGCharacterControllerFREE/<_Dodge>d__10::<>4__this
	RPGCharacterControllerFREE_t3229142430 * ___U3CU3E4__this_2;
	// System.Int32 RPGCharacterAnims.RPGCharacterControllerFREE/<_Dodge>d__10::direction
	int32_t ___direction_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3C_DodgeU3Ed__10_t2712610854, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3C_DodgeU3Ed__10_t2712610854, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3C_DodgeU3Ed__10_t2712610854, ___U3CU3E4__this_2)); }
	inline RPGCharacterControllerFREE_t3229142430 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RPGCharacterControllerFREE_t3229142430 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RPGCharacterControllerFREE_t3229142430 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_direction_3() { return static_cast<int32_t>(offsetof(U3C_DodgeU3Ed__10_t2712610854, ___direction_3)); }
	inline int32_t get_direction_3() const { return ___direction_3; }
	inline int32_t* get_address_of_direction_3() { return &___direction_3; }
	inline void set_direction_3(int32_t value)
	{
		___direction_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_DODGEU3ED__10_T2712610854_H
#ifndef U3C_GETCURRENTANIMATIONLENGTHU3ED__26_T1576132943_H
#define U3C_GETCURRENTANIMATIONLENGTHU3ED__26_T1576132943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGCharacterControllerFREE/<_GetCurrentAnimationLength>d__26
struct  U3C_GetCurrentAnimationLengthU3Ed__26_t1576132943  : public RuntimeObject
{
public:
	// System.Int32 RPGCharacterAnims.RPGCharacterControllerFREE/<_GetCurrentAnimationLength>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RPGCharacterAnims.RPGCharacterControllerFREE/<_GetCurrentAnimationLength>d__26::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RPGCharacterAnims.RPGCharacterControllerFREE RPGCharacterAnims.RPGCharacterControllerFREE/<_GetCurrentAnimationLength>d__26::<>4__this
	RPGCharacterControllerFREE_t3229142430 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3C_GetCurrentAnimationLengthU3Ed__26_t1576132943, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3C_GetCurrentAnimationLengthU3Ed__26_t1576132943, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3C_GetCurrentAnimationLengthU3Ed__26_t1576132943, ___U3CU3E4__this_2)); }
	inline RPGCharacterControllerFREE_t3229142430 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RPGCharacterControllerFREE_t3229142430 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RPGCharacterControllerFREE_t3229142430 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_GETCURRENTANIMATIONLENGTHU3ED__26_T1576132943_H
#ifndef U3C_LOCKU3ED__28_T9022957_H
#define U3C_LOCKU3ED__28_T9022957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGCharacterControllerFREE/<_Lock>d__28
struct  U3C_LockU3Ed__28_t9022957  : public RuntimeObject
{
public:
	// System.Int32 RPGCharacterAnims.RPGCharacterControllerFREE/<_Lock>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RPGCharacterAnims.RPGCharacterControllerFREE/<_Lock>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single RPGCharacterAnims.RPGCharacterControllerFREE/<_Lock>d__28::delayTime
	float ___delayTime_2;
	// System.Boolean RPGCharacterAnims.RPGCharacterControllerFREE/<_Lock>d__28::lockMovement
	bool ___lockMovement_3;
	// RPGCharacterAnims.RPGCharacterControllerFREE RPGCharacterAnims.RPGCharacterControllerFREE/<_Lock>d__28::<>4__this
	RPGCharacterControllerFREE_t3229142430 * ___U3CU3E4__this_4;
	// System.Boolean RPGCharacterAnims.RPGCharacterControllerFREE/<_Lock>d__28::lockAction
	bool ___lockAction_5;
	// System.Boolean RPGCharacterAnims.RPGCharacterControllerFREE/<_Lock>d__28::timed
	bool ___timed_6;
	// System.Single RPGCharacterAnims.RPGCharacterControllerFREE/<_Lock>d__28::lockTime
	float ___lockTime_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3C_LockU3Ed__28_t9022957, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3C_LockU3Ed__28_t9022957, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delayTime_2() { return static_cast<int32_t>(offsetof(U3C_LockU3Ed__28_t9022957, ___delayTime_2)); }
	inline float get_delayTime_2() const { return ___delayTime_2; }
	inline float* get_address_of_delayTime_2() { return &___delayTime_2; }
	inline void set_delayTime_2(float value)
	{
		___delayTime_2 = value;
	}

	inline static int32_t get_offset_of_lockMovement_3() { return static_cast<int32_t>(offsetof(U3C_LockU3Ed__28_t9022957, ___lockMovement_3)); }
	inline bool get_lockMovement_3() const { return ___lockMovement_3; }
	inline bool* get_address_of_lockMovement_3() { return &___lockMovement_3; }
	inline void set_lockMovement_3(bool value)
	{
		___lockMovement_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3C_LockU3Ed__28_t9022957, ___U3CU3E4__this_4)); }
	inline RPGCharacterControllerFREE_t3229142430 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline RPGCharacterControllerFREE_t3229142430 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(RPGCharacterControllerFREE_t3229142430 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_lockAction_5() { return static_cast<int32_t>(offsetof(U3C_LockU3Ed__28_t9022957, ___lockAction_5)); }
	inline bool get_lockAction_5() const { return ___lockAction_5; }
	inline bool* get_address_of_lockAction_5() { return &___lockAction_5; }
	inline void set_lockAction_5(bool value)
	{
		___lockAction_5 = value;
	}

	inline static int32_t get_offset_of_timed_6() { return static_cast<int32_t>(offsetof(U3C_LockU3Ed__28_t9022957, ___timed_6)); }
	inline bool get_timed_6() const { return ___timed_6; }
	inline bool* get_address_of_timed_6() { return &___timed_6; }
	inline void set_timed_6(bool value)
	{
		___timed_6 = value;
	}

	inline static int32_t get_offset_of_lockTime_7() { return static_cast<int32_t>(offsetof(U3C_LockU3Ed__28_t9022957, ___lockTime_7)); }
	inline float get_lockTime_7() const { return ___lockTime_7; }
	inline float* get_address_of_lockTime_7() { return &___lockTime_7; }
	inline void set_lockTime_7(float value)
	{
		___lockTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_LOCKU3ED__28_T9022957_H
#ifndef U3C_TURNINGU3ED__9_T2034464105_H
#define U3C_TURNINGU3ED__9_T2034464105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGCharacterControllerFREE/<_Turning>d__9
struct  U3C_TurningU3Ed__9_t2034464105  : public RuntimeObject
{
public:
	// System.Int32 RPGCharacterAnims.RPGCharacterControllerFREE/<_Turning>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RPGCharacterAnims.RPGCharacterControllerFREE/<_Turning>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 RPGCharacterAnims.RPGCharacterControllerFREE/<_Turning>d__9::direction
	int32_t ___direction_2;
	// RPGCharacterAnims.RPGCharacterControllerFREE RPGCharacterAnims.RPGCharacterControllerFREE/<_Turning>d__9::<>4__this
	RPGCharacterControllerFREE_t3229142430 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3C_TurningU3Ed__9_t2034464105, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3C_TurningU3Ed__9_t2034464105, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_direction_2() { return static_cast<int32_t>(offsetof(U3C_TurningU3Ed__9_t2034464105, ___direction_2)); }
	inline int32_t get_direction_2() const { return ___direction_2; }
	inline int32_t* get_address_of_direction_2() { return &___direction_2; }
	inline void set_direction_2(int32_t value)
	{
		___direction_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3C_TurningU3Ed__9_t2034464105, ___U3CU3E4__this_3)); }
	inline RPGCharacterControllerFREE_t3229142430 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline RPGCharacterControllerFREE_t3229142430 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(RPGCharacterControllerFREE_t3229142430 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_TURNINGU3ED__9_T2034464105_H
#ifndef U3C_ROLLU3ED__60_T3065912839_H
#define U3C_ROLLU3ED__60_T3065912839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGCharacterMovementController/<_Roll>d__60
struct  U3C_RollU3Ed__60_t3065912839  : public RuntimeObject
{
public:
	// System.Int32 RPGCharacterAnims.RPGCharacterMovementController/<_Roll>d__60::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RPGCharacterAnims.RPGCharacterMovementController/<_Roll>d__60::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RPGCharacterAnims.RPGCharacterMovementController RPGCharacterAnims.RPGCharacterMovementController/<_Roll>d__60::<>4__this
	RPGCharacterMovementController_t3656691424 * ___U3CU3E4__this_2;
	// System.Int32 RPGCharacterAnims.RPGCharacterMovementController/<_Roll>d__60::roll
	int32_t ___roll_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3C_RollU3Ed__60_t3065912839, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3C_RollU3Ed__60_t3065912839, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3C_RollU3Ed__60_t3065912839, ___U3CU3E4__this_2)); }
	inline RPGCharacterMovementController_t3656691424 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RPGCharacterMovementController_t3656691424 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RPGCharacterMovementController_t3656691424 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_roll_3() { return static_cast<int32_t>(offsetof(U3C_RollU3Ed__60_t3065912839, ___roll_3)); }
	inline int32_t get_roll_3() const { return ___roll_3; }
	inline int32_t* get_address_of_roll_3() { return &___roll_3; }
	inline void set_roll_3(int32_t value)
	{
		___roll_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_ROLLU3ED__60_T3065912839_H
#ifndef SECTION_T2694042830_H
#define SECTION_T2694042830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Runemark.DarkFantasyKit.Readme/Section
struct  Section_t2694042830  : public RuntimeObject
{
public:
	// System.String Runemark.DarkFantasyKit.Readme/Section::heading
	String_t* ___heading_0;
	// System.String Runemark.DarkFantasyKit.Readme/Section::text
	String_t* ___text_1;
	// System.String Runemark.DarkFantasyKit.Readme/Section::linkText
	String_t* ___linkText_2;
	// System.String Runemark.DarkFantasyKit.Readme/Section::url
	String_t* ___url_3;

public:
	inline static int32_t get_offset_of_heading_0() { return static_cast<int32_t>(offsetof(Section_t2694042830, ___heading_0)); }
	inline String_t* get_heading_0() const { return ___heading_0; }
	inline String_t** get_address_of_heading_0() { return &___heading_0; }
	inline void set_heading_0(String_t* value)
	{
		___heading_0 = value;
		Il2CppCodeGenWriteBarrier((&___heading_0), value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(Section_t2694042830, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_linkText_2() { return static_cast<int32_t>(offsetof(Section_t2694042830, ___linkText_2)); }
	inline String_t* get_linkText_2() const { return ___linkText_2; }
	inline String_t** get_address_of_linkText_2() { return &___linkText_2; }
	inline void set_linkText_2(String_t* value)
	{
		___linkText_2 = value;
		Il2CppCodeGenWriteBarrier((&___linkText_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(Section_t2694042830, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTION_T2694042830_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNIFORMS_T3797733410_H
#define UNIFORMS_T3797733410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms
struct  Uniforms_t3797733410  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3797733410_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Intensity
	int32_t ____Intensity_0;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Radius
	int32_t ____Radius_1;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_FogParams
	int32_t ____FogParams_2;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Downsample
	int32_t ____Downsample_3;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_SampleCount
	int32_t ____SampleCount_4;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture1
	int32_t ____OcclusionTexture1_5;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture2
	int32_t ____OcclusionTexture2_6;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture
	int32_t ____OcclusionTexture_7;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_MainTex
	int32_t ____MainTex_8;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_TempRT
	int32_t ____TempRT_9;

public:
	inline static int32_t get_offset_of__Intensity_0() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____Intensity_0)); }
	inline int32_t get__Intensity_0() const { return ____Intensity_0; }
	inline int32_t* get_address_of__Intensity_0() { return &____Intensity_0; }
	inline void set__Intensity_0(int32_t value)
	{
		____Intensity_0 = value;
	}

	inline static int32_t get_offset_of__Radius_1() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____Radius_1)); }
	inline int32_t get__Radius_1() const { return ____Radius_1; }
	inline int32_t* get_address_of__Radius_1() { return &____Radius_1; }
	inline void set__Radius_1(int32_t value)
	{
		____Radius_1 = value;
	}

	inline static int32_t get_offset_of__FogParams_2() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____FogParams_2)); }
	inline int32_t get__FogParams_2() const { return ____FogParams_2; }
	inline int32_t* get_address_of__FogParams_2() { return &____FogParams_2; }
	inline void set__FogParams_2(int32_t value)
	{
		____FogParams_2 = value;
	}

	inline static int32_t get_offset_of__Downsample_3() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____Downsample_3)); }
	inline int32_t get__Downsample_3() const { return ____Downsample_3; }
	inline int32_t* get_address_of__Downsample_3() { return &____Downsample_3; }
	inline void set__Downsample_3(int32_t value)
	{
		____Downsample_3 = value;
	}

	inline static int32_t get_offset_of__SampleCount_4() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____SampleCount_4)); }
	inline int32_t get__SampleCount_4() const { return ____SampleCount_4; }
	inline int32_t* get_address_of__SampleCount_4() { return &____SampleCount_4; }
	inline void set__SampleCount_4(int32_t value)
	{
		____SampleCount_4 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture1_5() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____OcclusionTexture1_5)); }
	inline int32_t get__OcclusionTexture1_5() const { return ____OcclusionTexture1_5; }
	inline int32_t* get_address_of__OcclusionTexture1_5() { return &____OcclusionTexture1_5; }
	inline void set__OcclusionTexture1_5(int32_t value)
	{
		____OcclusionTexture1_5 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture2_6() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____OcclusionTexture2_6)); }
	inline int32_t get__OcclusionTexture2_6() const { return ____OcclusionTexture2_6; }
	inline int32_t* get_address_of__OcclusionTexture2_6() { return &____OcclusionTexture2_6; }
	inline void set__OcclusionTexture2_6(int32_t value)
	{
		____OcclusionTexture2_6 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture_7() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____OcclusionTexture_7)); }
	inline int32_t get__OcclusionTexture_7() const { return ____OcclusionTexture_7; }
	inline int32_t* get_address_of__OcclusionTexture_7() { return &____OcclusionTexture_7; }
	inline void set__OcclusionTexture_7(int32_t value)
	{
		____OcclusionTexture_7 = value;
	}

	inline static int32_t get_offset_of__MainTex_8() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____MainTex_8)); }
	inline int32_t get__MainTex_8() const { return ____MainTex_8; }
	inline int32_t* get_address_of__MainTex_8() { return &____MainTex_8; }
	inline void set__MainTex_8(int32_t value)
	{
		____MainTex_8 = value;
	}

	inline static int32_t get_offset_of__TempRT_9() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____TempRT_9)); }
	inline int32_t get__TempRT_9() const { return ____TempRT_9; }
	inline int32_t* get_address_of__TempRT_9() { return &____TempRT_9; }
	inline void set__TempRT_9(int32_t value)
	{
		____TempRT_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3797733410_H
#ifndef UNIFORMS_T4164805197_H
#define UNIFORMS_T4164805197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomComponent/Uniforms
struct  Uniforms_t4164805197  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t4164805197_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_AutoExposure
	int32_t ____AutoExposure_0;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Threshold
	int32_t ____Threshold_1;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Curve
	int32_t ____Curve_2;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_PrefilterOffs
	int32_t ____PrefilterOffs_3;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_SampleScale
	int32_t ____SampleScale_4;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_BaseTex
	int32_t ____BaseTex_5;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_BloomTex
	int32_t ____BloomTex_6;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_Settings
	int32_t ____Bloom_Settings_7;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_DirtTex
	int32_t ____Bloom_DirtTex_8;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_DirtIntensity
	int32_t ____Bloom_DirtIntensity_9;

public:
	inline static int32_t get_offset_of__AutoExposure_0() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____AutoExposure_0)); }
	inline int32_t get__AutoExposure_0() const { return ____AutoExposure_0; }
	inline int32_t* get_address_of__AutoExposure_0() { return &____AutoExposure_0; }
	inline void set__AutoExposure_0(int32_t value)
	{
		____AutoExposure_0 = value;
	}

	inline static int32_t get_offset_of__Threshold_1() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Threshold_1)); }
	inline int32_t get__Threshold_1() const { return ____Threshold_1; }
	inline int32_t* get_address_of__Threshold_1() { return &____Threshold_1; }
	inline void set__Threshold_1(int32_t value)
	{
		____Threshold_1 = value;
	}

	inline static int32_t get_offset_of__Curve_2() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Curve_2)); }
	inline int32_t get__Curve_2() const { return ____Curve_2; }
	inline int32_t* get_address_of__Curve_2() { return &____Curve_2; }
	inline void set__Curve_2(int32_t value)
	{
		____Curve_2 = value;
	}

	inline static int32_t get_offset_of__PrefilterOffs_3() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____PrefilterOffs_3)); }
	inline int32_t get__PrefilterOffs_3() const { return ____PrefilterOffs_3; }
	inline int32_t* get_address_of__PrefilterOffs_3() { return &____PrefilterOffs_3; }
	inline void set__PrefilterOffs_3(int32_t value)
	{
		____PrefilterOffs_3 = value;
	}

	inline static int32_t get_offset_of__SampleScale_4() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____SampleScale_4)); }
	inline int32_t get__SampleScale_4() const { return ____SampleScale_4; }
	inline int32_t* get_address_of__SampleScale_4() { return &____SampleScale_4; }
	inline void set__SampleScale_4(int32_t value)
	{
		____SampleScale_4 = value;
	}

	inline static int32_t get_offset_of__BaseTex_5() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____BaseTex_5)); }
	inline int32_t get__BaseTex_5() const { return ____BaseTex_5; }
	inline int32_t* get_address_of__BaseTex_5() { return &____BaseTex_5; }
	inline void set__BaseTex_5(int32_t value)
	{
		____BaseTex_5 = value;
	}

	inline static int32_t get_offset_of__BloomTex_6() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____BloomTex_6)); }
	inline int32_t get__BloomTex_6() const { return ____BloomTex_6; }
	inline int32_t* get_address_of__BloomTex_6() { return &____BloomTex_6; }
	inline void set__BloomTex_6(int32_t value)
	{
		____BloomTex_6 = value;
	}

	inline static int32_t get_offset_of__Bloom_Settings_7() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Bloom_Settings_7)); }
	inline int32_t get__Bloom_Settings_7() const { return ____Bloom_Settings_7; }
	inline int32_t* get_address_of__Bloom_Settings_7() { return &____Bloom_Settings_7; }
	inline void set__Bloom_Settings_7(int32_t value)
	{
		____Bloom_Settings_7 = value;
	}

	inline static int32_t get_offset_of__Bloom_DirtTex_8() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Bloom_DirtTex_8)); }
	inline int32_t get__Bloom_DirtTex_8() const { return ____Bloom_DirtTex_8; }
	inline int32_t* get_address_of__Bloom_DirtTex_8() { return &____Bloom_DirtTex_8; }
	inline void set__Bloom_DirtTex_8(int32_t value)
	{
		____Bloom_DirtTex_8 = value;
	}

	inline static int32_t get_offset_of__Bloom_DirtIntensity_9() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Bloom_DirtIntensity_9)); }
	inline int32_t get__Bloom_DirtIntensity_9() const { return ____Bloom_DirtIntensity_9; }
	inline int32_t* get_address_of__Bloom_DirtIntensity_9() { return &____Bloom_DirtIntensity_9; }
	inline void set__Bloom_DirtIntensity_9(int32_t value)
	{
		____Bloom_DirtIntensity_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T4164805197_H
#ifndef ARROWARRAY_T303178545_H
#define ARROWARRAY_T303178545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray
struct  ArrowArray_t303178545  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::<mesh>k__BackingField
	Mesh_t3648964284 * ___U3CmeshU3Ek__BackingField_0;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::<columnCount>k__BackingField
	int32_t ___U3CcolumnCountU3Ek__BackingField_1;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::<rowCount>k__BackingField
	int32_t ___U3CrowCountU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmeshU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArrowArray_t303178545, ___U3CmeshU3Ek__BackingField_0)); }
	inline Mesh_t3648964284 * get_U3CmeshU3Ek__BackingField_0() const { return ___U3CmeshU3Ek__BackingField_0; }
	inline Mesh_t3648964284 ** get_address_of_U3CmeshU3Ek__BackingField_0() { return &___U3CmeshU3Ek__BackingField_0; }
	inline void set_U3CmeshU3Ek__BackingField_0(Mesh_t3648964284 * value)
	{
		___U3CmeshU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcolumnCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ArrowArray_t303178545, ___U3CcolumnCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CcolumnCountU3Ek__BackingField_1() const { return ___U3CcolumnCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CcolumnCountU3Ek__BackingField_1() { return &___U3CcolumnCountU3Ek__BackingField_1; }
	inline void set_U3CcolumnCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CcolumnCountU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CrowCountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ArrowArray_t303178545, ___U3CrowCountU3Ek__BackingField_2)); }
	inline int32_t get_U3CrowCountU3Ek__BackingField_2() const { return ___U3CrowCountU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CrowCountU3Ek__BackingField_2() { return &___U3CrowCountU3Ek__BackingField_2; }
	inline void set_U3CrowCountU3Ek__BackingField_2(int32_t value)
	{
		___U3CrowCountU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWARRAY_T303178545_H
#ifndef UNIFORMS_T2158582951_H
#define UNIFORMS_T2158582951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms
struct  Uniforms_t2158582951  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t2158582951_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_DepthScale
	int32_t ____DepthScale_0;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_TempRT
	int32_t ____TempRT_1;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_Opacity
	int32_t ____Opacity_2;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_MainTex
	int32_t ____MainTex_3;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_TempRT2
	int32_t ____TempRT2_4;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_Amplitude
	int32_t ____Amplitude_5;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_Scale
	int32_t ____Scale_6;

public:
	inline static int32_t get_offset_of__DepthScale_0() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____DepthScale_0)); }
	inline int32_t get__DepthScale_0() const { return ____DepthScale_0; }
	inline int32_t* get_address_of__DepthScale_0() { return &____DepthScale_0; }
	inline void set__DepthScale_0(int32_t value)
	{
		____DepthScale_0 = value;
	}

	inline static int32_t get_offset_of__TempRT_1() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____TempRT_1)); }
	inline int32_t get__TempRT_1() const { return ____TempRT_1; }
	inline int32_t* get_address_of__TempRT_1() { return &____TempRT_1; }
	inline void set__TempRT_1(int32_t value)
	{
		____TempRT_1 = value;
	}

	inline static int32_t get_offset_of__Opacity_2() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____Opacity_2)); }
	inline int32_t get__Opacity_2() const { return ____Opacity_2; }
	inline int32_t* get_address_of__Opacity_2() { return &____Opacity_2; }
	inline void set__Opacity_2(int32_t value)
	{
		____Opacity_2 = value;
	}

	inline static int32_t get_offset_of__MainTex_3() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____MainTex_3)); }
	inline int32_t get__MainTex_3() const { return ____MainTex_3; }
	inline int32_t* get_address_of__MainTex_3() { return &____MainTex_3; }
	inline void set__MainTex_3(int32_t value)
	{
		____MainTex_3 = value;
	}

	inline static int32_t get_offset_of__TempRT2_4() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____TempRT2_4)); }
	inline int32_t get__TempRT2_4() const { return ____TempRT2_4; }
	inline int32_t* get_address_of__TempRT2_4() { return &____TempRT2_4; }
	inline void set__TempRT2_4(int32_t value)
	{
		____TempRT2_4 = value;
	}

	inline static int32_t get_offset_of__Amplitude_5() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____Amplitude_5)); }
	inline int32_t get__Amplitude_5() const { return ____Amplitude_5; }
	inline int32_t* get_address_of__Amplitude_5() { return &____Amplitude_5; }
	inline void set__Amplitude_5(int32_t value)
	{
		____Amplitude_5 = value;
	}

	inline static int32_t get_offset_of__Scale_6() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____Scale_6)); }
	inline int32_t get__Scale_6() const { return ____Scale_6; }
	inline int32_t* get_address_of__Scale_6() { return &____Scale_6; }
	inline void set__Scale_6(int32_t value)
	{
		____Scale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T2158582951_H
#ifndef UNIFORMS_T424361460_H
#define UNIFORMS_T424361460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ChromaticAberrationComponent/Uniforms
struct  Uniforms_t424361460  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t424361460_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.ChromaticAberrationComponent/Uniforms::_ChromaticAberration_Amount
	int32_t ____ChromaticAberration_Amount_0;
	// System.Int32 UnityEngine.PostProcessing.ChromaticAberrationComponent/Uniforms::_ChromaticAberration_Spectrum
	int32_t ____ChromaticAberration_Spectrum_1;

public:
	inline static int32_t get_offset_of__ChromaticAberration_Amount_0() { return static_cast<int32_t>(offsetof(Uniforms_t424361460_StaticFields, ____ChromaticAberration_Amount_0)); }
	inline int32_t get__ChromaticAberration_Amount_0() const { return ____ChromaticAberration_Amount_0; }
	inline int32_t* get_address_of__ChromaticAberration_Amount_0() { return &____ChromaticAberration_Amount_0; }
	inline void set__ChromaticAberration_Amount_0(int32_t value)
	{
		____ChromaticAberration_Amount_0 = value;
	}

	inline static int32_t get_offset_of__ChromaticAberration_Spectrum_1() { return static_cast<int32_t>(offsetof(Uniforms_t424361460_StaticFields, ____ChromaticAberration_Spectrum_1)); }
	inline int32_t get__ChromaticAberration_Spectrum_1() const { return ____ChromaticAberration_Spectrum_1; }
	inline int32_t* get_address_of__ChromaticAberration_Spectrum_1() { return &____ChromaticAberration_Spectrum_1; }
	inline void set__ChromaticAberration_Spectrum_1(int32_t value)
	{
		____ChromaticAberration_Spectrum_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T424361460_H
#ifndef UNIFORMS_T3075607151_H
#define UNIFORMS_T3075607151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingComponent/Uniforms
struct  Uniforms_t3075607151  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3075607151_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_LutParams
	int32_t ____LutParams_0;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_NeutralTonemapperParams1
	int32_t ____NeutralTonemapperParams1_1;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_NeutralTonemapperParams2
	int32_t ____NeutralTonemapperParams2_2;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_HueShift
	int32_t ____HueShift_3;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Saturation
	int32_t ____Saturation_4;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Contrast
	int32_t ____Contrast_5;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Balance
	int32_t ____Balance_6;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Lift
	int32_t ____Lift_7;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_InvGamma
	int32_t ____InvGamma_8;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Gain
	int32_t ____Gain_9;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Slope
	int32_t ____Slope_10;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Power
	int32_t ____Power_11;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Offset
	int32_t ____Offset_12;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ChannelMixerRed
	int32_t ____ChannelMixerRed_13;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ChannelMixerGreen
	int32_t ____ChannelMixerGreen_14;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ChannelMixerBlue
	int32_t ____ChannelMixerBlue_15;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Curves
	int32_t ____Curves_16;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_LogLut
	int32_t ____LogLut_17;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_LogLut_Params
	int32_t ____LogLut_Params_18;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ExposureEV
	int32_t ____ExposureEV_19;

public:
	inline static int32_t get_offset_of__LutParams_0() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____LutParams_0)); }
	inline int32_t get__LutParams_0() const { return ____LutParams_0; }
	inline int32_t* get_address_of__LutParams_0() { return &____LutParams_0; }
	inline void set__LutParams_0(int32_t value)
	{
		____LutParams_0 = value;
	}

	inline static int32_t get_offset_of__NeutralTonemapperParams1_1() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____NeutralTonemapperParams1_1)); }
	inline int32_t get__NeutralTonemapperParams1_1() const { return ____NeutralTonemapperParams1_1; }
	inline int32_t* get_address_of__NeutralTonemapperParams1_1() { return &____NeutralTonemapperParams1_1; }
	inline void set__NeutralTonemapperParams1_1(int32_t value)
	{
		____NeutralTonemapperParams1_1 = value;
	}

	inline static int32_t get_offset_of__NeutralTonemapperParams2_2() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____NeutralTonemapperParams2_2)); }
	inline int32_t get__NeutralTonemapperParams2_2() const { return ____NeutralTonemapperParams2_2; }
	inline int32_t* get_address_of__NeutralTonemapperParams2_2() { return &____NeutralTonemapperParams2_2; }
	inline void set__NeutralTonemapperParams2_2(int32_t value)
	{
		____NeutralTonemapperParams2_2 = value;
	}

	inline static int32_t get_offset_of__HueShift_3() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____HueShift_3)); }
	inline int32_t get__HueShift_3() const { return ____HueShift_3; }
	inline int32_t* get_address_of__HueShift_3() { return &____HueShift_3; }
	inline void set__HueShift_3(int32_t value)
	{
		____HueShift_3 = value;
	}

	inline static int32_t get_offset_of__Saturation_4() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Saturation_4)); }
	inline int32_t get__Saturation_4() const { return ____Saturation_4; }
	inline int32_t* get_address_of__Saturation_4() { return &____Saturation_4; }
	inline void set__Saturation_4(int32_t value)
	{
		____Saturation_4 = value;
	}

	inline static int32_t get_offset_of__Contrast_5() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Contrast_5)); }
	inline int32_t get__Contrast_5() const { return ____Contrast_5; }
	inline int32_t* get_address_of__Contrast_5() { return &____Contrast_5; }
	inline void set__Contrast_5(int32_t value)
	{
		____Contrast_5 = value;
	}

	inline static int32_t get_offset_of__Balance_6() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Balance_6)); }
	inline int32_t get__Balance_6() const { return ____Balance_6; }
	inline int32_t* get_address_of__Balance_6() { return &____Balance_6; }
	inline void set__Balance_6(int32_t value)
	{
		____Balance_6 = value;
	}

	inline static int32_t get_offset_of__Lift_7() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Lift_7)); }
	inline int32_t get__Lift_7() const { return ____Lift_7; }
	inline int32_t* get_address_of__Lift_7() { return &____Lift_7; }
	inline void set__Lift_7(int32_t value)
	{
		____Lift_7 = value;
	}

	inline static int32_t get_offset_of__InvGamma_8() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____InvGamma_8)); }
	inline int32_t get__InvGamma_8() const { return ____InvGamma_8; }
	inline int32_t* get_address_of__InvGamma_8() { return &____InvGamma_8; }
	inline void set__InvGamma_8(int32_t value)
	{
		____InvGamma_8 = value;
	}

	inline static int32_t get_offset_of__Gain_9() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Gain_9)); }
	inline int32_t get__Gain_9() const { return ____Gain_9; }
	inline int32_t* get_address_of__Gain_9() { return &____Gain_9; }
	inline void set__Gain_9(int32_t value)
	{
		____Gain_9 = value;
	}

	inline static int32_t get_offset_of__Slope_10() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Slope_10)); }
	inline int32_t get__Slope_10() const { return ____Slope_10; }
	inline int32_t* get_address_of__Slope_10() { return &____Slope_10; }
	inline void set__Slope_10(int32_t value)
	{
		____Slope_10 = value;
	}

	inline static int32_t get_offset_of__Power_11() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Power_11)); }
	inline int32_t get__Power_11() const { return ____Power_11; }
	inline int32_t* get_address_of__Power_11() { return &____Power_11; }
	inline void set__Power_11(int32_t value)
	{
		____Power_11 = value;
	}

	inline static int32_t get_offset_of__Offset_12() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Offset_12)); }
	inline int32_t get__Offset_12() const { return ____Offset_12; }
	inline int32_t* get_address_of__Offset_12() { return &____Offset_12; }
	inline void set__Offset_12(int32_t value)
	{
		____Offset_12 = value;
	}

	inline static int32_t get_offset_of__ChannelMixerRed_13() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ChannelMixerRed_13)); }
	inline int32_t get__ChannelMixerRed_13() const { return ____ChannelMixerRed_13; }
	inline int32_t* get_address_of__ChannelMixerRed_13() { return &____ChannelMixerRed_13; }
	inline void set__ChannelMixerRed_13(int32_t value)
	{
		____ChannelMixerRed_13 = value;
	}

	inline static int32_t get_offset_of__ChannelMixerGreen_14() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ChannelMixerGreen_14)); }
	inline int32_t get__ChannelMixerGreen_14() const { return ____ChannelMixerGreen_14; }
	inline int32_t* get_address_of__ChannelMixerGreen_14() { return &____ChannelMixerGreen_14; }
	inline void set__ChannelMixerGreen_14(int32_t value)
	{
		____ChannelMixerGreen_14 = value;
	}

	inline static int32_t get_offset_of__ChannelMixerBlue_15() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ChannelMixerBlue_15)); }
	inline int32_t get__ChannelMixerBlue_15() const { return ____ChannelMixerBlue_15; }
	inline int32_t* get_address_of__ChannelMixerBlue_15() { return &____ChannelMixerBlue_15; }
	inline void set__ChannelMixerBlue_15(int32_t value)
	{
		____ChannelMixerBlue_15 = value;
	}

	inline static int32_t get_offset_of__Curves_16() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Curves_16)); }
	inline int32_t get__Curves_16() const { return ____Curves_16; }
	inline int32_t* get_address_of__Curves_16() { return &____Curves_16; }
	inline void set__Curves_16(int32_t value)
	{
		____Curves_16 = value;
	}

	inline static int32_t get_offset_of__LogLut_17() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____LogLut_17)); }
	inline int32_t get__LogLut_17() const { return ____LogLut_17; }
	inline int32_t* get_address_of__LogLut_17() { return &____LogLut_17; }
	inline void set__LogLut_17(int32_t value)
	{
		____LogLut_17 = value;
	}

	inline static int32_t get_offset_of__LogLut_Params_18() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____LogLut_Params_18)); }
	inline int32_t get__LogLut_Params_18() const { return ____LogLut_Params_18; }
	inline int32_t* get_address_of__LogLut_Params_18() { return &____LogLut_Params_18; }
	inline void set__LogLut_Params_18(int32_t value)
	{
		____LogLut_Params_18 = value;
	}

	inline static int32_t get_offset_of__ExposureEV_19() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ExposureEV_19)); }
	inline int32_t get__ExposureEV_19() const { return ____ExposureEV_19; }
	inline int32_t* get_address_of__ExposureEV_19() { return &____ExposureEV_19; }
	inline void set__ExposureEV_19(int32_t value)
	{
		____ExposureEV_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3075607151_H
#ifndef UNIFORMS_T3629868803_H
#define UNIFORMS_T3629868803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms
struct  Uniforms_t3629868803  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3629868803_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_DepthOfFieldTex
	int32_t ____DepthOfFieldTex_0;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_DepthOfFieldCoCTex
	int32_t ____DepthOfFieldCoCTex_1;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_Distance
	int32_t ____Distance_2;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_LensCoeff
	int32_t ____LensCoeff_3;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_MaxCoC
	int32_t ____MaxCoC_4;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_RcpMaxCoC
	int32_t ____RcpMaxCoC_5;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_RcpAspect
	int32_t ____RcpAspect_6;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_MainTex
	int32_t ____MainTex_7;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_CoCTex
	int32_t ____CoCTex_8;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_TaaParams
	int32_t ____TaaParams_9;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_DepthOfFieldParams
	int32_t ____DepthOfFieldParams_10;

public:
	inline static int32_t get_offset_of__DepthOfFieldTex_0() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____DepthOfFieldTex_0)); }
	inline int32_t get__DepthOfFieldTex_0() const { return ____DepthOfFieldTex_0; }
	inline int32_t* get_address_of__DepthOfFieldTex_0() { return &____DepthOfFieldTex_0; }
	inline void set__DepthOfFieldTex_0(int32_t value)
	{
		____DepthOfFieldTex_0 = value;
	}

	inline static int32_t get_offset_of__DepthOfFieldCoCTex_1() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____DepthOfFieldCoCTex_1)); }
	inline int32_t get__DepthOfFieldCoCTex_1() const { return ____DepthOfFieldCoCTex_1; }
	inline int32_t* get_address_of__DepthOfFieldCoCTex_1() { return &____DepthOfFieldCoCTex_1; }
	inline void set__DepthOfFieldCoCTex_1(int32_t value)
	{
		____DepthOfFieldCoCTex_1 = value;
	}

	inline static int32_t get_offset_of__Distance_2() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____Distance_2)); }
	inline int32_t get__Distance_2() const { return ____Distance_2; }
	inline int32_t* get_address_of__Distance_2() { return &____Distance_2; }
	inline void set__Distance_2(int32_t value)
	{
		____Distance_2 = value;
	}

	inline static int32_t get_offset_of__LensCoeff_3() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____LensCoeff_3)); }
	inline int32_t get__LensCoeff_3() const { return ____LensCoeff_3; }
	inline int32_t* get_address_of__LensCoeff_3() { return &____LensCoeff_3; }
	inline void set__LensCoeff_3(int32_t value)
	{
		____LensCoeff_3 = value;
	}

	inline static int32_t get_offset_of__MaxCoC_4() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____MaxCoC_4)); }
	inline int32_t get__MaxCoC_4() const { return ____MaxCoC_4; }
	inline int32_t* get_address_of__MaxCoC_4() { return &____MaxCoC_4; }
	inline void set__MaxCoC_4(int32_t value)
	{
		____MaxCoC_4 = value;
	}

	inline static int32_t get_offset_of__RcpMaxCoC_5() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____RcpMaxCoC_5)); }
	inline int32_t get__RcpMaxCoC_5() const { return ____RcpMaxCoC_5; }
	inline int32_t* get_address_of__RcpMaxCoC_5() { return &____RcpMaxCoC_5; }
	inline void set__RcpMaxCoC_5(int32_t value)
	{
		____RcpMaxCoC_5 = value;
	}

	inline static int32_t get_offset_of__RcpAspect_6() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____RcpAspect_6)); }
	inline int32_t get__RcpAspect_6() const { return ____RcpAspect_6; }
	inline int32_t* get_address_of__RcpAspect_6() { return &____RcpAspect_6; }
	inline void set__RcpAspect_6(int32_t value)
	{
		____RcpAspect_6 = value;
	}

	inline static int32_t get_offset_of__MainTex_7() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____MainTex_7)); }
	inline int32_t get__MainTex_7() const { return ____MainTex_7; }
	inline int32_t* get_address_of__MainTex_7() { return &____MainTex_7; }
	inline void set__MainTex_7(int32_t value)
	{
		____MainTex_7 = value;
	}

	inline static int32_t get_offset_of__CoCTex_8() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____CoCTex_8)); }
	inline int32_t get__CoCTex_8() const { return ____CoCTex_8; }
	inline int32_t* get_address_of__CoCTex_8() { return &____CoCTex_8; }
	inline void set__CoCTex_8(int32_t value)
	{
		____CoCTex_8 = value;
	}

	inline static int32_t get_offset_of__TaaParams_9() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____TaaParams_9)); }
	inline int32_t get__TaaParams_9() const { return ____TaaParams_9; }
	inline int32_t* get_address_of__TaaParams_9() { return &____TaaParams_9; }
	inline void set__TaaParams_9(int32_t value)
	{
		____TaaParams_9 = value;
	}

	inline static int32_t get_offset_of__DepthOfFieldParams_10() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____DepthOfFieldParams_10)); }
	inline int32_t get__DepthOfFieldParams_10() const { return ____DepthOfFieldParams_10; }
	inline int32_t* get_address_of__DepthOfFieldParams_10() { return &____DepthOfFieldParams_10; }
	inline void set__DepthOfFieldParams_10(int32_t value)
	{
		____DepthOfFieldParams_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3629868803_H
#ifndef UNIFORMS_T3745258951_H
#define UNIFORMS_T3745258951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringComponent/Uniforms
struct  Uniforms_t3745258951  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3745258951_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.DitheringComponent/Uniforms::_DitheringTex
	int32_t ____DitheringTex_0;
	// System.Int32 UnityEngine.PostProcessing.DitheringComponent/Uniforms::_DitheringCoords
	int32_t ____DitheringCoords_1;

public:
	inline static int32_t get_offset_of__DitheringTex_0() { return static_cast<int32_t>(offsetof(Uniforms_t3745258951_StaticFields, ____DitheringTex_0)); }
	inline int32_t get__DitheringTex_0() const { return ____DitheringTex_0; }
	inline int32_t* get_address_of__DitheringTex_0() { return &____DitheringTex_0; }
	inline void set__DitheringTex_0(int32_t value)
	{
		____DitheringTex_0 = value;
	}

	inline static int32_t get_offset_of__DitheringCoords_1() { return static_cast<int32_t>(offsetof(Uniforms_t3745258951_StaticFields, ____DitheringCoords_1)); }
	inline int32_t get__DitheringCoords_1() const { return ____DitheringCoords_1; }
	inline int32_t* get_address_of__DitheringCoords_1() { return &____DitheringCoords_1; }
	inline void set__DitheringCoords_1(int32_t value)
	{
		____DitheringCoords_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3745258951_H
#ifndef UNIFORMS_T95810089_H
#define UNIFORMS_T95810089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms
struct  Uniforms_t95810089  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t95810089_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_Params
	int32_t ____Params_0;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_Speed
	int32_t ____Speed_1;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_ScaleOffsetRes
	int32_t ____ScaleOffsetRes_2;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_ExposureCompensation
	int32_t ____ExposureCompensation_3;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_AutoExposure
	int32_t ____AutoExposure_4;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_DebugWidth
	int32_t ____DebugWidth_5;

public:
	inline static int32_t get_offset_of__Params_0() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____Params_0)); }
	inline int32_t get__Params_0() const { return ____Params_0; }
	inline int32_t* get_address_of__Params_0() { return &____Params_0; }
	inline void set__Params_0(int32_t value)
	{
		____Params_0 = value;
	}

	inline static int32_t get_offset_of__Speed_1() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____Speed_1)); }
	inline int32_t get__Speed_1() const { return ____Speed_1; }
	inline int32_t* get_address_of__Speed_1() { return &____Speed_1; }
	inline void set__Speed_1(int32_t value)
	{
		____Speed_1 = value;
	}

	inline static int32_t get_offset_of__ScaleOffsetRes_2() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____ScaleOffsetRes_2)); }
	inline int32_t get__ScaleOffsetRes_2() const { return ____ScaleOffsetRes_2; }
	inline int32_t* get_address_of__ScaleOffsetRes_2() { return &____ScaleOffsetRes_2; }
	inline void set__ScaleOffsetRes_2(int32_t value)
	{
		____ScaleOffsetRes_2 = value;
	}

	inline static int32_t get_offset_of__ExposureCompensation_3() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____ExposureCompensation_3)); }
	inline int32_t get__ExposureCompensation_3() const { return ____ExposureCompensation_3; }
	inline int32_t* get_address_of__ExposureCompensation_3() { return &____ExposureCompensation_3; }
	inline void set__ExposureCompensation_3(int32_t value)
	{
		____ExposureCompensation_3 = value;
	}

	inline static int32_t get_offset_of__AutoExposure_4() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____AutoExposure_4)); }
	inline int32_t get__AutoExposure_4() const { return ____AutoExposure_4; }
	inline int32_t* get_address_of__AutoExposure_4() { return &____AutoExposure_4; }
	inline void set__AutoExposure_4(int32_t value)
	{
		____AutoExposure_4 = value;
	}

	inline static int32_t get_offset_of__DebugWidth_5() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____DebugWidth_5)); }
	inline int32_t get__DebugWidth_5() const { return ____DebugWidth_5; }
	inline int32_t* get_address_of__DebugWidth_5() { return &____DebugWidth_5; }
	inline void set__DebugWidth_5(int32_t value)
	{
		____DebugWidth_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T95810089_H
#ifndef UNIFORMS_T459708260_H
#define UNIFORMS_T459708260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogComponent/Uniforms
struct  Uniforms_t459708260  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t459708260_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_FogColor
	int32_t ____FogColor_0;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_Density
	int32_t ____Density_1;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_Start
	int32_t ____Start_2;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_End
	int32_t ____End_3;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_TempRT
	int32_t ____TempRT_4;

public:
	inline static int32_t get_offset_of__FogColor_0() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____FogColor_0)); }
	inline int32_t get__FogColor_0() const { return ____FogColor_0; }
	inline int32_t* get_address_of__FogColor_0() { return &____FogColor_0; }
	inline void set__FogColor_0(int32_t value)
	{
		____FogColor_0 = value;
	}

	inline static int32_t get_offset_of__Density_1() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____Density_1)); }
	inline int32_t get__Density_1() const { return ____Density_1; }
	inline int32_t* get_address_of__Density_1() { return &____Density_1; }
	inline void set__Density_1(int32_t value)
	{
		____Density_1 = value;
	}

	inline static int32_t get_offset_of__Start_2() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____Start_2)); }
	inline int32_t get__Start_2() const { return ____Start_2; }
	inline int32_t* get_address_of__Start_2() { return &____Start_2; }
	inline void set__Start_2(int32_t value)
	{
		____Start_2 = value;
	}

	inline static int32_t get_offset_of__End_3() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____End_3)); }
	inline int32_t get__End_3() const { return ____End_3; }
	inline int32_t* get_address_of__End_3() { return &____End_3; }
	inline void set__End_3(int32_t value)
	{
		____End_3 = value;
	}

	inline static int32_t get_offset_of__TempRT_4() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____TempRT_4)); }
	inline int32_t get__TempRT_4() const { return ____TempRT_4; }
	inline int32_t* get_address_of__TempRT_4() { return &____TempRT_4; }
	inline void set__TempRT_4(int32_t value)
	{
		____TempRT_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T459708260_H
#ifndef UNIFORMS_T1850622510_H
#define UNIFORMS_T1850622510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FxaaComponent/Uniforms
struct  Uniforms_t1850622510  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t1850622510_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.FxaaComponent/Uniforms::_QualitySettings
	int32_t ____QualitySettings_0;
	// System.Int32 UnityEngine.PostProcessing.FxaaComponent/Uniforms::_ConsoleSettings
	int32_t ____ConsoleSettings_1;

public:
	inline static int32_t get_offset_of__QualitySettings_0() { return static_cast<int32_t>(offsetof(Uniforms_t1850622510_StaticFields, ____QualitySettings_0)); }
	inline int32_t get__QualitySettings_0() const { return ____QualitySettings_0; }
	inline int32_t* get_address_of__QualitySettings_0() { return &____QualitySettings_0; }
	inline void set__QualitySettings_0(int32_t value)
	{
		____QualitySettings_0 = value;
	}

	inline static int32_t get_offset_of__ConsoleSettings_1() { return static_cast<int32_t>(offsetof(Uniforms_t1850622510_StaticFields, ____ConsoleSettings_1)); }
	inline int32_t get__ConsoleSettings_1() const { return ____ConsoleSettings_1; }
	inline int32_t* get_address_of__ConsoleSettings_1() { return &____ConsoleSettings_1; }
	inline void set__ConsoleSettings_1(int32_t value)
	{
		____ConsoleSettings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T1850622510_H
#ifndef UNIFORMS_T1442519687_H
#define UNIFORMS_T1442519687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainComponent/Uniforms
struct  Uniforms_t1442519687  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t1442519687_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_Grain_Params1
	int32_t ____Grain_Params1_0;
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_Grain_Params2
	int32_t ____Grain_Params2_1;
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_GrainTex
	int32_t ____GrainTex_2;
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_Phase
	int32_t ____Phase_3;

public:
	inline static int32_t get_offset_of__Grain_Params1_0() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____Grain_Params1_0)); }
	inline int32_t get__Grain_Params1_0() const { return ____Grain_Params1_0; }
	inline int32_t* get_address_of__Grain_Params1_0() { return &____Grain_Params1_0; }
	inline void set__Grain_Params1_0(int32_t value)
	{
		____Grain_Params1_0 = value;
	}

	inline static int32_t get_offset_of__Grain_Params2_1() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____Grain_Params2_1)); }
	inline int32_t get__Grain_Params2_1() const { return ____Grain_Params2_1; }
	inline int32_t* get_address_of__Grain_Params2_1() { return &____Grain_Params2_1; }
	inline void set__Grain_Params2_1(int32_t value)
	{
		____Grain_Params2_1 = value;
	}

	inline static int32_t get_offset_of__GrainTex_2() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____GrainTex_2)); }
	inline int32_t get__GrainTex_2() const { return ____GrainTex_2; }
	inline int32_t* get_address_of__GrainTex_2() { return &____GrainTex_2; }
	inline void set__GrainTex_2(int32_t value)
	{
		____GrainTex_2 = value;
	}

	inline static int32_t get_offset_of__Phase_3() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____Phase_3)); }
	inline int32_t get__Phase_3() const { return ____Phase_3; }
	inline int32_t* get_address_of__Phase_3() { return &____Phase_3; }
	inline void set__Phase_3(int32_t value)
	{
		____Phase_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T1442519687_H
#ifndef UNIFORMS_T589754008_H
#define UNIFORMS_T589754008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent/Uniforms
struct  Uniforms_t589754008  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t589754008_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_VelocityScale
	int32_t ____VelocityScale_0;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_MaxBlurRadius
	int32_t ____MaxBlurRadius_1;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_RcpMaxBlurRadius
	int32_t ____RcpMaxBlurRadius_2;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_VelocityTex
	int32_t ____VelocityTex_3;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_MainTex
	int32_t ____MainTex_4;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_Tile2RT
	int32_t ____Tile2RT_5;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_Tile4RT
	int32_t ____Tile4RT_6;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_Tile8RT
	int32_t ____Tile8RT_7;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_TileMaxOffs
	int32_t ____TileMaxOffs_8;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_TileMaxLoop
	int32_t ____TileMaxLoop_9;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_TileVRT
	int32_t ____TileVRT_10;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_NeighborMaxTex
	int32_t ____NeighborMaxTex_11;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_LoopCount
	int32_t ____LoopCount_12;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_TempRT
	int32_t ____TempRT_13;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History1LumaTex
	int32_t ____History1LumaTex_14;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History2LumaTex
	int32_t ____History2LumaTex_15;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History3LumaTex
	int32_t ____History3LumaTex_16;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History4LumaTex
	int32_t ____History4LumaTex_17;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History1ChromaTex
	int32_t ____History1ChromaTex_18;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History2ChromaTex
	int32_t ____History2ChromaTex_19;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History3ChromaTex
	int32_t ____History3ChromaTex_20;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History4ChromaTex
	int32_t ____History4ChromaTex_21;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History1Weight
	int32_t ____History1Weight_22;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History2Weight
	int32_t ____History2Weight_23;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History3Weight
	int32_t ____History3Weight_24;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::_History4Weight
	int32_t ____History4Weight_25;

public:
	inline static int32_t get_offset_of__VelocityScale_0() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____VelocityScale_0)); }
	inline int32_t get__VelocityScale_0() const { return ____VelocityScale_0; }
	inline int32_t* get_address_of__VelocityScale_0() { return &____VelocityScale_0; }
	inline void set__VelocityScale_0(int32_t value)
	{
		____VelocityScale_0 = value;
	}

	inline static int32_t get_offset_of__MaxBlurRadius_1() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____MaxBlurRadius_1)); }
	inline int32_t get__MaxBlurRadius_1() const { return ____MaxBlurRadius_1; }
	inline int32_t* get_address_of__MaxBlurRadius_1() { return &____MaxBlurRadius_1; }
	inline void set__MaxBlurRadius_1(int32_t value)
	{
		____MaxBlurRadius_1 = value;
	}

	inline static int32_t get_offset_of__RcpMaxBlurRadius_2() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____RcpMaxBlurRadius_2)); }
	inline int32_t get__RcpMaxBlurRadius_2() const { return ____RcpMaxBlurRadius_2; }
	inline int32_t* get_address_of__RcpMaxBlurRadius_2() { return &____RcpMaxBlurRadius_2; }
	inline void set__RcpMaxBlurRadius_2(int32_t value)
	{
		____RcpMaxBlurRadius_2 = value;
	}

	inline static int32_t get_offset_of__VelocityTex_3() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____VelocityTex_3)); }
	inline int32_t get__VelocityTex_3() const { return ____VelocityTex_3; }
	inline int32_t* get_address_of__VelocityTex_3() { return &____VelocityTex_3; }
	inline void set__VelocityTex_3(int32_t value)
	{
		____VelocityTex_3 = value;
	}

	inline static int32_t get_offset_of__MainTex_4() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____MainTex_4)); }
	inline int32_t get__MainTex_4() const { return ____MainTex_4; }
	inline int32_t* get_address_of__MainTex_4() { return &____MainTex_4; }
	inline void set__MainTex_4(int32_t value)
	{
		____MainTex_4 = value;
	}

	inline static int32_t get_offset_of__Tile2RT_5() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____Tile2RT_5)); }
	inline int32_t get__Tile2RT_5() const { return ____Tile2RT_5; }
	inline int32_t* get_address_of__Tile2RT_5() { return &____Tile2RT_5; }
	inline void set__Tile2RT_5(int32_t value)
	{
		____Tile2RT_5 = value;
	}

	inline static int32_t get_offset_of__Tile4RT_6() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____Tile4RT_6)); }
	inline int32_t get__Tile4RT_6() const { return ____Tile4RT_6; }
	inline int32_t* get_address_of__Tile4RT_6() { return &____Tile4RT_6; }
	inline void set__Tile4RT_6(int32_t value)
	{
		____Tile4RT_6 = value;
	}

	inline static int32_t get_offset_of__Tile8RT_7() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____Tile8RT_7)); }
	inline int32_t get__Tile8RT_7() const { return ____Tile8RT_7; }
	inline int32_t* get_address_of__Tile8RT_7() { return &____Tile8RT_7; }
	inline void set__Tile8RT_7(int32_t value)
	{
		____Tile8RT_7 = value;
	}

	inline static int32_t get_offset_of__TileMaxOffs_8() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____TileMaxOffs_8)); }
	inline int32_t get__TileMaxOffs_8() const { return ____TileMaxOffs_8; }
	inline int32_t* get_address_of__TileMaxOffs_8() { return &____TileMaxOffs_8; }
	inline void set__TileMaxOffs_8(int32_t value)
	{
		____TileMaxOffs_8 = value;
	}

	inline static int32_t get_offset_of__TileMaxLoop_9() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____TileMaxLoop_9)); }
	inline int32_t get__TileMaxLoop_9() const { return ____TileMaxLoop_9; }
	inline int32_t* get_address_of__TileMaxLoop_9() { return &____TileMaxLoop_9; }
	inline void set__TileMaxLoop_9(int32_t value)
	{
		____TileMaxLoop_9 = value;
	}

	inline static int32_t get_offset_of__TileVRT_10() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____TileVRT_10)); }
	inline int32_t get__TileVRT_10() const { return ____TileVRT_10; }
	inline int32_t* get_address_of__TileVRT_10() { return &____TileVRT_10; }
	inline void set__TileVRT_10(int32_t value)
	{
		____TileVRT_10 = value;
	}

	inline static int32_t get_offset_of__NeighborMaxTex_11() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____NeighborMaxTex_11)); }
	inline int32_t get__NeighborMaxTex_11() const { return ____NeighborMaxTex_11; }
	inline int32_t* get_address_of__NeighborMaxTex_11() { return &____NeighborMaxTex_11; }
	inline void set__NeighborMaxTex_11(int32_t value)
	{
		____NeighborMaxTex_11 = value;
	}

	inline static int32_t get_offset_of__LoopCount_12() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____LoopCount_12)); }
	inline int32_t get__LoopCount_12() const { return ____LoopCount_12; }
	inline int32_t* get_address_of__LoopCount_12() { return &____LoopCount_12; }
	inline void set__LoopCount_12(int32_t value)
	{
		____LoopCount_12 = value;
	}

	inline static int32_t get_offset_of__TempRT_13() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____TempRT_13)); }
	inline int32_t get__TempRT_13() const { return ____TempRT_13; }
	inline int32_t* get_address_of__TempRT_13() { return &____TempRT_13; }
	inline void set__TempRT_13(int32_t value)
	{
		____TempRT_13 = value;
	}

	inline static int32_t get_offset_of__History1LumaTex_14() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History1LumaTex_14)); }
	inline int32_t get__History1LumaTex_14() const { return ____History1LumaTex_14; }
	inline int32_t* get_address_of__History1LumaTex_14() { return &____History1LumaTex_14; }
	inline void set__History1LumaTex_14(int32_t value)
	{
		____History1LumaTex_14 = value;
	}

	inline static int32_t get_offset_of__History2LumaTex_15() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History2LumaTex_15)); }
	inline int32_t get__History2LumaTex_15() const { return ____History2LumaTex_15; }
	inline int32_t* get_address_of__History2LumaTex_15() { return &____History2LumaTex_15; }
	inline void set__History2LumaTex_15(int32_t value)
	{
		____History2LumaTex_15 = value;
	}

	inline static int32_t get_offset_of__History3LumaTex_16() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History3LumaTex_16)); }
	inline int32_t get__History3LumaTex_16() const { return ____History3LumaTex_16; }
	inline int32_t* get_address_of__History3LumaTex_16() { return &____History3LumaTex_16; }
	inline void set__History3LumaTex_16(int32_t value)
	{
		____History3LumaTex_16 = value;
	}

	inline static int32_t get_offset_of__History4LumaTex_17() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History4LumaTex_17)); }
	inline int32_t get__History4LumaTex_17() const { return ____History4LumaTex_17; }
	inline int32_t* get_address_of__History4LumaTex_17() { return &____History4LumaTex_17; }
	inline void set__History4LumaTex_17(int32_t value)
	{
		____History4LumaTex_17 = value;
	}

	inline static int32_t get_offset_of__History1ChromaTex_18() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History1ChromaTex_18)); }
	inline int32_t get__History1ChromaTex_18() const { return ____History1ChromaTex_18; }
	inline int32_t* get_address_of__History1ChromaTex_18() { return &____History1ChromaTex_18; }
	inline void set__History1ChromaTex_18(int32_t value)
	{
		____History1ChromaTex_18 = value;
	}

	inline static int32_t get_offset_of__History2ChromaTex_19() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History2ChromaTex_19)); }
	inline int32_t get__History2ChromaTex_19() const { return ____History2ChromaTex_19; }
	inline int32_t* get_address_of__History2ChromaTex_19() { return &____History2ChromaTex_19; }
	inline void set__History2ChromaTex_19(int32_t value)
	{
		____History2ChromaTex_19 = value;
	}

	inline static int32_t get_offset_of__History3ChromaTex_20() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History3ChromaTex_20)); }
	inline int32_t get__History3ChromaTex_20() const { return ____History3ChromaTex_20; }
	inline int32_t* get_address_of__History3ChromaTex_20() { return &____History3ChromaTex_20; }
	inline void set__History3ChromaTex_20(int32_t value)
	{
		____History3ChromaTex_20 = value;
	}

	inline static int32_t get_offset_of__History4ChromaTex_21() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History4ChromaTex_21)); }
	inline int32_t get__History4ChromaTex_21() const { return ____History4ChromaTex_21; }
	inline int32_t* get_address_of__History4ChromaTex_21() { return &____History4ChromaTex_21; }
	inline void set__History4ChromaTex_21(int32_t value)
	{
		____History4ChromaTex_21 = value;
	}

	inline static int32_t get_offset_of__History1Weight_22() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History1Weight_22)); }
	inline int32_t get__History1Weight_22() const { return ____History1Weight_22; }
	inline int32_t* get_address_of__History1Weight_22() { return &____History1Weight_22; }
	inline void set__History1Weight_22(int32_t value)
	{
		____History1Weight_22 = value;
	}

	inline static int32_t get_offset_of__History2Weight_23() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History2Weight_23)); }
	inline int32_t get__History2Weight_23() const { return ____History2Weight_23; }
	inline int32_t* get_address_of__History2Weight_23() { return &____History2Weight_23; }
	inline void set__History2Weight_23(int32_t value)
	{
		____History2Weight_23 = value;
	}

	inline static int32_t get_offset_of__History3Weight_24() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History3Weight_24)); }
	inline int32_t get__History3Weight_24() const { return ____History3Weight_24; }
	inline int32_t* get_address_of__History3Weight_24() { return &____History3Weight_24; }
	inline void set__History3Weight_24(int32_t value)
	{
		____History3Weight_24 = value;
	}

	inline static int32_t get_offset_of__History4Weight_25() { return static_cast<int32_t>(offsetof(Uniforms_t589754008_StaticFields, ____History4Weight_25)); }
	inline int32_t get__History4Weight_25() const { return ____History4Weight_25; }
	inline int32_t* get_address_of__History4Weight_25() { return &____History4Weight_25; }
	inline void set__History4Weight_25(int32_t value)
	{
		____History4Weight_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T589754008_H
#ifndef POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#define POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentBase
struct  PostProcessingComponentBase_t2731103827  : public RuntimeObject
{
public:
	// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingComponentBase::context
	PostProcessingContext_t2014408948 * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(PostProcessingComponentBase_t2731103827, ___context_0)); }
	inline PostProcessingContext_t2014408948 * get_context_0() const { return ___context_0; }
	inline PostProcessingContext_t2014408948 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(PostProcessingContext_t2014408948 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifndef POSTPROCESSINGMODEL_T540111976_H
#define POSTPROCESSINGMODEL_T540111976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingModel
struct  PostProcessingModel_t540111976  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.PostProcessing.PostProcessingModel::m_Enabled
	bool ___m_Enabled_0;

public:
	inline static int32_t get_offset_of_m_Enabled_0() { return static_cast<int32_t>(offsetof(PostProcessingModel_t540111976, ___m_Enabled_0)); }
	inline bool get_m_Enabled_0() const { return ___m_Enabled_0; }
	inline bool* get_address_of_m_Enabled_0() { return &___m_Enabled_0; }
	inline void set_m_Enabled_0(bool value)
	{
		___m_Enabled_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGMODEL_T540111976_H
#ifndef UNIFORMS_T2970573890_H
#define UNIFORMS_T2970573890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms
struct  Uniforms_t2970573890  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t2970573890_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_RayStepSize
	int32_t ____RayStepSize_0;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_AdditiveReflection
	int32_t ____AdditiveReflection_1;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_BilateralUpsampling
	int32_t ____BilateralUpsampling_2;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_TreatBackfaceHitAsMiss
	int32_t ____TreatBackfaceHitAsMiss_3;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_AllowBackwardsRays
	int32_t ____AllowBackwardsRays_4;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_TraceBehindObjects
	int32_t ____TraceBehindObjects_5;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_MaxSteps
	int32_t ____MaxSteps_6;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FullResolutionFiltering
	int32_t ____FullResolutionFiltering_7;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_HalfResolution
	int32_t ____HalfResolution_8;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_HighlightSuppression
	int32_t ____HighlightSuppression_9;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_PixelsPerMeterAtOneMeter
	int32_t ____PixelsPerMeterAtOneMeter_10;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ScreenEdgeFading
	int32_t ____ScreenEdgeFading_11;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ReflectionBlur
	int32_t ____ReflectionBlur_12;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_MaxRayTraceDistance
	int32_t ____MaxRayTraceDistance_13;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FadeDistance
	int32_t ____FadeDistance_14;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_LayerThickness
	int32_t ____LayerThickness_15;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_SSRMultiplier
	int32_t ____SSRMultiplier_16;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FresnelFade
	int32_t ____FresnelFade_17;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FresnelFadePower
	int32_t ____FresnelFadePower_18;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ReflectionBufferSize
	int32_t ____ReflectionBufferSize_19;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ScreenSize
	int32_t ____ScreenSize_20;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_InvScreenSize
	int32_t ____InvScreenSize_21;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ProjInfo
	int32_t ____ProjInfo_22;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_CameraClipInfo
	int32_t ____CameraClipInfo_23;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_ProjectToPixelMatrix
	int32_t ____ProjectToPixelMatrix_24;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_WorldToCameraMatrix
	int32_t ____WorldToCameraMatrix_25;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_CameraToWorldMatrix
	int32_t ____CameraToWorldMatrix_26;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_Axis
	int32_t ____Axis_27;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_CurrentMipLevel
	int32_t ____CurrentMipLevel_28;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_NormalAndRoughnessTexture
	int32_t ____NormalAndRoughnessTexture_29;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_HitPointTexture
	int32_t ____HitPointTexture_30;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_BlurTexture
	int32_t ____BlurTexture_31;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FilteredReflections
	int32_t ____FilteredReflections_32;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_FinalReflectionTexture
	int32_t ____FinalReflectionTexture_33;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::_TempTexture
	int32_t ____TempTexture_34;

public:
	inline static int32_t get_offset_of__RayStepSize_0() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____RayStepSize_0)); }
	inline int32_t get__RayStepSize_0() const { return ____RayStepSize_0; }
	inline int32_t* get_address_of__RayStepSize_0() { return &____RayStepSize_0; }
	inline void set__RayStepSize_0(int32_t value)
	{
		____RayStepSize_0 = value;
	}

	inline static int32_t get_offset_of__AdditiveReflection_1() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____AdditiveReflection_1)); }
	inline int32_t get__AdditiveReflection_1() const { return ____AdditiveReflection_1; }
	inline int32_t* get_address_of__AdditiveReflection_1() { return &____AdditiveReflection_1; }
	inline void set__AdditiveReflection_1(int32_t value)
	{
		____AdditiveReflection_1 = value;
	}

	inline static int32_t get_offset_of__BilateralUpsampling_2() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____BilateralUpsampling_2)); }
	inline int32_t get__BilateralUpsampling_2() const { return ____BilateralUpsampling_2; }
	inline int32_t* get_address_of__BilateralUpsampling_2() { return &____BilateralUpsampling_2; }
	inline void set__BilateralUpsampling_2(int32_t value)
	{
		____BilateralUpsampling_2 = value;
	}

	inline static int32_t get_offset_of__TreatBackfaceHitAsMiss_3() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____TreatBackfaceHitAsMiss_3)); }
	inline int32_t get__TreatBackfaceHitAsMiss_3() const { return ____TreatBackfaceHitAsMiss_3; }
	inline int32_t* get_address_of__TreatBackfaceHitAsMiss_3() { return &____TreatBackfaceHitAsMiss_3; }
	inline void set__TreatBackfaceHitAsMiss_3(int32_t value)
	{
		____TreatBackfaceHitAsMiss_3 = value;
	}

	inline static int32_t get_offset_of__AllowBackwardsRays_4() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____AllowBackwardsRays_4)); }
	inline int32_t get__AllowBackwardsRays_4() const { return ____AllowBackwardsRays_4; }
	inline int32_t* get_address_of__AllowBackwardsRays_4() { return &____AllowBackwardsRays_4; }
	inline void set__AllowBackwardsRays_4(int32_t value)
	{
		____AllowBackwardsRays_4 = value;
	}

	inline static int32_t get_offset_of__TraceBehindObjects_5() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____TraceBehindObjects_5)); }
	inline int32_t get__TraceBehindObjects_5() const { return ____TraceBehindObjects_5; }
	inline int32_t* get_address_of__TraceBehindObjects_5() { return &____TraceBehindObjects_5; }
	inline void set__TraceBehindObjects_5(int32_t value)
	{
		____TraceBehindObjects_5 = value;
	}

	inline static int32_t get_offset_of__MaxSteps_6() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____MaxSteps_6)); }
	inline int32_t get__MaxSteps_6() const { return ____MaxSteps_6; }
	inline int32_t* get_address_of__MaxSteps_6() { return &____MaxSteps_6; }
	inline void set__MaxSteps_6(int32_t value)
	{
		____MaxSteps_6 = value;
	}

	inline static int32_t get_offset_of__FullResolutionFiltering_7() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FullResolutionFiltering_7)); }
	inline int32_t get__FullResolutionFiltering_7() const { return ____FullResolutionFiltering_7; }
	inline int32_t* get_address_of__FullResolutionFiltering_7() { return &____FullResolutionFiltering_7; }
	inline void set__FullResolutionFiltering_7(int32_t value)
	{
		____FullResolutionFiltering_7 = value;
	}

	inline static int32_t get_offset_of__HalfResolution_8() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____HalfResolution_8)); }
	inline int32_t get__HalfResolution_8() const { return ____HalfResolution_8; }
	inline int32_t* get_address_of__HalfResolution_8() { return &____HalfResolution_8; }
	inline void set__HalfResolution_8(int32_t value)
	{
		____HalfResolution_8 = value;
	}

	inline static int32_t get_offset_of__HighlightSuppression_9() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____HighlightSuppression_9)); }
	inline int32_t get__HighlightSuppression_9() const { return ____HighlightSuppression_9; }
	inline int32_t* get_address_of__HighlightSuppression_9() { return &____HighlightSuppression_9; }
	inline void set__HighlightSuppression_9(int32_t value)
	{
		____HighlightSuppression_9 = value;
	}

	inline static int32_t get_offset_of__PixelsPerMeterAtOneMeter_10() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____PixelsPerMeterAtOneMeter_10)); }
	inline int32_t get__PixelsPerMeterAtOneMeter_10() const { return ____PixelsPerMeterAtOneMeter_10; }
	inline int32_t* get_address_of__PixelsPerMeterAtOneMeter_10() { return &____PixelsPerMeterAtOneMeter_10; }
	inline void set__PixelsPerMeterAtOneMeter_10(int32_t value)
	{
		____PixelsPerMeterAtOneMeter_10 = value;
	}

	inline static int32_t get_offset_of__ScreenEdgeFading_11() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ScreenEdgeFading_11)); }
	inline int32_t get__ScreenEdgeFading_11() const { return ____ScreenEdgeFading_11; }
	inline int32_t* get_address_of__ScreenEdgeFading_11() { return &____ScreenEdgeFading_11; }
	inline void set__ScreenEdgeFading_11(int32_t value)
	{
		____ScreenEdgeFading_11 = value;
	}

	inline static int32_t get_offset_of__ReflectionBlur_12() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ReflectionBlur_12)); }
	inline int32_t get__ReflectionBlur_12() const { return ____ReflectionBlur_12; }
	inline int32_t* get_address_of__ReflectionBlur_12() { return &____ReflectionBlur_12; }
	inline void set__ReflectionBlur_12(int32_t value)
	{
		____ReflectionBlur_12 = value;
	}

	inline static int32_t get_offset_of__MaxRayTraceDistance_13() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____MaxRayTraceDistance_13)); }
	inline int32_t get__MaxRayTraceDistance_13() const { return ____MaxRayTraceDistance_13; }
	inline int32_t* get_address_of__MaxRayTraceDistance_13() { return &____MaxRayTraceDistance_13; }
	inline void set__MaxRayTraceDistance_13(int32_t value)
	{
		____MaxRayTraceDistance_13 = value;
	}

	inline static int32_t get_offset_of__FadeDistance_14() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FadeDistance_14)); }
	inline int32_t get__FadeDistance_14() const { return ____FadeDistance_14; }
	inline int32_t* get_address_of__FadeDistance_14() { return &____FadeDistance_14; }
	inline void set__FadeDistance_14(int32_t value)
	{
		____FadeDistance_14 = value;
	}

	inline static int32_t get_offset_of__LayerThickness_15() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____LayerThickness_15)); }
	inline int32_t get__LayerThickness_15() const { return ____LayerThickness_15; }
	inline int32_t* get_address_of__LayerThickness_15() { return &____LayerThickness_15; }
	inline void set__LayerThickness_15(int32_t value)
	{
		____LayerThickness_15 = value;
	}

	inline static int32_t get_offset_of__SSRMultiplier_16() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____SSRMultiplier_16)); }
	inline int32_t get__SSRMultiplier_16() const { return ____SSRMultiplier_16; }
	inline int32_t* get_address_of__SSRMultiplier_16() { return &____SSRMultiplier_16; }
	inline void set__SSRMultiplier_16(int32_t value)
	{
		____SSRMultiplier_16 = value;
	}

	inline static int32_t get_offset_of__FresnelFade_17() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FresnelFade_17)); }
	inline int32_t get__FresnelFade_17() const { return ____FresnelFade_17; }
	inline int32_t* get_address_of__FresnelFade_17() { return &____FresnelFade_17; }
	inline void set__FresnelFade_17(int32_t value)
	{
		____FresnelFade_17 = value;
	}

	inline static int32_t get_offset_of__FresnelFadePower_18() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FresnelFadePower_18)); }
	inline int32_t get__FresnelFadePower_18() const { return ____FresnelFadePower_18; }
	inline int32_t* get_address_of__FresnelFadePower_18() { return &____FresnelFadePower_18; }
	inline void set__FresnelFadePower_18(int32_t value)
	{
		____FresnelFadePower_18 = value;
	}

	inline static int32_t get_offset_of__ReflectionBufferSize_19() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ReflectionBufferSize_19)); }
	inline int32_t get__ReflectionBufferSize_19() const { return ____ReflectionBufferSize_19; }
	inline int32_t* get_address_of__ReflectionBufferSize_19() { return &____ReflectionBufferSize_19; }
	inline void set__ReflectionBufferSize_19(int32_t value)
	{
		____ReflectionBufferSize_19 = value;
	}

	inline static int32_t get_offset_of__ScreenSize_20() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ScreenSize_20)); }
	inline int32_t get__ScreenSize_20() const { return ____ScreenSize_20; }
	inline int32_t* get_address_of__ScreenSize_20() { return &____ScreenSize_20; }
	inline void set__ScreenSize_20(int32_t value)
	{
		____ScreenSize_20 = value;
	}

	inline static int32_t get_offset_of__InvScreenSize_21() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____InvScreenSize_21)); }
	inline int32_t get__InvScreenSize_21() const { return ____InvScreenSize_21; }
	inline int32_t* get_address_of__InvScreenSize_21() { return &____InvScreenSize_21; }
	inline void set__InvScreenSize_21(int32_t value)
	{
		____InvScreenSize_21 = value;
	}

	inline static int32_t get_offset_of__ProjInfo_22() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ProjInfo_22)); }
	inline int32_t get__ProjInfo_22() const { return ____ProjInfo_22; }
	inline int32_t* get_address_of__ProjInfo_22() { return &____ProjInfo_22; }
	inline void set__ProjInfo_22(int32_t value)
	{
		____ProjInfo_22 = value;
	}

	inline static int32_t get_offset_of__CameraClipInfo_23() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____CameraClipInfo_23)); }
	inline int32_t get__CameraClipInfo_23() const { return ____CameraClipInfo_23; }
	inline int32_t* get_address_of__CameraClipInfo_23() { return &____CameraClipInfo_23; }
	inline void set__CameraClipInfo_23(int32_t value)
	{
		____CameraClipInfo_23 = value;
	}

	inline static int32_t get_offset_of__ProjectToPixelMatrix_24() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____ProjectToPixelMatrix_24)); }
	inline int32_t get__ProjectToPixelMatrix_24() const { return ____ProjectToPixelMatrix_24; }
	inline int32_t* get_address_of__ProjectToPixelMatrix_24() { return &____ProjectToPixelMatrix_24; }
	inline void set__ProjectToPixelMatrix_24(int32_t value)
	{
		____ProjectToPixelMatrix_24 = value;
	}

	inline static int32_t get_offset_of__WorldToCameraMatrix_25() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____WorldToCameraMatrix_25)); }
	inline int32_t get__WorldToCameraMatrix_25() const { return ____WorldToCameraMatrix_25; }
	inline int32_t* get_address_of__WorldToCameraMatrix_25() { return &____WorldToCameraMatrix_25; }
	inline void set__WorldToCameraMatrix_25(int32_t value)
	{
		____WorldToCameraMatrix_25 = value;
	}

	inline static int32_t get_offset_of__CameraToWorldMatrix_26() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____CameraToWorldMatrix_26)); }
	inline int32_t get__CameraToWorldMatrix_26() const { return ____CameraToWorldMatrix_26; }
	inline int32_t* get_address_of__CameraToWorldMatrix_26() { return &____CameraToWorldMatrix_26; }
	inline void set__CameraToWorldMatrix_26(int32_t value)
	{
		____CameraToWorldMatrix_26 = value;
	}

	inline static int32_t get_offset_of__Axis_27() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____Axis_27)); }
	inline int32_t get__Axis_27() const { return ____Axis_27; }
	inline int32_t* get_address_of__Axis_27() { return &____Axis_27; }
	inline void set__Axis_27(int32_t value)
	{
		____Axis_27 = value;
	}

	inline static int32_t get_offset_of__CurrentMipLevel_28() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____CurrentMipLevel_28)); }
	inline int32_t get__CurrentMipLevel_28() const { return ____CurrentMipLevel_28; }
	inline int32_t* get_address_of__CurrentMipLevel_28() { return &____CurrentMipLevel_28; }
	inline void set__CurrentMipLevel_28(int32_t value)
	{
		____CurrentMipLevel_28 = value;
	}

	inline static int32_t get_offset_of__NormalAndRoughnessTexture_29() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____NormalAndRoughnessTexture_29)); }
	inline int32_t get__NormalAndRoughnessTexture_29() const { return ____NormalAndRoughnessTexture_29; }
	inline int32_t* get_address_of__NormalAndRoughnessTexture_29() { return &____NormalAndRoughnessTexture_29; }
	inline void set__NormalAndRoughnessTexture_29(int32_t value)
	{
		____NormalAndRoughnessTexture_29 = value;
	}

	inline static int32_t get_offset_of__HitPointTexture_30() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____HitPointTexture_30)); }
	inline int32_t get__HitPointTexture_30() const { return ____HitPointTexture_30; }
	inline int32_t* get_address_of__HitPointTexture_30() { return &____HitPointTexture_30; }
	inline void set__HitPointTexture_30(int32_t value)
	{
		____HitPointTexture_30 = value;
	}

	inline static int32_t get_offset_of__BlurTexture_31() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____BlurTexture_31)); }
	inline int32_t get__BlurTexture_31() const { return ____BlurTexture_31; }
	inline int32_t* get_address_of__BlurTexture_31() { return &____BlurTexture_31; }
	inline void set__BlurTexture_31(int32_t value)
	{
		____BlurTexture_31 = value;
	}

	inline static int32_t get_offset_of__FilteredReflections_32() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FilteredReflections_32)); }
	inline int32_t get__FilteredReflections_32() const { return ____FilteredReflections_32; }
	inline int32_t* get_address_of__FilteredReflections_32() { return &____FilteredReflections_32; }
	inline void set__FilteredReflections_32(int32_t value)
	{
		____FilteredReflections_32 = value;
	}

	inline static int32_t get_offset_of__FinalReflectionTexture_33() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____FinalReflectionTexture_33)); }
	inline int32_t get__FinalReflectionTexture_33() const { return ____FinalReflectionTexture_33; }
	inline int32_t* get_address_of__FinalReflectionTexture_33() { return &____FinalReflectionTexture_33; }
	inline void set__FinalReflectionTexture_33(int32_t value)
	{
		____FinalReflectionTexture_33 = value;
	}

	inline static int32_t get_offset_of__TempTexture_34() { return static_cast<int32_t>(offsetof(Uniforms_t2970573890_StaticFields, ____TempTexture_34)); }
	inline int32_t get__TempTexture_34() const { return ____TempTexture_34; }
	inline int32_t* get_address_of__TempTexture_34() { return &____TempTexture_34; }
	inline void set__TempTexture_34(int32_t value)
	{
		____TempTexture_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T2970573890_H
#ifndef UNIFORMS_T3024963833_H
#define UNIFORMS_T3024963833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TaaComponent/Uniforms
struct  Uniforms_t3024963833  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3024963833_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_Jitter
	int32_t ____Jitter_0;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_SharpenParameters
	int32_t ____SharpenParameters_1;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_FinalBlendParameters
	int32_t ____FinalBlendParameters_2;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_HistoryTex
	int32_t ____HistoryTex_3;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent/Uniforms::_MainTex
	int32_t ____MainTex_4;

public:
	inline static int32_t get_offset_of__Jitter_0() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____Jitter_0)); }
	inline int32_t get__Jitter_0() const { return ____Jitter_0; }
	inline int32_t* get_address_of__Jitter_0() { return &____Jitter_0; }
	inline void set__Jitter_0(int32_t value)
	{
		____Jitter_0 = value;
	}

	inline static int32_t get_offset_of__SharpenParameters_1() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____SharpenParameters_1)); }
	inline int32_t get__SharpenParameters_1() const { return ____SharpenParameters_1; }
	inline int32_t* get_address_of__SharpenParameters_1() { return &____SharpenParameters_1; }
	inline void set__SharpenParameters_1(int32_t value)
	{
		____SharpenParameters_1 = value;
	}

	inline static int32_t get_offset_of__FinalBlendParameters_2() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____FinalBlendParameters_2)); }
	inline int32_t get__FinalBlendParameters_2() const { return ____FinalBlendParameters_2; }
	inline int32_t* get_address_of__FinalBlendParameters_2() { return &____FinalBlendParameters_2; }
	inline void set__FinalBlendParameters_2(int32_t value)
	{
		____FinalBlendParameters_2 = value;
	}

	inline static int32_t get_offset_of__HistoryTex_3() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____HistoryTex_3)); }
	inline int32_t get__HistoryTex_3() const { return ____HistoryTex_3; }
	inline int32_t* get_address_of__HistoryTex_3() { return &____HistoryTex_3; }
	inline void set__HistoryTex_3(int32_t value)
	{
		____HistoryTex_3 = value;
	}

	inline static int32_t get_offset_of__MainTex_4() { return static_cast<int32_t>(offsetof(Uniforms_t3024963833_StaticFields, ____MainTex_4)); }
	inline int32_t get__MainTex_4() const { return ____MainTex_4; }
	inline int32_t* get_address_of__MainTex_4() { return &____MainTex_4; }
	inline void set__MainTex_4(int32_t value)
	{
		____MainTex_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3024963833_H
#ifndef UNIFORMS_T1046717683_H
#define UNIFORMS_T1046717683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutComponent/Uniforms
struct  Uniforms_t1046717683  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t1046717683_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.UserLutComponent/Uniforms::_UserLut
	int32_t ____UserLut_0;
	// System.Int32 UnityEngine.PostProcessing.UserLutComponent/Uniforms::_UserLut_Params
	int32_t ____UserLut_Params_1;

public:
	inline static int32_t get_offset_of__UserLut_0() { return static_cast<int32_t>(offsetof(Uniforms_t1046717683_StaticFields, ____UserLut_0)); }
	inline int32_t get__UserLut_0() const { return ____UserLut_0; }
	inline int32_t* get_address_of__UserLut_0() { return &____UserLut_0; }
	inline void set__UserLut_0(int32_t value)
	{
		____UserLut_0 = value;
	}

	inline static int32_t get_offset_of__UserLut_Params_1() { return static_cast<int32_t>(offsetof(Uniforms_t1046717683_StaticFields, ____UserLut_Params_1)); }
	inline int32_t get__UserLut_Params_1() const { return ____UserLut_Params_1; }
	inline int32_t* get_address_of__UserLut_Params_1() { return &____UserLut_Params_1; }
	inline void set__UserLut_Params_1(int32_t value)
	{
		____UserLut_Params_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T1046717683_H
#ifndef UNIFORMS_T2205824134_H
#define UNIFORMS_T2205824134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteComponent/Uniforms
struct  Uniforms_t2205824134  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t2205824134_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Color
	int32_t ____Vignette_Color_0;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Center
	int32_t ____Vignette_Center_1;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Settings
	int32_t ____Vignette_Settings_2;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Mask
	int32_t ____Vignette_Mask_3;
	// System.Int32 UnityEngine.PostProcessing.VignetteComponent/Uniforms::_Vignette_Opacity
	int32_t ____Vignette_Opacity_4;

public:
	inline static int32_t get_offset_of__Vignette_Color_0() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Color_0)); }
	inline int32_t get__Vignette_Color_0() const { return ____Vignette_Color_0; }
	inline int32_t* get_address_of__Vignette_Color_0() { return &____Vignette_Color_0; }
	inline void set__Vignette_Color_0(int32_t value)
	{
		____Vignette_Color_0 = value;
	}

	inline static int32_t get_offset_of__Vignette_Center_1() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Center_1)); }
	inline int32_t get__Vignette_Center_1() const { return ____Vignette_Center_1; }
	inline int32_t* get_address_of__Vignette_Center_1() { return &____Vignette_Center_1; }
	inline void set__Vignette_Center_1(int32_t value)
	{
		____Vignette_Center_1 = value;
	}

	inline static int32_t get_offset_of__Vignette_Settings_2() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Settings_2)); }
	inline int32_t get__Vignette_Settings_2() const { return ____Vignette_Settings_2; }
	inline int32_t* get_address_of__Vignette_Settings_2() { return &____Vignette_Settings_2; }
	inline void set__Vignette_Settings_2(int32_t value)
	{
		____Vignette_Settings_2 = value;
	}

	inline static int32_t get_offset_of__Vignette_Mask_3() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Mask_3)); }
	inline int32_t get__Vignette_Mask_3() const { return ____Vignette_Mask_3; }
	inline int32_t* get_address_of__Vignette_Mask_3() { return &____Vignette_Mask_3; }
	inline void set__Vignette_Mask_3(int32_t value)
	{
		____Vignette_Mask_3 = value;
	}

	inline static int32_t get_offset_of__Vignette_Opacity_4() { return static_cast<int32_t>(offsetof(Uniforms_t2205824134_StaticFields, ____Vignette_Opacity_4)); }
	inline int32_t get__Vignette_Opacity_4() const { return ____Vignette_Opacity_4; }
	inline int32_t* get_address_of__Vignette_Opacity_4() { return &____Vignette_Opacity_4; }
	inline void set__Vignette_Opacity_4(int32_t value)
	{
		____Vignette_Opacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T2205824134_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef FXAACONSOLESETTINGS_T950185286_H
#define FXAACONSOLESETTINGS_T950185286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings
struct  FxaaConsoleSettings_t950185286 
{
public:
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings::subpixelSpreadAmount
	float ___subpixelSpreadAmount_0;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings::edgeSharpnessAmount
	float ___edgeSharpnessAmount_1;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings::edgeDetectionThreshold
	float ___edgeDetectionThreshold_2;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings::minimumRequiredLuminance
	float ___minimumRequiredLuminance_3;

public:
	inline static int32_t get_offset_of_subpixelSpreadAmount_0() { return static_cast<int32_t>(offsetof(FxaaConsoleSettings_t950185286, ___subpixelSpreadAmount_0)); }
	inline float get_subpixelSpreadAmount_0() const { return ___subpixelSpreadAmount_0; }
	inline float* get_address_of_subpixelSpreadAmount_0() { return &___subpixelSpreadAmount_0; }
	inline void set_subpixelSpreadAmount_0(float value)
	{
		___subpixelSpreadAmount_0 = value;
	}

	inline static int32_t get_offset_of_edgeSharpnessAmount_1() { return static_cast<int32_t>(offsetof(FxaaConsoleSettings_t950185286, ___edgeSharpnessAmount_1)); }
	inline float get_edgeSharpnessAmount_1() const { return ___edgeSharpnessAmount_1; }
	inline float* get_address_of_edgeSharpnessAmount_1() { return &___edgeSharpnessAmount_1; }
	inline void set_edgeSharpnessAmount_1(float value)
	{
		___edgeSharpnessAmount_1 = value;
	}

	inline static int32_t get_offset_of_edgeDetectionThreshold_2() { return static_cast<int32_t>(offsetof(FxaaConsoleSettings_t950185286, ___edgeDetectionThreshold_2)); }
	inline float get_edgeDetectionThreshold_2() const { return ___edgeDetectionThreshold_2; }
	inline float* get_address_of_edgeDetectionThreshold_2() { return &___edgeDetectionThreshold_2; }
	inline void set_edgeDetectionThreshold_2(float value)
	{
		___edgeDetectionThreshold_2 = value;
	}

	inline static int32_t get_offset_of_minimumRequiredLuminance_3() { return static_cast<int32_t>(offsetof(FxaaConsoleSettings_t950185286, ___minimumRequiredLuminance_3)); }
	inline float get_minimumRequiredLuminance_3() const { return ___minimumRequiredLuminance_3; }
	inline float* get_address_of_minimumRequiredLuminance_3() { return &___minimumRequiredLuminance_3; }
	inline void set_minimumRequiredLuminance_3(float value)
	{
		___minimumRequiredLuminance_3 = value;
	}
};

struct FxaaConsoleSettings_t950185286_StaticFields
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings[] UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings::presets
	FxaaConsoleSettingsU5BU5D_t1126991587* ___presets_4;

public:
	inline static int32_t get_offset_of_presets_4() { return static_cast<int32_t>(offsetof(FxaaConsoleSettings_t950185286_StaticFields, ___presets_4)); }
	inline FxaaConsoleSettingsU5BU5D_t1126991587* get_presets_4() const { return ___presets_4; }
	inline FxaaConsoleSettingsU5BU5D_t1126991587** get_address_of_presets_4() { return &___presets_4; }
	inline void set_presets_4(FxaaConsoleSettingsU5BU5D_t1126991587* value)
	{
		___presets_4 = value;
		Il2CppCodeGenWriteBarrier((&___presets_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAACONSOLESETTINGS_T950185286_H
#ifndef FXAAQUALITYSETTINGS_T1558211909_H
#define FXAAQUALITYSETTINGS_T1558211909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings
struct  FxaaQualitySettings_t1558211909 
{
public:
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings::subpixelAliasingRemovalAmount
	float ___subpixelAliasingRemovalAmount_0;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings::edgeDetectionThreshold
	float ___edgeDetectionThreshold_1;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings::minimumRequiredLuminance
	float ___minimumRequiredLuminance_2;

public:
	inline static int32_t get_offset_of_subpixelAliasingRemovalAmount_0() { return static_cast<int32_t>(offsetof(FxaaQualitySettings_t1558211909, ___subpixelAliasingRemovalAmount_0)); }
	inline float get_subpixelAliasingRemovalAmount_0() const { return ___subpixelAliasingRemovalAmount_0; }
	inline float* get_address_of_subpixelAliasingRemovalAmount_0() { return &___subpixelAliasingRemovalAmount_0; }
	inline void set_subpixelAliasingRemovalAmount_0(float value)
	{
		___subpixelAliasingRemovalAmount_0 = value;
	}

	inline static int32_t get_offset_of_edgeDetectionThreshold_1() { return static_cast<int32_t>(offsetof(FxaaQualitySettings_t1558211909, ___edgeDetectionThreshold_1)); }
	inline float get_edgeDetectionThreshold_1() const { return ___edgeDetectionThreshold_1; }
	inline float* get_address_of_edgeDetectionThreshold_1() { return &___edgeDetectionThreshold_1; }
	inline void set_edgeDetectionThreshold_1(float value)
	{
		___edgeDetectionThreshold_1 = value;
	}

	inline static int32_t get_offset_of_minimumRequiredLuminance_2() { return static_cast<int32_t>(offsetof(FxaaQualitySettings_t1558211909, ___minimumRequiredLuminance_2)); }
	inline float get_minimumRequiredLuminance_2() const { return ___minimumRequiredLuminance_2; }
	inline float* get_address_of_minimumRequiredLuminance_2() { return &___minimumRequiredLuminance_2; }
	inline void set_minimumRequiredLuminance_2(float value)
	{
		___minimumRequiredLuminance_2 = value;
	}
};

struct FxaaQualitySettings_t1558211909_StaticFields
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings[] UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings::presets
	FxaaQualitySettingsU5BU5D_t3129287688* ___presets_3;

public:
	inline static int32_t get_offset_of_presets_3() { return static_cast<int32_t>(offsetof(FxaaQualitySettings_t1558211909_StaticFields, ___presets_3)); }
	inline FxaaQualitySettingsU5BU5D_t3129287688* get_presets_3() const { return ___presets_3; }
	inline FxaaQualitySettingsU5BU5D_t3129287688** get_address_of_presets_3() { return &___presets_3; }
	inline void set_presets_3(FxaaQualitySettingsU5BU5D_t3129287688* value)
	{
		___presets_3 = value;
		Il2CppCodeGenWriteBarrier((&___presets_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAAQUALITYSETTINGS_T1558211909_H
#ifndef TAASETTINGS_T2709374970_H
#define TAASETTINGS_T2709374970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/TaaSettings
struct  TaaSettings_t2709374970 
{
public:
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::jitterSpread
	float ___jitterSpread_0;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::sharpen
	float ___sharpen_1;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::stationaryBlending
	float ___stationaryBlending_2;
	// System.Single UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::motionBlending
	float ___motionBlending_3;

public:
	inline static int32_t get_offset_of_jitterSpread_0() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___jitterSpread_0)); }
	inline float get_jitterSpread_0() const { return ___jitterSpread_0; }
	inline float* get_address_of_jitterSpread_0() { return &___jitterSpread_0; }
	inline void set_jitterSpread_0(float value)
	{
		___jitterSpread_0 = value;
	}

	inline static int32_t get_offset_of_sharpen_1() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___sharpen_1)); }
	inline float get_sharpen_1() const { return ___sharpen_1; }
	inline float* get_address_of_sharpen_1() { return &___sharpen_1; }
	inline void set_sharpen_1(float value)
	{
		___sharpen_1 = value;
	}

	inline static int32_t get_offset_of_stationaryBlending_2() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___stationaryBlending_2)); }
	inline float get_stationaryBlending_2() const { return ___stationaryBlending_2; }
	inline float* get_address_of_stationaryBlending_2() { return &___stationaryBlending_2; }
	inline void set_stationaryBlending_2(float value)
	{
		___stationaryBlending_2 = value;
	}

	inline static int32_t get_offset_of_motionBlending_3() { return static_cast<int32_t>(offsetof(TaaSettings_t2709374970, ___motionBlending_3)); }
	inline float get_motionBlending_3() const { return ___motionBlending_3; }
	inline float* get_address_of_motionBlending_3() { return &___motionBlending_3; }
	inline void set_motionBlending_3(float value)
	{
		___motionBlending_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAASETTINGS_T2709374970_H
#ifndef BLOOMSETTINGS_T2599855122_H
#define BLOOMSETTINGS_T2599855122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomModel/BloomSettings
struct  BloomSettings_t2599855122 
{
public:
	// System.Single UnityEngine.PostProcessing.BloomModel/BloomSettings::intensity
	float ___intensity_0;
	// System.Single UnityEngine.PostProcessing.BloomModel/BloomSettings::threshold
	float ___threshold_1;
	// System.Single UnityEngine.PostProcessing.BloomModel/BloomSettings::softKnee
	float ___softKnee_2;
	// System.Single UnityEngine.PostProcessing.BloomModel/BloomSettings::radius
	float ___radius_3;
	// System.Boolean UnityEngine.PostProcessing.BloomModel/BloomSettings::antiFlicker
	bool ___antiFlicker_4;

public:
	inline static int32_t get_offset_of_intensity_0() { return static_cast<int32_t>(offsetof(BloomSettings_t2599855122, ___intensity_0)); }
	inline float get_intensity_0() const { return ___intensity_0; }
	inline float* get_address_of_intensity_0() { return &___intensity_0; }
	inline void set_intensity_0(float value)
	{
		___intensity_0 = value;
	}

	inline static int32_t get_offset_of_threshold_1() { return static_cast<int32_t>(offsetof(BloomSettings_t2599855122, ___threshold_1)); }
	inline float get_threshold_1() const { return ___threshold_1; }
	inline float* get_address_of_threshold_1() { return &___threshold_1; }
	inline void set_threshold_1(float value)
	{
		___threshold_1 = value;
	}

	inline static int32_t get_offset_of_softKnee_2() { return static_cast<int32_t>(offsetof(BloomSettings_t2599855122, ___softKnee_2)); }
	inline float get_softKnee_2() const { return ___softKnee_2; }
	inline float* get_address_of_softKnee_2() { return &___softKnee_2; }
	inline void set_softKnee_2(float value)
	{
		___softKnee_2 = value;
	}

	inline static int32_t get_offset_of_radius_3() { return static_cast<int32_t>(offsetof(BloomSettings_t2599855122, ___radius_3)); }
	inline float get_radius_3() const { return ___radius_3; }
	inline float* get_address_of_radius_3() { return &___radius_3; }
	inline void set_radius_3(float value)
	{
		___radius_3 = value;
	}

	inline static int32_t get_offset_of_antiFlicker_4() { return static_cast<int32_t>(offsetof(BloomSettings_t2599855122, ___antiFlicker_4)); }
	inline bool get_antiFlicker_4() const { return ___antiFlicker_4; }
	inline bool* get_address_of_antiFlicker_4() { return &___antiFlicker_4; }
	inline void set_antiFlicker_4(bool value)
	{
		___antiFlicker_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.BloomModel/BloomSettings
struct BloomSettings_t2599855122_marshaled_pinvoke
{
	float ___intensity_0;
	float ___threshold_1;
	float ___softKnee_2;
	float ___radius_3;
	int32_t ___antiFlicker_4;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.BloomModel/BloomSettings
struct BloomSettings_t2599855122_marshaled_com
{
	float ___intensity_0;
	float ___threshold_1;
	float ___softKnee_2;
	float ___radius_3;
	int32_t ___antiFlicker_4;
};
#endif // BLOOMSETTINGS_T2599855122_H
#ifndef LENSDIRTSETTINGS_T3693422705_H
#define LENSDIRTSETTINGS_T3693422705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomModel/LensDirtSettings
struct  LensDirtSettings_t3693422705 
{
public:
	// UnityEngine.Texture UnityEngine.PostProcessing.BloomModel/LensDirtSettings::texture
	Texture_t3661962703 * ___texture_0;
	// System.Single UnityEngine.PostProcessing.BloomModel/LensDirtSettings::intensity
	float ___intensity_1;

public:
	inline static int32_t get_offset_of_texture_0() { return static_cast<int32_t>(offsetof(LensDirtSettings_t3693422705, ___texture_0)); }
	inline Texture_t3661962703 * get_texture_0() const { return ___texture_0; }
	inline Texture_t3661962703 ** get_address_of_texture_0() { return &___texture_0; }
	inline void set_texture_0(Texture_t3661962703 * value)
	{
		___texture_0 = value;
		Il2CppCodeGenWriteBarrier((&___texture_0), value);
	}

	inline static int32_t get_offset_of_intensity_1() { return static_cast<int32_t>(offsetof(LensDirtSettings_t3693422705, ___intensity_1)); }
	inline float get_intensity_1() const { return ___intensity_1; }
	inline float* get_address_of_intensity_1() { return &___intensity_1; }
	inline void set_intensity_1(float value)
	{
		___intensity_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.BloomModel/LensDirtSettings
struct LensDirtSettings_t3693422705_marshaled_pinvoke
{
	Texture_t3661962703 * ___texture_0;
	float ___intensity_1;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.BloomModel/LensDirtSettings
struct LensDirtSettings_t3693422705_marshaled_com
{
	Texture_t3661962703 * ___texture_0;
	float ___intensity_1;
};
#endif // LENSDIRTSETTINGS_T3693422705_H
#ifndef DEPTHSETTINGS_T1820272864_H
#define DEPTHSETTINGS_T1820272864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsModel/DepthSettings
struct  DepthSettings_t1820272864 
{
public:
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/DepthSettings::scale
	float ___scale_0;

public:
	inline static int32_t get_offset_of_scale_0() { return static_cast<int32_t>(offsetof(DepthSettings_t1820272864, ___scale_0)); }
	inline float get_scale_0() const { return ___scale_0; }
	inline float* get_address_of_scale_0() { return &___scale_0; }
	inline void set_scale_0(float value)
	{
		___scale_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHSETTINGS_T1820272864_H
#ifndef MOTIONVECTORSSETTINGS_T3857813598_H
#define MOTIONVECTORSSETTINGS_T3857813598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings
struct  MotionVectorsSettings_t3857813598 
{
public:
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::sourceOpacity
	float ___sourceOpacity_0;
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::motionImageOpacity
	float ___motionImageOpacity_1;
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::motionImageAmplitude
	float ___motionImageAmplitude_2;
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::motionVectorsOpacity
	float ___motionVectorsOpacity_3;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::motionVectorsResolution
	int32_t ___motionVectorsResolution_4;
	// System.Single UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::motionVectorsAmplitude
	float ___motionVectorsAmplitude_5;

public:
	inline static int32_t get_offset_of_sourceOpacity_0() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___sourceOpacity_0)); }
	inline float get_sourceOpacity_0() const { return ___sourceOpacity_0; }
	inline float* get_address_of_sourceOpacity_0() { return &___sourceOpacity_0; }
	inline void set_sourceOpacity_0(float value)
	{
		___sourceOpacity_0 = value;
	}

	inline static int32_t get_offset_of_motionImageOpacity_1() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___motionImageOpacity_1)); }
	inline float get_motionImageOpacity_1() const { return ___motionImageOpacity_1; }
	inline float* get_address_of_motionImageOpacity_1() { return &___motionImageOpacity_1; }
	inline void set_motionImageOpacity_1(float value)
	{
		___motionImageOpacity_1 = value;
	}

	inline static int32_t get_offset_of_motionImageAmplitude_2() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___motionImageAmplitude_2)); }
	inline float get_motionImageAmplitude_2() const { return ___motionImageAmplitude_2; }
	inline float* get_address_of_motionImageAmplitude_2() { return &___motionImageAmplitude_2; }
	inline void set_motionImageAmplitude_2(float value)
	{
		___motionImageAmplitude_2 = value;
	}

	inline static int32_t get_offset_of_motionVectorsOpacity_3() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___motionVectorsOpacity_3)); }
	inline float get_motionVectorsOpacity_3() const { return ___motionVectorsOpacity_3; }
	inline float* get_address_of_motionVectorsOpacity_3() { return &___motionVectorsOpacity_3; }
	inline void set_motionVectorsOpacity_3(float value)
	{
		___motionVectorsOpacity_3 = value;
	}

	inline static int32_t get_offset_of_motionVectorsResolution_4() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___motionVectorsResolution_4)); }
	inline int32_t get_motionVectorsResolution_4() const { return ___motionVectorsResolution_4; }
	inline int32_t* get_address_of_motionVectorsResolution_4() { return &___motionVectorsResolution_4; }
	inline void set_motionVectorsResolution_4(int32_t value)
	{
		___motionVectorsResolution_4 = value;
	}

	inline static int32_t get_offset_of_motionVectorsAmplitude_5() { return static_cast<int32_t>(offsetof(MotionVectorsSettings_t3857813598, ___motionVectorsAmplitude_5)); }
	inline float get_motionVectorsAmplitude_5() const { return ___motionVectorsAmplitude_5; }
	inline float* get_address_of_motionVectorsAmplitude_5() { return &___motionVectorsAmplitude_5; }
	inline void set_motionVectorsAmplitude_5(float value)
	{
		___motionVectorsAmplitude_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONVECTORSSETTINGS_T3857813598_H
#ifndef SETTINGS_T2111398455_H
#define SETTINGS_T2111398455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ChromaticAberrationModel/Settings
struct  Settings_t2111398455 
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.ChromaticAberrationModel/Settings::spectralTexture
	Texture2D_t3840446185 * ___spectralTexture_0;
	// System.Single UnityEngine.PostProcessing.ChromaticAberrationModel/Settings::intensity
	float ___intensity_1;

public:
	inline static int32_t get_offset_of_spectralTexture_0() { return static_cast<int32_t>(offsetof(Settings_t2111398455, ___spectralTexture_0)); }
	inline Texture2D_t3840446185 * get_spectralTexture_0() const { return ___spectralTexture_0; }
	inline Texture2D_t3840446185 ** get_address_of_spectralTexture_0() { return &___spectralTexture_0; }
	inline void set_spectralTexture_0(Texture2D_t3840446185 * value)
	{
		___spectralTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___spectralTexture_0), value);
	}

	inline static int32_t get_offset_of_intensity_1() { return static_cast<int32_t>(offsetof(Settings_t2111398455, ___intensity_1)); }
	inline float get_intensity_1() const { return ___intensity_1; }
	inline float* get_address_of_intensity_1() { return &___intensity_1; }
	inline void set_intensity_1(float value)
	{
		___intensity_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ChromaticAberrationModel/Settings
struct Settings_t2111398455_marshaled_pinvoke
{
	Texture2D_t3840446185 * ___spectralTexture_0;
	float ___intensity_1;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ChromaticAberrationModel/Settings
struct Settings_t2111398455_marshaled_com
{
	Texture2D_t3840446185 * ___spectralTexture_0;
	float ___intensity_1;
};
#endif // SETTINGS_T2111398455_H
#ifndef BASICSETTINGS_T838098426_H
#define BASICSETTINGS_T838098426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/BasicSettings
struct  BasicSettings_t838098426 
{
public:
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::postExposure
	float ___postExposure_0;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::temperature
	float ___temperature_1;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::tint
	float ___tint_2;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::hueShift
	float ___hueShift_3;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::saturation
	float ___saturation_4;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::contrast
	float ___contrast_5;

public:
	inline static int32_t get_offset_of_postExposure_0() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___postExposure_0)); }
	inline float get_postExposure_0() const { return ___postExposure_0; }
	inline float* get_address_of_postExposure_0() { return &___postExposure_0; }
	inline void set_postExposure_0(float value)
	{
		___postExposure_0 = value;
	}

	inline static int32_t get_offset_of_temperature_1() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___temperature_1)); }
	inline float get_temperature_1() const { return ___temperature_1; }
	inline float* get_address_of_temperature_1() { return &___temperature_1; }
	inline void set_temperature_1(float value)
	{
		___temperature_1 = value;
	}

	inline static int32_t get_offset_of_tint_2() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___tint_2)); }
	inline float get_tint_2() const { return ___tint_2; }
	inline float* get_address_of_tint_2() { return &___tint_2; }
	inline void set_tint_2(float value)
	{
		___tint_2 = value;
	}

	inline static int32_t get_offset_of_hueShift_3() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___hueShift_3)); }
	inline float get_hueShift_3() const { return ___hueShift_3; }
	inline float* get_address_of_hueShift_3() { return &___hueShift_3; }
	inline void set_hueShift_3(float value)
	{
		___hueShift_3 = value;
	}

	inline static int32_t get_offset_of_saturation_4() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___saturation_4)); }
	inline float get_saturation_4() const { return ___saturation_4; }
	inline float* get_address_of_saturation_4() { return &___saturation_4; }
	inline void set_saturation_4(float value)
	{
		___saturation_4 = value;
	}

	inline static int32_t get_offset_of_contrast_5() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___contrast_5)); }
	inline float get_contrast_5() const { return ___contrast_5; }
	inline float* get_address_of_contrast_5() { return &___contrast_5; }
	inline void set_contrast_5(float value)
	{
		___contrast_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICSETTINGS_T838098426_H
#ifndef CURVESSETTINGS_T2830270037_H
#define CURVESSETTINGS_T2830270037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct  CurvesSettings_t2830270037 
{
public:
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::master
	ColorGradingCurve_t2000571184 * ___master_0;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::red
	ColorGradingCurve_t2000571184 * ___red_1;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::green
	ColorGradingCurve_t2000571184 * ___green_2;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::blue
	ColorGradingCurve_t2000571184 * ___blue_3;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::hueVShue
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::hueVSsat
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::satVSsat
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::lumVSsat
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurrentEditingCurve
	int32_t ___e_CurrentEditingCurve_8;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveY
	bool ___e_CurveY_9;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveR
	bool ___e_CurveR_10;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveG
	bool ___e_CurveG_11;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveB
	bool ___e_CurveB_12;

public:
	inline static int32_t get_offset_of_master_0() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___master_0)); }
	inline ColorGradingCurve_t2000571184 * get_master_0() const { return ___master_0; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_master_0() { return &___master_0; }
	inline void set_master_0(ColorGradingCurve_t2000571184 * value)
	{
		___master_0 = value;
		Il2CppCodeGenWriteBarrier((&___master_0), value);
	}

	inline static int32_t get_offset_of_red_1() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___red_1)); }
	inline ColorGradingCurve_t2000571184 * get_red_1() const { return ___red_1; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_red_1() { return &___red_1; }
	inline void set_red_1(ColorGradingCurve_t2000571184 * value)
	{
		___red_1 = value;
		Il2CppCodeGenWriteBarrier((&___red_1), value);
	}

	inline static int32_t get_offset_of_green_2() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___green_2)); }
	inline ColorGradingCurve_t2000571184 * get_green_2() const { return ___green_2; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_green_2() { return &___green_2; }
	inline void set_green_2(ColorGradingCurve_t2000571184 * value)
	{
		___green_2 = value;
		Il2CppCodeGenWriteBarrier((&___green_2), value);
	}

	inline static int32_t get_offset_of_blue_3() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___blue_3)); }
	inline ColorGradingCurve_t2000571184 * get_blue_3() const { return ___blue_3; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_blue_3() { return &___blue_3; }
	inline void set_blue_3(ColorGradingCurve_t2000571184 * value)
	{
		___blue_3 = value;
		Il2CppCodeGenWriteBarrier((&___blue_3), value);
	}

	inline static int32_t get_offset_of_hueVShue_4() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___hueVShue_4)); }
	inline ColorGradingCurve_t2000571184 * get_hueVShue_4() const { return ___hueVShue_4; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_hueVShue_4() { return &___hueVShue_4; }
	inline void set_hueVShue_4(ColorGradingCurve_t2000571184 * value)
	{
		___hueVShue_4 = value;
		Il2CppCodeGenWriteBarrier((&___hueVShue_4), value);
	}

	inline static int32_t get_offset_of_hueVSsat_5() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___hueVSsat_5)); }
	inline ColorGradingCurve_t2000571184 * get_hueVSsat_5() const { return ___hueVSsat_5; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_hueVSsat_5() { return &___hueVSsat_5; }
	inline void set_hueVSsat_5(ColorGradingCurve_t2000571184 * value)
	{
		___hueVSsat_5 = value;
		Il2CppCodeGenWriteBarrier((&___hueVSsat_5), value);
	}

	inline static int32_t get_offset_of_satVSsat_6() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___satVSsat_6)); }
	inline ColorGradingCurve_t2000571184 * get_satVSsat_6() const { return ___satVSsat_6; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_satVSsat_6() { return &___satVSsat_6; }
	inline void set_satVSsat_6(ColorGradingCurve_t2000571184 * value)
	{
		___satVSsat_6 = value;
		Il2CppCodeGenWriteBarrier((&___satVSsat_6), value);
	}

	inline static int32_t get_offset_of_lumVSsat_7() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___lumVSsat_7)); }
	inline ColorGradingCurve_t2000571184 * get_lumVSsat_7() const { return ___lumVSsat_7; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_lumVSsat_7() { return &___lumVSsat_7; }
	inline void set_lumVSsat_7(ColorGradingCurve_t2000571184 * value)
	{
		___lumVSsat_7 = value;
		Il2CppCodeGenWriteBarrier((&___lumVSsat_7), value);
	}

	inline static int32_t get_offset_of_e_CurrentEditingCurve_8() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurrentEditingCurve_8)); }
	inline int32_t get_e_CurrentEditingCurve_8() const { return ___e_CurrentEditingCurve_8; }
	inline int32_t* get_address_of_e_CurrentEditingCurve_8() { return &___e_CurrentEditingCurve_8; }
	inline void set_e_CurrentEditingCurve_8(int32_t value)
	{
		___e_CurrentEditingCurve_8 = value;
	}

	inline static int32_t get_offset_of_e_CurveY_9() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveY_9)); }
	inline bool get_e_CurveY_9() const { return ___e_CurveY_9; }
	inline bool* get_address_of_e_CurveY_9() { return &___e_CurveY_9; }
	inline void set_e_CurveY_9(bool value)
	{
		___e_CurveY_9 = value;
	}

	inline static int32_t get_offset_of_e_CurveR_10() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveR_10)); }
	inline bool get_e_CurveR_10() const { return ___e_CurveR_10; }
	inline bool* get_address_of_e_CurveR_10() { return &___e_CurveR_10; }
	inline void set_e_CurveR_10(bool value)
	{
		___e_CurveR_10 = value;
	}

	inline static int32_t get_offset_of_e_CurveG_11() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveG_11)); }
	inline bool get_e_CurveG_11() const { return ___e_CurveG_11; }
	inline bool* get_address_of_e_CurveG_11() { return &___e_CurveG_11; }
	inline void set_e_CurveG_11(bool value)
	{
		___e_CurveG_11 = value;
	}

	inline static int32_t get_offset_of_e_CurveB_12() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveB_12)); }
	inline bool get_e_CurveB_12() const { return ___e_CurveB_12; }
	inline bool* get_address_of_e_CurveB_12() { return &___e_CurveB_12; }
	inline void set_e_CurveB_12(bool value)
	{
		___e_CurveB_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct CurvesSettings_t2830270037_marshaled_pinvoke
{
	ColorGradingCurve_t2000571184 * ___master_0;
	ColorGradingCurve_t2000571184 * ___red_1;
	ColorGradingCurve_t2000571184 * ___green_2;
	ColorGradingCurve_t2000571184 * ___blue_3;
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	int32_t ___e_CurrentEditingCurve_8;
	int32_t ___e_CurveY_9;
	int32_t ___e_CurveR_10;
	int32_t ___e_CurveG_11;
	int32_t ___e_CurveB_12;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct CurvesSettings_t2830270037_marshaled_com
{
	ColorGradingCurve_t2000571184 * ___master_0;
	ColorGradingCurve_t2000571184 * ___red_1;
	ColorGradingCurve_t2000571184 * ___green_2;
	ColorGradingCurve_t2000571184 * ___blue_3;
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	int32_t ___e_CurrentEditingCurve_8;
	int32_t ___e_CurveY_9;
	int32_t ___e_CurveR_10;
	int32_t ___e_CurveG_11;
	int32_t ___e_CurveB_12;
};
#endif // CURVESSETTINGS_T2830270037_H
#ifndef FRAME_T295908221_H
#define FRAME_T295908221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame
struct  Frame_t295908221 
{
public:
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::lumaTexture
	RenderTexture_t2108887433 * ___lumaTexture_0;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::chromaTexture
	RenderTexture_t2108887433 * ___chromaTexture_1;
	// System.Single UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::m_Time
	float ___m_Time_2;
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::m_MRT
	RenderTargetIdentifierU5BU5D_t2742279485* ___m_MRT_3;

public:
	inline static int32_t get_offset_of_lumaTexture_0() { return static_cast<int32_t>(offsetof(Frame_t295908221, ___lumaTexture_0)); }
	inline RenderTexture_t2108887433 * get_lumaTexture_0() const { return ___lumaTexture_0; }
	inline RenderTexture_t2108887433 ** get_address_of_lumaTexture_0() { return &___lumaTexture_0; }
	inline void set_lumaTexture_0(RenderTexture_t2108887433 * value)
	{
		___lumaTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___lumaTexture_0), value);
	}

	inline static int32_t get_offset_of_chromaTexture_1() { return static_cast<int32_t>(offsetof(Frame_t295908221, ___chromaTexture_1)); }
	inline RenderTexture_t2108887433 * get_chromaTexture_1() const { return ___chromaTexture_1; }
	inline RenderTexture_t2108887433 ** get_address_of_chromaTexture_1() { return &___chromaTexture_1; }
	inline void set_chromaTexture_1(RenderTexture_t2108887433 * value)
	{
		___chromaTexture_1 = value;
		Il2CppCodeGenWriteBarrier((&___chromaTexture_1), value);
	}

	inline static int32_t get_offset_of_m_Time_2() { return static_cast<int32_t>(offsetof(Frame_t295908221, ___m_Time_2)); }
	inline float get_m_Time_2() const { return ___m_Time_2; }
	inline float* get_address_of_m_Time_2() { return &___m_Time_2; }
	inline void set_m_Time_2(float value)
	{
		___m_Time_2 = value;
	}

	inline static int32_t get_offset_of_m_MRT_3() { return static_cast<int32_t>(offsetof(Frame_t295908221, ___m_MRT_3)); }
	inline RenderTargetIdentifierU5BU5D_t2742279485* get_m_MRT_3() const { return ___m_MRT_3; }
	inline RenderTargetIdentifierU5BU5D_t2742279485** get_address_of_m_MRT_3() { return &___m_MRT_3; }
	inline void set_m_MRT_3(RenderTargetIdentifierU5BU5D_t2742279485* value)
	{
		___m_MRT_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame
struct Frame_t295908221_marshaled_pinvoke
{
	RenderTexture_t2108887433 * ___lumaTexture_0;
	RenderTexture_t2108887433 * ___chromaTexture_1;
	float ___m_Time_2;
	RenderTargetIdentifier_t2079184500 * ___m_MRT_3;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame
struct Frame_t295908221_marshaled_com
{
	RenderTexture_t2108887433 * ___lumaTexture_0;
	RenderTexture_t2108887433 * ___chromaTexture_1;
	float ___m_Time_2;
	RenderTargetIdentifier_t2079184500 * ___m_MRT_3;
};
#endif // FRAME_T295908221_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3977614564_H
#define POSTPROCESSINGCOMPONENT_1_T3977614564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AmbientOcclusionModel>
struct  PostProcessingComponent_1_t3977614564  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	AmbientOcclusionModel_t389471066 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3977614564, ___U3CmodelU3Ek__BackingField_1)); }
	inline AmbientOcclusionModel_t389471066 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline AmbientOcclusionModel_t389471066 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(AmbientOcclusionModel_t389471066 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3977614564_H
#ifndef POSTPROCESSINGCOMPONENT_1_T814315590_H
#define POSTPROCESSINGCOMPONENT_1_T814315590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AntialiasingModel>
struct  PostProcessingComponent_1_t814315590  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	AntialiasingModel_t1521139388 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t814315590, ___U3CmodelU3Ek__BackingField_1)); }
	inline AntialiasingModel_t1521139388 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline AntialiasingModel_t1521139388 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(AntialiasingModel_t1521139388 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T814315590_H
#ifndef POSTPROCESSINGCOMPONENT_1_T1392904062_H
#define POSTPROCESSINGCOMPONENT_1_T1392904062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.BloomModel>
struct  PostProcessingComponent_1_t1392904062  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	BloomModel_t2099727860 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t1392904062, ___U3CmodelU3Ek__BackingField_1)); }
	inline BloomModel_t2099727860 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline BloomModel_t2099727860 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(BloomModel_t2099727860 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T1392904062_H
#ifndef POSTPROCESSINGCOMPONENT_1_T755795042_H
#define POSTPROCESSINGCOMPONENT_1_T755795042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.BuiltinDebugViewsModel>
struct  PostProcessingComponent_1_t755795042  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	BuiltinDebugViewsModel_t1462618840 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t755795042, ___U3CmodelU3Ek__BackingField_1)); }
	inline BuiltinDebugViewsModel_t1462618840 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline BuiltinDebugViewsModel_t1462618840 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(BuiltinDebugViewsModel_t1462618840 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T755795042_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3256576055_H
#define POSTPROCESSINGCOMPONENT_1_T3256576055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ChromaticAberrationModel>
struct  PostProcessingComponent_1_t3256576055  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	ChromaticAberrationModel_t3963399853 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3256576055, ___U3CmodelU3Ek__BackingField_1)); }
	inline ChromaticAberrationModel_t3963399853 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline ChromaticAberrationModel_t3963399853 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(ChromaticAberrationModel_t3963399853 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3256576055_H
#ifndef POSTPROCESSINGCOMPONENT_1_T741224383_H
#define POSTPROCESSINGCOMPONENT_1_T741224383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ColorGradingModel>
struct  PostProcessingComponent_1_t741224383  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	ColorGradingModel_t1448048181 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t741224383, ___U3CmodelU3Ek__BackingField_1)); }
	inline ColorGradingModel_t1448048181 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline ColorGradingModel_t1448048181 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(ColorGradingModel_t1448048181 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T741224383_H
#ifndef POSTPROCESSINGCOMPONENT_1_T4102210828_H
#define POSTPROCESSINGCOMPONENT_1_T4102210828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.DepthOfFieldModel>
struct  PostProcessingComponent_1_t4102210828  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	DepthOfFieldModel_t514067330 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t4102210828, ___U3CmodelU3Ek__BackingField_1)); }
	inline DepthOfFieldModel_t514067330 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline DepthOfFieldModel_t514067330 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(DepthOfFieldModel_t514067330 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T4102210828_H
#ifndef POSTPROCESSINGCOMPONENT_1_T1722181598_H
#define POSTPROCESSINGCOMPONENT_1_T1722181598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.DitheringModel>
struct  PostProcessingComponent_1_t1722181598  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	DitheringModel_t2429005396 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t1722181598, ___U3CmodelU3Ek__BackingField_1)); }
	inline DitheringModel_t2429005396 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline DitheringModel_t2429005396 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(DitheringModel_t2429005396 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T1722181598_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3830967410_H
#define POSTPROCESSINGCOMPONENT_1_T3830967410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.EyeAdaptationModel>
struct  PostProcessingComponent_1_t3830967410  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	EyeAdaptationModel_t242823912 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3830967410, ___U3CmodelU3Ek__BackingField_1)); }
	inline EyeAdaptationModel_t242823912 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline EyeAdaptationModel_t242823912 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(EyeAdaptationModel_t242823912 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3830967410_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2913864951_H
#define POSTPROCESSINGCOMPONENT_1_T2913864951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.FogModel>
struct  PostProcessingComponent_1_t2913864951  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	FogModel_t3620688749 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2913864951, ___U3CmodelU3Ek__BackingField_1)); }
	inline FogModel_t3620688749 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline FogModel_t3620688749 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(FogModel_t3620688749 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2913864951_H
#ifndef POSTPROCESSINGCOMPONENT_1_T446058690_H
#define POSTPROCESSINGCOMPONENT_1_T446058690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.GrainModel>
struct  PostProcessingComponent_1_t446058690  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	GrainModel_t1152882488 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t446058690, ___U3CmodelU3Ek__BackingField_1)); }
	inline GrainModel_t1152882488 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline GrainModel_t1152882488 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(GrainModel_t1152882488 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T446058690_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2373462325_H
#define POSTPROCESSINGCOMPONENT_1_T2373462325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.MotionBlurModel>
struct  PostProcessingComponent_1_t2373462325  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	MotionBlurModel_t3080286123 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2373462325, ___U3CmodelU3Ek__BackingField_1)); }
	inline MotionBlurModel_t3080286123 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline MotionBlurModel_t3080286123 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(MotionBlurModel_t3080286123 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2373462325_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2319520934_H
#define POSTPROCESSINGCOMPONENT_1_T2319520934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>
struct  PostProcessingComponent_1_t2319520934  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	ScreenSpaceReflectionModel_t3026344732 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2319520934, ___U3CmodelU3Ek__BackingField_1)); }
	inline ScreenSpaceReflectionModel_t3026344732 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline ScreenSpaceReflectionModel_t3026344732 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(ScreenSpaceReflectionModel_t3026344732 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2319520934_H
#ifndef POSTPROCESSINGCOMPONENT_1_T963284282_H
#define POSTPROCESSINGCOMPONENT_1_T963284282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.UserLutModel>
struct  PostProcessingComponent_1_t963284282  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	UserLutModel_t1670108080 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t963284282, ___U3CmodelU3Ek__BackingField_1)); }
	inline UserLutModel_t1670108080 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline UserLutModel_t1670108080 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(UserLutModel_t1670108080 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T963284282_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2138693379_H
#define POSTPROCESSINGCOMPONENT_1_T2138693379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.VignetteModel>
struct  PostProcessingComponent_1_t2138693379  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	VignetteModel_t2845517177 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2138693379, ___U3CmodelU3Ek__BackingField_1)); }
	inline VignetteModel_t2845517177 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline VignetteModel_t2845517177 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(VignetteModel_t2845517177 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2138693379_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ROTATIONAXES_T1559384644_H
#define ROTATIONAXES_T1559384644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.RainMaker.DemoScript/RotationAxes
struct  RotationAxes_t1559384644 
{
public:
	// System.Int32 DigitalRuby.RainMaker.DemoScript/RotationAxes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotationAxes_t1559384644, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONAXES_T1559384644_H
#ifndef U3C_KNOCKBACKU3ED__64_T3256490646_H
#define U3C_KNOCKBACKU3ED__64_T3256490646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGCharacterMovementController/<_Knockback>d__64
struct  U3C_KnockbackU3Ed__64_t3256490646  : public RuntimeObject
{
public:
	// System.Int32 RPGCharacterAnims.RPGCharacterMovementController/<_Knockback>d__64::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RPGCharacterAnims.RPGCharacterMovementController/<_Knockback>d__64::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RPGCharacterAnims.RPGCharacterMovementController RPGCharacterAnims.RPGCharacterMovementController/<_Knockback>d__64::<>4__this
	RPGCharacterMovementController_t3656691424 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 RPGCharacterAnims.RPGCharacterMovementController/<_Knockback>d__64::knockDirection
	Vector3_t3722313464  ___knockDirection_3;
	// System.Int32 RPGCharacterAnims.RPGCharacterMovementController/<_Knockback>d__64::knockBackAmount
	int32_t ___knockBackAmount_4;
	// System.Int32 RPGCharacterAnims.RPGCharacterMovementController/<_Knockback>d__64::variableAmount
	int32_t ___variableAmount_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ed__64_t3256490646, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ed__64_t3256490646, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ed__64_t3256490646, ___U3CU3E4__this_2)); }
	inline RPGCharacterMovementController_t3656691424 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RPGCharacterMovementController_t3656691424 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RPGCharacterMovementController_t3656691424 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_knockDirection_3() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ed__64_t3256490646, ___knockDirection_3)); }
	inline Vector3_t3722313464  get_knockDirection_3() const { return ___knockDirection_3; }
	inline Vector3_t3722313464 * get_address_of_knockDirection_3() { return &___knockDirection_3; }
	inline void set_knockDirection_3(Vector3_t3722313464  value)
	{
		___knockDirection_3 = value;
	}

	inline static int32_t get_offset_of_knockBackAmount_4() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ed__64_t3256490646, ___knockBackAmount_4)); }
	inline int32_t get_knockBackAmount_4() const { return ___knockBackAmount_4; }
	inline int32_t* get_address_of_knockBackAmount_4() { return &___knockBackAmount_4; }
	inline void set_knockBackAmount_4(int32_t value)
	{
		___knockBackAmount_4 = value;
	}

	inline static int32_t get_offset_of_variableAmount_5() { return static_cast<int32_t>(offsetof(U3C_KnockbackU3Ed__64_t3256490646, ___variableAmount_5)); }
	inline int32_t get_variableAmount_5() const { return ___variableAmount_5; }
	inline int32_t* get_address_of_variableAmount_5() { return &___variableAmount_5; }
	inline void set_variableAmount_5(int32_t value)
	{
		___variableAmount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_KNOCKBACKU3ED__64_T3256490646_H
#ifndef U3C_KNOCKBACKFORCEU3ED__65_T780478027_H
#define U3C_KNOCKBACKFORCEU3ED__65_T780478027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGCharacterMovementController/<_KnockbackForce>d__65
struct  U3C_KnockbackForceU3Ed__65_t780478027  : public RuntimeObject
{
public:
	// System.Int32 RPGCharacterAnims.RPGCharacterMovementController/<_KnockbackForce>d__65::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RPGCharacterAnims.RPGCharacterMovementController/<_KnockbackForce>d__65::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RPGCharacterAnims.RPGCharacterMovementController RPGCharacterAnims.RPGCharacterMovementController/<_KnockbackForce>d__65::<>4__this
	RPGCharacterMovementController_t3656691424 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 RPGCharacterAnims.RPGCharacterMovementController/<_KnockbackForce>d__65::knockDirection
	Vector3_t3722313464  ___knockDirection_3;
	// System.Int32 RPGCharacterAnims.RPGCharacterMovementController/<_KnockbackForce>d__65::knockBackAmount
	int32_t ___knockBackAmount_4;
	// System.Int32 RPGCharacterAnims.RPGCharacterMovementController/<_KnockbackForce>d__65::variableAmount
	int32_t ___variableAmount_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ed__65_t780478027, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ed__65_t780478027, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ed__65_t780478027, ___U3CU3E4__this_2)); }
	inline RPGCharacterMovementController_t3656691424 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RPGCharacterMovementController_t3656691424 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RPGCharacterMovementController_t3656691424 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_knockDirection_3() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ed__65_t780478027, ___knockDirection_3)); }
	inline Vector3_t3722313464  get_knockDirection_3() const { return ___knockDirection_3; }
	inline Vector3_t3722313464 * get_address_of_knockDirection_3() { return &___knockDirection_3; }
	inline void set_knockDirection_3(Vector3_t3722313464  value)
	{
		___knockDirection_3 = value;
	}

	inline static int32_t get_offset_of_knockBackAmount_4() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ed__65_t780478027, ___knockBackAmount_4)); }
	inline int32_t get_knockBackAmount_4() const { return ___knockBackAmount_4; }
	inline int32_t* get_address_of_knockBackAmount_4() { return &___knockBackAmount_4; }
	inline void set_knockBackAmount_4(int32_t value)
	{
		___knockBackAmount_4 = value;
	}

	inline static int32_t get_offset_of_variableAmount_5() { return static_cast<int32_t>(offsetof(U3C_KnockbackForceU3Ed__65_t780478027, ___variableAmount_5)); }
	inline int32_t get_variableAmount_5() const { return ___variableAmount_5; }
	inline int32_t* get_address_of_variableAmount_5() { return &___variableAmount_5; }
	inline void set_variableAmount_5(int32_t value)
	{
		___variableAmount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_KNOCKBACKFORCEU3ED__65_T780478027_H
#ifndef RPGCHARACTERSTATE_T1431373774_H
#define RPGCHARACTERSTATE_T1431373774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGCharacterState
struct  RPGCharacterState_t1431373774 
{
public:
	// System.Int32 RPGCharacterAnims.RPGCharacterState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RPGCharacterState_t1431373774, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPGCHARACTERSTATE_T1431373774_H
#ifndef RPGINPUT_T667815195_H
#define RPGINPUT_T667815195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGInput
struct  RPGInput_t667815195 
{
public:
	// UnityEngine.Vector3 RPGCharacterAnims.RPGInput::moveInput
	Vector3_t3722313464  ___moveInput_0;
	// UnityEngine.Vector2 RPGCharacterAnims.RPGInput::aimInput
	Vector2_t2156229523  ___aimInput_1;
	// System.Boolean RPGCharacterAnims.RPGInput::jumpInput
	bool ___jumpInput_2;

public:
	inline static int32_t get_offset_of_moveInput_0() { return static_cast<int32_t>(offsetof(RPGInput_t667815195, ___moveInput_0)); }
	inline Vector3_t3722313464  get_moveInput_0() const { return ___moveInput_0; }
	inline Vector3_t3722313464 * get_address_of_moveInput_0() { return &___moveInput_0; }
	inline void set_moveInput_0(Vector3_t3722313464  value)
	{
		___moveInput_0 = value;
	}

	inline static int32_t get_offset_of_aimInput_1() { return static_cast<int32_t>(offsetof(RPGInput_t667815195, ___aimInput_1)); }
	inline Vector2_t2156229523  get_aimInput_1() const { return ___aimInput_1; }
	inline Vector2_t2156229523 * get_address_of_aimInput_1() { return &___aimInput_1; }
	inline void set_aimInput_1(Vector2_t2156229523  value)
	{
		___aimInput_1 = value;
	}

	inline static int32_t get_offset_of_jumpInput_2() { return static_cast<int32_t>(offsetof(RPGInput_t667815195, ___jumpInput_2)); }
	inline bool get_jumpInput_2() const { return ___jumpInput_2; }
	inline bool* get_address_of_jumpInput_2() { return &___jumpInput_2; }
	inline void set_jumpInput_2(bool value)
	{
		___jumpInput_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of RPGCharacterAnims.RPGInput
struct RPGInput_t667815195_marshaled_pinvoke
{
	Vector3_t3722313464  ___moveInput_0;
	Vector2_t2156229523  ___aimInput_1;
	int32_t ___jumpInput_2;
};
// Native definition for COM marshalling of RPGCharacterAnims.RPGInput
struct RPGInput_t667815195_marshaled_com
{
	Vector3_t3722313464  ___moveInput_0;
	Vector2_t2156229523  ___aimInput_1;
	int32_t ___jumpInput_2;
};
#endif // RPGINPUT_T667815195_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef OCCLUSIONSOURCE_T4221238007_H
#define OCCLUSIONSOURCE_T4221238007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent/OcclusionSource
struct  OcclusionSource_t4221238007 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/OcclusionSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OcclusionSource_t4221238007, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCCLUSIONSOURCE_T4221238007_H
#ifndef SAMPLECOUNT_T1158000259_H
#define SAMPLECOUNT_T1158000259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionModel/SampleCount
struct  SampleCount_t1158000259 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionModel/SampleCount::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SampleCount_t1158000259, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECOUNT_T1158000259_H
#ifndef FXAAPRESET_T2149486832_H
#define FXAAPRESET_T2149486832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/FxaaPreset
struct  FxaaPreset_t2149486832 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AntialiasingModel/FxaaPreset::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FxaaPreset_t2149486832, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAAPRESET_T2149486832_H
#ifndef METHOD_T287042102_H
#define METHOD_T287042102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/Method
struct  Method_t287042102 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AntialiasingModel/Method::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Method_t287042102, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHOD_T287042102_H
#ifndef SETTINGS_T181254429_H
#define SETTINGS_T181254429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomModel/Settings
struct  Settings_t181254429 
{
public:
	// UnityEngine.PostProcessing.BloomModel/BloomSettings UnityEngine.PostProcessing.BloomModel/Settings::bloom
	BloomSettings_t2599855122  ___bloom_0;
	// UnityEngine.PostProcessing.BloomModel/LensDirtSettings UnityEngine.PostProcessing.BloomModel/Settings::lensDirt
	LensDirtSettings_t3693422705  ___lensDirt_1;

public:
	inline static int32_t get_offset_of_bloom_0() { return static_cast<int32_t>(offsetof(Settings_t181254429, ___bloom_0)); }
	inline BloomSettings_t2599855122  get_bloom_0() const { return ___bloom_0; }
	inline BloomSettings_t2599855122 * get_address_of_bloom_0() { return &___bloom_0; }
	inline void set_bloom_0(BloomSettings_t2599855122  value)
	{
		___bloom_0 = value;
	}

	inline static int32_t get_offset_of_lensDirt_1() { return static_cast<int32_t>(offsetof(Settings_t181254429, ___lensDirt_1)); }
	inline LensDirtSettings_t3693422705  get_lensDirt_1() const { return ___lensDirt_1; }
	inline LensDirtSettings_t3693422705 * get_address_of_lensDirt_1() { return &___lensDirt_1; }
	inline void set_lensDirt_1(LensDirtSettings_t3693422705  value)
	{
		___lensDirt_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.BloomModel/Settings
struct Settings_t181254429_marshaled_pinvoke
{
	BloomSettings_t2599855122_marshaled_pinvoke ___bloom_0;
	LensDirtSettings_t3693422705_marshaled_pinvoke ___lensDirt_1;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.BloomModel/Settings
struct Settings_t181254429_marshaled_com
{
	BloomSettings_t2599855122_marshaled_com ___bloom_0;
	LensDirtSettings_t3693422705_marshaled_com ___lensDirt_1;
};
#endif // SETTINGS_T181254429_H
#ifndef PASS_T2117482_H
#define PASS_T2117482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Pass
struct  Pass_t2117482 
{
public:
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Pass::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pass_t2117482, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T2117482_H
#ifndef MODE_T2695902415_H
#define MODE_T2695902415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsModel/Mode
struct  Mode_t2695902415 
{
public:
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsModel/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t2695902415, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T2695902415_H
#ifndef CHROMATICABERRATIONMODEL_T3963399853_H
#define CHROMATICABERRATIONMODEL_T3963399853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ChromaticAberrationModel
struct  ChromaticAberrationModel_t3963399853  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.ChromaticAberrationModel/Settings UnityEngine.PostProcessing.ChromaticAberrationModel::m_Settings
	Settings_t2111398455  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(ChromaticAberrationModel_t3963399853, ___m_Settings_1)); }
	inline Settings_t2111398455  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t2111398455 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t2111398455  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATIONMODEL_T3963399853_H
#ifndef CHANNELMIXERSETTINGS_T898701698_H
#define CHANNELMIXERSETTINGS_T898701698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings
struct  ChannelMixerSettings_t898701698 
{
public:
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::red
	Vector3_t3722313464  ___red_0;
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::green
	Vector3_t3722313464  ___green_1;
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::blue
	Vector3_t3722313464  ___blue_2;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::currentEditingChannel
	int32_t ___currentEditingChannel_3;

public:
	inline static int32_t get_offset_of_red_0() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___red_0)); }
	inline Vector3_t3722313464  get_red_0() const { return ___red_0; }
	inline Vector3_t3722313464 * get_address_of_red_0() { return &___red_0; }
	inline void set_red_0(Vector3_t3722313464  value)
	{
		___red_0 = value;
	}

	inline static int32_t get_offset_of_green_1() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___green_1)); }
	inline Vector3_t3722313464  get_green_1() const { return ___green_1; }
	inline Vector3_t3722313464 * get_address_of_green_1() { return &___green_1; }
	inline void set_green_1(Vector3_t3722313464  value)
	{
		___green_1 = value;
	}

	inline static int32_t get_offset_of_blue_2() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___blue_2)); }
	inline Vector3_t3722313464  get_blue_2() const { return ___blue_2; }
	inline Vector3_t3722313464 * get_address_of_blue_2() { return &___blue_2; }
	inline void set_blue_2(Vector3_t3722313464  value)
	{
		___blue_2 = value;
	}

	inline static int32_t get_offset_of_currentEditingChannel_3() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___currentEditingChannel_3)); }
	inline int32_t get_currentEditingChannel_3() const { return ___currentEditingChannel_3; }
	inline int32_t* get_address_of_currentEditingChannel_3() { return &___currentEditingChannel_3; }
	inline void set_currentEditingChannel_3(int32_t value)
	{
		___currentEditingChannel_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELMIXERSETTINGS_T898701698_H
#ifndef COLORWHEELMODE_T1939415375_H
#define COLORWHEELMODE_T1939415375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode
struct  ColorWheelMode_t1939415375 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorWheelMode_t1939415375, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWHEELMODE_T1939415375_H
#ifndef LINEARWHEELSSETTINGS_T3897781309_H
#define LINEARWHEELSSETTINGS_T3897781309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings
struct  LinearWheelsSettings_t3897781309 
{
public:
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::lift
	Color_t2555686324  ___lift_0;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::gamma
	Color_t2555686324  ___gamma_1;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::gain
	Color_t2555686324  ___gain_2;

public:
	inline static int32_t get_offset_of_lift_0() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___lift_0)); }
	inline Color_t2555686324  get_lift_0() const { return ___lift_0; }
	inline Color_t2555686324 * get_address_of_lift_0() { return &___lift_0; }
	inline void set_lift_0(Color_t2555686324  value)
	{
		___lift_0 = value;
	}

	inline static int32_t get_offset_of_gamma_1() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___gamma_1)); }
	inline Color_t2555686324  get_gamma_1() const { return ___gamma_1; }
	inline Color_t2555686324 * get_address_of_gamma_1() { return &___gamma_1; }
	inline void set_gamma_1(Color_t2555686324  value)
	{
		___gamma_1 = value;
	}

	inline static int32_t get_offset_of_gain_2() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___gain_2)); }
	inline Color_t2555686324  get_gain_2() const { return ___gain_2; }
	inline Color_t2555686324 * get_address_of_gain_2() { return &___gain_2; }
	inline void set_gain_2(Color_t2555686324  value)
	{
		___gain_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEARWHEELSSETTINGS_T3897781309_H
#ifndef LOGWHEELSSETTINGS_T1545220311_H
#define LOGWHEELSSETTINGS_T1545220311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings
struct  LogWheelsSettings_t1545220311 
{
public:
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::slope
	Color_t2555686324  ___slope_0;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::power
	Color_t2555686324  ___power_1;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::offset
	Color_t2555686324  ___offset_2;

public:
	inline static int32_t get_offset_of_slope_0() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___slope_0)); }
	inline Color_t2555686324  get_slope_0() const { return ___slope_0; }
	inline Color_t2555686324 * get_address_of_slope_0() { return &___slope_0; }
	inline void set_slope_0(Color_t2555686324  value)
	{
		___slope_0 = value;
	}

	inline static int32_t get_offset_of_power_1() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___power_1)); }
	inline Color_t2555686324  get_power_1() const { return ___power_1; }
	inline Color_t2555686324 * get_address_of_power_1() { return &___power_1; }
	inline void set_power_1(Color_t2555686324  value)
	{
		___power_1 = value;
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___offset_2)); }
	inline Color_t2555686324  get_offset_2() const { return ___offset_2; }
	inline Color_t2555686324 * get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(Color_t2555686324  value)
	{
		___offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGWHEELSSETTINGS_T1545220311_H
#ifndef TONEMAPPER_T1404353651_H
#define TONEMAPPER_T1404353651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/Tonemapper
struct  Tonemapper_t1404353651 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/Tonemapper::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Tonemapper_t1404353651, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPER_T1404353651_H
#ifndef GETSETATTRIBUTE_T1349027187_H
#define GETSETATTRIBUTE_T1349027187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GetSetAttribute
struct  GetSetAttribute_t1349027187  : public PropertyAttribute_t3677895545
{
public:
	// System.String UnityEngine.PostProcessing.GetSetAttribute::name
	String_t* ___name_0;
	// System.Boolean UnityEngine.PostProcessing.GetSetAttribute::dirty
	bool ___dirty_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GetSetAttribute_t1349027187, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_dirty_1() { return static_cast<int32_t>(offsetof(GetSetAttribute_t1349027187, ___dirty_1)); }
	inline bool get_dirty_1() const { return ___dirty_1; }
	inline bool* get_address_of_dirty_1() { return &___dirty_1; }
	inline void set_dirty_1(bool value)
	{
		___dirty_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSETATTRIBUTE_T1349027187_H
#ifndef MINATTRIBUTE_T4172004135_H
#define MINATTRIBUTE_T4172004135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MinAttribute
struct  MinAttribute_t4172004135  : public PropertyAttribute_t3677895545
{
public:
	// System.Single UnityEngine.PostProcessing.MinAttribute::min
	float ___min_0;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinAttribute_t4172004135, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINATTRIBUTE_T4172004135_H
#ifndef PASS_T2620402414_H
#define PASS_T2620402414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent/Pass
struct  Pass_t2620402414 
{
public:
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/Pass::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Pass_t2620402414, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T2620402414_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.AmbientOcclusionModel>
struct  PostProcessingComponentCommandBuffer_1_t1664772955  : public PostProcessingComponent_1_t3977614564
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.BuiltinDebugViewsModel>
struct  PostProcessingComponentCommandBuffer_1_t2737920729  : public PostProcessingComponent_1_t755795042
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.FogModel>
struct  PostProcessingComponentCommandBuffer_1_t601023342  : public PostProcessingComponent_1_t2913864951
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T60620716_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T60620716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.MotionBlurModel>
struct  PostProcessingComponentCommandBuffer_1_t60620716  : public PostProcessingComponent_1_t2373462325
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T60620716_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T6679325_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T6679325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.ScreenSpaceReflectionModel>
struct  PostProcessingComponentCommandBuffer_1_t6679325  : public PostProcessingComponent_1_t2319520934
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T6679325_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.AntialiasingModel>
struct  PostProcessingComponentRenderTexture_1_t3089424429  : public PostProcessingComponent_1_t814315590
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.BloomModel>
struct  PostProcessingComponentRenderTexture_1_t3668012901  : public PostProcessingComponent_1_t1392904062
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.ChromaticAberrationModel>
struct  PostProcessingComponentRenderTexture_1_t1236717598  : public PostProcessingComponent_1_t3256576055
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.ColorGradingModel>
struct  PostProcessingComponentRenderTexture_1_t3016333222  : public PostProcessingComponent_1_t741224383
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.DepthOfFieldModel>
struct  PostProcessingComponentRenderTexture_1_t2082352371  : public PostProcessingComponent_1_t4102210828
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.DitheringModel>
struct  PostProcessingComponentRenderTexture_1_t3997290437  : public PostProcessingComponent_1_t1722181598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.EyeAdaptationModel>
struct  PostProcessingComponentRenderTexture_1_t1811108953  : public PostProcessingComponent_1_t3830967410
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.GrainModel>
struct  PostProcessingComponentRenderTexture_1_t2721167529  : public PostProcessingComponent_1_t446058690
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3238393121_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3238393121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.UserLutModel>
struct  PostProcessingComponentRenderTexture_1_t3238393121  : public PostProcessingComponent_1_t963284282
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3238393121_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T118834922_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T118834922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.VignetteModel>
struct  PostProcessingComponentRenderTexture_1_t118834922  : public PostProcessingComponent_1_t2138693379
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T118834922_H
#ifndef PASSINDEX_T1642913883_H
#define PASSINDEX_T1642913883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/PassIndex
struct  PassIndex_t1642913883 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/PassIndex::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PassIndex_t1642913883, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSINDEX_T1642913883_H
#ifndef TRACKBALLATTRIBUTE_T219960417_H
#define TRACKBALLATTRIBUTE_T219960417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TrackballAttribute
struct  TrackballAttribute_t219960417  : public PropertyAttribute_t3677895545
{
public:
	// System.String UnityEngine.PostProcessing.TrackballAttribute::method
	String_t* ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(TrackballAttribute_t219960417, ___method_0)); }
	inline String_t* get_method_0() const { return ___method_0; }
	inline String_t** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(String_t* value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBALLATTRIBUTE_T219960417_H
#ifndef TRACKBALLGROUPATTRIBUTE_T624107828_H
#define TRACKBALLGROUPATTRIBUTE_T624107828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TrackballGroupAttribute
struct  TrackballGroupAttribute_t624107828  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBALLGROUPATTRIBUTE_T624107828_H
#ifndef RENDERTEXTUREFORMAT_T962350765_H
#define RENDERTEXTUREFORMAT_T962350765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t962350765 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t962350765, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T962350765_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#define AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent
struct  AmbientOcclusionComponent_t4130625043  : public PostProcessingComponentCommandBuffer_1_t1664772955
{
public:
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.PostProcessing.AmbientOcclusionComponent::m_MRT
	RenderTargetIdentifierU5BU5D_t2742279485* ___m_MRT_4;

public:
	inline static int32_t get_offset_of_m_MRT_4() { return static_cast<int32_t>(offsetof(AmbientOcclusionComponent_t4130625043, ___m_MRT_4)); }
	inline RenderTargetIdentifierU5BU5D_t2742279485* get_m_MRT_4() const { return ___m_MRT_4; }
	inline RenderTargetIdentifierU5BU5D_t2742279485** get_address_of_m_MRT_4() { return &___m_MRT_4; }
	inline void set_m_MRT_4(RenderTargetIdentifierU5BU5D_t2742279485* value)
	{
		___m_MRT_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#ifndef SETTINGS_T3016786575_H
#define SETTINGS_T3016786575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionModel/Settings
struct  Settings_t3016786575 
{
public:
	// System.Single UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::intensity
	float ___intensity_0;
	// System.Single UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::radius
	float ___radius_1;
	// UnityEngine.PostProcessing.AmbientOcclusionModel/SampleCount UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::sampleCount
	int32_t ___sampleCount_2;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::downsampling
	bool ___downsampling_3;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::forceForwardCompatibility
	bool ___forceForwardCompatibility_4;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::ambientOnly
	bool ___ambientOnly_5;
	// System.Boolean UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::highPrecision
	bool ___highPrecision_6;

public:
	inline static int32_t get_offset_of_intensity_0() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___intensity_0)); }
	inline float get_intensity_0() const { return ___intensity_0; }
	inline float* get_address_of_intensity_0() { return &___intensity_0; }
	inline void set_intensity_0(float value)
	{
		___intensity_0 = value;
	}

	inline static int32_t get_offset_of_radius_1() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___radius_1)); }
	inline float get_radius_1() const { return ___radius_1; }
	inline float* get_address_of_radius_1() { return &___radius_1; }
	inline void set_radius_1(float value)
	{
		___radius_1 = value;
	}

	inline static int32_t get_offset_of_sampleCount_2() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___sampleCount_2)); }
	inline int32_t get_sampleCount_2() const { return ___sampleCount_2; }
	inline int32_t* get_address_of_sampleCount_2() { return &___sampleCount_2; }
	inline void set_sampleCount_2(int32_t value)
	{
		___sampleCount_2 = value;
	}

	inline static int32_t get_offset_of_downsampling_3() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___downsampling_3)); }
	inline bool get_downsampling_3() const { return ___downsampling_3; }
	inline bool* get_address_of_downsampling_3() { return &___downsampling_3; }
	inline void set_downsampling_3(bool value)
	{
		___downsampling_3 = value;
	}

	inline static int32_t get_offset_of_forceForwardCompatibility_4() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___forceForwardCompatibility_4)); }
	inline bool get_forceForwardCompatibility_4() const { return ___forceForwardCompatibility_4; }
	inline bool* get_address_of_forceForwardCompatibility_4() { return &___forceForwardCompatibility_4; }
	inline void set_forceForwardCompatibility_4(bool value)
	{
		___forceForwardCompatibility_4 = value;
	}

	inline static int32_t get_offset_of_ambientOnly_5() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___ambientOnly_5)); }
	inline bool get_ambientOnly_5() const { return ___ambientOnly_5; }
	inline bool* get_address_of_ambientOnly_5() { return &___ambientOnly_5; }
	inline void set_ambientOnly_5(bool value)
	{
		___ambientOnly_5 = value;
	}

	inline static int32_t get_offset_of_highPrecision_6() { return static_cast<int32_t>(offsetof(Settings_t3016786575, ___highPrecision_6)); }
	inline bool get_highPrecision_6() const { return ___highPrecision_6; }
	inline bool* get_address_of_highPrecision_6() { return &___highPrecision_6; }
	inline void set_highPrecision_6(bool value)
	{
		___highPrecision_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.AmbientOcclusionModel/Settings
struct Settings_t3016786575_marshaled_pinvoke
{
	float ___intensity_0;
	float ___radius_1;
	int32_t ___sampleCount_2;
	int32_t ___downsampling_3;
	int32_t ___forceForwardCompatibility_4;
	int32_t ___ambientOnly_5;
	int32_t ___highPrecision_6;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.AmbientOcclusionModel/Settings
struct Settings_t3016786575_marshaled_com
{
	float ___intensity_0;
	float ___radius_1;
	int32_t ___sampleCount_2;
	int32_t ___downsampling_3;
	int32_t ___forceForwardCompatibility_4;
	int32_t ___ambientOnly_5;
	int32_t ___highPrecision_6;
};
#endif // SETTINGS_T3016786575_H
#ifndef FXAASETTINGS_T1280675075_H
#define FXAASETTINGS_T1280675075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/FxaaSettings
struct  FxaaSettings_t1280675075 
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/FxaaPreset UnityEngine.PostProcessing.AntialiasingModel/FxaaSettings::preset
	int32_t ___preset_0;

public:
	inline static int32_t get_offset_of_preset_0() { return static_cast<int32_t>(offsetof(FxaaSettings_t1280675075, ___preset_0)); }
	inline int32_t get_preset_0() const { return ___preset_0; }
	inline int32_t* get_address_of_preset_0() { return &___preset_0; }
	inline void set_preset_0(int32_t value)
	{
		___preset_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAASETTINGS_T1280675075_H
#ifndef BLOOMCOMPONENT_T3791419130_H
#define BLOOMCOMPONENT_T3791419130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomComponent
struct  BloomComponent_t3791419130  : public PostProcessingComponentRenderTexture_1_t3668012901
{
public:
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.BloomComponent::m_BlurBuffer1
	RenderTextureU5BU5D_t4111643188* ___m_BlurBuffer1_3;
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.BloomComponent::m_BlurBuffer2
	RenderTextureU5BU5D_t4111643188* ___m_BlurBuffer2_4;

public:
	inline static int32_t get_offset_of_m_BlurBuffer1_3() { return static_cast<int32_t>(offsetof(BloomComponent_t3791419130, ___m_BlurBuffer1_3)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_BlurBuffer1_3() const { return ___m_BlurBuffer1_3; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_BlurBuffer1_3() { return &___m_BlurBuffer1_3; }
	inline void set_m_BlurBuffer1_3(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_BlurBuffer1_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlurBuffer1_3), value);
	}

	inline static int32_t get_offset_of_m_BlurBuffer2_4() { return static_cast<int32_t>(offsetof(BloomComponent_t3791419130, ___m_BlurBuffer2_4)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_BlurBuffer2_4() const { return ___m_BlurBuffer2_4; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_BlurBuffer2_4() { return &___m_BlurBuffer2_4; }
	inline void set_m_BlurBuffer2_4(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_BlurBuffer2_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlurBuffer2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMCOMPONENT_T3791419130_H
#ifndef BLOOMMODEL_T2099727860_H
#define BLOOMMODEL_T2099727860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomModel
struct  BloomModel_t2099727860  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.BloomModel/Settings UnityEngine.PostProcessing.BloomModel::m_Settings
	Settings_t181254429  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(BloomModel_t2099727860, ___m_Settings_1)); }
	inline Settings_t181254429  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t181254429 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t181254429  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMMODEL_T2099727860_H
#ifndef BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#define BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent
struct  BuiltinDebugViewsComponent_t2123147871  : public PostProcessingComponentCommandBuffer_1_t2737920729
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray UnityEngine.PostProcessing.BuiltinDebugViewsComponent::m_Arrows
	ArrowArray_t303178545 * ___m_Arrows_3;

public:
	inline static int32_t get_offset_of_m_Arrows_3() { return static_cast<int32_t>(offsetof(BuiltinDebugViewsComponent_t2123147871, ___m_Arrows_3)); }
	inline ArrowArray_t303178545 * get_m_Arrows_3() const { return ___m_Arrows_3; }
	inline ArrowArray_t303178545 ** get_address_of_m_Arrows_3() { return &___m_Arrows_3; }
	inline void set_m_Arrows_3(ArrowArray_t303178545 * value)
	{
		___m_Arrows_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arrows_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#ifndef SETTINGS_T3984509665_H
#define SETTINGS_T3984509665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings
struct  Settings_t3984509665 
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel/Mode UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings::mode
	int32_t ___mode_0;
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel/DepthSettings UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings::depth
	DepthSettings_t1820272864  ___depth_1;
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings::motionVectors
	MotionVectorsSettings_t3857813598  ___motionVectors_2;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(Settings_t3984509665, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_depth_1() { return static_cast<int32_t>(offsetof(Settings_t3984509665, ___depth_1)); }
	inline DepthSettings_t1820272864  get_depth_1() const { return ___depth_1; }
	inline DepthSettings_t1820272864 * get_address_of_depth_1() { return &___depth_1; }
	inline void set_depth_1(DepthSettings_t1820272864  value)
	{
		___depth_1 = value;
	}

	inline static int32_t get_offset_of_motionVectors_2() { return static_cast<int32_t>(offsetof(Settings_t3984509665, ___motionVectors_2)); }
	inline MotionVectorsSettings_t3857813598  get_motionVectors_2() const { return ___motionVectors_2; }
	inline MotionVectorsSettings_t3857813598 * get_address_of_motionVectors_2() { return &___motionVectors_2; }
	inline void set_motionVectors_2(MotionVectorsSettings_t3857813598  value)
	{
		___motionVectors_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T3984509665_H
#ifndef CHROMATICABERRATIONCOMPONENT_T1647263118_H
#define CHROMATICABERRATIONCOMPONENT_T1647263118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ChromaticAberrationComponent
struct  ChromaticAberrationComponent_t1647263118  : public PostProcessingComponentRenderTexture_1_t1236717598
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.ChromaticAberrationComponent::m_SpectrumLut
	Texture2D_t3840446185 * ___m_SpectrumLut_2;

public:
	inline static int32_t get_offset_of_m_SpectrumLut_2() { return static_cast<int32_t>(offsetof(ChromaticAberrationComponent_t1647263118, ___m_SpectrumLut_2)); }
	inline Texture2D_t3840446185 * get_m_SpectrumLut_2() const { return ___m_SpectrumLut_2; }
	inline Texture2D_t3840446185 ** get_address_of_m_SpectrumLut_2() { return &___m_SpectrumLut_2; }
	inline void set_m_SpectrumLut_2(Texture2D_t3840446185 * value)
	{
		___m_SpectrumLut_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpectrumLut_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATIONCOMPONENT_T1647263118_H
#ifndef COLORGRADINGCOMPONENT_T1715259467_H
#define COLORGRADINGCOMPONENT_T1715259467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingComponent
struct  ColorGradingComponent_t1715259467  : public PostProcessingComponentRenderTexture_1_t3016333222
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.ColorGradingComponent::m_GradingCurves
	Texture2D_t3840446185 * ___m_GradingCurves_5;
	// UnityEngine.Color[] UnityEngine.PostProcessing.ColorGradingComponent::m_pixels
	ColorU5BU5D_t941916413* ___m_pixels_6;

public:
	inline static int32_t get_offset_of_m_GradingCurves_5() { return static_cast<int32_t>(offsetof(ColorGradingComponent_t1715259467, ___m_GradingCurves_5)); }
	inline Texture2D_t3840446185 * get_m_GradingCurves_5() const { return ___m_GradingCurves_5; }
	inline Texture2D_t3840446185 ** get_address_of_m_GradingCurves_5() { return &___m_GradingCurves_5; }
	inline void set_m_GradingCurves_5(Texture2D_t3840446185 * value)
	{
		___m_GradingCurves_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradingCurves_5), value);
	}

	inline static int32_t get_offset_of_m_pixels_6() { return static_cast<int32_t>(offsetof(ColorGradingComponent_t1715259467, ___m_pixels_6)); }
	inline ColorU5BU5D_t941916413* get_m_pixels_6() const { return ___m_pixels_6; }
	inline ColorU5BU5D_t941916413** get_address_of_m_pixels_6() { return &___m_pixels_6; }
	inline void set_m_pixels_6(ColorU5BU5D_t941916413* value)
	{
		___m_pixels_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_pixels_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGCOMPONENT_T1715259467_H
#ifndef COLORWHEELSSETTINGS_T3120867866_H
#define COLORWHEELSSETTINGS_T3120867866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings
struct  ColorWheelsSettings_t3120867866 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::mode
	int32_t ___mode_0;
	// UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::log
	LogWheelsSettings_t1545220311  ___log_1;
	// UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::linear
	LinearWheelsSettings_t3897781309  ___linear_2;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_log_1() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___log_1)); }
	inline LogWheelsSettings_t1545220311  get_log_1() const { return ___log_1; }
	inline LogWheelsSettings_t1545220311 * get_address_of_log_1() { return &___log_1; }
	inline void set_log_1(LogWheelsSettings_t1545220311  value)
	{
		___log_1 = value;
	}

	inline static int32_t get_offset_of_linear_2() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___linear_2)); }
	inline LinearWheelsSettings_t3897781309  get_linear_2() const { return ___linear_2; }
	inline LinearWheelsSettings_t3897781309 * get_address_of_linear_2() { return &___linear_2; }
	inline void set_linear_2(LinearWheelsSettings_t3897781309  value)
	{
		___linear_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWHEELSSETTINGS_T3120867866_H
#ifndef TONEMAPPINGSETTINGS_T4154044775_H
#define TONEMAPPINGSETTINGS_T4154044775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings
struct  TonemappingSettings_t4154044775 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/Tonemapper UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::tonemapper
	int32_t ___tonemapper_0;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralBlackIn
	float ___neutralBlackIn_1;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteIn
	float ___neutralWhiteIn_2;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralBlackOut
	float ___neutralBlackOut_3;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteOut
	float ___neutralWhiteOut_4;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteLevel
	float ___neutralWhiteLevel_5;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteClip
	float ___neutralWhiteClip_6;

public:
	inline static int32_t get_offset_of_tonemapper_0() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___tonemapper_0)); }
	inline int32_t get_tonemapper_0() const { return ___tonemapper_0; }
	inline int32_t* get_address_of_tonemapper_0() { return &___tonemapper_0; }
	inline void set_tonemapper_0(int32_t value)
	{
		___tonemapper_0 = value;
	}

	inline static int32_t get_offset_of_neutralBlackIn_1() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralBlackIn_1)); }
	inline float get_neutralBlackIn_1() const { return ___neutralBlackIn_1; }
	inline float* get_address_of_neutralBlackIn_1() { return &___neutralBlackIn_1; }
	inline void set_neutralBlackIn_1(float value)
	{
		___neutralBlackIn_1 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteIn_2() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteIn_2)); }
	inline float get_neutralWhiteIn_2() const { return ___neutralWhiteIn_2; }
	inline float* get_address_of_neutralWhiteIn_2() { return &___neutralWhiteIn_2; }
	inline void set_neutralWhiteIn_2(float value)
	{
		___neutralWhiteIn_2 = value;
	}

	inline static int32_t get_offset_of_neutralBlackOut_3() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralBlackOut_3)); }
	inline float get_neutralBlackOut_3() const { return ___neutralBlackOut_3; }
	inline float* get_address_of_neutralBlackOut_3() { return &___neutralBlackOut_3; }
	inline void set_neutralBlackOut_3(float value)
	{
		___neutralBlackOut_3 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteOut_4() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteOut_4)); }
	inline float get_neutralWhiteOut_4() const { return ___neutralWhiteOut_4; }
	inline float* get_address_of_neutralWhiteOut_4() { return &___neutralWhiteOut_4; }
	inline void set_neutralWhiteOut_4(float value)
	{
		___neutralWhiteOut_4 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteLevel_5() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteLevel_5)); }
	inline float get_neutralWhiteLevel_5() const { return ___neutralWhiteLevel_5; }
	inline float* get_address_of_neutralWhiteLevel_5() { return &___neutralWhiteLevel_5; }
	inline void set_neutralWhiteLevel_5(float value)
	{
		___neutralWhiteLevel_5 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteClip_6() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteClip_6)); }
	inline float get_neutralWhiteClip_6() const { return ___neutralWhiteClip_6; }
	inline float* get_address_of_neutralWhiteClip_6() { return &___neutralWhiteClip_6; }
	inline void set_neutralWhiteClip_6(float value)
	{
		___neutralWhiteClip_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPINGSETTINGS_T4154044775_H
#ifndef DEPTHOFFIELDCOMPONENT_T554756766_H
#define DEPTHOFFIELDCOMPONENT_T554756766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldComponent
struct  DepthOfFieldComponent_t554756766  : public PostProcessingComponentRenderTexture_1_t2082352371
{
public:
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.DepthOfFieldComponent::m_CoCHistory
	RenderTexture_t2108887433 * ___m_CoCHistory_3;

public:
	inline static int32_t get_offset_of_m_CoCHistory_3() { return static_cast<int32_t>(offsetof(DepthOfFieldComponent_t554756766, ___m_CoCHistory_3)); }
	inline RenderTexture_t2108887433 * get_m_CoCHistory_3() const { return ___m_CoCHistory_3; }
	inline RenderTexture_t2108887433 ** get_address_of_m_CoCHistory_3() { return &___m_CoCHistory_3; }
	inline void set_m_CoCHistory_3(RenderTexture_t2108887433 * value)
	{
		___m_CoCHistory_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CoCHistory_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELDCOMPONENT_T554756766_H
#ifndef DITHERINGCOMPONENT_T277621267_H
#define DITHERINGCOMPONENT_T277621267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringComponent
struct  DitheringComponent_t277621267  : public PostProcessingComponentRenderTexture_1_t3997290437
{
public:
	// UnityEngine.Texture2D[] UnityEngine.PostProcessing.DitheringComponent::noiseTextures
	Texture2DU5BU5D_t149664596* ___noiseTextures_2;
	// System.Int32 UnityEngine.PostProcessing.DitheringComponent::textureIndex
	int32_t ___textureIndex_3;

public:
	inline static int32_t get_offset_of_noiseTextures_2() { return static_cast<int32_t>(offsetof(DitheringComponent_t277621267, ___noiseTextures_2)); }
	inline Texture2DU5BU5D_t149664596* get_noiseTextures_2() const { return ___noiseTextures_2; }
	inline Texture2DU5BU5D_t149664596** get_address_of_noiseTextures_2() { return &___noiseTextures_2; }
	inline void set_noiseTextures_2(Texture2DU5BU5D_t149664596* value)
	{
		___noiseTextures_2 = value;
		Il2CppCodeGenWriteBarrier((&___noiseTextures_2), value);
	}

	inline static int32_t get_offset_of_textureIndex_3() { return static_cast<int32_t>(offsetof(DitheringComponent_t277621267, ___textureIndex_3)); }
	inline int32_t get_textureIndex_3() const { return ___textureIndex_3; }
	inline int32_t* get_address_of_textureIndex_3() { return &___textureIndex_3; }
	inline void set_textureIndex_3(int32_t value)
	{
		___textureIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DITHERINGCOMPONENT_T277621267_H
#ifndef EYEADAPTATIONCOMPONENT_T3394805121_H
#define EYEADAPTATIONCOMPONENT_T3394805121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationComponent
struct  EyeAdaptationComponent_t3394805121  : public PostProcessingComponentRenderTexture_1_t1811108953
{
public:
	// UnityEngine.ComputeShader UnityEngine.PostProcessing.EyeAdaptationComponent::m_EyeCompute
	ComputeShader_t317220254 * ___m_EyeCompute_2;
	// UnityEngine.ComputeBuffer UnityEngine.PostProcessing.EyeAdaptationComponent::m_HistogramBuffer
	ComputeBuffer_t1033194329 * ___m_HistogramBuffer_3;
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.EyeAdaptationComponent::m_AutoExposurePool
	RenderTextureU5BU5D_t4111643188* ___m_AutoExposurePool_4;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent::m_AutoExposurePingPing
	int32_t ___m_AutoExposurePingPing_5;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.EyeAdaptationComponent::m_CurrentAutoExposure
	RenderTexture_t2108887433 * ___m_CurrentAutoExposure_6;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.EyeAdaptationComponent::m_DebugHistogram
	RenderTexture_t2108887433 * ___m_DebugHistogram_7;
	// System.Boolean UnityEngine.PostProcessing.EyeAdaptationComponent::m_FirstFrame
	bool ___m_FirstFrame_9;

public:
	inline static int32_t get_offset_of_m_EyeCompute_2() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_EyeCompute_2)); }
	inline ComputeShader_t317220254 * get_m_EyeCompute_2() const { return ___m_EyeCompute_2; }
	inline ComputeShader_t317220254 ** get_address_of_m_EyeCompute_2() { return &___m_EyeCompute_2; }
	inline void set_m_EyeCompute_2(ComputeShader_t317220254 * value)
	{
		___m_EyeCompute_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_EyeCompute_2), value);
	}

	inline static int32_t get_offset_of_m_HistogramBuffer_3() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_HistogramBuffer_3)); }
	inline ComputeBuffer_t1033194329 * get_m_HistogramBuffer_3() const { return ___m_HistogramBuffer_3; }
	inline ComputeBuffer_t1033194329 ** get_address_of_m_HistogramBuffer_3() { return &___m_HistogramBuffer_3; }
	inline void set_m_HistogramBuffer_3(ComputeBuffer_t1033194329 * value)
	{
		___m_HistogramBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistogramBuffer_3), value);
	}

	inline static int32_t get_offset_of_m_AutoExposurePool_4() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_AutoExposurePool_4)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_AutoExposurePool_4() const { return ___m_AutoExposurePool_4; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_AutoExposurePool_4() { return &___m_AutoExposurePool_4; }
	inline void set_m_AutoExposurePool_4(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_AutoExposurePool_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AutoExposurePool_4), value);
	}

	inline static int32_t get_offset_of_m_AutoExposurePingPing_5() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_AutoExposurePingPing_5)); }
	inline int32_t get_m_AutoExposurePingPing_5() const { return ___m_AutoExposurePingPing_5; }
	inline int32_t* get_address_of_m_AutoExposurePingPing_5() { return &___m_AutoExposurePingPing_5; }
	inline void set_m_AutoExposurePingPing_5(int32_t value)
	{
		___m_AutoExposurePingPing_5 = value;
	}

	inline static int32_t get_offset_of_m_CurrentAutoExposure_6() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_CurrentAutoExposure_6)); }
	inline RenderTexture_t2108887433 * get_m_CurrentAutoExposure_6() const { return ___m_CurrentAutoExposure_6; }
	inline RenderTexture_t2108887433 ** get_address_of_m_CurrentAutoExposure_6() { return &___m_CurrentAutoExposure_6; }
	inline void set_m_CurrentAutoExposure_6(RenderTexture_t2108887433 * value)
	{
		___m_CurrentAutoExposure_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentAutoExposure_6), value);
	}

	inline static int32_t get_offset_of_m_DebugHistogram_7() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_DebugHistogram_7)); }
	inline RenderTexture_t2108887433 * get_m_DebugHistogram_7() const { return ___m_DebugHistogram_7; }
	inline RenderTexture_t2108887433 ** get_address_of_m_DebugHistogram_7() { return &___m_DebugHistogram_7; }
	inline void set_m_DebugHistogram_7(RenderTexture_t2108887433 * value)
	{
		___m_DebugHistogram_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugHistogram_7), value);
	}

	inline static int32_t get_offset_of_m_FirstFrame_9() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_FirstFrame_9)); }
	inline bool get_m_FirstFrame_9() const { return ___m_FirstFrame_9; }
	inline bool* get_address_of_m_FirstFrame_9() { return &___m_FirstFrame_9; }
	inline void set_m_FirstFrame_9(bool value)
	{
		___m_FirstFrame_9 = value;
	}
};

struct EyeAdaptationComponent_t3394805121_StaticFields
{
public:
	// System.UInt32[] UnityEngine.PostProcessing.EyeAdaptationComponent::s_EmptyHistogramBuffer
	UInt32U5BU5D_t2770800703* ___s_EmptyHistogramBuffer_8;

public:
	inline static int32_t get_offset_of_s_EmptyHistogramBuffer_8() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121_StaticFields, ___s_EmptyHistogramBuffer_8)); }
	inline UInt32U5BU5D_t2770800703* get_s_EmptyHistogramBuffer_8() const { return ___s_EmptyHistogramBuffer_8; }
	inline UInt32U5BU5D_t2770800703** get_address_of_s_EmptyHistogramBuffer_8() { return &___s_EmptyHistogramBuffer_8; }
	inline void set_s_EmptyHistogramBuffer_8(UInt32U5BU5D_t2770800703* value)
	{
		___s_EmptyHistogramBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_EmptyHistogramBuffer_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONCOMPONENT_T3394805121_H
#ifndef FOGCOMPONENT_T3400726830_H
#define FOGCOMPONENT_T3400726830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogComponent
struct  FogComponent_t3400726830  : public PostProcessingComponentCommandBuffer_1_t601023342
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOGCOMPONENT_T3400726830_H
#ifndef FXAACOMPONENT_T1312385771_H
#define FXAACOMPONENT_T1312385771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FxaaComponent
struct  FxaaComponent_t1312385771  : public PostProcessingComponentRenderTexture_1_t3089424429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAACOMPONENT_T1312385771_H
#ifndef GRAINCOMPONENT_T866324317_H
#define GRAINCOMPONENT_T866324317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainComponent
struct  GrainComponent_t866324317  : public PostProcessingComponentRenderTexture_1_t2721167529
{
public:
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.GrainComponent::m_GrainLookupRT
	RenderTexture_t2108887433 * ___m_GrainLookupRT_2;

public:
	inline static int32_t get_offset_of_m_GrainLookupRT_2() { return static_cast<int32_t>(offsetof(GrainComponent_t866324317, ___m_GrainLookupRT_2)); }
	inline RenderTexture_t2108887433 * get_m_GrainLookupRT_2() const { return ___m_GrainLookupRT_2; }
	inline RenderTexture_t2108887433 ** get_address_of_m_GrainLookupRT_2() { return &___m_GrainLookupRT_2; }
	inline void set_m_GrainLookupRT_2(RenderTexture_t2108887433 * value)
	{
		___m_GrainLookupRT_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GrainLookupRT_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAINCOMPONENT_T866324317_H
#ifndef MOTIONBLURCOMPONENT_T3686516877_H
#define MOTIONBLURCOMPONENT_T3686516877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent
struct  MotionBlurComponent_t3686516877  : public PostProcessingComponentCommandBuffer_1_t60620716
{
public:
	// UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter UnityEngine.PostProcessing.MotionBlurComponent::m_ReconstructionFilter
	ReconstructionFilter_t705677647 * ___m_ReconstructionFilter_2;
	// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter UnityEngine.PostProcessing.MotionBlurComponent::m_FrameBlendingFilter
	FrameBlendingFilter_t2699796096 * ___m_FrameBlendingFilter_3;
	// System.Boolean UnityEngine.PostProcessing.MotionBlurComponent::m_FirstFrame
	bool ___m_FirstFrame_4;

public:
	inline static int32_t get_offset_of_m_ReconstructionFilter_2() { return static_cast<int32_t>(offsetof(MotionBlurComponent_t3686516877, ___m_ReconstructionFilter_2)); }
	inline ReconstructionFilter_t705677647 * get_m_ReconstructionFilter_2() const { return ___m_ReconstructionFilter_2; }
	inline ReconstructionFilter_t705677647 ** get_address_of_m_ReconstructionFilter_2() { return &___m_ReconstructionFilter_2; }
	inline void set_m_ReconstructionFilter_2(ReconstructionFilter_t705677647 * value)
	{
		___m_ReconstructionFilter_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReconstructionFilter_2), value);
	}

	inline static int32_t get_offset_of_m_FrameBlendingFilter_3() { return static_cast<int32_t>(offsetof(MotionBlurComponent_t3686516877, ___m_FrameBlendingFilter_3)); }
	inline FrameBlendingFilter_t2699796096 * get_m_FrameBlendingFilter_3() const { return ___m_FrameBlendingFilter_3; }
	inline FrameBlendingFilter_t2699796096 ** get_address_of_m_FrameBlendingFilter_3() { return &___m_FrameBlendingFilter_3; }
	inline void set_m_FrameBlendingFilter_3(FrameBlendingFilter_t2699796096 * value)
	{
		___m_FrameBlendingFilter_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_FrameBlendingFilter_3), value);
	}

	inline static int32_t get_offset_of_m_FirstFrame_4() { return static_cast<int32_t>(offsetof(MotionBlurComponent_t3686516877, ___m_FirstFrame_4)); }
	inline bool get_m_FirstFrame_4() const { return ___m_FirstFrame_4; }
	inline bool* get_address_of_m_FirstFrame_4() { return &___m_FirstFrame_4; }
	inline void set_m_FirstFrame_4(bool value)
	{
		___m_FirstFrame_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLURCOMPONENT_T3686516877_H
#ifndef FRAMEBLENDINGFILTER_T2699796096_H
#define FRAMEBLENDINGFILTER_T2699796096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter
struct  FrameBlendingFilter_t2699796096  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::m_UseCompression
	bool ___m_UseCompression_0;
	// UnityEngine.RenderTextureFormat UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::m_RawTextureFormat
	int32_t ___m_RawTextureFormat_1;
	// UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame[] UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::m_FrameList
	FrameU5BU5D_t1363420656* ___m_FrameList_2;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::m_LastFrameCount
	int32_t ___m_LastFrameCount_3;

public:
	inline static int32_t get_offset_of_m_UseCompression_0() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t2699796096, ___m_UseCompression_0)); }
	inline bool get_m_UseCompression_0() const { return ___m_UseCompression_0; }
	inline bool* get_address_of_m_UseCompression_0() { return &___m_UseCompression_0; }
	inline void set_m_UseCompression_0(bool value)
	{
		___m_UseCompression_0 = value;
	}

	inline static int32_t get_offset_of_m_RawTextureFormat_1() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t2699796096, ___m_RawTextureFormat_1)); }
	inline int32_t get_m_RawTextureFormat_1() const { return ___m_RawTextureFormat_1; }
	inline int32_t* get_address_of_m_RawTextureFormat_1() { return &___m_RawTextureFormat_1; }
	inline void set_m_RawTextureFormat_1(int32_t value)
	{
		___m_RawTextureFormat_1 = value;
	}

	inline static int32_t get_offset_of_m_FrameList_2() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t2699796096, ___m_FrameList_2)); }
	inline FrameU5BU5D_t1363420656* get_m_FrameList_2() const { return ___m_FrameList_2; }
	inline FrameU5BU5D_t1363420656** get_address_of_m_FrameList_2() { return &___m_FrameList_2; }
	inline void set_m_FrameList_2(FrameU5BU5D_t1363420656* value)
	{
		___m_FrameList_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FrameList_2), value);
	}

	inline static int32_t get_offset_of_m_LastFrameCount_3() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t2699796096, ___m_LastFrameCount_3)); }
	inline int32_t get_m_LastFrameCount_3() const { return ___m_LastFrameCount_3; }
	inline int32_t* get_address_of_m_LastFrameCount_3() { return &___m_LastFrameCount_3; }
	inline void set_m_LastFrameCount_3(int32_t value)
	{
		___m_LastFrameCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEBLENDINGFILTER_T2699796096_H
#ifndef RECONSTRUCTIONFILTER_T705677647_H
#define RECONSTRUCTIONFILTER_T705677647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter
struct  ReconstructionFilter_t705677647  : public RuntimeObject
{
public:
	// UnityEngine.RenderTextureFormat UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter::m_VectorRTFormat
	int32_t ___m_VectorRTFormat_0;
	// UnityEngine.RenderTextureFormat UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter::m_PackedRTFormat
	int32_t ___m_PackedRTFormat_1;

public:
	inline static int32_t get_offset_of_m_VectorRTFormat_0() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t705677647, ___m_VectorRTFormat_0)); }
	inline int32_t get_m_VectorRTFormat_0() const { return ___m_VectorRTFormat_0; }
	inline int32_t* get_address_of_m_VectorRTFormat_0() { return &___m_VectorRTFormat_0; }
	inline void set_m_VectorRTFormat_0(int32_t value)
	{
		___m_VectorRTFormat_0 = value;
	}

	inline static int32_t get_offset_of_m_PackedRTFormat_1() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t705677647, ___m_PackedRTFormat_1)); }
	inline int32_t get_m_PackedRTFormat_1() const { return ___m_PackedRTFormat_1; }
	inline int32_t* get_address_of_m_PackedRTFormat_1() { return &___m_PackedRTFormat_1; }
	inline void set_m_PackedRTFormat_1(int32_t value)
	{
		___m_PackedRTFormat_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONFILTER_T705677647_H
#ifndef SCREENSPACEREFLECTIONCOMPONENT_T856094247_H
#define SCREENSPACEREFLECTIONCOMPONENT_T856094247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent
struct  ScreenSpaceReflectionComponent_t856094247  : public PostProcessingComponentCommandBuffer_1_t6679325
{
public:
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_HighlightSuppression
	bool ___k_HighlightSuppression_2;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_TraceBehindObjects
	bool ___k_TraceBehindObjects_3;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_TreatBackfaceHitAsMiss
	bool ___k_TreatBackfaceHitAsMiss_4;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::k_BilateralUpsample
	bool ___k_BilateralUpsample_5;
	// System.Int32[] UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::m_ReflectionTextures
	Int32U5BU5D_t385246372* ___m_ReflectionTextures_6;

public:
	inline static int32_t get_offset_of_k_HighlightSuppression_2() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_HighlightSuppression_2)); }
	inline bool get_k_HighlightSuppression_2() const { return ___k_HighlightSuppression_2; }
	inline bool* get_address_of_k_HighlightSuppression_2() { return &___k_HighlightSuppression_2; }
	inline void set_k_HighlightSuppression_2(bool value)
	{
		___k_HighlightSuppression_2 = value;
	}

	inline static int32_t get_offset_of_k_TraceBehindObjects_3() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_TraceBehindObjects_3)); }
	inline bool get_k_TraceBehindObjects_3() const { return ___k_TraceBehindObjects_3; }
	inline bool* get_address_of_k_TraceBehindObjects_3() { return &___k_TraceBehindObjects_3; }
	inline void set_k_TraceBehindObjects_3(bool value)
	{
		___k_TraceBehindObjects_3 = value;
	}

	inline static int32_t get_offset_of_k_TreatBackfaceHitAsMiss_4() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_TreatBackfaceHitAsMiss_4)); }
	inline bool get_k_TreatBackfaceHitAsMiss_4() const { return ___k_TreatBackfaceHitAsMiss_4; }
	inline bool* get_address_of_k_TreatBackfaceHitAsMiss_4() { return &___k_TreatBackfaceHitAsMiss_4; }
	inline void set_k_TreatBackfaceHitAsMiss_4(bool value)
	{
		___k_TreatBackfaceHitAsMiss_4 = value;
	}

	inline static int32_t get_offset_of_k_BilateralUpsample_5() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___k_BilateralUpsample_5)); }
	inline bool get_k_BilateralUpsample_5() const { return ___k_BilateralUpsample_5; }
	inline bool* get_address_of_k_BilateralUpsample_5() { return &___k_BilateralUpsample_5; }
	inline void set_k_BilateralUpsample_5(bool value)
	{
		___k_BilateralUpsample_5 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionTextures_6() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionComponent_t856094247, ___m_ReflectionTextures_6)); }
	inline Int32U5BU5D_t385246372* get_m_ReflectionTextures_6() const { return ___m_ReflectionTextures_6; }
	inline Int32U5BU5D_t385246372** get_address_of_m_ReflectionTextures_6() { return &___m_ReflectionTextures_6; }
	inline void set_m_ReflectionTextures_6(Int32U5BU5D_t385246372* value)
	{
		___m_ReflectionTextures_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionTextures_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONCOMPONENT_T856094247_H
#ifndef TAACOMPONENT_T3791749658_H
#define TAACOMPONENT_T3791749658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TaaComponent
struct  TaaComponent_t3791749658  : public PostProcessingComponentRenderTexture_1_t3089424429
{
public:
	// UnityEngine.RenderBuffer[] UnityEngine.PostProcessing.TaaComponent::m_MRT
	RenderBufferU5BU5D_t1615831949* ___m_MRT_4;
	// System.Int32 UnityEngine.PostProcessing.TaaComponent::m_SampleIndex
	int32_t ___m_SampleIndex_5;
	// System.Boolean UnityEngine.PostProcessing.TaaComponent::m_ResetHistory
	bool ___m_ResetHistory_6;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.TaaComponent::m_HistoryTexture
	RenderTexture_t2108887433 * ___m_HistoryTexture_7;
	// UnityEngine.Vector2 UnityEngine.PostProcessing.TaaComponent::<jitterVector>k__BackingField
	Vector2_t2156229523  ___U3CjitterVectorU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_m_MRT_4() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_MRT_4)); }
	inline RenderBufferU5BU5D_t1615831949* get_m_MRT_4() const { return ___m_MRT_4; }
	inline RenderBufferU5BU5D_t1615831949** get_address_of_m_MRT_4() { return &___m_MRT_4; }
	inline void set_m_MRT_4(RenderBufferU5BU5D_t1615831949* value)
	{
		___m_MRT_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_4), value);
	}

	inline static int32_t get_offset_of_m_SampleIndex_5() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_SampleIndex_5)); }
	inline int32_t get_m_SampleIndex_5() const { return ___m_SampleIndex_5; }
	inline int32_t* get_address_of_m_SampleIndex_5() { return &___m_SampleIndex_5; }
	inline void set_m_SampleIndex_5(int32_t value)
	{
		___m_SampleIndex_5 = value;
	}

	inline static int32_t get_offset_of_m_ResetHistory_6() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_ResetHistory_6)); }
	inline bool get_m_ResetHistory_6() const { return ___m_ResetHistory_6; }
	inline bool* get_address_of_m_ResetHistory_6() { return &___m_ResetHistory_6; }
	inline void set_m_ResetHistory_6(bool value)
	{
		___m_ResetHistory_6 = value;
	}

	inline static int32_t get_offset_of_m_HistoryTexture_7() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___m_HistoryTexture_7)); }
	inline RenderTexture_t2108887433 * get_m_HistoryTexture_7() const { return ___m_HistoryTexture_7; }
	inline RenderTexture_t2108887433 ** get_address_of_m_HistoryTexture_7() { return &___m_HistoryTexture_7; }
	inline void set_m_HistoryTexture_7(RenderTexture_t2108887433 * value)
	{
		___m_HistoryTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistoryTexture_7), value);
	}

	inline static int32_t get_offset_of_U3CjitterVectorU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TaaComponent_t3791749658, ___U3CjitterVectorU3Ek__BackingField_8)); }
	inline Vector2_t2156229523  get_U3CjitterVectorU3Ek__BackingField_8() const { return ___U3CjitterVectorU3Ek__BackingField_8; }
	inline Vector2_t2156229523 * get_address_of_U3CjitterVectorU3Ek__BackingField_8() { return &___U3CjitterVectorU3Ek__BackingField_8; }
	inline void set_U3CjitterVectorU3Ek__BackingField_8(Vector2_t2156229523  value)
	{
		___U3CjitterVectorU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAACOMPONENT_T3791749658_H
#ifndef USERLUTCOMPONENT_T2843161776_H
#define USERLUTCOMPONENT_T2843161776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutComponent
struct  UserLutComponent_t2843161776  : public PostProcessingComponentRenderTexture_1_t3238393121
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERLUTCOMPONENT_T2843161776_H
#ifndef VIGNETTECOMPONENT_T3243642943_H
#define VIGNETTECOMPONENT_T3243642943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteComponent
struct  VignetteComponent_t3243642943  : public PostProcessingComponentRenderTexture_1_t118834922
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTECOMPONENT_T3243642943_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef README_T3138389917_H
#define README_T3138389917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Runemark.DarkFantasyKit.Readme
struct  Readme_t3138389917  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Texture2D Runemark.DarkFantasyKit.Readme::icon
	Texture2D_t3840446185 * ___icon_4;
	// System.String Runemark.DarkFantasyKit.Readme::title
	String_t* ___title_5;
	// System.Collections.Generic.List`1<Runemark.DarkFantasyKit.Readme/Section> Runemark.DarkFantasyKit.Readme::sections
	List_1_t4166117572 * ___sections_6;
	// System.Boolean Runemark.DarkFantasyKit.Readme::loadedLayout
	bool ___loadedLayout_7;

public:
	inline static int32_t get_offset_of_icon_4() { return static_cast<int32_t>(offsetof(Readme_t3138389917, ___icon_4)); }
	inline Texture2D_t3840446185 * get_icon_4() const { return ___icon_4; }
	inline Texture2D_t3840446185 ** get_address_of_icon_4() { return &___icon_4; }
	inline void set_icon_4(Texture2D_t3840446185 * value)
	{
		___icon_4 = value;
		Il2CppCodeGenWriteBarrier((&___icon_4), value);
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(Readme_t3138389917, ___title_5)); }
	inline String_t* get_title_5() const { return ___title_5; }
	inline String_t** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(String_t* value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier((&___title_5), value);
	}

	inline static int32_t get_offset_of_sections_6() { return static_cast<int32_t>(offsetof(Readme_t3138389917, ___sections_6)); }
	inline List_1_t4166117572 * get_sections_6() const { return ___sections_6; }
	inline List_1_t4166117572 ** get_address_of_sections_6() { return &___sections_6; }
	inline void set_sections_6(List_1_t4166117572 * value)
	{
		___sections_6 = value;
		Il2CppCodeGenWriteBarrier((&___sections_6), value);
	}

	inline static int32_t get_offset_of_loadedLayout_7() { return static_cast<int32_t>(offsetof(Readme_t3138389917, ___loadedLayout_7)); }
	inline bool get_loadedLayout_7() const { return ___loadedLayout_7; }
	inline bool* get_address_of_loadedLayout_7() { return &___loadedLayout_7; }
	inline void set_loadedLayout_7(bool value)
	{
		___loadedLayout_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // README_T3138389917_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef AMBIENTOCCLUSIONMODEL_T389471066_H
#define AMBIENTOCCLUSIONMODEL_T389471066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionModel
struct  AmbientOcclusionModel_t389471066  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.AmbientOcclusionModel/Settings UnityEngine.PostProcessing.AmbientOcclusionModel::m_Settings
	Settings_t3016786575  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(AmbientOcclusionModel_t389471066, ___m_Settings_1)); }
	inline Settings_t3016786575  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t3016786575 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t3016786575  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONMODEL_T389471066_H
#ifndef SETTINGS_T4292431647_H
#define SETTINGS_T4292431647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel/Settings
struct  Settings_t4292431647 
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/Method UnityEngine.PostProcessing.AntialiasingModel/Settings::method
	int32_t ___method_0;
	// UnityEngine.PostProcessing.AntialiasingModel/FxaaSettings UnityEngine.PostProcessing.AntialiasingModel/Settings::fxaaSettings
	FxaaSettings_t1280675075  ___fxaaSettings_1;
	// UnityEngine.PostProcessing.AntialiasingModel/TaaSettings UnityEngine.PostProcessing.AntialiasingModel/Settings::taaSettings
	TaaSettings_t2709374970  ___taaSettings_2;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(Settings_t4292431647, ___method_0)); }
	inline int32_t get_method_0() const { return ___method_0; }
	inline int32_t* get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(int32_t value)
	{
		___method_0 = value;
	}

	inline static int32_t get_offset_of_fxaaSettings_1() { return static_cast<int32_t>(offsetof(Settings_t4292431647, ___fxaaSettings_1)); }
	inline FxaaSettings_t1280675075  get_fxaaSettings_1() const { return ___fxaaSettings_1; }
	inline FxaaSettings_t1280675075 * get_address_of_fxaaSettings_1() { return &___fxaaSettings_1; }
	inline void set_fxaaSettings_1(FxaaSettings_t1280675075  value)
	{
		___fxaaSettings_1 = value;
	}

	inline static int32_t get_offset_of_taaSettings_2() { return static_cast<int32_t>(offsetof(Settings_t4292431647, ___taaSettings_2)); }
	inline TaaSettings_t2709374970  get_taaSettings_2() const { return ___taaSettings_2; }
	inline TaaSettings_t2709374970 * get_address_of_taaSettings_2() { return &___taaSettings_2; }
	inline void set_taaSettings_2(TaaSettings_t2709374970  value)
	{
		___taaSettings_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T4292431647_H
#ifndef BUILTINDEBUGVIEWSMODEL_T1462618840_H
#define BUILTINDEBUGVIEWSMODEL_T1462618840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsModel
struct  BuiltinDebugViewsModel_t1462618840  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings UnityEngine.PostProcessing.BuiltinDebugViewsModel::m_Settings
	Settings_t3984509665  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(BuiltinDebugViewsModel_t1462618840, ___m_Settings_1)); }
	inline Settings_t3984509665  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t3984509665 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t3984509665  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINDEBUGVIEWSMODEL_T1462618840_H
#ifndef SETTINGS_T451872061_H
#define SETTINGS_T451872061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/Settings
struct  Settings_t451872061 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::tonemapping
	TonemappingSettings_t4154044775  ___tonemapping_0;
	// UnityEngine.PostProcessing.ColorGradingModel/BasicSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::basic
	BasicSettings_t838098426  ___basic_1;
	// UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::channelMixer
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::colorWheels
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	// UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::curves
	CurvesSettings_t2830270037  ___curves_4;

public:
	inline static int32_t get_offset_of_tonemapping_0() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___tonemapping_0)); }
	inline TonemappingSettings_t4154044775  get_tonemapping_0() const { return ___tonemapping_0; }
	inline TonemappingSettings_t4154044775 * get_address_of_tonemapping_0() { return &___tonemapping_0; }
	inline void set_tonemapping_0(TonemappingSettings_t4154044775  value)
	{
		___tonemapping_0 = value;
	}

	inline static int32_t get_offset_of_basic_1() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___basic_1)); }
	inline BasicSettings_t838098426  get_basic_1() const { return ___basic_1; }
	inline BasicSettings_t838098426 * get_address_of_basic_1() { return &___basic_1; }
	inline void set_basic_1(BasicSettings_t838098426  value)
	{
		___basic_1 = value;
	}

	inline static int32_t get_offset_of_channelMixer_2() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___channelMixer_2)); }
	inline ChannelMixerSettings_t898701698  get_channelMixer_2() const { return ___channelMixer_2; }
	inline ChannelMixerSettings_t898701698 * get_address_of_channelMixer_2() { return &___channelMixer_2; }
	inline void set_channelMixer_2(ChannelMixerSettings_t898701698  value)
	{
		___channelMixer_2 = value;
	}

	inline static int32_t get_offset_of_colorWheels_3() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___colorWheels_3)); }
	inline ColorWheelsSettings_t3120867866  get_colorWheels_3() const { return ___colorWheels_3; }
	inline ColorWheelsSettings_t3120867866 * get_address_of_colorWheels_3() { return &___colorWheels_3; }
	inline void set_colorWheels_3(ColorWheelsSettings_t3120867866  value)
	{
		___colorWheels_3 = value;
	}

	inline static int32_t get_offset_of_curves_4() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___curves_4)); }
	inline CurvesSettings_t2830270037  get_curves_4() const { return ___curves_4; }
	inline CurvesSettings_t2830270037 * get_address_of_curves_4() { return &___curves_4; }
	inline void set_curves_4(CurvesSettings_t2830270037  value)
	{
		___curves_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ColorGradingModel/Settings
struct Settings_t451872061_marshaled_pinvoke
{
	TonemappingSettings_t4154044775  ___tonemapping_0;
	BasicSettings_t838098426  ___basic_1;
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	CurvesSettings_t2830270037_marshaled_pinvoke ___curves_4;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ColorGradingModel/Settings
struct Settings_t451872061_marshaled_com
{
	TonemappingSettings_t4154044775  ___tonemapping_0;
	BasicSettings_t838098426  ___basic_1;
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	CurvesSettings_t2830270037_marshaled_com ___curves_4;
};
#endif // SETTINGS_T451872061_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ANTIALIASINGMODEL_T1521139388_H
#define ANTIALIASINGMODEL_T1521139388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AntialiasingModel
struct  AntialiasingModel_t1521139388  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.AntialiasingModel/Settings UnityEngine.PostProcessing.AntialiasingModel::m_Settings
	Settings_t4292431647  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(AntialiasingModel_t1521139388, ___m_Settings_1)); }
	inline Settings_t4292431647  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t4292431647 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t4292431647  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANTIALIASINGMODEL_T1521139388_H
#ifndef COLORGRADINGMODEL_T1448048181_H
#define COLORGRADINGMODEL_T1448048181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel
struct  ColorGradingModel_t1448048181  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/Settings UnityEngine.PostProcessing.ColorGradingModel::m_Settings
	Settings_t451872061  ___m_Settings_1;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel::<isDirty>k__BackingField
	bool ___U3CisDirtyU3Ek__BackingField_2;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.ColorGradingModel::<bakedLut>k__BackingField
	RenderTexture_t2108887433 * ___U3CbakedLutU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(ColorGradingModel_t1448048181, ___m_Settings_1)); }
	inline Settings_t451872061  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t451872061 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t451872061  value)
	{
		___m_Settings_1 = value;
	}

	inline static int32_t get_offset_of_U3CisDirtyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ColorGradingModel_t1448048181, ___U3CisDirtyU3Ek__BackingField_2)); }
	inline bool get_U3CisDirtyU3Ek__BackingField_2() const { return ___U3CisDirtyU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CisDirtyU3Ek__BackingField_2() { return &___U3CisDirtyU3Ek__BackingField_2; }
	inline void set_U3CisDirtyU3Ek__BackingField_2(bool value)
	{
		___U3CisDirtyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CbakedLutU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ColorGradingModel_t1448048181, ___U3CbakedLutU3Ek__BackingField_3)); }
	inline RenderTexture_t2108887433 * get_U3CbakedLutU3Ek__BackingField_3() const { return ___U3CbakedLutU3Ek__BackingField_3; }
	inline RenderTexture_t2108887433 ** get_address_of_U3CbakedLutU3Ek__BackingField_3() { return &___U3CbakedLutU3Ek__BackingField_3; }
	inline void set_U3CbakedLutU3Ek__BackingField_3(RenderTexture_t2108887433 * value)
	{
		___U3CbakedLutU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbakedLutU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGMODEL_T1448048181_H
#ifndef ALPHABUTTONCLICKMASK_T141136539_H
#define ALPHABUTTONCLICKMASK_T141136539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlphaButtonClickMask
struct  AlphaButtonClickMask_t141136539  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image AlphaButtonClickMask::_image
	Image_t2670269651 * ____image_4;

public:
	inline static int32_t get_offset_of__image_4() { return static_cast<int32_t>(offsetof(AlphaButtonClickMask_t141136539, ____image_4)); }
	inline Image_t2670269651 * get__image_4() const { return ____image_4; }
	inline Image_t2670269651 ** get_address_of__image_4() { return &____image_4; }
	inline void set__image_4(Image_t2670269651 * value)
	{
		____image_4 = value;
		Il2CppCodeGenWriteBarrier((&____image_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALPHABUTTONCLICKMASK_T141136539_H
#ifndef BASERAINSCRIPT_T980278882_H
#define BASERAINSCRIPT_T980278882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.RainMaker.BaseRainScript
struct  BaseRainScript_t980278882  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera DigitalRuby.RainMaker.BaseRainScript::Camera
	Camera_t4157153871 * ___Camera_4;
	// System.Boolean DigitalRuby.RainMaker.BaseRainScript::FollowCamera
	bool ___FollowCamera_5;
	// UnityEngine.AudioClip DigitalRuby.RainMaker.BaseRainScript::RainSoundLight
	AudioClip_t3680889665 * ___RainSoundLight_6;
	// UnityEngine.AudioClip DigitalRuby.RainMaker.BaseRainScript::RainSoundMedium
	AudioClip_t3680889665 * ___RainSoundMedium_7;
	// UnityEngine.AudioClip DigitalRuby.RainMaker.BaseRainScript::RainSoundHeavy
	AudioClip_t3680889665 * ___RainSoundHeavy_8;
	// System.Single DigitalRuby.RainMaker.BaseRainScript::RainIntensity
	float ___RainIntensity_9;
	// UnityEngine.ParticleSystem DigitalRuby.RainMaker.BaseRainScript::RainFallParticleSystem
	ParticleSystem_t1800779281 * ___RainFallParticleSystem_10;
	// UnityEngine.ParticleSystem DigitalRuby.RainMaker.BaseRainScript::RainExplosionParticleSystem
	ParticleSystem_t1800779281 * ___RainExplosionParticleSystem_11;
	// UnityEngine.ParticleSystem DigitalRuby.RainMaker.BaseRainScript::RainMistParticleSystem
	ParticleSystem_t1800779281 * ___RainMistParticleSystem_12;
	// System.Single DigitalRuby.RainMaker.BaseRainScript::RainMistThreshold
	float ___RainMistThreshold_13;
	// UnityEngine.AudioClip DigitalRuby.RainMaker.BaseRainScript::WindSound
	AudioClip_t3680889665 * ___WindSound_14;
	// System.Single DigitalRuby.RainMaker.BaseRainScript::WindSoundVolumeModifier
	float ___WindSoundVolumeModifier_15;
	// UnityEngine.WindZone DigitalRuby.RainMaker.BaseRainScript::WindZone
	WindZone_t1835817526 * ___WindZone_16;
	// UnityEngine.Vector3 DigitalRuby.RainMaker.BaseRainScript::WindSpeedRange
	Vector3_t3722313464  ___WindSpeedRange_17;
	// UnityEngine.Vector2 DigitalRuby.RainMaker.BaseRainScript::WindChangeInterval
	Vector2_t2156229523  ___WindChangeInterval_18;
	// System.Boolean DigitalRuby.RainMaker.BaseRainScript::EnableWind
	bool ___EnableWind_19;
	// DigitalRuby.RainMaker.LoopingAudioSource DigitalRuby.RainMaker.BaseRainScript::audioSourceRainLight
	LoopingAudioSource_t2892973928 * ___audioSourceRainLight_20;
	// DigitalRuby.RainMaker.LoopingAudioSource DigitalRuby.RainMaker.BaseRainScript::audioSourceRainMedium
	LoopingAudioSource_t2892973928 * ___audioSourceRainMedium_21;
	// DigitalRuby.RainMaker.LoopingAudioSource DigitalRuby.RainMaker.BaseRainScript::audioSourceRainHeavy
	LoopingAudioSource_t2892973928 * ___audioSourceRainHeavy_22;
	// DigitalRuby.RainMaker.LoopingAudioSource DigitalRuby.RainMaker.BaseRainScript::audioSourceRainCurrent
	LoopingAudioSource_t2892973928 * ___audioSourceRainCurrent_23;
	// DigitalRuby.RainMaker.LoopingAudioSource DigitalRuby.RainMaker.BaseRainScript::audioSourceWind
	LoopingAudioSource_t2892973928 * ___audioSourceWind_24;
	// UnityEngine.Material DigitalRuby.RainMaker.BaseRainScript::rainMaterial
	Material_t340375123 * ___rainMaterial_25;
	// UnityEngine.Material DigitalRuby.RainMaker.BaseRainScript::rainExplosionMaterial
	Material_t340375123 * ___rainExplosionMaterial_26;
	// UnityEngine.Material DigitalRuby.RainMaker.BaseRainScript::rainMistMaterial
	Material_t340375123 * ___rainMistMaterial_27;
	// System.Single DigitalRuby.RainMaker.BaseRainScript::lastRainIntensityValue
	float ___lastRainIntensityValue_28;
	// System.Single DigitalRuby.RainMaker.BaseRainScript::nextWindTime
	float ___nextWindTime_29;

public:
	inline static int32_t get_offset_of_Camera_4() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___Camera_4)); }
	inline Camera_t4157153871 * get_Camera_4() const { return ___Camera_4; }
	inline Camera_t4157153871 ** get_address_of_Camera_4() { return &___Camera_4; }
	inline void set_Camera_4(Camera_t4157153871 * value)
	{
		___Camera_4 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_4), value);
	}

	inline static int32_t get_offset_of_FollowCamera_5() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___FollowCamera_5)); }
	inline bool get_FollowCamera_5() const { return ___FollowCamera_5; }
	inline bool* get_address_of_FollowCamera_5() { return &___FollowCamera_5; }
	inline void set_FollowCamera_5(bool value)
	{
		___FollowCamera_5 = value;
	}

	inline static int32_t get_offset_of_RainSoundLight_6() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___RainSoundLight_6)); }
	inline AudioClip_t3680889665 * get_RainSoundLight_6() const { return ___RainSoundLight_6; }
	inline AudioClip_t3680889665 ** get_address_of_RainSoundLight_6() { return &___RainSoundLight_6; }
	inline void set_RainSoundLight_6(AudioClip_t3680889665 * value)
	{
		___RainSoundLight_6 = value;
		Il2CppCodeGenWriteBarrier((&___RainSoundLight_6), value);
	}

	inline static int32_t get_offset_of_RainSoundMedium_7() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___RainSoundMedium_7)); }
	inline AudioClip_t3680889665 * get_RainSoundMedium_7() const { return ___RainSoundMedium_7; }
	inline AudioClip_t3680889665 ** get_address_of_RainSoundMedium_7() { return &___RainSoundMedium_7; }
	inline void set_RainSoundMedium_7(AudioClip_t3680889665 * value)
	{
		___RainSoundMedium_7 = value;
		Il2CppCodeGenWriteBarrier((&___RainSoundMedium_7), value);
	}

	inline static int32_t get_offset_of_RainSoundHeavy_8() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___RainSoundHeavy_8)); }
	inline AudioClip_t3680889665 * get_RainSoundHeavy_8() const { return ___RainSoundHeavy_8; }
	inline AudioClip_t3680889665 ** get_address_of_RainSoundHeavy_8() { return &___RainSoundHeavy_8; }
	inline void set_RainSoundHeavy_8(AudioClip_t3680889665 * value)
	{
		___RainSoundHeavy_8 = value;
		Il2CppCodeGenWriteBarrier((&___RainSoundHeavy_8), value);
	}

	inline static int32_t get_offset_of_RainIntensity_9() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___RainIntensity_9)); }
	inline float get_RainIntensity_9() const { return ___RainIntensity_9; }
	inline float* get_address_of_RainIntensity_9() { return &___RainIntensity_9; }
	inline void set_RainIntensity_9(float value)
	{
		___RainIntensity_9 = value;
	}

	inline static int32_t get_offset_of_RainFallParticleSystem_10() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___RainFallParticleSystem_10)); }
	inline ParticleSystem_t1800779281 * get_RainFallParticleSystem_10() const { return ___RainFallParticleSystem_10; }
	inline ParticleSystem_t1800779281 ** get_address_of_RainFallParticleSystem_10() { return &___RainFallParticleSystem_10; }
	inline void set_RainFallParticleSystem_10(ParticleSystem_t1800779281 * value)
	{
		___RainFallParticleSystem_10 = value;
		Il2CppCodeGenWriteBarrier((&___RainFallParticleSystem_10), value);
	}

	inline static int32_t get_offset_of_RainExplosionParticleSystem_11() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___RainExplosionParticleSystem_11)); }
	inline ParticleSystem_t1800779281 * get_RainExplosionParticleSystem_11() const { return ___RainExplosionParticleSystem_11; }
	inline ParticleSystem_t1800779281 ** get_address_of_RainExplosionParticleSystem_11() { return &___RainExplosionParticleSystem_11; }
	inline void set_RainExplosionParticleSystem_11(ParticleSystem_t1800779281 * value)
	{
		___RainExplosionParticleSystem_11 = value;
		Il2CppCodeGenWriteBarrier((&___RainExplosionParticleSystem_11), value);
	}

	inline static int32_t get_offset_of_RainMistParticleSystem_12() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___RainMistParticleSystem_12)); }
	inline ParticleSystem_t1800779281 * get_RainMistParticleSystem_12() const { return ___RainMistParticleSystem_12; }
	inline ParticleSystem_t1800779281 ** get_address_of_RainMistParticleSystem_12() { return &___RainMistParticleSystem_12; }
	inline void set_RainMistParticleSystem_12(ParticleSystem_t1800779281 * value)
	{
		___RainMistParticleSystem_12 = value;
		Il2CppCodeGenWriteBarrier((&___RainMistParticleSystem_12), value);
	}

	inline static int32_t get_offset_of_RainMistThreshold_13() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___RainMistThreshold_13)); }
	inline float get_RainMistThreshold_13() const { return ___RainMistThreshold_13; }
	inline float* get_address_of_RainMistThreshold_13() { return &___RainMistThreshold_13; }
	inline void set_RainMistThreshold_13(float value)
	{
		___RainMistThreshold_13 = value;
	}

	inline static int32_t get_offset_of_WindSound_14() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___WindSound_14)); }
	inline AudioClip_t3680889665 * get_WindSound_14() const { return ___WindSound_14; }
	inline AudioClip_t3680889665 ** get_address_of_WindSound_14() { return &___WindSound_14; }
	inline void set_WindSound_14(AudioClip_t3680889665 * value)
	{
		___WindSound_14 = value;
		Il2CppCodeGenWriteBarrier((&___WindSound_14), value);
	}

	inline static int32_t get_offset_of_WindSoundVolumeModifier_15() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___WindSoundVolumeModifier_15)); }
	inline float get_WindSoundVolumeModifier_15() const { return ___WindSoundVolumeModifier_15; }
	inline float* get_address_of_WindSoundVolumeModifier_15() { return &___WindSoundVolumeModifier_15; }
	inline void set_WindSoundVolumeModifier_15(float value)
	{
		___WindSoundVolumeModifier_15 = value;
	}

	inline static int32_t get_offset_of_WindZone_16() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___WindZone_16)); }
	inline WindZone_t1835817526 * get_WindZone_16() const { return ___WindZone_16; }
	inline WindZone_t1835817526 ** get_address_of_WindZone_16() { return &___WindZone_16; }
	inline void set_WindZone_16(WindZone_t1835817526 * value)
	{
		___WindZone_16 = value;
		Il2CppCodeGenWriteBarrier((&___WindZone_16), value);
	}

	inline static int32_t get_offset_of_WindSpeedRange_17() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___WindSpeedRange_17)); }
	inline Vector3_t3722313464  get_WindSpeedRange_17() const { return ___WindSpeedRange_17; }
	inline Vector3_t3722313464 * get_address_of_WindSpeedRange_17() { return &___WindSpeedRange_17; }
	inline void set_WindSpeedRange_17(Vector3_t3722313464  value)
	{
		___WindSpeedRange_17 = value;
	}

	inline static int32_t get_offset_of_WindChangeInterval_18() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___WindChangeInterval_18)); }
	inline Vector2_t2156229523  get_WindChangeInterval_18() const { return ___WindChangeInterval_18; }
	inline Vector2_t2156229523 * get_address_of_WindChangeInterval_18() { return &___WindChangeInterval_18; }
	inline void set_WindChangeInterval_18(Vector2_t2156229523  value)
	{
		___WindChangeInterval_18 = value;
	}

	inline static int32_t get_offset_of_EnableWind_19() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___EnableWind_19)); }
	inline bool get_EnableWind_19() const { return ___EnableWind_19; }
	inline bool* get_address_of_EnableWind_19() { return &___EnableWind_19; }
	inline void set_EnableWind_19(bool value)
	{
		___EnableWind_19 = value;
	}

	inline static int32_t get_offset_of_audioSourceRainLight_20() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___audioSourceRainLight_20)); }
	inline LoopingAudioSource_t2892973928 * get_audioSourceRainLight_20() const { return ___audioSourceRainLight_20; }
	inline LoopingAudioSource_t2892973928 ** get_address_of_audioSourceRainLight_20() { return &___audioSourceRainLight_20; }
	inline void set_audioSourceRainLight_20(LoopingAudioSource_t2892973928 * value)
	{
		___audioSourceRainLight_20 = value;
		Il2CppCodeGenWriteBarrier((&___audioSourceRainLight_20), value);
	}

	inline static int32_t get_offset_of_audioSourceRainMedium_21() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___audioSourceRainMedium_21)); }
	inline LoopingAudioSource_t2892973928 * get_audioSourceRainMedium_21() const { return ___audioSourceRainMedium_21; }
	inline LoopingAudioSource_t2892973928 ** get_address_of_audioSourceRainMedium_21() { return &___audioSourceRainMedium_21; }
	inline void set_audioSourceRainMedium_21(LoopingAudioSource_t2892973928 * value)
	{
		___audioSourceRainMedium_21 = value;
		Il2CppCodeGenWriteBarrier((&___audioSourceRainMedium_21), value);
	}

	inline static int32_t get_offset_of_audioSourceRainHeavy_22() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___audioSourceRainHeavy_22)); }
	inline LoopingAudioSource_t2892973928 * get_audioSourceRainHeavy_22() const { return ___audioSourceRainHeavy_22; }
	inline LoopingAudioSource_t2892973928 ** get_address_of_audioSourceRainHeavy_22() { return &___audioSourceRainHeavy_22; }
	inline void set_audioSourceRainHeavy_22(LoopingAudioSource_t2892973928 * value)
	{
		___audioSourceRainHeavy_22 = value;
		Il2CppCodeGenWriteBarrier((&___audioSourceRainHeavy_22), value);
	}

	inline static int32_t get_offset_of_audioSourceRainCurrent_23() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___audioSourceRainCurrent_23)); }
	inline LoopingAudioSource_t2892973928 * get_audioSourceRainCurrent_23() const { return ___audioSourceRainCurrent_23; }
	inline LoopingAudioSource_t2892973928 ** get_address_of_audioSourceRainCurrent_23() { return &___audioSourceRainCurrent_23; }
	inline void set_audioSourceRainCurrent_23(LoopingAudioSource_t2892973928 * value)
	{
		___audioSourceRainCurrent_23 = value;
		Il2CppCodeGenWriteBarrier((&___audioSourceRainCurrent_23), value);
	}

	inline static int32_t get_offset_of_audioSourceWind_24() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___audioSourceWind_24)); }
	inline LoopingAudioSource_t2892973928 * get_audioSourceWind_24() const { return ___audioSourceWind_24; }
	inline LoopingAudioSource_t2892973928 ** get_address_of_audioSourceWind_24() { return &___audioSourceWind_24; }
	inline void set_audioSourceWind_24(LoopingAudioSource_t2892973928 * value)
	{
		___audioSourceWind_24 = value;
		Il2CppCodeGenWriteBarrier((&___audioSourceWind_24), value);
	}

	inline static int32_t get_offset_of_rainMaterial_25() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___rainMaterial_25)); }
	inline Material_t340375123 * get_rainMaterial_25() const { return ___rainMaterial_25; }
	inline Material_t340375123 ** get_address_of_rainMaterial_25() { return &___rainMaterial_25; }
	inline void set_rainMaterial_25(Material_t340375123 * value)
	{
		___rainMaterial_25 = value;
		Il2CppCodeGenWriteBarrier((&___rainMaterial_25), value);
	}

	inline static int32_t get_offset_of_rainExplosionMaterial_26() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___rainExplosionMaterial_26)); }
	inline Material_t340375123 * get_rainExplosionMaterial_26() const { return ___rainExplosionMaterial_26; }
	inline Material_t340375123 ** get_address_of_rainExplosionMaterial_26() { return &___rainExplosionMaterial_26; }
	inline void set_rainExplosionMaterial_26(Material_t340375123 * value)
	{
		___rainExplosionMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((&___rainExplosionMaterial_26), value);
	}

	inline static int32_t get_offset_of_rainMistMaterial_27() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___rainMistMaterial_27)); }
	inline Material_t340375123 * get_rainMistMaterial_27() const { return ___rainMistMaterial_27; }
	inline Material_t340375123 ** get_address_of_rainMistMaterial_27() { return &___rainMistMaterial_27; }
	inline void set_rainMistMaterial_27(Material_t340375123 * value)
	{
		___rainMistMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((&___rainMistMaterial_27), value);
	}

	inline static int32_t get_offset_of_lastRainIntensityValue_28() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___lastRainIntensityValue_28)); }
	inline float get_lastRainIntensityValue_28() const { return ___lastRainIntensityValue_28; }
	inline float* get_address_of_lastRainIntensityValue_28() { return &___lastRainIntensityValue_28; }
	inline void set_lastRainIntensityValue_28(float value)
	{
		___lastRainIntensityValue_28 = value;
	}

	inline static int32_t get_offset_of_nextWindTime_29() { return static_cast<int32_t>(offsetof(BaseRainScript_t980278882, ___nextWindTime_29)); }
	inline float get_nextWindTime_29() const { return ___nextWindTime_29; }
	inline float* get_address_of_nextWindTime_29() { return &___nextWindTime_29; }
	inline void set_nextWindTime_29(float value)
	{
		___nextWindTime_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAINSCRIPT_T980278882_H
#ifndef DEMOSCRIPT_T2536370086_H
#define DEMOSCRIPT_T2536370086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.RainMaker.DemoScript
struct  DemoScript_t2536370086  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRuby.RainMaker.RainScript DigitalRuby.RainMaker.DemoScript::RainScript
	RainScript_t4186780704 * ___RainScript_4;
	// UnityEngine.UI.Toggle DigitalRuby.RainMaker.DemoScript::MouseLookToggle
	Toggle_t2735377061 * ___MouseLookToggle_5;
	// UnityEngine.UI.Toggle DigitalRuby.RainMaker.DemoScript::FlashlightToggle
	Toggle_t2735377061 * ___FlashlightToggle_6;
	// UnityEngine.UI.Slider DigitalRuby.RainMaker.DemoScript::RainSlider
	Slider_t3903728902 * ___RainSlider_7;
	// UnityEngine.Light DigitalRuby.RainMaker.DemoScript::Flashlight
	Light_t3756812086 * ___Flashlight_8;
	// UnityEngine.GameObject DigitalRuby.RainMaker.DemoScript::Sun
	GameObject_t1113636619 * ___Sun_9;
	// DigitalRuby.RainMaker.DemoScript/RotationAxes DigitalRuby.RainMaker.DemoScript::axes
	int32_t ___axes_10;
	// System.Single DigitalRuby.RainMaker.DemoScript::sensitivityX
	float ___sensitivityX_11;
	// System.Single DigitalRuby.RainMaker.DemoScript::sensitivityY
	float ___sensitivityY_12;
	// System.Single DigitalRuby.RainMaker.DemoScript::minimumX
	float ___minimumX_13;
	// System.Single DigitalRuby.RainMaker.DemoScript::maximumX
	float ___maximumX_14;
	// System.Single DigitalRuby.RainMaker.DemoScript::minimumY
	float ___minimumY_15;
	// System.Single DigitalRuby.RainMaker.DemoScript::maximumY
	float ___maximumY_16;
	// System.Single DigitalRuby.RainMaker.DemoScript::rotationX
	float ___rotationX_17;
	// System.Single DigitalRuby.RainMaker.DemoScript::rotationY
	float ___rotationY_18;
	// UnityEngine.Quaternion DigitalRuby.RainMaker.DemoScript::originalRotation
	Quaternion_t2301928331  ___originalRotation_19;

public:
	inline static int32_t get_offset_of_RainScript_4() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___RainScript_4)); }
	inline RainScript_t4186780704 * get_RainScript_4() const { return ___RainScript_4; }
	inline RainScript_t4186780704 ** get_address_of_RainScript_4() { return &___RainScript_4; }
	inline void set_RainScript_4(RainScript_t4186780704 * value)
	{
		___RainScript_4 = value;
		Il2CppCodeGenWriteBarrier((&___RainScript_4), value);
	}

	inline static int32_t get_offset_of_MouseLookToggle_5() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___MouseLookToggle_5)); }
	inline Toggle_t2735377061 * get_MouseLookToggle_5() const { return ___MouseLookToggle_5; }
	inline Toggle_t2735377061 ** get_address_of_MouseLookToggle_5() { return &___MouseLookToggle_5; }
	inline void set_MouseLookToggle_5(Toggle_t2735377061 * value)
	{
		___MouseLookToggle_5 = value;
		Il2CppCodeGenWriteBarrier((&___MouseLookToggle_5), value);
	}

	inline static int32_t get_offset_of_FlashlightToggle_6() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___FlashlightToggle_6)); }
	inline Toggle_t2735377061 * get_FlashlightToggle_6() const { return ___FlashlightToggle_6; }
	inline Toggle_t2735377061 ** get_address_of_FlashlightToggle_6() { return &___FlashlightToggle_6; }
	inline void set_FlashlightToggle_6(Toggle_t2735377061 * value)
	{
		___FlashlightToggle_6 = value;
		Il2CppCodeGenWriteBarrier((&___FlashlightToggle_6), value);
	}

	inline static int32_t get_offset_of_RainSlider_7() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___RainSlider_7)); }
	inline Slider_t3903728902 * get_RainSlider_7() const { return ___RainSlider_7; }
	inline Slider_t3903728902 ** get_address_of_RainSlider_7() { return &___RainSlider_7; }
	inline void set_RainSlider_7(Slider_t3903728902 * value)
	{
		___RainSlider_7 = value;
		Il2CppCodeGenWriteBarrier((&___RainSlider_7), value);
	}

	inline static int32_t get_offset_of_Flashlight_8() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___Flashlight_8)); }
	inline Light_t3756812086 * get_Flashlight_8() const { return ___Flashlight_8; }
	inline Light_t3756812086 ** get_address_of_Flashlight_8() { return &___Flashlight_8; }
	inline void set_Flashlight_8(Light_t3756812086 * value)
	{
		___Flashlight_8 = value;
		Il2CppCodeGenWriteBarrier((&___Flashlight_8), value);
	}

	inline static int32_t get_offset_of_Sun_9() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___Sun_9)); }
	inline GameObject_t1113636619 * get_Sun_9() const { return ___Sun_9; }
	inline GameObject_t1113636619 ** get_address_of_Sun_9() { return &___Sun_9; }
	inline void set_Sun_9(GameObject_t1113636619 * value)
	{
		___Sun_9 = value;
		Il2CppCodeGenWriteBarrier((&___Sun_9), value);
	}

	inline static int32_t get_offset_of_axes_10() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___axes_10)); }
	inline int32_t get_axes_10() const { return ___axes_10; }
	inline int32_t* get_address_of_axes_10() { return &___axes_10; }
	inline void set_axes_10(int32_t value)
	{
		___axes_10 = value;
	}

	inline static int32_t get_offset_of_sensitivityX_11() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___sensitivityX_11)); }
	inline float get_sensitivityX_11() const { return ___sensitivityX_11; }
	inline float* get_address_of_sensitivityX_11() { return &___sensitivityX_11; }
	inline void set_sensitivityX_11(float value)
	{
		___sensitivityX_11 = value;
	}

	inline static int32_t get_offset_of_sensitivityY_12() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___sensitivityY_12)); }
	inline float get_sensitivityY_12() const { return ___sensitivityY_12; }
	inline float* get_address_of_sensitivityY_12() { return &___sensitivityY_12; }
	inline void set_sensitivityY_12(float value)
	{
		___sensitivityY_12 = value;
	}

	inline static int32_t get_offset_of_minimumX_13() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___minimumX_13)); }
	inline float get_minimumX_13() const { return ___minimumX_13; }
	inline float* get_address_of_minimumX_13() { return &___minimumX_13; }
	inline void set_minimumX_13(float value)
	{
		___minimumX_13 = value;
	}

	inline static int32_t get_offset_of_maximumX_14() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___maximumX_14)); }
	inline float get_maximumX_14() const { return ___maximumX_14; }
	inline float* get_address_of_maximumX_14() { return &___maximumX_14; }
	inline void set_maximumX_14(float value)
	{
		___maximumX_14 = value;
	}

	inline static int32_t get_offset_of_minimumY_15() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___minimumY_15)); }
	inline float get_minimumY_15() const { return ___minimumY_15; }
	inline float* get_address_of_minimumY_15() { return &___minimumY_15; }
	inline void set_minimumY_15(float value)
	{
		___minimumY_15 = value;
	}

	inline static int32_t get_offset_of_maximumY_16() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___maximumY_16)); }
	inline float get_maximumY_16() const { return ___maximumY_16; }
	inline float* get_address_of_maximumY_16() { return &___maximumY_16; }
	inline void set_maximumY_16(float value)
	{
		___maximumY_16 = value;
	}

	inline static int32_t get_offset_of_rotationX_17() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___rotationX_17)); }
	inline float get_rotationX_17() const { return ___rotationX_17; }
	inline float* get_address_of_rotationX_17() { return &___rotationX_17; }
	inline void set_rotationX_17(float value)
	{
		___rotationX_17 = value;
	}

	inline static int32_t get_offset_of_rotationY_18() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___rotationY_18)); }
	inline float get_rotationY_18() const { return ___rotationY_18; }
	inline float* get_address_of_rotationY_18() { return &___rotationY_18; }
	inline void set_rotationY_18(float value)
	{
		___rotationY_18 = value;
	}

	inline static int32_t get_offset_of_originalRotation_19() { return static_cast<int32_t>(offsetof(DemoScript_t2536370086, ___originalRotation_19)); }
	inline Quaternion_t2301928331  get_originalRotation_19() const { return ___originalRotation_19; }
	inline Quaternion_t2301928331 * get_address_of_originalRotation_19() { return &___originalRotation_19; }
	inline void set_originalRotation_19(Quaternion_t2301928331  value)
	{
		___originalRotation_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPT_T2536370086_H
#ifndef DEMOSCRIPT2D_T1883634098_H
#define DEMOSCRIPT2D_T1883634098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.RainMaker.DemoScript2D
struct  DemoScript2D_t1883634098  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider DigitalRuby.RainMaker.DemoScript2D::RainSlider
	Slider_t3903728902 * ___RainSlider_4;
	// DigitalRuby.RainMaker.RainScript2D DigitalRuby.RainMaker.DemoScript2D::RainScript
	RainScript2D_t3497865708 * ___RainScript_5;

public:
	inline static int32_t get_offset_of_RainSlider_4() { return static_cast<int32_t>(offsetof(DemoScript2D_t1883634098, ___RainSlider_4)); }
	inline Slider_t3903728902 * get_RainSlider_4() const { return ___RainSlider_4; }
	inline Slider_t3903728902 ** get_address_of_RainSlider_4() { return &___RainSlider_4; }
	inline void set_RainSlider_4(Slider_t3903728902 * value)
	{
		___RainSlider_4 = value;
		Il2CppCodeGenWriteBarrier((&___RainSlider_4), value);
	}

	inline static int32_t get_offset_of_RainScript_5() { return static_cast<int32_t>(offsetof(DemoScript2D_t1883634098, ___RainScript_5)); }
	inline RainScript2D_t3497865708 * get_RainScript_5() const { return ___RainScript_5; }
	inline RainScript2D_t3497865708 ** get_address_of_RainScript_5() { return &___RainScript_5; }
	inline void set_RainScript_5(RainScript2D_t3497865708 * value)
	{
		___RainScript_5 = value;
		Il2CppCodeGenWriteBarrier((&___RainScript_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPT2D_T1883634098_H
#ifndef DEMOSCRIPTSTARTRAINONSPACEBAR_T3473520399_H
#define DEMOSCRIPTSTARTRAINONSPACEBAR_T3473520399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.RainMaker.DemoScriptStartRainOnSpaceBar
struct  DemoScriptStartRainOnSpaceBar_t3473520399  : public MonoBehaviour_t3962482529
{
public:
	// DigitalRuby.RainMaker.BaseRainScript DigitalRuby.RainMaker.DemoScriptStartRainOnSpaceBar::RainScript
	BaseRainScript_t980278882 * ___RainScript_4;

public:
	inline static int32_t get_offset_of_RainScript_4() { return static_cast<int32_t>(offsetof(DemoScriptStartRainOnSpaceBar_t3473520399, ___RainScript_4)); }
	inline BaseRainScript_t980278882 * get_RainScript_4() const { return ___RainScript_4; }
	inline BaseRainScript_t980278882 ** get_address_of_RainScript_4() { return &___RainScript_4; }
	inline void set_RainScript_4(BaseRainScript_t980278882 * value)
	{
		___RainScript_4 = value;
		Il2CppCodeGenWriteBarrier((&___RainScript_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPTSTARTRAINONSPACEBAR_T3473520399_H
#ifndef RAINCOLLISION_T1762856611_H
#define RAINCOLLISION_T1762856611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.RainMaker.RainCollision
struct  RainCollision_t1762856611  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent> DigitalRuby.RainMaker.RainCollision::collisionEvents
	List_1_t1232140387 * ___collisionEvents_5;
	// UnityEngine.ParticleSystem DigitalRuby.RainMaker.RainCollision::RainExplosion
	ParticleSystem_t1800779281 * ___RainExplosion_6;
	// UnityEngine.ParticleSystem DigitalRuby.RainMaker.RainCollision::RainParticleSystem
	ParticleSystem_t1800779281 * ___RainParticleSystem_7;

public:
	inline static int32_t get_offset_of_collisionEvents_5() { return static_cast<int32_t>(offsetof(RainCollision_t1762856611, ___collisionEvents_5)); }
	inline List_1_t1232140387 * get_collisionEvents_5() const { return ___collisionEvents_5; }
	inline List_1_t1232140387 ** get_address_of_collisionEvents_5() { return &___collisionEvents_5; }
	inline void set_collisionEvents_5(List_1_t1232140387 * value)
	{
		___collisionEvents_5 = value;
		Il2CppCodeGenWriteBarrier((&___collisionEvents_5), value);
	}

	inline static int32_t get_offset_of_RainExplosion_6() { return static_cast<int32_t>(offsetof(RainCollision_t1762856611, ___RainExplosion_6)); }
	inline ParticleSystem_t1800779281 * get_RainExplosion_6() const { return ___RainExplosion_6; }
	inline ParticleSystem_t1800779281 ** get_address_of_RainExplosion_6() { return &___RainExplosion_6; }
	inline void set_RainExplosion_6(ParticleSystem_t1800779281 * value)
	{
		___RainExplosion_6 = value;
		Il2CppCodeGenWriteBarrier((&___RainExplosion_6), value);
	}

	inline static int32_t get_offset_of_RainParticleSystem_7() { return static_cast<int32_t>(offsetof(RainCollision_t1762856611, ___RainParticleSystem_7)); }
	inline ParticleSystem_t1800779281 * get_RainParticleSystem_7() const { return ___RainParticleSystem_7; }
	inline ParticleSystem_t1800779281 ** get_address_of_RainParticleSystem_7() { return &___RainParticleSystem_7; }
	inline void set_RainParticleSystem_7(ParticleSystem_t1800779281 * value)
	{
		___RainParticleSystem_7 = value;
		Il2CppCodeGenWriteBarrier((&___RainParticleSystem_7), value);
	}
};

struct RainCollision_t1762856611_StaticFields
{
public:
	// UnityEngine.Color32 DigitalRuby.RainMaker.RainCollision::color
	Color32_t2600501292  ___color_4;

public:
	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(RainCollision_t1762856611_StaticFields, ___color_4)); }
	inline Color32_t2600501292  get_color_4() const { return ___color_4; }
	inline Color32_t2600501292 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t2600501292  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAINCOLLISION_T1762856611_H
#ifndef EVENTSYSTEMCHECKER_T1882757729_H
#define EVENTSYSTEMCHECKER_T1882757729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventSystemChecker
struct  EventSystemChecker_t1882757729  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEMCHECKER_T1882757729_H
#ifndef FORCEDRESET_T301124368_H
#define FORCEDRESET_T301124368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForcedReset
struct  ForcedReset_t301124368  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEDRESET_T301124368_H
#ifndef IKHANDS_T3058586594_H
#define IKHANDS_T3058586594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IKHands
struct  IKHands_t3058586594  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform IKHands::leftHandObj
	Transform_t3600365921 * ___leftHandObj_4;
	// UnityEngine.Transform IKHands::rightHandObj
	Transform_t3600365921 * ___rightHandObj_5;
	// UnityEngine.Transform IKHands::attachLeft
	Transform_t3600365921 * ___attachLeft_6;
	// UnityEngine.Transform IKHands::attachRight
	Transform_t3600365921 * ___attachRight_7;
	// System.Single IKHands::leftHandPositionWeight
	float ___leftHandPositionWeight_8;
	// System.Single IKHands::leftHandRotationWeight
	float ___leftHandRotationWeight_9;
	// System.Single IKHands::rightHandPositionWeight
	float ___rightHandPositionWeight_10;
	// System.Single IKHands::rightHandRotationWeight
	float ___rightHandRotationWeight_11;
	// UnityEngine.Animator IKHands::animator
	Animator_t434523843 * ___animator_12;

public:
	inline static int32_t get_offset_of_leftHandObj_4() { return static_cast<int32_t>(offsetof(IKHands_t3058586594, ___leftHandObj_4)); }
	inline Transform_t3600365921 * get_leftHandObj_4() const { return ___leftHandObj_4; }
	inline Transform_t3600365921 ** get_address_of_leftHandObj_4() { return &___leftHandObj_4; }
	inline void set_leftHandObj_4(Transform_t3600365921 * value)
	{
		___leftHandObj_4 = value;
		Il2CppCodeGenWriteBarrier((&___leftHandObj_4), value);
	}

	inline static int32_t get_offset_of_rightHandObj_5() { return static_cast<int32_t>(offsetof(IKHands_t3058586594, ___rightHandObj_5)); }
	inline Transform_t3600365921 * get_rightHandObj_5() const { return ___rightHandObj_5; }
	inline Transform_t3600365921 ** get_address_of_rightHandObj_5() { return &___rightHandObj_5; }
	inline void set_rightHandObj_5(Transform_t3600365921 * value)
	{
		___rightHandObj_5 = value;
		Il2CppCodeGenWriteBarrier((&___rightHandObj_5), value);
	}

	inline static int32_t get_offset_of_attachLeft_6() { return static_cast<int32_t>(offsetof(IKHands_t3058586594, ___attachLeft_6)); }
	inline Transform_t3600365921 * get_attachLeft_6() const { return ___attachLeft_6; }
	inline Transform_t3600365921 ** get_address_of_attachLeft_6() { return &___attachLeft_6; }
	inline void set_attachLeft_6(Transform_t3600365921 * value)
	{
		___attachLeft_6 = value;
		Il2CppCodeGenWriteBarrier((&___attachLeft_6), value);
	}

	inline static int32_t get_offset_of_attachRight_7() { return static_cast<int32_t>(offsetof(IKHands_t3058586594, ___attachRight_7)); }
	inline Transform_t3600365921 * get_attachRight_7() const { return ___attachRight_7; }
	inline Transform_t3600365921 ** get_address_of_attachRight_7() { return &___attachRight_7; }
	inline void set_attachRight_7(Transform_t3600365921 * value)
	{
		___attachRight_7 = value;
		Il2CppCodeGenWriteBarrier((&___attachRight_7), value);
	}

	inline static int32_t get_offset_of_leftHandPositionWeight_8() { return static_cast<int32_t>(offsetof(IKHands_t3058586594, ___leftHandPositionWeight_8)); }
	inline float get_leftHandPositionWeight_8() const { return ___leftHandPositionWeight_8; }
	inline float* get_address_of_leftHandPositionWeight_8() { return &___leftHandPositionWeight_8; }
	inline void set_leftHandPositionWeight_8(float value)
	{
		___leftHandPositionWeight_8 = value;
	}

	inline static int32_t get_offset_of_leftHandRotationWeight_9() { return static_cast<int32_t>(offsetof(IKHands_t3058586594, ___leftHandRotationWeight_9)); }
	inline float get_leftHandRotationWeight_9() const { return ___leftHandRotationWeight_9; }
	inline float* get_address_of_leftHandRotationWeight_9() { return &___leftHandRotationWeight_9; }
	inline void set_leftHandRotationWeight_9(float value)
	{
		___leftHandRotationWeight_9 = value;
	}

	inline static int32_t get_offset_of_rightHandPositionWeight_10() { return static_cast<int32_t>(offsetof(IKHands_t3058586594, ___rightHandPositionWeight_10)); }
	inline float get_rightHandPositionWeight_10() const { return ___rightHandPositionWeight_10; }
	inline float* get_address_of_rightHandPositionWeight_10() { return &___rightHandPositionWeight_10; }
	inline void set_rightHandPositionWeight_10(float value)
	{
		___rightHandPositionWeight_10 = value;
	}

	inline static int32_t get_offset_of_rightHandRotationWeight_11() { return static_cast<int32_t>(offsetof(IKHands_t3058586594, ___rightHandRotationWeight_11)); }
	inline float get_rightHandRotationWeight_11() const { return ___rightHandRotationWeight_11; }
	inline float* get_address_of_rightHandRotationWeight_11() { return &___rightHandRotationWeight_11; }
	inline void set_rightHandRotationWeight_11(float value)
	{
		___rightHandRotationWeight_11 = value;
	}

	inline static int32_t get_offset_of_animator_12() { return static_cast<int32_t>(offsetof(IKHands_t3058586594, ___animator_12)); }
	inline Animator_t434523843 * get_animator_12() const { return ___animator_12; }
	inline Animator_t434523843 ** get_address_of_animator_12() { return &___animator_12; }
	inline void set_animator_12(Animator_t434523843 * value)
	{
		___animator_12 = value;
		Il2CppCodeGenWriteBarrier((&___animator_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKHANDS_T3058586594_H
#ifndef GUICONTROLS_T1241352961_H
#define GUICONTROLS_T1241352961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.GUIControls
struct  GUIControls_t1241352961  : public MonoBehaviour_t3962482529
{
public:
	// RPGCharacterAnims.RPGCharacterControllerFREE RPGCharacterAnims.GUIControls::rpgCharacterController
	RPGCharacterControllerFREE_t3229142430 * ___rpgCharacterController_4;
	// RPGCharacterAnims.RPGCharacterMovementController RPGCharacterAnims.GUIControls::rpgCharacterMovementController
	RPGCharacterMovementController_t3656691424 * ___rpgCharacterMovementController_5;
	// System.Boolean RPGCharacterAnims.GUIControls::useNavAgent
	bool ___useNavAgent_6;

public:
	inline static int32_t get_offset_of_rpgCharacterController_4() { return static_cast<int32_t>(offsetof(GUIControls_t1241352961, ___rpgCharacterController_4)); }
	inline RPGCharacterControllerFREE_t3229142430 * get_rpgCharacterController_4() const { return ___rpgCharacterController_4; }
	inline RPGCharacterControllerFREE_t3229142430 ** get_address_of_rpgCharacterController_4() { return &___rpgCharacterController_4; }
	inline void set_rpgCharacterController_4(RPGCharacterControllerFREE_t3229142430 * value)
	{
		___rpgCharacterController_4 = value;
		Il2CppCodeGenWriteBarrier((&___rpgCharacterController_4), value);
	}

	inline static int32_t get_offset_of_rpgCharacterMovementController_5() { return static_cast<int32_t>(offsetof(GUIControls_t1241352961, ___rpgCharacterMovementController_5)); }
	inline RPGCharacterMovementController_t3656691424 * get_rpgCharacterMovementController_5() const { return ___rpgCharacterMovementController_5; }
	inline RPGCharacterMovementController_t3656691424 ** get_address_of_rpgCharacterMovementController_5() { return &___rpgCharacterMovementController_5; }
	inline void set_rpgCharacterMovementController_5(RPGCharacterMovementController_t3656691424 * value)
	{
		___rpgCharacterMovementController_5 = value;
		Il2CppCodeGenWriteBarrier((&___rpgCharacterMovementController_5), value);
	}

	inline static int32_t get_offset_of_useNavAgent_6() { return static_cast<int32_t>(offsetof(GUIControls_t1241352961, ___useNavAgent_6)); }
	inline bool get_useNavAgent_6() const { return ___useNavAgent_6; }
	inline bool* get_address_of_useNavAgent_6() { return &___useNavAgent_6; }
	inline void set_useNavAgent_6(bool value)
	{
		___useNavAgent_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUICONTROLS_T1241352961_H
#ifndef RPGCHARACTERCONTROLLERFREE_T3229142430_H
#define RPGCHARACTERCONTROLLERFREE_T3229142430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGCharacterControllerFREE
struct  RPGCharacterControllerFREE_t3229142430  : public MonoBehaviour_t3962482529
{
public:
	// RPGCharacterAnims.RPGCharacterMovementController RPGCharacterAnims.RPGCharacterControllerFREE::rpgCharacterMovementController
	RPGCharacterMovementController_t3656691424 * ___rpgCharacterMovementController_4;
	// RPGCharacterAnims.RPGCharacterInputController RPGCharacterAnims.RPGCharacterControllerFREE::rpgCharacterInputController
	RPGCharacterInputController_t2111465464 * ___rpgCharacterInputController_5;
	// UnityEngine.Animator RPGCharacterAnims.RPGCharacterControllerFREE::animator
	Animator_t434523843 * ___animator_6;
	// UnityEngine.GameObject RPGCharacterAnims.RPGCharacterControllerFREE::target
	GameObject_t1113636619 * ___target_7;
	// System.Boolean RPGCharacterAnims.RPGCharacterControllerFREE::isDead
	bool ___isDead_8;
	// System.Boolean RPGCharacterAnims.RPGCharacterControllerFREE::canAction
	bool ___canAction_9;
	// System.Boolean RPGCharacterAnims.RPGCharacterControllerFREE::isStrafing
	bool ___isStrafing_10;

public:
	inline static int32_t get_offset_of_rpgCharacterMovementController_4() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3229142430, ___rpgCharacterMovementController_4)); }
	inline RPGCharacterMovementController_t3656691424 * get_rpgCharacterMovementController_4() const { return ___rpgCharacterMovementController_4; }
	inline RPGCharacterMovementController_t3656691424 ** get_address_of_rpgCharacterMovementController_4() { return &___rpgCharacterMovementController_4; }
	inline void set_rpgCharacterMovementController_4(RPGCharacterMovementController_t3656691424 * value)
	{
		___rpgCharacterMovementController_4 = value;
		Il2CppCodeGenWriteBarrier((&___rpgCharacterMovementController_4), value);
	}

	inline static int32_t get_offset_of_rpgCharacterInputController_5() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3229142430, ___rpgCharacterInputController_5)); }
	inline RPGCharacterInputController_t2111465464 * get_rpgCharacterInputController_5() const { return ___rpgCharacterInputController_5; }
	inline RPGCharacterInputController_t2111465464 ** get_address_of_rpgCharacterInputController_5() { return &___rpgCharacterInputController_5; }
	inline void set_rpgCharacterInputController_5(RPGCharacterInputController_t2111465464 * value)
	{
		___rpgCharacterInputController_5 = value;
		Il2CppCodeGenWriteBarrier((&___rpgCharacterInputController_5), value);
	}

	inline static int32_t get_offset_of_animator_6() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3229142430, ___animator_6)); }
	inline Animator_t434523843 * get_animator_6() const { return ___animator_6; }
	inline Animator_t434523843 ** get_address_of_animator_6() { return &___animator_6; }
	inline void set_animator_6(Animator_t434523843 * value)
	{
		___animator_6 = value;
		Il2CppCodeGenWriteBarrier((&___animator_6), value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3229142430, ___target_7)); }
	inline GameObject_t1113636619 * get_target_7() const { return ___target_7; }
	inline GameObject_t1113636619 ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(GameObject_t1113636619 * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}

	inline static int32_t get_offset_of_isDead_8() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3229142430, ___isDead_8)); }
	inline bool get_isDead_8() const { return ___isDead_8; }
	inline bool* get_address_of_isDead_8() { return &___isDead_8; }
	inline void set_isDead_8(bool value)
	{
		___isDead_8 = value;
	}

	inline static int32_t get_offset_of_canAction_9() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3229142430, ___canAction_9)); }
	inline bool get_canAction_9() const { return ___canAction_9; }
	inline bool* get_address_of_canAction_9() { return &___canAction_9; }
	inline void set_canAction_9(bool value)
	{
		___canAction_9 = value;
	}

	inline static int32_t get_offset_of_isStrafing_10() { return static_cast<int32_t>(offsetof(RPGCharacterControllerFREE_t3229142430, ___isStrafing_10)); }
	inline bool get_isStrafing_10() const { return ___isStrafing_10; }
	inline bool* get_address_of_isStrafing_10() { return &___isStrafing_10; }
	inline void set_isStrafing_10(bool value)
	{
		___isStrafing_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPGCHARACTERCONTROLLERFREE_T3229142430_H
#ifndef RPGCHARACTERINPUTCONTROLLER_T2111465464_H
#define RPGCHARACTERINPUTCONTROLLER_T2111465464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGCharacterInputController
struct  RPGCharacterInputController_t2111465464  : public MonoBehaviour_t3962482529
{
public:
	// RPGCharacterAnims.RPGInput RPGCharacterAnims.RPGCharacterInputController::current
	RPGInput_t667815195  ___current_4;
	// System.Boolean RPGCharacterAnims.RPGCharacterInputController::inputJump
	bool ___inputJump_5;
	// System.Boolean RPGCharacterAnims.RPGCharacterInputController::inputLightHit
	bool ___inputLightHit_6;
	// System.Boolean RPGCharacterAnims.RPGCharacterInputController::inputDeath
	bool ___inputDeath_7;
	// System.Boolean RPGCharacterAnims.RPGCharacterInputController::inputAttackL
	bool ___inputAttackL_8;
	// System.Boolean RPGCharacterAnims.RPGCharacterInputController::inputAttackR
	bool ___inputAttackR_9;
	// System.Boolean RPGCharacterAnims.RPGCharacterInputController::inputStrafe
	bool ___inputStrafe_10;
	// System.Single RPGCharacterAnims.RPGCharacterInputController::inputTargetBlock
	float ___inputTargetBlock_11;
	// System.Single RPGCharacterAnims.RPGCharacterInputController::inputAimVertical
	float ___inputAimVertical_12;
	// System.Single RPGCharacterAnims.RPGCharacterInputController::inputAimHorizontal
	float ___inputAimHorizontal_13;
	// System.Single RPGCharacterAnims.RPGCharacterInputController::inputHorizontal
	float ___inputHorizontal_14;
	// System.Single RPGCharacterAnims.RPGCharacterInputController::inputVertical
	float ___inputVertical_15;
	// System.Boolean RPGCharacterAnims.RPGCharacterInputController::inputAiming
	bool ___inputAiming_16;
	// System.Boolean RPGCharacterAnims.RPGCharacterInputController::inputRoll
	bool ___inputRoll_17;

public:
	inline static int32_t get_offset_of_current_4() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___current_4)); }
	inline RPGInput_t667815195  get_current_4() const { return ___current_4; }
	inline RPGInput_t667815195 * get_address_of_current_4() { return &___current_4; }
	inline void set_current_4(RPGInput_t667815195  value)
	{
		___current_4 = value;
	}

	inline static int32_t get_offset_of_inputJump_5() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputJump_5)); }
	inline bool get_inputJump_5() const { return ___inputJump_5; }
	inline bool* get_address_of_inputJump_5() { return &___inputJump_5; }
	inline void set_inputJump_5(bool value)
	{
		___inputJump_5 = value;
	}

	inline static int32_t get_offset_of_inputLightHit_6() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputLightHit_6)); }
	inline bool get_inputLightHit_6() const { return ___inputLightHit_6; }
	inline bool* get_address_of_inputLightHit_6() { return &___inputLightHit_6; }
	inline void set_inputLightHit_6(bool value)
	{
		___inputLightHit_6 = value;
	}

	inline static int32_t get_offset_of_inputDeath_7() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputDeath_7)); }
	inline bool get_inputDeath_7() const { return ___inputDeath_7; }
	inline bool* get_address_of_inputDeath_7() { return &___inputDeath_7; }
	inline void set_inputDeath_7(bool value)
	{
		___inputDeath_7 = value;
	}

	inline static int32_t get_offset_of_inputAttackL_8() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputAttackL_8)); }
	inline bool get_inputAttackL_8() const { return ___inputAttackL_8; }
	inline bool* get_address_of_inputAttackL_8() { return &___inputAttackL_8; }
	inline void set_inputAttackL_8(bool value)
	{
		___inputAttackL_8 = value;
	}

	inline static int32_t get_offset_of_inputAttackR_9() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputAttackR_9)); }
	inline bool get_inputAttackR_9() const { return ___inputAttackR_9; }
	inline bool* get_address_of_inputAttackR_9() { return &___inputAttackR_9; }
	inline void set_inputAttackR_9(bool value)
	{
		___inputAttackR_9 = value;
	}

	inline static int32_t get_offset_of_inputStrafe_10() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputStrafe_10)); }
	inline bool get_inputStrafe_10() const { return ___inputStrafe_10; }
	inline bool* get_address_of_inputStrafe_10() { return &___inputStrafe_10; }
	inline void set_inputStrafe_10(bool value)
	{
		___inputStrafe_10 = value;
	}

	inline static int32_t get_offset_of_inputTargetBlock_11() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputTargetBlock_11)); }
	inline float get_inputTargetBlock_11() const { return ___inputTargetBlock_11; }
	inline float* get_address_of_inputTargetBlock_11() { return &___inputTargetBlock_11; }
	inline void set_inputTargetBlock_11(float value)
	{
		___inputTargetBlock_11 = value;
	}

	inline static int32_t get_offset_of_inputAimVertical_12() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputAimVertical_12)); }
	inline float get_inputAimVertical_12() const { return ___inputAimVertical_12; }
	inline float* get_address_of_inputAimVertical_12() { return &___inputAimVertical_12; }
	inline void set_inputAimVertical_12(float value)
	{
		___inputAimVertical_12 = value;
	}

	inline static int32_t get_offset_of_inputAimHorizontal_13() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputAimHorizontal_13)); }
	inline float get_inputAimHorizontal_13() const { return ___inputAimHorizontal_13; }
	inline float* get_address_of_inputAimHorizontal_13() { return &___inputAimHorizontal_13; }
	inline void set_inputAimHorizontal_13(float value)
	{
		___inputAimHorizontal_13 = value;
	}

	inline static int32_t get_offset_of_inputHorizontal_14() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputHorizontal_14)); }
	inline float get_inputHorizontal_14() const { return ___inputHorizontal_14; }
	inline float* get_address_of_inputHorizontal_14() { return &___inputHorizontal_14; }
	inline void set_inputHorizontal_14(float value)
	{
		___inputHorizontal_14 = value;
	}

	inline static int32_t get_offset_of_inputVertical_15() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputVertical_15)); }
	inline float get_inputVertical_15() const { return ___inputVertical_15; }
	inline float* get_address_of_inputVertical_15() { return &___inputVertical_15; }
	inline void set_inputVertical_15(float value)
	{
		___inputVertical_15 = value;
	}

	inline static int32_t get_offset_of_inputAiming_16() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputAiming_16)); }
	inline bool get_inputAiming_16() const { return ___inputAiming_16; }
	inline bool* get_address_of_inputAiming_16() { return &___inputAiming_16; }
	inline void set_inputAiming_16(bool value)
	{
		___inputAiming_16 = value;
	}

	inline static int32_t get_offset_of_inputRoll_17() { return static_cast<int32_t>(offsetof(RPGCharacterInputController_t2111465464, ___inputRoll_17)); }
	inline bool get_inputRoll_17() const { return ___inputRoll_17; }
	inline bool* get_address_of_inputRoll_17() { return &___inputRoll_17; }
	inline void set_inputRoll_17(bool value)
	{
		___inputRoll_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPGCHARACTERINPUTCONTROLLER_T2111465464_H
#ifndef SMOOTHFOLLOW_T2070928547_H
#define SMOOTHFOLLOW_T2070928547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothFollow
struct  SmoothFollow_t2070928547  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SmoothFollow::LockX
	bool ___LockX_5;
	// System.Single SmoothFollow::offSetZ
	float ___offSetZ_6;
	// System.Boolean SmoothFollow::LockY
	bool ___LockY_7;
	// System.Boolean SmoothFollow::LockZ
	bool ___LockZ_8;
	// System.Boolean SmoothFollow::useSmoothing
	bool ___useSmoothing_9;
	// UnityEngine.Transform SmoothFollow::target
	Transform_t3600365921 * ___target_10;
	// UnityEngine.GameObject SmoothFollow::hudElements
	GameObject_t1113636619 * ___hudElements_11;
	// UnityEngine.Transform SmoothFollow::thisTransform
	Transform_t3600365921 * ___thisTransform_12;
	// UnityEngine.Vector3 SmoothFollow::velocity
	Vector3_t3722313464  ___velocity_13;
	// System.Boolean SmoothFollow::hudActive
	bool ___hudActive_14;

public:
	inline static int32_t get_offset_of_LockX_5() { return static_cast<int32_t>(offsetof(SmoothFollow_t2070928547, ___LockX_5)); }
	inline bool get_LockX_5() const { return ___LockX_5; }
	inline bool* get_address_of_LockX_5() { return &___LockX_5; }
	inline void set_LockX_5(bool value)
	{
		___LockX_5 = value;
	}

	inline static int32_t get_offset_of_offSetZ_6() { return static_cast<int32_t>(offsetof(SmoothFollow_t2070928547, ___offSetZ_6)); }
	inline float get_offSetZ_6() const { return ___offSetZ_6; }
	inline float* get_address_of_offSetZ_6() { return &___offSetZ_6; }
	inline void set_offSetZ_6(float value)
	{
		___offSetZ_6 = value;
	}

	inline static int32_t get_offset_of_LockY_7() { return static_cast<int32_t>(offsetof(SmoothFollow_t2070928547, ___LockY_7)); }
	inline bool get_LockY_7() const { return ___LockY_7; }
	inline bool* get_address_of_LockY_7() { return &___LockY_7; }
	inline void set_LockY_7(bool value)
	{
		___LockY_7 = value;
	}

	inline static int32_t get_offset_of_LockZ_8() { return static_cast<int32_t>(offsetof(SmoothFollow_t2070928547, ___LockZ_8)); }
	inline bool get_LockZ_8() const { return ___LockZ_8; }
	inline bool* get_address_of_LockZ_8() { return &___LockZ_8; }
	inline void set_LockZ_8(bool value)
	{
		___LockZ_8 = value;
	}

	inline static int32_t get_offset_of_useSmoothing_9() { return static_cast<int32_t>(offsetof(SmoothFollow_t2070928547, ___useSmoothing_9)); }
	inline bool get_useSmoothing_9() const { return ___useSmoothing_9; }
	inline bool* get_address_of_useSmoothing_9() { return &___useSmoothing_9; }
	inline void set_useSmoothing_9(bool value)
	{
		___useSmoothing_9 = value;
	}

	inline static int32_t get_offset_of_target_10() { return static_cast<int32_t>(offsetof(SmoothFollow_t2070928547, ___target_10)); }
	inline Transform_t3600365921 * get_target_10() const { return ___target_10; }
	inline Transform_t3600365921 ** get_address_of_target_10() { return &___target_10; }
	inline void set_target_10(Transform_t3600365921 * value)
	{
		___target_10 = value;
		Il2CppCodeGenWriteBarrier((&___target_10), value);
	}

	inline static int32_t get_offset_of_hudElements_11() { return static_cast<int32_t>(offsetof(SmoothFollow_t2070928547, ___hudElements_11)); }
	inline GameObject_t1113636619 * get_hudElements_11() const { return ___hudElements_11; }
	inline GameObject_t1113636619 ** get_address_of_hudElements_11() { return &___hudElements_11; }
	inline void set_hudElements_11(GameObject_t1113636619 * value)
	{
		___hudElements_11 = value;
		Il2CppCodeGenWriteBarrier((&___hudElements_11), value);
	}

	inline static int32_t get_offset_of_thisTransform_12() { return static_cast<int32_t>(offsetof(SmoothFollow_t2070928547, ___thisTransform_12)); }
	inline Transform_t3600365921 * get_thisTransform_12() const { return ___thisTransform_12; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_12() { return &___thisTransform_12; }
	inline void set_thisTransform_12(Transform_t3600365921 * value)
	{
		___thisTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_12), value);
	}

	inline static int32_t get_offset_of_velocity_13() { return static_cast<int32_t>(offsetof(SmoothFollow_t2070928547, ___velocity_13)); }
	inline Vector3_t3722313464  get_velocity_13() const { return ___velocity_13; }
	inline Vector3_t3722313464 * get_address_of_velocity_13() { return &___velocity_13; }
	inline void set_velocity_13(Vector3_t3722313464  value)
	{
		___velocity_13 = value;
	}

	inline static int32_t get_offset_of_hudActive_14() { return static_cast<int32_t>(offsetof(SmoothFollow_t2070928547, ___hudActive_14)); }
	inline bool get_hudActive_14() const { return ___hudActive_14; }
	inline bool* get_address_of_hudActive_14() { return &___hudActive_14; }
	inline void set_hudActive_14(bool value)
	{
		___hudActive_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW_T2070928547_H
#ifndef SUPERSTATEMACHINE_T3068594726_H
#define SUPERSTATEMACHINE_T3068594726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuperStateMachine
struct  SuperStateMachine_t3068594726  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SuperStateMachine::timeEnteredState
	float ___timeEnteredState_4;
	// SuperStateMachine/State SuperStateMachine::state
	State_t3931175374 * ___state_5;
	// System.Enum SuperStateMachine::lastState
	Enum_t4135868527 * ___lastState_6;
	// System.Collections.Generic.Dictionary`2<System.Enum,System.Collections.Generic.Dictionary`2<System.String,System.Delegate>> SuperStateMachine::_cache
	Dictionary_2_t74011037 * ____cache_7;

public:
	inline static int32_t get_offset_of_timeEnteredState_4() { return static_cast<int32_t>(offsetof(SuperStateMachine_t3068594726, ___timeEnteredState_4)); }
	inline float get_timeEnteredState_4() const { return ___timeEnteredState_4; }
	inline float* get_address_of_timeEnteredState_4() { return &___timeEnteredState_4; }
	inline void set_timeEnteredState_4(float value)
	{
		___timeEnteredState_4 = value;
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(SuperStateMachine_t3068594726, ___state_5)); }
	inline State_t3931175374 * get_state_5() const { return ___state_5; }
	inline State_t3931175374 ** get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(State_t3931175374 * value)
	{
		___state_5 = value;
		Il2CppCodeGenWriteBarrier((&___state_5), value);
	}

	inline static int32_t get_offset_of_lastState_6() { return static_cast<int32_t>(offsetof(SuperStateMachine_t3068594726, ___lastState_6)); }
	inline Enum_t4135868527 * get_lastState_6() const { return ___lastState_6; }
	inline Enum_t4135868527 ** get_address_of_lastState_6() { return &___lastState_6; }
	inline void set_lastState_6(Enum_t4135868527 * value)
	{
		___lastState_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastState_6), value);
	}

	inline static int32_t get_offset_of__cache_7() { return static_cast<int32_t>(offsetof(SuperStateMachine_t3068594726, ____cache_7)); }
	inline Dictionary_2_t74011037 * get__cache_7() const { return ____cache_7; }
	inline Dictionary_2_t74011037 ** get_address_of__cache_7() { return &____cache_7; }
	inline void set__cache_7(Dictionary_2_t74011037 * value)
	{
		____cache_7 = value;
		Il2CppCodeGenWriteBarrier((&____cache_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPERSTATEMACHINE_T3068594726_H
#ifndef RAINSCRIPT_T4186780704_H
#define RAINSCRIPT_T4186780704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.RainMaker.RainScript
struct  RainScript_t4186780704  : public BaseRainScript_t980278882
{
public:
	// System.Single DigitalRuby.RainMaker.RainScript::RainHeight
	float ___RainHeight_30;
	// System.Single DigitalRuby.RainMaker.RainScript::RainForwardOffset
	float ___RainForwardOffset_31;
	// System.Single DigitalRuby.RainMaker.RainScript::RainMistHeight
	float ___RainMistHeight_32;

public:
	inline static int32_t get_offset_of_RainHeight_30() { return static_cast<int32_t>(offsetof(RainScript_t4186780704, ___RainHeight_30)); }
	inline float get_RainHeight_30() const { return ___RainHeight_30; }
	inline float* get_address_of_RainHeight_30() { return &___RainHeight_30; }
	inline void set_RainHeight_30(float value)
	{
		___RainHeight_30 = value;
	}

	inline static int32_t get_offset_of_RainForwardOffset_31() { return static_cast<int32_t>(offsetof(RainScript_t4186780704, ___RainForwardOffset_31)); }
	inline float get_RainForwardOffset_31() const { return ___RainForwardOffset_31; }
	inline float* get_address_of_RainForwardOffset_31() { return &___RainForwardOffset_31; }
	inline void set_RainForwardOffset_31(float value)
	{
		___RainForwardOffset_31 = value;
	}

	inline static int32_t get_offset_of_RainMistHeight_32() { return static_cast<int32_t>(offsetof(RainScript_t4186780704, ___RainMistHeight_32)); }
	inline float get_RainMistHeight_32() const { return ___RainMistHeight_32; }
	inline float* get_address_of_RainMistHeight_32() { return &___RainMistHeight_32; }
	inline void set_RainMistHeight_32(float value)
	{
		___RainMistHeight_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAINSCRIPT_T4186780704_H
#ifndef RAINSCRIPT2D_T3497865708_H
#define RAINSCRIPT2D_T3497865708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DigitalRuby.RainMaker.RainScript2D
struct  RainScript2D_t3497865708  : public BaseRainScript_t980278882
{
public:
	// System.Single DigitalRuby.RainMaker.RainScript2D::cameraMultiplier
	float ___cameraMultiplier_31;
	// UnityEngine.Bounds DigitalRuby.RainMaker.RainScript2D::visibleBounds
	Bounds_t2266837910  ___visibleBounds_32;
	// System.Single DigitalRuby.RainMaker.RainScript2D::yOffset
	float ___yOffset_33;
	// System.Single DigitalRuby.RainMaker.RainScript2D::visibleWorldWidth
	float ___visibleWorldWidth_34;
	// System.Single DigitalRuby.RainMaker.RainScript2D::initialEmissionRain
	float ___initialEmissionRain_35;
	// UnityEngine.Vector2 DigitalRuby.RainMaker.RainScript2D::initialStartSpeedRain
	Vector2_t2156229523  ___initialStartSpeedRain_36;
	// UnityEngine.Vector2 DigitalRuby.RainMaker.RainScript2D::initialStartSizeRain
	Vector2_t2156229523  ___initialStartSizeRain_37;
	// UnityEngine.Vector2 DigitalRuby.RainMaker.RainScript2D::initialStartSpeedMist
	Vector2_t2156229523  ___initialStartSpeedMist_38;
	// UnityEngine.Vector2 DigitalRuby.RainMaker.RainScript2D::initialStartSizeMist
	Vector2_t2156229523  ___initialStartSizeMist_39;
	// UnityEngine.Vector2 DigitalRuby.RainMaker.RainScript2D::initialStartSpeedExplosion
	Vector2_t2156229523  ___initialStartSpeedExplosion_40;
	// UnityEngine.Vector2 DigitalRuby.RainMaker.RainScript2D::initialStartSizeExplosion
	Vector2_t2156229523  ___initialStartSizeExplosion_41;
	// UnityEngine.ParticleSystem/Particle[] DigitalRuby.RainMaker.RainScript2D::particles
	ParticleU5BU5D_t3069227754* ___particles_42;
	// System.Single DigitalRuby.RainMaker.RainScript2D::RainHeightMultiplier
	float ___RainHeightMultiplier_43;
	// System.Single DigitalRuby.RainMaker.RainScript2D::RainWidthMultiplier
	float ___RainWidthMultiplier_44;
	// UnityEngine.LayerMask DigitalRuby.RainMaker.RainScript2D::CollisionMask
	LayerMask_t3493934918  ___CollisionMask_45;
	// System.Single DigitalRuby.RainMaker.RainScript2D::CollisionLifeTimeRain
	float ___CollisionLifeTimeRain_46;
	// System.Single DigitalRuby.RainMaker.RainScript2D::RainMistCollisionMultiplier
	float ___RainMistCollisionMultiplier_47;

public:
	inline static int32_t get_offset_of_cameraMultiplier_31() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___cameraMultiplier_31)); }
	inline float get_cameraMultiplier_31() const { return ___cameraMultiplier_31; }
	inline float* get_address_of_cameraMultiplier_31() { return &___cameraMultiplier_31; }
	inline void set_cameraMultiplier_31(float value)
	{
		___cameraMultiplier_31 = value;
	}

	inline static int32_t get_offset_of_visibleBounds_32() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___visibleBounds_32)); }
	inline Bounds_t2266837910  get_visibleBounds_32() const { return ___visibleBounds_32; }
	inline Bounds_t2266837910 * get_address_of_visibleBounds_32() { return &___visibleBounds_32; }
	inline void set_visibleBounds_32(Bounds_t2266837910  value)
	{
		___visibleBounds_32 = value;
	}

	inline static int32_t get_offset_of_yOffset_33() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___yOffset_33)); }
	inline float get_yOffset_33() const { return ___yOffset_33; }
	inline float* get_address_of_yOffset_33() { return &___yOffset_33; }
	inline void set_yOffset_33(float value)
	{
		___yOffset_33 = value;
	}

	inline static int32_t get_offset_of_visibleWorldWidth_34() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___visibleWorldWidth_34)); }
	inline float get_visibleWorldWidth_34() const { return ___visibleWorldWidth_34; }
	inline float* get_address_of_visibleWorldWidth_34() { return &___visibleWorldWidth_34; }
	inline void set_visibleWorldWidth_34(float value)
	{
		___visibleWorldWidth_34 = value;
	}

	inline static int32_t get_offset_of_initialEmissionRain_35() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___initialEmissionRain_35)); }
	inline float get_initialEmissionRain_35() const { return ___initialEmissionRain_35; }
	inline float* get_address_of_initialEmissionRain_35() { return &___initialEmissionRain_35; }
	inline void set_initialEmissionRain_35(float value)
	{
		___initialEmissionRain_35 = value;
	}

	inline static int32_t get_offset_of_initialStartSpeedRain_36() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___initialStartSpeedRain_36)); }
	inline Vector2_t2156229523  get_initialStartSpeedRain_36() const { return ___initialStartSpeedRain_36; }
	inline Vector2_t2156229523 * get_address_of_initialStartSpeedRain_36() { return &___initialStartSpeedRain_36; }
	inline void set_initialStartSpeedRain_36(Vector2_t2156229523  value)
	{
		___initialStartSpeedRain_36 = value;
	}

	inline static int32_t get_offset_of_initialStartSizeRain_37() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___initialStartSizeRain_37)); }
	inline Vector2_t2156229523  get_initialStartSizeRain_37() const { return ___initialStartSizeRain_37; }
	inline Vector2_t2156229523 * get_address_of_initialStartSizeRain_37() { return &___initialStartSizeRain_37; }
	inline void set_initialStartSizeRain_37(Vector2_t2156229523  value)
	{
		___initialStartSizeRain_37 = value;
	}

	inline static int32_t get_offset_of_initialStartSpeedMist_38() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___initialStartSpeedMist_38)); }
	inline Vector2_t2156229523  get_initialStartSpeedMist_38() const { return ___initialStartSpeedMist_38; }
	inline Vector2_t2156229523 * get_address_of_initialStartSpeedMist_38() { return &___initialStartSpeedMist_38; }
	inline void set_initialStartSpeedMist_38(Vector2_t2156229523  value)
	{
		___initialStartSpeedMist_38 = value;
	}

	inline static int32_t get_offset_of_initialStartSizeMist_39() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___initialStartSizeMist_39)); }
	inline Vector2_t2156229523  get_initialStartSizeMist_39() const { return ___initialStartSizeMist_39; }
	inline Vector2_t2156229523 * get_address_of_initialStartSizeMist_39() { return &___initialStartSizeMist_39; }
	inline void set_initialStartSizeMist_39(Vector2_t2156229523  value)
	{
		___initialStartSizeMist_39 = value;
	}

	inline static int32_t get_offset_of_initialStartSpeedExplosion_40() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___initialStartSpeedExplosion_40)); }
	inline Vector2_t2156229523  get_initialStartSpeedExplosion_40() const { return ___initialStartSpeedExplosion_40; }
	inline Vector2_t2156229523 * get_address_of_initialStartSpeedExplosion_40() { return &___initialStartSpeedExplosion_40; }
	inline void set_initialStartSpeedExplosion_40(Vector2_t2156229523  value)
	{
		___initialStartSpeedExplosion_40 = value;
	}

	inline static int32_t get_offset_of_initialStartSizeExplosion_41() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___initialStartSizeExplosion_41)); }
	inline Vector2_t2156229523  get_initialStartSizeExplosion_41() const { return ___initialStartSizeExplosion_41; }
	inline Vector2_t2156229523 * get_address_of_initialStartSizeExplosion_41() { return &___initialStartSizeExplosion_41; }
	inline void set_initialStartSizeExplosion_41(Vector2_t2156229523  value)
	{
		___initialStartSizeExplosion_41 = value;
	}

	inline static int32_t get_offset_of_particles_42() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___particles_42)); }
	inline ParticleU5BU5D_t3069227754* get_particles_42() const { return ___particles_42; }
	inline ParticleU5BU5D_t3069227754** get_address_of_particles_42() { return &___particles_42; }
	inline void set_particles_42(ParticleU5BU5D_t3069227754* value)
	{
		___particles_42 = value;
		Il2CppCodeGenWriteBarrier((&___particles_42), value);
	}

	inline static int32_t get_offset_of_RainHeightMultiplier_43() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___RainHeightMultiplier_43)); }
	inline float get_RainHeightMultiplier_43() const { return ___RainHeightMultiplier_43; }
	inline float* get_address_of_RainHeightMultiplier_43() { return &___RainHeightMultiplier_43; }
	inline void set_RainHeightMultiplier_43(float value)
	{
		___RainHeightMultiplier_43 = value;
	}

	inline static int32_t get_offset_of_RainWidthMultiplier_44() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___RainWidthMultiplier_44)); }
	inline float get_RainWidthMultiplier_44() const { return ___RainWidthMultiplier_44; }
	inline float* get_address_of_RainWidthMultiplier_44() { return &___RainWidthMultiplier_44; }
	inline void set_RainWidthMultiplier_44(float value)
	{
		___RainWidthMultiplier_44 = value;
	}

	inline static int32_t get_offset_of_CollisionMask_45() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___CollisionMask_45)); }
	inline LayerMask_t3493934918  get_CollisionMask_45() const { return ___CollisionMask_45; }
	inline LayerMask_t3493934918 * get_address_of_CollisionMask_45() { return &___CollisionMask_45; }
	inline void set_CollisionMask_45(LayerMask_t3493934918  value)
	{
		___CollisionMask_45 = value;
	}

	inline static int32_t get_offset_of_CollisionLifeTimeRain_46() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___CollisionLifeTimeRain_46)); }
	inline float get_CollisionLifeTimeRain_46() const { return ___CollisionLifeTimeRain_46; }
	inline float* get_address_of_CollisionLifeTimeRain_46() { return &___CollisionLifeTimeRain_46; }
	inline void set_CollisionLifeTimeRain_46(float value)
	{
		___CollisionLifeTimeRain_46 = value;
	}

	inline static int32_t get_offset_of_RainMistCollisionMultiplier_47() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708, ___RainMistCollisionMultiplier_47)); }
	inline float get_RainMistCollisionMultiplier_47() const { return ___RainMistCollisionMultiplier_47; }
	inline float* get_address_of_RainMistCollisionMultiplier_47() { return &___RainMistCollisionMultiplier_47; }
	inline void set_RainMistCollisionMultiplier_47(float value)
	{
		___RainMistCollisionMultiplier_47 = value;
	}
};

struct RainScript2D_t3497865708_StaticFields
{
public:
	// UnityEngine.Color32 DigitalRuby.RainMaker.RainScript2D::explosionColor
	Color32_t2600501292  ___explosionColor_30;

public:
	inline static int32_t get_offset_of_explosionColor_30() { return static_cast<int32_t>(offsetof(RainScript2D_t3497865708_StaticFields, ___explosionColor_30)); }
	inline Color32_t2600501292  get_explosionColor_30() const { return ___explosionColor_30; }
	inline Color32_t2600501292 * get_address_of_explosionColor_30() { return &___explosionColor_30; }
	inline void set_explosionColor_30(Color32_t2600501292  value)
	{
		___explosionColor_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAINSCRIPT2D_T3497865708_H
#ifndef RPGCHARACTERMOVEMENTCONTROLLER_T3656691424_H
#define RPGCHARACTERMOVEMENTCONTROLLER_T3656691424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RPGCharacterAnims.RPGCharacterMovementController
struct  RPGCharacterMovementController_t3656691424  : public SuperStateMachine_t3068594726
{
public:
	// UnityEngine.AI.NavMeshAgent RPGCharacterAnims.RPGCharacterMovementController::navMeshAgent
	NavMeshAgent_t1276799816 * ___navMeshAgent_8;
	// SuperCharacterController RPGCharacterAnims.RPGCharacterMovementController::superCharacterController
	SuperCharacterController_t3568978942 * ___superCharacterController_9;
	// RPGCharacterAnims.RPGCharacterControllerFREE RPGCharacterAnims.RPGCharacterMovementController::rpgCharacterController
	RPGCharacterControllerFREE_t3229142430 * ___rpgCharacterController_10;
	// RPGCharacterAnims.RPGCharacterInputController RPGCharacterAnims.RPGCharacterMovementController::rpgCharacterInputController
	RPGCharacterInputController_t2111465464 * ___rpgCharacterInputController_11;
	// UnityEngine.Rigidbody RPGCharacterAnims.RPGCharacterMovementController::rb
	Rigidbody_t3916780224 * ___rb_12;
	// UnityEngine.Animator RPGCharacterAnims.RPGCharacterMovementController::animator
	Animator_t434523843 * ___animator_13;
	// RPGCharacterAnims.RPGCharacterState RPGCharacterAnims.RPGCharacterMovementController::rpgCharacterState
	int32_t ___rpgCharacterState_14;
	// System.Boolean RPGCharacterAnims.RPGCharacterMovementController::useMeshNav
	bool ___useMeshNav_15;
	// UnityEngine.Vector3 RPGCharacterAnims.RPGCharacterMovementController::<lookDirection>k__BackingField
	Vector3_t3722313464  ___U3ClookDirectionU3Ek__BackingField_16;
	// System.Boolean RPGCharacterAnims.RPGCharacterMovementController::isKnockback
	bool ___isKnockback_17;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::knockbackMultiplier
	float ___knockbackMultiplier_18;
	// System.Boolean RPGCharacterAnims.RPGCharacterMovementController::canJump
	bool ___canJump_19;
	// System.Boolean RPGCharacterAnims.RPGCharacterMovementController::canDoubleJump
	bool ___canDoubleJump_20;
	// System.Boolean RPGCharacterAnims.RPGCharacterMovementController::doublejumped
	bool ___doublejumped_21;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::gravity
	float ___gravity_22;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::jumpAcceleration
	float ___jumpAcceleration_23;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::jumpHeight
	float ___jumpHeight_24;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::doubleJumpHeight
	float ___doubleJumpHeight_25;
	// UnityEngine.Vector3 RPGCharacterAnims.RPGCharacterMovementController::currentVelocity
	Vector3_t3722313464  ___currentVelocity_26;
	// System.Boolean RPGCharacterAnims.RPGCharacterMovementController::isMoving
	bool ___isMoving_27;
	// System.Boolean RPGCharacterAnims.RPGCharacterMovementController::canMove
	bool ___canMove_28;
	// System.Boolean RPGCharacterAnims.RPGCharacterMovementController::crouch
	bool ___crouch_29;
	// System.Boolean RPGCharacterAnims.RPGCharacterMovementController::isSprinting
	bool ___isSprinting_30;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::movementAcceleration
	float ___movementAcceleration_31;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::walkSpeed
	float ___walkSpeed_32;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::runSpeed
	float ___runSpeed_33;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::sprintSpeed
	float ___sprintSpeed_34;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::rotationSpeed
	float ___rotationSpeed_35;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::groundFriction
	float ___groundFriction_36;
	// System.Boolean RPGCharacterAnims.RPGCharacterMovementController::isRolling
	bool ___isRolling_37;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::rollSpeed
	float ___rollSpeed_38;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::rollduration
	float ___rollduration_39;
	// System.Int32 RPGCharacterAnims.RPGCharacterMovementController::rollNumber
	int32_t ___rollNumber_40;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::inAirSpeed
	float ___inAirSpeed_41;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::swimSpeed
	float ___swimSpeed_42;
	// System.Single RPGCharacterAnims.RPGCharacterMovementController::waterFriction
	float ___waterFriction_43;

public:
	inline static int32_t get_offset_of_navMeshAgent_8() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___navMeshAgent_8)); }
	inline NavMeshAgent_t1276799816 * get_navMeshAgent_8() const { return ___navMeshAgent_8; }
	inline NavMeshAgent_t1276799816 ** get_address_of_navMeshAgent_8() { return &___navMeshAgent_8; }
	inline void set_navMeshAgent_8(NavMeshAgent_t1276799816 * value)
	{
		___navMeshAgent_8 = value;
		Il2CppCodeGenWriteBarrier((&___navMeshAgent_8), value);
	}

	inline static int32_t get_offset_of_superCharacterController_9() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___superCharacterController_9)); }
	inline SuperCharacterController_t3568978942 * get_superCharacterController_9() const { return ___superCharacterController_9; }
	inline SuperCharacterController_t3568978942 ** get_address_of_superCharacterController_9() { return &___superCharacterController_9; }
	inline void set_superCharacterController_9(SuperCharacterController_t3568978942 * value)
	{
		___superCharacterController_9 = value;
		Il2CppCodeGenWriteBarrier((&___superCharacterController_9), value);
	}

	inline static int32_t get_offset_of_rpgCharacterController_10() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___rpgCharacterController_10)); }
	inline RPGCharacterControllerFREE_t3229142430 * get_rpgCharacterController_10() const { return ___rpgCharacterController_10; }
	inline RPGCharacterControllerFREE_t3229142430 ** get_address_of_rpgCharacterController_10() { return &___rpgCharacterController_10; }
	inline void set_rpgCharacterController_10(RPGCharacterControllerFREE_t3229142430 * value)
	{
		___rpgCharacterController_10 = value;
		Il2CppCodeGenWriteBarrier((&___rpgCharacterController_10), value);
	}

	inline static int32_t get_offset_of_rpgCharacterInputController_11() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___rpgCharacterInputController_11)); }
	inline RPGCharacterInputController_t2111465464 * get_rpgCharacterInputController_11() const { return ___rpgCharacterInputController_11; }
	inline RPGCharacterInputController_t2111465464 ** get_address_of_rpgCharacterInputController_11() { return &___rpgCharacterInputController_11; }
	inline void set_rpgCharacterInputController_11(RPGCharacterInputController_t2111465464 * value)
	{
		___rpgCharacterInputController_11 = value;
		Il2CppCodeGenWriteBarrier((&___rpgCharacterInputController_11), value);
	}

	inline static int32_t get_offset_of_rb_12() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___rb_12)); }
	inline Rigidbody_t3916780224 * get_rb_12() const { return ___rb_12; }
	inline Rigidbody_t3916780224 ** get_address_of_rb_12() { return &___rb_12; }
	inline void set_rb_12(Rigidbody_t3916780224 * value)
	{
		___rb_12 = value;
		Il2CppCodeGenWriteBarrier((&___rb_12), value);
	}

	inline static int32_t get_offset_of_animator_13() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___animator_13)); }
	inline Animator_t434523843 * get_animator_13() const { return ___animator_13; }
	inline Animator_t434523843 ** get_address_of_animator_13() { return &___animator_13; }
	inline void set_animator_13(Animator_t434523843 * value)
	{
		___animator_13 = value;
		Il2CppCodeGenWriteBarrier((&___animator_13), value);
	}

	inline static int32_t get_offset_of_rpgCharacterState_14() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___rpgCharacterState_14)); }
	inline int32_t get_rpgCharacterState_14() const { return ___rpgCharacterState_14; }
	inline int32_t* get_address_of_rpgCharacterState_14() { return &___rpgCharacterState_14; }
	inline void set_rpgCharacterState_14(int32_t value)
	{
		___rpgCharacterState_14 = value;
	}

	inline static int32_t get_offset_of_useMeshNav_15() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___useMeshNav_15)); }
	inline bool get_useMeshNav_15() const { return ___useMeshNav_15; }
	inline bool* get_address_of_useMeshNav_15() { return &___useMeshNav_15; }
	inline void set_useMeshNav_15(bool value)
	{
		___useMeshNav_15 = value;
	}

	inline static int32_t get_offset_of_U3ClookDirectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___U3ClookDirectionU3Ek__BackingField_16)); }
	inline Vector3_t3722313464  get_U3ClookDirectionU3Ek__BackingField_16() const { return ___U3ClookDirectionU3Ek__BackingField_16; }
	inline Vector3_t3722313464 * get_address_of_U3ClookDirectionU3Ek__BackingField_16() { return &___U3ClookDirectionU3Ek__BackingField_16; }
	inline void set_U3ClookDirectionU3Ek__BackingField_16(Vector3_t3722313464  value)
	{
		___U3ClookDirectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_isKnockback_17() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___isKnockback_17)); }
	inline bool get_isKnockback_17() const { return ___isKnockback_17; }
	inline bool* get_address_of_isKnockback_17() { return &___isKnockback_17; }
	inline void set_isKnockback_17(bool value)
	{
		___isKnockback_17 = value;
	}

	inline static int32_t get_offset_of_knockbackMultiplier_18() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___knockbackMultiplier_18)); }
	inline float get_knockbackMultiplier_18() const { return ___knockbackMultiplier_18; }
	inline float* get_address_of_knockbackMultiplier_18() { return &___knockbackMultiplier_18; }
	inline void set_knockbackMultiplier_18(float value)
	{
		___knockbackMultiplier_18 = value;
	}

	inline static int32_t get_offset_of_canJump_19() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___canJump_19)); }
	inline bool get_canJump_19() const { return ___canJump_19; }
	inline bool* get_address_of_canJump_19() { return &___canJump_19; }
	inline void set_canJump_19(bool value)
	{
		___canJump_19 = value;
	}

	inline static int32_t get_offset_of_canDoubleJump_20() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___canDoubleJump_20)); }
	inline bool get_canDoubleJump_20() const { return ___canDoubleJump_20; }
	inline bool* get_address_of_canDoubleJump_20() { return &___canDoubleJump_20; }
	inline void set_canDoubleJump_20(bool value)
	{
		___canDoubleJump_20 = value;
	}

	inline static int32_t get_offset_of_doublejumped_21() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___doublejumped_21)); }
	inline bool get_doublejumped_21() const { return ___doublejumped_21; }
	inline bool* get_address_of_doublejumped_21() { return &___doublejumped_21; }
	inline void set_doublejumped_21(bool value)
	{
		___doublejumped_21 = value;
	}

	inline static int32_t get_offset_of_gravity_22() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___gravity_22)); }
	inline float get_gravity_22() const { return ___gravity_22; }
	inline float* get_address_of_gravity_22() { return &___gravity_22; }
	inline void set_gravity_22(float value)
	{
		___gravity_22 = value;
	}

	inline static int32_t get_offset_of_jumpAcceleration_23() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___jumpAcceleration_23)); }
	inline float get_jumpAcceleration_23() const { return ___jumpAcceleration_23; }
	inline float* get_address_of_jumpAcceleration_23() { return &___jumpAcceleration_23; }
	inline void set_jumpAcceleration_23(float value)
	{
		___jumpAcceleration_23 = value;
	}

	inline static int32_t get_offset_of_jumpHeight_24() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___jumpHeight_24)); }
	inline float get_jumpHeight_24() const { return ___jumpHeight_24; }
	inline float* get_address_of_jumpHeight_24() { return &___jumpHeight_24; }
	inline void set_jumpHeight_24(float value)
	{
		___jumpHeight_24 = value;
	}

	inline static int32_t get_offset_of_doubleJumpHeight_25() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___doubleJumpHeight_25)); }
	inline float get_doubleJumpHeight_25() const { return ___doubleJumpHeight_25; }
	inline float* get_address_of_doubleJumpHeight_25() { return &___doubleJumpHeight_25; }
	inline void set_doubleJumpHeight_25(float value)
	{
		___doubleJumpHeight_25 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_26() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___currentVelocity_26)); }
	inline Vector3_t3722313464  get_currentVelocity_26() const { return ___currentVelocity_26; }
	inline Vector3_t3722313464 * get_address_of_currentVelocity_26() { return &___currentVelocity_26; }
	inline void set_currentVelocity_26(Vector3_t3722313464  value)
	{
		___currentVelocity_26 = value;
	}

	inline static int32_t get_offset_of_isMoving_27() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___isMoving_27)); }
	inline bool get_isMoving_27() const { return ___isMoving_27; }
	inline bool* get_address_of_isMoving_27() { return &___isMoving_27; }
	inline void set_isMoving_27(bool value)
	{
		___isMoving_27 = value;
	}

	inline static int32_t get_offset_of_canMove_28() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___canMove_28)); }
	inline bool get_canMove_28() const { return ___canMove_28; }
	inline bool* get_address_of_canMove_28() { return &___canMove_28; }
	inline void set_canMove_28(bool value)
	{
		___canMove_28 = value;
	}

	inline static int32_t get_offset_of_crouch_29() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___crouch_29)); }
	inline bool get_crouch_29() const { return ___crouch_29; }
	inline bool* get_address_of_crouch_29() { return &___crouch_29; }
	inline void set_crouch_29(bool value)
	{
		___crouch_29 = value;
	}

	inline static int32_t get_offset_of_isSprinting_30() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___isSprinting_30)); }
	inline bool get_isSprinting_30() const { return ___isSprinting_30; }
	inline bool* get_address_of_isSprinting_30() { return &___isSprinting_30; }
	inline void set_isSprinting_30(bool value)
	{
		___isSprinting_30 = value;
	}

	inline static int32_t get_offset_of_movementAcceleration_31() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___movementAcceleration_31)); }
	inline float get_movementAcceleration_31() const { return ___movementAcceleration_31; }
	inline float* get_address_of_movementAcceleration_31() { return &___movementAcceleration_31; }
	inline void set_movementAcceleration_31(float value)
	{
		___movementAcceleration_31 = value;
	}

	inline static int32_t get_offset_of_walkSpeed_32() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___walkSpeed_32)); }
	inline float get_walkSpeed_32() const { return ___walkSpeed_32; }
	inline float* get_address_of_walkSpeed_32() { return &___walkSpeed_32; }
	inline void set_walkSpeed_32(float value)
	{
		___walkSpeed_32 = value;
	}

	inline static int32_t get_offset_of_runSpeed_33() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___runSpeed_33)); }
	inline float get_runSpeed_33() const { return ___runSpeed_33; }
	inline float* get_address_of_runSpeed_33() { return &___runSpeed_33; }
	inline void set_runSpeed_33(float value)
	{
		___runSpeed_33 = value;
	}

	inline static int32_t get_offset_of_sprintSpeed_34() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___sprintSpeed_34)); }
	inline float get_sprintSpeed_34() const { return ___sprintSpeed_34; }
	inline float* get_address_of_sprintSpeed_34() { return &___sprintSpeed_34; }
	inline void set_sprintSpeed_34(float value)
	{
		___sprintSpeed_34 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_35() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___rotationSpeed_35)); }
	inline float get_rotationSpeed_35() const { return ___rotationSpeed_35; }
	inline float* get_address_of_rotationSpeed_35() { return &___rotationSpeed_35; }
	inline void set_rotationSpeed_35(float value)
	{
		___rotationSpeed_35 = value;
	}

	inline static int32_t get_offset_of_groundFriction_36() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___groundFriction_36)); }
	inline float get_groundFriction_36() const { return ___groundFriction_36; }
	inline float* get_address_of_groundFriction_36() { return &___groundFriction_36; }
	inline void set_groundFriction_36(float value)
	{
		___groundFriction_36 = value;
	}

	inline static int32_t get_offset_of_isRolling_37() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___isRolling_37)); }
	inline bool get_isRolling_37() const { return ___isRolling_37; }
	inline bool* get_address_of_isRolling_37() { return &___isRolling_37; }
	inline void set_isRolling_37(bool value)
	{
		___isRolling_37 = value;
	}

	inline static int32_t get_offset_of_rollSpeed_38() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___rollSpeed_38)); }
	inline float get_rollSpeed_38() const { return ___rollSpeed_38; }
	inline float* get_address_of_rollSpeed_38() { return &___rollSpeed_38; }
	inline void set_rollSpeed_38(float value)
	{
		___rollSpeed_38 = value;
	}

	inline static int32_t get_offset_of_rollduration_39() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___rollduration_39)); }
	inline float get_rollduration_39() const { return ___rollduration_39; }
	inline float* get_address_of_rollduration_39() { return &___rollduration_39; }
	inline void set_rollduration_39(float value)
	{
		___rollduration_39 = value;
	}

	inline static int32_t get_offset_of_rollNumber_40() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___rollNumber_40)); }
	inline int32_t get_rollNumber_40() const { return ___rollNumber_40; }
	inline int32_t* get_address_of_rollNumber_40() { return &___rollNumber_40; }
	inline void set_rollNumber_40(int32_t value)
	{
		___rollNumber_40 = value;
	}

	inline static int32_t get_offset_of_inAirSpeed_41() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___inAirSpeed_41)); }
	inline float get_inAirSpeed_41() const { return ___inAirSpeed_41; }
	inline float* get_address_of_inAirSpeed_41() { return &___inAirSpeed_41; }
	inline void set_inAirSpeed_41(float value)
	{
		___inAirSpeed_41 = value;
	}

	inline static int32_t get_offset_of_swimSpeed_42() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___swimSpeed_42)); }
	inline float get_swimSpeed_42() const { return ___swimSpeed_42; }
	inline float* get_address_of_swimSpeed_42() { return &___swimSpeed_42; }
	inline void set_swimSpeed_42(float value)
	{
		___swimSpeed_42 = value;
	}

	inline static int32_t get_offset_of_waterFriction_43() { return static_cast<int32_t>(offsetof(RPGCharacterMovementController_t3656691424, ___waterFriction_43)); }
	inline float get_waterFriction_43() const { return ___waterFriction_43; }
	inline float* get_address_of_waterFriction_43() { return &___waterFriction_43; }
	inline void set_waterFriction_43(float value)
	{
		___waterFriction_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPGCHARACTERMOVEMENTCONTROLLER_T3656691424_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4300 = { sizeof (AlphaButtonClickMask_t141136539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4300[1] = 
{
	AlphaButtonClickMask_t141136539::get_offset_of__image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4301 = { sizeof (EventSystemChecker_t1882757729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4302 = { sizeof (ForcedReset_t301124368), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4303 = { sizeof (IKHands_t3058586594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4303[9] = 
{
	IKHands_t3058586594::get_offset_of_leftHandObj_4(),
	IKHands_t3058586594::get_offset_of_rightHandObj_5(),
	IKHands_t3058586594::get_offset_of_attachLeft_6(),
	IKHands_t3058586594::get_offset_of_attachRight_7(),
	IKHands_t3058586594::get_offset_of_leftHandPositionWeight_8(),
	IKHands_t3058586594::get_offset_of_leftHandRotationWeight_9(),
	IKHands_t3058586594::get_offset_of_rightHandPositionWeight_10(),
	IKHands_t3058586594::get_offset_of_rightHandRotationWeight_11(),
	IKHands_t3058586594::get_offset_of_animator_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4304 = { sizeof (SmoothFollow_t2070928547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4304[11] = 
{
	0,
	SmoothFollow_t2070928547::get_offset_of_LockX_5(),
	SmoothFollow_t2070928547::get_offset_of_offSetZ_6(),
	SmoothFollow_t2070928547::get_offset_of_LockY_7(),
	SmoothFollow_t2070928547::get_offset_of_LockZ_8(),
	SmoothFollow_t2070928547::get_offset_of_useSmoothing_9(),
	SmoothFollow_t2070928547::get_offset_of_target_10(),
	SmoothFollow_t2070928547::get_offset_of_hudElements_11(),
	SmoothFollow_t2070928547::get_offset_of_thisTransform_12(),
	SmoothFollow_t2070928547::get_offset_of_velocity_13(),
	SmoothFollow_t2070928547::get_offset_of_hudActive_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4305 = { sizeof (Readme_t3138389917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4305[4] = 
{
	Readme_t3138389917::get_offset_of_icon_4(),
	Readme_t3138389917::get_offset_of_title_5(),
	Readme_t3138389917::get_offset_of_sections_6(),
	Readme_t3138389917::get_offset_of_loadedLayout_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4306 = { sizeof (Section_t2694042830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4306[4] = 
{
	Section_t2694042830::get_offset_of_heading_0(),
	Section_t2694042830::get_offset_of_text_1(),
	Section_t2694042830::get_offset_of_linkText_2(),
	Section_t2694042830::get_offset_of_url_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4307 = { sizeof (GUIControls_t1241352961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4307[3] = 
{
	GUIControls_t1241352961::get_offset_of_rpgCharacterController_4(),
	GUIControls_t1241352961::get_offset_of_rpgCharacterMovementController_5(),
	GUIControls_t1241352961::get_offset_of_useNavAgent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4308 = { sizeof (RPGCharacterControllerFREE_t3229142430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4308[7] = 
{
	RPGCharacterControllerFREE_t3229142430::get_offset_of_rpgCharacterMovementController_4(),
	RPGCharacterControllerFREE_t3229142430::get_offset_of_rpgCharacterInputController_5(),
	RPGCharacterControllerFREE_t3229142430::get_offset_of_animator_6(),
	RPGCharacterControllerFREE_t3229142430::get_offset_of_target_7(),
	RPGCharacterControllerFREE_t3229142430::get_offset_of_isDead_8(),
	RPGCharacterControllerFREE_t3229142430::get_offset_of_canAction_9(),
	RPGCharacterControllerFREE_t3229142430::get_offset_of_isStrafing_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4309 = { sizeof (U3C_TurningU3Ed__9_t2034464105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4309[4] = 
{
	U3C_TurningU3Ed__9_t2034464105::get_offset_of_U3CU3E1__state_0(),
	U3C_TurningU3Ed__9_t2034464105::get_offset_of_U3CU3E2__current_1(),
	U3C_TurningU3Ed__9_t2034464105::get_offset_of_direction_2(),
	U3C_TurningU3Ed__9_t2034464105::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4310 = { sizeof (U3C_DodgeU3Ed__10_t2712610854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4310[4] = 
{
	U3C_DodgeU3Ed__10_t2712610854::get_offset_of_U3CU3E1__state_0(),
	U3C_DodgeU3Ed__10_t2712610854::get_offset_of_U3CU3E2__current_1(),
	U3C_DodgeU3Ed__10_t2712610854::get_offset_of_U3CU3E4__this_2(),
	U3C_DodgeU3Ed__10_t2712610854::get_offset_of_direction_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4311 = { sizeof (U3C_GetCurrentAnimationLengthU3Ed__26_t1576132943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4311[3] = 
{
	U3C_GetCurrentAnimationLengthU3Ed__26_t1576132943::get_offset_of_U3CU3E1__state_0(),
	U3C_GetCurrentAnimationLengthU3Ed__26_t1576132943::get_offset_of_U3CU3E2__current_1(),
	U3C_GetCurrentAnimationLengthU3Ed__26_t1576132943::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4312 = { sizeof (U3C_LockU3Ed__28_t9022957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4312[8] = 
{
	U3C_LockU3Ed__28_t9022957::get_offset_of_U3CU3E1__state_0(),
	U3C_LockU3Ed__28_t9022957::get_offset_of_U3CU3E2__current_1(),
	U3C_LockU3Ed__28_t9022957::get_offset_of_delayTime_2(),
	U3C_LockU3Ed__28_t9022957::get_offset_of_lockMovement_3(),
	U3C_LockU3Ed__28_t9022957::get_offset_of_U3CU3E4__this_4(),
	U3C_LockU3Ed__28_t9022957::get_offset_of_lockAction_5(),
	U3C_LockU3Ed__28_t9022957::get_offset_of_timed_6(),
	U3C_LockU3Ed__28_t9022957::get_offset_of_lockTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4313 = { sizeof (RPGCharacterInputController_t2111465464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4313[14] = 
{
	RPGCharacterInputController_t2111465464::get_offset_of_current_4(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputJump_5(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputLightHit_6(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputDeath_7(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputAttackL_8(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputAttackR_9(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputStrafe_10(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputTargetBlock_11(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputAimVertical_12(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputAimHorizontal_13(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputHorizontal_14(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputVertical_15(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputAiming_16(),
	RPGCharacterInputController_t2111465464::get_offset_of_inputRoll_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4314 = { sizeof (RPGInput_t667815195)+ sizeof (RuntimeObject), sizeof(RPGInput_t667815195_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4314[3] = 
{
	RPGInput_t667815195::get_offset_of_moveInput_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RPGInput_t667815195::get_offset_of_aimInput_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RPGInput_t667815195::get_offset_of_jumpInput_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4315 = { sizeof (RPGCharacterState_t1431373774)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4315[7] = 
{
	RPGCharacterState_t1431373774::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4316 = { sizeof (RPGCharacterMovementController_t3656691424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4316[36] = 
{
	RPGCharacterMovementController_t3656691424::get_offset_of_navMeshAgent_8(),
	RPGCharacterMovementController_t3656691424::get_offset_of_superCharacterController_9(),
	RPGCharacterMovementController_t3656691424::get_offset_of_rpgCharacterController_10(),
	RPGCharacterMovementController_t3656691424::get_offset_of_rpgCharacterInputController_11(),
	RPGCharacterMovementController_t3656691424::get_offset_of_rb_12(),
	RPGCharacterMovementController_t3656691424::get_offset_of_animator_13(),
	RPGCharacterMovementController_t3656691424::get_offset_of_rpgCharacterState_14(),
	RPGCharacterMovementController_t3656691424::get_offset_of_useMeshNav_15(),
	RPGCharacterMovementController_t3656691424::get_offset_of_U3ClookDirectionU3Ek__BackingField_16(),
	RPGCharacterMovementController_t3656691424::get_offset_of_isKnockback_17(),
	RPGCharacterMovementController_t3656691424::get_offset_of_knockbackMultiplier_18(),
	RPGCharacterMovementController_t3656691424::get_offset_of_canJump_19(),
	RPGCharacterMovementController_t3656691424::get_offset_of_canDoubleJump_20(),
	RPGCharacterMovementController_t3656691424::get_offset_of_doublejumped_21(),
	RPGCharacterMovementController_t3656691424::get_offset_of_gravity_22(),
	RPGCharacterMovementController_t3656691424::get_offset_of_jumpAcceleration_23(),
	RPGCharacterMovementController_t3656691424::get_offset_of_jumpHeight_24(),
	RPGCharacterMovementController_t3656691424::get_offset_of_doubleJumpHeight_25(),
	RPGCharacterMovementController_t3656691424::get_offset_of_currentVelocity_26(),
	RPGCharacterMovementController_t3656691424::get_offset_of_isMoving_27(),
	RPGCharacterMovementController_t3656691424::get_offset_of_canMove_28(),
	RPGCharacterMovementController_t3656691424::get_offset_of_crouch_29(),
	RPGCharacterMovementController_t3656691424::get_offset_of_isSprinting_30(),
	RPGCharacterMovementController_t3656691424::get_offset_of_movementAcceleration_31(),
	RPGCharacterMovementController_t3656691424::get_offset_of_walkSpeed_32(),
	RPGCharacterMovementController_t3656691424::get_offset_of_runSpeed_33(),
	RPGCharacterMovementController_t3656691424::get_offset_of_sprintSpeed_34(),
	RPGCharacterMovementController_t3656691424::get_offset_of_rotationSpeed_35(),
	RPGCharacterMovementController_t3656691424::get_offset_of_groundFriction_36(),
	RPGCharacterMovementController_t3656691424::get_offset_of_isRolling_37(),
	RPGCharacterMovementController_t3656691424::get_offset_of_rollSpeed_38(),
	RPGCharacterMovementController_t3656691424::get_offset_of_rollduration_39(),
	RPGCharacterMovementController_t3656691424::get_offset_of_rollNumber_40(),
	RPGCharacterMovementController_t3656691424::get_offset_of_inAirSpeed_41(),
	RPGCharacterMovementController_t3656691424::get_offset_of_swimSpeed_42(),
	RPGCharacterMovementController_t3656691424::get_offset_of_waterFriction_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4317 = { sizeof (U3C_RollU3Ed__60_t3065912839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4317[4] = 
{
	U3C_RollU3Ed__60_t3065912839::get_offset_of_U3CU3E1__state_0(),
	U3C_RollU3Ed__60_t3065912839::get_offset_of_U3CU3E2__current_1(),
	U3C_RollU3Ed__60_t3065912839::get_offset_of_U3CU3E4__this_2(),
	U3C_RollU3Ed__60_t3065912839::get_offset_of_roll_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4318 = { sizeof (U3C_KnockbackU3Ed__64_t3256490646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4318[6] = 
{
	U3C_KnockbackU3Ed__64_t3256490646::get_offset_of_U3CU3E1__state_0(),
	U3C_KnockbackU3Ed__64_t3256490646::get_offset_of_U3CU3E2__current_1(),
	U3C_KnockbackU3Ed__64_t3256490646::get_offset_of_U3CU3E4__this_2(),
	U3C_KnockbackU3Ed__64_t3256490646::get_offset_of_knockDirection_3(),
	U3C_KnockbackU3Ed__64_t3256490646::get_offset_of_knockBackAmount_4(),
	U3C_KnockbackU3Ed__64_t3256490646::get_offset_of_variableAmount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4319 = { sizeof (U3C_KnockbackForceU3Ed__65_t780478027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4319[6] = 
{
	U3C_KnockbackForceU3Ed__65_t780478027::get_offset_of_U3CU3E1__state_0(),
	U3C_KnockbackForceU3Ed__65_t780478027::get_offset_of_U3CU3E2__current_1(),
	U3C_KnockbackForceU3Ed__65_t780478027::get_offset_of_U3CU3E4__this_2(),
	U3C_KnockbackForceU3Ed__65_t780478027::get_offset_of_knockDirection_3(),
	U3C_KnockbackForceU3Ed__65_t780478027::get_offset_of_knockBackAmount_4(),
	U3C_KnockbackForceU3Ed__65_t780478027::get_offset_of_variableAmount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4320 = { sizeof (DemoScript_t2536370086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4320[16] = 
{
	DemoScript_t2536370086::get_offset_of_RainScript_4(),
	DemoScript_t2536370086::get_offset_of_MouseLookToggle_5(),
	DemoScript_t2536370086::get_offset_of_FlashlightToggle_6(),
	DemoScript_t2536370086::get_offset_of_RainSlider_7(),
	DemoScript_t2536370086::get_offset_of_Flashlight_8(),
	DemoScript_t2536370086::get_offset_of_Sun_9(),
	DemoScript_t2536370086::get_offset_of_axes_10(),
	DemoScript_t2536370086::get_offset_of_sensitivityX_11(),
	DemoScript_t2536370086::get_offset_of_sensitivityY_12(),
	DemoScript_t2536370086::get_offset_of_minimumX_13(),
	DemoScript_t2536370086::get_offset_of_maximumX_14(),
	DemoScript_t2536370086::get_offset_of_minimumY_15(),
	DemoScript_t2536370086::get_offset_of_maximumY_16(),
	DemoScript_t2536370086::get_offset_of_rotationX_17(),
	DemoScript_t2536370086::get_offset_of_rotationY_18(),
	DemoScript_t2536370086::get_offset_of_originalRotation_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4321 = { sizeof (RotationAxes_t1559384644)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4321[4] = 
{
	RotationAxes_t1559384644::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4322 = { sizeof (DemoScript2D_t1883634098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4322[2] = 
{
	DemoScript2D_t1883634098::get_offset_of_RainSlider_4(),
	DemoScript2D_t1883634098::get_offset_of_RainScript_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4323 = { sizeof (DemoScriptStartRainOnSpaceBar_t3473520399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4323[1] = 
{
	DemoScriptStartRainOnSpaceBar_t3473520399::get_offset_of_RainScript_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4324 = { sizeof (BaseRainScript_t980278882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4324[26] = 
{
	BaseRainScript_t980278882::get_offset_of_Camera_4(),
	BaseRainScript_t980278882::get_offset_of_FollowCamera_5(),
	BaseRainScript_t980278882::get_offset_of_RainSoundLight_6(),
	BaseRainScript_t980278882::get_offset_of_RainSoundMedium_7(),
	BaseRainScript_t980278882::get_offset_of_RainSoundHeavy_8(),
	BaseRainScript_t980278882::get_offset_of_RainIntensity_9(),
	BaseRainScript_t980278882::get_offset_of_RainFallParticleSystem_10(),
	BaseRainScript_t980278882::get_offset_of_RainExplosionParticleSystem_11(),
	BaseRainScript_t980278882::get_offset_of_RainMistParticleSystem_12(),
	BaseRainScript_t980278882::get_offset_of_RainMistThreshold_13(),
	BaseRainScript_t980278882::get_offset_of_WindSound_14(),
	BaseRainScript_t980278882::get_offset_of_WindSoundVolumeModifier_15(),
	BaseRainScript_t980278882::get_offset_of_WindZone_16(),
	BaseRainScript_t980278882::get_offset_of_WindSpeedRange_17(),
	BaseRainScript_t980278882::get_offset_of_WindChangeInterval_18(),
	BaseRainScript_t980278882::get_offset_of_EnableWind_19(),
	BaseRainScript_t980278882::get_offset_of_audioSourceRainLight_20(),
	BaseRainScript_t980278882::get_offset_of_audioSourceRainMedium_21(),
	BaseRainScript_t980278882::get_offset_of_audioSourceRainHeavy_22(),
	BaseRainScript_t980278882::get_offset_of_audioSourceRainCurrent_23(),
	BaseRainScript_t980278882::get_offset_of_audioSourceWind_24(),
	BaseRainScript_t980278882::get_offset_of_rainMaterial_25(),
	BaseRainScript_t980278882::get_offset_of_rainExplosionMaterial_26(),
	BaseRainScript_t980278882::get_offset_of_rainMistMaterial_27(),
	BaseRainScript_t980278882::get_offset_of_lastRainIntensityValue_28(),
	BaseRainScript_t980278882::get_offset_of_nextWindTime_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4325 = { sizeof (LoopingAudioSource_t2892973928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4325[2] = 
{
	LoopingAudioSource_t2892973928::get_offset_of_U3CAudioSourceU3Ek__BackingField_0(),
	LoopingAudioSource_t2892973928::get_offset_of_U3CTargetVolumeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4326 = { sizeof (RainCollision_t1762856611), -1, sizeof(RainCollision_t1762856611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4326[4] = 
{
	RainCollision_t1762856611_StaticFields::get_offset_of_color_4(),
	RainCollision_t1762856611::get_offset_of_collisionEvents_5(),
	RainCollision_t1762856611::get_offset_of_RainExplosion_6(),
	RainCollision_t1762856611::get_offset_of_RainParticleSystem_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4327 = { sizeof (RainScript_t4186780704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4327[3] = 
{
	RainScript_t4186780704::get_offset_of_RainHeight_30(),
	RainScript_t4186780704::get_offset_of_RainForwardOffset_31(),
	RainScript_t4186780704::get_offset_of_RainMistHeight_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4328 = { sizeof (RainScript2D_t3497865708), -1, sizeof(RainScript2D_t3497865708_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4328[18] = 
{
	RainScript2D_t3497865708_StaticFields::get_offset_of_explosionColor_30(),
	RainScript2D_t3497865708::get_offset_of_cameraMultiplier_31(),
	RainScript2D_t3497865708::get_offset_of_visibleBounds_32(),
	RainScript2D_t3497865708::get_offset_of_yOffset_33(),
	RainScript2D_t3497865708::get_offset_of_visibleWorldWidth_34(),
	RainScript2D_t3497865708::get_offset_of_initialEmissionRain_35(),
	RainScript2D_t3497865708::get_offset_of_initialStartSpeedRain_36(),
	RainScript2D_t3497865708::get_offset_of_initialStartSizeRain_37(),
	RainScript2D_t3497865708::get_offset_of_initialStartSpeedMist_38(),
	RainScript2D_t3497865708::get_offset_of_initialStartSizeMist_39(),
	RainScript2D_t3497865708::get_offset_of_initialStartSpeedExplosion_40(),
	RainScript2D_t3497865708::get_offset_of_initialStartSizeExplosion_41(),
	RainScript2D_t3497865708::get_offset_of_particles_42(),
	RainScript2D_t3497865708::get_offset_of_RainHeightMultiplier_43(),
	RainScript2D_t3497865708::get_offset_of_RainWidthMultiplier_44(),
	RainScript2D_t3497865708::get_offset_of_CollisionMask_45(),
	RainScript2D_t3497865708::get_offset_of_CollisionLifeTimeRain_46(),
	RainScript2D_t3497865708::get_offset_of_RainMistCollisionMultiplier_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4329 = { sizeof (GetSetAttribute_t1349027187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4329[2] = 
{
	GetSetAttribute_t1349027187::get_offset_of_name_0(),
	GetSetAttribute_t1349027187::get_offset_of_dirty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4330 = { sizeof (MinAttribute_t4172004135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4330[1] = 
{
	MinAttribute_t4172004135::get_offset_of_min_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4331 = { sizeof (TrackballAttribute_t219960417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4331[1] = 
{
	TrackballAttribute_t219960417::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4332 = { sizeof (TrackballGroupAttribute_t624107828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4333 = { sizeof (AmbientOcclusionComponent_t4130625043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4333[3] = 
{
	0,
	0,
	AmbientOcclusionComponent_t4130625043::get_offset_of_m_MRT_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4334 = { sizeof (Uniforms_t3797733410), -1, sizeof(Uniforms_t3797733410_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4334[10] = 
{
	Uniforms_t3797733410_StaticFields::get_offset_of__Intensity_0(),
	Uniforms_t3797733410_StaticFields::get_offset_of__Radius_1(),
	Uniforms_t3797733410_StaticFields::get_offset_of__FogParams_2(),
	Uniforms_t3797733410_StaticFields::get_offset_of__Downsample_3(),
	Uniforms_t3797733410_StaticFields::get_offset_of__SampleCount_4(),
	Uniforms_t3797733410_StaticFields::get_offset_of__OcclusionTexture1_5(),
	Uniforms_t3797733410_StaticFields::get_offset_of__OcclusionTexture2_6(),
	Uniforms_t3797733410_StaticFields::get_offset_of__OcclusionTexture_7(),
	Uniforms_t3797733410_StaticFields::get_offset_of__MainTex_8(),
	Uniforms_t3797733410_StaticFields::get_offset_of__TempRT_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4335 = { sizeof (OcclusionSource_t4221238007)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4335[4] = 
{
	OcclusionSource_t4221238007::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4336 = { sizeof (BloomComponent_t3791419130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4336[3] = 
{
	0,
	BloomComponent_t3791419130::get_offset_of_m_BlurBuffer1_3(),
	BloomComponent_t3791419130::get_offset_of_m_BlurBuffer2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4337 = { sizeof (Uniforms_t4164805197), -1, sizeof(Uniforms_t4164805197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4337[10] = 
{
	Uniforms_t4164805197_StaticFields::get_offset_of__AutoExposure_0(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Threshold_1(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Curve_2(),
	Uniforms_t4164805197_StaticFields::get_offset_of__PrefilterOffs_3(),
	Uniforms_t4164805197_StaticFields::get_offset_of__SampleScale_4(),
	Uniforms_t4164805197_StaticFields::get_offset_of__BaseTex_5(),
	Uniforms_t4164805197_StaticFields::get_offset_of__BloomTex_6(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Bloom_Settings_7(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Bloom_DirtTex_8(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Bloom_DirtIntensity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4338 = { sizeof (BuiltinDebugViewsComponent_t2123147871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4338[2] = 
{
	0,
	BuiltinDebugViewsComponent_t2123147871::get_offset_of_m_Arrows_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4339 = { sizeof (Uniforms_t2158582951), -1, sizeof(Uniforms_t2158582951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4339[7] = 
{
	Uniforms_t2158582951_StaticFields::get_offset_of__DepthScale_0(),
	Uniforms_t2158582951_StaticFields::get_offset_of__TempRT_1(),
	Uniforms_t2158582951_StaticFields::get_offset_of__Opacity_2(),
	Uniforms_t2158582951_StaticFields::get_offset_of__MainTex_3(),
	Uniforms_t2158582951_StaticFields::get_offset_of__TempRT2_4(),
	Uniforms_t2158582951_StaticFields::get_offset_of__Amplitude_5(),
	Uniforms_t2158582951_StaticFields::get_offset_of__Scale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4340 = { sizeof (Pass_t2117482)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4340[6] = 
{
	Pass_t2117482::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4341 = { sizeof (ArrowArray_t303178545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4341[3] = 
{
	ArrowArray_t303178545::get_offset_of_U3CmeshU3Ek__BackingField_0(),
	ArrowArray_t303178545::get_offset_of_U3CcolumnCountU3Ek__BackingField_1(),
	ArrowArray_t303178545::get_offset_of_U3CrowCountU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4342 = { sizeof (ChromaticAberrationComponent_t1647263118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4342[1] = 
{
	ChromaticAberrationComponent_t1647263118::get_offset_of_m_SpectrumLut_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4343 = { sizeof (Uniforms_t424361460), -1, sizeof(Uniforms_t424361460_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4343[2] = 
{
	Uniforms_t424361460_StaticFields::get_offset_of__ChromaticAberration_Amount_0(),
	Uniforms_t424361460_StaticFields::get_offset_of__ChromaticAberration_Spectrum_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4344 = { sizeof (ColorGradingComponent_t1715259467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4344[5] = 
{
	0,
	0,
	0,
	ColorGradingComponent_t1715259467::get_offset_of_m_GradingCurves_5(),
	ColorGradingComponent_t1715259467::get_offset_of_m_pixels_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4345 = { sizeof (Uniforms_t3075607151), -1, sizeof(Uniforms_t3075607151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4345[20] = 
{
	Uniforms_t3075607151_StaticFields::get_offset_of__LutParams_0(),
	Uniforms_t3075607151_StaticFields::get_offset_of__NeutralTonemapperParams1_1(),
	Uniforms_t3075607151_StaticFields::get_offset_of__NeutralTonemapperParams2_2(),
	Uniforms_t3075607151_StaticFields::get_offset_of__HueShift_3(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Saturation_4(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Contrast_5(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Balance_6(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Lift_7(),
	Uniforms_t3075607151_StaticFields::get_offset_of__InvGamma_8(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Gain_9(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Slope_10(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Power_11(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Offset_12(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ChannelMixerRed_13(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ChannelMixerGreen_14(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ChannelMixerBlue_15(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Curves_16(),
	Uniforms_t3075607151_StaticFields::get_offset_of__LogLut_17(),
	Uniforms_t3075607151_StaticFields::get_offset_of__LogLut_Params_18(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ExposureEV_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4346 = { sizeof (DepthOfFieldComponent_t554756766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4346[3] = 
{
	0,
	DepthOfFieldComponent_t554756766::get_offset_of_m_CoCHistory_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4347 = { sizeof (Uniforms_t3629868803), -1, sizeof(Uniforms_t3629868803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4347[11] = 
{
	Uniforms_t3629868803_StaticFields::get_offset_of__DepthOfFieldTex_0(),
	Uniforms_t3629868803_StaticFields::get_offset_of__DepthOfFieldCoCTex_1(),
	Uniforms_t3629868803_StaticFields::get_offset_of__Distance_2(),
	Uniforms_t3629868803_StaticFields::get_offset_of__LensCoeff_3(),
	Uniforms_t3629868803_StaticFields::get_offset_of__MaxCoC_4(),
	Uniforms_t3629868803_StaticFields::get_offset_of__RcpMaxCoC_5(),
	Uniforms_t3629868803_StaticFields::get_offset_of__RcpAspect_6(),
	Uniforms_t3629868803_StaticFields::get_offset_of__MainTex_7(),
	Uniforms_t3629868803_StaticFields::get_offset_of__CoCTex_8(),
	Uniforms_t3629868803_StaticFields::get_offset_of__TaaParams_9(),
	Uniforms_t3629868803_StaticFields::get_offset_of__DepthOfFieldParams_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4348 = { sizeof (DitheringComponent_t277621267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4348[3] = 
{
	DitheringComponent_t277621267::get_offset_of_noiseTextures_2(),
	DitheringComponent_t277621267::get_offset_of_textureIndex_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4349 = { sizeof (Uniforms_t3745258951), -1, sizeof(Uniforms_t3745258951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4349[2] = 
{
	Uniforms_t3745258951_StaticFields::get_offset_of__DitheringTex_0(),
	Uniforms_t3745258951_StaticFields::get_offset_of__DitheringCoords_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4350 = { sizeof (EyeAdaptationComponent_t3394805121), -1, sizeof(EyeAdaptationComponent_t3394805121_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4350[11] = 
{
	EyeAdaptationComponent_t3394805121::get_offset_of_m_EyeCompute_2(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_HistogramBuffer_3(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_AutoExposurePool_4(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_AutoExposurePingPing_5(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_CurrentAutoExposure_6(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_DebugHistogram_7(),
	EyeAdaptationComponent_t3394805121_StaticFields::get_offset_of_s_EmptyHistogramBuffer_8(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_FirstFrame_9(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4351 = { sizeof (Uniforms_t95810089), -1, sizeof(Uniforms_t95810089_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4351[6] = 
{
	Uniforms_t95810089_StaticFields::get_offset_of__Params_0(),
	Uniforms_t95810089_StaticFields::get_offset_of__Speed_1(),
	Uniforms_t95810089_StaticFields::get_offset_of__ScaleOffsetRes_2(),
	Uniforms_t95810089_StaticFields::get_offset_of__ExposureCompensation_3(),
	Uniforms_t95810089_StaticFields::get_offset_of__AutoExposure_4(),
	Uniforms_t95810089_StaticFields::get_offset_of__DebugWidth_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4352 = { sizeof (FogComponent_t3400726830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4352[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4353 = { sizeof (Uniforms_t459708260), -1, sizeof(Uniforms_t459708260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4353[5] = 
{
	Uniforms_t459708260_StaticFields::get_offset_of__FogColor_0(),
	Uniforms_t459708260_StaticFields::get_offset_of__Density_1(),
	Uniforms_t459708260_StaticFields::get_offset_of__Start_2(),
	Uniforms_t459708260_StaticFields::get_offset_of__End_3(),
	Uniforms_t459708260_StaticFields::get_offset_of__TempRT_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4354 = { sizeof (FxaaComponent_t1312385771), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4355 = { sizeof (Uniforms_t1850622510), -1, sizeof(Uniforms_t1850622510_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4355[2] = 
{
	Uniforms_t1850622510_StaticFields::get_offset_of__QualitySettings_0(),
	Uniforms_t1850622510_StaticFields::get_offset_of__ConsoleSettings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4356 = { sizeof (GrainComponent_t866324317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4356[1] = 
{
	GrainComponent_t866324317::get_offset_of_m_GrainLookupRT_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4357 = { sizeof (Uniforms_t1442519687), -1, sizeof(Uniforms_t1442519687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4357[4] = 
{
	Uniforms_t1442519687_StaticFields::get_offset_of__Grain_Params1_0(),
	Uniforms_t1442519687_StaticFields::get_offset_of__Grain_Params2_1(),
	Uniforms_t1442519687_StaticFields::get_offset_of__GrainTex_2(),
	Uniforms_t1442519687_StaticFields::get_offset_of__Phase_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4358 = { sizeof (MotionBlurComponent_t3686516877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4358[3] = 
{
	MotionBlurComponent_t3686516877::get_offset_of_m_ReconstructionFilter_2(),
	MotionBlurComponent_t3686516877::get_offset_of_m_FrameBlendingFilter_3(),
	MotionBlurComponent_t3686516877::get_offset_of_m_FirstFrame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4359 = { sizeof (Uniforms_t589754008), -1, sizeof(Uniforms_t589754008_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4359[26] = 
{
	Uniforms_t589754008_StaticFields::get_offset_of__VelocityScale_0(),
	Uniforms_t589754008_StaticFields::get_offset_of__MaxBlurRadius_1(),
	Uniforms_t589754008_StaticFields::get_offset_of__RcpMaxBlurRadius_2(),
	Uniforms_t589754008_StaticFields::get_offset_of__VelocityTex_3(),
	Uniforms_t589754008_StaticFields::get_offset_of__MainTex_4(),
	Uniforms_t589754008_StaticFields::get_offset_of__Tile2RT_5(),
	Uniforms_t589754008_StaticFields::get_offset_of__Tile4RT_6(),
	Uniforms_t589754008_StaticFields::get_offset_of__Tile8RT_7(),
	Uniforms_t589754008_StaticFields::get_offset_of__TileMaxOffs_8(),
	Uniforms_t589754008_StaticFields::get_offset_of__TileMaxLoop_9(),
	Uniforms_t589754008_StaticFields::get_offset_of__TileVRT_10(),
	Uniforms_t589754008_StaticFields::get_offset_of__NeighborMaxTex_11(),
	Uniforms_t589754008_StaticFields::get_offset_of__LoopCount_12(),
	Uniforms_t589754008_StaticFields::get_offset_of__TempRT_13(),
	Uniforms_t589754008_StaticFields::get_offset_of__History1LumaTex_14(),
	Uniforms_t589754008_StaticFields::get_offset_of__History2LumaTex_15(),
	Uniforms_t589754008_StaticFields::get_offset_of__History3LumaTex_16(),
	Uniforms_t589754008_StaticFields::get_offset_of__History4LumaTex_17(),
	Uniforms_t589754008_StaticFields::get_offset_of__History1ChromaTex_18(),
	Uniforms_t589754008_StaticFields::get_offset_of__History2ChromaTex_19(),
	Uniforms_t589754008_StaticFields::get_offset_of__History3ChromaTex_20(),
	Uniforms_t589754008_StaticFields::get_offset_of__History4ChromaTex_21(),
	Uniforms_t589754008_StaticFields::get_offset_of__History1Weight_22(),
	Uniforms_t589754008_StaticFields::get_offset_of__History2Weight_23(),
	Uniforms_t589754008_StaticFields::get_offset_of__History3Weight_24(),
	Uniforms_t589754008_StaticFields::get_offset_of__History4Weight_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4360 = { sizeof (Pass_t2620402414)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4360[10] = 
{
	Pass_t2620402414::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4361 = { sizeof (ReconstructionFilter_t705677647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4361[2] = 
{
	ReconstructionFilter_t705677647::get_offset_of_m_VectorRTFormat_0(),
	ReconstructionFilter_t705677647::get_offset_of_m_PackedRTFormat_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4362 = { sizeof (FrameBlendingFilter_t2699796096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4362[4] = 
{
	FrameBlendingFilter_t2699796096::get_offset_of_m_UseCompression_0(),
	FrameBlendingFilter_t2699796096::get_offset_of_m_RawTextureFormat_1(),
	FrameBlendingFilter_t2699796096::get_offset_of_m_FrameList_2(),
	FrameBlendingFilter_t2699796096::get_offset_of_m_LastFrameCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4363 = { sizeof (Frame_t295908221)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4363[4] = 
{
	Frame_t295908221::get_offset_of_lumaTexture_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t295908221::get_offset_of_chromaTexture_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t295908221::get_offset_of_m_Time_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t295908221::get_offset_of_m_MRT_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4364 = { sizeof (ScreenSpaceReflectionComponent_t856094247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4364[5] = 
{
	ScreenSpaceReflectionComponent_t856094247::get_offset_of_k_HighlightSuppression_2(),
	ScreenSpaceReflectionComponent_t856094247::get_offset_of_k_TraceBehindObjects_3(),
	ScreenSpaceReflectionComponent_t856094247::get_offset_of_k_TreatBackfaceHitAsMiss_4(),
	ScreenSpaceReflectionComponent_t856094247::get_offset_of_k_BilateralUpsample_5(),
	ScreenSpaceReflectionComponent_t856094247::get_offset_of_m_ReflectionTextures_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4365 = { sizeof (Uniforms_t2970573890), -1, sizeof(Uniforms_t2970573890_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4365[35] = 
{
	Uniforms_t2970573890_StaticFields::get_offset_of__RayStepSize_0(),
	Uniforms_t2970573890_StaticFields::get_offset_of__AdditiveReflection_1(),
	Uniforms_t2970573890_StaticFields::get_offset_of__BilateralUpsampling_2(),
	Uniforms_t2970573890_StaticFields::get_offset_of__TreatBackfaceHitAsMiss_3(),
	Uniforms_t2970573890_StaticFields::get_offset_of__AllowBackwardsRays_4(),
	Uniforms_t2970573890_StaticFields::get_offset_of__TraceBehindObjects_5(),
	Uniforms_t2970573890_StaticFields::get_offset_of__MaxSteps_6(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FullResolutionFiltering_7(),
	Uniforms_t2970573890_StaticFields::get_offset_of__HalfResolution_8(),
	Uniforms_t2970573890_StaticFields::get_offset_of__HighlightSuppression_9(),
	Uniforms_t2970573890_StaticFields::get_offset_of__PixelsPerMeterAtOneMeter_10(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ScreenEdgeFading_11(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ReflectionBlur_12(),
	Uniforms_t2970573890_StaticFields::get_offset_of__MaxRayTraceDistance_13(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FadeDistance_14(),
	Uniforms_t2970573890_StaticFields::get_offset_of__LayerThickness_15(),
	Uniforms_t2970573890_StaticFields::get_offset_of__SSRMultiplier_16(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FresnelFade_17(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FresnelFadePower_18(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ReflectionBufferSize_19(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ScreenSize_20(),
	Uniforms_t2970573890_StaticFields::get_offset_of__InvScreenSize_21(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ProjInfo_22(),
	Uniforms_t2970573890_StaticFields::get_offset_of__CameraClipInfo_23(),
	Uniforms_t2970573890_StaticFields::get_offset_of__ProjectToPixelMatrix_24(),
	Uniforms_t2970573890_StaticFields::get_offset_of__WorldToCameraMatrix_25(),
	Uniforms_t2970573890_StaticFields::get_offset_of__CameraToWorldMatrix_26(),
	Uniforms_t2970573890_StaticFields::get_offset_of__Axis_27(),
	Uniforms_t2970573890_StaticFields::get_offset_of__CurrentMipLevel_28(),
	Uniforms_t2970573890_StaticFields::get_offset_of__NormalAndRoughnessTexture_29(),
	Uniforms_t2970573890_StaticFields::get_offset_of__HitPointTexture_30(),
	Uniforms_t2970573890_StaticFields::get_offset_of__BlurTexture_31(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FilteredReflections_32(),
	Uniforms_t2970573890_StaticFields::get_offset_of__FinalReflectionTexture_33(),
	Uniforms_t2970573890_StaticFields::get_offset_of__TempTexture_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4366 = { sizeof (PassIndex_t1642913883)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4366[10] = 
{
	PassIndex_t1642913883::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4367 = { sizeof (TaaComponent_t3791749658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4367[7] = 
{
	0,
	0,
	TaaComponent_t3791749658::get_offset_of_m_MRT_4(),
	TaaComponent_t3791749658::get_offset_of_m_SampleIndex_5(),
	TaaComponent_t3791749658::get_offset_of_m_ResetHistory_6(),
	TaaComponent_t3791749658::get_offset_of_m_HistoryTexture_7(),
	TaaComponent_t3791749658::get_offset_of_U3CjitterVectorU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4368 = { sizeof (Uniforms_t3024963833), -1, sizeof(Uniforms_t3024963833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4368[5] = 
{
	Uniforms_t3024963833_StaticFields::get_offset_of__Jitter_0(),
	Uniforms_t3024963833_StaticFields::get_offset_of__SharpenParameters_1(),
	Uniforms_t3024963833_StaticFields::get_offset_of__FinalBlendParameters_2(),
	Uniforms_t3024963833_StaticFields::get_offset_of__HistoryTex_3(),
	Uniforms_t3024963833_StaticFields::get_offset_of__MainTex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4369 = { sizeof (UserLutComponent_t2843161776), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4370 = { sizeof (Uniforms_t1046717683), -1, sizeof(Uniforms_t1046717683_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4370[2] = 
{
	Uniforms_t1046717683_StaticFields::get_offset_of__UserLut_0(),
	Uniforms_t1046717683_StaticFields::get_offset_of__UserLut_Params_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4371 = { sizeof (VignetteComponent_t3243642943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4372 = { sizeof (Uniforms_t2205824134), -1, sizeof(Uniforms_t2205824134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4372[5] = 
{
	Uniforms_t2205824134_StaticFields::get_offset_of__Vignette_Color_0(),
	Uniforms_t2205824134_StaticFields::get_offset_of__Vignette_Center_1(),
	Uniforms_t2205824134_StaticFields::get_offset_of__Vignette_Settings_2(),
	Uniforms_t2205824134_StaticFields::get_offset_of__Vignette_Mask_3(),
	Uniforms_t2205824134_StaticFields::get_offset_of__Vignette_Opacity_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4373 = { sizeof (AmbientOcclusionModel_t389471066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4373[1] = 
{
	AmbientOcclusionModel_t389471066::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4374 = { sizeof (SampleCount_t1158000259)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4374[5] = 
{
	SampleCount_t1158000259::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4375 = { sizeof (Settings_t3016786575)+ sizeof (RuntimeObject), sizeof(Settings_t3016786575_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4375[7] = 
{
	Settings_t3016786575::get_offset_of_intensity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_radius_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_sampleCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_downsampling_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_forceForwardCompatibility_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_ambientOnly_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3016786575::get_offset_of_highPrecision_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4376 = { sizeof (AntialiasingModel_t1521139388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4376[1] = 
{
	AntialiasingModel_t1521139388::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4377 = { sizeof (Method_t287042102)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4377[3] = 
{
	Method_t287042102::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4378 = { sizeof (FxaaPreset_t2149486832)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4378[6] = 
{
	FxaaPreset_t2149486832::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4379 = { sizeof (FxaaQualitySettings_t1558211909)+ sizeof (RuntimeObject), sizeof(FxaaQualitySettings_t1558211909 ), sizeof(FxaaQualitySettings_t1558211909_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4379[4] = 
{
	FxaaQualitySettings_t1558211909::get_offset_of_subpixelAliasingRemovalAmount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaQualitySettings_t1558211909::get_offset_of_edgeDetectionThreshold_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaQualitySettings_t1558211909::get_offset_of_minimumRequiredLuminance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaQualitySettings_t1558211909_StaticFields::get_offset_of_presets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4380 = { sizeof (FxaaConsoleSettings_t950185286)+ sizeof (RuntimeObject), sizeof(FxaaConsoleSettings_t950185286 ), sizeof(FxaaConsoleSettings_t950185286_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4380[5] = 
{
	FxaaConsoleSettings_t950185286::get_offset_of_subpixelSpreadAmount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaConsoleSettings_t950185286::get_offset_of_edgeSharpnessAmount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaConsoleSettings_t950185286::get_offset_of_edgeDetectionThreshold_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaConsoleSettings_t950185286::get_offset_of_minimumRequiredLuminance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FxaaConsoleSettings_t950185286_StaticFields::get_offset_of_presets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4381 = { sizeof (FxaaSettings_t1280675075)+ sizeof (RuntimeObject), sizeof(FxaaSettings_t1280675075 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4381[1] = 
{
	FxaaSettings_t1280675075::get_offset_of_preset_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4382 = { sizeof (TaaSettings_t2709374970)+ sizeof (RuntimeObject), sizeof(TaaSettings_t2709374970 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4382[4] = 
{
	TaaSettings_t2709374970::get_offset_of_jitterSpread_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaaSettings_t2709374970::get_offset_of_sharpen_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaaSettings_t2709374970::get_offset_of_stationaryBlending_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TaaSettings_t2709374970::get_offset_of_motionBlending_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4383 = { sizeof (Settings_t4292431647)+ sizeof (RuntimeObject), sizeof(Settings_t4292431647 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4383[3] = 
{
	Settings_t4292431647::get_offset_of_method_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4292431647::get_offset_of_fxaaSettings_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4292431647::get_offset_of_taaSettings_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4384 = { sizeof (BloomModel_t2099727860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4384[1] = 
{
	BloomModel_t2099727860::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4385 = { sizeof (BloomSettings_t2599855122)+ sizeof (RuntimeObject), sizeof(BloomSettings_t2599855122_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4385[5] = 
{
	BloomSettings_t2599855122::get_offset_of_intensity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BloomSettings_t2599855122::get_offset_of_threshold_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BloomSettings_t2599855122::get_offset_of_softKnee_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BloomSettings_t2599855122::get_offset_of_radius_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BloomSettings_t2599855122::get_offset_of_antiFlicker_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4386 = { sizeof (LensDirtSettings_t3693422705)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4386[2] = 
{
	LensDirtSettings_t3693422705::get_offset_of_texture_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensDirtSettings_t3693422705::get_offset_of_intensity_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4387 = { sizeof (Settings_t181254429)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4387[2] = 
{
	Settings_t181254429::get_offset_of_bloom_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t181254429::get_offset_of_lensDirt_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4388 = { sizeof (BuiltinDebugViewsModel_t1462618840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4388[1] = 
{
	BuiltinDebugViewsModel_t1462618840::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4389 = { sizeof (DepthSettings_t1820272864)+ sizeof (RuntimeObject), sizeof(DepthSettings_t1820272864 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4389[1] = 
{
	DepthSettings_t1820272864::get_offset_of_scale_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4390 = { sizeof (MotionVectorsSettings_t3857813598)+ sizeof (RuntimeObject), sizeof(MotionVectorsSettings_t3857813598 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4390[6] = 
{
	MotionVectorsSettings_t3857813598::get_offset_of_sourceOpacity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MotionVectorsSettings_t3857813598::get_offset_of_motionImageOpacity_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MotionVectorsSettings_t3857813598::get_offset_of_motionImageAmplitude_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MotionVectorsSettings_t3857813598::get_offset_of_motionVectorsOpacity_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MotionVectorsSettings_t3857813598::get_offset_of_motionVectorsResolution_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MotionVectorsSettings_t3857813598::get_offset_of_motionVectorsAmplitude_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4391 = { sizeof (Mode_t2695902415)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4391[11] = 
{
	Mode_t2695902415::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4392 = { sizeof (Settings_t3984509665)+ sizeof (RuntimeObject), sizeof(Settings_t3984509665 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4392[3] = 
{
	Settings_t3984509665::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3984509665::get_offset_of_depth_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3984509665::get_offset_of_motionVectors_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4393 = { sizeof (ChromaticAberrationModel_t3963399853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4393[1] = 
{
	ChromaticAberrationModel_t3963399853::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4394 = { sizeof (Settings_t2111398455)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4394[2] = 
{
	Settings_t2111398455::get_offset_of_spectralTexture_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2111398455::get_offset_of_intensity_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4395 = { sizeof (ColorGradingModel_t1448048181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4395[3] = 
{
	ColorGradingModel_t1448048181::get_offset_of_m_Settings_1(),
	ColorGradingModel_t1448048181::get_offset_of_U3CisDirtyU3Ek__BackingField_2(),
	ColorGradingModel_t1448048181::get_offset_of_U3CbakedLutU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4396 = { sizeof (Tonemapper_t1404353651)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4396[4] = 
{
	Tonemapper_t1404353651::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4397 = { sizeof (TonemappingSettings_t4154044775)+ sizeof (RuntimeObject), sizeof(TonemappingSettings_t4154044775 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4397[7] = 
{
	TonemappingSettings_t4154044775::get_offset_of_tonemapper_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t4154044775::get_offset_of_neutralBlackIn_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t4154044775::get_offset_of_neutralWhiteIn_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t4154044775::get_offset_of_neutralBlackOut_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t4154044775::get_offset_of_neutralWhiteOut_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t4154044775::get_offset_of_neutralWhiteLevel_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t4154044775::get_offset_of_neutralWhiteClip_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4398 = { sizeof (BasicSettings_t838098426)+ sizeof (RuntimeObject), sizeof(BasicSettings_t838098426 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4398[6] = 
{
	BasicSettings_t838098426::get_offset_of_postExposure_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicSettings_t838098426::get_offset_of_temperature_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicSettings_t838098426::get_offset_of_tint_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicSettings_t838098426::get_offset_of_hueShift_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicSettings_t838098426::get_offset_of_saturation_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicSettings_t838098426::get_offset_of_contrast_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4399 = { sizeof (ChannelMixerSettings_t898701698)+ sizeof (RuntimeObject), sizeof(ChannelMixerSettings_t898701698 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4399[4] = 
{
	ChannelMixerSettings_t898701698::get_offset_of_red_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelMixerSettings_t898701698::get_offset_of_green_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelMixerSettings_t898701698::get_offset_of_blue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelMixerSettings_t898701698::get_offset_of_currentEditingChannel_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
