﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// EnemyAttack
struct EnemyAttack_t24081459;
// EnemyAttackSinglePlayer
struct EnemyAttackSinglePlayer_t4127103160;
// EnemyHealth
struct EnemyHealth_t797421206;
// EnemyNavigation
struct EnemyNavigation_t1695490929;
// ExitDoor
struct ExitDoor_t1886296669;
// ManageEnemy
struct ManageEnemy_t3567251697;
// ManageScene
struct ManageScene_t3196455047;
// PlayerHealth
struct PlayerHealth_t2068385516;
// SimpleHealthBar
struct SimpleHealthBar_t721070758;
// SimpleHealthBar_SpaceshipExample.AsteroidController
struct AsteroidController_t3828879615;
// SimpleHealthBar_SpaceshipExample.GameManager
struct GameManager_t3924726104;
// SimpleHealthBar_SpaceshipExample.HealthPickupController
struct HealthPickupController_t1709822218;
// SimpleHealthBar_SpaceshipExample.PlayerController
struct PlayerController_t2576101795;
// SimpleHealthBar_SpaceshipExample.PlayerHealth
struct PlayerHealth_t992877501;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1276799816;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Gradient
struct Gradient_t3067099924;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct ThirdPersonCharacter_t1711070432;
// VillagerInteraction
struct VillagerInteraction_t490587617;
// WarriorAnimationDemoFREE
struct WarriorAnimationDemoFREE_t1507088667;
// WarriorP2
struct WarriorP2_t1586698613;
// WizardController
struct WizardController_t2370936914;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSOUNDDELAYU3ED__18_T748685287_H
#define U3CSOUNDDELAYU3ED__18_T748685287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyAttack/<SoundDelay>d__18
struct  U3CSoundDelayU3Ed__18_t748685287  : public RuntimeObject
{
public:
	// System.Int32 EnemyAttack/<SoundDelay>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object EnemyAttack/<SoundDelay>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// EnemyAttack EnemyAttack/<SoundDelay>d__18::<>4__this
	EnemyAttack_t24081459 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__18_t748685287, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__18_t748685287, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__18_t748685287, ___U3CU3E4__this_2)); }
	inline EnemyAttack_t24081459 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline EnemyAttack_t24081459 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(EnemyAttack_t24081459 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSOUNDDELAYU3ED__18_T748685287_H
#ifndef U3CSOUNDDELAYU3ED__15_T3259149061_H
#define U3CSOUNDDELAYU3ED__15_T3259149061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyAttackSinglePlayer/<SoundDelay>d__15
struct  U3CSoundDelayU3Ed__15_t3259149061  : public RuntimeObject
{
public:
	// System.Int32 EnemyAttackSinglePlayer/<SoundDelay>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object EnemyAttackSinglePlayer/<SoundDelay>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// EnemyAttackSinglePlayer EnemyAttackSinglePlayer/<SoundDelay>d__15::<>4__this
	EnemyAttackSinglePlayer_t4127103160 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__15_t3259149061, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__15_t3259149061, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__15_t3259149061, ___U3CU3E4__this_2)); }
	inline EnemyAttackSinglePlayer_t4127103160 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline EnemyAttackSinglePlayer_t4127103160 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(EnemyAttackSinglePlayer_t4127103160 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSOUNDDELAYU3ED__15_T3259149061_H
#ifndef U3CSOUNDDELAYU3ED__36_T1499302528_H
#define U3CSOUNDDELAYU3ED__36_T1499302528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyNavigation/<SoundDelay>d__36
struct  U3CSoundDelayU3Ed__36_t1499302528  : public RuntimeObject
{
public:
	// System.Int32 EnemyNavigation/<SoundDelay>d__36::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object EnemyNavigation/<SoundDelay>d__36::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// EnemyNavigation EnemyNavigation/<SoundDelay>d__36::<>4__this
	EnemyNavigation_t1695490929 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__36_t1499302528, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__36_t1499302528, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__36_t1499302528, ___U3CU3E4__this_2)); }
	inline EnemyNavigation_t1695490929 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline EnemyNavigation_t1695490929 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(EnemyNavigation_t1695490929 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSOUNDDELAYU3ED__36_T1499302528_H
#ifndef U3CDELAYRESTARTP1U3ED__10_T1872549700_H
#define U3CDELAYRESTARTP1U3ED__10_T1872549700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManageScene/<DelayRestartP1>d__10
struct  U3CDelayRestartP1U3Ed__10_t1872549700  : public RuntimeObject
{
public:
	// System.Int32 ManageScene/<DelayRestartP1>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ManageScene/<DelayRestartP1>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ManageScene ManageScene/<DelayRestartP1>d__10::<>4__this
	ManageScene_t3196455047 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayRestartP1U3Ed__10_t1872549700, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayRestartP1U3Ed__10_t1872549700, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDelayRestartP1U3Ed__10_t1872549700, ___U3CU3E4__this_2)); }
	inline ManageScene_t3196455047 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ManageScene_t3196455047 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ManageScene_t3196455047 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYRESTARTP1U3ED__10_T1872549700_H
#ifndef U3CDELAYRESTARTP2U3ED__11_T568587784_H
#define U3CDELAYRESTARTP2U3ED__11_T568587784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManageScene/<DelayRestartP2>d__11
struct  U3CDelayRestartP2U3Ed__11_t568587784  : public RuntimeObject
{
public:
	// System.Int32 ManageScene/<DelayRestartP2>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ManageScene/<DelayRestartP2>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ManageScene ManageScene/<DelayRestartP2>d__11::<>4__this
	ManageScene_t3196455047 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayRestartP2U3Ed__11_t568587784, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayRestartP2U3Ed__11_t568587784, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDelayRestartP2U3Ed__11_t568587784, ___U3CU3E4__this_2)); }
	inline ManageScene_t3196455047 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ManageScene_t3196455047 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ManageScene_t3196455047 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYRESTARTP2U3ED__11_T568587784_H
#ifndef U3CCOSTUNPAUSEU3ED__14_T3804073412_H
#define U3CCOSTUNPAUSEU3ED__14_T3804073412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerControlP1/<COStunPause>d__14
struct  U3CCOStunPauseU3Ed__14_t3804073412  : public RuntimeObject
{
public:
	// System.Int32 PlayerControlP1/<COStunPause>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PlayerControlP1/<COStunPause>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single PlayerControlP1/<COStunPause>d__14::pauseTime
	float ___pauseTime_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCOStunPauseU3Ed__14_t3804073412, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCOStunPauseU3Ed__14_t3804073412, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_pauseTime_2() { return static_cast<int32_t>(offsetof(U3CCOStunPauseU3Ed__14_t3804073412, ___pauseTime_2)); }
	inline float get_pauseTime_2() const { return ___pauseTime_2; }
	inline float* get_address_of_pauseTime_2() { return &___pauseTime_2; }
	inline void set_pauseTime_2(float value)
	{
		___pauseTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOSTUNPAUSEU3ED__14_T3804073412_H
#ifndef U3CDELAYINITIALDESTRUCTIONU3ED__7_T3782258133_H
#define U3CDELAYINITIALDESTRUCTIONU3ED__7_T3782258133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>d__7
struct  U3CDelayInitialDestructionU3Ed__7_t3782258133  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>d__7::delayTime
	float ___delayTime_2;
	// SimpleHealthBar_SpaceshipExample.AsteroidController SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>d__7::<>4__this
	AsteroidController_t3828879615 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__7_t3782258133, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__7_t3782258133, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delayTime_2() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__7_t3782258133, ___delayTime_2)); }
	inline float get_delayTime_2() const { return ___delayTime_2; }
	inline float* get_address_of_delayTime_2() { return &___delayTime_2; }
	inline void set_delayTime_2(float value)
	{
		___delayTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__7_t3782258133, ___U3CU3E4__this_3)); }
	inline AsteroidController_t3828879615 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AsteroidController_t3828879615 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AsteroidController_t3828879615 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYINITIALDESTRUCTIONU3ED__7_T3782258133_H
#ifndef U3CSPAWNHEALTHTIMERU3ED__25_T234955219_H
#define U3CSPAWNHEALTHTIMERU3ED__25_T234955219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>d__25
struct  U3CSpawnHealthTimerU3Ed__25_t234955219  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>d__25::<>4__this
	GameManager_t3924726104 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSpawnHealthTimerU3Ed__25_t234955219, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSpawnHealthTimerU3Ed__25_t234955219, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSpawnHealthTimerU3Ed__25_t234955219, ___U3CU3E4__this_2)); }
	inline GameManager_t3924726104 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameManager_t3924726104 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameManager_t3924726104 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNHEALTHTIMERU3ED__25_T234955219_H
#ifndef U3CSPAWNTIMERU3ED__27_T2700198560_H
#define U3CSPAWNTIMERU3ED__27_T2700198560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>d__27
struct  U3CSpawnTimerU3Ed__27_t2700198560  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>d__27::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>d__27::<>4__this
	GameManager_t3924726104 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSpawnTimerU3Ed__27_t2700198560, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSpawnTimerU3Ed__27_t2700198560, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSpawnTimerU3Ed__27_t2700198560, ___U3CU3E4__this_2)); }
	inline GameManager_t3924726104 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameManager_t3924726104 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameManager_t3924726104 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNTIMERU3ED__27_T2700198560_H
#ifndef U3CDELAYINITIALDESTRUCTIONU3ED__6_T2327916900_H
#define U3CDELAYINITIALDESTRUCTIONU3ED__6_T2327916900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>d__6
struct  U3CDelayInitialDestructionU3Ed__6_t2327916900  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>d__6::delayTime
	float ___delayTime_2;
	// SimpleHealthBar_SpaceshipExample.HealthPickupController SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>d__6::<>4__this
	HealthPickupController_t1709822218 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__6_t2327916900, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__6_t2327916900, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delayTime_2() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__6_t2327916900, ___delayTime_2)); }
	inline float get_delayTime_2() const { return ___delayTime_2; }
	inline float* get_address_of_delayTime_2() { return &___delayTime_2; }
	inline void set_delayTime_2(float value)
	{
		___delayTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ed__6_t2327916900, ___U3CU3E4__this_3)); }
	inline HealthPickupController_t1709822218 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline HealthPickupController_t1709822218 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(HealthPickupController_t1709822218 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYINITIALDESTRUCTIONU3ED__6_T2327916900_H
#ifndef U3CDELAYOVERHEATU3ED__29_T3308176875_H
#define U3CDELAYOVERHEATU3ED__29_T3308176875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>d__29
struct  U3CDelayOverheatU3Ed__29_t3308176875  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>d__29::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleHealthBar_SpaceshipExample.PlayerController SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>d__29::<>4__this
	PlayerController_t2576101795 * ___U3CU3E4__this_2;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>d__29::<t>5__2
	float ___U3CtU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ed__29_t3308176875, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ed__29_t3308176875, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ed__29_t3308176875, ___U3CU3E4__this_2)); }
	inline PlayerController_t2576101795 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PlayerController_t2576101795 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PlayerController_t2576101795 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ed__29_t3308176875, ___U3CtU3E5__2_3)); }
	inline float get_U3CtU3E5__2_3() const { return ___U3CtU3E5__2_3; }
	inline float* get_address_of_U3CtU3E5__2_3() { return &___U3CtU3E5__2_3; }
	inline void set_U3CtU3E5__2_3(float value)
	{
		___U3CtU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYOVERHEATU3ED__29_T3308176875_H
#ifndef U3CINVULNERABLILTYU3ED__20_T2384978279_H
#define U3CINVULNERABLILTYU3ED__20_T2384978279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>d__20
struct  U3CInvulnerabliltyU3Ed__20_t2384978279  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>d__20::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleHealthBar_SpaceshipExample.PlayerHealth SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>d__20::<>4__this
	PlayerHealth_t992877501 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInvulnerabliltyU3Ed__20_t2384978279, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInvulnerabliltyU3Ed__20_t2384978279, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInvulnerabliltyU3Ed__20_t2384978279, ___U3CU3E4__this_2)); }
	inline PlayerHealth_t992877501 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PlayerHealth_t992877501 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PlayerHealth_t992877501 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVULNERABLILTYU3ED__20_T2384978279_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CTALKHANDLERU3ED__11_T2840649544_H
#define U3CTALKHANDLERU3ED__11_T2840649544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VillagerInteraction/<TalkHandler>d__11
struct  U3CTalkHandlerU3Ed__11_t2840649544  : public RuntimeObject
{
public:
	// System.Int32 VillagerInteraction/<TalkHandler>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VillagerInteraction/<TalkHandler>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// VillagerInteraction VillagerInteraction/<TalkHandler>d__11::<>4__this
	VillagerInteraction_t490587617 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTalkHandlerU3Ed__11_t2840649544, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTalkHandlerU3Ed__11_t2840649544, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTalkHandlerU3Ed__11_t2840649544, ___U3CU3E4__this_2)); }
	inline VillagerInteraction_t490587617 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VillagerInteraction_t490587617 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VillagerInteraction_t490587617 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTALKHANDLERU3ED__11_T2840649544_H
#ifndef U3CCOSTUNPAUSEU3ED__14_T3614912138_H
#define U3CCOSTUNPAUSEU3ED__14_T3614912138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WarriorAnimationDemoFREE/<COStunPause>d__14
struct  U3CCOStunPauseU3Ed__14_t3614912138  : public RuntimeObject
{
public:
	// System.Int32 WarriorAnimationDemoFREE/<COStunPause>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object WarriorAnimationDemoFREE/<COStunPause>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single WarriorAnimationDemoFREE/<COStunPause>d__14::pauseTime
	float ___pauseTime_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCOStunPauseU3Ed__14_t3614912138, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCOStunPauseU3Ed__14_t3614912138, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_pauseTime_2() { return static_cast<int32_t>(offsetof(U3CCOStunPauseU3Ed__14_t3614912138, ___pauseTime_2)); }
	inline float get_pauseTime_2() const { return ___pauseTime_2; }
	inline float* get_address_of_pauseTime_2() { return &___pauseTime_2; }
	inline void set_pauseTime_2(float value)
	{
		___pauseTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOSTUNPAUSEU3ED__14_T3614912138_H
#ifndef U3CCOSTUNPAUSEU3ED__14_T2433058018_H
#define U3CCOSTUNPAUSEU3ED__14_T2433058018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WarriorP2/<COStunPause>d__14
struct  U3CCOStunPauseU3Ed__14_t2433058018  : public RuntimeObject
{
public:
	// System.Int32 WarriorP2/<COStunPause>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object WarriorP2/<COStunPause>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single WarriorP2/<COStunPause>d__14::pauseTime
	float ___pauseTime_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCOStunPauseU3Ed__14_t2433058018, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCOStunPauseU3Ed__14_t2433058018, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_pauseTime_2() { return static_cast<int32_t>(offsetof(U3CCOStunPauseU3Ed__14_t2433058018, ___pauseTime_2)); }
	inline float get_pauseTime_2() const { return ___pauseTime_2; }
	inline float* get_address_of_pauseTime_2() { return &___pauseTime_2; }
	inline void set_pauseTime_2(float value)
	{
		___pauseTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOSTUNPAUSEU3ED__14_T2433058018_H
#ifndef U3CSOUNDDELAYU3ED__36_T1519778301_H
#define U3CSOUNDDELAYU3ED__36_T1519778301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WizardController/<SoundDelay>d__36
struct  U3CSoundDelayU3Ed__36_t1519778301  : public RuntimeObject
{
public:
	// System.Int32 WizardController/<SoundDelay>d__36::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object WizardController/<SoundDelay>d__36::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// WizardController WizardController/<SoundDelay>d__36::<>4__this
	WizardController_t2370936914 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__36_t1519778301, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__36_t1519778301, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSoundDelayU3Ed__36_t1519778301, ___U3CU3E4__this_2)); }
	inline WizardController_t2370936914 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WizardController_t2370936914 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WizardController_t2370936914 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSOUNDDELAYU3ED__36_T1519778301_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef BUBBLETYPE_T1488104874_H
#define BUBBLETYPE_T1488104874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BubbleType
struct  BubbleType_t1488104874 
{
public:
	// System.Int32 BubbleType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BubbleType_t1488104874, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUBBLETYPE_T1488104874_H
#ifndef WARRIOR_T2382298878_H
#define WARRIOR_T2382298878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerControlP1/Warrior
struct  Warrior_t2382298878 
{
public:
	// System.Int32 PlayerControlP1/Warrior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Warrior_t2382298878, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARRIOR_T2382298878_H
#ifndef COLORMODE_T635806747_H
#define COLORMODE_T635806747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar/ColorMode
struct  ColorMode_t635806747 
{
public:
	// System.Int32 SimpleHealthBar/ColorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorMode_t635806747, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T635806747_H
#ifndef DISPLAYTEXT_T3312458044_H
#define DISPLAYTEXT_T3312458044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar/DisplayText
struct  DisplayText_t3312458044 
{
public:
	// System.Int32 SimpleHealthBar/DisplayText::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DisplayText_t3312458044, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYTEXT_T3312458044_H
#ifndef U3CFADEDEATHSCREENU3ED__32_T3488881226_H
#define U3CFADEDEATHSCREENU3ED__32_T3488881226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32
struct  U3CFadeDeathScreenU3Ed__32_t3488881226  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<>4__this
	GameManager_t3924726104 * ___U3CU3E4__this_2;
	// UnityEngine.Color SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<imageColor>5__2
	Color_t2555686324  ___U3CimageColorU3E5__2_3;
	// UnityEngine.Color SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<textColor>5__3
	Color_t2555686324  ___U3CtextColorU3E5__3_4;
	// UnityEngine.Color SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<finalScoreTextColor>5__4
	Color_t2555686324  ___U3CfinalScoreTextColorU3E5__4_5;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>d__32::<t>5__5
	float ___U3CtU3E5__5_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t3488881226, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t3488881226, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t3488881226, ___U3CU3E4__this_2)); }
	inline GameManager_t3924726104 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameManager_t3924726104 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameManager_t3924726104 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CimageColorU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t3488881226, ___U3CimageColorU3E5__2_3)); }
	inline Color_t2555686324  get_U3CimageColorU3E5__2_3() const { return ___U3CimageColorU3E5__2_3; }
	inline Color_t2555686324 * get_address_of_U3CimageColorU3E5__2_3() { return &___U3CimageColorU3E5__2_3; }
	inline void set_U3CimageColorU3E5__2_3(Color_t2555686324  value)
	{
		___U3CimageColorU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CtextColorU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t3488881226, ___U3CtextColorU3E5__3_4)); }
	inline Color_t2555686324  get_U3CtextColorU3E5__3_4() const { return ___U3CtextColorU3E5__3_4; }
	inline Color_t2555686324 * get_address_of_U3CtextColorU3E5__3_4() { return &___U3CtextColorU3E5__3_4; }
	inline void set_U3CtextColorU3E5__3_4(Color_t2555686324  value)
	{
		___U3CtextColorU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CfinalScoreTextColorU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t3488881226, ___U3CfinalScoreTextColorU3E5__4_5)); }
	inline Color_t2555686324  get_U3CfinalScoreTextColorU3E5__4_5() const { return ___U3CfinalScoreTextColorU3E5__4_5; }
	inline Color_t2555686324 * get_address_of_U3CfinalScoreTextColorU3E5__4_5() { return &___U3CfinalScoreTextColorU3E5__4_5; }
	inline void set_U3CfinalScoreTextColorU3E5__4_5(Color_t2555686324  value)
	{
		___U3CfinalScoreTextColorU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ed__32_t3488881226, ___U3CtU3E5__5_6)); }
	inline float get_U3CtU3E5__5_6() const { return ___U3CtU3E5__5_6; }
	inline float* get_address_of_U3CtU3E5__5_6() { return &___U3CtU3E5__5_6; }
	inline void set_U3CtU3E5__5_6(float value)
	{
		___U3CtU3E5__5_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEDEATHSCREENU3ED__32_T3488881226_H
#ifndef U3CSHAKECAMERAU3ED__21_T2693912377_H
#define U3CSHAKECAMERAU3ED__21_T2693912377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>d__21
struct  U3CShakeCameraU3Ed__21_t2693912377  : public RuntimeObject
{
public:
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Vector2 SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>d__21::<origPos>5__2
	Vector2_t2156229523  ___U3CorigPosU3E5__2_2;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>d__21::<t>5__3
	float ___U3CtU3E5__3_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ed__21_t2693912377, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ed__21_t2693912377, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CorigPosU3E5__2_2() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ed__21_t2693912377, ___U3CorigPosU3E5__2_2)); }
	inline Vector2_t2156229523  get_U3CorigPosU3E5__2_2() const { return ___U3CorigPosU3E5__2_2; }
	inline Vector2_t2156229523 * get_address_of_U3CorigPosU3E5__2_2() { return &___U3CorigPosU3E5__2_2; }
	inline void set_U3CorigPosU3E5__2_2(Vector2_t2156229523  value)
	{
		___U3CorigPosU3E5__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__3_3() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ed__21_t2693912377, ___U3CtU3E5__3_3)); }
	inline float get_U3CtU3E5__3_3() const { return ___U3CtU3E5__3_3; }
	inline float* get_address_of_U3CtU3E5__3_3() { return &___U3CtU3E5__3_3; }
	inline void set_U3CtU3E5__3_3(float value)
	{
		___U3CtU3E5__3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHAKECAMERAU3ED__21_T2693912377_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef WARRIOR_T3094985692_H
#define WARRIOR_T3094985692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WarriorAnimationDemoFREE/Warrior
struct  Warrior_t3094985692 
{
public:
	// System.Int32 WarriorAnimationDemoFREE/Warrior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Warrior_t3094985692, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARRIOR_T3094985692_H
#ifndef WARRIOR_T1316392392_H
#define WARRIOR_T1316392392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WarriorP2/Warrior
struct  Warrior_t1316392392 
{
public:
	// System.Int32 WarriorP2/Warrior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Warrior_t1316392392, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARRIOR_T1316392392_H
#ifndef PIXELBUBBLE_T3317741694_H
#define PIXELBUBBLE_T3317741694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssemblyCSharp.PixelBubble
struct  PixelBubble_t3317741694  : public RuntimeObject
{
public:
	// System.String AssemblyCSharp.PixelBubble::vMessage
	String_t* ___vMessage_0;
	// BubbleType AssemblyCSharp.PixelBubble::vMessageForm
	int32_t ___vMessageForm_1;
	// UnityEngine.Color AssemblyCSharp.PixelBubble::vBorderColor
	Color_t2555686324  ___vBorderColor_2;
	// UnityEngine.Color AssemblyCSharp.PixelBubble::vBodyColor
	Color_t2555686324  ___vBodyColor_3;
	// UnityEngine.Color AssemblyCSharp.PixelBubble::vFontColor
	Color_t2555686324  ___vFontColor_4;
	// System.Boolean AssemblyCSharp.PixelBubble::vClickToCloseBubble
	bool ___vClickToCloseBubble_5;

public:
	inline static int32_t get_offset_of_vMessage_0() { return static_cast<int32_t>(offsetof(PixelBubble_t3317741694, ___vMessage_0)); }
	inline String_t* get_vMessage_0() const { return ___vMessage_0; }
	inline String_t** get_address_of_vMessage_0() { return &___vMessage_0; }
	inline void set_vMessage_0(String_t* value)
	{
		___vMessage_0 = value;
		Il2CppCodeGenWriteBarrier((&___vMessage_0), value);
	}

	inline static int32_t get_offset_of_vMessageForm_1() { return static_cast<int32_t>(offsetof(PixelBubble_t3317741694, ___vMessageForm_1)); }
	inline int32_t get_vMessageForm_1() const { return ___vMessageForm_1; }
	inline int32_t* get_address_of_vMessageForm_1() { return &___vMessageForm_1; }
	inline void set_vMessageForm_1(int32_t value)
	{
		___vMessageForm_1 = value;
	}

	inline static int32_t get_offset_of_vBorderColor_2() { return static_cast<int32_t>(offsetof(PixelBubble_t3317741694, ___vBorderColor_2)); }
	inline Color_t2555686324  get_vBorderColor_2() const { return ___vBorderColor_2; }
	inline Color_t2555686324 * get_address_of_vBorderColor_2() { return &___vBorderColor_2; }
	inline void set_vBorderColor_2(Color_t2555686324  value)
	{
		___vBorderColor_2 = value;
	}

	inline static int32_t get_offset_of_vBodyColor_3() { return static_cast<int32_t>(offsetof(PixelBubble_t3317741694, ___vBodyColor_3)); }
	inline Color_t2555686324  get_vBodyColor_3() const { return ___vBodyColor_3; }
	inline Color_t2555686324 * get_address_of_vBodyColor_3() { return &___vBodyColor_3; }
	inline void set_vBodyColor_3(Color_t2555686324  value)
	{
		___vBodyColor_3 = value;
	}

	inline static int32_t get_offset_of_vFontColor_4() { return static_cast<int32_t>(offsetof(PixelBubble_t3317741694, ___vFontColor_4)); }
	inline Color_t2555686324  get_vFontColor_4() const { return ___vFontColor_4; }
	inline Color_t2555686324 * get_address_of_vFontColor_4() { return &___vFontColor_4; }
	inline void set_vFontColor_4(Color_t2555686324  value)
	{
		___vFontColor_4 = value;
	}

	inline static int32_t get_offset_of_vClickToCloseBubble_5() { return static_cast<int32_t>(offsetof(PixelBubble_t3317741694, ___vClickToCloseBubble_5)); }
	inline bool get_vClickToCloseBubble_5() const { return ___vClickToCloseBubble_5; }
	inline bool* get_address_of_vClickToCloseBubble_5() { return &___vClickToCloseBubble_5; }
	inline void set_vClickToCloseBubble_5(bool value)
	{
		___vClickToCloseBubble_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXELBUBBLE_T3317741694_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ARCHERBEHAVIOR_T1637475703_H
#define ARCHERBEHAVIOR_T1637475703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArcherBehavior
struct  ArcherBehavior_t1637475703  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform ArcherBehavior::arrowSpawnPoint
	Transform_t3600365921 * ___arrowSpawnPoint_4;
	// UnityEngine.GameObject ArcherBehavior::arrow
	GameObject_t1113636619 * ___arrow_5;
	// UnityEngine.AudioClip[] ArcherBehavior::clips
	AudioClipU5BU5D_t143221404* ___clips_6;
	// UnityEngine.AudioSource ArcherBehavior::AS
	AudioSource_t3935305588 * ___AS_7;
	// UnityEngine.Animator ArcherBehavior::anim
	Animator_t434523843 * ___anim_8;

public:
	inline static int32_t get_offset_of_arrowSpawnPoint_4() { return static_cast<int32_t>(offsetof(ArcherBehavior_t1637475703, ___arrowSpawnPoint_4)); }
	inline Transform_t3600365921 * get_arrowSpawnPoint_4() const { return ___arrowSpawnPoint_4; }
	inline Transform_t3600365921 ** get_address_of_arrowSpawnPoint_4() { return &___arrowSpawnPoint_4; }
	inline void set_arrowSpawnPoint_4(Transform_t3600365921 * value)
	{
		___arrowSpawnPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___arrowSpawnPoint_4), value);
	}

	inline static int32_t get_offset_of_arrow_5() { return static_cast<int32_t>(offsetof(ArcherBehavior_t1637475703, ___arrow_5)); }
	inline GameObject_t1113636619 * get_arrow_5() const { return ___arrow_5; }
	inline GameObject_t1113636619 ** get_address_of_arrow_5() { return &___arrow_5; }
	inline void set_arrow_5(GameObject_t1113636619 * value)
	{
		___arrow_5 = value;
		Il2CppCodeGenWriteBarrier((&___arrow_5), value);
	}

	inline static int32_t get_offset_of_clips_6() { return static_cast<int32_t>(offsetof(ArcherBehavior_t1637475703, ___clips_6)); }
	inline AudioClipU5BU5D_t143221404* get_clips_6() const { return ___clips_6; }
	inline AudioClipU5BU5D_t143221404** get_address_of_clips_6() { return &___clips_6; }
	inline void set_clips_6(AudioClipU5BU5D_t143221404* value)
	{
		___clips_6 = value;
		Il2CppCodeGenWriteBarrier((&___clips_6), value);
	}

	inline static int32_t get_offset_of_AS_7() { return static_cast<int32_t>(offsetof(ArcherBehavior_t1637475703, ___AS_7)); }
	inline AudioSource_t3935305588 * get_AS_7() const { return ___AS_7; }
	inline AudioSource_t3935305588 ** get_address_of_AS_7() { return &___AS_7; }
	inline void set_AS_7(AudioSource_t3935305588 * value)
	{
		___AS_7 = value;
		Il2CppCodeGenWriteBarrier((&___AS_7), value);
	}

	inline static int32_t get_offset_of_anim_8() { return static_cast<int32_t>(offsetof(ArcherBehavior_t1637475703, ___anim_8)); }
	inline Animator_t434523843 * get_anim_8() const { return ___anim_8; }
	inline Animator_t434523843 ** get_address_of_anim_8() { return &___anim_8; }
	inline void set_anim_8(Animator_t434523843 * value)
	{
		___anim_8 = value;
		Il2CppCodeGenWriteBarrier((&___anim_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCHERBEHAVIOR_T1637475703_H
#ifndef ARROWBEHAVIOR_T1475592159_H
#define ARROWBEHAVIOR_T1475592159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArrowBehavior
struct  ArrowBehavior_t1475592159  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ArrowBehavior::speed
	float ___speed_4;
	// System.Single ArrowBehavior::damageAmount
	float ___damageAmount_5;
	// UnityEngine.AudioClip[] ArrowBehavior::clips
	AudioClipU5BU5D_t143221404* ___clips_6;
	// PlayerHealth ArrowBehavior::P1H
	PlayerHealth_t2068385516 * ___P1H_7;
	// PlayerHealth ArrowBehavior::P2H
	PlayerHealth_t2068385516 * ___P2H_8;
	// UnityEngine.GameObject ArrowBehavior::P1
	GameObject_t1113636619 * ___P1_9;
	// UnityEngine.GameObject ArrowBehavior::P2
	GameObject_t1113636619 * ___P2_10;
	// UnityEngine.AudioSource ArrowBehavior::AS
	AudioSource_t3935305588 * ___AS_11;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(ArrowBehavior_t1475592159, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_damageAmount_5() { return static_cast<int32_t>(offsetof(ArrowBehavior_t1475592159, ___damageAmount_5)); }
	inline float get_damageAmount_5() const { return ___damageAmount_5; }
	inline float* get_address_of_damageAmount_5() { return &___damageAmount_5; }
	inline void set_damageAmount_5(float value)
	{
		___damageAmount_5 = value;
	}

	inline static int32_t get_offset_of_clips_6() { return static_cast<int32_t>(offsetof(ArrowBehavior_t1475592159, ___clips_6)); }
	inline AudioClipU5BU5D_t143221404* get_clips_6() const { return ___clips_6; }
	inline AudioClipU5BU5D_t143221404** get_address_of_clips_6() { return &___clips_6; }
	inline void set_clips_6(AudioClipU5BU5D_t143221404* value)
	{
		___clips_6 = value;
		Il2CppCodeGenWriteBarrier((&___clips_6), value);
	}

	inline static int32_t get_offset_of_P1H_7() { return static_cast<int32_t>(offsetof(ArrowBehavior_t1475592159, ___P1H_7)); }
	inline PlayerHealth_t2068385516 * get_P1H_7() const { return ___P1H_7; }
	inline PlayerHealth_t2068385516 ** get_address_of_P1H_7() { return &___P1H_7; }
	inline void set_P1H_7(PlayerHealth_t2068385516 * value)
	{
		___P1H_7 = value;
		Il2CppCodeGenWriteBarrier((&___P1H_7), value);
	}

	inline static int32_t get_offset_of_P2H_8() { return static_cast<int32_t>(offsetof(ArrowBehavior_t1475592159, ___P2H_8)); }
	inline PlayerHealth_t2068385516 * get_P2H_8() const { return ___P2H_8; }
	inline PlayerHealth_t2068385516 ** get_address_of_P2H_8() { return &___P2H_8; }
	inline void set_P2H_8(PlayerHealth_t2068385516 * value)
	{
		___P2H_8 = value;
		Il2CppCodeGenWriteBarrier((&___P2H_8), value);
	}

	inline static int32_t get_offset_of_P1_9() { return static_cast<int32_t>(offsetof(ArrowBehavior_t1475592159, ___P1_9)); }
	inline GameObject_t1113636619 * get_P1_9() const { return ___P1_9; }
	inline GameObject_t1113636619 ** get_address_of_P1_9() { return &___P1_9; }
	inline void set_P1_9(GameObject_t1113636619 * value)
	{
		___P1_9 = value;
		Il2CppCodeGenWriteBarrier((&___P1_9), value);
	}

	inline static int32_t get_offset_of_P2_10() { return static_cast<int32_t>(offsetof(ArrowBehavior_t1475592159, ___P2_10)); }
	inline GameObject_t1113636619 * get_P2_10() const { return ___P2_10; }
	inline GameObject_t1113636619 ** get_address_of_P2_10() { return &___P2_10; }
	inline void set_P2_10(GameObject_t1113636619 * value)
	{
		___P2_10 = value;
		Il2CppCodeGenWriteBarrier((&___P2_10), value);
	}

	inline static int32_t get_offset_of_AS_11() { return static_cast<int32_t>(offsetof(ArrowBehavior_t1475592159, ___AS_11)); }
	inline AudioSource_t3935305588 * get_AS_11() const { return ___AS_11; }
	inline AudioSource_t3935305588 ** get_address_of_AS_11() { return &___AS_11; }
	inline void set_AS_11(AudioSource_t3935305588 * value)
	{
		___AS_11 = value;
		Il2CppCodeGenWriteBarrier((&___AS_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWBEHAVIOR_T1475592159_H
#ifndef CAMCONTROL_T437195931_H
#define CAMCONTROL_T437195931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CamControl
struct  CamControl_t437195931  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CamControl::player
	GameObject_t1113636619 * ___player_4;
	// UnityEngine.Vector3 CamControl::offset
	Vector3_t3722313464  ___offset_5;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(CamControl_t437195931, ___player_4)); }
	inline GameObject_t1113636619 * get_player_4() const { return ___player_4; }
	inline GameObject_t1113636619 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_t1113636619 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(CamControl_t437195931, ___offset_5)); }
	inline Vector3_t3722313464  get_offset_5() const { return ___offset_5; }
	inline Vector3_t3722313464 * get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(Vector3_t3722313464  value)
	{
		___offset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMCONTROL_T437195931_H
#ifndef CAMROTATE_T2888940566_H
#define CAMROTATE_T2888940566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CamRotate
struct  CamRotate_t2888940566  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CamRotate::speed
	float ___speed_4;
	// UnityEngine.Transform CamRotate::target
	Transform_t3600365921 * ___target_5;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(CamRotate_t2888940566, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(CamRotate_t2888940566, ___target_5)); }
	inline Transform_t3600365921 * get_target_5() const { return ___target_5; }
	inline Transform_t3600365921 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Transform_t3600365921 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((&___target_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMROTATE_T2888940566_H
#ifndef CAMSWITCHER_T2345021740_H
#define CAMSWITCHER_T2345021740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CamSwitcher
struct  CamSwitcher_t2345021740  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CamSwitcher::player1
	bool ___player1_4;
	// System.Boolean CamSwitcher::player2
	bool ___player2_5;
	// UnityEngine.Camera CamSwitcher::cam1
	Camera_t4157153871 * ___cam1_6;
	// UnityEngine.Camera CamSwitcher::cam2
	Camera_t4157153871 * ___cam2_7;
	// UnityEngine.GameObject CamSwitcher::p1
	GameObject_t1113636619 * ___p1_8;
	// UnityEngine.GameObject CamSwitcher::p2
	GameObject_t1113636619 * ___p2_9;
	// WarriorAnimationDemoFREE CamSwitcher::WADF
	WarriorAnimationDemoFREE_t1507088667 * ___WADF_10;
	// WarriorP2 CamSwitcher::WP2
	WarriorP2_t1586698613 * ___WP2_11;
	// UnityEngine.Transform CamSwitcher::P1R1
	Transform_t3600365921 * ___P1R1_12;
	// UnityEngine.Transform CamSwitcher::P1R2
	Transform_t3600365921 * ___P1R2_13;
	// UnityEngine.Transform CamSwitcher::P2R1
	Transform_t3600365921 * ___P2R1_14;
	// UnityEngine.Transform CamSwitcher::P2R2
	Transform_t3600365921 * ___P2R2_15;

public:
	inline static int32_t get_offset_of_player1_4() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___player1_4)); }
	inline bool get_player1_4() const { return ___player1_4; }
	inline bool* get_address_of_player1_4() { return &___player1_4; }
	inline void set_player1_4(bool value)
	{
		___player1_4 = value;
	}

	inline static int32_t get_offset_of_player2_5() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___player2_5)); }
	inline bool get_player2_5() const { return ___player2_5; }
	inline bool* get_address_of_player2_5() { return &___player2_5; }
	inline void set_player2_5(bool value)
	{
		___player2_5 = value;
	}

	inline static int32_t get_offset_of_cam1_6() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___cam1_6)); }
	inline Camera_t4157153871 * get_cam1_6() const { return ___cam1_6; }
	inline Camera_t4157153871 ** get_address_of_cam1_6() { return &___cam1_6; }
	inline void set_cam1_6(Camera_t4157153871 * value)
	{
		___cam1_6 = value;
		Il2CppCodeGenWriteBarrier((&___cam1_6), value);
	}

	inline static int32_t get_offset_of_cam2_7() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___cam2_7)); }
	inline Camera_t4157153871 * get_cam2_7() const { return ___cam2_7; }
	inline Camera_t4157153871 ** get_address_of_cam2_7() { return &___cam2_7; }
	inline void set_cam2_7(Camera_t4157153871 * value)
	{
		___cam2_7 = value;
		Il2CppCodeGenWriteBarrier((&___cam2_7), value);
	}

	inline static int32_t get_offset_of_p1_8() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___p1_8)); }
	inline GameObject_t1113636619 * get_p1_8() const { return ___p1_8; }
	inline GameObject_t1113636619 ** get_address_of_p1_8() { return &___p1_8; }
	inline void set_p1_8(GameObject_t1113636619 * value)
	{
		___p1_8 = value;
		Il2CppCodeGenWriteBarrier((&___p1_8), value);
	}

	inline static int32_t get_offset_of_p2_9() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___p2_9)); }
	inline GameObject_t1113636619 * get_p2_9() const { return ___p2_9; }
	inline GameObject_t1113636619 ** get_address_of_p2_9() { return &___p2_9; }
	inline void set_p2_9(GameObject_t1113636619 * value)
	{
		___p2_9 = value;
		Il2CppCodeGenWriteBarrier((&___p2_9), value);
	}

	inline static int32_t get_offset_of_WADF_10() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___WADF_10)); }
	inline WarriorAnimationDemoFREE_t1507088667 * get_WADF_10() const { return ___WADF_10; }
	inline WarriorAnimationDemoFREE_t1507088667 ** get_address_of_WADF_10() { return &___WADF_10; }
	inline void set_WADF_10(WarriorAnimationDemoFREE_t1507088667 * value)
	{
		___WADF_10 = value;
		Il2CppCodeGenWriteBarrier((&___WADF_10), value);
	}

	inline static int32_t get_offset_of_WP2_11() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___WP2_11)); }
	inline WarriorP2_t1586698613 * get_WP2_11() const { return ___WP2_11; }
	inline WarriorP2_t1586698613 ** get_address_of_WP2_11() { return &___WP2_11; }
	inline void set_WP2_11(WarriorP2_t1586698613 * value)
	{
		___WP2_11 = value;
		Il2CppCodeGenWriteBarrier((&___WP2_11), value);
	}

	inline static int32_t get_offset_of_P1R1_12() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___P1R1_12)); }
	inline Transform_t3600365921 * get_P1R1_12() const { return ___P1R1_12; }
	inline Transform_t3600365921 ** get_address_of_P1R1_12() { return &___P1R1_12; }
	inline void set_P1R1_12(Transform_t3600365921 * value)
	{
		___P1R1_12 = value;
		Il2CppCodeGenWriteBarrier((&___P1R1_12), value);
	}

	inline static int32_t get_offset_of_P1R2_13() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___P1R2_13)); }
	inline Transform_t3600365921 * get_P1R2_13() const { return ___P1R2_13; }
	inline Transform_t3600365921 ** get_address_of_P1R2_13() { return &___P1R2_13; }
	inline void set_P1R2_13(Transform_t3600365921 * value)
	{
		___P1R2_13 = value;
		Il2CppCodeGenWriteBarrier((&___P1R2_13), value);
	}

	inline static int32_t get_offset_of_P2R1_14() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___P2R1_14)); }
	inline Transform_t3600365921 * get_P2R1_14() const { return ___P2R1_14; }
	inline Transform_t3600365921 ** get_address_of_P2R1_14() { return &___P2R1_14; }
	inline void set_P2R1_14(Transform_t3600365921 * value)
	{
		___P2R1_14 = value;
		Il2CppCodeGenWriteBarrier((&___P2R1_14), value);
	}

	inline static int32_t get_offset_of_P2R2_15() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740, ___P2R2_15)); }
	inline Transform_t3600365921 * get_P2R2_15() const { return ___P2R2_15; }
	inline Transform_t3600365921 ** get_address_of_P2R2_15() { return &___P2R2_15; }
	inline void set_P2R2_15(Transform_t3600365921 * value)
	{
		___P2R2_15 = value;
		Il2CppCodeGenWriteBarrier((&___P2R2_15), value);
	}
};

struct CamSwitcher_t2345021740_StaticFields
{
public:
	// System.Boolean CamSwitcher::P1InRoom1
	bool ___P1InRoom1_16;
	// System.Boolean CamSwitcher::P2InRoom1
	bool ___P2InRoom1_17;

public:
	inline static int32_t get_offset_of_P1InRoom1_16() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740_StaticFields, ___P1InRoom1_16)); }
	inline bool get_P1InRoom1_16() const { return ___P1InRoom1_16; }
	inline bool* get_address_of_P1InRoom1_16() { return &___P1InRoom1_16; }
	inline void set_P1InRoom1_16(bool value)
	{
		___P1InRoom1_16 = value;
	}

	inline static int32_t get_offset_of_P2InRoom1_17() { return static_cast<int32_t>(offsetof(CamSwitcher_t2345021740_StaticFields, ___P2InRoom1_17)); }
	inline bool get_P2InRoom1_17() const { return ___P2InRoom1_17; }
	inline bool* get_address_of_P2InRoom1_17() { return &___P2InRoom1_17; }
	inline void set_P2InRoom1_17(bool value)
	{
		___P2InRoom1_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMSWITCHER_T2345021740_H
#ifndef CAMSWITCHER2_T1376530732_H
#define CAMSWITCHER2_T1376530732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CamSwitcher2
struct  CamSwitcher2_t1376530732  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CamSwitcher2::player1
	bool ___player1_4;
	// System.Boolean CamSwitcher2::player2
	bool ___player2_5;
	// UnityEngine.Camera CamSwitcher2::cam1
	Camera_t4157153871 * ___cam1_6;
	// UnityEngine.Camera CamSwitcher2::cam2
	Camera_t4157153871 * ___cam2_7;
	// UnityEngine.GameObject CamSwitcher2::p1
	GameObject_t1113636619 * ___p1_8;
	// UnityEngine.GameObject CamSwitcher2::p2
	GameObject_t1113636619 * ___p2_9;
	// WarriorAnimationDemoFREE CamSwitcher2::WADF
	WarriorAnimationDemoFREE_t1507088667 * ___WADF_10;
	// WarriorP2 CamSwitcher2::WP2
	WarriorP2_t1586698613 * ___WP2_11;
	// UnityEngine.Transform CamSwitcher2::P1R1
	Transform_t3600365921 * ___P1R1_12;
	// UnityEngine.Transform CamSwitcher2::P1R2
	Transform_t3600365921 * ___P1R2_13;
	// UnityEngine.Transform CamSwitcher2::P2R1
	Transform_t3600365921 * ___P2R1_14;
	// UnityEngine.Transform CamSwitcher2::P2R2
	Transform_t3600365921 * ___P2R2_15;
	// System.Boolean CamSwitcher2::P1InRoom1
	bool ___P1InRoom1_16;
	// System.Boolean CamSwitcher2::P2InRoom1
	bool ___P2InRoom1_17;

public:
	inline static int32_t get_offset_of_player1_4() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___player1_4)); }
	inline bool get_player1_4() const { return ___player1_4; }
	inline bool* get_address_of_player1_4() { return &___player1_4; }
	inline void set_player1_4(bool value)
	{
		___player1_4 = value;
	}

	inline static int32_t get_offset_of_player2_5() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___player2_5)); }
	inline bool get_player2_5() const { return ___player2_5; }
	inline bool* get_address_of_player2_5() { return &___player2_5; }
	inline void set_player2_5(bool value)
	{
		___player2_5 = value;
	}

	inline static int32_t get_offset_of_cam1_6() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___cam1_6)); }
	inline Camera_t4157153871 * get_cam1_6() const { return ___cam1_6; }
	inline Camera_t4157153871 ** get_address_of_cam1_6() { return &___cam1_6; }
	inline void set_cam1_6(Camera_t4157153871 * value)
	{
		___cam1_6 = value;
		Il2CppCodeGenWriteBarrier((&___cam1_6), value);
	}

	inline static int32_t get_offset_of_cam2_7() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___cam2_7)); }
	inline Camera_t4157153871 * get_cam2_7() const { return ___cam2_7; }
	inline Camera_t4157153871 ** get_address_of_cam2_7() { return &___cam2_7; }
	inline void set_cam2_7(Camera_t4157153871 * value)
	{
		___cam2_7 = value;
		Il2CppCodeGenWriteBarrier((&___cam2_7), value);
	}

	inline static int32_t get_offset_of_p1_8() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___p1_8)); }
	inline GameObject_t1113636619 * get_p1_8() const { return ___p1_8; }
	inline GameObject_t1113636619 ** get_address_of_p1_8() { return &___p1_8; }
	inline void set_p1_8(GameObject_t1113636619 * value)
	{
		___p1_8 = value;
		Il2CppCodeGenWriteBarrier((&___p1_8), value);
	}

	inline static int32_t get_offset_of_p2_9() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___p2_9)); }
	inline GameObject_t1113636619 * get_p2_9() const { return ___p2_9; }
	inline GameObject_t1113636619 ** get_address_of_p2_9() { return &___p2_9; }
	inline void set_p2_9(GameObject_t1113636619 * value)
	{
		___p2_9 = value;
		Il2CppCodeGenWriteBarrier((&___p2_9), value);
	}

	inline static int32_t get_offset_of_WADF_10() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___WADF_10)); }
	inline WarriorAnimationDemoFREE_t1507088667 * get_WADF_10() const { return ___WADF_10; }
	inline WarriorAnimationDemoFREE_t1507088667 ** get_address_of_WADF_10() { return &___WADF_10; }
	inline void set_WADF_10(WarriorAnimationDemoFREE_t1507088667 * value)
	{
		___WADF_10 = value;
		Il2CppCodeGenWriteBarrier((&___WADF_10), value);
	}

	inline static int32_t get_offset_of_WP2_11() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___WP2_11)); }
	inline WarriorP2_t1586698613 * get_WP2_11() const { return ___WP2_11; }
	inline WarriorP2_t1586698613 ** get_address_of_WP2_11() { return &___WP2_11; }
	inline void set_WP2_11(WarriorP2_t1586698613 * value)
	{
		___WP2_11 = value;
		Il2CppCodeGenWriteBarrier((&___WP2_11), value);
	}

	inline static int32_t get_offset_of_P1R1_12() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___P1R1_12)); }
	inline Transform_t3600365921 * get_P1R1_12() const { return ___P1R1_12; }
	inline Transform_t3600365921 ** get_address_of_P1R1_12() { return &___P1R1_12; }
	inline void set_P1R1_12(Transform_t3600365921 * value)
	{
		___P1R1_12 = value;
		Il2CppCodeGenWriteBarrier((&___P1R1_12), value);
	}

	inline static int32_t get_offset_of_P1R2_13() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___P1R2_13)); }
	inline Transform_t3600365921 * get_P1R2_13() const { return ___P1R2_13; }
	inline Transform_t3600365921 ** get_address_of_P1R2_13() { return &___P1R2_13; }
	inline void set_P1R2_13(Transform_t3600365921 * value)
	{
		___P1R2_13 = value;
		Il2CppCodeGenWriteBarrier((&___P1R2_13), value);
	}

	inline static int32_t get_offset_of_P2R1_14() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___P2R1_14)); }
	inline Transform_t3600365921 * get_P2R1_14() const { return ___P2R1_14; }
	inline Transform_t3600365921 ** get_address_of_P2R1_14() { return &___P2R1_14; }
	inline void set_P2R1_14(Transform_t3600365921 * value)
	{
		___P2R1_14 = value;
		Il2CppCodeGenWriteBarrier((&___P2R1_14), value);
	}

	inline static int32_t get_offset_of_P2R2_15() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___P2R2_15)); }
	inline Transform_t3600365921 * get_P2R2_15() const { return ___P2R2_15; }
	inline Transform_t3600365921 ** get_address_of_P2R2_15() { return &___P2R2_15; }
	inline void set_P2R2_15(Transform_t3600365921 * value)
	{
		___P2R2_15 = value;
		Il2CppCodeGenWriteBarrier((&___P2R2_15), value);
	}

	inline static int32_t get_offset_of_P1InRoom1_16() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___P1InRoom1_16)); }
	inline bool get_P1InRoom1_16() const { return ___P1InRoom1_16; }
	inline bool* get_address_of_P1InRoom1_16() { return &___P1InRoom1_16; }
	inline void set_P1InRoom1_16(bool value)
	{
		___P1InRoom1_16 = value;
	}

	inline static int32_t get_offset_of_P2InRoom1_17() { return static_cast<int32_t>(offsetof(CamSwitcher2_t1376530732, ___P2InRoom1_17)); }
	inline bool get_P2InRoom1_17() const { return ___P2InRoom1_17; }
	inline bool* get_address_of_P2InRoom1_17() { return &___P2InRoom1_17; }
	inline void set_P2InRoom1_17(bool value)
	{
		___P2InRoom1_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMSWITCHER2_T1376530732_H
#ifndef CASACONTROL_T3217110703_H
#define CASACONTROL_T3217110703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CasaControl
struct  CasaControl_t3217110703  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CasaControl::P1
	Transform_t3600365921 * ___P1_4;
	// UnityEngine.Transform CasaControl::P2
	Transform_t3600365921 * ___P2_5;
	// UnityEngine.Transform CasaControl::P1P2
	Transform_t3600365921 * ___P1P2_6;
	// UnityEngine.Transform CasaControl::P2P2
	Transform_t3600365921 * ___P2P2_7;

public:
	inline static int32_t get_offset_of_P1_4() { return static_cast<int32_t>(offsetof(CasaControl_t3217110703, ___P1_4)); }
	inline Transform_t3600365921 * get_P1_4() const { return ___P1_4; }
	inline Transform_t3600365921 ** get_address_of_P1_4() { return &___P1_4; }
	inline void set_P1_4(Transform_t3600365921 * value)
	{
		___P1_4 = value;
		Il2CppCodeGenWriteBarrier((&___P1_4), value);
	}

	inline static int32_t get_offset_of_P2_5() { return static_cast<int32_t>(offsetof(CasaControl_t3217110703, ___P2_5)); }
	inline Transform_t3600365921 * get_P2_5() const { return ___P2_5; }
	inline Transform_t3600365921 ** get_address_of_P2_5() { return &___P2_5; }
	inline void set_P2_5(Transform_t3600365921 * value)
	{
		___P2_5 = value;
		Il2CppCodeGenWriteBarrier((&___P2_5), value);
	}

	inline static int32_t get_offset_of_P1P2_6() { return static_cast<int32_t>(offsetof(CasaControl_t3217110703, ___P1P2_6)); }
	inline Transform_t3600365921 * get_P1P2_6() const { return ___P1P2_6; }
	inline Transform_t3600365921 ** get_address_of_P1P2_6() { return &___P1P2_6; }
	inline void set_P1P2_6(Transform_t3600365921 * value)
	{
		___P1P2_6 = value;
		Il2CppCodeGenWriteBarrier((&___P1P2_6), value);
	}

	inline static int32_t get_offset_of_P2P2_7() { return static_cast<int32_t>(offsetof(CasaControl_t3217110703, ___P2P2_7)); }
	inline Transform_t3600365921 * get_P2P2_7() const { return ___P2P2_7; }
	inline Transform_t3600365921 ** get_address_of_P2P2_7() { return &___P2P2_7; }
	inline void set_P2P2_7(Transform_t3600365921 * value)
	{
		___P2P2_7 = value;
		Il2CppCodeGenWriteBarrier((&___P2P2_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASACONTROL_T3217110703_H
#ifndef CHARCONTROLLER_MOTOR_T3258469724_H
#define CHARCONTROLLER_MOTOR_T3258469724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharController_Motor
struct  CharController_Motor_t3258469724  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CharController_Motor::speed
	float ___speed_4;
	// System.Single CharController_Motor::sensitivity
	float ___sensitivity_5;
	// System.Single CharController_Motor::WaterHeight
	float ___WaterHeight_6;
	// UnityEngine.CharacterController CharController_Motor::character
	CharacterController_t1138636865 * ___character_7;
	// UnityEngine.GameObject CharController_Motor::cam
	GameObject_t1113636619 * ___cam_8;
	// System.Single CharController_Motor::moveFB
	float ___moveFB_9;
	// System.Single CharController_Motor::moveLR
	float ___moveLR_10;
	// System.Single CharController_Motor::rotX
	float ___rotX_11;
	// System.Single CharController_Motor::rotY
	float ___rotY_12;
	// System.Boolean CharController_Motor::webGLRightClickRotation
	bool ___webGLRightClickRotation_13;
	// System.Single CharController_Motor::gravity
	float ___gravity_14;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(CharController_Motor_t3258469724, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_sensitivity_5() { return static_cast<int32_t>(offsetof(CharController_Motor_t3258469724, ___sensitivity_5)); }
	inline float get_sensitivity_5() const { return ___sensitivity_5; }
	inline float* get_address_of_sensitivity_5() { return &___sensitivity_5; }
	inline void set_sensitivity_5(float value)
	{
		___sensitivity_5 = value;
	}

	inline static int32_t get_offset_of_WaterHeight_6() { return static_cast<int32_t>(offsetof(CharController_Motor_t3258469724, ___WaterHeight_6)); }
	inline float get_WaterHeight_6() const { return ___WaterHeight_6; }
	inline float* get_address_of_WaterHeight_6() { return &___WaterHeight_6; }
	inline void set_WaterHeight_6(float value)
	{
		___WaterHeight_6 = value;
	}

	inline static int32_t get_offset_of_character_7() { return static_cast<int32_t>(offsetof(CharController_Motor_t3258469724, ___character_7)); }
	inline CharacterController_t1138636865 * get_character_7() const { return ___character_7; }
	inline CharacterController_t1138636865 ** get_address_of_character_7() { return &___character_7; }
	inline void set_character_7(CharacterController_t1138636865 * value)
	{
		___character_7 = value;
		Il2CppCodeGenWriteBarrier((&___character_7), value);
	}

	inline static int32_t get_offset_of_cam_8() { return static_cast<int32_t>(offsetof(CharController_Motor_t3258469724, ___cam_8)); }
	inline GameObject_t1113636619 * get_cam_8() const { return ___cam_8; }
	inline GameObject_t1113636619 ** get_address_of_cam_8() { return &___cam_8; }
	inline void set_cam_8(GameObject_t1113636619 * value)
	{
		___cam_8 = value;
		Il2CppCodeGenWriteBarrier((&___cam_8), value);
	}

	inline static int32_t get_offset_of_moveFB_9() { return static_cast<int32_t>(offsetof(CharController_Motor_t3258469724, ___moveFB_9)); }
	inline float get_moveFB_9() const { return ___moveFB_9; }
	inline float* get_address_of_moveFB_9() { return &___moveFB_9; }
	inline void set_moveFB_9(float value)
	{
		___moveFB_9 = value;
	}

	inline static int32_t get_offset_of_moveLR_10() { return static_cast<int32_t>(offsetof(CharController_Motor_t3258469724, ___moveLR_10)); }
	inline float get_moveLR_10() const { return ___moveLR_10; }
	inline float* get_address_of_moveLR_10() { return &___moveLR_10; }
	inline void set_moveLR_10(float value)
	{
		___moveLR_10 = value;
	}

	inline static int32_t get_offset_of_rotX_11() { return static_cast<int32_t>(offsetof(CharController_Motor_t3258469724, ___rotX_11)); }
	inline float get_rotX_11() const { return ___rotX_11; }
	inline float* get_address_of_rotX_11() { return &___rotX_11; }
	inline void set_rotX_11(float value)
	{
		___rotX_11 = value;
	}

	inline static int32_t get_offset_of_rotY_12() { return static_cast<int32_t>(offsetof(CharController_Motor_t3258469724, ___rotY_12)); }
	inline float get_rotY_12() const { return ___rotY_12; }
	inline float* get_address_of_rotY_12() { return &___rotY_12; }
	inline void set_rotY_12(float value)
	{
		___rotY_12 = value;
	}

	inline static int32_t get_offset_of_webGLRightClickRotation_13() { return static_cast<int32_t>(offsetof(CharController_Motor_t3258469724, ___webGLRightClickRotation_13)); }
	inline bool get_webGLRightClickRotation_13() const { return ___webGLRightClickRotation_13; }
	inline bool* get_address_of_webGLRightClickRotation_13() { return &___webGLRightClickRotation_13; }
	inline void set_webGLRightClickRotation_13(bool value)
	{
		___webGLRightClickRotation_13 = value;
	}

	inline static int32_t get_offset_of_gravity_14() { return static_cast<int32_t>(offsetof(CharController_Motor_t3258469724, ___gravity_14)); }
	inline float get_gravity_14() const { return ___gravity_14; }
	inline float* get_address_of_gravity_14() { return &___gravity_14; }
	inline void set_gravity_14(float value)
	{
		___gravity_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARCONTROLLER_MOTOR_T3258469724_H
#ifndef DESTROYSELF_T3777301674_H
#define DESTROYSELF_T3777301674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroySelf
struct  DestroySelf_t3777301674  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYSELF_T3777301674_H
#ifndef DISABLERENDERER_T123364433_H
#define DISABLERENDERER_T123364433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisableRenderer
struct  DisableRenderer_t123364433  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLERENDERER_T123364433_H
#ifndef ENEMYATTACK_T24081459_H
#define ENEMYATTACK_T24081459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyAttack
struct  EnemyAttack_t24081459  : public MonoBehaviour_t3962482529
{
public:
	// System.Single EnemyAttack::nextAttack
	float ___nextAttack_4;
	// UnityEngine.Animator EnemyAttack::anim
	Animator_t434523843 * ___anim_5;
	// UnityEngine.AudioSource EnemyAttack::AS
	AudioSource_t3935305588 * ___AS_6;
	// UnityEngine.AudioClip[] EnemyAttack::playerHurtClips
	AudioClipU5BU5D_t143221404* ___playerHurtClips_7;
	// PlayerHealth EnemyAttack::P1H
	PlayerHealth_t2068385516 * ___P1H_8;
	// PlayerHealth EnemyAttack::P2H
	PlayerHealth_t2068385516 * ___P2H_9;
	// System.Boolean EnemyAttack::P1InRange
	bool ___P1InRange_10;
	// System.Boolean EnemyAttack::P2InRange
	bool ___P2InRange_11;
	// ManageScene EnemyAttack::MS
	ManageScene_t3196455047 * ___MS_12;
	// System.Single EnemyAttack::attackRate
	float ___attackRate_13;
	// System.Single EnemyAttack::soundDelay
	float ___soundDelay_14;
	// System.Single EnemyAttack::damageAmount
	float ___damageAmount_15;

public:
	inline static int32_t get_offset_of_nextAttack_4() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___nextAttack_4)); }
	inline float get_nextAttack_4() const { return ___nextAttack_4; }
	inline float* get_address_of_nextAttack_4() { return &___nextAttack_4; }
	inline void set_nextAttack_4(float value)
	{
		___nextAttack_4 = value;
	}

	inline static int32_t get_offset_of_anim_5() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___anim_5)); }
	inline Animator_t434523843 * get_anim_5() const { return ___anim_5; }
	inline Animator_t434523843 ** get_address_of_anim_5() { return &___anim_5; }
	inline void set_anim_5(Animator_t434523843 * value)
	{
		___anim_5 = value;
		Il2CppCodeGenWriteBarrier((&___anim_5), value);
	}

	inline static int32_t get_offset_of_AS_6() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___AS_6)); }
	inline AudioSource_t3935305588 * get_AS_6() const { return ___AS_6; }
	inline AudioSource_t3935305588 ** get_address_of_AS_6() { return &___AS_6; }
	inline void set_AS_6(AudioSource_t3935305588 * value)
	{
		___AS_6 = value;
		Il2CppCodeGenWriteBarrier((&___AS_6), value);
	}

	inline static int32_t get_offset_of_playerHurtClips_7() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___playerHurtClips_7)); }
	inline AudioClipU5BU5D_t143221404* get_playerHurtClips_7() const { return ___playerHurtClips_7; }
	inline AudioClipU5BU5D_t143221404** get_address_of_playerHurtClips_7() { return &___playerHurtClips_7; }
	inline void set_playerHurtClips_7(AudioClipU5BU5D_t143221404* value)
	{
		___playerHurtClips_7 = value;
		Il2CppCodeGenWriteBarrier((&___playerHurtClips_7), value);
	}

	inline static int32_t get_offset_of_P1H_8() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___P1H_8)); }
	inline PlayerHealth_t2068385516 * get_P1H_8() const { return ___P1H_8; }
	inline PlayerHealth_t2068385516 ** get_address_of_P1H_8() { return &___P1H_8; }
	inline void set_P1H_8(PlayerHealth_t2068385516 * value)
	{
		___P1H_8 = value;
		Il2CppCodeGenWriteBarrier((&___P1H_8), value);
	}

	inline static int32_t get_offset_of_P2H_9() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___P2H_9)); }
	inline PlayerHealth_t2068385516 * get_P2H_9() const { return ___P2H_9; }
	inline PlayerHealth_t2068385516 ** get_address_of_P2H_9() { return &___P2H_9; }
	inline void set_P2H_9(PlayerHealth_t2068385516 * value)
	{
		___P2H_9 = value;
		Il2CppCodeGenWriteBarrier((&___P2H_9), value);
	}

	inline static int32_t get_offset_of_P1InRange_10() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___P1InRange_10)); }
	inline bool get_P1InRange_10() const { return ___P1InRange_10; }
	inline bool* get_address_of_P1InRange_10() { return &___P1InRange_10; }
	inline void set_P1InRange_10(bool value)
	{
		___P1InRange_10 = value;
	}

	inline static int32_t get_offset_of_P2InRange_11() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___P2InRange_11)); }
	inline bool get_P2InRange_11() const { return ___P2InRange_11; }
	inline bool* get_address_of_P2InRange_11() { return &___P2InRange_11; }
	inline void set_P2InRange_11(bool value)
	{
		___P2InRange_11 = value;
	}

	inline static int32_t get_offset_of_MS_12() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___MS_12)); }
	inline ManageScene_t3196455047 * get_MS_12() const { return ___MS_12; }
	inline ManageScene_t3196455047 ** get_address_of_MS_12() { return &___MS_12; }
	inline void set_MS_12(ManageScene_t3196455047 * value)
	{
		___MS_12 = value;
		Il2CppCodeGenWriteBarrier((&___MS_12), value);
	}

	inline static int32_t get_offset_of_attackRate_13() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___attackRate_13)); }
	inline float get_attackRate_13() const { return ___attackRate_13; }
	inline float* get_address_of_attackRate_13() { return &___attackRate_13; }
	inline void set_attackRate_13(float value)
	{
		___attackRate_13 = value;
	}

	inline static int32_t get_offset_of_soundDelay_14() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___soundDelay_14)); }
	inline float get_soundDelay_14() const { return ___soundDelay_14; }
	inline float* get_address_of_soundDelay_14() { return &___soundDelay_14; }
	inline void set_soundDelay_14(float value)
	{
		___soundDelay_14 = value;
	}

	inline static int32_t get_offset_of_damageAmount_15() { return static_cast<int32_t>(offsetof(EnemyAttack_t24081459, ___damageAmount_15)); }
	inline float get_damageAmount_15() const { return ___damageAmount_15; }
	inline float* get_address_of_damageAmount_15() { return &___damageAmount_15; }
	inline void set_damageAmount_15(float value)
	{
		___damageAmount_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYATTACK_T24081459_H
#ifndef ENEMYATTACKSINGLEPLAYER_T4127103160_H
#define ENEMYATTACKSINGLEPLAYER_T4127103160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyAttackSinglePlayer
struct  EnemyAttackSinglePlayer_t4127103160  : public MonoBehaviour_t3962482529
{
public:
	// System.Single EnemyAttackSinglePlayer::nextAttack
	float ___nextAttack_4;
	// UnityEngine.Animator EnemyAttackSinglePlayer::anim
	Animator_t434523843 * ___anim_5;
	// UnityEngine.AudioSource EnemyAttackSinglePlayer::AS
	AudioSource_t3935305588 * ___AS_6;
	// UnityEngine.AudioClip[] EnemyAttackSinglePlayer::playerHurtClips
	AudioClipU5BU5D_t143221404* ___playerHurtClips_7;
	// PlayerHealth EnemyAttackSinglePlayer::P1H
	PlayerHealth_t2068385516 * ___P1H_8;
	// System.Boolean EnemyAttackSinglePlayer::P1InRange
	bool ___P1InRange_9;
	// ManageScene EnemyAttackSinglePlayer::MS
	ManageScene_t3196455047 * ___MS_10;
	// System.Single EnemyAttackSinglePlayer::attackRate
	float ___attackRate_11;
	// System.Single EnemyAttackSinglePlayer::soundDelay
	float ___soundDelay_12;
	// System.Single EnemyAttackSinglePlayer::damageAmount
	float ___damageAmount_13;

public:
	inline static int32_t get_offset_of_nextAttack_4() { return static_cast<int32_t>(offsetof(EnemyAttackSinglePlayer_t4127103160, ___nextAttack_4)); }
	inline float get_nextAttack_4() const { return ___nextAttack_4; }
	inline float* get_address_of_nextAttack_4() { return &___nextAttack_4; }
	inline void set_nextAttack_4(float value)
	{
		___nextAttack_4 = value;
	}

	inline static int32_t get_offset_of_anim_5() { return static_cast<int32_t>(offsetof(EnemyAttackSinglePlayer_t4127103160, ___anim_5)); }
	inline Animator_t434523843 * get_anim_5() const { return ___anim_5; }
	inline Animator_t434523843 ** get_address_of_anim_5() { return &___anim_5; }
	inline void set_anim_5(Animator_t434523843 * value)
	{
		___anim_5 = value;
		Il2CppCodeGenWriteBarrier((&___anim_5), value);
	}

	inline static int32_t get_offset_of_AS_6() { return static_cast<int32_t>(offsetof(EnemyAttackSinglePlayer_t4127103160, ___AS_6)); }
	inline AudioSource_t3935305588 * get_AS_6() const { return ___AS_6; }
	inline AudioSource_t3935305588 ** get_address_of_AS_6() { return &___AS_6; }
	inline void set_AS_6(AudioSource_t3935305588 * value)
	{
		___AS_6 = value;
		Il2CppCodeGenWriteBarrier((&___AS_6), value);
	}

	inline static int32_t get_offset_of_playerHurtClips_7() { return static_cast<int32_t>(offsetof(EnemyAttackSinglePlayer_t4127103160, ___playerHurtClips_7)); }
	inline AudioClipU5BU5D_t143221404* get_playerHurtClips_7() const { return ___playerHurtClips_7; }
	inline AudioClipU5BU5D_t143221404** get_address_of_playerHurtClips_7() { return &___playerHurtClips_7; }
	inline void set_playerHurtClips_7(AudioClipU5BU5D_t143221404* value)
	{
		___playerHurtClips_7 = value;
		Il2CppCodeGenWriteBarrier((&___playerHurtClips_7), value);
	}

	inline static int32_t get_offset_of_P1H_8() { return static_cast<int32_t>(offsetof(EnemyAttackSinglePlayer_t4127103160, ___P1H_8)); }
	inline PlayerHealth_t2068385516 * get_P1H_8() const { return ___P1H_8; }
	inline PlayerHealth_t2068385516 ** get_address_of_P1H_8() { return &___P1H_8; }
	inline void set_P1H_8(PlayerHealth_t2068385516 * value)
	{
		___P1H_8 = value;
		Il2CppCodeGenWriteBarrier((&___P1H_8), value);
	}

	inline static int32_t get_offset_of_P1InRange_9() { return static_cast<int32_t>(offsetof(EnemyAttackSinglePlayer_t4127103160, ___P1InRange_9)); }
	inline bool get_P1InRange_9() const { return ___P1InRange_9; }
	inline bool* get_address_of_P1InRange_9() { return &___P1InRange_9; }
	inline void set_P1InRange_9(bool value)
	{
		___P1InRange_9 = value;
	}

	inline static int32_t get_offset_of_MS_10() { return static_cast<int32_t>(offsetof(EnemyAttackSinglePlayer_t4127103160, ___MS_10)); }
	inline ManageScene_t3196455047 * get_MS_10() const { return ___MS_10; }
	inline ManageScene_t3196455047 ** get_address_of_MS_10() { return &___MS_10; }
	inline void set_MS_10(ManageScene_t3196455047 * value)
	{
		___MS_10 = value;
		Il2CppCodeGenWriteBarrier((&___MS_10), value);
	}

	inline static int32_t get_offset_of_attackRate_11() { return static_cast<int32_t>(offsetof(EnemyAttackSinglePlayer_t4127103160, ___attackRate_11)); }
	inline float get_attackRate_11() const { return ___attackRate_11; }
	inline float* get_address_of_attackRate_11() { return &___attackRate_11; }
	inline void set_attackRate_11(float value)
	{
		___attackRate_11 = value;
	}

	inline static int32_t get_offset_of_soundDelay_12() { return static_cast<int32_t>(offsetof(EnemyAttackSinglePlayer_t4127103160, ___soundDelay_12)); }
	inline float get_soundDelay_12() const { return ___soundDelay_12; }
	inline float* get_address_of_soundDelay_12() { return &___soundDelay_12; }
	inline void set_soundDelay_12(float value)
	{
		___soundDelay_12 = value;
	}

	inline static int32_t get_offset_of_damageAmount_13() { return static_cast<int32_t>(offsetof(EnemyAttackSinglePlayer_t4127103160, ___damageAmount_13)); }
	inline float get_damageAmount_13() const { return ___damageAmount_13; }
	inline float* get_address_of_damageAmount_13() { return &___damageAmount_13; }
	inline void set_damageAmount_13(float value)
	{
		___damageAmount_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYATTACKSINGLEPLAYER_T4127103160_H
#ifndef ENEMYNAVIGATION_T1695490929_H
#define ENEMYNAVIGATION_T1695490929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyNavigation
struct  EnemyNavigation_t1695490929  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AI.NavMeshAgent EnemyNavigation::<agent>k__BackingField
	NavMeshAgent_t1276799816 * ___U3CagentU3Ek__BackingField_4;
	// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter EnemyNavigation::<character>k__BackingField
	ThirdPersonCharacter_t1711070432 * ___U3CcharacterU3Ek__BackingField_5;
	// System.Single EnemyNavigation::enemyWalkSpeed
	float ___enemyWalkSpeed_6;
	// System.Single EnemyNavigation::enemyRunSpeed
	float ___enemyRunSpeed_7;
	// UnityEngine.GameObject[] EnemyNavigation::players
	GameObjectU5BU5D_t3328599146* ___players_8;
	// UnityEngine.GameObject[] EnemyNavigation::targets
	GameObjectU5BU5D_t3328599146* ___targets_9;
	// System.Single EnemyNavigation::attackRate
	float ___attackRate_11;
	// System.Single EnemyNavigation::soundDelay
	float ___soundDelay_12;
	// System.Single EnemyNavigation::damageAmount
	float ___damageAmount_13;
	// UnityEngine.GameObject EnemyNavigation::currentPlayer
	GameObject_t1113636619 * ___currentPlayer_14;
	// UnityEngine.GameObject EnemyNavigation::currentTarget
	GameObject_t1113636619 * ___currentTarget_15;
	// System.Single EnemyNavigation::timer
	float ___timer_16;
	// System.Single EnemyNavigation::timerRate
	float ___timerRate_17;
	// System.Single EnemyNavigation::nextAttack
	float ___nextAttack_18;
	// System.Single EnemyNavigation::playerDistance
	float ___playerDistance_19;
	// UnityEngine.Animator EnemyNavigation::anim
	Animator_t434523843 * ___anim_20;
	// System.Boolean EnemyNavigation::playerInRange
	bool ___playerInRange_21;
	// UnityEngine.AudioSource EnemyNavigation::AS
	AudioSource_t3935305588 * ___AS_22;
	// System.Int32 EnemyNavigation::index
	int32_t ___index_23;
	// UnityEngine.AudioClip[] EnemyNavigation::playerHurtClips
	AudioClipU5BU5D_t143221404* ___playerHurtClips_24;

public:
	inline static int32_t get_offset_of_U3CagentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___U3CagentU3Ek__BackingField_4)); }
	inline NavMeshAgent_t1276799816 * get_U3CagentU3Ek__BackingField_4() const { return ___U3CagentU3Ek__BackingField_4; }
	inline NavMeshAgent_t1276799816 ** get_address_of_U3CagentU3Ek__BackingField_4() { return &___U3CagentU3Ek__BackingField_4; }
	inline void set_U3CagentU3Ek__BackingField_4(NavMeshAgent_t1276799816 * value)
	{
		___U3CagentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CagentU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcharacterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___U3CcharacterU3Ek__BackingField_5)); }
	inline ThirdPersonCharacter_t1711070432 * get_U3CcharacterU3Ek__BackingField_5() const { return ___U3CcharacterU3Ek__BackingField_5; }
	inline ThirdPersonCharacter_t1711070432 ** get_address_of_U3CcharacterU3Ek__BackingField_5() { return &___U3CcharacterU3Ek__BackingField_5; }
	inline void set_U3CcharacterU3Ek__BackingField_5(ThirdPersonCharacter_t1711070432 * value)
	{
		___U3CcharacterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcharacterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_enemyWalkSpeed_6() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___enemyWalkSpeed_6)); }
	inline float get_enemyWalkSpeed_6() const { return ___enemyWalkSpeed_6; }
	inline float* get_address_of_enemyWalkSpeed_6() { return &___enemyWalkSpeed_6; }
	inline void set_enemyWalkSpeed_6(float value)
	{
		___enemyWalkSpeed_6 = value;
	}

	inline static int32_t get_offset_of_enemyRunSpeed_7() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___enemyRunSpeed_7)); }
	inline float get_enemyRunSpeed_7() const { return ___enemyRunSpeed_7; }
	inline float* get_address_of_enemyRunSpeed_7() { return &___enemyRunSpeed_7; }
	inline void set_enemyRunSpeed_7(float value)
	{
		___enemyRunSpeed_7 = value;
	}

	inline static int32_t get_offset_of_players_8() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___players_8)); }
	inline GameObjectU5BU5D_t3328599146* get_players_8() const { return ___players_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_players_8() { return &___players_8; }
	inline void set_players_8(GameObjectU5BU5D_t3328599146* value)
	{
		___players_8 = value;
		Il2CppCodeGenWriteBarrier((&___players_8), value);
	}

	inline static int32_t get_offset_of_targets_9() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___targets_9)); }
	inline GameObjectU5BU5D_t3328599146* get_targets_9() const { return ___targets_9; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_targets_9() { return &___targets_9; }
	inline void set_targets_9(GameObjectU5BU5D_t3328599146* value)
	{
		___targets_9 = value;
		Il2CppCodeGenWriteBarrier((&___targets_9), value);
	}

	inline static int32_t get_offset_of_attackRate_11() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___attackRate_11)); }
	inline float get_attackRate_11() const { return ___attackRate_11; }
	inline float* get_address_of_attackRate_11() { return &___attackRate_11; }
	inline void set_attackRate_11(float value)
	{
		___attackRate_11 = value;
	}

	inline static int32_t get_offset_of_soundDelay_12() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___soundDelay_12)); }
	inline float get_soundDelay_12() const { return ___soundDelay_12; }
	inline float* get_address_of_soundDelay_12() { return &___soundDelay_12; }
	inline void set_soundDelay_12(float value)
	{
		___soundDelay_12 = value;
	}

	inline static int32_t get_offset_of_damageAmount_13() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___damageAmount_13)); }
	inline float get_damageAmount_13() const { return ___damageAmount_13; }
	inline float* get_address_of_damageAmount_13() { return &___damageAmount_13; }
	inline void set_damageAmount_13(float value)
	{
		___damageAmount_13 = value;
	}

	inline static int32_t get_offset_of_currentPlayer_14() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___currentPlayer_14)); }
	inline GameObject_t1113636619 * get_currentPlayer_14() const { return ___currentPlayer_14; }
	inline GameObject_t1113636619 ** get_address_of_currentPlayer_14() { return &___currentPlayer_14; }
	inline void set_currentPlayer_14(GameObject_t1113636619 * value)
	{
		___currentPlayer_14 = value;
		Il2CppCodeGenWriteBarrier((&___currentPlayer_14), value);
	}

	inline static int32_t get_offset_of_currentTarget_15() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___currentTarget_15)); }
	inline GameObject_t1113636619 * get_currentTarget_15() const { return ___currentTarget_15; }
	inline GameObject_t1113636619 ** get_address_of_currentTarget_15() { return &___currentTarget_15; }
	inline void set_currentTarget_15(GameObject_t1113636619 * value)
	{
		___currentTarget_15 = value;
		Il2CppCodeGenWriteBarrier((&___currentTarget_15), value);
	}

	inline static int32_t get_offset_of_timer_16() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___timer_16)); }
	inline float get_timer_16() const { return ___timer_16; }
	inline float* get_address_of_timer_16() { return &___timer_16; }
	inline void set_timer_16(float value)
	{
		___timer_16 = value;
	}

	inline static int32_t get_offset_of_timerRate_17() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___timerRate_17)); }
	inline float get_timerRate_17() const { return ___timerRate_17; }
	inline float* get_address_of_timerRate_17() { return &___timerRate_17; }
	inline void set_timerRate_17(float value)
	{
		___timerRate_17 = value;
	}

	inline static int32_t get_offset_of_nextAttack_18() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___nextAttack_18)); }
	inline float get_nextAttack_18() const { return ___nextAttack_18; }
	inline float* get_address_of_nextAttack_18() { return &___nextAttack_18; }
	inline void set_nextAttack_18(float value)
	{
		___nextAttack_18 = value;
	}

	inline static int32_t get_offset_of_playerDistance_19() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___playerDistance_19)); }
	inline float get_playerDistance_19() const { return ___playerDistance_19; }
	inline float* get_address_of_playerDistance_19() { return &___playerDistance_19; }
	inline void set_playerDistance_19(float value)
	{
		___playerDistance_19 = value;
	}

	inline static int32_t get_offset_of_anim_20() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___anim_20)); }
	inline Animator_t434523843 * get_anim_20() const { return ___anim_20; }
	inline Animator_t434523843 ** get_address_of_anim_20() { return &___anim_20; }
	inline void set_anim_20(Animator_t434523843 * value)
	{
		___anim_20 = value;
		Il2CppCodeGenWriteBarrier((&___anim_20), value);
	}

	inline static int32_t get_offset_of_playerInRange_21() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___playerInRange_21)); }
	inline bool get_playerInRange_21() const { return ___playerInRange_21; }
	inline bool* get_address_of_playerInRange_21() { return &___playerInRange_21; }
	inline void set_playerInRange_21(bool value)
	{
		___playerInRange_21 = value;
	}

	inline static int32_t get_offset_of_AS_22() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___AS_22)); }
	inline AudioSource_t3935305588 * get_AS_22() const { return ___AS_22; }
	inline AudioSource_t3935305588 ** get_address_of_AS_22() { return &___AS_22; }
	inline void set_AS_22(AudioSource_t3935305588 * value)
	{
		___AS_22 = value;
		Il2CppCodeGenWriteBarrier((&___AS_22), value);
	}

	inline static int32_t get_offset_of_index_23() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___index_23)); }
	inline int32_t get_index_23() const { return ___index_23; }
	inline int32_t* get_address_of_index_23() { return &___index_23; }
	inline void set_index_23(int32_t value)
	{
		___index_23 = value;
	}

	inline static int32_t get_offset_of_playerHurtClips_24() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929, ___playerHurtClips_24)); }
	inline AudioClipU5BU5D_t143221404* get_playerHurtClips_24() const { return ___playerHurtClips_24; }
	inline AudioClipU5BU5D_t143221404** get_address_of_playerHurtClips_24() { return &___playerHurtClips_24; }
	inline void set_playerHurtClips_24(AudioClipU5BU5D_t143221404* value)
	{
		___playerHurtClips_24 = value;
		Il2CppCodeGenWriteBarrier((&___playerHurtClips_24), value);
	}
};

struct EnemyNavigation_t1695490929_StaticFields
{
public:
	// System.Boolean EnemyNavigation::isPlayerAlive
	bool ___isPlayerAlive_10;

public:
	inline static int32_t get_offset_of_isPlayerAlive_10() { return static_cast<int32_t>(offsetof(EnemyNavigation_t1695490929_StaticFields, ___isPlayerAlive_10)); }
	inline bool get_isPlayerAlive_10() const { return ___isPlayerAlive_10; }
	inline bool* get_address_of_isPlayerAlive_10() { return &___isPlayerAlive_10; }
	inline void set_isPlayerAlive_10(bool value)
	{
		___isPlayerAlive_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYNAVIGATION_T1695490929_H
#ifndef EXITDOOR_T1886296669_H
#define EXITDOOR_T1886296669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitDoor
struct  ExitDoor_t1886296669  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean ExitDoor::canOpen
	bool ___canOpen_4;
	// System.Boolean ExitDoor::triggering
	bool ___triggering_5;
	// System.Int32 ExitDoor::level
	int32_t ___level_6;

public:
	inline static int32_t get_offset_of_canOpen_4() { return static_cast<int32_t>(offsetof(ExitDoor_t1886296669, ___canOpen_4)); }
	inline bool get_canOpen_4() const { return ___canOpen_4; }
	inline bool* get_address_of_canOpen_4() { return &___canOpen_4; }
	inline void set_canOpen_4(bool value)
	{
		___canOpen_4 = value;
	}

	inline static int32_t get_offset_of_triggering_5() { return static_cast<int32_t>(offsetof(ExitDoor_t1886296669, ___triggering_5)); }
	inline bool get_triggering_5() const { return ___triggering_5; }
	inline bool* get_address_of_triggering_5() { return &___triggering_5; }
	inline void set_triggering_5(bool value)
	{
		___triggering_5 = value;
	}

	inline static int32_t get_offset_of_level_6() { return static_cast<int32_t>(offsetof(ExitDoor_t1886296669, ___level_6)); }
	inline int32_t get_level_6() const { return ___level_6; }
	inline int32_t* get_address_of_level_6() { return &___level_6; }
	inline void set_level_6(int32_t value)
	{
		___level_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXITDOOR_T1886296669_H
#ifndef FPSDISPLAY_T1066203289_H
#define FPSDISPLAY_T1066203289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSDisplay
struct  FPSDisplay_t1066203289  : public MonoBehaviour_t3962482529
{
public:
	// System.Single FPSDisplay::deltaTime
	float ___deltaTime_4;

public:
	inline static int32_t get_offset_of_deltaTime_4() { return static_cast<int32_t>(offsetof(FPSDisplay_t1066203289, ___deltaTime_4)); }
	inline float get_deltaTime_4() const { return ___deltaTime_4; }
	inline float* get_address_of_deltaTime_4() { return &___deltaTime_4; }
	inline void set_deltaTime_4(float value)
	{
		___deltaTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSDISPLAY_T1066203289_H
#ifndef FORGEINTERACTION_T3089908856_H
#define FORGEINTERACTION_T3089908856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForgeInteraction
struct  ForgeInteraction_t3089908856  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpriteRenderer ForgeInteraction::SR
	SpriteRenderer_t3235626157 * ___SR_4;
	// VillagerInteraction ForgeInteraction::VI
	VillagerInteraction_t490587617 * ___VI_5;
	// UnityEngine.AudioSource ForgeInteraction::AS
	AudioSource_t3935305588 * ___AS_6;
	// UnityEngine.GameObject ForgeInteraction::particles
	GameObject_t1113636619 * ___particles_7;

public:
	inline static int32_t get_offset_of_SR_4() { return static_cast<int32_t>(offsetof(ForgeInteraction_t3089908856, ___SR_4)); }
	inline SpriteRenderer_t3235626157 * get_SR_4() const { return ___SR_4; }
	inline SpriteRenderer_t3235626157 ** get_address_of_SR_4() { return &___SR_4; }
	inline void set_SR_4(SpriteRenderer_t3235626157 * value)
	{
		___SR_4 = value;
		Il2CppCodeGenWriteBarrier((&___SR_4), value);
	}

	inline static int32_t get_offset_of_VI_5() { return static_cast<int32_t>(offsetof(ForgeInteraction_t3089908856, ___VI_5)); }
	inline VillagerInteraction_t490587617 * get_VI_5() const { return ___VI_5; }
	inline VillagerInteraction_t490587617 ** get_address_of_VI_5() { return &___VI_5; }
	inline void set_VI_5(VillagerInteraction_t490587617 * value)
	{
		___VI_5 = value;
		Il2CppCodeGenWriteBarrier((&___VI_5), value);
	}

	inline static int32_t get_offset_of_AS_6() { return static_cast<int32_t>(offsetof(ForgeInteraction_t3089908856, ___AS_6)); }
	inline AudioSource_t3935305588 * get_AS_6() const { return ___AS_6; }
	inline AudioSource_t3935305588 ** get_address_of_AS_6() { return &___AS_6; }
	inline void set_AS_6(AudioSource_t3935305588 * value)
	{
		___AS_6 = value;
		Il2CppCodeGenWriteBarrier((&___AS_6), value);
	}

	inline static int32_t get_offset_of_particles_7() { return static_cast<int32_t>(offsetof(ForgeInteraction_t3089908856, ___particles_7)); }
	inline GameObject_t1113636619 * get_particles_7() const { return ___particles_7; }
	inline GameObject_t1113636619 ** get_address_of_particles_7() { return &___particles_7; }
	inline void set_particles_7(GameObject_t1113636619 * value)
	{
		___particles_7 = value;
		Il2CppCodeGenWriteBarrier((&___particles_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORGEINTERACTION_T3089908856_H
#ifndef HEALTHPOTIONINTERACTION_T3696356582_H
#define HEALTHPOTIONINTERACTION_T3696356582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HealthPotionInteraction
struct  HealthPotionInteraction_t3696356582  : public MonoBehaviour_t3962482529
{
public:
	// PlayerHealth HealthPotionInteraction::P1H
	PlayerHealth_t2068385516 * ___P1H_4;
	// PlayerHealth HealthPotionInteraction::P2H
	PlayerHealth_t2068385516 * ___P2H_5;

public:
	inline static int32_t get_offset_of_P1H_4() { return static_cast<int32_t>(offsetof(HealthPotionInteraction_t3696356582, ___P1H_4)); }
	inline PlayerHealth_t2068385516 * get_P1H_4() const { return ___P1H_4; }
	inline PlayerHealth_t2068385516 ** get_address_of_P1H_4() { return &___P1H_4; }
	inline void set_P1H_4(PlayerHealth_t2068385516 * value)
	{
		___P1H_4 = value;
		Il2CppCodeGenWriteBarrier((&___P1H_4), value);
	}

	inline static int32_t get_offset_of_P2H_5() { return static_cast<int32_t>(offsetof(HealthPotionInteraction_t3696356582, ___P2H_5)); }
	inline PlayerHealth_t2068385516 * get_P2H_5() const { return ___P2H_5; }
	inline PlayerHealth_t2068385516 ** get_address_of_P2H_5() { return &___P2H_5; }
	inline void set_P2H_5(PlayerHealth_t2068385516 * value)
	{
		___P2H_5 = value;
		Il2CppCodeGenWriteBarrier((&___P2H_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTHPOTIONINTERACTION_T3696356582_H
#ifndef KEYINTERACTION_T1379241489_H
#define KEYINTERACTION_T1379241489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KeyInteraction
struct  KeyInteraction_t1379241489  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean KeyInteraction::gotKey
	bool ___gotKey_4;
	// UnityEngine.UI.Image KeyInteraction::keyImage
	Image_t2670269651 * ___keyImage_5;
	// ExitDoor KeyInteraction::ED
	ExitDoor_t1886296669 * ___ED_6;

public:
	inline static int32_t get_offset_of_gotKey_4() { return static_cast<int32_t>(offsetof(KeyInteraction_t1379241489, ___gotKey_4)); }
	inline bool get_gotKey_4() const { return ___gotKey_4; }
	inline bool* get_address_of_gotKey_4() { return &___gotKey_4; }
	inline void set_gotKey_4(bool value)
	{
		___gotKey_4 = value;
	}

	inline static int32_t get_offset_of_keyImage_5() { return static_cast<int32_t>(offsetof(KeyInteraction_t1379241489, ___keyImage_5)); }
	inline Image_t2670269651 * get_keyImage_5() const { return ___keyImage_5; }
	inline Image_t2670269651 ** get_address_of_keyImage_5() { return &___keyImage_5; }
	inline void set_keyImage_5(Image_t2670269651 * value)
	{
		___keyImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___keyImage_5), value);
	}

	inline static int32_t get_offset_of_ED_6() { return static_cast<int32_t>(offsetof(KeyInteraction_t1379241489, ___ED_6)); }
	inline ExitDoor_t1886296669 * get_ED_6() const { return ___ED_6; }
	inline ExitDoor_t1886296669 ** get_address_of_ED_6() { return &___ED_6; }
	inline void set_ED_6(ExitDoor_t1886296669 * value)
	{
		___ED_6 = value;
		Il2CppCodeGenWriteBarrier((&___ED_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYINTERACTION_T1379241489_H
#ifndef LEAVECASACONTROL_T1102745401_H
#define LEAVECASACONTROL_T1102745401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaveCasaControl
struct  LeaveCasaControl_t1102745401  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform LeaveCasaControl::P1
	Transform_t3600365921 * ___P1_4;
	// UnityEngine.Transform LeaveCasaControl::P2
	Transform_t3600365921 * ___P2_5;
	// UnityEngine.Transform LeaveCasaControl::P1P1
	Transform_t3600365921 * ___P1P1_6;
	// UnityEngine.Transform LeaveCasaControl::P2P1
	Transform_t3600365921 * ___P2P1_7;

public:
	inline static int32_t get_offset_of_P1_4() { return static_cast<int32_t>(offsetof(LeaveCasaControl_t1102745401, ___P1_4)); }
	inline Transform_t3600365921 * get_P1_4() const { return ___P1_4; }
	inline Transform_t3600365921 ** get_address_of_P1_4() { return &___P1_4; }
	inline void set_P1_4(Transform_t3600365921 * value)
	{
		___P1_4 = value;
		Il2CppCodeGenWriteBarrier((&___P1_4), value);
	}

	inline static int32_t get_offset_of_P2_5() { return static_cast<int32_t>(offsetof(LeaveCasaControl_t1102745401, ___P2_5)); }
	inline Transform_t3600365921 * get_P2_5() const { return ___P2_5; }
	inline Transform_t3600365921 ** get_address_of_P2_5() { return &___P2_5; }
	inline void set_P2_5(Transform_t3600365921 * value)
	{
		___P2_5 = value;
		Il2CppCodeGenWriteBarrier((&___P2_5), value);
	}

	inline static int32_t get_offset_of_P1P1_6() { return static_cast<int32_t>(offsetof(LeaveCasaControl_t1102745401, ___P1P1_6)); }
	inline Transform_t3600365921 * get_P1P1_6() const { return ___P1P1_6; }
	inline Transform_t3600365921 ** get_address_of_P1P1_6() { return &___P1P1_6; }
	inline void set_P1P1_6(Transform_t3600365921 * value)
	{
		___P1P1_6 = value;
		Il2CppCodeGenWriteBarrier((&___P1P1_6), value);
	}

	inline static int32_t get_offset_of_P2P1_7() { return static_cast<int32_t>(offsetof(LeaveCasaControl_t1102745401, ___P2P1_7)); }
	inline Transform_t3600365921 * get_P2P1_7() const { return ___P2P1_7; }
	inline Transform_t3600365921 ** get_address_of_P2P1_7() { return &___P2P1_7; }
	inline void set_P2P1_7(Transform_t3600365921 * value)
	{
		___P2P1_7 = value;
		Il2CppCodeGenWriteBarrier((&___P2P1_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEAVECASACONTROL_T1102745401_H
#ifndef MAINMENUBEHAVIOR_T2264954362_H
#define MAINMENUBEHAVIOR_T2264954362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenuBehavior
struct  MainMenuBehavior_t2264954362  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 MainMenuBehavior::Scene1P
	int32_t ___Scene1P_4;
	// System.Int32 MainMenuBehavior::Scene2P
	int32_t ___Scene2P_5;

public:
	inline static int32_t get_offset_of_Scene1P_4() { return static_cast<int32_t>(offsetof(MainMenuBehavior_t2264954362, ___Scene1P_4)); }
	inline int32_t get_Scene1P_4() const { return ___Scene1P_4; }
	inline int32_t* get_address_of_Scene1P_4() { return &___Scene1P_4; }
	inline void set_Scene1P_4(int32_t value)
	{
		___Scene1P_4 = value;
	}

	inline static int32_t get_offset_of_Scene2P_5() { return static_cast<int32_t>(offsetof(MainMenuBehavior_t2264954362, ___Scene2P_5)); }
	inline int32_t get_Scene2P_5() const { return ___Scene2P_5; }
	inline int32_t* get_address_of_Scene2P_5() { return &___Scene2P_5; }
	inline void set_Scene2P_5(int32_t value)
	{
		___Scene2P_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENUBEHAVIOR_T2264954362_H
#ifndef MAINMENUCONTROL_T4231413698_H
#define MAINMENUCONTROL_T4231413698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenuControl
struct  MainMenuControl_t4231413698  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 MainMenuControl::scene1
	int32_t ___scene1_4;
	// System.Int32 MainMenuControl::scene2
	int32_t ___scene2_5;

public:
	inline static int32_t get_offset_of_scene1_4() { return static_cast<int32_t>(offsetof(MainMenuControl_t4231413698, ___scene1_4)); }
	inline int32_t get_scene1_4() const { return ___scene1_4; }
	inline int32_t* get_address_of_scene1_4() { return &___scene1_4; }
	inline void set_scene1_4(int32_t value)
	{
		___scene1_4 = value;
	}

	inline static int32_t get_offset_of_scene2_5() { return static_cast<int32_t>(offsetof(MainMenuControl_t4231413698, ___scene2_5)); }
	inline int32_t get_scene2_5() const { return ___scene2_5; }
	inline int32_t* get_address_of_scene2_5() { return &___scene2_5; }
	inline void set_scene2_5(int32_t value)
	{
		___scene2_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENUCONTROL_T4231413698_H
#ifndef MANAGEENEMY_T3567251697_H
#define MANAGEENEMY_T3567251697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManageEnemy
struct  ManageEnemy_t3567251697  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean ManageEnemy::spawnEnemies
	bool ___spawnEnemies_4;
	// System.Int32 ManageEnemy::enemyCount
	int32_t ___enemyCount_5;
	// UnityEngine.AudioSource ManageEnemy::AS
	AudioSource_t3935305588 * ___AS_6;
	// UnityEngine.AudioClip ManageEnemy::AC
	AudioClip_t3680889665 * ___AC_7;
	// UnityEngine.Transform ManageEnemy::spawnPoint1
	Transform_t3600365921 * ___spawnPoint1_8;
	// UnityEngine.Transform ManageEnemy::spawnPoint2
	Transform_t3600365921 * ___spawnPoint2_9;
	// UnityEngine.GameObject ManageEnemy::EnemyToSpawn
	GameObject_t1113636619 * ___EnemyToSpawn_10;
	// UnityEngine.GameObject ManageEnemy::smoke
	GameObject_t1113636619 * ___smoke_11;

public:
	inline static int32_t get_offset_of_spawnEnemies_4() { return static_cast<int32_t>(offsetof(ManageEnemy_t3567251697, ___spawnEnemies_4)); }
	inline bool get_spawnEnemies_4() const { return ___spawnEnemies_4; }
	inline bool* get_address_of_spawnEnemies_4() { return &___spawnEnemies_4; }
	inline void set_spawnEnemies_4(bool value)
	{
		___spawnEnemies_4 = value;
	}

	inline static int32_t get_offset_of_enemyCount_5() { return static_cast<int32_t>(offsetof(ManageEnemy_t3567251697, ___enemyCount_5)); }
	inline int32_t get_enemyCount_5() const { return ___enemyCount_5; }
	inline int32_t* get_address_of_enemyCount_5() { return &___enemyCount_5; }
	inline void set_enemyCount_5(int32_t value)
	{
		___enemyCount_5 = value;
	}

	inline static int32_t get_offset_of_AS_6() { return static_cast<int32_t>(offsetof(ManageEnemy_t3567251697, ___AS_6)); }
	inline AudioSource_t3935305588 * get_AS_6() const { return ___AS_6; }
	inline AudioSource_t3935305588 ** get_address_of_AS_6() { return &___AS_6; }
	inline void set_AS_6(AudioSource_t3935305588 * value)
	{
		___AS_6 = value;
		Il2CppCodeGenWriteBarrier((&___AS_6), value);
	}

	inline static int32_t get_offset_of_AC_7() { return static_cast<int32_t>(offsetof(ManageEnemy_t3567251697, ___AC_7)); }
	inline AudioClip_t3680889665 * get_AC_7() const { return ___AC_7; }
	inline AudioClip_t3680889665 ** get_address_of_AC_7() { return &___AC_7; }
	inline void set_AC_7(AudioClip_t3680889665 * value)
	{
		___AC_7 = value;
		Il2CppCodeGenWriteBarrier((&___AC_7), value);
	}

	inline static int32_t get_offset_of_spawnPoint1_8() { return static_cast<int32_t>(offsetof(ManageEnemy_t3567251697, ___spawnPoint1_8)); }
	inline Transform_t3600365921 * get_spawnPoint1_8() const { return ___spawnPoint1_8; }
	inline Transform_t3600365921 ** get_address_of_spawnPoint1_8() { return &___spawnPoint1_8; }
	inline void set_spawnPoint1_8(Transform_t3600365921 * value)
	{
		___spawnPoint1_8 = value;
		Il2CppCodeGenWriteBarrier((&___spawnPoint1_8), value);
	}

	inline static int32_t get_offset_of_spawnPoint2_9() { return static_cast<int32_t>(offsetof(ManageEnemy_t3567251697, ___spawnPoint2_9)); }
	inline Transform_t3600365921 * get_spawnPoint2_9() const { return ___spawnPoint2_9; }
	inline Transform_t3600365921 ** get_address_of_spawnPoint2_9() { return &___spawnPoint2_9; }
	inline void set_spawnPoint2_9(Transform_t3600365921 * value)
	{
		___spawnPoint2_9 = value;
		Il2CppCodeGenWriteBarrier((&___spawnPoint2_9), value);
	}

	inline static int32_t get_offset_of_EnemyToSpawn_10() { return static_cast<int32_t>(offsetof(ManageEnemy_t3567251697, ___EnemyToSpawn_10)); }
	inline GameObject_t1113636619 * get_EnemyToSpawn_10() const { return ___EnemyToSpawn_10; }
	inline GameObject_t1113636619 ** get_address_of_EnemyToSpawn_10() { return &___EnemyToSpawn_10; }
	inline void set_EnemyToSpawn_10(GameObject_t1113636619 * value)
	{
		___EnemyToSpawn_10 = value;
		Il2CppCodeGenWriteBarrier((&___EnemyToSpawn_10), value);
	}

	inline static int32_t get_offset_of_smoke_11() { return static_cast<int32_t>(offsetof(ManageEnemy_t3567251697, ___smoke_11)); }
	inline GameObject_t1113636619 * get_smoke_11() const { return ___smoke_11; }
	inline GameObject_t1113636619 ** get_address_of_smoke_11() { return &___smoke_11; }
	inline void set_smoke_11(GameObject_t1113636619 * value)
	{
		___smoke_11 = value;
		Il2CppCodeGenWriteBarrier((&___smoke_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANAGEENEMY_T3567251697_H
#ifndef MANAGESCENE_T3196455047_H
#define MANAGESCENE_T3196455047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ManageScene
struct  ManageScene_t3196455047  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ManageScene::P1
	GameObject_t1113636619 * ___P1_4;
	// UnityEngine.GameObject ManageScene::P2
	GameObject_t1113636619 * ___P2_5;
	// PlayerHealth ManageScene::P1H
	PlayerHealth_t2068385516 * ___P1H_6;
	// PlayerHealth ManageScene::P2H
	PlayerHealth_t2068385516 * ___P2H_7;
	// System.Boolean ManageScene::P1Dead
	bool ___P1Dead_8;
	// System.Boolean ManageScene::P2Dead
	bool ___P2Dead_9;
	// System.Boolean ManageScene::singlePlayer
	bool ___singlePlayer_10;
	// System.Int32 ManageScene::whichSceneToLoad
	int32_t ___whichSceneToLoad_11;

public:
	inline static int32_t get_offset_of_P1_4() { return static_cast<int32_t>(offsetof(ManageScene_t3196455047, ___P1_4)); }
	inline GameObject_t1113636619 * get_P1_4() const { return ___P1_4; }
	inline GameObject_t1113636619 ** get_address_of_P1_4() { return &___P1_4; }
	inline void set_P1_4(GameObject_t1113636619 * value)
	{
		___P1_4 = value;
		Il2CppCodeGenWriteBarrier((&___P1_4), value);
	}

	inline static int32_t get_offset_of_P2_5() { return static_cast<int32_t>(offsetof(ManageScene_t3196455047, ___P2_5)); }
	inline GameObject_t1113636619 * get_P2_5() const { return ___P2_5; }
	inline GameObject_t1113636619 ** get_address_of_P2_5() { return &___P2_5; }
	inline void set_P2_5(GameObject_t1113636619 * value)
	{
		___P2_5 = value;
		Il2CppCodeGenWriteBarrier((&___P2_5), value);
	}

	inline static int32_t get_offset_of_P1H_6() { return static_cast<int32_t>(offsetof(ManageScene_t3196455047, ___P1H_6)); }
	inline PlayerHealth_t2068385516 * get_P1H_6() const { return ___P1H_6; }
	inline PlayerHealth_t2068385516 ** get_address_of_P1H_6() { return &___P1H_6; }
	inline void set_P1H_6(PlayerHealth_t2068385516 * value)
	{
		___P1H_6 = value;
		Il2CppCodeGenWriteBarrier((&___P1H_6), value);
	}

	inline static int32_t get_offset_of_P2H_7() { return static_cast<int32_t>(offsetof(ManageScene_t3196455047, ___P2H_7)); }
	inline PlayerHealth_t2068385516 * get_P2H_7() const { return ___P2H_7; }
	inline PlayerHealth_t2068385516 ** get_address_of_P2H_7() { return &___P2H_7; }
	inline void set_P2H_7(PlayerHealth_t2068385516 * value)
	{
		___P2H_7 = value;
		Il2CppCodeGenWriteBarrier((&___P2H_7), value);
	}

	inline static int32_t get_offset_of_P1Dead_8() { return static_cast<int32_t>(offsetof(ManageScene_t3196455047, ___P1Dead_8)); }
	inline bool get_P1Dead_8() const { return ___P1Dead_8; }
	inline bool* get_address_of_P1Dead_8() { return &___P1Dead_8; }
	inline void set_P1Dead_8(bool value)
	{
		___P1Dead_8 = value;
	}

	inline static int32_t get_offset_of_P2Dead_9() { return static_cast<int32_t>(offsetof(ManageScene_t3196455047, ___P2Dead_9)); }
	inline bool get_P2Dead_9() const { return ___P2Dead_9; }
	inline bool* get_address_of_P2Dead_9() { return &___P2Dead_9; }
	inline void set_P2Dead_9(bool value)
	{
		___P2Dead_9 = value;
	}

	inline static int32_t get_offset_of_singlePlayer_10() { return static_cast<int32_t>(offsetof(ManageScene_t3196455047, ___singlePlayer_10)); }
	inline bool get_singlePlayer_10() const { return ___singlePlayer_10; }
	inline bool* get_address_of_singlePlayer_10() { return &___singlePlayer_10; }
	inline void set_singlePlayer_10(bool value)
	{
		___singlePlayer_10 = value;
	}

	inline static int32_t get_offset_of_whichSceneToLoad_11() { return static_cast<int32_t>(offsetof(ManageScene_t3196455047, ___whichSceneToLoad_11)); }
	inline int32_t get_whichSceneToLoad_11() const { return ___whichSceneToLoad_11; }
	inline int32_t* get_address_of_whichSceneToLoad_11() { return &___whichSceneToLoad_11; }
	inline void set_whichSceneToLoad_11(int32_t value)
	{
		___whichSceneToLoad_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANAGESCENE_T3196455047_H
#ifndef NEXTLEVEL_T1903424389_H
#define NEXTLEVEL_T1903424389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NextLevel
struct  NextLevel_t1903424389  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpriteRenderer NextLevel::SR
	SpriteRenderer_t3235626157 * ___SR_4;
	// System.Int32 NextLevel::sceneToLoad
	int32_t ___sceneToLoad_5;

public:
	inline static int32_t get_offset_of_SR_4() { return static_cast<int32_t>(offsetof(NextLevel_t1903424389, ___SR_4)); }
	inline SpriteRenderer_t3235626157 * get_SR_4() const { return ___SR_4; }
	inline SpriteRenderer_t3235626157 ** get_address_of_SR_4() { return &___SR_4; }
	inline void set_SR_4(SpriteRenderer_t3235626157 * value)
	{
		___SR_4 = value;
		Il2CppCodeGenWriteBarrier((&___SR_4), value);
	}

	inline static int32_t get_offset_of_sceneToLoad_5() { return static_cast<int32_t>(offsetof(NextLevel_t1903424389, ___sceneToLoad_5)); }
	inline int32_t get_sceneToLoad_5() const { return ___sceneToLoad_5; }
	inline int32_t* get_address_of_sceneToLoad_5() { return &___sceneToLoad_5; }
	inline void set_sceneToLoad_5(int32_t value)
	{
		___sceneToLoad_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEXTLEVEL_T1903424389_H
#ifndef P1ATTACK_T893104030_H
#define P1ATTACK_T893104030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// P1Attack
struct  P1Attack_t893104030  : public MonoBehaviour_t3962482529
{
public:
	// System.Single P1Attack::nextSwing
	float ___nextSwing_4;
	// System.Boolean P1Attack::enemyInRange
	bool ___enemyInRange_5;
	// EnemyHealth P1Attack::EH
	EnemyHealth_t797421206 * ___EH_6;
	// System.Single P1Attack::swingRate
	float ___swingRate_7;
	// System.Int32 P1Attack::playerDamage
	int32_t ___playerDamage_8;

public:
	inline static int32_t get_offset_of_nextSwing_4() { return static_cast<int32_t>(offsetof(P1Attack_t893104030, ___nextSwing_4)); }
	inline float get_nextSwing_4() const { return ___nextSwing_4; }
	inline float* get_address_of_nextSwing_4() { return &___nextSwing_4; }
	inline void set_nextSwing_4(float value)
	{
		___nextSwing_4 = value;
	}

	inline static int32_t get_offset_of_enemyInRange_5() { return static_cast<int32_t>(offsetof(P1Attack_t893104030, ___enemyInRange_5)); }
	inline bool get_enemyInRange_5() const { return ___enemyInRange_5; }
	inline bool* get_address_of_enemyInRange_5() { return &___enemyInRange_5; }
	inline void set_enemyInRange_5(bool value)
	{
		___enemyInRange_5 = value;
	}

	inline static int32_t get_offset_of_EH_6() { return static_cast<int32_t>(offsetof(P1Attack_t893104030, ___EH_6)); }
	inline EnemyHealth_t797421206 * get_EH_6() const { return ___EH_6; }
	inline EnemyHealth_t797421206 ** get_address_of_EH_6() { return &___EH_6; }
	inline void set_EH_6(EnemyHealth_t797421206 * value)
	{
		___EH_6 = value;
		Il2CppCodeGenWriteBarrier((&___EH_6), value);
	}

	inline static int32_t get_offset_of_swingRate_7() { return static_cast<int32_t>(offsetof(P1Attack_t893104030, ___swingRate_7)); }
	inline float get_swingRate_7() const { return ___swingRate_7; }
	inline float* get_address_of_swingRate_7() { return &___swingRate_7; }
	inline void set_swingRate_7(float value)
	{
		___swingRate_7 = value;
	}

	inline static int32_t get_offset_of_playerDamage_8() { return static_cast<int32_t>(offsetof(P1Attack_t893104030, ___playerDamage_8)); }
	inline int32_t get_playerDamage_8() const { return ___playerDamage_8; }
	inline int32_t* get_address_of_playerDamage_8() { return &___playerDamage_8; }
	inline void set_playerDamage_8(int32_t value)
	{
		___playerDamage_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // P1ATTACK_T893104030_H
#ifndef P2ATTACK_T895266718_H
#define P2ATTACK_T895266718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// P2Attack
struct  P2Attack_t895266718  : public MonoBehaviour_t3962482529
{
public:
	// System.Single P2Attack::nextSwing
	float ___nextSwing_4;
	// System.Boolean P2Attack::enemyInRange
	bool ___enemyInRange_5;
	// EnemyHealth P2Attack::EH
	EnemyHealth_t797421206 * ___EH_6;
	// System.Single P2Attack::swingRate
	float ___swingRate_7;
	// System.Int32 P2Attack::playerDamage
	int32_t ___playerDamage_8;

public:
	inline static int32_t get_offset_of_nextSwing_4() { return static_cast<int32_t>(offsetof(P2Attack_t895266718, ___nextSwing_4)); }
	inline float get_nextSwing_4() const { return ___nextSwing_4; }
	inline float* get_address_of_nextSwing_4() { return &___nextSwing_4; }
	inline void set_nextSwing_4(float value)
	{
		___nextSwing_4 = value;
	}

	inline static int32_t get_offset_of_enemyInRange_5() { return static_cast<int32_t>(offsetof(P2Attack_t895266718, ___enemyInRange_5)); }
	inline bool get_enemyInRange_5() const { return ___enemyInRange_5; }
	inline bool* get_address_of_enemyInRange_5() { return &___enemyInRange_5; }
	inline void set_enemyInRange_5(bool value)
	{
		___enemyInRange_5 = value;
	}

	inline static int32_t get_offset_of_EH_6() { return static_cast<int32_t>(offsetof(P2Attack_t895266718, ___EH_6)); }
	inline EnemyHealth_t797421206 * get_EH_6() const { return ___EH_6; }
	inline EnemyHealth_t797421206 ** get_address_of_EH_6() { return &___EH_6; }
	inline void set_EH_6(EnemyHealth_t797421206 * value)
	{
		___EH_6 = value;
		Il2CppCodeGenWriteBarrier((&___EH_6), value);
	}

	inline static int32_t get_offset_of_swingRate_7() { return static_cast<int32_t>(offsetof(P2Attack_t895266718, ___swingRate_7)); }
	inline float get_swingRate_7() const { return ___swingRate_7; }
	inline float* get_address_of_swingRate_7() { return &___swingRate_7; }
	inline void set_swingRate_7(float value)
	{
		___swingRate_7 = value;
	}

	inline static int32_t get_offset_of_playerDamage_8() { return static_cast<int32_t>(offsetof(P2Attack_t895266718, ___playerDamage_8)); }
	inline int32_t get_playerDamage_8() const { return ___playerDamage_8; }
	inline int32_t* get_address_of_playerDamage_8() { return &___playerDamage_8; }
	inline void set_playerDamage_8(int32_t value)
	{
		___playerDamage_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // P2ATTACK_T895266718_H
#ifndef PAUSEMENU1_T3917150987_H
#define PAUSEMENU1_T3917150987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseMenu1
struct  PauseMenu1_t3917150987  : public MonoBehaviour_t3962482529
{
public:
	// System.Single PauseMenu1::m_TimeScaleRef
	float ___m_TimeScaleRef_4;
	// System.Single PauseMenu1::m_VolumeRef
	float ___m_VolumeRef_5;
	// System.Boolean PauseMenu1::m_Paused
	bool ___m_Paused_6;
	// UnityEngine.UI.Image PauseMenu1::image
	Image_t2670269651 * ___image_7;
	// UnityEngine.UI.Image PauseMenu1::B
	Image_t2670269651 * ___B_8;
	// UnityEngine.UI.Text PauseMenu1::pauseText
	Text_t1901882714 * ___pauseText_9;
	// UnityEngine.UI.Text PauseMenu1::exitText
	Text_t1901882714 * ___exitText_10;

public:
	inline static int32_t get_offset_of_m_TimeScaleRef_4() { return static_cast<int32_t>(offsetof(PauseMenu1_t3917150987, ___m_TimeScaleRef_4)); }
	inline float get_m_TimeScaleRef_4() const { return ___m_TimeScaleRef_4; }
	inline float* get_address_of_m_TimeScaleRef_4() { return &___m_TimeScaleRef_4; }
	inline void set_m_TimeScaleRef_4(float value)
	{
		___m_TimeScaleRef_4 = value;
	}

	inline static int32_t get_offset_of_m_VolumeRef_5() { return static_cast<int32_t>(offsetof(PauseMenu1_t3917150987, ___m_VolumeRef_5)); }
	inline float get_m_VolumeRef_5() const { return ___m_VolumeRef_5; }
	inline float* get_address_of_m_VolumeRef_5() { return &___m_VolumeRef_5; }
	inline void set_m_VolumeRef_5(float value)
	{
		___m_VolumeRef_5 = value;
	}

	inline static int32_t get_offset_of_m_Paused_6() { return static_cast<int32_t>(offsetof(PauseMenu1_t3917150987, ___m_Paused_6)); }
	inline bool get_m_Paused_6() const { return ___m_Paused_6; }
	inline bool* get_address_of_m_Paused_6() { return &___m_Paused_6; }
	inline void set_m_Paused_6(bool value)
	{
		___m_Paused_6 = value;
	}

	inline static int32_t get_offset_of_image_7() { return static_cast<int32_t>(offsetof(PauseMenu1_t3917150987, ___image_7)); }
	inline Image_t2670269651 * get_image_7() const { return ___image_7; }
	inline Image_t2670269651 ** get_address_of_image_7() { return &___image_7; }
	inline void set_image_7(Image_t2670269651 * value)
	{
		___image_7 = value;
		Il2CppCodeGenWriteBarrier((&___image_7), value);
	}

	inline static int32_t get_offset_of_B_8() { return static_cast<int32_t>(offsetof(PauseMenu1_t3917150987, ___B_8)); }
	inline Image_t2670269651 * get_B_8() const { return ___B_8; }
	inline Image_t2670269651 ** get_address_of_B_8() { return &___B_8; }
	inline void set_B_8(Image_t2670269651 * value)
	{
		___B_8 = value;
		Il2CppCodeGenWriteBarrier((&___B_8), value);
	}

	inline static int32_t get_offset_of_pauseText_9() { return static_cast<int32_t>(offsetof(PauseMenu1_t3917150987, ___pauseText_9)); }
	inline Text_t1901882714 * get_pauseText_9() const { return ___pauseText_9; }
	inline Text_t1901882714 ** get_address_of_pauseText_9() { return &___pauseText_9; }
	inline void set_pauseText_9(Text_t1901882714 * value)
	{
		___pauseText_9 = value;
		Il2CppCodeGenWriteBarrier((&___pauseText_9), value);
	}

	inline static int32_t get_offset_of_exitText_10() { return static_cast<int32_t>(offsetof(PauseMenu1_t3917150987, ___exitText_10)); }
	inline Text_t1901882714 * get_exitText_10() const { return ___exitText_10; }
	inline Text_t1901882714 ** get_address_of_exitText_10() { return &___exitText_10; }
	inline void set_exitText_10(Text_t1901882714 * value)
	{
		___exitText_10 = value;
		Il2CppCodeGenWriteBarrier((&___exitText_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEMENU1_T3917150987_H
#ifndef PLAYERCONTROLP1_T3101175660_H
#define PLAYERCONTROLP1_T3101175660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerControlP1
struct  PlayerControlP1_t3101175660  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator PlayerControlP1::animator
	Animator_t434523843 * ___animator_4;
	// System.Single PlayerControlP1::rotationSpeed
	float ___rotationSpeed_5;
	// UnityEngine.Vector3 PlayerControlP1::inputVec
	Vector3_t3722313464  ___inputVec_6;
	// UnityEngine.Vector3 PlayerControlP1::targetDirection
	Vector3_t3722313464  ___targetDirection_7;
	// UnityEngine.Transform PlayerControlP1::cameraTransform
	Transform_t3600365921 * ___cameraTransform_8;
	// System.Boolean PlayerControlP1::camBool1
	bool ___camBool1_9;
	// System.Boolean PlayerControlP1::camBool2
	bool ___camBool2_10;
	// UnityEngine.GameObject PlayerControlP1::cam1
	GameObject_t1113636619 * ___cam1_11;
	// UnityEngine.GameObject PlayerControlP1::cam2
	GameObject_t1113636619 * ___cam2_12;
	// PlayerControlP1/Warrior PlayerControlP1::warrior
	int32_t ___warrior_13;
	// System.Single PlayerControlP1::gravity
	float ___gravity_14;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(PlayerControlP1_t3101175660, ___animator_4)); }
	inline Animator_t434523843 * get_animator_4() const { return ___animator_4; }
	inline Animator_t434523843 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t434523843 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}

	inline static int32_t get_offset_of_rotationSpeed_5() { return static_cast<int32_t>(offsetof(PlayerControlP1_t3101175660, ___rotationSpeed_5)); }
	inline float get_rotationSpeed_5() const { return ___rotationSpeed_5; }
	inline float* get_address_of_rotationSpeed_5() { return &___rotationSpeed_5; }
	inline void set_rotationSpeed_5(float value)
	{
		___rotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_inputVec_6() { return static_cast<int32_t>(offsetof(PlayerControlP1_t3101175660, ___inputVec_6)); }
	inline Vector3_t3722313464  get_inputVec_6() const { return ___inputVec_6; }
	inline Vector3_t3722313464 * get_address_of_inputVec_6() { return &___inputVec_6; }
	inline void set_inputVec_6(Vector3_t3722313464  value)
	{
		___inputVec_6 = value;
	}

	inline static int32_t get_offset_of_targetDirection_7() { return static_cast<int32_t>(offsetof(PlayerControlP1_t3101175660, ___targetDirection_7)); }
	inline Vector3_t3722313464  get_targetDirection_7() const { return ___targetDirection_7; }
	inline Vector3_t3722313464 * get_address_of_targetDirection_7() { return &___targetDirection_7; }
	inline void set_targetDirection_7(Vector3_t3722313464  value)
	{
		___targetDirection_7 = value;
	}

	inline static int32_t get_offset_of_cameraTransform_8() { return static_cast<int32_t>(offsetof(PlayerControlP1_t3101175660, ___cameraTransform_8)); }
	inline Transform_t3600365921 * get_cameraTransform_8() const { return ___cameraTransform_8; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_8() { return &___cameraTransform_8; }
	inline void set_cameraTransform_8(Transform_t3600365921 * value)
	{
		___cameraTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_8), value);
	}

	inline static int32_t get_offset_of_camBool1_9() { return static_cast<int32_t>(offsetof(PlayerControlP1_t3101175660, ___camBool1_9)); }
	inline bool get_camBool1_9() const { return ___camBool1_9; }
	inline bool* get_address_of_camBool1_9() { return &___camBool1_9; }
	inline void set_camBool1_9(bool value)
	{
		___camBool1_9 = value;
	}

	inline static int32_t get_offset_of_camBool2_10() { return static_cast<int32_t>(offsetof(PlayerControlP1_t3101175660, ___camBool2_10)); }
	inline bool get_camBool2_10() const { return ___camBool2_10; }
	inline bool* get_address_of_camBool2_10() { return &___camBool2_10; }
	inline void set_camBool2_10(bool value)
	{
		___camBool2_10 = value;
	}

	inline static int32_t get_offset_of_cam1_11() { return static_cast<int32_t>(offsetof(PlayerControlP1_t3101175660, ___cam1_11)); }
	inline GameObject_t1113636619 * get_cam1_11() const { return ___cam1_11; }
	inline GameObject_t1113636619 ** get_address_of_cam1_11() { return &___cam1_11; }
	inline void set_cam1_11(GameObject_t1113636619 * value)
	{
		___cam1_11 = value;
		Il2CppCodeGenWriteBarrier((&___cam1_11), value);
	}

	inline static int32_t get_offset_of_cam2_12() { return static_cast<int32_t>(offsetof(PlayerControlP1_t3101175660, ___cam2_12)); }
	inline GameObject_t1113636619 * get_cam2_12() const { return ___cam2_12; }
	inline GameObject_t1113636619 ** get_address_of_cam2_12() { return &___cam2_12; }
	inline void set_cam2_12(GameObject_t1113636619 * value)
	{
		___cam2_12 = value;
		Il2CppCodeGenWriteBarrier((&___cam2_12), value);
	}

	inline static int32_t get_offset_of_warrior_13() { return static_cast<int32_t>(offsetof(PlayerControlP1_t3101175660, ___warrior_13)); }
	inline int32_t get_warrior_13() const { return ___warrior_13; }
	inline int32_t* get_address_of_warrior_13() { return &___warrior_13; }
	inline void set_warrior_13(int32_t value)
	{
		___warrior_13 = value;
	}

	inline static int32_t get_offset_of_gravity_14() { return static_cast<int32_t>(offsetof(PlayerControlP1_t3101175660, ___gravity_14)); }
	inline float get_gravity_14() const { return ___gravity_14; }
	inline float* get_address_of_gravity_14() { return &___gravity_14; }
	inline void set_gravity_14(float value)
	{
		___gravity_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLP1_T3101175660_H
#ifndef PLAYERHEALTH_T2068385516_H
#define PLAYERHEALTH_T2068385516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerHealth
struct  PlayerHealth_t2068385516  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PlayerHealth::player
	GameObject_t1113636619 * ___player_4;
	// UnityEngine.GameObject PlayerHealth::ragdoll
	GameObject_t1113636619 * ___ragdoll_5;
	// System.Single PlayerHealth::currentHealth
	float ___currentHealth_6;
	// UnityEngine.UI.Image PlayerHealth::healthBar
	Image_t2670269651 * ___healthBar_7;
	// System.Single PlayerHealth::maxHealth
	float ___maxHealth_8;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(PlayerHealth_t2068385516, ___player_4)); }
	inline GameObject_t1113636619 * get_player_4() const { return ___player_4; }
	inline GameObject_t1113636619 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_t1113636619 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_ragdoll_5() { return static_cast<int32_t>(offsetof(PlayerHealth_t2068385516, ___ragdoll_5)); }
	inline GameObject_t1113636619 * get_ragdoll_5() const { return ___ragdoll_5; }
	inline GameObject_t1113636619 ** get_address_of_ragdoll_5() { return &___ragdoll_5; }
	inline void set_ragdoll_5(GameObject_t1113636619 * value)
	{
		___ragdoll_5 = value;
		Il2CppCodeGenWriteBarrier((&___ragdoll_5), value);
	}

	inline static int32_t get_offset_of_currentHealth_6() { return static_cast<int32_t>(offsetof(PlayerHealth_t2068385516, ___currentHealth_6)); }
	inline float get_currentHealth_6() const { return ___currentHealth_6; }
	inline float* get_address_of_currentHealth_6() { return &___currentHealth_6; }
	inline void set_currentHealth_6(float value)
	{
		___currentHealth_6 = value;
	}

	inline static int32_t get_offset_of_healthBar_7() { return static_cast<int32_t>(offsetof(PlayerHealth_t2068385516, ___healthBar_7)); }
	inline Image_t2670269651 * get_healthBar_7() const { return ___healthBar_7; }
	inline Image_t2670269651 ** get_address_of_healthBar_7() { return &___healthBar_7; }
	inline void set_healthBar_7(Image_t2670269651 * value)
	{
		___healthBar_7 = value;
		Il2CppCodeGenWriteBarrier((&___healthBar_7), value);
	}

	inline static int32_t get_offset_of_maxHealth_8() { return static_cast<int32_t>(offsetof(PlayerHealth_t2068385516, ___maxHealth_8)); }
	inline float get_maxHealth_8() const { return ___maxHealth_8; }
	inline float* get_address_of_maxHealth_8() { return &___maxHealth_8; }
	inline void set_maxHealth_8(float value)
	{
		___maxHealth_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERHEALTH_T2068385516_H
#ifndef PLAYERINTERACTION_T264875739_H
#define PLAYERINTERACTION_T264875739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerInteraction
struct  PlayerInteraction_t264875739  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PlayerInteraction::fireplace1
	bool ___fireplace1_4;
	// System.Boolean PlayerInteraction::triggeringP1
	bool ___triggeringP1_5;
	// System.Boolean PlayerInteraction::triggeringP2
	bool ___triggeringP2_6;
	// System.Boolean PlayerInteraction::fire1On
	bool ___fire1On_7;
	// System.Boolean PlayerInteraction::bothOn
	bool ___bothOn_8;
	// ManageEnemy PlayerInteraction::ME
	ManageEnemy_t3567251697 * ___ME_9;
	// UnityEngine.GameObject PlayerInteraction::fire1
	GameObject_t1113636619 * ___fire1_10;

public:
	inline static int32_t get_offset_of_fireplace1_4() { return static_cast<int32_t>(offsetof(PlayerInteraction_t264875739, ___fireplace1_4)); }
	inline bool get_fireplace1_4() const { return ___fireplace1_4; }
	inline bool* get_address_of_fireplace1_4() { return &___fireplace1_4; }
	inline void set_fireplace1_4(bool value)
	{
		___fireplace1_4 = value;
	}

	inline static int32_t get_offset_of_triggeringP1_5() { return static_cast<int32_t>(offsetof(PlayerInteraction_t264875739, ___triggeringP1_5)); }
	inline bool get_triggeringP1_5() const { return ___triggeringP1_5; }
	inline bool* get_address_of_triggeringP1_5() { return &___triggeringP1_5; }
	inline void set_triggeringP1_5(bool value)
	{
		___triggeringP1_5 = value;
	}

	inline static int32_t get_offset_of_triggeringP2_6() { return static_cast<int32_t>(offsetof(PlayerInteraction_t264875739, ___triggeringP2_6)); }
	inline bool get_triggeringP2_6() const { return ___triggeringP2_6; }
	inline bool* get_address_of_triggeringP2_6() { return &___triggeringP2_6; }
	inline void set_triggeringP2_6(bool value)
	{
		___triggeringP2_6 = value;
	}

	inline static int32_t get_offset_of_fire1On_7() { return static_cast<int32_t>(offsetof(PlayerInteraction_t264875739, ___fire1On_7)); }
	inline bool get_fire1On_7() const { return ___fire1On_7; }
	inline bool* get_address_of_fire1On_7() { return &___fire1On_7; }
	inline void set_fire1On_7(bool value)
	{
		___fire1On_7 = value;
	}

	inline static int32_t get_offset_of_bothOn_8() { return static_cast<int32_t>(offsetof(PlayerInteraction_t264875739, ___bothOn_8)); }
	inline bool get_bothOn_8() const { return ___bothOn_8; }
	inline bool* get_address_of_bothOn_8() { return &___bothOn_8; }
	inline void set_bothOn_8(bool value)
	{
		___bothOn_8 = value;
	}

	inline static int32_t get_offset_of_ME_9() { return static_cast<int32_t>(offsetof(PlayerInteraction_t264875739, ___ME_9)); }
	inline ManageEnemy_t3567251697 * get_ME_9() const { return ___ME_9; }
	inline ManageEnemy_t3567251697 ** get_address_of_ME_9() { return &___ME_9; }
	inline void set_ME_9(ManageEnemy_t3567251697 * value)
	{
		___ME_9 = value;
		Il2CppCodeGenWriteBarrier((&___ME_9), value);
	}

	inline static int32_t get_offset_of_fire1_10() { return static_cast<int32_t>(offsetof(PlayerInteraction_t264875739, ___fire1_10)); }
	inline GameObject_t1113636619 * get_fire1_10() const { return ___fire1_10; }
	inline GameObject_t1113636619 ** get_address_of_fire1_10() { return &___fire1_10; }
	inline void set_fire1_10(GameObject_t1113636619 * value)
	{
		___fire1_10 = value;
		Il2CppCodeGenWriteBarrier((&___fire1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERINTERACTION_T264875739_H
#ifndef SIMPLEHEALTHBAR_T721070758_H
#define SIMPLEHEALTHBAR_T721070758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar
struct  SimpleHealthBar_t721070758  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image SimpleHealthBar::barImage
	Image_t2670269651 * ___barImage_4;
	// SimpleHealthBar/ColorMode SimpleHealthBar::colorMode
	int32_t ___colorMode_5;
	// UnityEngine.Color SimpleHealthBar::barColor
	Color_t2555686324  ___barColor_6;
	// UnityEngine.Gradient SimpleHealthBar::barGradient
	Gradient_t3067099924 * ___barGradient_7;
	// SimpleHealthBar/DisplayText SimpleHealthBar::displayText
	int32_t ___displayText_8;
	// UnityEngine.UI.Text SimpleHealthBar::barText
	Text_t1901882714 * ___barText_9;
	// System.String SimpleHealthBar::additionalText
	String_t* ___additionalText_10;
	// System.Single SimpleHealthBar::_currentFraction
	float ____currentFraction_11;
	// System.Single SimpleHealthBar::_maxValue
	float ____maxValue_12;
	// System.Single SimpleHealthBar::targetFill
	float ___targetFill_13;

public:
	inline static int32_t get_offset_of_barImage_4() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___barImage_4)); }
	inline Image_t2670269651 * get_barImage_4() const { return ___barImage_4; }
	inline Image_t2670269651 ** get_address_of_barImage_4() { return &___barImage_4; }
	inline void set_barImage_4(Image_t2670269651 * value)
	{
		___barImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___barImage_4), value);
	}

	inline static int32_t get_offset_of_colorMode_5() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___colorMode_5)); }
	inline int32_t get_colorMode_5() const { return ___colorMode_5; }
	inline int32_t* get_address_of_colorMode_5() { return &___colorMode_5; }
	inline void set_colorMode_5(int32_t value)
	{
		___colorMode_5 = value;
	}

	inline static int32_t get_offset_of_barColor_6() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___barColor_6)); }
	inline Color_t2555686324  get_barColor_6() const { return ___barColor_6; }
	inline Color_t2555686324 * get_address_of_barColor_6() { return &___barColor_6; }
	inline void set_barColor_6(Color_t2555686324  value)
	{
		___barColor_6 = value;
	}

	inline static int32_t get_offset_of_barGradient_7() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___barGradient_7)); }
	inline Gradient_t3067099924 * get_barGradient_7() const { return ___barGradient_7; }
	inline Gradient_t3067099924 ** get_address_of_barGradient_7() { return &___barGradient_7; }
	inline void set_barGradient_7(Gradient_t3067099924 * value)
	{
		___barGradient_7 = value;
		Il2CppCodeGenWriteBarrier((&___barGradient_7), value);
	}

	inline static int32_t get_offset_of_displayText_8() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___displayText_8)); }
	inline int32_t get_displayText_8() const { return ___displayText_8; }
	inline int32_t* get_address_of_displayText_8() { return &___displayText_8; }
	inline void set_displayText_8(int32_t value)
	{
		___displayText_8 = value;
	}

	inline static int32_t get_offset_of_barText_9() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___barText_9)); }
	inline Text_t1901882714 * get_barText_9() const { return ___barText_9; }
	inline Text_t1901882714 ** get_address_of_barText_9() { return &___barText_9; }
	inline void set_barText_9(Text_t1901882714 * value)
	{
		___barText_9 = value;
		Il2CppCodeGenWriteBarrier((&___barText_9), value);
	}

	inline static int32_t get_offset_of_additionalText_10() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___additionalText_10)); }
	inline String_t* get_additionalText_10() const { return ___additionalText_10; }
	inline String_t** get_address_of_additionalText_10() { return &___additionalText_10; }
	inline void set_additionalText_10(String_t* value)
	{
		___additionalText_10 = value;
		Il2CppCodeGenWriteBarrier((&___additionalText_10), value);
	}

	inline static int32_t get_offset_of__currentFraction_11() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ____currentFraction_11)); }
	inline float get__currentFraction_11() const { return ____currentFraction_11; }
	inline float* get_address_of__currentFraction_11() { return &____currentFraction_11; }
	inline void set__currentFraction_11(float value)
	{
		____currentFraction_11 = value;
	}

	inline static int32_t get_offset_of__maxValue_12() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ____maxValue_12)); }
	inline float get__maxValue_12() const { return ____maxValue_12; }
	inline float* get_address_of__maxValue_12() { return &____maxValue_12; }
	inline void set__maxValue_12(float value)
	{
		____maxValue_12 = value;
	}

	inline static int32_t get_offset_of_targetFill_13() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___targetFill_13)); }
	inline float get_targetFill_13() const { return ___targetFill_13; }
	inline float* get_address_of_targetFill_13() { return &___targetFill_13; }
	inline void set_targetFill_13(float value)
	{
		___targetFill_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEHEALTHBAR_T721070758_H
#ifndef ASTEROIDCONTROLLER_T3828879615_H
#define ASTEROIDCONTROLLER_T3828879615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.AsteroidController
struct  AsteroidController_t3828879615  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody SimpleHealthBar_SpaceshipExample.AsteroidController::myRigidbody
	Rigidbody_t3916780224 * ___myRigidbody_4;
	// System.Boolean SimpleHealthBar_SpaceshipExample.AsteroidController::canDestroy
	bool ___canDestroy_5;
	// System.Boolean SimpleHealthBar_SpaceshipExample.AsteroidController::isDestroyed
	bool ___isDestroyed_6;
	// System.Boolean SimpleHealthBar_SpaceshipExample.AsteroidController::isDebris
	bool ___isDebris_7;
	// System.Single SimpleHealthBar_SpaceshipExample.AsteroidController::health
	float ___health_8;
	// System.Single SimpleHealthBar_SpaceshipExample.AsteroidController::maxHealth
	float ___maxHealth_9;

public:
	inline static int32_t get_offset_of_myRigidbody_4() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___myRigidbody_4)); }
	inline Rigidbody_t3916780224 * get_myRigidbody_4() const { return ___myRigidbody_4; }
	inline Rigidbody_t3916780224 ** get_address_of_myRigidbody_4() { return &___myRigidbody_4; }
	inline void set_myRigidbody_4(Rigidbody_t3916780224 * value)
	{
		___myRigidbody_4 = value;
		Il2CppCodeGenWriteBarrier((&___myRigidbody_4), value);
	}

	inline static int32_t get_offset_of_canDestroy_5() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___canDestroy_5)); }
	inline bool get_canDestroy_5() const { return ___canDestroy_5; }
	inline bool* get_address_of_canDestroy_5() { return &___canDestroy_5; }
	inline void set_canDestroy_5(bool value)
	{
		___canDestroy_5 = value;
	}

	inline static int32_t get_offset_of_isDestroyed_6() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___isDestroyed_6)); }
	inline bool get_isDestroyed_6() const { return ___isDestroyed_6; }
	inline bool* get_address_of_isDestroyed_6() { return &___isDestroyed_6; }
	inline void set_isDestroyed_6(bool value)
	{
		___isDestroyed_6 = value;
	}

	inline static int32_t get_offset_of_isDebris_7() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___isDebris_7)); }
	inline bool get_isDebris_7() const { return ___isDebris_7; }
	inline bool* get_address_of_isDebris_7() { return &___isDebris_7; }
	inline void set_isDebris_7(bool value)
	{
		___isDebris_7 = value;
	}

	inline static int32_t get_offset_of_health_8() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___health_8)); }
	inline float get_health_8() const { return ___health_8; }
	inline float* get_address_of_health_8() { return &___health_8; }
	inline void set_health_8(float value)
	{
		___health_8 = value;
	}

	inline static int32_t get_offset_of_maxHealth_9() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___maxHealth_9)); }
	inline float get_maxHealth_9() const { return ___maxHealth_9; }
	inline float* get_address_of_maxHealth_9() { return &___maxHealth_9; }
	inline void set_maxHealth_9(float value)
	{
		___maxHealth_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTEROIDCONTROLLER_T3828879615_H
#ifndef GAMEMANAGER_T3924726104_H
#define GAMEMANAGER_T3924726104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager
struct  GameManager_t3924726104  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::astroidPrefab
	GameObject_t1113636619 * ___astroidPrefab_5;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::debrisPrefab
	GameObject_t1113636619 * ___debrisPrefab_6;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::explosionPrefab
	GameObject_t1113636619 * ___explosionPrefab_7;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::healthPickupPrefab
	GameObject_t1113636619 * ___healthPickupPrefab_8;
	// System.Boolean SimpleHealthBar_SpaceshipExample.GameManager::spawning
	bool ___spawning_9;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::spawnTimeMin
	float ___spawnTimeMin_10;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::spawnTimeMax
	float ___spawnTimeMax_11;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::startingAsteroids
	int32_t ___startingAsteroids_12;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::healthSpawnTimeMin
	float ___healthSpawnTimeMin_13;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::healthSpawnTimeMax
	float ___healthSpawnTimeMax_14;
	// UnityEngine.UI.Text SimpleHealthBar_SpaceshipExample.GameManager::scoreText
	Text_t1901882714 * ___scoreText_15;
	// UnityEngine.UI.Text SimpleHealthBar_SpaceshipExample.GameManager::finalScoreText
	Text_t1901882714 * ___finalScoreText_16;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::score
	int32_t ___score_17;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::asteroidPoints
	int32_t ___asteroidPoints_18;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::debrisPoints
	int32_t ___debrisPoints_19;
	// UnityEngine.UI.Image SimpleHealthBar_SpaceshipExample.GameManager::gameOverScreen
	Image_t2670269651 * ___gameOverScreen_20;
	// UnityEngine.UI.Text SimpleHealthBar_SpaceshipExample.GameManager::gameOverText
	Text_t1901882714 * ___gameOverText_21;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::asteroidHealth
	int32_t ___asteroidHealth_22;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::debrisHealth
	int32_t ___debrisHealth_23;
	// System.Boolean SimpleHealthBar_SpaceshipExample.GameManager::hasLost
	bool ___hasLost_24;

public:
	inline static int32_t get_offset_of_astroidPrefab_5() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___astroidPrefab_5)); }
	inline GameObject_t1113636619 * get_astroidPrefab_5() const { return ___astroidPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_astroidPrefab_5() { return &___astroidPrefab_5; }
	inline void set_astroidPrefab_5(GameObject_t1113636619 * value)
	{
		___astroidPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___astroidPrefab_5), value);
	}

	inline static int32_t get_offset_of_debrisPrefab_6() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___debrisPrefab_6)); }
	inline GameObject_t1113636619 * get_debrisPrefab_6() const { return ___debrisPrefab_6; }
	inline GameObject_t1113636619 ** get_address_of_debrisPrefab_6() { return &___debrisPrefab_6; }
	inline void set_debrisPrefab_6(GameObject_t1113636619 * value)
	{
		___debrisPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___debrisPrefab_6), value);
	}

	inline static int32_t get_offset_of_explosionPrefab_7() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___explosionPrefab_7)); }
	inline GameObject_t1113636619 * get_explosionPrefab_7() const { return ___explosionPrefab_7; }
	inline GameObject_t1113636619 ** get_address_of_explosionPrefab_7() { return &___explosionPrefab_7; }
	inline void set_explosionPrefab_7(GameObject_t1113636619 * value)
	{
		___explosionPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___explosionPrefab_7), value);
	}

	inline static int32_t get_offset_of_healthPickupPrefab_8() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___healthPickupPrefab_8)); }
	inline GameObject_t1113636619 * get_healthPickupPrefab_8() const { return ___healthPickupPrefab_8; }
	inline GameObject_t1113636619 ** get_address_of_healthPickupPrefab_8() { return &___healthPickupPrefab_8; }
	inline void set_healthPickupPrefab_8(GameObject_t1113636619 * value)
	{
		___healthPickupPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___healthPickupPrefab_8), value);
	}

	inline static int32_t get_offset_of_spawning_9() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___spawning_9)); }
	inline bool get_spawning_9() const { return ___spawning_9; }
	inline bool* get_address_of_spawning_9() { return &___spawning_9; }
	inline void set_spawning_9(bool value)
	{
		___spawning_9 = value;
	}

	inline static int32_t get_offset_of_spawnTimeMin_10() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___spawnTimeMin_10)); }
	inline float get_spawnTimeMin_10() const { return ___spawnTimeMin_10; }
	inline float* get_address_of_spawnTimeMin_10() { return &___spawnTimeMin_10; }
	inline void set_spawnTimeMin_10(float value)
	{
		___spawnTimeMin_10 = value;
	}

	inline static int32_t get_offset_of_spawnTimeMax_11() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___spawnTimeMax_11)); }
	inline float get_spawnTimeMax_11() const { return ___spawnTimeMax_11; }
	inline float* get_address_of_spawnTimeMax_11() { return &___spawnTimeMax_11; }
	inline void set_spawnTimeMax_11(float value)
	{
		___spawnTimeMax_11 = value;
	}

	inline static int32_t get_offset_of_startingAsteroids_12() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___startingAsteroids_12)); }
	inline int32_t get_startingAsteroids_12() const { return ___startingAsteroids_12; }
	inline int32_t* get_address_of_startingAsteroids_12() { return &___startingAsteroids_12; }
	inline void set_startingAsteroids_12(int32_t value)
	{
		___startingAsteroids_12 = value;
	}

	inline static int32_t get_offset_of_healthSpawnTimeMin_13() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___healthSpawnTimeMin_13)); }
	inline float get_healthSpawnTimeMin_13() const { return ___healthSpawnTimeMin_13; }
	inline float* get_address_of_healthSpawnTimeMin_13() { return &___healthSpawnTimeMin_13; }
	inline void set_healthSpawnTimeMin_13(float value)
	{
		___healthSpawnTimeMin_13 = value;
	}

	inline static int32_t get_offset_of_healthSpawnTimeMax_14() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___healthSpawnTimeMax_14)); }
	inline float get_healthSpawnTimeMax_14() const { return ___healthSpawnTimeMax_14; }
	inline float* get_address_of_healthSpawnTimeMax_14() { return &___healthSpawnTimeMax_14; }
	inline void set_healthSpawnTimeMax_14(float value)
	{
		___healthSpawnTimeMax_14 = value;
	}

	inline static int32_t get_offset_of_scoreText_15() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___scoreText_15)); }
	inline Text_t1901882714 * get_scoreText_15() const { return ___scoreText_15; }
	inline Text_t1901882714 ** get_address_of_scoreText_15() { return &___scoreText_15; }
	inline void set_scoreText_15(Text_t1901882714 * value)
	{
		___scoreText_15 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_15), value);
	}

	inline static int32_t get_offset_of_finalScoreText_16() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___finalScoreText_16)); }
	inline Text_t1901882714 * get_finalScoreText_16() const { return ___finalScoreText_16; }
	inline Text_t1901882714 ** get_address_of_finalScoreText_16() { return &___finalScoreText_16; }
	inline void set_finalScoreText_16(Text_t1901882714 * value)
	{
		___finalScoreText_16 = value;
		Il2CppCodeGenWriteBarrier((&___finalScoreText_16), value);
	}

	inline static int32_t get_offset_of_score_17() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___score_17)); }
	inline int32_t get_score_17() const { return ___score_17; }
	inline int32_t* get_address_of_score_17() { return &___score_17; }
	inline void set_score_17(int32_t value)
	{
		___score_17 = value;
	}

	inline static int32_t get_offset_of_asteroidPoints_18() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___asteroidPoints_18)); }
	inline int32_t get_asteroidPoints_18() const { return ___asteroidPoints_18; }
	inline int32_t* get_address_of_asteroidPoints_18() { return &___asteroidPoints_18; }
	inline void set_asteroidPoints_18(int32_t value)
	{
		___asteroidPoints_18 = value;
	}

	inline static int32_t get_offset_of_debrisPoints_19() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___debrisPoints_19)); }
	inline int32_t get_debrisPoints_19() const { return ___debrisPoints_19; }
	inline int32_t* get_address_of_debrisPoints_19() { return &___debrisPoints_19; }
	inline void set_debrisPoints_19(int32_t value)
	{
		___debrisPoints_19 = value;
	}

	inline static int32_t get_offset_of_gameOverScreen_20() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___gameOverScreen_20)); }
	inline Image_t2670269651 * get_gameOverScreen_20() const { return ___gameOverScreen_20; }
	inline Image_t2670269651 ** get_address_of_gameOverScreen_20() { return &___gameOverScreen_20; }
	inline void set_gameOverScreen_20(Image_t2670269651 * value)
	{
		___gameOverScreen_20 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverScreen_20), value);
	}

	inline static int32_t get_offset_of_gameOverText_21() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___gameOverText_21)); }
	inline Text_t1901882714 * get_gameOverText_21() const { return ___gameOverText_21; }
	inline Text_t1901882714 ** get_address_of_gameOverText_21() { return &___gameOverText_21; }
	inline void set_gameOverText_21(Text_t1901882714 * value)
	{
		___gameOverText_21 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverText_21), value);
	}

	inline static int32_t get_offset_of_asteroidHealth_22() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___asteroidHealth_22)); }
	inline int32_t get_asteroidHealth_22() const { return ___asteroidHealth_22; }
	inline int32_t* get_address_of_asteroidHealth_22() { return &___asteroidHealth_22; }
	inline void set_asteroidHealth_22(int32_t value)
	{
		___asteroidHealth_22 = value;
	}

	inline static int32_t get_offset_of_debrisHealth_23() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___debrisHealth_23)); }
	inline int32_t get_debrisHealth_23() const { return ___debrisHealth_23; }
	inline int32_t* get_address_of_debrisHealth_23() { return &___debrisHealth_23; }
	inline void set_debrisHealth_23(int32_t value)
	{
		___debrisHealth_23 = value;
	}

	inline static int32_t get_offset_of_hasLost_24() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___hasLost_24)); }
	inline bool get_hasLost_24() const { return ___hasLost_24; }
	inline bool* get_address_of_hasLost_24() { return &___hasLost_24; }
	inline void set_hasLost_24(bool value)
	{
		___hasLost_24 = value;
	}
};

struct GameManager_t3924726104_StaticFields
{
public:
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager::instance
	GameManager_t3924726104 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(GameManager_t3924726104_StaticFields, ___instance_4)); }
	inline GameManager_t3924726104 * get_instance_4() const { return ___instance_4; }
	inline GameManager_t3924726104 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GameManager_t3924726104 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T3924726104_H
#ifndef HEALTHPICKUPCONTROLLER_T1709822218_H
#define HEALTHPICKUPCONTROLLER_T1709822218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.HealthPickupController
struct  HealthPickupController_t1709822218  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody SimpleHealthBar_SpaceshipExample.HealthPickupController::myRigidbody
	Rigidbody_t3916780224 * ___myRigidbody_4;
	// UnityEngine.ParticleSystem SimpleHealthBar_SpaceshipExample.HealthPickupController::particles
	ParticleSystem_t1800779281 * ___particles_5;
	// UnityEngine.SpriteRenderer SimpleHealthBar_SpaceshipExample.HealthPickupController::mySprite
	SpriteRenderer_t3235626157 * ___mySprite_6;
	// System.Boolean SimpleHealthBar_SpaceshipExample.HealthPickupController::canDestroy
	bool ___canDestroy_7;
	// System.Boolean SimpleHealthBar_SpaceshipExample.HealthPickupController::canPickup
	bool ___canPickup_8;

public:
	inline static int32_t get_offset_of_myRigidbody_4() { return static_cast<int32_t>(offsetof(HealthPickupController_t1709822218, ___myRigidbody_4)); }
	inline Rigidbody_t3916780224 * get_myRigidbody_4() const { return ___myRigidbody_4; }
	inline Rigidbody_t3916780224 ** get_address_of_myRigidbody_4() { return &___myRigidbody_4; }
	inline void set_myRigidbody_4(Rigidbody_t3916780224 * value)
	{
		___myRigidbody_4 = value;
		Il2CppCodeGenWriteBarrier((&___myRigidbody_4), value);
	}

	inline static int32_t get_offset_of_particles_5() { return static_cast<int32_t>(offsetof(HealthPickupController_t1709822218, ___particles_5)); }
	inline ParticleSystem_t1800779281 * get_particles_5() const { return ___particles_5; }
	inline ParticleSystem_t1800779281 ** get_address_of_particles_5() { return &___particles_5; }
	inline void set_particles_5(ParticleSystem_t1800779281 * value)
	{
		___particles_5 = value;
		Il2CppCodeGenWriteBarrier((&___particles_5), value);
	}

	inline static int32_t get_offset_of_mySprite_6() { return static_cast<int32_t>(offsetof(HealthPickupController_t1709822218, ___mySprite_6)); }
	inline SpriteRenderer_t3235626157 * get_mySprite_6() const { return ___mySprite_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of_mySprite_6() { return &___mySprite_6; }
	inline void set_mySprite_6(SpriteRenderer_t3235626157 * value)
	{
		___mySprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___mySprite_6), value);
	}

	inline static int32_t get_offset_of_canDestroy_7() { return static_cast<int32_t>(offsetof(HealthPickupController_t1709822218, ___canDestroy_7)); }
	inline bool get_canDestroy_7() const { return ___canDestroy_7; }
	inline bool* get_address_of_canDestroy_7() { return &___canDestroy_7; }
	inline void set_canDestroy_7(bool value)
	{
		___canDestroy_7 = value;
	}

	inline static int32_t get_offset_of_canPickup_8() { return static_cast<int32_t>(offsetof(HealthPickupController_t1709822218, ___canPickup_8)); }
	inline bool get_canPickup_8() const { return ___canPickup_8; }
	inline bool* get_address_of_canPickup_8() { return &___canPickup_8; }
	inline void set_canPickup_8(bool value)
	{
		___canPickup_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTHPICKUPCONTROLLER_T1709822218_H
#ifndef PLAYERCONTROLLER_T2576101795_H
#define PLAYERCONTROLLER_T2576101795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerController
struct  PlayerController_t2576101795  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::rotationSpeed
	float ___rotationSpeed_5;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::accelerationSpeed
	float ___accelerationSpeed_6;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::maxSpeed
	float ___maxSpeed_7;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::shootingCooldown
	float ___shootingCooldown_8;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.PlayerController::bulletPrefab
	GameObject_t1113636619 * ___bulletPrefab_9;
	// UnityEngine.Rigidbody SimpleHealthBar_SpaceshipExample.PlayerController::myRigidbody
	Rigidbody_t3916780224 * ___myRigidbody_10;
	// UnityEngine.Transform SimpleHealthBar_SpaceshipExample.PlayerController::gunTrans
	Transform_t3600365921 * ___gunTrans_11;
	// UnityEngine.Transform SimpleHealthBar_SpaceshipExample.PlayerController::bulletSpawnPos
	Transform_t3600365921 * ___bulletSpawnPos_12;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::shootingTimer
	float ___shootingTimer_13;
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerController::canControl
	bool ___canControl_14;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::rotation
	float ___rotation_15;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::acceleration
	float ___acceleration_16;
	// UnityEngine.RectTransform SimpleHealthBar_SpaceshipExample.PlayerController::overheatVisual
	RectTransform_t3704657025 * ___overheatVisual_17;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::overheatTimer
	float ___overheatTimer_18;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::overheatTimerMax
	float ___overheatTimerMax_19;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::cooldownSpeed
	float ___cooldownSpeed_20;
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerController::canShoot
	bool ___canShoot_21;
	// SimpleHealthBar SimpleHealthBar_SpaceshipExample.PlayerController::gunHeatBar
	SimpleHealthBar_t721070758 * ___gunHeatBar_22;

public:
	inline static int32_t get_offset_of_rotationSpeed_5() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___rotationSpeed_5)); }
	inline float get_rotationSpeed_5() const { return ___rotationSpeed_5; }
	inline float* get_address_of_rotationSpeed_5() { return &___rotationSpeed_5; }
	inline void set_rotationSpeed_5(float value)
	{
		___rotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_accelerationSpeed_6() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___accelerationSpeed_6)); }
	inline float get_accelerationSpeed_6() const { return ___accelerationSpeed_6; }
	inline float* get_address_of_accelerationSpeed_6() { return &___accelerationSpeed_6; }
	inline void set_accelerationSpeed_6(float value)
	{
		___accelerationSpeed_6 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_7() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___maxSpeed_7)); }
	inline float get_maxSpeed_7() const { return ___maxSpeed_7; }
	inline float* get_address_of_maxSpeed_7() { return &___maxSpeed_7; }
	inline void set_maxSpeed_7(float value)
	{
		___maxSpeed_7 = value;
	}

	inline static int32_t get_offset_of_shootingCooldown_8() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___shootingCooldown_8)); }
	inline float get_shootingCooldown_8() const { return ___shootingCooldown_8; }
	inline float* get_address_of_shootingCooldown_8() { return &___shootingCooldown_8; }
	inline void set_shootingCooldown_8(float value)
	{
		___shootingCooldown_8 = value;
	}

	inline static int32_t get_offset_of_bulletPrefab_9() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___bulletPrefab_9)); }
	inline GameObject_t1113636619 * get_bulletPrefab_9() const { return ___bulletPrefab_9; }
	inline GameObject_t1113636619 ** get_address_of_bulletPrefab_9() { return &___bulletPrefab_9; }
	inline void set_bulletPrefab_9(GameObject_t1113636619 * value)
	{
		___bulletPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_9), value);
	}

	inline static int32_t get_offset_of_myRigidbody_10() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___myRigidbody_10)); }
	inline Rigidbody_t3916780224 * get_myRigidbody_10() const { return ___myRigidbody_10; }
	inline Rigidbody_t3916780224 ** get_address_of_myRigidbody_10() { return &___myRigidbody_10; }
	inline void set_myRigidbody_10(Rigidbody_t3916780224 * value)
	{
		___myRigidbody_10 = value;
		Il2CppCodeGenWriteBarrier((&___myRigidbody_10), value);
	}

	inline static int32_t get_offset_of_gunTrans_11() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___gunTrans_11)); }
	inline Transform_t3600365921 * get_gunTrans_11() const { return ___gunTrans_11; }
	inline Transform_t3600365921 ** get_address_of_gunTrans_11() { return &___gunTrans_11; }
	inline void set_gunTrans_11(Transform_t3600365921 * value)
	{
		___gunTrans_11 = value;
		Il2CppCodeGenWriteBarrier((&___gunTrans_11), value);
	}

	inline static int32_t get_offset_of_bulletSpawnPos_12() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___bulletSpawnPos_12)); }
	inline Transform_t3600365921 * get_bulletSpawnPos_12() const { return ___bulletSpawnPos_12; }
	inline Transform_t3600365921 ** get_address_of_bulletSpawnPos_12() { return &___bulletSpawnPos_12; }
	inline void set_bulletSpawnPos_12(Transform_t3600365921 * value)
	{
		___bulletSpawnPos_12 = value;
		Il2CppCodeGenWriteBarrier((&___bulletSpawnPos_12), value);
	}

	inline static int32_t get_offset_of_shootingTimer_13() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___shootingTimer_13)); }
	inline float get_shootingTimer_13() const { return ___shootingTimer_13; }
	inline float* get_address_of_shootingTimer_13() { return &___shootingTimer_13; }
	inline void set_shootingTimer_13(float value)
	{
		___shootingTimer_13 = value;
	}

	inline static int32_t get_offset_of_canControl_14() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___canControl_14)); }
	inline bool get_canControl_14() const { return ___canControl_14; }
	inline bool* get_address_of_canControl_14() { return &___canControl_14; }
	inline void set_canControl_14(bool value)
	{
		___canControl_14 = value;
	}

	inline static int32_t get_offset_of_rotation_15() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___rotation_15)); }
	inline float get_rotation_15() const { return ___rotation_15; }
	inline float* get_address_of_rotation_15() { return &___rotation_15; }
	inline void set_rotation_15(float value)
	{
		___rotation_15 = value;
	}

	inline static int32_t get_offset_of_acceleration_16() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___acceleration_16)); }
	inline float get_acceleration_16() const { return ___acceleration_16; }
	inline float* get_address_of_acceleration_16() { return &___acceleration_16; }
	inline void set_acceleration_16(float value)
	{
		___acceleration_16 = value;
	}

	inline static int32_t get_offset_of_overheatVisual_17() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___overheatVisual_17)); }
	inline RectTransform_t3704657025 * get_overheatVisual_17() const { return ___overheatVisual_17; }
	inline RectTransform_t3704657025 ** get_address_of_overheatVisual_17() { return &___overheatVisual_17; }
	inline void set_overheatVisual_17(RectTransform_t3704657025 * value)
	{
		___overheatVisual_17 = value;
		Il2CppCodeGenWriteBarrier((&___overheatVisual_17), value);
	}

	inline static int32_t get_offset_of_overheatTimer_18() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___overheatTimer_18)); }
	inline float get_overheatTimer_18() const { return ___overheatTimer_18; }
	inline float* get_address_of_overheatTimer_18() { return &___overheatTimer_18; }
	inline void set_overheatTimer_18(float value)
	{
		___overheatTimer_18 = value;
	}

	inline static int32_t get_offset_of_overheatTimerMax_19() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___overheatTimerMax_19)); }
	inline float get_overheatTimerMax_19() const { return ___overheatTimerMax_19; }
	inline float* get_address_of_overheatTimerMax_19() { return &___overheatTimerMax_19; }
	inline void set_overheatTimerMax_19(float value)
	{
		___overheatTimerMax_19 = value;
	}

	inline static int32_t get_offset_of_cooldownSpeed_20() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___cooldownSpeed_20)); }
	inline float get_cooldownSpeed_20() const { return ___cooldownSpeed_20; }
	inline float* get_address_of_cooldownSpeed_20() { return &___cooldownSpeed_20; }
	inline void set_cooldownSpeed_20(float value)
	{
		___cooldownSpeed_20 = value;
	}

	inline static int32_t get_offset_of_canShoot_21() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___canShoot_21)); }
	inline bool get_canShoot_21() const { return ___canShoot_21; }
	inline bool* get_address_of_canShoot_21() { return &___canShoot_21; }
	inline void set_canShoot_21(bool value)
	{
		___canShoot_21 = value;
	}

	inline static int32_t get_offset_of_gunHeatBar_22() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___gunHeatBar_22)); }
	inline SimpleHealthBar_t721070758 * get_gunHeatBar_22() const { return ___gunHeatBar_22; }
	inline SimpleHealthBar_t721070758 ** get_address_of_gunHeatBar_22() { return &___gunHeatBar_22; }
	inline void set_gunHeatBar_22(SimpleHealthBar_t721070758 * value)
	{
		___gunHeatBar_22 = value;
		Il2CppCodeGenWriteBarrier((&___gunHeatBar_22), value);
	}
};

struct PlayerController_t2576101795_StaticFields
{
public:
	// SimpleHealthBar_SpaceshipExample.PlayerController SimpleHealthBar_SpaceshipExample.PlayerController::instance
	PlayerController_t2576101795 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795_StaticFields, ___instance_4)); }
	inline PlayerController_t2576101795 * get_instance_4() const { return ___instance_4; }
	inline PlayerController_t2576101795 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(PlayerController_t2576101795 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T2576101795_H
#ifndef PLAYERHEALTH_T992877501_H
#define PLAYERHEALTH_T992877501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerHealth
struct  PlayerHealth_t992877501  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerHealth::canTakeDamage
	bool ___canTakeDamage_5;
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth::maxHealth
	int32_t ___maxHealth_6;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::currentHealth
	float ___currentHealth_7;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::invulnerabilityTime
	float ___invulnerabilityTime_8;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::currentShield
	float ___currentShield_9;
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth::maxShield
	int32_t ___maxShield_10;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::regenShieldTimer
	float ___regenShieldTimer_11;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::regenShieldTimerMax
	float ___regenShieldTimerMax_12;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.PlayerHealth::explosionParticles
	GameObject_t1113636619 * ___explosionParticles_13;
	// SimpleHealthBar SimpleHealthBar_SpaceshipExample.PlayerHealth::healthBar
	SimpleHealthBar_t721070758 * ___healthBar_14;
	// SimpleHealthBar SimpleHealthBar_SpaceshipExample.PlayerHealth::shieldBar
	SimpleHealthBar_t721070758 * ___shieldBar_15;

public:
	inline static int32_t get_offset_of_canTakeDamage_5() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___canTakeDamage_5)); }
	inline bool get_canTakeDamage_5() const { return ___canTakeDamage_5; }
	inline bool* get_address_of_canTakeDamage_5() { return &___canTakeDamage_5; }
	inline void set_canTakeDamage_5(bool value)
	{
		___canTakeDamage_5 = value;
	}

	inline static int32_t get_offset_of_maxHealth_6() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___maxHealth_6)); }
	inline int32_t get_maxHealth_6() const { return ___maxHealth_6; }
	inline int32_t* get_address_of_maxHealth_6() { return &___maxHealth_6; }
	inline void set_maxHealth_6(int32_t value)
	{
		___maxHealth_6 = value;
	}

	inline static int32_t get_offset_of_currentHealth_7() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___currentHealth_7)); }
	inline float get_currentHealth_7() const { return ___currentHealth_7; }
	inline float* get_address_of_currentHealth_7() { return &___currentHealth_7; }
	inline void set_currentHealth_7(float value)
	{
		___currentHealth_7 = value;
	}

	inline static int32_t get_offset_of_invulnerabilityTime_8() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___invulnerabilityTime_8)); }
	inline float get_invulnerabilityTime_8() const { return ___invulnerabilityTime_8; }
	inline float* get_address_of_invulnerabilityTime_8() { return &___invulnerabilityTime_8; }
	inline void set_invulnerabilityTime_8(float value)
	{
		___invulnerabilityTime_8 = value;
	}

	inline static int32_t get_offset_of_currentShield_9() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___currentShield_9)); }
	inline float get_currentShield_9() const { return ___currentShield_9; }
	inline float* get_address_of_currentShield_9() { return &___currentShield_9; }
	inline void set_currentShield_9(float value)
	{
		___currentShield_9 = value;
	}

	inline static int32_t get_offset_of_maxShield_10() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___maxShield_10)); }
	inline int32_t get_maxShield_10() const { return ___maxShield_10; }
	inline int32_t* get_address_of_maxShield_10() { return &___maxShield_10; }
	inline void set_maxShield_10(int32_t value)
	{
		___maxShield_10 = value;
	}

	inline static int32_t get_offset_of_regenShieldTimer_11() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___regenShieldTimer_11)); }
	inline float get_regenShieldTimer_11() const { return ___regenShieldTimer_11; }
	inline float* get_address_of_regenShieldTimer_11() { return &___regenShieldTimer_11; }
	inline void set_regenShieldTimer_11(float value)
	{
		___regenShieldTimer_11 = value;
	}

	inline static int32_t get_offset_of_regenShieldTimerMax_12() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___regenShieldTimerMax_12)); }
	inline float get_regenShieldTimerMax_12() const { return ___regenShieldTimerMax_12; }
	inline float* get_address_of_regenShieldTimerMax_12() { return &___regenShieldTimerMax_12; }
	inline void set_regenShieldTimerMax_12(float value)
	{
		___regenShieldTimerMax_12 = value;
	}

	inline static int32_t get_offset_of_explosionParticles_13() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___explosionParticles_13)); }
	inline GameObject_t1113636619 * get_explosionParticles_13() const { return ___explosionParticles_13; }
	inline GameObject_t1113636619 ** get_address_of_explosionParticles_13() { return &___explosionParticles_13; }
	inline void set_explosionParticles_13(GameObject_t1113636619 * value)
	{
		___explosionParticles_13 = value;
		Il2CppCodeGenWriteBarrier((&___explosionParticles_13), value);
	}

	inline static int32_t get_offset_of_healthBar_14() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___healthBar_14)); }
	inline SimpleHealthBar_t721070758 * get_healthBar_14() const { return ___healthBar_14; }
	inline SimpleHealthBar_t721070758 ** get_address_of_healthBar_14() { return &___healthBar_14; }
	inline void set_healthBar_14(SimpleHealthBar_t721070758 * value)
	{
		___healthBar_14 = value;
		Il2CppCodeGenWriteBarrier((&___healthBar_14), value);
	}

	inline static int32_t get_offset_of_shieldBar_15() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___shieldBar_15)); }
	inline SimpleHealthBar_t721070758 * get_shieldBar_15() const { return ___shieldBar_15; }
	inline SimpleHealthBar_t721070758 ** get_address_of_shieldBar_15() { return &___shieldBar_15; }
	inline void set_shieldBar_15(SimpleHealthBar_t721070758 * value)
	{
		___shieldBar_15 = value;
		Il2CppCodeGenWriteBarrier((&___shieldBar_15), value);
	}
};

struct PlayerHealth_t992877501_StaticFields
{
public:
	// SimpleHealthBar_SpaceshipExample.PlayerHealth SimpleHealthBar_SpaceshipExample.PlayerHealth::instance
	PlayerHealth_t992877501 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501_StaticFields, ___instance_4)); }
	inline PlayerHealth_t992877501 * get_instance_4() const { return ___instance_4; }
	inline PlayerHealth_t992877501 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(PlayerHealth_t992877501 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERHEALTH_T992877501_H
#ifndef SLASHERNAV_T209608926_H
#define SLASHERNAV_T209608926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SlasherNAV
struct  SlasherNAV_t209608926  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AI.NavMeshAgent SlasherNAV::<NMA>k__BackingField
	NavMeshAgent_t1276799816 * ___U3CNMAU3Ek__BackingField_4;
	// UnityEngine.Transform SlasherNAV::target
	Transform_t3600365921 * ___target_5;

public:
	inline static int32_t get_offset_of_U3CNMAU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SlasherNAV_t209608926, ___U3CNMAU3Ek__BackingField_4)); }
	inline NavMeshAgent_t1276799816 * get_U3CNMAU3Ek__BackingField_4() const { return ___U3CNMAU3Ek__BackingField_4; }
	inline NavMeshAgent_t1276799816 ** get_address_of_U3CNMAU3Ek__BackingField_4() { return &___U3CNMAU3Ek__BackingField_4; }
	inline void set_U3CNMAU3Ek__BackingField_4(NavMeshAgent_t1276799816 * value)
	{
		___U3CNMAU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNMAU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(SlasherNAV_t209608926, ___target_5)); }
	inline Transform_t3600365921 * get_target_5() const { return ___target_5; }
	inline Transform_t3600365921 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Transform_t3600365921 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((&___target_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLASHERNAV_T209608926_H
#ifndef VILLAGERINTERACTION_T490587617_H
#define VILLAGERINTERACTION_T490587617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VillagerInteraction
struct  VillagerInteraction_t490587617  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean VillagerInteraction::isTalking
	bool ___isTalking_4;
	// UnityEngine.AudioSource VillagerInteraction::AS
	AudioSource_t3935305588 * ___AS_5;
	// UnityEngine.GameObject VillagerInteraction::UI
	GameObject_t1113636619 * ___UI_6;
	// System.Boolean VillagerInteraction::p1Active
	bool ___p1Active_7;
	// System.Boolean VillagerInteraction::p2Active
	bool ___p2Active_8;
	// UnityEngine.AudioClip VillagerInteraction::talkSound
	AudioClip_t3680889665 * ___talkSound_9;
	// UnityEngine.BoxCollider VillagerInteraction::BC
	BoxCollider_t1640800422 * ___BC_10;
	// System.Boolean VillagerInteraction::onePlayer
	bool ___onePlayer_11;

public:
	inline static int32_t get_offset_of_isTalking_4() { return static_cast<int32_t>(offsetof(VillagerInteraction_t490587617, ___isTalking_4)); }
	inline bool get_isTalking_4() const { return ___isTalking_4; }
	inline bool* get_address_of_isTalking_4() { return &___isTalking_4; }
	inline void set_isTalking_4(bool value)
	{
		___isTalking_4 = value;
	}

	inline static int32_t get_offset_of_AS_5() { return static_cast<int32_t>(offsetof(VillagerInteraction_t490587617, ___AS_5)); }
	inline AudioSource_t3935305588 * get_AS_5() const { return ___AS_5; }
	inline AudioSource_t3935305588 ** get_address_of_AS_5() { return &___AS_5; }
	inline void set_AS_5(AudioSource_t3935305588 * value)
	{
		___AS_5 = value;
		Il2CppCodeGenWriteBarrier((&___AS_5), value);
	}

	inline static int32_t get_offset_of_UI_6() { return static_cast<int32_t>(offsetof(VillagerInteraction_t490587617, ___UI_6)); }
	inline GameObject_t1113636619 * get_UI_6() const { return ___UI_6; }
	inline GameObject_t1113636619 ** get_address_of_UI_6() { return &___UI_6; }
	inline void set_UI_6(GameObject_t1113636619 * value)
	{
		___UI_6 = value;
		Il2CppCodeGenWriteBarrier((&___UI_6), value);
	}

	inline static int32_t get_offset_of_p1Active_7() { return static_cast<int32_t>(offsetof(VillagerInteraction_t490587617, ___p1Active_7)); }
	inline bool get_p1Active_7() const { return ___p1Active_7; }
	inline bool* get_address_of_p1Active_7() { return &___p1Active_7; }
	inline void set_p1Active_7(bool value)
	{
		___p1Active_7 = value;
	}

	inline static int32_t get_offset_of_p2Active_8() { return static_cast<int32_t>(offsetof(VillagerInteraction_t490587617, ___p2Active_8)); }
	inline bool get_p2Active_8() const { return ___p2Active_8; }
	inline bool* get_address_of_p2Active_8() { return &___p2Active_8; }
	inline void set_p2Active_8(bool value)
	{
		___p2Active_8 = value;
	}

	inline static int32_t get_offset_of_talkSound_9() { return static_cast<int32_t>(offsetof(VillagerInteraction_t490587617, ___talkSound_9)); }
	inline AudioClip_t3680889665 * get_talkSound_9() const { return ___talkSound_9; }
	inline AudioClip_t3680889665 ** get_address_of_talkSound_9() { return &___talkSound_9; }
	inline void set_talkSound_9(AudioClip_t3680889665 * value)
	{
		___talkSound_9 = value;
		Il2CppCodeGenWriteBarrier((&___talkSound_9), value);
	}

	inline static int32_t get_offset_of_BC_10() { return static_cast<int32_t>(offsetof(VillagerInteraction_t490587617, ___BC_10)); }
	inline BoxCollider_t1640800422 * get_BC_10() const { return ___BC_10; }
	inline BoxCollider_t1640800422 ** get_address_of_BC_10() { return &___BC_10; }
	inline void set_BC_10(BoxCollider_t1640800422 * value)
	{
		___BC_10 = value;
		Il2CppCodeGenWriteBarrier((&___BC_10), value);
	}

	inline static int32_t get_offset_of_onePlayer_11() { return static_cast<int32_t>(offsetof(VillagerInteraction_t490587617, ___onePlayer_11)); }
	inline bool get_onePlayer_11() const { return ___onePlayer_11; }
	inline bool* get_address_of_onePlayer_11() { return &___onePlayer_11; }
	inline void set_onePlayer_11(bool value)
	{
		___onePlayer_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VILLAGERINTERACTION_T490587617_H
#ifndef WARRIORANIMATIONDEMOFREE_T1507088667_H
#define WARRIORANIMATIONDEMOFREE_T1507088667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WarriorAnimationDemoFREE
struct  WarriorAnimationDemoFREE_t1507088667  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator WarriorAnimationDemoFREE::animator
	Animator_t434523843 * ___animator_4;
	// System.Single WarriorAnimationDemoFREE::rotationSpeed
	float ___rotationSpeed_5;
	// UnityEngine.Vector3 WarriorAnimationDemoFREE::inputVec
	Vector3_t3722313464  ___inputVec_6;
	// UnityEngine.Vector3 WarriorAnimationDemoFREE::targetDirection
	Vector3_t3722313464  ___targetDirection_7;
	// UnityEngine.Transform WarriorAnimationDemoFREE::cameraTransform
	Transform_t3600365921 * ___cameraTransform_8;
	// System.Boolean WarriorAnimationDemoFREE::camBool1
	bool ___camBool1_9;
	// System.Boolean WarriorAnimationDemoFREE::camBool2
	bool ___camBool2_10;
	// UnityEngine.GameObject WarriorAnimationDemoFREE::cam1
	GameObject_t1113636619 * ___cam1_11;
	// UnityEngine.GameObject WarriorAnimationDemoFREE::cam2
	GameObject_t1113636619 * ___cam2_12;
	// WarriorAnimationDemoFREE/Warrior WarriorAnimationDemoFREE::warrior
	int32_t ___warrior_13;
	// System.Single WarriorAnimationDemoFREE::gravity
	float ___gravity_14;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(WarriorAnimationDemoFREE_t1507088667, ___animator_4)); }
	inline Animator_t434523843 * get_animator_4() const { return ___animator_4; }
	inline Animator_t434523843 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t434523843 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}

	inline static int32_t get_offset_of_rotationSpeed_5() { return static_cast<int32_t>(offsetof(WarriorAnimationDemoFREE_t1507088667, ___rotationSpeed_5)); }
	inline float get_rotationSpeed_5() const { return ___rotationSpeed_5; }
	inline float* get_address_of_rotationSpeed_5() { return &___rotationSpeed_5; }
	inline void set_rotationSpeed_5(float value)
	{
		___rotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_inputVec_6() { return static_cast<int32_t>(offsetof(WarriorAnimationDemoFREE_t1507088667, ___inputVec_6)); }
	inline Vector3_t3722313464  get_inputVec_6() const { return ___inputVec_6; }
	inline Vector3_t3722313464 * get_address_of_inputVec_6() { return &___inputVec_6; }
	inline void set_inputVec_6(Vector3_t3722313464  value)
	{
		___inputVec_6 = value;
	}

	inline static int32_t get_offset_of_targetDirection_7() { return static_cast<int32_t>(offsetof(WarriorAnimationDemoFREE_t1507088667, ___targetDirection_7)); }
	inline Vector3_t3722313464  get_targetDirection_7() const { return ___targetDirection_7; }
	inline Vector3_t3722313464 * get_address_of_targetDirection_7() { return &___targetDirection_7; }
	inline void set_targetDirection_7(Vector3_t3722313464  value)
	{
		___targetDirection_7 = value;
	}

	inline static int32_t get_offset_of_cameraTransform_8() { return static_cast<int32_t>(offsetof(WarriorAnimationDemoFREE_t1507088667, ___cameraTransform_8)); }
	inline Transform_t3600365921 * get_cameraTransform_8() const { return ___cameraTransform_8; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_8() { return &___cameraTransform_8; }
	inline void set_cameraTransform_8(Transform_t3600365921 * value)
	{
		___cameraTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_8), value);
	}

	inline static int32_t get_offset_of_camBool1_9() { return static_cast<int32_t>(offsetof(WarriorAnimationDemoFREE_t1507088667, ___camBool1_9)); }
	inline bool get_camBool1_9() const { return ___camBool1_9; }
	inline bool* get_address_of_camBool1_9() { return &___camBool1_9; }
	inline void set_camBool1_9(bool value)
	{
		___camBool1_9 = value;
	}

	inline static int32_t get_offset_of_camBool2_10() { return static_cast<int32_t>(offsetof(WarriorAnimationDemoFREE_t1507088667, ___camBool2_10)); }
	inline bool get_camBool2_10() const { return ___camBool2_10; }
	inline bool* get_address_of_camBool2_10() { return &___camBool2_10; }
	inline void set_camBool2_10(bool value)
	{
		___camBool2_10 = value;
	}

	inline static int32_t get_offset_of_cam1_11() { return static_cast<int32_t>(offsetof(WarriorAnimationDemoFREE_t1507088667, ___cam1_11)); }
	inline GameObject_t1113636619 * get_cam1_11() const { return ___cam1_11; }
	inline GameObject_t1113636619 ** get_address_of_cam1_11() { return &___cam1_11; }
	inline void set_cam1_11(GameObject_t1113636619 * value)
	{
		___cam1_11 = value;
		Il2CppCodeGenWriteBarrier((&___cam1_11), value);
	}

	inline static int32_t get_offset_of_cam2_12() { return static_cast<int32_t>(offsetof(WarriorAnimationDemoFREE_t1507088667, ___cam2_12)); }
	inline GameObject_t1113636619 * get_cam2_12() const { return ___cam2_12; }
	inline GameObject_t1113636619 ** get_address_of_cam2_12() { return &___cam2_12; }
	inline void set_cam2_12(GameObject_t1113636619 * value)
	{
		___cam2_12 = value;
		Il2CppCodeGenWriteBarrier((&___cam2_12), value);
	}

	inline static int32_t get_offset_of_warrior_13() { return static_cast<int32_t>(offsetof(WarriorAnimationDemoFREE_t1507088667, ___warrior_13)); }
	inline int32_t get_warrior_13() const { return ___warrior_13; }
	inline int32_t* get_address_of_warrior_13() { return &___warrior_13; }
	inline void set_warrior_13(int32_t value)
	{
		___warrior_13 = value;
	}

	inline static int32_t get_offset_of_gravity_14() { return static_cast<int32_t>(offsetof(WarriorAnimationDemoFREE_t1507088667, ___gravity_14)); }
	inline float get_gravity_14() const { return ___gravity_14; }
	inline float* get_address_of_gravity_14() { return &___gravity_14; }
	inline void set_gravity_14(float value)
	{
		___gravity_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARRIORANIMATIONDEMOFREE_T1507088667_H
#ifndef WARRIORP2_T1586698613_H
#define WARRIORP2_T1586698613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WarriorP2
struct  WarriorP2_t1586698613  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator WarriorP2::animator
	Animator_t434523843 * ___animator_4;
	// System.Single WarriorP2::rotationSpeed
	float ___rotationSpeed_5;
	// UnityEngine.Vector3 WarriorP2::inputVec
	Vector3_t3722313464  ___inputVec_6;
	// UnityEngine.Vector3 WarriorP2::targetDirection
	Vector3_t3722313464  ___targetDirection_7;
	// System.Boolean WarriorP2::camBool1
	bool ___camBool1_8;
	// System.Boolean WarriorP2::camBool2
	bool ___camBool2_9;
	// UnityEngine.GameObject WarriorP2::cam1
	GameObject_t1113636619 * ___cam1_10;
	// UnityEngine.GameObject WarriorP2::cam2
	GameObject_t1113636619 * ___cam2_11;
	// UnityEngine.Transform WarriorP2::cameraTransform
	Transform_t3600365921 * ___cameraTransform_12;
	// WarriorP2/Warrior WarriorP2::warrior
	int32_t ___warrior_13;
	// System.Single WarriorP2::gravity
	float ___gravity_14;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(WarriorP2_t1586698613, ___animator_4)); }
	inline Animator_t434523843 * get_animator_4() const { return ___animator_4; }
	inline Animator_t434523843 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t434523843 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}

	inline static int32_t get_offset_of_rotationSpeed_5() { return static_cast<int32_t>(offsetof(WarriorP2_t1586698613, ___rotationSpeed_5)); }
	inline float get_rotationSpeed_5() const { return ___rotationSpeed_5; }
	inline float* get_address_of_rotationSpeed_5() { return &___rotationSpeed_5; }
	inline void set_rotationSpeed_5(float value)
	{
		___rotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_inputVec_6() { return static_cast<int32_t>(offsetof(WarriorP2_t1586698613, ___inputVec_6)); }
	inline Vector3_t3722313464  get_inputVec_6() const { return ___inputVec_6; }
	inline Vector3_t3722313464 * get_address_of_inputVec_6() { return &___inputVec_6; }
	inline void set_inputVec_6(Vector3_t3722313464  value)
	{
		___inputVec_6 = value;
	}

	inline static int32_t get_offset_of_targetDirection_7() { return static_cast<int32_t>(offsetof(WarriorP2_t1586698613, ___targetDirection_7)); }
	inline Vector3_t3722313464  get_targetDirection_7() const { return ___targetDirection_7; }
	inline Vector3_t3722313464 * get_address_of_targetDirection_7() { return &___targetDirection_7; }
	inline void set_targetDirection_7(Vector3_t3722313464  value)
	{
		___targetDirection_7 = value;
	}

	inline static int32_t get_offset_of_camBool1_8() { return static_cast<int32_t>(offsetof(WarriorP2_t1586698613, ___camBool1_8)); }
	inline bool get_camBool1_8() const { return ___camBool1_8; }
	inline bool* get_address_of_camBool1_8() { return &___camBool1_8; }
	inline void set_camBool1_8(bool value)
	{
		___camBool1_8 = value;
	}

	inline static int32_t get_offset_of_camBool2_9() { return static_cast<int32_t>(offsetof(WarriorP2_t1586698613, ___camBool2_9)); }
	inline bool get_camBool2_9() const { return ___camBool2_9; }
	inline bool* get_address_of_camBool2_9() { return &___camBool2_9; }
	inline void set_camBool2_9(bool value)
	{
		___camBool2_9 = value;
	}

	inline static int32_t get_offset_of_cam1_10() { return static_cast<int32_t>(offsetof(WarriorP2_t1586698613, ___cam1_10)); }
	inline GameObject_t1113636619 * get_cam1_10() const { return ___cam1_10; }
	inline GameObject_t1113636619 ** get_address_of_cam1_10() { return &___cam1_10; }
	inline void set_cam1_10(GameObject_t1113636619 * value)
	{
		___cam1_10 = value;
		Il2CppCodeGenWriteBarrier((&___cam1_10), value);
	}

	inline static int32_t get_offset_of_cam2_11() { return static_cast<int32_t>(offsetof(WarriorP2_t1586698613, ___cam2_11)); }
	inline GameObject_t1113636619 * get_cam2_11() const { return ___cam2_11; }
	inline GameObject_t1113636619 ** get_address_of_cam2_11() { return &___cam2_11; }
	inline void set_cam2_11(GameObject_t1113636619 * value)
	{
		___cam2_11 = value;
		Il2CppCodeGenWriteBarrier((&___cam2_11), value);
	}

	inline static int32_t get_offset_of_cameraTransform_12() { return static_cast<int32_t>(offsetof(WarriorP2_t1586698613, ___cameraTransform_12)); }
	inline Transform_t3600365921 * get_cameraTransform_12() const { return ___cameraTransform_12; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_12() { return &___cameraTransform_12; }
	inline void set_cameraTransform_12(Transform_t3600365921 * value)
	{
		___cameraTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_12), value);
	}

	inline static int32_t get_offset_of_warrior_13() { return static_cast<int32_t>(offsetof(WarriorP2_t1586698613, ___warrior_13)); }
	inline int32_t get_warrior_13() const { return ___warrior_13; }
	inline int32_t* get_address_of_warrior_13() { return &___warrior_13; }
	inline void set_warrior_13(int32_t value)
	{
		___warrior_13 = value;
	}

	inline static int32_t get_offset_of_gravity_14() { return static_cast<int32_t>(offsetof(WarriorP2_t1586698613, ___gravity_14)); }
	inline float get_gravity_14() const { return ___gravity_14; }
	inline float* get_address_of_gravity_14() { return &___gravity_14; }
	inline void set_gravity_14(float value)
	{
		___gravity_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARRIORP2_T1586698613_H
#ifndef WATERFLOAT_T311037891_H
#define WATERFLOAT_T311037891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WaterFloat
struct  WaterFloat_t311037891  : public MonoBehaviour_t3962482529
{
public:
	// System.Single WaterFloat::WaterHeight
	float ___WaterHeight_4;

public:
	inline static int32_t get_offset_of_WaterHeight_4() { return static_cast<int32_t>(offsetof(WaterFloat_t311037891, ___WaterHeight_4)); }
	inline float get_WaterHeight_4() const { return ___WaterHeight_4; }
	inline float* get_address_of_WaterHeight_4() { return &___WaterHeight_4; }
	inline void set_WaterHeight_4(float value)
	{
		___WaterHeight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERFLOAT_T311037891_H
#ifndef WIZARDCONTROLLER_T2370936914_H
#define WIZARDCONTROLLER_T2370936914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WizardController
struct  WizardController_t2370936914  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AI.NavMeshAgent WizardController::<agent>k__BackingField
	NavMeshAgent_t1276799816 * ___U3CagentU3Ek__BackingField_4;
	// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter WizardController::<character>k__BackingField
	ThirdPersonCharacter_t1711070432 * ___U3CcharacterU3Ek__BackingField_5;
	// System.Single WizardController::enemyWalkSpeed
	float ___enemyWalkSpeed_6;
	// System.Single WizardController::enemyRunSpeed
	float ___enemyRunSpeed_7;
	// UnityEngine.GameObject[] WizardController::players
	GameObjectU5BU5D_t3328599146* ___players_8;
	// UnityEngine.GameObject[] WizardController::targets
	GameObjectU5BU5D_t3328599146* ___targets_9;
	// System.Single WizardController::attackRate
	float ___attackRate_11;
	// System.Single WizardController::soundDelay
	float ___soundDelay_12;
	// System.Single WizardController::damageAmount
	float ___damageAmount_13;
	// UnityEngine.GameObject WizardController::currentPlayer
	GameObject_t1113636619 * ___currentPlayer_14;
	// UnityEngine.GameObject WizardController::currentTarget
	GameObject_t1113636619 * ___currentTarget_15;
	// System.Single WizardController::timer
	float ___timer_16;
	// System.Single WizardController::timerRate
	float ___timerRate_17;
	// System.Single WizardController::nextAttack
	float ___nextAttack_18;
	// System.Single WizardController::playerDistance
	float ___playerDistance_19;
	// UnityEngine.Animator WizardController::anim
	Animator_t434523843 * ___anim_20;
	// System.Boolean WizardController::playerInRange
	bool ___playerInRange_21;
	// UnityEngine.AudioSource WizardController::AS
	AudioSource_t3935305588 * ___AS_22;
	// System.Int32 WizardController::index
	int32_t ___index_23;
	// UnityEngine.AudioClip[] WizardController::playerHurtClips
	AudioClipU5BU5D_t143221404* ___playerHurtClips_24;

public:
	inline static int32_t get_offset_of_U3CagentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___U3CagentU3Ek__BackingField_4)); }
	inline NavMeshAgent_t1276799816 * get_U3CagentU3Ek__BackingField_4() const { return ___U3CagentU3Ek__BackingField_4; }
	inline NavMeshAgent_t1276799816 ** get_address_of_U3CagentU3Ek__BackingField_4() { return &___U3CagentU3Ek__BackingField_4; }
	inline void set_U3CagentU3Ek__BackingField_4(NavMeshAgent_t1276799816 * value)
	{
		___U3CagentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CagentU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcharacterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___U3CcharacterU3Ek__BackingField_5)); }
	inline ThirdPersonCharacter_t1711070432 * get_U3CcharacterU3Ek__BackingField_5() const { return ___U3CcharacterU3Ek__BackingField_5; }
	inline ThirdPersonCharacter_t1711070432 ** get_address_of_U3CcharacterU3Ek__BackingField_5() { return &___U3CcharacterU3Ek__BackingField_5; }
	inline void set_U3CcharacterU3Ek__BackingField_5(ThirdPersonCharacter_t1711070432 * value)
	{
		___U3CcharacterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcharacterU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_enemyWalkSpeed_6() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___enemyWalkSpeed_6)); }
	inline float get_enemyWalkSpeed_6() const { return ___enemyWalkSpeed_6; }
	inline float* get_address_of_enemyWalkSpeed_6() { return &___enemyWalkSpeed_6; }
	inline void set_enemyWalkSpeed_6(float value)
	{
		___enemyWalkSpeed_6 = value;
	}

	inline static int32_t get_offset_of_enemyRunSpeed_7() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___enemyRunSpeed_7)); }
	inline float get_enemyRunSpeed_7() const { return ___enemyRunSpeed_7; }
	inline float* get_address_of_enemyRunSpeed_7() { return &___enemyRunSpeed_7; }
	inline void set_enemyRunSpeed_7(float value)
	{
		___enemyRunSpeed_7 = value;
	}

	inline static int32_t get_offset_of_players_8() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___players_8)); }
	inline GameObjectU5BU5D_t3328599146* get_players_8() const { return ___players_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_players_8() { return &___players_8; }
	inline void set_players_8(GameObjectU5BU5D_t3328599146* value)
	{
		___players_8 = value;
		Il2CppCodeGenWriteBarrier((&___players_8), value);
	}

	inline static int32_t get_offset_of_targets_9() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___targets_9)); }
	inline GameObjectU5BU5D_t3328599146* get_targets_9() const { return ___targets_9; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_targets_9() { return &___targets_9; }
	inline void set_targets_9(GameObjectU5BU5D_t3328599146* value)
	{
		___targets_9 = value;
		Il2CppCodeGenWriteBarrier((&___targets_9), value);
	}

	inline static int32_t get_offset_of_attackRate_11() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___attackRate_11)); }
	inline float get_attackRate_11() const { return ___attackRate_11; }
	inline float* get_address_of_attackRate_11() { return &___attackRate_11; }
	inline void set_attackRate_11(float value)
	{
		___attackRate_11 = value;
	}

	inline static int32_t get_offset_of_soundDelay_12() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___soundDelay_12)); }
	inline float get_soundDelay_12() const { return ___soundDelay_12; }
	inline float* get_address_of_soundDelay_12() { return &___soundDelay_12; }
	inline void set_soundDelay_12(float value)
	{
		___soundDelay_12 = value;
	}

	inline static int32_t get_offset_of_damageAmount_13() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___damageAmount_13)); }
	inline float get_damageAmount_13() const { return ___damageAmount_13; }
	inline float* get_address_of_damageAmount_13() { return &___damageAmount_13; }
	inline void set_damageAmount_13(float value)
	{
		___damageAmount_13 = value;
	}

	inline static int32_t get_offset_of_currentPlayer_14() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___currentPlayer_14)); }
	inline GameObject_t1113636619 * get_currentPlayer_14() const { return ___currentPlayer_14; }
	inline GameObject_t1113636619 ** get_address_of_currentPlayer_14() { return &___currentPlayer_14; }
	inline void set_currentPlayer_14(GameObject_t1113636619 * value)
	{
		___currentPlayer_14 = value;
		Il2CppCodeGenWriteBarrier((&___currentPlayer_14), value);
	}

	inline static int32_t get_offset_of_currentTarget_15() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___currentTarget_15)); }
	inline GameObject_t1113636619 * get_currentTarget_15() const { return ___currentTarget_15; }
	inline GameObject_t1113636619 ** get_address_of_currentTarget_15() { return &___currentTarget_15; }
	inline void set_currentTarget_15(GameObject_t1113636619 * value)
	{
		___currentTarget_15 = value;
		Il2CppCodeGenWriteBarrier((&___currentTarget_15), value);
	}

	inline static int32_t get_offset_of_timer_16() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___timer_16)); }
	inline float get_timer_16() const { return ___timer_16; }
	inline float* get_address_of_timer_16() { return &___timer_16; }
	inline void set_timer_16(float value)
	{
		___timer_16 = value;
	}

	inline static int32_t get_offset_of_timerRate_17() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___timerRate_17)); }
	inline float get_timerRate_17() const { return ___timerRate_17; }
	inline float* get_address_of_timerRate_17() { return &___timerRate_17; }
	inline void set_timerRate_17(float value)
	{
		___timerRate_17 = value;
	}

	inline static int32_t get_offset_of_nextAttack_18() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___nextAttack_18)); }
	inline float get_nextAttack_18() const { return ___nextAttack_18; }
	inline float* get_address_of_nextAttack_18() { return &___nextAttack_18; }
	inline void set_nextAttack_18(float value)
	{
		___nextAttack_18 = value;
	}

	inline static int32_t get_offset_of_playerDistance_19() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___playerDistance_19)); }
	inline float get_playerDistance_19() const { return ___playerDistance_19; }
	inline float* get_address_of_playerDistance_19() { return &___playerDistance_19; }
	inline void set_playerDistance_19(float value)
	{
		___playerDistance_19 = value;
	}

	inline static int32_t get_offset_of_anim_20() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___anim_20)); }
	inline Animator_t434523843 * get_anim_20() const { return ___anim_20; }
	inline Animator_t434523843 ** get_address_of_anim_20() { return &___anim_20; }
	inline void set_anim_20(Animator_t434523843 * value)
	{
		___anim_20 = value;
		Il2CppCodeGenWriteBarrier((&___anim_20), value);
	}

	inline static int32_t get_offset_of_playerInRange_21() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___playerInRange_21)); }
	inline bool get_playerInRange_21() const { return ___playerInRange_21; }
	inline bool* get_address_of_playerInRange_21() { return &___playerInRange_21; }
	inline void set_playerInRange_21(bool value)
	{
		___playerInRange_21 = value;
	}

	inline static int32_t get_offset_of_AS_22() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___AS_22)); }
	inline AudioSource_t3935305588 * get_AS_22() const { return ___AS_22; }
	inline AudioSource_t3935305588 ** get_address_of_AS_22() { return &___AS_22; }
	inline void set_AS_22(AudioSource_t3935305588 * value)
	{
		___AS_22 = value;
		Il2CppCodeGenWriteBarrier((&___AS_22), value);
	}

	inline static int32_t get_offset_of_index_23() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___index_23)); }
	inline int32_t get_index_23() const { return ___index_23; }
	inline int32_t* get_address_of_index_23() { return &___index_23; }
	inline void set_index_23(int32_t value)
	{
		___index_23 = value;
	}

	inline static int32_t get_offset_of_playerHurtClips_24() { return static_cast<int32_t>(offsetof(WizardController_t2370936914, ___playerHurtClips_24)); }
	inline AudioClipU5BU5D_t143221404* get_playerHurtClips_24() const { return ___playerHurtClips_24; }
	inline AudioClipU5BU5D_t143221404** get_address_of_playerHurtClips_24() { return &___playerHurtClips_24; }
	inline void set_playerHurtClips_24(AudioClipU5BU5D_t143221404* value)
	{
		___playerHurtClips_24 = value;
		Il2CppCodeGenWriteBarrier((&___playerHurtClips_24), value);
	}
};

struct WizardController_t2370936914_StaticFields
{
public:
	// System.Boolean WizardController::isPlayerAlive
	bool ___isPlayerAlive_10;

public:
	inline static int32_t get_offset_of_isPlayerAlive_10() { return static_cast<int32_t>(offsetof(WizardController_t2370936914_StaticFields, ___isPlayerAlive_10)); }
	inline bool get_isPlayerAlive_10() const { return ___isPlayerAlive_10; }
	inline bool* get_address_of_isPlayerAlive_10() { return &___isPlayerAlive_10; }
	inline void set_isPlayerAlive_10(bool value)
	{
		___isPlayerAlive_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIZARDCONTROLLER_T2370936914_H
#ifndef ROTATOR_T2029753424_H
#define ROTATOR_T2029753424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// rotator
struct  rotator_t2029753424  : public MonoBehaviour_t3962482529
{
public:
	// System.Single rotator::speed
	float ___speed_4;
	// UnityEngine.Vector3 rotator::direction
	Vector3_t3722313464  ___direction_5;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(rotator_t2029753424, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_direction_5() { return static_cast<int32_t>(offsetof(rotator_t2029753424, ___direction_5)); }
	inline Vector3_t3722313464  get_direction_5() const { return ___direction_5; }
	inline Vector3_t3722313464 * get_address_of_direction_5() { return &___direction_5; }
	inline void set_direction_5(Vector3_t3722313464  value)
	{
		___direction_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATOR_T2029753424_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600 = { sizeof (MainMenuBehavior_t2264954362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4600[2] = 
{
	MainMenuBehavior_t2264954362::get_offset_of_Scene1P_4(),
	MainMenuBehavior_t2264954362::get_offset_of_Scene2P_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601 = { sizeof (PlayerHealth_t2068385516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4601[5] = 
{
	PlayerHealth_t2068385516::get_offset_of_player_4(),
	PlayerHealth_t2068385516::get_offset_of_ragdoll_5(),
	PlayerHealth_t2068385516::get_offset_of_currentHealth_6(),
	PlayerHealth_t2068385516::get_offset_of_healthBar_7(),
	PlayerHealth_t2068385516::get_offset_of_maxHealth_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602 = { sizeof (SimpleHealthBar_t721070758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4602[10] = 
{
	SimpleHealthBar_t721070758::get_offset_of_barImage_4(),
	SimpleHealthBar_t721070758::get_offset_of_colorMode_5(),
	SimpleHealthBar_t721070758::get_offset_of_barColor_6(),
	SimpleHealthBar_t721070758::get_offset_of_barGradient_7(),
	SimpleHealthBar_t721070758::get_offset_of_displayText_8(),
	SimpleHealthBar_t721070758::get_offset_of_barText_9(),
	SimpleHealthBar_t721070758::get_offset_of_additionalText_10(),
	SimpleHealthBar_t721070758::get_offset_of__currentFraction_11(),
	SimpleHealthBar_t721070758::get_offset_of__maxValue_12(),
	SimpleHealthBar_t721070758::get_offset_of_targetFill_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603 = { sizeof (ColorMode_t635806747)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4603[3] = 
{
	ColorMode_t635806747::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604 = { sizeof (DisplayText_t3312458044)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4604[5] = 
{
	DisplayText_t3312458044::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605 = { sizeof (rotator_t2029753424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4605[2] = 
{
	rotator_t2029753424::get_offset_of_speed_4(),
	rotator_t2029753424::get_offset_of_direction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606 = { sizeof (ArcherBehavior_t1637475703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4606[5] = 
{
	ArcherBehavior_t1637475703::get_offset_of_arrowSpawnPoint_4(),
	ArcherBehavior_t1637475703::get_offset_of_arrow_5(),
	ArcherBehavior_t1637475703::get_offset_of_clips_6(),
	ArcherBehavior_t1637475703::get_offset_of_AS_7(),
	ArcherBehavior_t1637475703::get_offset_of_anim_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607 = { sizeof (ArrowBehavior_t1475592159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4607[8] = 
{
	ArrowBehavior_t1475592159::get_offset_of_speed_4(),
	ArrowBehavior_t1475592159::get_offset_of_damageAmount_5(),
	ArrowBehavior_t1475592159::get_offset_of_clips_6(),
	ArrowBehavior_t1475592159::get_offset_of_P1H_7(),
	ArrowBehavior_t1475592159::get_offset_of_P2H_8(),
	ArrowBehavior_t1475592159::get_offset_of_P1_9(),
	ArrowBehavior_t1475592159::get_offset_of_P2_10(),
	ArrowBehavior_t1475592159::get_offset_of_AS_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608 = { sizeof (PlayerControlP1_t3101175660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4608[11] = 
{
	PlayerControlP1_t3101175660::get_offset_of_animator_4(),
	PlayerControlP1_t3101175660::get_offset_of_rotationSpeed_5(),
	PlayerControlP1_t3101175660::get_offset_of_inputVec_6(),
	PlayerControlP1_t3101175660::get_offset_of_targetDirection_7(),
	PlayerControlP1_t3101175660::get_offset_of_cameraTransform_8(),
	PlayerControlP1_t3101175660::get_offset_of_camBool1_9(),
	PlayerControlP1_t3101175660::get_offset_of_camBool2_10(),
	PlayerControlP1_t3101175660::get_offset_of_cam1_11(),
	PlayerControlP1_t3101175660::get_offset_of_cam2_12(),
	PlayerControlP1_t3101175660::get_offset_of_warrior_13(),
	PlayerControlP1_t3101175660::get_offset_of_gravity_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609 = { sizeof (Warrior_t2382298878)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4609[13] = 
{
	Warrior_t2382298878::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610 = { sizeof (U3CCOStunPauseU3Ed__14_t3804073412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4610[3] = 
{
	U3CCOStunPauseU3Ed__14_t3804073412::get_offset_of_U3CU3E1__state_0(),
	U3CCOStunPauseU3Ed__14_t3804073412::get_offset_of_U3CU3E2__current_1(),
	U3CCOStunPauseU3Ed__14_t3804073412::get_offset_of_pauseTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611 = { sizeof (CasaControl_t3217110703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4611[4] = 
{
	CasaControl_t3217110703::get_offset_of_P1_4(),
	CasaControl_t3217110703::get_offset_of_P2_5(),
	CasaControl_t3217110703::get_offset_of_P1P2_6(),
	CasaControl_t3217110703::get_offset_of_P2P2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612 = { sizeof (ForgeInteraction_t3089908856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4612[4] = 
{
	ForgeInteraction_t3089908856::get_offset_of_SR_4(),
	ForgeInteraction_t3089908856::get_offset_of_VI_5(),
	ForgeInteraction_t3089908856::get_offset_of_AS_6(),
	ForgeInteraction_t3089908856::get_offset_of_particles_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613 = { sizeof (LeaveCasaControl_t1102745401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4613[4] = 
{
	LeaveCasaControl_t1102745401::get_offset_of_P1_4(),
	LeaveCasaControl_t1102745401::get_offset_of_P2_5(),
	LeaveCasaControl_t1102745401::get_offset_of_P1P1_6(),
	LeaveCasaControl_t1102745401::get_offset_of_P2P1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614 = { sizeof (VillagerInteraction_t490587617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4614[8] = 
{
	VillagerInteraction_t490587617::get_offset_of_isTalking_4(),
	VillagerInteraction_t490587617::get_offset_of_AS_5(),
	VillagerInteraction_t490587617::get_offset_of_UI_6(),
	VillagerInteraction_t490587617::get_offset_of_p1Active_7(),
	VillagerInteraction_t490587617::get_offset_of_p2Active_8(),
	VillagerInteraction_t490587617::get_offset_of_talkSound_9(),
	VillagerInteraction_t490587617::get_offset_of_BC_10(),
	VillagerInteraction_t490587617::get_offset_of_onePlayer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615 = { sizeof (U3CTalkHandlerU3Ed__11_t2840649544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4615[3] = 
{
	U3CTalkHandlerU3Ed__11_t2840649544::get_offset_of_U3CU3E1__state_0(),
	U3CTalkHandlerU3Ed__11_t2840649544::get_offset_of_U3CU3E2__current_1(),
	U3CTalkHandlerU3Ed__11_t2840649544::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616 = { sizeof (WizardController_t2370936914), -1, sizeof(WizardController_t2370936914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4616[21] = 
{
	WizardController_t2370936914::get_offset_of_U3CagentU3Ek__BackingField_4(),
	WizardController_t2370936914::get_offset_of_U3CcharacterU3Ek__BackingField_5(),
	WizardController_t2370936914::get_offset_of_enemyWalkSpeed_6(),
	WizardController_t2370936914::get_offset_of_enemyRunSpeed_7(),
	WizardController_t2370936914::get_offset_of_players_8(),
	WizardController_t2370936914::get_offset_of_targets_9(),
	WizardController_t2370936914_StaticFields::get_offset_of_isPlayerAlive_10(),
	WizardController_t2370936914::get_offset_of_attackRate_11(),
	WizardController_t2370936914::get_offset_of_soundDelay_12(),
	WizardController_t2370936914::get_offset_of_damageAmount_13(),
	WizardController_t2370936914::get_offset_of_currentPlayer_14(),
	WizardController_t2370936914::get_offset_of_currentTarget_15(),
	WizardController_t2370936914::get_offset_of_timer_16(),
	WizardController_t2370936914::get_offset_of_timerRate_17(),
	WizardController_t2370936914::get_offset_of_nextAttack_18(),
	WizardController_t2370936914::get_offset_of_playerDistance_19(),
	WizardController_t2370936914::get_offset_of_anim_20(),
	WizardController_t2370936914::get_offset_of_playerInRange_21(),
	WizardController_t2370936914::get_offset_of_AS_22(),
	WizardController_t2370936914::get_offset_of_index_23(),
	WizardController_t2370936914::get_offset_of_playerHurtClips_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617 = { sizeof (U3CSoundDelayU3Ed__36_t1519778301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4617[3] = 
{
	U3CSoundDelayU3Ed__36_t1519778301::get_offset_of_U3CU3E1__state_0(),
	U3CSoundDelayU3Ed__36_t1519778301::get_offset_of_U3CU3E2__current_1(),
	U3CSoundDelayU3Ed__36_t1519778301::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618 = { sizeof (MainMenuControl_t4231413698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4618[2] = 
{
	MainMenuControl_t4231413698::get_offset_of_scene1_4(),
	MainMenuControl_t4231413698::get_offset_of_scene2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619 = { sizeof (NextLevel_t1903424389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4619[2] = 
{
	NextLevel_t1903424389::get_offset_of_SR_4(),
	NextLevel_t1903424389::get_offset_of_sceneToLoad_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620 = { sizeof (PauseMenu1_t3917150987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4620[7] = 
{
	PauseMenu1_t3917150987::get_offset_of_m_TimeScaleRef_4(),
	PauseMenu1_t3917150987::get_offset_of_m_VolumeRef_5(),
	PauseMenu1_t3917150987::get_offset_of_m_Paused_6(),
	PauseMenu1_t3917150987::get_offset_of_image_7(),
	PauseMenu1_t3917150987::get_offset_of_B_8(),
	PauseMenu1_t3917150987::get_offset_of_pauseText_9(),
	PauseMenu1_t3917150987::get_offset_of_exitText_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621 = { sizeof (EnemyAttackSinglePlayer_t4127103160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4621[10] = 
{
	EnemyAttackSinglePlayer_t4127103160::get_offset_of_nextAttack_4(),
	EnemyAttackSinglePlayer_t4127103160::get_offset_of_anim_5(),
	EnemyAttackSinglePlayer_t4127103160::get_offset_of_AS_6(),
	EnemyAttackSinglePlayer_t4127103160::get_offset_of_playerHurtClips_7(),
	EnemyAttackSinglePlayer_t4127103160::get_offset_of_P1H_8(),
	EnemyAttackSinglePlayer_t4127103160::get_offset_of_P1InRange_9(),
	EnemyAttackSinglePlayer_t4127103160::get_offset_of_MS_10(),
	EnemyAttackSinglePlayer_t4127103160::get_offset_of_attackRate_11(),
	EnemyAttackSinglePlayer_t4127103160::get_offset_of_soundDelay_12(),
	EnemyAttackSinglePlayer_t4127103160::get_offset_of_damageAmount_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622 = { sizeof (U3CSoundDelayU3Ed__15_t3259149061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4622[3] = 
{
	U3CSoundDelayU3Ed__15_t3259149061::get_offset_of_U3CU3E1__state_0(),
	U3CSoundDelayU3Ed__15_t3259149061::get_offset_of_U3CU3E2__current_1(),
	U3CSoundDelayU3Ed__15_t3259149061::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623 = { sizeof (CamControl_t437195931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4623[2] = 
{
	CamControl_t437195931::get_offset_of_player_4(),
	CamControl_t437195931::get_offset_of_offset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624 = { sizeof (CamRotate_t2888940566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4624[2] = 
{
	CamRotate_t2888940566::get_offset_of_speed_4(),
	CamRotate_t2888940566::get_offset_of_target_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625 = { sizeof (CamSwitcher_t2345021740), -1, sizeof(CamSwitcher_t2345021740_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4625[14] = 
{
	CamSwitcher_t2345021740::get_offset_of_player1_4(),
	CamSwitcher_t2345021740::get_offset_of_player2_5(),
	CamSwitcher_t2345021740::get_offset_of_cam1_6(),
	CamSwitcher_t2345021740::get_offset_of_cam2_7(),
	CamSwitcher_t2345021740::get_offset_of_p1_8(),
	CamSwitcher_t2345021740::get_offset_of_p2_9(),
	CamSwitcher_t2345021740::get_offset_of_WADF_10(),
	CamSwitcher_t2345021740::get_offset_of_WP2_11(),
	CamSwitcher_t2345021740::get_offset_of_P1R1_12(),
	CamSwitcher_t2345021740::get_offset_of_P1R2_13(),
	CamSwitcher_t2345021740::get_offset_of_P2R1_14(),
	CamSwitcher_t2345021740::get_offset_of_P2R2_15(),
	CamSwitcher_t2345021740_StaticFields::get_offset_of_P1InRoom1_16(),
	CamSwitcher_t2345021740_StaticFields::get_offset_of_P2InRoom1_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626 = { sizeof (CamSwitcher2_t1376530732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4626[14] = 
{
	CamSwitcher2_t1376530732::get_offset_of_player1_4(),
	CamSwitcher2_t1376530732::get_offset_of_player2_5(),
	CamSwitcher2_t1376530732::get_offset_of_cam1_6(),
	CamSwitcher2_t1376530732::get_offset_of_cam2_7(),
	CamSwitcher2_t1376530732::get_offset_of_p1_8(),
	CamSwitcher2_t1376530732::get_offset_of_p2_9(),
	CamSwitcher2_t1376530732::get_offset_of_WADF_10(),
	CamSwitcher2_t1376530732::get_offset_of_WP2_11(),
	CamSwitcher2_t1376530732::get_offset_of_P1R1_12(),
	CamSwitcher2_t1376530732::get_offset_of_P1R2_13(),
	CamSwitcher2_t1376530732::get_offset_of_P2R1_14(),
	CamSwitcher2_t1376530732::get_offset_of_P2R2_15(),
	CamSwitcher2_t1376530732::get_offset_of_P1InRoom1_16(),
	CamSwitcher2_t1376530732::get_offset_of_P2InRoom1_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627 = { sizeof (DestroySelf_t3777301674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628 = { sizeof (EnemyAttack_t24081459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4628[12] = 
{
	EnemyAttack_t24081459::get_offset_of_nextAttack_4(),
	EnemyAttack_t24081459::get_offset_of_anim_5(),
	EnemyAttack_t24081459::get_offset_of_AS_6(),
	EnemyAttack_t24081459::get_offset_of_playerHurtClips_7(),
	EnemyAttack_t24081459::get_offset_of_P1H_8(),
	EnemyAttack_t24081459::get_offset_of_P2H_9(),
	EnemyAttack_t24081459::get_offset_of_P1InRange_10(),
	EnemyAttack_t24081459::get_offset_of_P2InRange_11(),
	EnemyAttack_t24081459::get_offset_of_MS_12(),
	EnemyAttack_t24081459::get_offset_of_attackRate_13(),
	EnemyAttack_t24081459::get_offset_of_soundDelay_14(),
	EnemyAttack_t24081459::get_offset_of_damageAmount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629 = { sizeof (U3CSoundDelayU3Ed__18_t748685287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4629[3] = 
{
	U3CSoundDelayU3Ed__18_t748685287::get_offset_of_U3CU3E1__state_0(),
	U3CSoundDelayU3Ed__18_t748685287::get_offset_of_U3CU3E2__current_1(),
	U3CSoundDelayU3Ed__18_t748685287::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630 = { sizeof (EnemyNavigation_t1695490929), -1, sizeof(EnemyNavigation_t1695490929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4630[21] = 
{
	EnemyNavigation_t1695490929::get_offset_of_U3CagentU3Ek__BackingField_4(),
	EnemyNavigation_t1695490929::get_offset_of_U3CcharacterU3Ek__BackingField_5(),
	EnemyNavigation_t1695490929::get_offset_of_enemyWalkSpeed_6(),
	EnemyNavigation_t1695490929::get_offset_of_enemyRunSpeed_7(),
	EnemyNavigation_t1695490929::get_offset_of_players_8(),
	EnemyNavigation_t1695490929::get_offset_of_targets_9(),
	EnemyNavigation_t1695490929_StaticFields::get_offset_of_isPlayerAlive_10(),
	EnemyNavigation_t1695490929::get_offset_of_attackRate_11(),
	EnemyNavigation_t1695490929::get_offset_of_soundDelay_12(),
	EnemyNavigation_t1695490929::get_offset_of_damageAmount_13(),
	EnemyNavigation_t1695490929::get_offset_of_currentPlayer_14(),
	EnemyNavigation_t1695490929::get_offset_of_currentTarget_15(),
	EnemyNavigation_t1695490929::get_offset_of_timer_16(),
	EnemyNavigation_t1695490929::get_offset_of_timerRate_17(),
	EnemyNavigation_t1695490929::get_offset_of_nextAttack_18(),
	EnemyNavigation_t1695490929::get_offset_of_playerDistance_19(),
	EnemyNavigation_t1695490929::get_offset_of_anim_20(),
	EnemyNavigation_t1695490929::get_offset_of_playerInRange_21(),
	EnemyNavigation_t1695490929::get_offset_of_AS_22(),
	EnemyNavigation_t1695490929::get_offset_of_index_23(),
	EnemyNavigation_t1695490929::get_offset_of_playerHurtClips_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631 = { sizeof (U3CSoundDelayU3Ed__36_t1499302528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4631[3] = 
{
	U3CSoundDelayU3Ed__36_t1499302528::get_offset_of_U3CU3E1__state_0(),
	U3CSoundDelayU3Ed__36_t1499302528::get_offset_of_U3CU3E2__current_1(),
	U3CSoundDelayU3Ed__36_t1499302528::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632 = { sizeof (ExitDoor_t1886296669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4632[3] = 
{
	ExitDoor_t1886296669::get_offset_of_canOpen_4(),
	ExitDoor_t1886296669::get_offset_of_triggering_5(),
	ExitDoor_t1886296669::get_offset_of_level_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633 = { sizeof (HealthPotionInteraction_t3696356582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4633[2] = 
{
	HealthPotionInteraction_t3696356582::get_offset_of_P1H_4(),
	HealthPotionInteraction_t3696356582::get_offset_of_P2H_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634 = { sizeof (KeyInteraction_t1379241489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4634[3] = 
{
	KeyInteraction_t1379241489::get_offset_of_gotKey_4(),
	KeyInteraction_t1379241489::get_offset_of_keyImage_5(),
	KeyInteraction_t1379241489::get_offset_of_ED_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635 = { sizeof (ManageEnemy_t3567251697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4635[8] = 
{
	ManageEnemy_t3567251697::get_offset_of_spawnEnemies_4(),
	ManageEnemy_t3567251697::get_offset_of_enemyCount_5(),
	ManageEnemy_t3567251697::get_offset_of_AS_6(),
	ManageEnemy_t3567251697::get_offset_of_AC_7(),
	ManageEnemy_t3567251697::get_offset_of_spawnPoint1_8(),
	ManageEnemy_t3567251697::get_offset_of_spawnPoint2_9(),
	ManageEnemy_t3567251697::get_offset_of_EnemyToSpawn_10(),
	ManageEnemy_t3567251697::get_offset_of_smoke_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636 = { sizeof (ManageScene_t3196455047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4636[8] = 
{
	ManageScene_t3196455047::get_offset_of_P1_4(),
	ManageScene_t3196455047::get_offset_of_P2_5(),
	ManageScene_t3196455047::get_offset_of_P1H_6(),
	ManageScene_t3196455047::get_offset_of_P2H_7(),
	ManageScene_t3196455047::get_offset_of_P1Dead_8(),
	ManageScene_t3196455047::get_offset_of_P2Dead_9(),
	ManageScene_t3196455047::get_offset_of_singlePlayer_10(),
	ManageScene_t3196455047::get_offset_of_whichSceneToLoad_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637 = { sizeof (U3CDelayRestartP1U3Ed__10_t1872549700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4637[3] = 
{
	U3CDelayRestartP1U3Ed__10_t1872549700::get_offset_of_U3CU3E1__state_0(),
	U3CDelayRestartP1U3Ed__10_t1872549700::get_offset_of_U3CU3E2__current_1(),
	U3CDelayRestartP1U3Ed__10_t1872549700::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638 = { sizeof (U3CDelayRestartP2U3Ed__11_t568587784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4638[3] = 
{
	U3CDelayRestartP2U3Ed__11_t568587784::get_offset_of_U3CU3E1__state_0(),
	U3CDelayRestartP2U3Ed__11_t568587784::get_offset_of_U3CU3E2__current_1(),
	U3CDelayRestartP2U3Ed__11_t568587784::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639 = { sizeof (P1Attack_t893104030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4639[5] = 
{
	P1Attack_t893104030::get_offset_of_nextSwing_4(),
	P1Attack_t893104030::get_offset_of_enemyInRange_5(),
	P1Attack_t893104030::get_offset_of_EH_6(),
	P1Attack_t893104030::get_offset_of_swingRate_7(),
	P1Attack_t893104030::get_offset_of_playerDamage_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640 = { sizeof (P2Attack_t895266718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4640[5] = 
{
	P2Attack_t895266718::get_offset_of_nextSwing_4(),
	P2Attack_t895266718::get_offset_of_enemyInRange_5(),
	P2Attack_t895266718::get_offset_of_EH_6(),
	P2Attack_t895266718::get_offset_of_swingRate_7(),
	P2Attack_t895266718::get_offset_of_playerDamage_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641 = { sizeof (PlayerInteraction_t264875739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4641[7] = 
{
	PlayerInteraction_t264875739::get_offset_of_fireplace1_4(),
	PlayerInteraction_t264875739::get_offset_of_triggeringP1_5(),
	PlayerInteraction_t264875739::get_offset_of_triggeringP2_6(),
	PlayerInteraction_t264875739::get_offset_of_fire1On_7(),
	PlayerInteraction_t264875739::get_offset_of_bothOn_8(),
	PlayerInteraction_t264875739::get_offset_of_ME_9(),
	PlayerInteraction_t264875739::get_offset_of_fire1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642 = { sizeof (WarriorAnimationDemoFREE_t1507088667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4642[11] = 
{
	WarriorAnimationDemoFREE_t1507088667::get_offset_of_animator_4(),
	WarriorAnimationDemoFREE_t1507088667::get_offset_of_rotationSpeed_5(),
	WarriorAnimationDemoFREE_t1507088667::get_offset_of_inputVec_6(),
	WarriorAnimationDemoFREE_t1507088667::get_offset_of_targetDirection_7(),
	WarriorAnimationDemoFREE_t1507088667::get_offset_of_cameraTransform_8(),
	WarriorAnimationDemoFREE_t1507088667::get_offset_of_camBool1_9(),
	WarriorAnimationDemoFREE_t1507088667::get_offset_of_camBool2_10(),
	WarriorAnimationDemoFREE_t1507088667::get_offset_of_cam1_11(),
	WarriorAnimationDemoFREE_t1507088667::get_offset_of_cam2_12(),
	WarriorAnimationDemoFREE_t1507088667::get_offset_of_warrior_13(),
	WarriorAnimationDemoFREE_t1507088667::get_offset_of_gravity_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643 = { sizeof (Warrior_t3094985692)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4643[13] = 
{
	Warrior_t3094985692::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644 = { sizeof (U3CCOStunPauseU3Ed__14_t3614912138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4644[3] = 
{
	U3CCOStunPauseU3Ed__14_t3614912138::get_offset_of_U3CU3E1__state_0(),
	U3CCOStunPauseU3Ed__14_t3614912138::get_offset_of_U3CU3E2__current_1(),
	U3CCOStunPauseU3Ed__14_t3614912138::get_offset_of_pauseTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645 = { sizeof (WarriorP2_t1586698613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4645[11] = 
{
	WarriorP2_t1586698613::get_offset_of_animator_4(),
	WarriorP2_t1586698613::get_offset_of_rotationSpeed_5(),
	WarriorP2_t1586698613::get_offset_of_inputVec_6(),
	WarriorP2_t1586698613::get_offset_of_targetDirection_7(),
	WarriorP2_t1586698613::get_offset_of_camBool1_8(),
	WarriorP2_t1586698613::get_offset_of_camBool2_9(),
	WarriorP2_t1586698613::get_offset_of_cam1_10(),
	WarriorP2_t1586698613::get_offset_of_cam2_11(),
	WarriorP2_t1586698613::get_offset_of_cameraTransform_12(),
	WarriorP2_t1586698613::get_offset_of_warrior_13(),
	WarriorP2_t1586698613::get_offset_of_gravity_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646 = { sizeof (Warrior_t1316392392)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4646[13] = 
{
	Warrior_t1316392392::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647 = { sizeof (U3CCOStunPauseU3Ed__14_t2433058018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4647[3] = 
{
	U3CCOStunPauseU3Ed__14_t2433058018::get_offset_of_U3CU3E1__state_0(),
	U3CCOStunPauseU3Ed__14_t2433058018::get_offset_of_U3CU3E2__current_1(),
	U3CCOStunPauseU3Ed__14_t2433058018::get_offset_of_pauseTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648 = { sizeof (DisableRenderer_t123364433), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649 = { sizeof (CharController_Motor_t3258469724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4649[11] = 
{
	CharController_Motor_t3258469724::get_offset_of_speed_4(),
	CharController_Motor_t3258469724::get_offset_of_sensitivity_5(),
	CharController_Motor_t3258469724::get_offset_of_WaterHeight_6(),
	CharController_Motor_t3258469724::get_offset_of_character_7(),
	CharController_Motor_t3258469724::get_offset_of_cam_8(),
	CharController_Motor_t3258469724::get_offset_of_moveFB_9(),
	CharController_Motor_t3258469724::get_offset_of_moveLR_10(),
	CharController_Motor_t3258469724::get_offset_of_rotX_11(),
	CharController_Motor_t3258469724::get_offset_of_rotY_12(),
	CharController_Motor_t3258469724::get_offset_of_webGLRightClickRotation_13(),
	CharController_Motor_t3258469724::get_offset_of_gravity_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650 = { sizeof (FPSDisplay_t1066203289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4650[1] = 
{
	FPSDisplay_t1066203289::get_offset_of_deltaTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651 = { sizeof (WaterFloat_t311037891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4651[1] = 
{
	WaterFloat_t311037891::get_offset_of_WaterHeight_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652 = { sizeof (SlasherNAV_t209608926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4652[2] = 
{
	SlasherNAV_t209608926::get_offset_of_U3CNMAU3Ek__BackingField_4(),
	SlasherNAV_t209608926::get_offset_of_target_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653 = { sizeof (AsteroidController_t3828879615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4653[6] = 
{
	AsteroidController_t3828879615::get_offset_of_myRigidbody_4(),
	AsteroidController_t3828879615::get_offset_of_canDestroy_5(),
	AsteroidController_t3828879615::get_offset_of_isDestroyed_6(),
	AsteroidController_t3828879615::get_offset_of_isDebris_7(),
	AsteroidController_t3828879615::get_offset_of_health_8(),
	AsteroidController_t3828879615::get_offset_of_maxHealth_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654 = { sizeof (U3CDelayInitialDestructionU3Ed__7_t3782258133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4654[4] = 
{
	U3CDelayInitialDestructionU3Ed__7_t3782258133::get_offset_of_U3CU3E1__state_0(),
	U3CDelayInitialDestructionU3Ed__7_t3782258133::get_offset_of_U3CU3E2__current_1(),
	U3CDelayInitialDestructionU3Ed__7_t3782258133::get_offset_of_delayTime_2(),
	U3CDelayInitialDestructionU3Ed__7_t3782258133::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655 = { sizeof (GameManager_t3924726104), -1, sizeof(GameManager_t3924726104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4655[21] = 
{
	GameManager_t3924726104_StaticFields::get_offset_of_instance_4(),
	GameManager_t3924726104::get_offset_of_astroidPrefab_5(),
	GameManager_t3924726104::get_offset_of_debrisPrefab_6(),
	GameManager_t3924726104::get_offset_of_explosionPrefab_7(),
	GameManager_t3924726104::get_offset_of_healthPickupPrefab_8(),
	GameManager_t3924726104::get_offset_of_spawning_9(),
	GameManager_t3924726104::get_offset_of_spawnTimeMin_10(),
	GameManager_t3924726104::get_offset_of_spawnTimeMax_11(),
	GameManager_t3924726104::get_offset_of_startingAsteroids_12(),
	GameManager_t3924726104::get_offset_of_healthSpawnTimeMin_13(),
	GameManager_t3924726104::get_offset_of_healthSpawnTimeMax_14(),
	GameManager_t3924726104::get_offset_of_scoreText_15(),
	GameManager_t3924726104::get_offset_of_finalScoreText_16(),
	GameManager_t3924726104::get_offset_of_score_17(),
	GameManager_t3924726104::get_offset_of_asteroidPoints_18(),
	GameManager_t3924726104::get_offset_of_debrisPoints_19(),
	GameManager_t3924726104::get_offset_of_gameOverScreen_20(),
	GameManager_t3924726104::get_offset_of_gameOverText_21(),
	GameManager_t3924726104::get_offset_of_asteroidHealth_22(),
	GameManager_t3924726104::get_offset_of_debrisHealth_23(),
	GameManager_t3924726104::get_offset_of_hasLost_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656 = { sizeof (U3CSpawnHealthTimerU3Ed__25_t234955219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4656[3] = 
{
	U3CSpawnHealthTimerU3Ed__25_t234955219::get_offset_of_U3CU3E1__state_0(),
	U3CSpawnHealthTimerU3Ed__25_t234955219::get_offset_of_U3CU3E2__current_1(),
	U3CSpawnHealthTimerU3Ed__25_t234955219::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657 = { sizeof (U3CSpawnTimerU3Ed__27_t2700198560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4657[3] = 
{
	U3CSpawnTimerU3Ed__27_t2700198560::get_offset_of_U3CU3E1__state_0(),
	U3CSpawnTimerU3Ed__27_t2700198560::get_offset_of_U3CU3E2__current_1(),
	U3CSpawnTimerU3Ed__27_t2700198560::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658 = { sizeof (U3CFadeDeathScreenU3Ed__32_t3488881226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4658[7] = 
{
	U3CFadeDeathScreenU3Ed__32_t3488881226::get_offset_of_U3CU3E1__state_0(),
	U3CFadeDeathScreenU3Ed__32_t3488881226::get_offset_of_U3CU3E2__current_1(),
	U3CFadeDeathScreenU3Ed__32_t3488881226::get_offset_of_U3CU3E4__this_2(),
	U3CFadeDeathScreenU3Ed__32_t3488881226::get_offset_of_U3CimageColorU3E5__2_3(),
	U3CFadeDeathScreenU3Ed__32_t3488881226::get_offset_of_U3CtextColorU3E5__3_4(),
	U3CFadeDeathScreenU3Ed__32_t3488881226::get_offset_of_U3CfinalScoreTextColorU3E5__4_5(),
	U3CFadeDeathScreenU3Ed__32_t3488881226::get_offset_of_U3CtU3E5__5_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659 = { sizeof (HealthPickupController_t1709822218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4659[5] = 
{
	HealthPickupController_t1709822218::get_offset_of_myRigidbody_4(),
	HealthPickupController_t1709822218::get_offset_of_particles_5(),
	HealthPickupController_t1709822218::get_offset_of_mySprite_6(),
	HealthPickupController_t1709822218::get_offset_of_canDestroy_7(),
	HealthPickupController_t1709822218::get_offset_of_canPickup_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660 = { sizeof (U3CDelayInitialDestructionU3Ed__6_t2327916900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4660[4] = 
{
	U3CDelayInitialDestructionU3Ed__6_t2327916900::get_offset_of_U3CU3E1__state_0(),
	U3CDelayInitialDestructionU3Ed__6_t2327916900::get_offset_of_U3CU3E2__current_1(),
	U3CDelayInitialDestructionU3Ed__6_t2327916900::get_offset_of_delayTime_2(),
	U3CDelayInitialDestructionU3Ed__6_t2327916900::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661 = { sizeof (PlayerController_t2576101795), -1, sizeof(PlayerController_t2576101795_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4661[19] = 
{
	PlayerController_t2576101795_StaticFields::get_offset_of_instance_4(),
	PlayerController_t2576101795::get_offset_of_rotationSpeed_5(),
	PlayerController_t2576101795::get_offset_of_accelerationSpeed_6(),
	PlayerController_t2576101795::get_offset_of_maxSpeed_7(),
	PlayerController_t2576101795::get_offset_of_shootingCooldown_8(),
	PlayerController_t2576101795::get_offset_of_bulletPrefab_9(),
	PlayerController_t2576101795::get_offset_of_myRigidbody_10(),
	PlayerController_t2576101795::get_offset_of_gunTrans_11(),
	PlayerController_t2576101795::get_offset_of_bulletSpawnPos_12(),
	PlayerController_t2576101795::get_offset_of_shootingTimer_13(),
	PlayerController_t2576101795::get_offset_of_canControl_14(),
	PlayerController_t2576101795::get_offset_of_rotation_15(),
	PlayerController_t2576101795::get_offset_of_acceleration_16(),
	PlayerController_t2576101795::get_offset_of_overheatVisual_17(),
	PlayerController_t2576101795::get_offset_of_overheatTimer_18(),
	PlayerController_t2576101795::get_offset_of_overheatTimerMax_19(),
	PlayerController_t2576101795::get_offset_of_cooldownSpeed_20(),
	PlayerController_t2576101795::get_offset_of_canShoot_21(),
	PlayerController_t2576101795::get_offset_of_gunHeatBar_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662 = { sizeof (U3CDelayOverheatU3Ed__29_t3308176875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4662[4] = 
{
	U3CDelayOverheatU3Ed__29_t3308176875::get_offset_of_U3CU3E1__state_0(),
	U3CDelayOverheatU3Ed__29_t3308176875::get_offset_of_U3CU3E2__current_1(),
	U3CDelayOverheatU3Ed__29_t3308176875::get_offset_of_U3CU3E4__this_2(),
	U3CDelayOverheatU3Ed__29_t3308176875::get_offset_of_U3CtU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663 = { sizeof (PlayerHealth_t992877501), -1, sizeof(PlayerHealth_t992877501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4663[12] = 
{
	PlayerHealth_t992877501_StaticFields::get_offset_of_instance_4(),
	PlayerHealth_t992877501::get_offset_of_canTakeDamage_5(),
	PlayerHealth_t992877501::get_offset_of_maxHealth_6(),
	PlayerHealth_t992877501::get_offset_of_currentHealth_7(),
	PlayerHealth_t992877501::get_offset_of_invulnerabilityTime_8(),
	PlayerHealth_t992877501::get_offset_of_currentShield_9(),
	PlayerHealth_t992877501::get_offset_of_maxShield_10(),
	PlayerHealth_t992877501::get_offset_of_regenShieldTimer_11(),
	PlayerHealth_t992877501::get_offset_of_regenShieldTimerMax_12(),
	PlayerHealth_t992877501::get_offset_of_explosionParticles_13(),
	PlayerHealth_t992877501::get_offset_of_healthBar_14(),
	PlayerHealth_t992877501::get_offset_of_shieldBar_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664 = { sizeof (U3CInvulnerabliltyU3Ed__20_t2384978279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4664[3] = 
{
	U3CInvulnerabliltyU3Ed__20_t2384978279::get_offset_of_U3CU3E1__state_0(),
	U3CInvulnerabliltyU3Ed__20_t2384978279::get_offset_of_U3CU3E2__current_1(),
	U3CInvulnerabliltyU3Ed__20_t2384978279::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665 = { sizeof (U3CShakeCameraU3Ed__21_t2693912377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4665[4] = 
{
	U3CShakeCameraU3Ed__21_t2693912377::get_offset_of_U3CU3E1__state_0(),
	U3CShakeCameraU3Ed__21_t2693912377::get_offset_of_U3CU3E2__current_1(),
	U3CShakeCameraU3Ed__21_t2693912377::get_offset_of_U3CorigPosU3E5__2_2(),
	U3CShakeCameraU3Ed__21_t2693912377::get_offset_of_U3CtU3E5__3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666 = { sizeof (PixelBubble_t3317741694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4666[6] = 
{
	PixelBubble_t3317741694::get_offset_of_vMessage_0(),
	PixelBubble_t3317741694::get_offset_of_vMessageForm_1(),
	PixelBubble_t3317741694::get_offset_of_vBorderColor_2(),
	PixelBubble_t3317741694::get_offset_of_vBodyColor_3(),
	PixelBubble_t3317741694::get_offset_of_vFontColor_4(),
	PixelBubble_t3317741694::get_offset_of_vClickToCloseBubble_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
