﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct List_1_t1232140387;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.ParticleCollisionEvent[]
struct ParticleCollisionEventU5BU5D_t4144522048;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t3069227754;
// UnityEngine.ParticleSystemRenderer
struct ParticleSystemRenderer_t2065813411;

extern RuntimeClass* AnimationCurve_t3046754366_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3454777273;
extern String_t* _stringLiteral4178700366;
extern const RuntimeMethod* ParticlePhysicsExtensions_GetCollisionEvents_m1108737549_RuntimeMethod_var;
extern const uint32_t MinMaxCurve_t1067599125_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t ParticlePhysicsExtensions_GetCollisionEvents_m1108737549_MetadataUsageId;
extern const uint32_t ParticleSystem_Emit_m497964751_MetadataUsageId;
extern const uint32_t Particle_set_angularVelocity3D_m3163963446_MetadataUsageId;
extern const uint32_t Particle_set_rotation3D_m2156157200_MetadataUsageId;
struct AnimationCurve_t3046754366;;
struct AnimationCurve_t3046754366_marshaled_com;
struct AnimationCurve_t3046754366_marshaled_com;;
struct AnimationCurve_t3046754366_marshaled_pinvoke;
struct AnimationCurve_t3046754366_marshaled_pinvoke;;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ParticleU5BU5D_t3069227754;


#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T1232140387_H
#define LIST_1_T1232140387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct  List_1_t1232140387  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ParticleCollisionEventU5BU5D_t4144522048* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1232140387, ____items_1)); }
	inline ParticleCollisionEventU5BU5D_t4144522048* get__items_1() const { return ____items_1; }
	inline ParticleCollisionEventU5BU5D_t4144522048** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ParticleCollisionEventU5BU5D_t4144522048* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1232140387, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1232140387, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t1232140387, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t1232140387_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ParticleCollisionEventU5BU5D_t4144522048* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t1232140387_StaticFields, ____emptyArray_5)); }
	inline ParticleCollisionEventU5BU5D_t4144522048* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ParticleCollisionEventU5BU5D_t4144522048** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ParticleCollisionEventU5BU5D_t4144522048* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1232140387_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef PARTICLEPHYSICSEXTENSIONS_T1867354557_H
#define PARTICLEPHYSICSEXTENSIONS_T1867354557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticlePhysicsExtensions
struct  ParticlePhysicsExtensions_t1867354557  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEPHYSICSEXTENSIONS_T1867354557_H
#ifndef PARTICLESYSTEMEXTENSIONSIMPL_T490859600_H
#define PARTICLESYSTEMEXTENSIONSIMPL_T490859600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemExtensionsImpl
struct  ParticleSystemExtensionsImpl_t490859600  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMEXTENSIONSIMPL_T490859600_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef EMISSIONMODULE_T311448003_H
#define EMISSIONMODULE_T311448003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/EmissionModule
struct  EmissionModule_t311448003 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/EmissionModule::m_ParticleSystem
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(EmissionModule_t311448003, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t1800779281 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t1800779281 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t1800779281 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t311448003_marshaled_pinvoke
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t311448003_marshaled_com
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
#endif // EMISSIONMODULE_T311448003_H
#ifndef MAINMODULE_T2320046318_H
#define MAINMODULE_T2320046318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MainModule
struct  MainModule_t2320046318 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t2320046318, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t1800779281 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t1800779281 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t1800779281 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t2320046318_marshaled_pinvoke
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t2320046318_marshaled_com
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
#endif // MAINMODULE_T2320046318_H
#ifndef SHAPEMODULE_T3608330829_H
#define SHAPEMODULE_T3608330829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/ShapeModule
struct  ShapeModule_t3608330829 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/ShapeModule::m_ParticleSystem
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(ShapeModule_t3608330829, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t1800779281 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t1800779281 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t1800779281 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/ShapeModule
struct ShapeModule_t3608330829_marshaled_pinvoke
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/ShapeModule
struct ShapeModule_t3608330829_marshaled_com
{
	ParticleSystem_t1800779281 * ___m_ParticleSystem_0;
};
#endif // SHAPEMODULE_T3608330829_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ARGUMENTEXCEPTION_T132251570_H
#define ARGUMENTEXCEPTION_T132251570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t132251570  : public SystemException_t176217640
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t132251570, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_paramName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T132251570_H
#ifndef ANIMATIONCURVE_T3046754366_H
#define ANIMATIONCURVE_T3046754366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t3046754366  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t3046754366, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T3046754366_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef PARTICLECOLLISIONEVENT_T4055032941_H
#define PARTICLECOLLISIONEVENT_T4055032941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleCollisionEvent
struct  ParticleCollisionEvent_t4055032941 
{
public:
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Intersection
	Vector3_t3722313464  ___m_Intersection_0;
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::m_Velocity
	Vector3_t3722313464  ___m_Velocity_2;
	// System.Int32 UnityEngine.ParticleCollisionEvent::m_ColliderInstanceID
	int32_t ___m_ColliderInstanceID_3;

public:
	inline static int32_t get_offset_of_m_Intersection_0() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t4055032941, ___m_Intersection_0)); }
	inline Vector3_t3722313464  get_m_Intersection_0() const { return ___m_Intersection_0; }
	inline Vector3_t3722313464 * get_address_of_m_Intersection_0() { return &___m_Intersection_0; }
	inline void set_m_Intersection_0(Vector3_t3722313464  value)
	{
		___m_Intersection_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t4055032941, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_2() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t4055032941, ___m_Velocity_2)); }
	inline Vector3_t3722313464  get_m_Velocity_2() const { return ___m_Velocity_2; }
	inline Vector3_t3722313464 * get_address_of_m_Velocity_2() { return &___m_Velocity_2; }
	inline void set_m_Velocity_2(Vector3_t3722313464  value)
	{
		___m_Velocity_2 = value;
	}

	inline static int32_t get_offset_of_m_ColliderInstanceID_3() { return static_cast<int32_t>(offsetof(ParticleCollisionEvent_t4055032941, ___m_ColliderInstanceID_3)); }
	inline int32_t get_m_ColliderInstanceID_3() const { return ___m_ColliderInstanceID_3; }
	inline int32_t* get_address_of_m_ColliderInstanceID_3() { return &___m_ColliderInstanceID_3; }
	inline void set_m_ColliderInstanceID_3(int32_t value)
	{
		___m_ColliderInstanceID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECOLLISIONEVENT_T4055032941_H
#ifndef PARTICLE_T1882894987_H
#define PARTICLE_T1882894987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/Particle
struct  Particle_t1882894987 
{
public:
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Position
	Vector3_t3722313464  ___m_Position_0;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Velocity
	Vector3_t3722313464  ___m_Velocity_1;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AnimatedVelocity
	Vector3_t3722313464  ___m_AnimatedVelocity_2;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_InitialVelocity
	Vector3_t3722313464  ___m_InitialVelocity_3;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AxisOfRotation
	Vector3_t3722313464  ___m_AxisOfRotation_4;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Rotation
	Vector3_t3722313464  ___m_Rotation_5;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AngularVelocity
	Vector3_t3722313464  ___m_AngularVelocity_6;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_StartSize
	Vector3_t3722313464  ___m_StartSize_7;
	// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::m_StartColor
	Color32_t2600501292  ___m_StartColor_8;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_RandomSeed
	uint32_t ___m_RandomSeed_9;
	// System.Single UnityEngine.ParticleSystem/Particle::m_Lifetime
	float ___m_Lifetime_10;
	// System.Single UnityEngine.ParticleSystem/Particle::m_StartLifetime
	float ___m_StartLifetime_11;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator0
	float ___m_EmitAccumulator0_12;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator1
	float ___m_EmitAccumulator1_13;

public:
	inline static int32_t get_offset_of_m_Position_0() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_Position_0)); }
	inline Vector3_t3722313464  get_m_Position_0() const { return ___m_Position_0; }
	inline Vector3_t3722313464 * get_address_of_m_Position_0() { return &___m_Position_0; }
	inline void set_m_Position_0(Vector3_t3722313464  value)
	{
		___m_Position_0 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_1() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_Velocity_1)); }
	inline Vector3_t3722313464  get_m_Velocity_1() const { return ___m_Velocity_1; }
	inline Vector3_t3722313464 * get_address_of_m_Velocity_1() { return &___m_Velocity_1; }
	inline void set_m_Velocity_1(Vector3_t3722313464  value)
	{
		___m_Velocity_1 = value;
	}

	inline static int32_t get_offset_of_m_AnimatedVelocity_2() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_AnimatedVelocity_2)); }
	inline Vector3_t3722313464  get_m_AnimatedVelocity_2() const { return ___m_AnimatedVelocity_2; }
	inline Vector3_t3722313464 * get_address_of_m_AnimatedVelocity_2() { return &___m_AnimatedVelocity_2; }
	inline void set_m_AnimatedVelocity_2(Vector3_t3722313464  value)
	{
		___m_AnimatedVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_InitialVelocity_3() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_InitialVelocity_3)); }
	inline Vector3_t3722313464  get_m_InitialVelocity_3() const { return ___m_InitialVelocity_3; }
	inline Vector3_t3722313464 * get_address_of_m_InitialVelocity_3() { return &___m_InitialVelocity_3; }
	inline void set_m_InitialVelocity_3(Vector3_t3722313464  value)
	{
		___m_InitialVelocity_3 = value;
	}

	inline static int32_t get_offset_of_m_AxisOfRotation_4() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_AxisOfRotation_4)); }
	inline Vector3_t3722313464  get_m_AxisOfRotation_4() const { return ___m_AxisOfRotation_4; }
	inline Vector3_t3722313464 * get_address_of_m_AxisOfRotation_4() { return &___m_AxisOfRotation_4; }
	inline void set_m_AxisOfRotation_4(Vector3_t3722313464  value)
	{
		___m_AxisOfRotation_4 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_5() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_Rotation_5)); }
	inline Vector3_t3722313464  get_m_Rotation_5() const { return ___m_Rotation_5; }
	inline Vector3_t3722313464 * get_address_of_m_Rotation_5() { return &___m_Rotation_5; }
	inline void set_m_Rotation_5(Vector3_t3722313464  value)
	{
		___m_Rotation_5 = value;
	}

	inline static int32_t get_offset_of_m_AngularVelocity_6() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_AngularVelocity_6)); }
	inline Vector3_t3722313464  get_m_AngularVelocity_6() const { return ___m_AngularVelocity_6; }
	inline Vector3_t3722313464 * get_address_of_m_AngularVelocity_6() { return &___m_AngularVelocity_6; }
	inline void set_m_AngularVelocity_6(Vector3_t3722313464  value)
	{
		___m_AngularVelocity_6 = value;
	}

	inline static int32_t get_offset_of_m_StartSize_7() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_StartSize_7)); }
	inline Vector3_t3722313464  get_m_StartSize_7() const { return ___m_StartSize_7; }
	inline Vector3_t3722313464 * get_address_of_m_StartSize_7() { return &___m_StartSize_7; }
	inline void set_m_StartSize_7(Vector3_t3722313464  value)
	{
		___m_StartSize_7 = value;
	}

	inline static int32_t get_offset_of_m_StartColor_8() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_StartColor_8)); }
	inline Color32_t2600501292  get_m_StartColor_8() const { return ___m_StartColor_8; }
	inline Color32_t2600501292 * get_address_of_m_StartColor_8() { return &___m_StartColor_8; }
	inline void set_m_StartColor_8(Color32_t2600501292  value)
	{
		___m_StartColor_8 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeed_9() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_RandomSeed_9)); }
	inline uint32_t get_m_RandomSeed_9() const { return ___m_RandomSeed_9; }
	inline uint32_t* get_address_of_m_RandomSeed_9() { return &___m_RandomSeed_9; }
	inline void set_m_RandomSeed_9(uint32_t value)
	{
		___m_RandomSeed_9 = value;
	}

	inline static int32_t get_offset_of_m_Lifetime_10() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_Lifetime_10)); }
	inline float get_m_Lifetime_10() const { return ___m_Lifetime_10; }
	inline float* get_address_of_m_Lifetime_10() { return &___m_Lifetime_10; }
	inline void set_m_Lifetime_10(float value)
	{
		___m_Lifetime_10 = value;
	}

	inline static int32_t get_offset_of_m_StartLifetime_11() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_StartLifetime_11)); }
	inline float get_m_StartLifetime_11() const { return ___m_StartLifetime_11; }
	inline float* get_address_of_m_StartLifetime_11() { return &___m_StartLifetime_11; }
	inline void set_m_StartLifetime_11(float value)
	{
		___m_StartLifetime_11 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator0_12() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_EmitAccumulator0_12)); }
	inline float get_m_EmitAccumulator0_12() const { return ___m_EmitAccumulator0_12; }
	inline float* get_address_of_m_EmitAccumulator0_12() { return &___m_EmitAccumulator0_12; }
	inline void set_m_EmitAccumulator0_12(float value)
	{
		___m_EmitAccumulator0_12 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator1_13() { return static_cast<int32_t>(offsetof(Particle_t1882894987, ___m_EmitAccumulator1_13)); }
	inline float get_m_EmitAccumulator1_13() const { return ___m_EmitAccumulator1_13; }
	inline float* get_address_of_m_EmitAccumulator1_13() { return &___m_EmitAccumulator1_13; }
	inline void set_m_EmitAccumulator1_13(float value)
	{
		___m_EmitAccumulator1_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLE_T1882894987_H
#ifndef PARTICLESYSTEMCURVEMODE_T3859704052_H
#define PARTICLESYSTEMCURVEMODE_T3859704052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemCurveMode
struct  ParticleSystemCurveMode_t3859704052 
{
public:
	// System.Int32 UnityEngine.ParticleSystemCurveMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemCurveMode_t3859704052, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMCURVEMODE_T3859704052_H
#ifndef PARTICLESYSTEMSHAPETYPE_T4289797000_H
#define PARTICLESYSTEMSHAPETYPE_T4289797000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemShapeType
struct  ParticleSystemShapeType_t4289797000 
{
public:
	// System.Int32 UnityEngine.ParticleSystemShapeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemShapeType_t4289797000, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSHAPETYPE_T4289797000_H
#ifndef PARTICLESYSTEMSTOPBEHAVIOR_T2808326180_H
#define PARTICLESYSTEMSTOPBEHAVIOR_T2808326180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemStopBehavior
struct  ParticleSystemStopBehavior_t2808326180 
{
public:
	// System.Int32 UnityEngine.ParticleSystemStopBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemStopBehavior_t2808326180, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSTOPBEHAVIOR_T2808326180_H
#ifndef ARGUMENTNULLEXCEPTION_T1615371798_H
#define ARGUMENTNULLEXCEPTION_T1615371798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t1615371798  : public ArgumentException_t132251570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T1615371798_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef EMITPARAMS_T2216423628_H
#define EMITPARAMS_T2216423628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/EmitParams
struct  EmitParams_t2216423628 
{
public:
	// UnityEngine.ParticleSystem/Particle UnityEngine.ParticleSystem/EmitParams::m_Particle
	Particle_t1882894987  ___m_Particle_0;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_PositionSet
	bool ___m_PositionSet_1;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_VelocitySet
	bool ___m_VelocitySet_2;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AxisOfRotationSet
	bool ___m_AxisOfRotationSet_3;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RotationSet
	bool ___m_RotationSet_4;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AngularVelocitySet
	bool ___m_AngularVelocitySet_5;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartSizeSet
	bool ___m_StartSizeSet_6;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartColorSet
	bool ___m_StartColorSet_7;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RandomSeedSet
	bool ___m_RandomSeedSet_8;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartLifetimeSet
	bool ___m_StartLifetimeSet_9;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_ApplyShapeToPosition
	bool ___m_ApplyShapeToPosition_10;

public:
	inline static int32_t get_offset_of_m_Particle_0() { return static_cast<int32_t>(offsetof(EmitParams_t2216423628, ___m_Particle_0)); }
	inline Particle_t1882894987  get_m_Particle_0() const { return ___m_Particle_0; }
	inline Particle_t1882894987 * get_address_of_m_Particle_0() { return &___m_Particle_0; }
	inline void set_m_Particle_0(Particle_t1882894987  value)
	{
		___m_Particle_0 = value;
	}

	inline static int32_t get_offset_of_m_PositionSet_1() { return static_cast<int32_t>(offsetof(EmitParams_t2216423628, ___m_PositionSet_1)); }
	inline bool get_m_PositionSet_1() const { return ___m_PositionSet_1; }
	inline bool* get_address_of_m_PositionSet_1() { return &___m_PositionSet_1; }
	inline void set_m_PositionSet_1(bool value)
	{
		___m_PositionSet_1 = value;
	}

	inline static int32_t get_offset_of_m_VelocitySet_2() { return static_cast<int32_t>(offsetof(EmitParams_t2216423628, ___m_VelocitySet_2)); }
	inline bool get_m_VelocitySet_2() const { return ___m_VelocitySet_2; }
	inline bool* get_address_of_m_VelocitySet_2() { return &___m_VelocitySet_2; }
	inline void set_m_VelocitySet_2(bool value)
	{
		___m_VelocitySet_2 = value;
	}

	inline static int32_t get_offset_of_m_AxisOfRotationSet_3() { return static_cast<int32_t>(offsetof(EmitParams_t2216423628, ___m_AxisOfRotationSet_3)); }
	inline bool get_m_AxisOfRotationSet_3() const { return ___m_AxisOfRotationSet_3; }
	inline bool* get_address_of_m_AxisOfRotationSet_3() { return &___m_AxisOfRotationSet_3; }
	inline void set_m_AxisOfRotationSet_3(bool value)
	{
		___m_AxisOfRotationSet_3 = value;
	}

	inline static int32_t get_offset_of_m_RotationSet_4() { return static_cast<int32_t>(offsetof(EmitParams_t2216423628, ___m_RotationSet_4)); }
	inline bool get_m_RotationSet_4() const { return ___m_RotationSet_4; }
	inline bool* get_address_of_m_RotationSet_4() { return &___m_RotationSet_4; }
	inline void set_m_RotationSet_4(bool value)
	{
		___m_RotationSet_4 = value;
	}

	inline static int32_t get_offset_of_m_AngularVelocitySet_5() { return static_cast<int32_t>(offsetof(EmitParams_t2216423628, ___m_AngularVelocitySet_5)); }
	inline bool get_m_AngularVelocitySet_5() const { return ___m_AngularVelocitySet_5; }
	inline bool* get_address_of_m_AngularVelocitySet_5() { return &___m_AngularVelocitySet_5; }
	inline void set_m_AngularVelocitySet_5(bool value)
	{
		___m_AngularVelocitySet_5 = value;
	}

	inline static int32_t get_offset_of_m_StartSizeSet_6() { return static_cast<int32_t>(offsetof(EmitParams_t2216423628, ___m_StartSizeSet_6)); }
	inline bool get_m_StartSizeSet_6() const { return ___m_StartSizeSet_6; }
	inline bool* get_address_of_m_StartSizeSet_6() { return &___m_StartSizeSet_6; }
	inline void set_m_StartSizeSet_6(bool value)
	{
		___m_StartSizeSet_6 = value;
	}

	inline static int32_t get_offset_of_m_StartColorSet_7() { return static_cast<int32_t>(offsetof(EmitParams_t2216423628, ___m_StartColorSet_7)); }
	inline bool get_m_StartColorSet_7() const { return ___m_StartColorSet_7; }
	inline bool* get_address_of_m_StartColorSet_7() { return &___m_StartColorSet_7; }
	inline void set_m_StartColorSet_7(bool value)
	{
		___m_StartColorSet_7 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeedSet_8() { return static_cast<int32_t>(offsetof(EmitParams_t2216423628, ___m_RandomSeedSet_8)); }
	inline bool get_m_RandomSeedSet_8() const { return ___m_RandomSeedSet_8; }
	inline bool* get_address_of_m_RandomSeedSet_8() { return &___m_RandomSeedSet_8; }
	inline void set_m_RandomSeedSet_8(bool value)
	{
		___m_RandomSeedSet_8 = value;
	}

	inline static int32_t get_offset_of_m_StartLifetimeSet_9() { return static_cast<int32_t>(offsetof(EmitParams_t2216423628, ___m_StartLifetimeSet_9)); }
	inline bool get_m_StartLifetimeSet_9() const { return ___m_StartLifetimeSet_9; }
	inline bool* get_address_of_m_StartLifetimeSet_9() { return &___m_StartLifetimeSet_9; }
	inline void set_m_StartLifetimeSet_9(bool value)
	{
		___m_StartLifetimeSet_9 = value;
	}

	inline static int32_t get_offset_of_m_ApplyShapeToPosition_10() { return static_cast<int32_t>(offsetof(EmitParams_t2216423628, ___m_ApplyShapeToPosition_10)); }
	inline bool get_m_ApplyShapeToPosition_10() const { return ___m_ApplyShapeToPosition_10; }
	inline bool* get_address_of_m_ApplyShapeToPosition_10() { return &___m_ApplyShapeToPosition_10; }
	inline void set_m_ApplyShapeToPosition_10(bool value)
	{
		___m_ApplyShapeToPosition_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_t2216423628_marshaled_pinvoke
{
	Particle_t1882894987  ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_ApplyShapeToPosition_10;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_t2216423628_marshaled_com
{
	Particle_t1882894987  ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_ApplyShapeToPosition_10;
};
#endif // EMITPARAMS_T2216423628_H
#ifndef MINMAXCURVE_T1067599125_H
#define MINMAXCURVE_T1067599125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MinMaxCurve
struct  MinMaxCurve_t1067599125 
{
public:
	// UnityEngine.ParticleSystemCurveMode UnityEngine.ParticleSystem/MinMaxCurve::m_Mode
	int32_t ___m_Mode_0;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMultiplier
	float ___m_CurveMultiplier_1;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMin
	AnimationCurve_t3046754366 * ___m_CurveMin_2;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMax
	AnimationCurve_t3046754366 * ___m_CurveMax_3;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMin
	float ___m_ConstantMin_4;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMax
	float ___m_ConstantMax_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_CurveMultiplier_1() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_CurveMultiplier_1)); }
	inline float get_m_CurveMultiplier_1() const { return ___m_CurveMultiplier_1; }
	inline float* get_address_of_m_CurveMultiplier_1() { return &___m_CurveMultiplier_1; }
	inline void set_m_CurveMultiplier_1(float value)
	{
		___m_CurveMultiplier_1 = value;
	}

	inline static int32_t get_offset_of_m_CurveMin_2() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_CurveMin_2)); }
	inline AnimationCurve_t3046754366 * get_m_CurveMin_2() const { return ___m_CurveMin_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_m_CurveMin_2() { return &___m_CurveMin_2; }
	inline void set_m_CurveMin_2(AnimationCurve_t3046754366 * value)
	{
		___m_CurveMin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurveMin_2), value);
	}

	inline static int32_t get_offset_of_m_CurveMax_3() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_CurveMax_3)); }
	inline AnimationCurve_t3046754366 * get_m_CurveMax_3() const { return ___m_CurveMax_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_m_CurveMax_3() { return &___m_CurveMax_3; }
	inline void set_m_CurveMax_3(AnimationCurve_t3046754366 * value)
	{
		___m_CurveMax_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurveMax_3), value);
	}

	inline static int32_t get_offset_of_m_ConstantMin_4() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_ConstantMin_4)); }
	inline float get_m_ConstantMin_4() const { return ___m_ConstantMin_4; }
	inline float* get_address_of_m_ConstantMin_4() { return &___m_ConstantMin_4; }
	inline void set_m_ConstantMin_4(float value)
	{
		___m_ConstantMin_4 = value;
	}

	inline static int32_t get_offset_of_m_ConstantMax_5() { return static_cast<int32_t>(offsetof(MinMaxCurve_t1067599125, ___m_ConstantMax_5)); }
	inline float get_m_ConstantMax_5() const { return ___m_ConstantMax_5; }
	inline float* get_address_of_m_ConstantMax_5() { return &___m_ConstantMax_5; }
	inline void set_m_ConstantMax_5(float value)
	{
		___m_ConstantMax_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t1067599125_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	float ___m_CurveMultiplier_1;
	AnimationCurve_t3046754366_marshaled_pinvoke ___m_CurveMin_2;
	AnimationCurve_t3046754366_marshaled_pinvoke ___m_CurveMax_3;
	float ___m_ConstantMin_4;
	float ___m_ConstantMax_5;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t1067599125_marshaled_com
{
	int32_t ___m_Mode_0;
	float ___m_CurveMultiplier_1;
	AnimationCurve_t3046754366_marshaled_com* ___m_CurveMin_2;
	AnimationCurve_t3046754366_marshaled_com* ___m_CurveMax_3;
	float ___m_ConstantMin_4;
	float ___m_ConstantMax_5;
};
#endif // MINMAXCURVE_T1067599125_H
#ifndef PARTICLESYSTEM_T1800779281_H
#define PARTICLESYSTEM_T1800779281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t1800779281  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T1800779281_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef PARTICLESYSTEMRENDERER_T2065813411_H
#define PARTICLESYSTEMRENDERER_T2065813411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemRenderer
struct  ParticleSystemRenderer_t2065813411  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMRENDERER_T2065813411_H
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t3069227754  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Particle_t1882894987  m_Items[1];

public:
	inline Particle_t1882894987  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Particle_t1882894987 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Particle_t1882894987  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Particle_t1882894987  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Particle_t1882894987 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Particle_t1882894987  value)
	{
		m_Items[index] = value;
	}
};

extern "C" void AnimationCurve_t3046754366_marshal_pinvoke(const AnimationCurve_t3046754366& unmarshaled, AnimationCurve_t3046754366_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_back(const AnimationCurve_t3046754366_marshaled_pinvoke& marshaled, AnimationCurve_t3046754366& unmarshaled);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_cleanup(AnimationCurve_t3046754366_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3046754366_marshal_com(const AnimationCurve_t3046754366& unmarshaled, AnimationCurve_t3046754366_marshaled_com& marshaled);
extern "C" void AnimationCurve_t3046754366_marshal_com_back(const AnimationCurve_t3046754366_marshaled_com& marshaled, AnimationCurve_t3046754366& unmarshaled);
extern "C" void AnimationCurve_t3046754366_marshal_com_cleanup(AnimationCurve_t3046754366_marshaled_com& marshaled);


// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::get_intersection()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  ParticleCollisionEvent_get_intersection_m2741146774 (ParticleCollisionEvent_t4055032941 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::get_velocity()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  ParticleCollisionEvent_get_velocity_m1941659151 (ParticleCollisionEvent_t4055032941 * __this, const RuntimeMethod* method);
// UnityEngine.Component UnityEngine.ParticleCollisionEvent::InstanceIDToColliderComponent(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Component_t1923634451 * ParticleCollisionEvent_InstanceIDToColliderComponent_m3582923694 (RuntimeObject * __this /* static, unused */, int32_t ___instanceID0, const RuntimeMethod* method);
// UnityEngine.Component UnityEngine.ParticleCollisionEvent::get_colliderComponent()
extern "C" IL2CPP_METHOD_ATTR Component_t1923634451 * ParticleCollisionEvent_get_colliderComponent_m1489433520 (ParticleCollisionEvent_t4055032941 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.ParticleSystemExtensionsImpl::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,System.Object)
extern "C" IL2CPP_METHOD_ATTR int32_t ParticleSystemExtensionsImpl_GetCollisionEvents_m2141038518 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___ps0, GameObject_t1113636619 * ___go1, RuntimeObject * ___collisionEvents2, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR void MainModule__ctor_m1745438521 (MainModule_t2320046318 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule__ctor_m3908790904 (EmissionModule_t311448003 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR void ShapeModule__ctor_m3109297265 (ShapeModule_t3608330829 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Play_m163824593 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Stop_m3396581118 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Clear_m2603704560 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::INTERNAL_CALL_Emit(UnityEngine.ParticleSystem,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_INTERNAL_CALL_Emit_m662166748 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, int32_t ___count1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Internal_Emit(UnityEngine.ParticleSystem/EmitParams&,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Internal_Emit_m2240046946 (ParticleSystem_t1800779281 * __this, EmitParams_t2216423628 * ___emitParams0, int32_t ___count1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_position_m4147191379 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_velocity_m1686335204 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_lifetime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_lifetime_m1971908220 (Particle_t1882894987 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_startLifetime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_startLifetime_m2608171500 (Particle_t1882894987 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_startSize_m2554682920 (Particle_t1882894987 * __this, float ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_rotation3D(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_rotation3D_m2156157200 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity3D(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_angularVelocity3D_m3163963446 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_startColor_m3825027702 (Particle_t1882894987 * __this, Color32_t2600501292  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_randomSeed(System.UInt32)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_randomSeed_m2900137887 (Particle_t1882894987 * __this, uint32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Internal_EmitOld(UnityEngine.ParticleSystem/Particle&)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Internal_EmitOld_m3511379528 (ParticleSystem_t1800779281 * __this, Particle_t1882894987 * ___particle0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule_SetEnabled_m1150129533 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, bool ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule_set_enabled_m353945573 (EmissionModule_t311448003 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule_SetRateOverTime_m1878531883 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule_set_rateOverTime_m3001949402 (EmissionModule_t311448003 * __this, MinMaxCurve_t1067599125  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::GetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule_GetRateOverTime_m4185674785 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/EmissionModule::get_rateOverTime()
extern "C" IL2CPP_METHOD_ATTR MinMaxCurve_t1067599125  EmissionModule_get_rateOverTime_m865519278 (EmissionModule_t311448003 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmitParams::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void EmitParams_set_position_m3162245934 (EmitParams_t2216423628 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmitParams::set_velocity(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void EmitParams_set_velocity_m4290211678 (EmitParams_t2216423628 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmitParams::set_startLifetime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void EmitParams_set_startLifetime_m3865544057 (EmitParams_t2216423628 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmitParams::set_startSize(System.Single)
extern "C" IL2CPP_METHOD_ATTR void EmitParams_set_startSize_m814526808 (EmitParams_t2216423628 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmitParams::set_startColor(UnityEngine.Color32)
extern "C" IL2CPP_METHOD_ATTR void EmitParams_set_startColor_m4087630947 (EmitParams_t2216423628 * __this, Color32_t2600501292  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartLifetime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_GetStartLifetime_m3113513190 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_startLifetime()
extern "C" IL2CPP_METHOD_ATTR MinMaxCurve_t1067599125  MainModule_get_startLifetime_m2343501481 (MainModule_t2320046318 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::GetStartLifetimeMultiplier(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR float MainModule_GetStartLifetimeMultiplier_m2481667184 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startLifetimeMultiplier()
extern "C" IL2CPP_METHOD_ATTR float MainModule_get_startLifetimeMultiplier_m1798732576 (MainModule_t2320046318 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartLifetimeMultiplier(UnityEngine.ParticleSystem,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_SetStartLifetimeMultiplier_m3747684520 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startLifetimeMultiplier(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_set_startLifetimeMultiplier_m4121815467 (MainModule_t2320046318 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSpeed(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_SetStartSpeed_m1242675629 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeed(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C" IL2CPP_METHOD_ATTR void MainModule_set_startSpeed_m3763022313 (MainModule_t2320046318 * __this, MinMaxCurve_t1067599125  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartSpeed(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_GetStartSpeed_m1661822356 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_startSpeed()
extern "C" IL2CPP_METHOD_ATTR MinMaxCurve_t1067599125  MainModule_get_startSpeed_m3768187059 (MainModule_t2320046318 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::GetStartSpeedMultiplier(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR float MainModule_GetStartSpeedMultiplier_m1633461692 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSpeedMultiplier()
extern "C" IL2CPP_METHOD_ATTR float MainModule_get_startSpeedMultiplier_m3876540512 (MainModule_t2320046318 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSpeedMultiplier(UnityEngine.ParticleSystem,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_SetStartSpeedMultiplier_m2549987384 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeedMultiplier(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_set_startSpeedMultiplier_m3632924240 (MainModule_t2320046318 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSizeX(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_SetStartSizeX_m4044089380 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSize(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C" IL2CPP_METHOD_ATTR void MainModule_set_startSize_m1275429743 (MainModule_t2320046318 * __this, MinMaxCurve_t1067599125  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartSizeX(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_GetStartSizeX_m1773159447 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_startSize()
extern "C" IL2CPP_METHOD_ATTR MinMaxCurve_t1067599125  MainModule_get_startSize_m4033600788 (MainModule_t2320046318 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::GetStartSizeXMultiplier(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR float MainModule_GetStartSizeXMultiplier_m3834697463 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSizeMultiplier()
extern "C" IL2CPP_METHOD_ATTR float MainModule_get_startSizeMultiplier_m1188945012 (MainModule_t2320046318 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSizeXMultiplier(UnityEngine.ParticleSystem,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_SetStartSizeXMultiplier_m1251507676 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSizeMultiplier(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_set_startSizeMultiplier_m2700116177 (MainModule_t2320046318 * __this, float ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.ParticleSystem/MainModule::GetMaxParticles(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR int32_t MainModule_GetMaxParticles_m2130637551 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method);
// System.Int32 UnityEngine.ParticleSystem/MainModule::get_maxParticles()
extern "C" IL2CPP_METHOD_ATTR int32_t MainModule_get_maxParticles_m2105979121 (MainModule_t2320046318 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AnimationCurve__ctor_m3000526466 (AnimationCurve_t3046754366 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MinMaxCurve__ctor_m1734431933 (MinMaxCurve_t1067599125 * __this, float ___constant0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::set_mode(UnityEngine.ParticleSystemCurveMode)
extern "C" IL2CPP_METHOD_ATTR void MinMaxCurve_set_mode_m1255242291 (MinMaxCurve_t1067599125 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constantMax()
extern "C" IL2CPP_METHOD_ATTR float MinMaxCurve_get_constantMax_m743132770 (MinMaxCurve_t1067599125 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::set_constantMax(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MinMaxCurve_set_constantMax_m4228540433 (MinMaxCurve_t1067599125 * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constantMin()
extern "C" IL2CPP_METHOD_ATTR float MinMaxCurve_get_constantMin_m744050282 (MinMaxCurve_t1067599125 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::set_constantMin(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MinMaxCurve_set_constantMin_m1844578410 (MinMaxCurve_t1067599125 * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constant()
extern "C" IL2CPP_METHOD_ATTR float MinMaxCurve_get_constant_m2963124720 (MinMaxCurve_t1067599125 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_remainingLifetime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_remainingLifetime_m3875738131 (Particle_t1882894987 * __this, float ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Particle_get_position_m855792854 (Particle_t1882894987 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::get_velocity()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Particle_get_velocity_m2555333515 (Particle_t1882894987 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/Particle::get_remainingLifetime()
extern "C" IL2CPP_METHOD_ATTR float Particle_get_remainingLifetime_m4053779941 (Particle_t1882894987 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/ShapeModule::SetShapeType(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemShapeType)
extern "C" IL2CPP_METHOD_ATTR void ShapeModule_SetShapeType_m2621352696 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, int32_t ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/ShapeModule::set_shapeType(UnityEngine.ParticleSystemShapeType)
extern "C" IL2CPP_METHOD_ATTR void ShapeModule_set_shapeType_m507168252 (ShapeModule_t3608330829 * __this, int32_t ___value0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::get_intersection()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  ParticleCollisionEvent_get_intersection_m2741146774 (ParticleCollisionEvent_t4055032941 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Intersection_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3722313464  ParticleCollisionEvent_get_intersection_m2741146774_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ParticleCollisionEvent_t4055032941 * _thisAdjusted = reinterpret_cast<ParticleCollisionEvent_t4055032941 *>(__this + 1);
	return ParticleCollisionEvent_get_intersection_m2741146774(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.ParticleCollisionEvent::get_velocity()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  ParticleCollisionEvent_get_velocity_m1941659151 (ParticleCollisionEvent_t4055032941 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Velocity_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3722313464  ParticleCollisionEvent_get_velocity_m1941659151_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ParticleCollisionEvent_t4055032941 * _thisAdjusted = reinterpret_cast<ParticleCollisionEvent_t4055032941 *>(__this + 1);
	return ParticleCollisionEvent_get_velocity_m1941659151(_thisAdjusted, method);
}
// UnityEngine.Component UnityEngine.ParticleCollisionEvent::get_colliderComponent()
extern "C" IL2CPP_METHOD_ATTR Component_t1923634451 * ParticleCollisionEvent_get_colliderComponent_m1489433520 (ParticleCollisionEvent_t4055032941 * __this, const RuntimeMethod* method)
{
	Component_t1923634451 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_m_ColliderInstanceID_3();
		Component_t1923634451 * L_1 = ParticleCollisionEvent_InstanceIDToColliderComponent_m3582923694(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Component_t1923634451 * L_2 = V_0;
		return L_2;
	}
}
extern "C"  Component_t1923634451 * ParticleCollisionEvent_get_colliderComponent_m1489433520_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ParticleCollisionEvent_t4055032941 * _thisAdjusted = reinterpret_cast<ParticleCollisionEvent_t4055032941 *>(__this + 1);
	return ParticleCollisionEvent_get_colliderComponent_m1489433520(_thisAdjusted, method);
}
// UnityEngine.Component UnityEngine.ParticleCollisionEvent::InstanceIDToColliderComponent(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Component_t1923634451 * ParticleCollisionEvent_InstanceIDToColliderComponent_m3582923694 (RuntimeObject * __this /* static, unused */, int32_t ___instanceID0, const RuntimeMethod* method)
{
	typedef Component_t1923634451 * (*ParticleCollisionEvent_InstanceIDToColliderComponent_m3582923694_ftn) (int32_t);
	static ParticleCollisionEvent_InstanceIDToColliderComponent_m3582923694_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleCollisionEvent_InstanceIDToColliderComponent_m3582923694_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleCollisionEvent::InstanceIDToColliderComponent(System.Int32)");
	Component_t1923634451 * retVal = _il2cpp_icall_func(___instanceID0);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 UnityEngine.ParticlePhysicsExtensions::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>)
extern "C" IL2CPP_METHOD_ATTR int32_t ParticlePhysicsExtensions_GetCollisionEvents_m1108737549 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___ps0, GameObject_t1113636619 * ___go1, List_1_t1232140387 * ___collisionEvents2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticlePhysicsExtensions_GetCollisionEvents_m1108737549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		GameObject_t1113636619 * L_0 = ___go1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_2, _stringLiteral3454777273, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, ParticlePhysicsExtensions_GetCollisionEvents_m1108737549_RuntimeMethod_var);
	}

IL_0018:
	{
		List_1_t1232140387 * L_3 = ___collisionEvents2;
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_4 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_4, _stringLiteral4178700366, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, ParticlePhysicsExtensions_GetCollisionEvents_m1108737549_RuntimeMethod_var);
	}

IL_0029:
	{
		ParticleSystem_t1800779281 * L_5 = ___ps0;
		GameObject_t1113636619 * L_6 = ___go1;
		List_1_t1232140387 * L_7 = ___collisionEvents2;
		int32_t L_8 = ParticleSystemExtensionsImpl_GetCollisionEvents_m2141038518(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0037;
	}

IL_0037:
	{
		int32_t L_9 = V_0;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
extern "C" IL2CPP_METHOD_ATTR bool ParticleSystem_get_isPlaying_m1820717466 (ParticleSystem_t1800779281 * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_get_isPlaying_m1820717466_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_isPlaying_m1820717466_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isPlaying_m1820717466_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isPlaying()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C" IL2CPP_METHOD_ATTR MainModule_t2320046318  ParticleSystem_get_main_m3006917117 (ParticleSystem_t1800779281 * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MainModule_t2320046318  L_0;
		memset(&L_0, 0, sizeof(L_0));
		MainModule__ctor_m1745438521((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		MainModule_t2320046318  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern "C" IL2CPP_METHOD_ATTR EmissionModule_t311448003  ParticleSystem_get_emission_m1034302947 (ParticleSystem_t1800779281 * __this, const RuntimeMethod* method)
{
	EmissionModule_t311448003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		EmissionModule_t311448003  L_0;
		memset(&L_0, 0, sizeof(L_0));
		EmissionModule__ctor_m3908790904((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		EmissionModule_t311448003  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ShapeModule UnityEngine.ParticleSystem::get_shape()
extern "C" IL2CPP_METHOD_ATTR ShapeModule_t3608330829  ParticleSystem_get_shape_m2012347897 (ParticleSystem_t1800779281 * __this, const RuntimeMethod* method)
{
	ShapeModule_t3608330829  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ShapeModule_t3608330829  L_0;
		memset(&L_0, 0, sizeof(L_0));
		ShapeModule__ctor_m3109297265((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ShapeModule_t3608330829  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_SetParticles_m1018124896 (ParticleSystem_t1800779281 * __this, ParticleU5BU5D_t3069227754* ___particles0, int32_t ___size1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_SetParticles_m1018124896_ftn) (ParticleSystem_t1800779281 *, ParticleU5BU5D_t3069227754*, int32_t);
	static ParticleSystem_SetParticles_m1018124896_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetParticles_m1018124896_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)");
	_il2cpp_icall_func(__this, ___particles0, ___size1);
}
// System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])
extern "C" IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticles_m3661771371 (ParticleSystem_t1800779281 * __this, ParticleU5BU5D_t3069227754* ___particles0, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystem_GetParticles_m3661771371_ftn) (ParticleSystem_t1800779281 *, ParticleU5BU5D_t3069227754*);
	static ParticleSystem_GetParticles_m3661771371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetParticles_m3661771371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])");
	int32_t retVal = _il2cpp_icall_func(__this, ___particles0);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Play_m163824593 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Play_m163824593_ftn) (ParticleSystem_t1800779281 *, bool);
	static ParticleSystem_Play_m163824593_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Play_m163824593_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Play(System.Boolean)");
	_il2cpp_icall_func(__this, ___withChildren0);
}
// System.Void UnityEngine.ParticleSystem::Play()
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Play_m882713458 (ParticleSystem_t1800779281 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		ParticleSystem_Play_m163824593(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Stop_m3396581118 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Stop_m3396581118_ftn) (ParticleSystem_t1800779281 *, bool, int32_t);
	static ParticleSystem_Stop_m3396581118_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Stop_m3396581118_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)");
	_il2cpp_icall_func(__this, ___withChildren0, ___stopBehavior1);
}
// System.Void UnityEngine.ParticleSystem::Stop()
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Stop_m3125854227 (ParticleSystem_t1800779281 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 1;
		V_1 = (bool)1;
		bool L_0 = V_1;
		int32_t L_1 = V_0;
		ParticleSystem_Stop_m3396581118(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Clear_m2603704560 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Clear_m2603704560_ftn) (ParticleSystem_t1800779281 *, bool);
	static ParticleSystem_Clear_m2603704560_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Clear_m2603704560_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Clear(System.Boolean)");
	_il2cpp_icall_func(__this, ___withChildren0);
}
// System.Void UnityEngine.ParticleSystem::Clear()
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Clear_m381529807 (ParticleSystem_t1800779281 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		ParticleSystem_Clear_m2603704560(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Emit(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Emit_m2162102900 (ParticleSystem_t1800779281 * __this, int32_t ___count0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___count0;
		ParticleSystem_INTERNAL_CALL_Emit_m662166748(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::INTERNAL_CALL_Emit(UnityEngine.ParticleSystem,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_INTERNAL_CALL_Emit_m662166748 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, int32_t ___count1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_INTERNAL_CALL_Emit_m662166748_ftn) (ParticleSystem_t1800779281 *, int32_t);
	static ParticleSystem_INTERNAL_CALL_Emit_m662166748_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_INTERNAL_CALL_Emit_m662166748_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::INTERNAL_CALL_Emit(UnityEngine.ParticleSystem,System.Int32)");
	_il2cpp_icall_func(___self0, ___count1);
}
// System.Void UnityEngine.ParticleSystem::Internal_EmitOld(UnityEngine.ParticleSystem/Particle&)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Internal_EmitOld_m3511379528 (ParticleSystem_t1800779281 * __this, Particle_t1882894987 * ___particle0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Internal_EmitOld_m3511379528_ftn) (ParticleSystem_t1800779281 *, Particle_t1882894987 *);
	static ParticleSystem_Internal_EmitOld_m3511379528_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_EmitOld_m3511379528_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_EmitOld(UnityEngine.ParticleSystem/Particle&)");
	_il2cpp_icall_func(__this, ___particle0);
}
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/EmitParams,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Emit_m1241484254 (ParticleSystem_t1800779281 * __this, EmitParams_t2216423628  ___emitParams0, int32_t ___count1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___count1;
		ParticleSystem_Internal_Emit_m2240046946(__this, (EmitParams_t2216423628 *)(&___emitParams0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Internal_Emit(UnityEngine.ParticleSystem/EmitParams&,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Internal_Emit_m2240046946 (ParticleSystem_t1800779281 * __this, EmitParams_t2216423628 * ___emitParams0, int32_t ___count1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Internal_Emit_m2240046946_ftn) (ParticleSystem_t1800779281 *, EmitParams_t2216423628 *, int32_t);
	static ParticleSystem_Internal_Emit_m2240046946_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Emit_m2240046946_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Emit(UnityEngine.ParticleSystem/EmitParams&,System.Int32)");
	_il2cpp_icall_func(__this, ___emitParams0, ___count1);
}
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color32)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Emit_m497964751 (ParticleSystem_t1800779281 * __this, Vector3_t3722313464  ___position0, Vector3_t3722313464  ___velocity1, float ___size2, float ___lifetime3, Color32_t2600501292  ___color4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Emit_m497964751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Particle_t1882894987  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(Particle_t1882894987 ));
		Vector3_t3722313464  L_0 = ___position0;
		Particle_set_position_m4147191379((Particle_t1882894987 *)(&V_0), L_0, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = ___velocity1;
		Particle_set_velocity_m1686335204((Particle_t1882894987 *)(&V_0), L_1, /*hidden argument*/NULL);
		float L_2 = ___lifetime3;
		Particle_set_lifetime_m1971908220((Particle_t1882894987 *)(&V_0), L_2, /*hidden argument*/NULL);
		float L_3 = ___lifetime3;
		Particle_set_startLifetime_m2608171500((Particle_t1882894987 *)(&V_0), L_3, /*hidden argument*/NULL);
		float L_4 = ___size2;
		Particle_set_startSize_m2554682920((Particle_t1882894987 *)(&V_0), L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_5 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		Particle_set_rotation3D_m2156157200((Particle_t1882894987 *)(&V_0), L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		Particle_set_angularVelocity3D_m3163963446((Particle_t1882894987 *)(&V_0), L_6, /*hidden argument*/NULL);
		Color32_t2600501292  L_7 = ___color4;
		Particle_set_startColor_m3825027702((Particle_t1882894987 *)(&V_0), L_7, /*hidden argument*/NULL);
		Particle_set_randomSeed_m2900137887((Particle_t1882894987 *)(&V_0), 5, /*hidden argument*/NULL);
		ParticleSystem_Internal_EmitOld_m3511379528(__this, (Particle_t1882894987 *)(&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/Particle)
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Emit_m4116897928 (ParticleSystem_t1800779281 * __this, Particle_t1882894987  ___particle0, const RuntimeMethod* method)
{
	{
		ParticleSystem_Internal_EmitOld_m3511379528(__this, (Particle_t1882894987 *)(&___particle0), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t311448003_marshal_pinvoke(const EmissionModule_t311448003& unmarshaled, EmissionModule_t311448003_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
extern "C" void EmissionModule_t311448003_marshal_pinvoke_back(const EmissionModule_t311448003_marshaled_pinvoke& marshaled, EmissionModule_t311448003& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t311448003_marshal_pinvoke_cleanup(EmissionModule_t311448003_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t311448003_marshal_com(const EmissionModule_t311448003& unmarshaled, EmissionModule_t311448003_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
extern "C" void EmissionModule_t311448003_marshal_com_back(const EmissionModule_t311448003_marshaled_com& marshaled, EmissionModule_t311448003& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t311448003_marshal_com_cleanup(EmissionModule_t311448003_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule__ctor_m3908790904 (EmissionModule_t311448003 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void EmissionModule__ctor_m3908790904_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method)
{
	EmissionModule_t311448003 * _thisAdjusted = reinterpret_cast<EmissionModule_t311448003 *>(__this + 1);
	EmissionModule__ctor_m3908790904(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule_set_enabled_m353945573 (EmissionModule_t311448003 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		bool L_1 = ___value0;
		EmissionModule_SetEnabled_m1150129533(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void EmissionModule_set_enabled_m353945573_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	EmissionModule_t311448003 * _thisAdjusted = reinterpret_cast<EmissionModule_t311448003 *>(__this + 1);
	EmissionModule_set_enabled_m353945573(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule_set_rateOverTime_m3001949402 (EmissionModule_t311448003 * __this, MinMaxCurve_t1067599125  ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		EmissionModule_SetRateOverTime_m1878531883(NULL /*static, unused*/, L_0, (MinMaxCurve_t1067599125 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void EmissionModule_set_rateOverTime_m3001949402_AdjustorThunk (RuntimeObject * __this, MinMaxCurve_t1067599125  ___value0, const RuntimeMethod* method)
{
	EmissionModule_t311448003 * _thisAdjusted = reinterpret_cast<EmissionModule_t311448003 *>(__this + 1);
	EmissionModule_set_rateOverTime_m3001949402(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/EmissionModule::get_rateOverTime()
extern "C" IL2CPP_METHOD_ATTR MinMaxCurve_t1067599125  EmissionModule_get_rateOverTime_m865519278 (EmissionModule_t311448003 * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MinMaxCurve_t1067599125  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(MinMaxCurve_t1067599125 ));
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		EmissionModule_GetRateOverTime_m4185674785(NULL /*static, unused*/, L_0, (MinMaxCurve_t1067599125 *)(&V_0), /*hidden argument*/NULL);
		MinMaxCurve_t1067599125  L_1 = V_0;
		V_1 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		MinMaxCurve_t1067599125  L_2 = V_1;
		return L_2;
	}
}
extern "C"  MinMaxCurve_t1067599125  EmissionModule_get_rateOverTime_m865519278_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	EmissionModule_t311448003 * _thisAdjusted = reinterpret_cast<EmissionModule_t311448003 *>(__this + 1);
	return EmissionModule_get_rateOverTime_m865519278(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule_SetEnabled_m1150129533 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*EmissionModule_SetEnabled_m1150129533_ftn) (ParticleSystem_t1800779281 *, bool);
	static EmissionModule_SetEnabled_m1150129533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_SetEnabled_m1150129533_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)");
	_il2cpp_icall_func(___system0, ___value1);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule_SetRateOverTime_m1878531883 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*EmissionModule_SetRateOverTime_m1878531883_ftn) (ParticleSystem_t1800779281 *, MinMaxCurve_t1067599125 *);
	static EmissionModule_SetRateOverTime_m1878531883_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_SetRateOverTime_m1878531883_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::GetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void EmissionModule_GetRateOverTime_m4185674785 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*EmissionModule_GetRateOverTime_m4185674785_ftn) (ParticleSystem_t1800779281 *, MinMaxCurve_t1067599125 *);
	static EmissionModule_GetRateOverTime_m4185674785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_GetRateOverTime_m4185674785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::GetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmitParams
extern "C" void EmitParams_t2216423628_marshal_pinvoke(const EmitParams_t2216423628& unmarshaled, EmitParams_t2216423628_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Particle_0 = unmarshaled.get_m_Particle_0();
	marshaled.___m_PositionSet_1 = static_cast<int32_t>(unmarshaled.get_m_PositionSet_1());
	marshaled.___m_VelocitySet_2 = static_cast<int32_t>(unmarshaled.get_m_VelocitySet_2());
	marshaled.___m_AxisOfRotationSet_3 = static_cast<int32_t>(unmarshaled.get_m_AxisOfRotationSet_3());
	marshaled.___m_RotationSet_4 = static_cast<int32_t>(unmarshaled.get_m_RotationSet_4());
	marshaled.___m_AngularVelocitySet_5 = static_cast<int32_t>(unmarshaled.get_m_AngularVelocitySet_5());
	marshaled.___m_StartSizeSet_6 = static_cast<int32_t>(unmarshaled.get_m_StartSizeSet_6());
	marshaled.___m_StartColorSet_7 = static_cast<int32_t>(unmarshaled.get_m_StartColorSet_7());
	marshaled.___m_RandomSeedSet_8 = static_cast<int32_t>(unmarshaled.get_m_RandomSeedSet_8());
	marshaled.___m_StartLifetimeSet_9 = static_cast<int32_t>(unmarshaled.get_m_StartLifetimeSet_9());
	marshaled.___m_ApplyShapeToPosition_10 = static_cast<int32_t>(unmarshaled.get_m_ApplyShapeToPosition_10());
}
extern "C" void EmitParams_t2216423628_marshal_pinvoke_back(const EmitParams_t2216423628_marshaled_pinvoke& marshaled, EmitParams_t2216423628& unmarshaled)
{
	Particle_t1882894987  unmarshaled_m_Particle_temp_0;
	memset(&unmarshaled_m_Particle_temp_0, 0, sizeof(unmarshaled_m_Particle_temp_0));
	unmarshaled_m_Particle_temp_0 = marshaled.___m_Particle_0;
	unmarshaled.set_m_Particle_0(unmarshaled_m_Particle_temp_0);
	bool unmarshaled_m_PositionSet_temp_1 = false;
	unmarshaled_m_PositionSet_temp_1 = static_cast<bool>(marshaled.___m_PositionSet_1);
	unmarshaled.set_m_PositionSet_1(unmarshaled_m_PositionSet_temp_1);
	bool unmarshaled_m_VelocitySet_temp_2 = false;
	unmarshaled_m_VelocitySet_temp_2 = static_cast<bool>(marshaled.___m_VelocitySet_2);
	unmarshaled.set_m_VelocitySet_2(unmarshaled_m_VelocitySet_temp_2);
	bool unmarshaled_m_AxisOfRotationSet_temp_3 = false;
	unmarshaled_m_AxisOfRotationSet_temp_3 = static_cast<bool>(marshaled.___m_AxisOfRotationSet_3);
	unmarshaled.set_m_AxisOfRotationSet_3(unmarshaled_m_AxisOfRotationSet_temp_3);
	bool unmarshaled_m_RotationSet_temp_4 = false;
	unmarshaled_m_RotationSet_temp_4 = static_cast<bool>(marshaled.___m_RotationSet_4);
	unmarshaled.set_m_RotationSet_4(unmarshaled_m_RotationSet_temp_4);
	bool unmarshaled_m_AngularVelocitySet_temp_5 = false;
	unmarshaled_m_AngularVelocitySet_temp_5 = static_cast<bool>(marshaled.___m_AngularVelocitySet_5);
	unmarshaled.set_m_AngularVelocitySet_5(unmarshaled_m_AngularVelocitySet_temp_5);
	bool unmarshaled_m_StartSizeSet_temp_6 = false;
	unmarshaled_m_StartSizeSet_temp_6 = static_cast<bool>(marshaled.___m_StartSizeSet_6);
	unmarshaled.set_m_StartSizeSet_6(unmarshaled_m_StartSizeSet_temp_6);
	bool unmarshaled_m_StartColorSet_temp_7 = false;
	unmarshaled_m_StartColorSet_temp_7 = static_cast<bool>(marshaled.___m_StartColorSet_7);
	unmarshaled.set_m_StartColorSet_7(unmarshaled_m_StartColorSet_temp_7);
	bool unmarshaled_m_RandomSeedSet_temp_8 = false;
	unmarshaled_m_RandomSeedSet_temp_8 = static_cast<bool>(marshaled.___m_RandomSeedSet_8);
	unmarshaled.set_m_RandomSeedSet_8(unmarshaled_m_RandomSeedSet_temp_8);
	bool unmarshaled_m_StartLifetimeSet_temp_9 = false;
	unmarshaled_m_StartLifetimeSet_temp_9 = static_cast<bool>(marshaled.___m_StartLifetimeSet_9);
	unmarshaled.set_m_StartLifetimeSet_9(unmarshaled_m_StartLifetimeSet_temp_9);
	bool unmarshaled_m_ApplyShapeToPosition_temp_10 = false;
	unmarshaled_m_ApplyShapeToPosition_temp_10 = static_cast<bool>(marshaled.___m_ApplyShapeToPosition_10);
	unmarshaled.set_m_ApplyShapeToPosition_10(unmarshaled_m_ApplyShapeToPosition_temp_10);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmitParams
extern "C" void EmitParams_t2216423628_marshal_pinvoke_cleanup(EmitParams_t2216423628_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmitParams
extern "C" void EmitParams_t2216423628_marshal_com(const EmitParams_t2216423628& unmarshaled, EmitParams_t2216423628_marshaled_com& marshaled)
{
	marshaled.___m_Particle_0 = unmarshaled.get_m_Particle_0();
	marshaled.___m_PositionSet_1 = static_cast<int32_t>(unmarshaled.get_m_PositionSet_1());
	marshaled.___m_VelocitySet_2 = static_cast<int32_t>(unmarshaled.get_m_VelocitySet_2());
	marshaled.___m_AxisOfRotationSet_3 = static_cast<int32_t>(unmarshaled.get_m_AxisOfRotationSet_3());
	marshaled.___m_RotationSet_4 = static_cast<int32_t>(unmarshaled.get_m_RotationSet_4());
	marshaled.___m_AngularVelocitySet_5 = static_cast<int32_t>(unmarshaled.get_m_AngularVelocitySet_5());
	marshaled.___m_StartSizeSet_6 = static_cast<int32_t>(unmarshaled.get_m_StartSizeSet_6());
	marshaled.___m_StartColorSet_7 = static_cast<int32_t>(unmarshaled.get_m_StartColorSet_7());
	marshaled.___m_RandomSeedSet_8 = static_cast<int32_t>(unmarshaled.get_m_RandomSeedSet_8());
	marshaled.___m_StartLifetimeSet_9 = static_cast<int32_t>(unmarshaled.get_m_StartLifetimeSet_9());
	marshaled.___m_ApplyShapeToPosition_10 = static_cast<int32_t>(unmarshaled.get_m_ApplyShapeToPosition_10());
}
extern "C" void EmitParams_t2216423628_marshal_com_back(const EmitParams_t2216423628_marshaled_com& marshaled, EmitParams_t2216423628& unmarshaled)
{
	Particle_t1882894987  unmarshaled_m_Particle_temp_0;
	memset(&unmarshaled_m_Particle_temp_0, 0, sizeof(unmarshaled_m_Particle_temp_0));
	unmarshaled_m_Particle_temp_0 = marshaled.___m_Particle_0;
	unmarshaled.set_m_Particle_0(unmarshaled_m_Particle_temp_0);
	bool unmarshaled_m_PositionSet_temp_1 = false;
	unmarshaled_m_PositionSet_temp_1 = static_cast<bool>(marshaled.___m_PositionSet_1);
	unmarshaled.set_m_PositionSet_1(unmarshaled_m_PositionSet_temp_1);
	bool unmarshaled_m_VelocitySet_temp_2 = false;
	unmarshaled_m_VelocitySet_temp_2 = static_cast<bool>(marshaled.___m_VelocitySet_2);
	unmarshaled.set_m_VelocitySet_2(unmarshaled_m_VelocitySet_temp_2);
	bool unmarshaled_m_AxisOfRotationSet_temp_3 = false;
	unmarshaled_m_AxisOfRotationSet_temp_3 = static_cast<bool>(marshaled.___m_AxisOfRotationSet_3);
	unmarshaled.set_m_AxisOfRotationSet_3(unmarshaled_m_AxisOfRotationSet_temp_3);
	bool unmarshaled_m_RotationSet_temp_4 = false;
	unmarshaled_m_RotationSet_temp_4 = static_cast<bool>(marshaled.___m_RotationSet_4);
	unmarshaled.set_m_RotationSet_4(unmarshaled_m_RotationSet_temp_4);
	bool unmarshaled_m_AngularVelocitySet_temp_5 = false;
	unmarshaled_m_AngularVelocitySet_temp_5 = static_cast<bool>(marshaled.___m_AngularVelocitySet_5);
	unmarshaled.set_m_AngularVelocitySet_5(unmarshaled_m_AngularVelocitySet_temp_5);
	bool unmarshaled_m_StartSizeSet_temp_6 = false;
	unmarshaled_m_StartSizeSet_temp_6 = static_cast<bool>(marshaled.___m_StartSizeSet_6);
	unmarshaled.set_m_StartSizeSet_6(unmarshaled_m_StartSizeSet_temp_6);
	bool unmarshaled_m_StartColorSet_temp_7 = false;
	unmarshaled_m_StartColorSet_temp_7 = static_cast<bool>(marshaled.___m_StartColorSet_7);
	unmarshaled.set_m_StartColorSet_7(unmarshaled_m_StartColorSet_temp_7);
	bool unmarshaled_m_RandomSeedSet_temp_8 = false;
	unmarshaled_m_RandomSeedSet_temp_8 = static_cast<bool>(marshaled.___m_RandomSeedSet_8);
	unmarshaled.set_m_RandomSeedSet_8(unmarshaled_m_RandomSeedSet_temp_8);
	bool unmarshaled_m_StartLifetimeSet_temp_9 = false;
	unmarshaled_m_StartLifetimeSet_temp_9 = static_cast<bool>(marshaled.___m_StartLifetimeSet_9);
	unmarshaled.set_m_StartLifetimeSet_9(unmarshaled_m_StartLifetimeSet_temp_9);
	bool unmarshaled_m_ApplyShapeToPosition_temp_10 = false;
	unmarshaled_m_ApplyShapeToPosition_temp_10 = static_cast<bool>(marshaled.___m_ApplyShapeToPosition_10);
	unmarshaled.set_m_ApplyShapeToPosition_10(unmarshaled_m_ApplyShapeToPosition_temp_10);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmitParams
extern "C" void EmitParams_t2216423628_marshal_com_cleanup(EmitParams_t2216423628_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/EmitParams::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void EmitParams_set_position_m3162245934 (EmitParams_t2216423628 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	{
		Particle_t1882894987 * L_0 = __this->get_address_of_m_Particle_0();
		Vector3_t3722313464  L_1 = ___value0;
		Particle_set_position_m4147191379((Particle_t1882894987 *)L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_PositionSet_1((bool)1);
		return;
	}
}
extern "C"  void EmitParams_set_position_m3162245934_AdjustorThunk (RuntimeObject * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	EmitParams_t2216423628 * _thisAdjusted = reinterpret_cast<EmitParams_t2216423628 *>(__this + 1);
	EmitParams_set_position_m3162245934(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/EmitParams::set_velocity(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void EmitParams_set_velocity_m4290211678 (EmitParams_t2216423628 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	{
		Particle_t1882894987 * L_0 = __this->get_address_of_m_Particle_0();
		Vector3_t3722313464  L_1 = ___value0;
		Particle_set_velocity_m1686335204((Particle_t1882894987 *)L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_VelocitySet_2((bool)1);
		return;
	}
}
extern "C"  void EmitParams_set_velocity_m4290211678_AdjustorThunk (RuntimeObject * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	EmitParams_t2216423628 * _thisAdjusted = reinterpret_cast<EmitParams_t2216423628 *>(__this + 1);
	EmitParams_set_velocity_m4290211678(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/EmitParams::set_startLifetime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void EmitParams_set_startLifetime_m3865544057 (EmitParams_t2216423628 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Particle_t1882894987 * L_0 = __this->get_address_of_m_Particle_0();
		float L_1 = ___value0;
		Particle_set_startLifetime_m2608171500((Particle_t1882894987 *)L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_StartLifetimeSet_9((bool)1);
		return;
	}
}
extern "C"  void EmitParams_set_startLifetime_m3865544057_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	EmitParams_t2216423628 * _thisAdjusted = reinterpret_cast<EmitParams_t2216423628 *>(__this + 1);
	EmitParams_set_startLifetime_m3865544057(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/EmitParams::set_startSize(System.Single)
extern "C" IL2CPP_METHOD_ATTR void EmitParams_set_startSize_m814526808 (EmitParams_t2216423628 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Particle_t1882894987 * L_0 = __this->get_address_of_m_Particle_0();
		float L_1 = ___value0;
		Particle_set_startSize_m2554682920((Particle_t1882894987 *)L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_StartSizeSet_6((bool)1);
		return;
	}
}
extern "C"  void EmitParams_set_startSize_m814526808_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	EmitParams_t2216423628 * _thisAdjusted = reinterpret_cast<EmitParams_t2216423628 *>(__this + 1);
	EmitParams_set_startSize_m814526808(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/EmitParams::set_startColor(UnityEngine.Color32)
extern "C" IL2CPP_METHOD_ATTR void EmitParams_set_startColor_m4087630947 (EmitParams_t2216423628 * __this, Color32_t2600501292  ___value0, const RuntimeMethod* method)
{
	{
		Particle_t1882894987 * L_0 = __this->get_address_of_m_Particle_0();
		Color32_t2600501292  L_1 = ___value0;
		Particle_set_startColor_m3825027702((Particle_t1882894987 *)L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_StartColorSet_7((bool)1);
		return;
	}
}
extern "C"  void EmitParams_set_startColor_m4087630947_AdjustorThunk (RuntimeObject * __this, Color32_t2600501292  ___value0, const RuntimeMethod* method)
{
	EmitParams_t2216423628 * _thisAdjusted = reinterpret_cast<EmitParams_t2216423628 *>(__this + 1);
	EmitParams_set_startColor_m4087630947(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_pinvoke(const MainModule_t2320046318& unmarshaled, MainModule_t2320046318_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
extern "C" void MainModule_t2320046318_marshal_pinvoke_back(const MainModule_t2320046318_marshaled_pinvoke& marshaled, MainModule_t2320046318& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_pinvoke_cleanup(MainModule_t2320046318_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_com(const MainModule_t2320046318& unmarshaled, MainModule_t2320046318_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
extern "C" void MainModule_t2320046318_marshal_com_back(const MainModule_t2320046318_marshaled_com& marshaled, MainModule_t2320046318& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_com_cleanup(MainModule_t2320046318_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR void MainModule__ctor_m1745438521 (MainModule_t2320046318 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void MainModule__ctor_m1745438521_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	MainModule__ctor_m1745438521(_thisAdjusted, ___particleSystem0, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_startLifetime()
extern "C" IL2CPP_METHOD_ATTR MinMaxCurve_t1067599125  MainModule_get_startLifetime_m2343501481 (MainModule_t2320046318 * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MinMaxCurve_t1067599125  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(MinMaxCurve_t1067599125 ));
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_GetStartLifetime_m3113513190(NULL /*static, unused*/, L_0, (MinMaxCurve_t1067599125 *)(&V_0), /*hidden argument*/NULL);
		MinMaxCurve_t1067599125  L_1 = V_0;
		V_1 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		MinMaxCurve_t1067599125  L_2 = V_1;
		return L_2;
	}
}
extern "C"  MinMaxCurve_t1067599125  MainModule_get_startLifetime_m2343501481_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	return MainModule_get_startLifetime_m2343501481(_thisAdjusted, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startLifetimeMultiplier()
extern "C" IL2CPP_METHOD_ATTR float MainModule_get_startLifetimeMultiplier_m1798732576 (MainModule_t2320046318 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = MainModule_GetStartLifetimeMultiplier_m2481667184(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float MainModule_get_startLifetimeMultiplier_m1798732576_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	return MainModule_get_startLifetimeMultiplier_m1798732576(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startLifetimeMultiplier(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_set_startLifetimeMultiplier_m4121815467 (MainModule_t2320046318 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = ___value0;
		MainModule_SetStartLifetimeMultiplier_m3747684520(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startLifetimeMultiplier_m4121815467_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	MainModule_set_startLifetimeMultiplier_m4121815467(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeed(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C" IL2CPP_METHOD_ATTR void MainModule_set_startSpeed_m3763022313 (MainModule_t2320046318 * __this, MinMaxCurve_t1067599125  ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_SetStartSpeed_m1242675629(NULL /*static, unused*/, L_0, (MinMaxCurve_t1067599125 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startSpeed_m3763022313_AdjustorThunk (RuntimeObject * __this, MinMaxCurve_t1067599125  ___value0, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	MainModule_set_startSpeed_m3763022313(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_startSpeed()
extern "C" IL2CPP_METHOD_ATTR MinMaxCurve_t1067599125  MainModule_get_startSpeed_m3768187059 (MainModule_t2320046318 * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MinMaxCurve_t1067599125  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(MinMaxCurve_t1067599125 ));
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_GetStartSpeed_m1661822356(NULL /*static, unused*/, L_0, (MinMaxCurve_t1067599125 *)(&V_0), /*hidden argument*/NULL);
		MinMaxCurve_t1067599125  L_1 = V_0;
		V_1 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		MinMaxCurve_t1067599125  L_2 = V_1;
		return L_2;
	}
}
extern "C"  MinMaxCurve_t1067599125  MainModule_get_startSpeed_m3768187059_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	return MainModule_get_startSpeed_m3768187059(_thisAdjusted, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSpeedMultiplier()
extern "C" IL2CPP_METHOD_ATTR float MainModule_get_startSpeedMultiplier_m3876540512 (MainModule_t2320046318 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = MainModule_GetStartSpeedMultiplier_m1633461692(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float MainModule_get_startSpeedMultiplier_m3876540512_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	return MainModule_get_startSpeedMultiplier_m3876540512(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeedMultiplier(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_set_startSpeedMultiplier_m3632924240 (MainModule_t2320046318 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = ___value0;
		MainModule_SetStartSpeedMultiplier_m2549987384(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startSpeedMultiplier_m3632924240_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	MainModule_set_startSpeedMultiplier_m3632924240(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSize(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C" IL2CPP_METHOD_ATTR void MainModule_set_startSize_m1275429743 (MainModule_t2320046318 * __this, MinMaxCurve_t1067599125  ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_SetStartSizeX_m4044089380(NULL /*static, unused*/, L_0, (MinMaxCurve_t1067599125 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startSize_m1275429743_AdjustorThunk (RuntimeObject * __this, MinMaxCurve_t1067599125  ___value0, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	MainModule_set_startSize_m1275429743(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_startSize()
extern "C" IL2CPP_METHOD_ATTR MinMaxCurve_t1067599125  MainModule_get_startSize_m4033600788 (MainModule_t2320046318 * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MinMaxCurve_t1067599125  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(MinMaxCurve_t1067599125 ));
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_GetStartSizeX_m1773159447(NULL /*static, unused*/, L_0, (MinMaxCurve_t1067599125 *)(&V_0), /*hidden argument*/NULL);
		MinMaxCurve_t1067599125  L_1 = V_0;
		V_1 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		MinMaxCurve_t1067599125  L_2 = V_1;
		return L_2;
	}
}
extern "C"  MinMaxCurve_t1067599125  MainModule_get_startSize_m4033600788_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	return MainModule_get_startSize_m4033600788(_thisAdjusted, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSizeMultiplier()
extern "C" IL2CPP_METHOD_ATTR float MainModule_get_startSizeMultiplier_m1188945012 (MainModule_t2320046318 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = MainModule_GetStartSizeXMultiplier_m3834697463(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float MainModule_get_startSizeMultiplier_m1188945012_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	return MainModule_get_startSizeMultiplier_m1188945012(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSizeMultiplier(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_set_startSizeMultiplier_m2700116177 (MainModule_t2320046318 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = ___value0;
		MainModule_SetStartSizeXMultiplier_m1251507676(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startSizeMultiplier_m2700116177_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	MainModule_set_startSizeMultiplier_m2700116177(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.ParticleSystem/MainModule::get_maxParticles()
extern "C" IL2CPP_METHOD_ATTR int32_t MainModule_get_maxParticles_m2105979121 (MainModule_t2320046318 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = MainModule_GetMaxParticles_m2130637551(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t MainModule_get_maxParticles_m2105979121_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	return MainModule_get_maxParticles_m2105979121(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartLifetime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_GetStartLifetime_m3113513190 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*MainModule_GetStartLifetime_m3113513190_ftn) (ParticleSystem_t1800779281 *, MinMaxCurve_t1067599125 *);
	static MainModule_GetStartLifetime_m3113513190_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetStartLifetime_m3113513190_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetStartLifetime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartLifetimeMultiplier(UnityEngine.ParticleSystem,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_SetStartLifetimeMultiplier_m3747684520 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetStartLifetimeMultiplier_m3747684520_ftn) (ParticleSystem_t1800779281 *, float);
	static MainModule_SetStartLifetimeMultiplier_m3747684520_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartLifetimeMultiplier_m3747684520_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartLifetimeMultiplier(UnityEngine.ParticleSystem,System.Single)");
	_il2cpp_icall_func(___system0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::GetStartLifetimeMultiplier(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR float MainModule_GetStartLifetimeMultiplier_m2481667184 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method)
{
	typedef float (*MainModule_GetStartLifetimeMultiplier_m2481667184_ftn) (ParticleSystem_t1800779281 *);
	static MainModule_GetStartLifetimeMultiplier_m2481667184_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetStartLifetimeMultiplier_m2481667184_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetStartLifetimeMultiplier(UnityEngine.ParticleSystem)");
	float retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSpeed(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_SetStartSpeed_m1242675629 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetStartSpeed_m1242675629_ftn) (ParticleSystem_t1800779281 *, MinMaxCurve_t1067599125 *);
	static MainModule_SetStartSpeed_m1242675629_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartSpeed_m1242675629_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartSpeed(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartSpeed(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_GetStartSpeed_m1661822356 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*MainModule_GetStartSpeed_m1661822356_ftn) (ParticleSystem_t1800779281 *, MinMaxCurve_t1067599125 *);
	static MainModule_GetStartSpeed_m1661822356_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetStartSpeed_m1661822356_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetStartSpeed(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSpeedMultiplier(UnityEngine.ParticleSystem,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_SetStartSpeedMultiplier_m2549987384 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetStartSpeedMultiplier_m2549987384_ftn) (ParticleSystem_t1800779281 *, float);
	static MainModule_SetStartSpeedMultiplier_m2549987384_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartSpeedMultiplier_m2549987384_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartSpeedMultiplier(UnityEngine.ParticleSystem,System.Single)");
	_il2cpp_icall_func(___system0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::GetStartSpeedMultiplier(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR float MainModule_GetStartSpeedMultiplier_m1633461692 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method)
{
	typedef float (*MainModule_GetStartSpeedMultiplier_m1633461692_ftn) (ParticleSystem_t1800779281 *);
	static MainModule_GetStartSpeedMultiplier_m1633461692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetStartSpeedMultiplier_m1633461692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetStartSpeedMultiplier(UnityEngine.ParticleSystem)");
	float retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSizeX(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_SetStartSizeX_m4044089380 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetStartSizeX_m4044089380_ftn) (ParticleSystem_t1800779281 *, MinMaxCurve_t1067599125 *);
	static MainModule_SetStartSizeX_m4044089380_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartSizeX_m4044089380_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartSizeX(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartSizeX(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C" IL2CPP_METHOD_ATTR void MainModule_GetStartSizeX_m1773159447 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*MainModule_GetStartSizeX_m1773159447_ftn) (ParticleSystem_t1800779281 *, MinMaxCurve_t1067599125 *);
	static MainModule_GetStartSizeX_m1773159447_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetStartSizeX_m1773159447_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetStartSizeX(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartSizeXMultiplier(UnityEngine.ParticleSystem,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainModule_SetStartSizeXMultiplier_m1251507676 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetStartSizeXMultiplier_m1251507676_ftn) (ParticleSystem_t1800779281 *, float);
	static MainModule_SetStartSizeXMultiplier_m1251507676_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartSizeXMultiplier_m1251507676_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartSizeXMultiplier(UnityEngine.ParticleSystem,System.Single)");
	_il2cpp_icall_func(___system0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::GetStartSizeXMultiplier(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR float MainModule_GetStartSizeXMultiplier_m3834697463 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method)
{
	typedef float (*MainModule_GetStartSizeXMultiplier_m3834697463_ftn) (ParticleSystem_t1800779281 *);
	static MainModule_GetStartSizeXMultiplier_m3834697463_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetStartSizeXMultiplier_m3834697463_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetStartSizeXMultiplier(UnityEngine.ParticleSystem)");
	float retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
// System.Int32 UnityEngine.ParticleSystem/MainModule::GetMaxParticles(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR int32_t MainModule_GetMaxParticles_m2130637551 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const RuntimeMethod* method)
{
	typedef int32_t (*MainModule_GetMaxParticles_m2130637551_ftn) (ParticleSystem_t1800779281 *);
	static MainModule_GetMaxParticles_m2130637551_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetMaxParticles_m2130637551_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetMaxParticles(UnityEngine.ParticleSystem)");
	int32_t retVal = _il2cpp_icall_func(___system0);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke(const MinMaxCurve_t1067599125& unmarshaled, MinMaxCurve_t1067599125_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3046754366_marshal_pinvoke(*unmarshaled.get_m_CurveMin_2(), marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3046754366_marshal_pinvoke(*unmarshaled.get_m_CurveMax_3(), marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke_back(const MinMaxCurve_t1067599125_marshaled_pinvoke& marshaled, MinMaxCurve_t1067599125& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinMaxCurve_t1067599125_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	unmarshaled.set_m_CurveMin_2((AnimationCurve_t3046754366 *)il2cpp_codegen_object_new(AnimationCurve_t3046754366_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m3000526466(unmarshaled.get_m_CurveMin_2(), NULL);
	AnimationCurve_t3046754366_marshal_pinvoke_back(marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	unmarshaled.set_m_CurveMax_3((AnimationCurve_t3046754366 *)il2cpp_codegen_object_new(AnimationCurve_t3046754366_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m3000526466(unmarshaled.get_m_CurveMax_3(), NULL);
	AnimationCurve_t3046754366_marshal_pinvoke_back(marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke_cleanup(MinMaxCurve_t1067599125_marshaled_pinvoke& marshaled)
{
	AnimationCurve_t3046754366_marshal_pinvoke_cleanup(marshaled.___m_CurveMin_2);
	AnimationCurve_t3046754366_marshal_pinvoke_cleanup(marshaled.___m_CurveMax_3);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_com(const MinMaxCurve_t1067599125& unmarshaled, MinMaxCurve_t1067599125_marshaled_com& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3046754366_marshal_com(*unmarshaled.get_m_CurveMin_2(), *marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3046754366_marshal_com(*unmarshaled.get_m_CurveMax_3(), *marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t1067599125_marshal_com_back(const MinMaxCurve_t1067599125_marshaled_com& marshaled, MinMaxCurve_t1067599125& unmarshaled)
{
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	if (unmarshaled.get_m_CurveMin_2() != NULL)
	{
		AnimationCurve__ctor_m3000526466(unmarshaled.get_m_CurveMin_2(), NULL);
		AnimationCurve_t3046754366_marshal_com_back(*marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	}
	if (unmarshaled.get_m_CurveMax_3() != NULL)
	{
		AnimationCurve__ctor_m3000526466(unmarshaled.get_m_CurveMax_3(), NULL);
		AnimationCurve_t3046754366_marshal_com_back(*marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	}
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_com_cleanup(MinMaxCurve_t1067599125_marshaled_com& marshaled)
{
	if (&(*marshaled.___m_CurveMin_2) != NULL) AnimationCurve_t3046754366_marshal_com_cleanup(*marshaled.___m_CurveMin_2);
	if (&(*marshaled.___m_CurveMax_3) != NULL) AnimationCurve_t3046754366_marshal_com_cleanup(*marshaled.___m_CurveMax_3);
}
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MinMaxCurve__ctor_m1734431933 (MinMaxCurve_t1067599125 * __this, float ___constant0, const RuntimeMethod* method)
{
	{
		__this->set_m_Mode_0(0);
		__this->set_m_CurveMultiplier_1((0.0f));
		__this->set_m_CurveMin_2((AnimationCurve_t3046754366 *)NULL);
		__this->set_m_CurveMax_3((AnimationCurve_t3046754366 *)NULL);
		__this->set_m_ConstantMin_4((0.0f));
		float L_0 = ___constant0;
		__this->set_m_ConstantMax_5(L_0);
		return;
	}
}
extern "C"  void MinMaxCurve__ctor_m1734431933_AdjustorThunk (RuntimeObject * __this, float ___constant0, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t1067599125 *>(__this + 1);
	MinMaxCurve__ctor_m1734431933(_thisAdjusted, ___constant0, method);
}
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::set_mode(UnityEngine.ParticleSystemCurveMode)
extern "C" IL2CPP_METHOD_ATTR void MinMaxCurve_set_mode_m1255242291 (MinMaxCurve_t1067599125 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_Mode_0(L_0);
		return;
	}
}
extern "C"  void MinMaxCurve_set_mode_m1255242291_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t1067599125 *>(__this + 1);
	MinMaxCurve_set_mode_m1255242291(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constantMax()
extern "C" IL2CPP_METHOD_ATTR float MinMaxCurve_get_constantMax_m743132770 (MinMaxCurve_t1067599125 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_ConstantMax_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float MinMaxCurve_get_constantMax_m743132770_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t1067599125 *>(__this + 1);
	return MinMaxCurve_get_constantMax_m743132770(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::set_constantMax(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MinMaxCurve_set_constantMax_m4228540433 (MinMaxCurve_t1067599125 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_ConstantMax_5(L_0);
		return;
	}
}
extern "C"  void MinMaxCurve_set_constantMax_m4228540433_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t1067599125 *>(__this + 1);
	MinMaxCurve_set_constantMax_m4228540433(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constantMin()
extern "C" IL2CPP_METHOD_ATTR float MinMaxCurve_get_constantMin_m744050282 (MinMaxCurve_t1067599125 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_ConstantMin_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float MinMaxCurve_get_constantMin_m744050282_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t1067599125 *>(__this + 1);
	return MinMaxCurve_get_constantMin_m744050282(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::set_constantMin(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MinMaxCurve_set_constantMin_m1844578410 (MinMaxCurve_t1067599125 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_ConstantMin_4(L_0);
		return;
	}
}
extern "C"  void MinMaxCurve_set_constantMin_m1844578410_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t1067599125 *>(__this + 1);
	MinMaxCurve_set_constantMin_m1844578410(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constant()
extern "C" IL2CPP_METHOD_ATTR float MinMaxCurve_get_constant_m2963124720 (MinMaxCurve_t1067599125 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_ConstantMax_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float MinMaxCurve_get_constant_m2963124720_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t1067599125 *>(__this + 1);
	return MinMaxCurve_get_constant_m2963124720(_thisAdjusted, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single)
extern "C" IL2CPP_METHOD_ATTR MinMaxCurve_t1067599125  MinMaxCurve_op_Implicit_m2087694379 (RuntimeObject * __this /* static, unused */, float ___constant0, const RuntimeMethod* method)
{
	MinMaxCurve_t1067599125  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___constant0;
		MinMaxCurve_t1067599125  L_1;
		memset(&L_1, 0, sizeof(L_1));
		MinMaxCurve__ctor_m1734431933((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		MinMaxCurve_t1067599125  L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.ParticleSystem/Particle::set_lifetime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_lifetime_m1971908220 (Particle_t1882894987 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Particle_set_remainingLifetime_m3875738131((Particle_t1882894987 *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Particle_set_lifetime_m1971908220_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_lifetime_m1971908220(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Particle_get_position_m855792854 (Particle_t1882894987 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Position_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3722313464  Particle_get_position_m855792854_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	return Particle_get_position_m855792854(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_position_m4147191379 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_m_Position_0(L_0);
		return;
	}
}
extern "C"  void Particle_set_position_m4147191379_AdjustorThunk (RuntimeObject * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_position_m4147191379(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::get_velocity()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Particle_get_velocity_m2555333515 (Particle_t1882894987 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Velocity_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3722313464  Particle_get_velocity_m2555333515_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	return Particle_get_velocity_m2555333515(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_velocity_m1686335204 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_m_Velocity_1(L_0);
		return;
	}
}
extern "C"  void Particle_set_velocity_m1686335204_AdjustorThunk (RuntimeObject * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_velocity_m1686335204(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/Particle::get_remainingLifetime()
extern "C" IL2CPP_METHOD_ATTR float Particle_get_remainingLifetime_m4053779941 (Particle_t1882894987 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Lifetime_10();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Particle_get_remainingLifetime_m4053779941_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	return Particle_get_remainingLifetime_m4053779941(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_remainingLifetime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_remainingLifetime_m3875738131 (Particle_t1882894987 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Lifetime_10(L_0);
		return;
	}
}
extern "C"  void Particle_set_remainingLifetime_m3875738131_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_remainingLifetime_m3875738131(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startLifetime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_startLifetime_m2608171500 (Particle_t1882894987 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_StartLifetime_11(L_0);
		return;
	}
}
extern "C"  void Particle_set_startLifetime_m2608171500_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_startLifetime_m2608171500(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_startColor_m3825027702 (Particle_t1882894987 * __this, Color32_t2600501292  ___value0, const RuntimeMethod* method)
{
	{
		Color32_t2600501292  L_0 = ___value0;
		__this->set_m_StartColor_8(L_0);
		return;
	}
}
extern "C"  void Particle_set_startColor_m3825027702_AdjustorThunk (RuntimeObject * __this, Color32_t2600501292  ___value0, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_startColor_m3825027702(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_randomSeed(System.UInt32)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_randomSeed_m2900137887 (Particle_t1882894987 * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_m_RandomSeed_9(L_0);
		return;
	}
}
extern "C"  void Particle_set_randomSeed_m2900137887_AdjustorThunk (RuntimeObject * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_randomSeed_m2900137887(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_startSize_m2554682920 (Particle_t1882894987 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		float L_1 = ___value0;
		float L_2 = ___value0;
		Vector3_t3722313464  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m3353183577((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->set_m_StartSize_7(L_3);
		return;
	}
}
extern "C"  void Particle_set_startSize_m2554682920_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_startSize_m2554682920(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_rotation3D(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_rotation3D_m2156157200 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Particle_set_rotation3D_m2156157200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		__this->set_m_Rotation_5(L_1);
		return;
	}
}
extern "C"  void Particle_set_rotation3D_m2156157200_AdjustorThunk (RuntimeObject * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_rotation3D_m2156157200(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity3D(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Particle_set_angularVelocity3D_m3163963446 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Particle_set_angularVelocity3D_m3163963446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		__this->set_m_AngularVelocity_6(L_1);
		return;
	}
}
extern "C"  void Particle_set_angularVelocity3D_m3163963446_AdjustorThunk (RuntimeObject * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_angularVelocity3D_m3163963446(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t3608330829_marshal_pinvoke(const ShapeModule_t3608330829& unmarshaled, ShapeModule_t3608330829_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
extern "C" void ShapeModule_t3608330829_marshal_pinvoke_back(const ShapeModule_t3608330829_marshaled_pinvoke& marshaled, ShapeModule_t3608330829& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t3608330829_marshal_pinvoke_cleanup(ShapeModule_t3608330829_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t3608330829_marshal_com(const ShapeModule_t3608330829& unmarshaled, ShapeModule_t3608330829_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
extern "C" void ShapeModule_t3608330829_marshal_com_back(const ShapeModule_t3608330829_marshaled_com& marshaled, ShapeModule_t3608330829& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t3608330829_marshal_com_cleanup(ShapeModule_t3608330829_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem)
extern "C" IL2CPP_METHOD_ATTR void ShapeModule__ctor_m3109297265 (ShapeModule_t3608330829 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void ShapeModule__ctor_m3109297265_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const RuntimeMethod* method)
{
	ShapeModule_t3608330829 * _thisAdjusted = reinterpret_cast<ShapeModule_t3608330829 *>(__this + 1);
	ShapeModule__ctor_m3109297265(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::set_shapeType(UnityEngine.ParticleSystemShapeType)
extern "C" IL2CPP_METHOD_ATTR void ShapeModule_set_shapeType_m507168252 (ShapeModule_t3608330829 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		int32_t L_1 = ___value0;
		ShapeModule_SetShapeType_m2621352696(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ShapeModule_set_shapeType_m507168252_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	ShapeModule_t3608330829 * _thisAdjusted = reinterpret_cast<ShapeModule_t3608330829 *>(__this + 1);
	ShapeModule_set_shapeType_m507168252(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::SetShapeType(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemShapeType)
extern "C" IL2CPP_METHOD_ATTR void ShapeModule_SetShapeType_m2621352696 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, int32_t ___value1, const RuntimeMethod* method)
{
	typedef void (*ShapeModule_SetShapeType_m2621352696_ftn) (ParticleSystem_t1800779281 *, int32_t);
	static ShapeModule_SetShapeType_m2621352696_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ShapeModule_SetShapeType_m2621352696_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/ShapeModule::SetShapeType(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemShapeType)");
	_il2cpp_icall_func(___system0, ___value1);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 UnityEngine.ParticleSystemExtensionsImpl::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,System.Object)
extern "C" IL2CPP_METHOD_ATTR int32_t ParticleSystemExtensionsImpl_GetCollisionEvents_m2141038518 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___ps0, GameObject_t1113636619 * ___go1, RuntimeObject * ___collisionEvents2, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystemExtensionsImpl_GetCollisionEvents_m2141038518_ftn) (ParticleSystem_t1800779281 *, GameObject_t1113636619 *, RuntimeObject *);
	static ParticleSystemExtensionsImpl_GetCollisionEvents_m2141038518_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemExtensionsImpl_GetCollisionEvents_m2141038518_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemExtensionsImpl::GetCollisionEvents(UnityEngine.ParticleSystem,UnityEngine.GameObject,System.Object)");
	int32_t retVal = _il2cpp_icall_func(___ps0, ___go1, ___collisionEvents2);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 UnityEngine.ParticleSystemRenderer::Internal_GetMeshCount()
extern "C" IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_Internal_GetMeshCount_m2639012328 (ParticleSystemRenderer_t2065813411 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystemRenderer_Internal_GetMeshCount_m2639012328_ftn) (ParticleSystemRenderer_t2065813411 *);
	static ParticleSystemRenderer_Internal_GetMeshCount_m2639012328_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_Internal_GetMeshCount_m2639012328_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::Internal_GetMeshCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
